//************************************************************************
// H2D2 - External declaration of public symbols
// List of modules - registration of faked Dll/SO
// Entry point: void fake_dlls()
//
// This file is generated automatically, any change will be lost.
// Generated 2012-02-07 10:10:36.164000
//************************************************************************
#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif

void fake_dll_algo_remesh();
void fake_dll_b_MUMPS();
void fake_dll_cd2d_age();
void fake_dll_cd2d_b1l();
void fake_dll_cd2d_bnl();
void fake_dll_cd2d_bse();
void fake_dll_cd2d_ccn();
void fake_dll_cd2d_clf();
void fake_dll_cd2d_mes();
void fake_dll_cd2d_mes1();
void fake_dll_cd2d_mes3();
void fake_dll_cd2d_mesn();
void fake_dll_cd2d_tmp();
void fake_dll_h2d2_algo();
void fake_dll_h2d2_elem();
void fake_dll_h2d2_gpu();
void fake_dll_h2d2_krnl();
void fake_dll_h2d2_post();
void fake_dll_h2d2_svc();
void fake_dll_h2d2_umef();
void fake_dll_lmgo_l2();
void fake_dll_lmgo_l3l();
void fake_dll_lmgo_p12l();
void fake_dll_lmgo_t3();
void fake_dll_lmgo_t6l();
void fake_dll_ni_p12l();
void fake_dll_ni_t6l();
void fake_dll_ns_macro3d();
void fake_dll_numr_bgl();
void fake_dll_numr_front();
void fake_dll_numr_pmetis();
void fake_dll_numr_scotch();
void fake_dll_sed2d();
void fake_dll_slvr_mkl();
void fake_dll_slvr_mumps();
void fake_dll_slvr_slus();
void fake_dll_sv2d();
void fake_dll_tool_gbp();

#ifdef FAKE_DLL
void fake_dlls()
{
  fake_dll_b_MUMPS();
  fake_dll_cd2d_age();
  fake_dll_cd2d_b1l();
  //fake_dll_cd2d_bnl();
  fake_dll_cd2d_bse();
  fake_dll_cd2d_ccn();
  fake_dll_cd2d_clf();
  fake_dll_cd2d_mes();
  //fake_dll_cd2d_mes1();
  //fake_dll_cd2d_mes3();
  //fake_dll_cd2d_mesn();
  fake_dll_cd2d_tmp();
  fake_dll_h2d2_algo();
  fake_dll_h2d2_elem();
  fake_dll_h2d2_gpu();
  fake_dll_h2d2_krnl();
  fake_dll_h2d2_post();
  fake_dll_h2d2_svc();
  fake_dll_h2d2_umef();
  fake_dll_lmgo_l2();
  fake_dll_lmgo_l3l();
  fake_dll_lmgo_t3();
  fake_dll_lmgo_t6l();
  fake_dll_numr_front();
  fake_dll_numr_pmetis();
  fake_dll_numr_scotch();
#if defined(H2D2_CMPLR_INTEL)
  fake_dll_slvr_mkl();
#endif
  fake_dll_slvr_mumps();
  fake_dll_slvr_slus();
  fake_dll_sv2d();
  
#if defined(H2D2_HAS_TOOL_GBP)
  fake_dll_tool_gbp();
#endif

#if defined(H2D2_HAS_ELEMENT_NSMACRO3D)
  fake_dll_lmgo_p12l();
  fake_dll_ni_p12l();
  fake_dll_ni_t6l();
  fake_dll_ns_macro3d();
#endif

#if defined(H2D2_HAS_ELEMENT_SED2D)
  fake_dll_sed2d();
#endif

#if defined(H2D2_HAS_REMESH)
   fake_dll_algo_remesh();
#endif
  
}
#endif    //  FAKE_DLL

#ifdef __cplusplus
}
#endif

