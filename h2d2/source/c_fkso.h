//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire:
//
// Description:
//
// Notes:
//
//************************************************************************
#ifndef C_FKSO_H_DEJA_INCLU
#define C_FKSO_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_FKSO_INI F2C_CONF_DECOR_FNC(C_FKSO_INI, c_fkso_ini)


F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FKSO_INI ();


#ifdef __cplusplus
}
#endif

#endif   // C_FKSO_H_DEJA_INCLU
