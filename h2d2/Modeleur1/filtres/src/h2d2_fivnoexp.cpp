//************************************************************************
// Fichier:  h2d2_fivnoexp.cpp
//
// Description:
//    Prend un vno Vectoriel pour le transformer
//    en fichier de sollicitation répartis pour
//    HYDROSIM.
//
//************************************************************************
#include <string>
#include <fstream>
#include <sstream>
#include <math.h>

using namespace std;

//********* Constantes **************
const unsigned int PRECISION = 17;
const unsigned int BUF_SIZE = 1024;
const char ESPACE = ' ';
const char FINL = '\n';

//*********** prototypes ************

int filtre(const string&, const string&);


int main(int nbArg, char* arguments[])
{
   int err = 0;

	if (nbArg < 3)
	{
      err = -1;
	}
   if (!err)
   {
      string isName(arguments[1]);
      string osName(arguments[2]);

	   err = filtre(isName, osName);
   }
   return err;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:  const string& isName: nom du fichier d'entrée
//          const string& osName: nom du fichier de sortie
//
// Sortie: -----
//
// Résultat:
//************************************************************************
int filtre(const string& isName, const string& osName)
{
   int err = 0;

	ifstream is(isName.c_str());
	ofstream os(osName.c_str());

   unsigned int nbrNoeuds;
   unsigned int dimension;
   double temps = 0.0;
   char buffer[BUF_SIZE];

   is.getline(buffer, BUF_SIZE-1);
   istringstream isb(buffer);
   isb >> nbrNoeuds;
   isb >> dimension;
   if (! isb) dimension = 1;
   os << nbrNoeuds << ESPACE << dimension << ESPACE << temps << FINL;

   for (unsigned int i=0; i < nbrNoeuds; ++i)
   {
      is.getline(buffer, BUF_SIZE-1);
      os << buffer << FINL;
   }

   return err;
}
