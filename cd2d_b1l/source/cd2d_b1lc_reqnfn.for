C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction CD2D_B1LC_REQNFN retourne le nom de la fonction
C     associé au code demandé. Le nom combine le nom de la DLL et
C     de la fonction sous la forme "nomFonction@nomDll".
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     KFNC     Nom de la fonction stocké dans une table INTEGER
C
C Notes:
C************************************************************************
      FUNCTION CD2D_B1LC_REQNFN(KFNC, IFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_B1LC_REQNFN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER CD2D_B1LC_REQNFN
      INTEGER KFNC(*)
      INTEGER IFNC

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'

      INTEGER        CD2D_B1L_REQNFN
      INTEGER        CD2D_BSEC_REQNFN
      INTEGER        I
      INTEGER        IERR
      INTEGER        JF(16)
      CHARACTER*(64) NF
      EQUIVALENCE (NF, JF)
      LOGICAL        SAUTE
C-----------------------------------------------------------------------

      IERR = CD2D_BSEC_REQNFN(KFNC, IFNC)
      IERR = CD2D_B1L_REQNFN (KFNC, IFNC)

      SAUTE = .FALSE.
      IF (IFNC .EQ. EA_FUNC_ASMF) THEN
                                    NF = 'CD2D_B1LC_ASMF@cd2d_b1l'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMK) THEN
                                    NF = 'CD2D_B1LC_ASMK@cd2d_b1l'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKT) THEN
                                    NF = 'CD2D_B1LC_ASMKT@cd2d_b1l'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKU) THEN
                                    NF = 'CD2D_B1LC_ASMKU@cd2d_b1l'
      ELSEIF (IFNC .EQ. EA_FUNC_REQPRM) THEN
                                    NF = 'CD2D_B1LC_REQPRM@cd2d_b1l'
      ELSEIF (IFNC .EQ. EA_FUNC_REQNFN) THEN
                                    NF = 'CD2D_B1LC_REQNFN@cd2d_b1l'
      ELSE
         SAUTE = .TRUE.
      ENDIF

      IF (.NOT. SAUTE) THEN
         DO I=1,16
            KFNC(I) = JF(I)
         ENDDO
         CALL ERR_RESET()
      ENDIF

9999  CONTINUE
      CD2D_B1LC_REQNFN = ERR_TYP()
      RETURN
      END
