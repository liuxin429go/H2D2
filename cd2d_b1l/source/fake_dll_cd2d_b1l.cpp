//************************************************************************
// H2D2 - External declaration of public symbols
// Module: cd2d_b1l
// Entry point: extern "C" void fake_dll_cd2d_b1l()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:46:55.470084
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class CD2D_B1L
F_PROT(CD2D_B1L_ASGCMN, cd2d_b1l_asgcmn);
F_PROT(CD2D_B1L_PRCCLIM, cd2d_b1l_prcclim);
F_PROT(CD2D_B1L_PRNPRNO, cd2d_b1l_prnprno);
F_PROT(CD2D_B1L_REQNFN, cd2d_b1l_reqnfn);
F_PROT(CD2D_B1L_REQPRM, cd2d_b1l_reqprm);
 
// ---  class CD2D_B1LC
F_PROT(CD2D_B1LC_ASGCMN, cd2d_b1lc_asgcmn);
F_PROT(CD2D_B1LC_ASMF, cd2d_b1lc_asmf);
F_PROT(CD2D_B1LC_ASMK, cd2d_b1lc_asmk);
F_PROT(CD2D_B1LC_ASMKT, cd2d_b1lc_asmkt);
F_PROT(CD2D_B1LC_ASMKU, cd2d_b1lc_asmku);
F_PROT(CD2D_B1LC_REQNFN, cd2d_b1lc_reqnfn);
F_PROT(CD2D_B1LC_REQPRM, cd2d_b1lc_reqprm);
 
// ---  class CD2D_B1LN
F_PROT(CD2D_B1LN_ASGCMN, cd2d_b1ln_asgcmn);
F_PROT(CD2D_B1LN_ASMF, cd2d_b1ln_asmf);
F_PROT(CD2D_B1LN_ASMK, cd2d_b1ln_asmk);
F_PROT(CD2D_B1LN_ASMKT, cd2d_b1ln_asmkt);
F_PROT(CD2D_B1LN_ASMKU, cd2d_b1ln_asmku);
F_PROT(CD2D_B1LN_REQNFN, cd2d_b1ln_reqnfn);
F_PROT(CD2D_B1LN_REQPRM, cd2d_b1ln_reqprm);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_cd2d_b1l()
{
   static char libname[] = "cd2d_b1l";
 
   // ---  class CD2D_B1L
   F_RGST(CD2D_B1L_ASGCMN, cd2d_b1l_asgcmn, libname);
   F_RGST(CD2D_B1L_PRCCLIM, cd2d_b1l_prcclim, libname);
   F_RGST(CD2D_B1L_PRNPRNO, cd2d_b1l_prnprno, libname);
   F_RGST(CD2D_B1L_REQNFN, cd2d_b1l_reqnfn, libname);
   F_RGST(CD2D_B1L_REQPRM, cd2d_b1l_reqprm, libname);
 
   // ---  class CD2D_B1LC
   F_RGST(CD2D_B1LC_ASGCMN, cd2d_b1lc_asgcmn, libname);
   F_RGST(CD2D_B1LC_ASMF, cd2d_b1lc_asmf, libname);
   F_RGST(CD2D_B1LC_ASMK, cd2d_b1lc_asmk, libname);
   F_RGST(CD2D_B1LC_ASMKT, cd2d_b1lc_asmkt, libname);
   F_RGST(CD2D_B1LC_ASMKU, cd2d_b1lc_asmku, libname);
   F_RGST(CD2D_B1LC_REQNFN, cd2d_b1lc_reqnfn, libname);
   F_RGST(CD2D_B1LC_REQPRM, cd2d_b1lc_reqprm, libname);
 
   // ---  class CD2D_B1LN
   F_RGST(CD2D_B1LN_ASGCMN, cd2d_b1ln_asgcmn, libname);
   F_RGST(CD2D_B1LN_ASMF, cd2d_b1ln_asmf, libname);
   F_RGST(CD2D_B1LN_ASMK, cd2d_b1ln_asmk, libname);
   F_RGST(CD2D_B1LN_ASMKT, cd2d_b1ln_asmkt, libname);
   F_RGST(CD2D_B1LN_ASMKU, cd2d_b1ln_asmku, libname);
   F_RGST(CD2D_B1LN_REQNFN, cd2d_b1ln_reqnfn, libname);
   F_RGST(CD2D_B1LN_REQPRM, cd2d_b1ln_reqprm, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
