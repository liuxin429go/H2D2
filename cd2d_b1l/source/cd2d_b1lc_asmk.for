C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de convection-diffusion eulerienne 2-D
C     Contaminant conservatif
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_B1LC_ASMK
C
C Description:
C     La fonction CD2D_B1LC_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_B1LC_ASMK(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_B1LC_ASMK
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'

      INTEGER CD2D_B1L_NDLEMAX
      PARAMETER (CD2D_B1L_NDLEMAX = 3)

      REAL*8  VKE  (CD2D_B1L_NDLEMAX, CD2D_B1L_NDLEMAX)
      REAL*8  VDLE (CD2D_B1L_NDLEMAX)
      INTEGER KLOCE(CD2D_B1L_NDLEMAX)
      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN  .EQ. 1)
D     CALL ERR_PRE(LM_CMMN_NDLEV .EQ. CD2D_B1L_NDLEMAX)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     ASSEMBLAGE DU TERME PUITS-SOURCE DANS LA MATRICE
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKE, VDLE, KLOCE)
         CALL CD2D_B1L_ASMK_V(KLOCE,
     &                        VDLE,
     &                        VKE,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLR,
     &                        VDLG,
     &                        HMTX,
     &                        F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     ASSEMBLAGE DE LA MATRICE DE TRANSPORT-DIFFUSION
      IF (ERR_GOOD()) THEN
         CALL CD2D_BSEC_ASMK(VCORG,
     &                       KLOCN,
     &                       KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       VPRES,
     &                       VSOLC,
     &                       VSOLR,
     &                       KCLCND,
     &                       VCLCNV,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       VCLDST,
     &                       KDIMP,
     &                       VDIMP,
     &                       KEIMP,
     &                       VDLG,
     &                       HMTX,
     &                       F_ASM)
      ENDIF

      RETURN
      END
