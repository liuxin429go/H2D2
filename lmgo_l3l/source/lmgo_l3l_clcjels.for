C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_L3L_CLCJELS
C
C Description:
C     La fonction LMGO_L3L_CLCJELS calcule les métriques pour des
C     éléments de surface de type L3L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_L3L_CLCJELS(NDIM,
     &                          NNT,
     &                          NCELV,
     &                          NCELS,
     &                          NELV,
     &                          NELS,
     &                          NDJV,
     &                          NDJS,
     &                          VCORG,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_L3L_CLCJELS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDIM
      INTEGER NNT
      INTEGER NCELV
      INTEGER NCELS
      INTEGER NELV
      INTEGER NELS
      INTEGER NDJV
      INTEGER NDJS
      REAL*8  VCORG(NDIM, NNT)
      INTEGER KNGV (NCELV,NELV)
      INTEGER KNGS (NCELS,NELS)
      REAL*8  VDJV (NDJV, NELV)
      REAL*8  VDJS (NDJS, NELS)

      INCLUDE 'lmgo_l3l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_TODO('LMGO_L3L_CLCJELS  jamais teste, SUREMENT FAUX!!!')
      CALL ERR_ASR(.FALSE.)

      LMGO_L3L_CLCJELS = ERR_TYP()
      RETURN
      END

