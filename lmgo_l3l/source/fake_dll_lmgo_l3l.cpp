//************************************************************************
// H2D2 - External declaration of public symbols
// Module: lmgo_l3l
// Entry point: extern "C" void fake_dll_lmgo_l3l()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:02.614000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class LM_GOL3L
F_PROT(LM_GOL3L_000, lm_gol3l_000);
F_PROT(LM_GOL3L_999, lm_gol3l_999);
F_PROT(LM_GOL3L_CTR, lm_gol3l_ctr);
F_PROT(LM_GOL3L_DTR, lm_gol3l_dtr);
F_PROT(LM_GOL3L_INI, lm_gol3l_ini);
F_PROT(LM_GOL3L_RST, lm_gol3l_rst);
F_PROT(LM_GOL3L_REQHBASE, lm_gol3l_reqhbase);
F_PROT(LM_GOL3L_HVALIDE, lm_gol3l_hvalide);
F_PROT(LM_GOL3L_CLCJELV, lm_gol3l_clcjelv);
 
// ---  class LMGO_L3L
F_PROT(LMGO_L3L_ASGCMN, lmgo_l3l_asgcmn);
F_PROT(LMGO_L3L_ASMESCL, lmgo_l3l_asmescl);
F_PROT(LMGO_L3L_CLCJELS, lmgo_l3l_clcjels);
F_PROT(LMGO_L3L_CLCJELV, lmgo_l3l_clcjelv);
F_PROT(LMGO_L3L_REQNFN, lmgo_l3l_reqnfn);
F_PROT(LMGO_L3L_REQPRM, lmgo_l3l_reqprm);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_lmgo_l3l()
{
   static char libname[] = "lmgo_l3l";
 
   // ---  class LM_GOL3L
   F_RGST(LM_GOL3L_000, lm_gol3l_000, libname);
   F_RGST(LM_GOL3L_999, lm_gol3l_999, libname);
   F_RGST(LM_GOL3L_CTR, lm_gol3l_ctr, libname);
   F_RGST(LM_GOL3L_DTR, lm_gol3l_dtr, libname);
   F_RGST(LM_GOL3L_INI, lm_gol3l_ini, libname);
   F_RGST(LM_GOL3L_RST, lm_gol3l_rst, libname);
   F_RGST(LM_GOL3L_REQHBASE, lm_gol3l_reqhbase, libname);
   F_RGST(LM_GOL3L_HVALIDE, lm_gol3l_hvalide, libname);
   F_RGST(LM_GOL3L_CLCJELV, lm_gol3l_clcjelv, libname);
 
   // ---  class LMGO_L3L
   F_RGST(LMGO_L3L_ASGCMN, lmgo_l3l_asgcmn, libname);
   F_RGST(LMGO_L3L_ASMESCL, lmgo_l3l_asmescl, libname);
   F_RGST(LMGO_L3L_CLCJELS, lmgo_l3l_clcjels, libname);
   F_RGST(LMGO_L3L_CLCJELV, lmgo_l3l_clcjelv, libname);
   F_RGST(LMGO_L3L_REQNFN, lmgo_l3l_reqnfn, libname);
   F_RGST(LMGO_L3L_REQPRM, lmgo_l3l_reqprm, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
