C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_L3L_ASMPEAU
C
C Description:
C     La fonction LMGO_L3L_ASMPEAU assemble la peau pour des
C     éléments de volume de type L3L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_L3L_ASMPEAU(NCELV,
     &                         NELV,
     &                         KNGV,
     &                         NNT,
     &                         KPNT,
     &                         NLIEN,
     &                         KLIEN,
     &                         INEXT)

      IMPLICIT NONE

      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER NNT
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER INEXT

      INCLUDE 'lmgo_l3l.fi'
      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF
      INTEGER INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------

      CALL LOG_TODO('LMGO_L3L_ASMPEAU   jamais teste, SUREMENT FAUX!!!')

      INFO(2) = 0
      DO IE=1,NELV

C---        NOEUD 1
         INFO(1) = KNGV(1, IE)
         INFO(3) = IE
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        NOEUD 2
         INFO(1) = KNGV(3, IE)
         INFO(3) = IE
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

      ENDDO

9999  CONTINUE
      LMGO_L3L_ASMPEAU = ERR_TYP()
      RETURN
      END

