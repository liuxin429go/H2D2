//************************************************************************
// H2D2 - External declaration of public symbols
// Module: None
// Entry point: extern "C" void fake_dll_libNone()
//
// This file is generated automatically, any change will be lost.
// Generated 2008-01-16 21:55:22.425337
//************************************************************************
#include "cconfig.h"

#undef DSYM
#undef DNAM
#undef DNAM_UTL

#define DNAM_UTL(f) #f
#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define DSYM(fmaj, fmin) fmaj
#  define DNAM(fmaj, fmin) DNAM_UTL( fmaj )
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define DSYM(fmaj, fmin) fmin ## _
#  define DNAM(fmaj, fmin) DNAM_UTL( fmin ## _ )
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define DSYM(fmaj, fmin) fmin ## __
#  define DNAM(fmaj, fmin) DNAM_UTL( fmin ## __ )
#else
#  error Invalid Fortran to C mangling convention
#endif

#ifdef __cplusplus
extern "C"
{
#endif


// ---  class None

// ---  class None

// ---  class None

// ---  class None

void fake_dll_lib_reg(void (*)(), const char*, const char*);

#ifdef FAKE_DLL
void fake_dll_libNone()
{
   static char libname[] = "libNone.so";

   // ---  class None

   // ---  class None

   // ---  class None

   // ---  class None
}
#endif    // FAKE_DLL

#ifdef __cplusplus
}
#endif

