C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C                     SVCRKTP
C************************************************************************

C************************************************************************
C Sommaire: SVCRKTP
C
C Description:
C     ASSEMBLAGE DE LA MATRICE TANGENTE CALCULEE PAR PERTURBATION
C     EQUATION : SAINT-VENANT CONSERVATIF
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
C      SUBROUTINE CD2D$BSE$ASMKT(VCORG,
      SUBROUTINE LM$ELIB$KTPRT (VCORG,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VDSLC,
     &                          VDSLR,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F$ASM)

      IMPLICIT REAL*8 (A-H, O-Z)
      INCLUDE 'lmcmmn.fc'

C      INTEGER  KLOCE (JJ$NDLEV)
C      REAL*8   VKE   (JJ$NDLE, JJ$NDLE)
      REAL*8   VCORG (JJ$NDIM, JJ$NNT)
      INTEGER  KNGV  (JJ$NCELV,JJ$NELV)
      INTEGER  KNGS  (JJ$NCELS,JJ$NELS)
      REAL*8   VDJV  (JJ$NDJV, JJ$NELV)
      REAL*8   VDJS  (JJ$NDJS, JJ$NELS)
      REAL*8   VPRGL (JJ$NPRGL)
      REAL*8   VPRNO (JJ$NPRNO,JJ$NNT)
      REAL*8   VPREV (JJ$NPREV,JJ$NELV)
      REAL*8   VPRES (JJ$NPRES,JJ$NELS)
      REAL*8   VDSLR (JJ$NDLN, JJ$NNT)
      INTEGER  KDIMP (JJ$NDLN, JJ$NNT)
      REAL*8   VDIMP (JJ$NDLN, JJ$NNT)
      INTEGER  KEIMP (JJ$NELS)
      REAL*8   VDLG  (JJ$NDLN, JJ$NNT)
      INTEGER  HMTX
      INTEGER  F$ASM
      EXTERNAL F$ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER NDLE
      PARAMETER (NDLE = 18)

      INTEGER  KLOCE(NDLE)
      REAL*8   VKE  (NDLE, NDLE)
      REAL*8   VDLE (NDLE), VDLEP(NDLE)
      REAL*8   VRESE(NDLE), VRESEP(NDLE)

      INTEGER  IERR
      INTEGER  IC, IE, IPERT
      INTEGER  IN, ID, II
      INTEGER  NO
      REAL*8   DELPRT
      REAL*8   XPERT
      REAL*8   XPERTI
      REAL*8   XPERTI0
C-----------------------------------------------------------------------

C---     COEFFICIENT DE PERTURBATION
      IF (DELPRT .EQ. ZERO) DELPRT = PETIT
      XPERT   = DABS(DELPRT)
      XPERTI0 = UN/XPERT

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,JJ$NELCOL
CDIR$ IVDEP
      DO 20 IE=JJ$KELCOL(1,IC),JJ$KELCOL(2,IC)

C---        DDL de l'element
         II=0
         DO IN=1,JJ$NNELV
            NO = KNGV(IN,IE)
            DO ID=1,JJ$NDLN
               II = II+1
               VDLE(II) = VDLG(ID, NO)
            ENDDO
         ENDDO

C---        ASSEMBLAGE DU RESIDU DE REFERENCE DANS VRESE
         CALL LM_ELIB_ASMKUE(IE, VDLE, VRESE)

C---        BOUCLE SUR LES PERTURBATIONS DES DDL
         DO IPERT=1,NDLE

C---           PERTURBATION DES D.D.L
            CALL DCOPY(NDLE, VDLE, 1, VDLEP, 1)       ! VDLEP=VDLE
            VDDL = VDLE(IPERT)
            IF (ABS(VDDL) .LE. PETIT) THEN
               XPERTI = XPERTI0
               VDLEP(IPERT) = VDDL + XPERT
            ELSE
               XPERTI = XPERTI0 / VDDL
               VDLEP(IPERT) = VDDL * (UN + XPERT)
            ENDIF

C---           ASSEMBLAGE DU RESIDU DE PERTURBATION DANS VRESEP
            CALL LM_ELIB_ASMKUE(IE)

C---           CONSTRUCTION DE LA MATRICE TANGENTE
            CALL DAXPY(NDLE, -1.0D0, VRESE, 1, VRESEP, 1)
            CALL DAXPY(NDLE, XPERTI, VRESEP, 1, VKE(1,IPERT), 1)
C            II = 0
C            DO IL=1,NNEL
C               DO ID=1,NDLN
C                  II = II+1
C                  VKE(II,IPERT) = (VRESEP(ID,IL)-VRESE(ID,IL))*XPERTI
C               ENDDO
C            ENDDO
         ENDDO

CC---        ROTATION DE LA MATRICE TANGENTE DANS LE REPERE N-T
C         CALL SVCRKXN (IE, KNG, VPRNO, VKE)
C---        TERME PETIT SUR LA DIAGONALE SI ZERO
         DETJ = VDJV(5,IE)    !! LE DETJ N'EST PAS TOUJOURS EN 5
         DO ID=1,NDLE
            IF (VKE(ID,ID) .EQ. ZERO) VKE(ID,ID) = DETJ * PETIT
         ENDDO

C---       ASSEMBLAGE DE LA MATRICE
         IERR = F$ASM(HMTX, JJ$NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(A,I9)')'ERR_CALCUL_MATRICE_KT_ELEM: ',IE
D           CALL LOG_WRITE(LOG_BUF)
D        ENDIF

20    CONTINUE
10    CONTINUE

      RETURN
      END

