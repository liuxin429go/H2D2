C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes : Fichier contenant les subroutines de base pour le calcul de
C         transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     Assemblage de la matrice [K] elementaire pour les elements de volume
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D$BSE$ASMKEV(IE,
     &                           KLOCE,
     &                           VKE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VDSLC,
     &                           VDSLR,
     &                           KDIMP,
     &                           VDIMP,
     &                           KEIMP,
     &                           VDLG)

      IMPLICIT REAL*8 (A-H, O-Z)
      INCLUDE 'lmcmmn.fc'

      INTEGER  KLOCE (JJ$NDLEV)
      REAL*8   VKE   (JJ$NDLE, JJ$NDLE)
      REAL*8   VCORG (JJ$NDIM, JJ$NNT)
      INTEGER  KLOCN (LM_CMMN_NDLN, LM_CMMN_NNT)
      INTEGER  KNGV  (JJ$NCELV,JJ$NELV)
      INTEGER  KNGS  (JJ$NCELS,JJ$NELS)
      REAL*8   VDJV  (JJ$NDJV, JJ$NELV)
      REAL*8   VDJS  (JJ$NDJS, JJ$NELS)
      REAL*8   VPRGL (JJ$NPRGL)
      REAL*8   VPRNO (JJ$NPRNO,JJ$NNT)
      REAL*8   VPREV (JJ$NPREV,JJ$NELV)
      REAL*8   VPRES (JJ$NPRES,JJ$NELS)
      REAL*8   VDSLR (JJ$NDLN, JJ$NNT)
      INTEGER  KDIMP (JJ$NDLN, JJ$NNT)
      REAL*8   VDIMP (JJ$NDLN, JJ$NNT)
      INTEGER  KEIMP (JJ$NELS)
      REAL*8   VDLG  (JJ$NDLN, JJ$NNT)
      INTEGER  HMTX
      INTEGER  F$ASM
      EXTERNAL F$ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      PARAMETER (QUATRE = 4.0D0)
      DIMENSION KONE(3)
C-----------------------------------------------------------------------

      IERR0=0

C---     INITIALISE VKE
      CALL SINIT(JJ$NDLE*JJ$NDLE,ZERO,VKE,1)

C---     INITIALISE KLOCE
      CALL IINIT(JJ$NDLE,0,KLOCE,1)

C---     CONNECTIVITES DU T3
      NO1  = KNGV(1,IE)
      NO2  = KNGV(2,IE)
      NO3  = KNGV(3,IE)

C---     CONSTRUIRE KONE
      KONE(1)=NO1
      KONE(2)=NO2
      KONE(3)=NO3

C---     TABLE KLOCE DE LOCALISATION DES DDLS
      JD=0
      DO I=1,JJ$NNELV
         IN = (KONE(I)-1)*JJ$NDLN
         DO ID=1,JJ$NDLN
            JD = JD+1
            KLOCE(JD)=IN+ID
         ENDDO
      ENDDO

C---     MÉTRIQUES DU T3
      VKX    = VDJV(1,IE)
      VEX    = VDJV(2,IE)
      VKY    = VDJV(3,IE)
      VEY    = VDJV(4,IE)
      DETJT3 = VDJV(5,IE)
      VSX    = -(VKX+VEX)
      VSY    = -(VKY+VEY)
      DELX   = VPREV(1,IE)
      DELY   = VPREV(2,IE)

C---     CONSTANTES DÉPENDANT DU VOLUME
      DISP  = UN_24/DETJT3                             ! Dispersion
      CONV  = UN_24                                    ! Convection
      SOLR  = UN_120*DETJT3                            ! Sol. réparties
      STAB  = UN_12                                    ! Stabilisation

C---     INITIALISATION DES PROPRIÉTÉS ÉLÉMENTAIRES
      AKXX = VPREV(4,IE)
      AKXY = VPREV(5,IE)
      AKYY = VPREV(6,IE)

C---     INITIALISATION DES PROPRIÉTÉS NODALES
      H1  = VPRNO( 3,NO1)
      HU1 = VPRNO( 9,NO1)
      HV1 = VPRNO(10,NO1)
      H2  = VPRNO( 3,NO2)
      HU2 = VPRNO( 9,NO2)
      HV2 = VPRNO(10,NO2)
      H3  = VPRNO( 3,NO3)
      HU3 = VPRNO( 9,NO3)
      HV3 = VPRNO(10,NO3)
      SHU = HU1 + HU2 + HU3
      SHV = HV1 + HV2 + HV3
      ASHU = ABS(HU1) + ABS(HU2) + ABS(HU3)
      ASHV = ABS(HV1) + ABS(HV2) + ABS(HV3)

C---       DISPERSION
      DFX1 = DISP*(AKXX*VSX + AKXY*VSY)
      DFX2 = DISP*(AKXX*VKX + AKXY*VKY)
      DFX3 = DISP*(AKXX*VEX + AKXY*VEY)
      DFY1 = DISP*(AKXY*VSX + AKYY*VSY)
      DFY2 = DISP*(AKXY*VKX + AKYY*VKY)
      DFY3 = DISP*(AKXY*VEX + AKYY*VEY)

C---       CONVECTION
      CUX1 = CONV*(SHU+HU1)
      CUX2 = CONV*(SHU+HU2)
      CUX3 = CONV*(SHU+HU3)
      CUY1 = CONV*(SHV+HV1)
      CUY2 = CONV*(SHV+HV2)
      CUY3 = CONV*(SHV+HV3)

C---          STABILISATION DE LA CONVECTION
      STBX  = STAB * DELX * ASHU
      STBY  = STAB * DELY * ASHV
      STBX1 = STBX * VSX
      STBX2 = STBX * VKX
      STBX3 = STBX * VEX
      STBY1 = STBY * VSY
      STBY2 = STBY * VKY
      STBY3 = STBY * VEY

C---       TERMES COMMUNS À TOUS LES DDL
      VKE11 = (VSX*DFX1 + VSY*DFY1) + (VSX*CUX1 + VSY*CUY1) +
     .        (VSX*STBX1 + VSY*STBY1)
      VKE21 = (VKX*DFX1 + VKY*DFY1) + (VSX*CUX2 + VSY*CUY2) +
     .        (VKX*STBX1 + VKY*STBY1)
      VKE31 = (VEX*DFX1 + VEY*DFY1) + (VSX*CUX3 + VSY*CUY3) +
     .        (VEX*STBX1 + VEY*STBY1)
      VKE12 = (VSX*DFX2 + VSY*DFY2) + (VKX*CUX1 + VKY*CUY1) +
     .        (VSX*STBX2 + VSY*STBY2)
      VKE22 = (VKX*DFX2 + VKY*DFY2) + (VKX*CUX2 + VKY*CUY2) +
     .        (VKX*STBX2 + VKY*STBY2)
      VKE32 = (VEX*DFX2 + VEY*DFY2) + (VKX*CUX3 + VKY*CUY3) +
     .        (VEX*STBX2 + VEY*STBY2)
      VKE13 = (VSX*DFX3 + VSY*DFY3) + (VEX*CUX1 + VEY*CUY1) +
     .        (VSX*STBX3 + VSY*STBY3)
      VKE23 = (VKX*DFX3 + VKY*DFY3) + (VEX*CUX2 + VEY*CUY2) +
     .        (VKX*STBX3 + VKY*STBY3)
      VKE33 = (VEX*DFX3 + VEY*DFY3) + (VEX*CUX3 + VEY*CUY3) +
     .        (VEX*STBX3 + VEY*STBY3)

C------    CALCUL DE LA MATRICE DE TRANSPORT-DIFFUSION
      DO ID=1,JJ$NDLN
         ID1 = ID
         ID2 = ID1+JJ$NDLN
         ID3 = ID2+JJ$NDLN

C---       SOLLICITATIONS RÉPARTIES (QH ENTRÉE)
         QH1   = SOLR * VDSLR(ID,NO1)*H1
         QH2   = SOLR * VDSLR(ID,NO2)*H2
         QH3   = SOLR * VDSLR(ID,NO3)*H3
         SQH   = DEUX*(QH1 + QH2 + QH3)
         SOL11 = (SQH + QUATRE*QH1)
         SOL12 = (SQH - QH3)
         SOL13 = (SQH - QH2)
         SOL22 = (SQH + QUATRE*QH2)
         SOL23 = (SQH - QH1)
         SOL33 = (SQH + QUATRE*QH3)

C---       ASSEMBLAGE DES CONTRIBUTONS
         VKE(ID1,ID1) = VKE11 + SOL11
         VKE(ID2,ID1) = VKE21 + SOL12
         VKE(ID3,ID1) = VKE31 + SOL13
         VKE(ID1,ID2) = VKE12 + SOL12
         VKE(ID2,ID2) = VKE22 + SOL22
         VKE(ID3,ID2) = VKE32 + SOL23
         VKE(ID1,ID3) = VKE13 + SOL13
         VKE(ID2,ID3) = VKE23 + SOL23
         VKE(ID3,ID3) = VKE33 + SOL33
      ENDDO

C---    TERME PETIT SUR LA DIAGONALE SI ZERO
      DO I=1,JJ$NDLE
         IF (VKE(I,I) .EQ. ZERO) VKE(I,I) = DETJT3*PETIT
      ENDDO

      RETURN
      END

