# H2D2

[H2D2](http://www.gre-ehn.ete.inrs.ca/H2D2) is a finite element software, modular and extensible. 
In its present form, it enables the shallow water equations solving, 
also known as St. Venant equations, advection-diffusion equations 
with different kinetics (colibacteria, heavy metals, suspended matter, etc…).
Relying on well-established third party libraries, 
[H2D2](http://www.gre-ehn.ete.inrs.ca/H2D2) can run on 
distributed memory cluster and on shared memory computing nodes. 
It is mostly used in a batch processing mode with a command file, 
but can also be used in an interactive mode for lighter tasks.

Developed at [INRS-ETE](http://www.ete.inrs.ca/ete), 
a research center part of the Université du Québec network, 
[H2D2](http://www.gre-ehn.ete.inrs.ca/H2D2) is in the continuity
of Hydrosim and Dispersim (Hydrosim2-Dispersim2). 
It is based on the last Hydrosim and Dispersim versions as well 
as on various related developments done during previous mandates.

