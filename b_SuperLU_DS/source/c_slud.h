//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Pont avec la DLL SuperLU
//************************************************************************
#ifndef C_SLUD_H_DEJA_INCLU
#define C_SLUD_H_DEJA_INCLU

#ifndef MODULE_SUPERLU_DIST
#  define MODULE_SUPERLU_DIST 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_SLUD_INITIALIZE  C_SLUD_INITIALIZE
#  define C_SLUD_FINALIZE    C_SLUD_FINALIZE
#  define C_SLUD_MAT_CREATE  C_SLUD_MAT_CREATE
#  define C_SLUD_MAT_DESTROY C_SLUD_MAT_DESTROY
#  define C_SLUD_MAT_ANALYZE C_SLUD_MAT_ANALYZE
#  define C_SLUD_MAT_FACT    C_SLUD_MAT_FACT
#  define C_SLUD_MAT_SOLVE   C_SLUD_MAT_SOLVE
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_SLUD_INITIALIZE  c_slud_initialize_
#  define C_SLUD_FINALIZE    c_slud_finalize_
#  define C_SLUD_MAT_CREATE  c_slud_mat_create_
#  define C_SLUD_MAT_DESTROY c_slud_mat_destroy_
#  define C_SLUD_MAT_ANALYZE c_slud_mat_analyze_
#  define C_SLUD_MAT_FACT    c_slud_mat_fact_
#  define C_SLUD_MAT_SOLVE   c_slud_mat_solve_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_SLUD_INITIALIZE  c_slud_initialize__
#  define C_SLUD_FINALIZE    c_slud_finalize__
#  define C_SLUD_MAT_CREATE  c_slud_mat_create__
#  define C_SLUD_MAT_DESTROY c_slud_mat_destroy__
#  define C_SLUD_MAT_ANALYZE c_slud_mat_analyze__
#  define C_SLUD_MAT_FACT    c_slud_mat_fact__
#  define C_SLUD_MAT_SOLVE   c_slud_mat_solve__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_SUPERLU_DIST)

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_INITIALIZE  ();
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_FINALIZE    ();
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_MAT_CREATE  (fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_MAT_DESTROY (fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_MAT_ANALYZE (fint_t*, fint_t*, fint_t*, double*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_MAT_FACT    (fint_t*, fint_t*, fint_t*, double*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_MAT_SOLVE   (fint_t*, fint_t*, fint_t*, double*);


#ifdef __cplusplus
}
#endif

#endif   // C_SLUD_H_DEJA_INCLU
