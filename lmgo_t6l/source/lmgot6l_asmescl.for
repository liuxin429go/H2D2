C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_GOT6L_ASMESCL
C   Private:
C     INTEGER LM_GOT6L_ASMESCL_0
C     INTEGER LM_GOT6L_ASMESCL_1
C     INTEGER LM_GOT6L_ASMESCL_2
C     INTEGER LM_GOT6L_ASMESCL_3
C
C************************************************************************

      SUBMODULE(LM_GOT6L_M) LM_GOT6L_ASMESCL_M
      
      IMPLICIT NONE

      CONTAINS
      
C************************************************************************
C Sommaire:  LM_GOT6L_ASMESCL_0
C
C Description:
C     La fonction privée LM_GOT6L_ASMESCL_0 extrait les éléments de la
C     limite ICL en se basant sur les noeuds de cette limite.
C
C Entrée:
C
C Sortie:
C     KCLELE      Table des éléments des limites
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOT6L_ASMESCL_0(ICL, GDTA)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      INTEGER, INTENT(IN) :: ICL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA

      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IEP, IE, IN
      INTEGER INOCLINF, INOCLSUP
      INTEGER IELCLINF
      INTEGER IELE
      INTEGER NHIT
      INTEGER NP1, NP3
C-----------------------------------------------------------------------

      IF (ICL .EQ. 1) THEN
         IELE = 0
      ELSE
         IELE = GDTA%KCLLIM(6,ICL-1)
      ENDIF

      GDTA%KCLLIM(5,ICL) = IELE + 1

      INOCLINF = GDTA%KCLLIM(3,ICL)
      INOCLSUP = GDTA%KCLLIM(4,ICL)
      IELCLINF = GDTA%KCLLIM(5,ICL)

C---     Boucle sur les éléments de surface
      DO IEP=1,GDTA%NELLS
         NP1 = GDTA%KNGS(1,IEP)
         NP3 = GDTA%KNGS(3,IEP)

C---        Cherche les noeuds
         NHIT = 0
         DO IN=INOCLINF,INOCLSUP
            IF (GDTA%KCLNOD(IN) .EQ. NP1) NHIT=NHIT + 1
            IF (GDTA%KCLNOD(IN) .EQ. NP3) NHIT=NHIT + 1
            IF (NHIT .EQ. 2) GOTO 189
         ENDDO
189      CONTINUE
         IF (NHIT .NE. 2) GOTO 199

C---        Contrôle les dédoublements
         DO IE=IELCLINF, IELE
            IF (GDTA%KCLELE(IE) .EQ. IEP) GOTO 9900
         ENDDO

C---        Ajoute l'élément de limite
         IELE = IELE + 1
         GDTA%KCLELE(IELE) = IEP

199      CONTINUE
      ENDDO

      GDTA%KCLLIM(6,ICL) = IELE
      GDTA%KCLLIM(7,ICL) = EG_TPLMT_INDEFINI

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_MSG, '(A)') 'ERR_ELEMENT_LIMITE_DOUBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LM_GOT6L_ASMESCL_0 = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT6L_ASMESCL_0

C************************************************************************
C Sommaire:  LM_GOT6L_ASMESCL_1
C
C Description:
C     La fonction privée LM_GOT6L_ASMESCL_1 détermine le type de limite.
C     S'il n'y a qu'un seul segment, elle filtre les noeuds de bout.
C     Comme les éléments de peau sont numérotés anti-horaire, le noeud
C     de début est le noeud 1 de l'élément retourné.
C
C Entrée:
C
C Sortie:
C     IEL      ! Indice dans KCLELE de l'élément de début
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOT6L_ASMESCL_1(IEL, ICL, GDTA)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      INTEGER, INTENT(IN) :: ICL
      INTEGER, INTENT(OUT) :: IEL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA

      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IN, IE, IEP, IEH
      INTEGER INOCLINF, INOCLSUP, NNOLIMT
      INTEGER IELCLINF, IELCLSUP, NELLIMT
      INTEGER ICOD
      INTEGER NOCL, NHITS, NHITM
      INTEGER NP1, NP2, NP3
      INTEGER NSGL, NSIN, NSMT, NMED, KCLSMT(2,2)
C-----------------------------------------------------------------------

      IEL = -1

C---     Indices
      INOCLINF = GDTA%KCLLIM(3,ICL)
      INOCLSUP = GDTA%KCLLIM(4,ICL)
      IELCLINF = GDTA%KCLLIM(5,ICL)
      IELCLSUP = GDTA%KCLLIM(6,ICL)
      NNOLIMT = INOCLSUP - INOCLINF + 1
      NELLIMT = IELCLSUP - IELCLINF + 1

C---     Boucle sur les noeuds de la limite
      NSGL = 0    ! Nombre de noeuds single
      NSMT = 0    ! Nombre de sommets de segments
      NSIN = 0    ! Nombre de sommets internes
      NMED = 0    ! Nombre de sommets milieux
      DO IN=INOCLINF,INOCLSUP
         NOCL = GDTA%KCLNOD(IN)

C---        Compte le nb d'apparitions
         NHITS = 0
         NHITM = 0
         DO IE=IELCLINF,IELCLSUP
            IEP = GDTA%KCLELE(IE)

            NP1 = GDTA%KNGS(1,IEP)
            NP2 = GDTA%KNGS(2,IEP)
            NP3 = GDTA%KNGS(3,IEP)

            IF (NHITS .EQ. 0) IEH = IE
            IF (NOCL .EQ. NP1) NHITS = NHITS + 1
            IF (NOCL .EQ. NP2) NHITM = NHITM + 1
            IF (NOCL .EQ. NP3) NHITS = NHITS + 1
         ENDDO

C---        Comptabilise et accumule
         IF (NHITS .EQ. 0) THEN
            IF (NHITM .EQ. 0) THEN
               NSGL = NSGL + 1
            ELSE
               NMED = NMED + 1
            ENDIF
         ELSEIF (NHITS .EQ. 1) THEN
            NSMT = NSMT + 1
            IF (NSMT .LE. 2) THEN
               KCLSMT(1, NSMT) = IN
               KCLSMT(2, NSMT) = IEH
            ENDIF
         ELSEIF (NHITS .EQ. 2) THEN
            NSIN = NSIN + 1
         ELSE
            GOTO 9900
         ENDIF
      ENDDO

C---     Diagnostic
      IF (MOD(NSMT,2) .NE. 0) GOTO 9901
      IF (NMED .NE. 0 .AND. NMED .NE. NELLIMT) GOTO 9901
      ICOD = EG_TPLMT_CMPLX
      IF (NSGL .EQ. NNOLIMT) ICOD = EG_TPLMT_NOEUDS
      IF (NSGL .EQ. 0 .AND. NSMT .EQ. 2) ICOD = EG_TPLMT_1SGMT
      IF (NSGL .EQ. 0 .AND. NSMT .GT. 2) ICOD = EG_TPLMT_nSGMT
      GDTA%KCLLIM(7, ICL) = ICOD

C---     Noeud de début
      IF (GDTA%KCLLIM(7, ICL) .NE. EG_TPLMT_1SGMT) GOTO 9999
      IN = KCLSMT(1, 1)
      IE = KCLSMT(2, 1)
      IEP = GDTA%KCLELE(IE)
      IF (GDTA%KCLNOD(IN) .NE. GDTA%KNGS(1,IEP)) THEN
         KCLSMT(1, 1) = KCLSMT(1, 2)
         KCLSMT(1, 2) = IN
         KCLSMT(2, 1) = KCLSMT(2, 2)
         KCLSMT(2, 2) = IE
      ENDIF
D     IN = KCLSMT(1, 1)
D     IE = KCLSMT(2, 1)
D     IEP = GDTA%KCLELE(IE)
D     CALL ERR_ASR(GDTA%KCLNOD(IN) .EQ. GDTA%KNGS(1,IEP)) ! transformer en erreur

      IEL = KCLSMT(2,1)
      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_MSG, '(A)') 'ERR_LIMITE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_MSG, '(A)') 'ERR_LIMITE_INCOHERENTE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_MSG, '(2A, I6)') 'MSG_NOEUDS_LIMITE', ': ', NNOLIMT
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_MSG, '(2A, I6)') 'MSG_ELEMENTS_LIMITE', ': ', NELLIMT
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_MSG, '(2A, I6)') 'MSG_NOEUDS_MILIEUX', ': ', NMED
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_MSG, '(2A, I6)') 'MSG_NOEUDS_UNIQUES', ': ', NSGL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_MSG, '(2A, I6)') 'MSG_NOEUDS_SOMMETS', ': ', NSMT
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_MSG, '(2A, I6)') 'MSG_NOEUDS_INTERNES', ': ', NSIN
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LM_GOT6L_ASMESCL_1 = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT6L_ASMESCL_1

C************************************************************************
C Sommaire:  LM_GOT6L_ASMESCL_2
C
C Description:
C     La fonction privée LM_GOT6L_ASMESCL_2 réordonne les éléments et
C     les noeuds de la limite ICL
C
C Entrée:
C     IEL      ! Indice dans KCLELE de l'élément de début
C
C Sortie:
C     KCLNOD         Table des noeuds triée
C     KCLELE         Table des noeuds triée
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOT6L_ASMESCL_2(IEL, ICL, GDTA)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      INTEGER, INTENT(IN) :: ICL
      INTEGER, INTENT(IN) :: IEL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IEP, IE1, IE2
      INTEGER IN
      INTEGER INOCLINF, INOCLSUP
      INTEGER IELCLINF, IELCLSUP
D     INTEGER NNOLIMT, NELLIMT
      INTEGER ISWP
      INTEGER NP1, NP3
      LOGICAL ANOMED
C-----------------------------------------------------------------------

      INOCLINF = GDTA%KCLLIM(3,ICL)
      INOCLSUP = GDTA%KCLLIM(4,ICL)
      IELCLINF = GDTA%KCLLIM(5,ICL)
      IELCLSUP = GDTA%KCLLIM(6,ICL)
D     NNOLIMT = INOCLSUP - INOCLINF + 1
D     NELLIMT = IELCLSUP - IELCLINF + 1
D     CALL ERR_ASR(NNOLIMT .EQ.   NELLIMT+1 .OR.
D    &             NNOLIMT .EQ. 2*NELLIMT+1)
      ANOMED = (IELCLSUP-IELCLINF+1) .NE. (INOCLSUP-INOCLINF+1-1)

C---     Réordonne les éléments
      ISWP= IELCLINF
      IEP = GDTA%KCLELE(IEL)
D     CALL ERR_ASR(ISWP .LE. IEL)
      GDTA%KCLELE(IEL)  = GDTA%KCLELE(ISWP)
      GDTA%KCLELE(ISWP) = IEP
      NP1 = GDTA%KNGS(1,IEP)
      NP3 = GDTA%KNGS(3,IEP)
      DO IE1=IELCLINF+1, IELCLSUP-1
         DO IE2=IE1, IELCLSUP
            IEP = GDTA%KCLELE(IE2)
            IF (GDTA%KNGS(1,IEP) .EQ. NP3) THEN
               ISWP = ISWP + 1
D              CALL ERR_ASR(ISWP .LE. IE2)
               GDTA%KCLELE(IE2)  = GDTA%KCLELE(ISWP)
               GDTA%KCLELE(ISWP) = IEP
               NP1 = NP3
               NP3 = GDTA%KNGS(3,IEP)
               GOTO 199
            ENDIF
         ENDDO
         GOTO 9900
199      CONTINUE
      ENDDO

C---     Réordonne les noeuds (non sécuritaire, on écrase)
      IN = INOCLINF
      DO IE1=IELCLINF, IELCLSUP
         IEP = GDTA%KCLELE(IE1)
         GDTA%KCLNOD(IN) = GDTA%KNGS(1,IEP)
         IN = IN + 1
         IF (ANOMED) THEN
            GDTA%KCLNOD(IN) = GDTA%KNGS(2,IEP)
            IN = IN + 1
         ENDIF
      ENDDO
D     CALL ERR_ASR(IN .LE. GDTA%NCLNOD)
      GDTA%KCLNOD(IN) = GDTA%KNGS(3,IEP)

C---        Réordonne les noeuds
!!          Algo non fonctionnel s'il y a des noeuds milieux
!!      ISWP= INOCLINF-1
!!      DO IE1=IELCLINF, IELCLSUP
!!         IEP = GDTA%KCLELE(IE1)
!!         NP1 = GDTA%KNGS(1,IEP)
!!         DO IN=ISWP+1, INOCLSUP
!!            IF (GDTA%KCLNOD(IN) .EQ. NP1) THEN
!!               ISWP = ISWP + 1
!!               GDTA%KCLNOD(IN) = GDTA%KCLNOD(ISWP)
!!               GDTA%KCLNOD(ISWP) = NP1
!!               GOTO 299
!!            ENDIF
!!         ENDDO
!!         GOTO 9901
!!299      CONTINUE
!!      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_MSG, '(A)') 'ERR_LIMITE_TROP_COMPLEXE'
      CALL ERR_ASG(ERR_WRN, ERR_BUF)
      WRITE(ERR_MSG, '(A)') 'MSG_CHAINAGE_INTROUVABLE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_MSG, '(2A, I6)') 'MSG_NOEUD', ': ', NP3
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_MSG, '(A)') 'ERR_LIMITE_TROP_COMPLEXE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_MSG, '(A)') 'MSG_CHAINAGE_INTROUVABLE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_MSG, '(2A, I6)') 'MSG_NOEUD', ': ', NP1
      CALL ERR_AJT(ERR_BUF)

      GOTO 9999

9999  CONTINUE
      LM_GOT6L_ASMESCL_2 = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT6L_ASMESCL_2

C************************************************************************
C Sommaire:  LM_GOT6L_ASMESCL_3
C
C Description:
C     La fonction privée LM_GOT6L_ASMESCL_2 calcule la distance relative
C     des noeuds de la limite ICL. Les noeuds doivent être ordonnés.
C
C Entrée:
C
C Sortie:
C     VCLDST         Table des distances
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOT6L_ASMESCL_3(ICL, GDTA)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      INTEGER, INTENT(IN) :: ICL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER INOCLINF, INOCLSUP
      INTEGER INL, NOP
      REAL*8  DS, DT
      REAL*8  X1, Y1, X2, Y2, DX, DY
C-----------------------------------------------------------------------

      INOCLINF = GDTA%KCLLIM(3,ICL)
      INOCLSUP = GDTA%KCLLIM(4,ICL)

C---     Calcule la distance cumulée
      NOP = GDTA%KCLNOD(INOCLINF)
      X1 = GDTA%VCORG(1, NOP)
      Y1 = GDTA%VCORG(2, NOP)
      DT = 0.0D0
      GDTA%VCLDST(INOCLINF) = DT
      DO INL=INOCLINF+1, INOCLSUP
         NOP = GDTA%KCLNOD(INL)
         X2 = GDTA%VCORG(1, NOP)
         Y2 = GDTA%VCORG(2, NOP)
         DX = X2 - X1
         DY = Y2 - Y1
         DS = HYPOT(DX, DY)
         DT = DT + DS
         GDTA%VCLDST(INL) = DT
         X1 = X2
         Y1 = Y2
      ENDDO
D     CALL ERR_ASR(DT .GT. 0.0D0)

C---     Calcule la distance cumulée relative
      DO INL=INOCLINF+1, INOCLSUP
         GDTA%VCLDST(INL) = GDTA%VCLDST(INL) / DT
      ENDDO

      LM_GOT6L_ASMESCL_3 = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT6L_ASMESCL_3

C************************************************************************
C Sommaire:  LM_GOT6L_ASMESCL
C
C Description:
C     La fonction LM_GOT6L_ASMESCL assemble les éléments de surface
C     des conditions limites. Pour un élément L3L, il suffit que les
C     deux noeuds sommets d'un élément se trouvent dans la liste des
C     noeuds de CL pour que l'élément soit reconnu.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_GOT6L_ASMESCL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT6L_ASMESCL
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      CLASS(LM_GOT6L_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER IERR
      INTEGER ICL, IEL
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     CALL ERR_PRE(GDTA%NNL   .GE. 6)
D     CALL ERR_PRE(GDTA%NCELS .GE. 5)
D     CALL ERR_PRE(GDTA%NDJV  .GE. 5)
D     CALL ERR_PRE(GDTA%NDJS  .GE. 3)

C---     Initialise les distances
      GDTA%VCLDST(1:GDTA%NCLNOD) = 0.0D0

C---     Boucle sur les limites
      DO ICL=1, GDTA%NCLLIM

C---        Détermine les éléments de la limite
         IF (ERR_GOOD())
     &      IERR = LM_GOT6L_ASMESCL_0(ICL, GDTA)

C---        Filtre l'élément de début
         IEL = -1
         IF (ERR_GOOD())
     &      IERR = LM_GOT6L_ASMESCL_1(IEL, ICL, GDTA)

C---        Réordonne les éléments et les noeuds
         IF (ERR_GOOD() .AND. GDTA%KCLLIM(7,ICL) .EQ. EG_TPLMT_1SGMT)
     &      IERR = LM_GOT6L_ASMESCL_2(IEL, ICL, GDTA)

C---        Calcule les distances relatives
         IF (ERR_GOOD() .AND. GDTA%KCLLIM(7,ICL) .EQ. EG_TPLMT_1SGMT)
     &      IERR = LM_GOT6L_ASMESCL_3(ICL, GDTA)

         IF (ERR_ESTMSG('ERR_LIMITE_COMPLEXE')) THEN
            CALL ERR_RESET()
         ENDIF
      ENDDO

      LM_GOT6L_ASMESCL = ERR_TYP()
      RETURN
      END

      END SUBMODULE LM_GOT6L_ASMESCL_M
