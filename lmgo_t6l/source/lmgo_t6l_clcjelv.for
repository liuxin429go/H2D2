C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T6L_CLCJELV
C
C Description:
C     La fonction LMGO_T6L_CLCJELV calcule les métriques pour des
C     éléments de volume de type T6L. Les métriques sont celles de
C     l'élément T6L et non celles d'un sous-élément T3.
C
C Entrée:
C     NDIM                 Nombre de DIMension de l'élément = 3
C     NNT                  Nombre de Noeud Total
C     NCELV                Nombre de Connectivité par ELéments de Volume
C     NELV                 Nombre d'ELéments de Volume
C     NDJV                 Nombre de métrique par élément de volume pouvant être calculées indép. de la solution  =5
C     VCORG(NDIM,  NNT)    Table des COoRdonnées Globales
C     KNGV (NCELV, NELV)   Table des coNectivités Globales de Volume
C
C Sortie:
C     VDJV(NDJV,  NELV)    Table des métriques de l'élément de volume:
C                          les métriques sont dXi/dx, dEta/dx, dXi/dy, dEta/dy, det(j)
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_CLCJELV(NDIM,
     &                          NNT,
     &                          NCELV,
     &                          NELV,
     &                          NDJV,
     &                          VCORG,
     &                          KNGV,
     &                          VDJV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_CLCJELV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER NDIM
      INTEGER NNT
      INTEGER NCELV
      INTEGER NELV
      INTEGER NDJV
      REAL*8  VCORG(NDIM,  NNT)
      INTEGER KNGV (NCELV, NELV)
      REAL*8  VDJV (NDJV,  NELV)

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      REAL*8  X1(2), X3(2), X5(2)
      INTEGER IC, IE, IN
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNT   .GE. 3)
D     CALL ERR_PRE(NCELV .EQ. 6)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(X1, X3, X5)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)

C---     Assigne les noeuds milieux en position
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         X1 = VCORG(:, NO1)
         X3 = VCORG(:, NO3)
         X5 = VCORG(:, NO5)
         VCORG(:, NO2) = UN_2*(X1 + X3)
         VCORG(:, NO4) = UN_2*(X3 + X5)
         VCORG(:, NO6) = UN_2*(X5 + X1)

      ENDDO
!$omp end do
      ENDDO

C---     Calcul des métriques
!$omp  do 
      DO IE=1,NELV
         NO1  = KNGV(1,IE)
         NO2  = KNGV(3,IE)
         NO3  = KNGV(5,IE)
         VDJV(1,IE) = VCORG(2,NO3) - VCORG(2,NO1)                   ! Ksi,x
         VDJV(2,IE) = VCORG(2,NO1) - VCORG(2,NO2)                   ! Eta,x
         VDJV(3,IE) = VCORG(1,NO1) - VCORG(1,NO3)                   ! Ksi,y
         VDJV(4,IE) = VCORG(1,NO2) - VCORG(1,NO1)                   ! Eta,y
         VDJV(5,IE) = VDJV(4,IE)*VDJV(1,IE) - VDJV(3,IE)*VDJV(2,IE) ! Det J
      ENDDO
!$omp end do

!$omp end parallel
      
C---        Imprime les éléments à déterminant négatif
      DO IE=1,NELV
         IF (VDJV(5,IE) .LE. ZERO) THEN
            WRITE(ERR_BUF, '(A)') 'ERR_DETJ_NEGATIF'
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
            WRITE(LOG_BUF, '(A,I9,A,16I9)')
     &         'ERR_DETJ_NEGATIF:', IE, '--', (KNGV(IN,IE),IN=1,NCELV)
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
      ENDDO

      LMGO_T6L_CLCJELV = ERR_TYP()
      RETURN
      END

