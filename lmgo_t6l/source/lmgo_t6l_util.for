C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C**************************************************************
C Sommaire: LMGO_T6L_CLCGRAD
C
C Description:
C     Calcul du gradient
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_CLCGRAD(KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VNO,
     &                          VDX,
     &                          VDY,
     &                          VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_CLCGRAD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_CLCGRAD

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE, IN
      REAL*8  COEF

      INTEGER LMGO_T6L_KUEGRDV
      INTEGER LMGO_T6L_KUEML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(IERR)

C---     Assemble le gradient et Ml
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LMGO_T6L_KUEGRDV(KNGV(1,IE),
     &                           VDJV(1,IE),
     &                           VNO,
     &                           VDX,
     &                           VDY)
         IERR = LMGO_T6L_KUEML  (KNGV(1,IE),
     &                           VDJV(1,IE),
     &                           VML)

      ENDDO
!$omp  end do
      ENDDO

C---     Résous pour F,x et F,y
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VML(IN) = UN / VML(IN)
      ENDDO
!$omp  end do
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VDX(IN) = VDX(IN) * VML(IN)
      ENDDO
!$omp  end do
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VDY(IN) = VDY(IN) * VML(IN)
      ENDDO
!$omp  end do

!$omp  end parallel
      
      LMGO_T6L_CLCGRAD = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_CLCDDX
C
C Description:
C     Calcul du gradient
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_CLCDDX(KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VNO,
     &                         VDX,
     &                         VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_CLCDDX
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_CLCDDX

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE, IN

      INTEGER LMGO_T6L_KUEDDX
      INTEGER LMGO_T6L_KUEML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(IERR)

C---     Assemble F,x et Ml
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LMGO_T6L_KUEDDX(KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VNO,
     &                          VDX)
         IERR = LMGO_T6L_KUEML (KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VML)

      ENDDO
!$omp  end do
      ENDDO

C---     Résous pour F,x
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VDX(IN) = VDX(IN) / VML(IN)
      ENDDO
!$omp  end do

!$omp  end parallel

      LMGO_T6L_CLCDDX = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_CLCDDY
C
C Description:
C     Calcul du gradient
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_CLCDDY(KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VNO,
     &                         VDY,
     &                         VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_CLCDDY
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_CLCDDY

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE, IN

      INTEGER LMGO_T6L_KUEDDY
      INTEGER LMGO_T6L_KUEML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(IERR)

C---     Assemble F,y et Ml
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LMGO_T6L_KUEDDY(KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VNO,
     &                          VDY)
         IERR = LMGO_T6L_KUEML (KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VML)

      ENDDO
!$omp  end do
      ENDDO

C---     Résous pour F,y
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VDY(IN) = VDY(IN) / VML(IN)
      ENDDO
!$omp  end do

!$omp  end parallel

      LMGO_T6L_CLCDDY = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_CLCHESS
C
C Description:
C     Calcul du Hessien par le formule de Green.
C
C Entrée:
C
C Sortie:
C
C Notes:
C  Les termes de contour:
C     {N} . d/dx(du/dx) = - {N,x}<N,x>{u} + {N}<N,x>{u}.nx
C     {N} . d/dx(du/dy) = - {N,x}<N,y>{u} + {N}<N,y>{u}.nx
C     {N} . d/dy(du/dy) = - {N,y}<N,x>{u} + {N}<N,x>{u}.ny
C     {N} . d/dy(du/dy) = - {N,y}<N,y>{u} + {N}<N,y>{u}.ny
C
C****************************************************************
      FUNCTION LMGO_T6L_CLCHESS(KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VNO,
     &                          VHS,
     &                          VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_CLCHESS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_CLCHESS

      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO   (EG_CMMN_NNL)
      REAL*8   VHS   (3, EG_CMMN_NNL)
      REAL*8   VML   (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE, IN
      INTEGER IES, IEV
      REAL*8  COEF

      INTEGER IDAMAX
      INTEGER LMGO_T6L_KUEHSSV
      INTEGER LMGO_T6L_KUEHSSS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(IES, IEV)
!$omp& private(IERR)
!$omp& private(COEF)

C---     Assemble les termes de volume
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LMGO_T6L_KUEHSSV(KNGV(1,IE),
     &                           VDJV(1,IE),
     &                           VNO,
     &                           VHS,
     &                           VML)

      ENDDO
!$omp  end do
      ENDDO

C---     Assemble les termes de contour
      DO IES=1,EG_CMMN_NELS

         IEV = KNGS(4,IES)
         IERR = LMGO_T6L_KUEHSSS(KNGV(1,IEV),
     &                           KNGS(1,IES),
     &                           VDJV(1,IEV),
     &                           VDJS(1,IES),
     &                           VNO,
     &                           VHS,
     &                           VML)

      ENDDO

C---     Finalise
!$omp  do
      DO IN=1,EG_CMMN_NNL
         COEF = UN / VML(IN)
         VHS(1,IN) = VHS(1,IN) * COEF
         VHS(2,IN) = VHS(2,IN) * COEF
         VHS(3,IN) = VHS(3,IN) * COEF
      ENDDO
!$omp  end do

!$omp  end parallel

      LMGO_T6L_CLCHESS = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_ASMDDX
C
C Description:
C     Assemble la dérivée en y
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_ASMDDX(KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VNO,
     &                         VDX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_ASMDDX
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ASMDDX

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE

      INTEGER LMGO_T6L_KUEDDX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(IERR)

C---     Assemble F,x
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LMGO_T6L_KUEDDX(KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VNO,
     &                          VDX)

      ENDDO
!$omp  end do
      ENDDO

!$omp  end parallel

      LMGO_T6L_ASMDDX = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_ASMDDY
C
C Description:
C     Assemble la dérivée en y
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_ASMDDY(KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VNO,
     &                         VDY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_ASMDDY
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ASMDDY

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE

      INTEGER LMGO_T6L_KUEDDY
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(IERR)

C---     Assemble F,y
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LMGO_T6L_KUEDDY(KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VNO,
     &                          VDY)

      ENDDO
!$omp  end do
      ENDDO

!$omp  end parallel

      LMGO_T6L_ASMDDY = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_ASMML
C
C Description:
C     Assemble la matrice masse lumpée
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_ASMML(KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_ASMML
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ASMML

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE

      INTEGER LMGO_T6L_KUEML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(IERR)

C---     Assemble Ml
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LMGO_T6L_KUEML  (KNGV(1,IE),
     &                           VDJV(1,IE),
     &                           VML)

      ENDDO
!$omp  end do
      ENDDO

!$omp  end parallel

      LMGO_T6L_ASMML = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_KUEHSSV
C
C Description:
C     Contribution élémentaire de volume du Hessien.
C
C Entrée:
C
C Sortie:
C
C Notes:
C  Les termes de contour:
C     {N} . d/dx(du/dx) = - {N,x}<N,x>{u} + {N}<N,x>{u}.nx
C     {N} . d/dx(du/dy) = - {N,x}<N,y>{u} + {N}<N,y>{u}.nx
C     {N} . d/dy(du/dy) = - {N,y}<N,x>{u} + {N}<N,x>{u}.ny
C     {N} . d/dy(du/dy) = - {N,y}<N,y>{u} + {N}<N,y>{u}.ny
C
C     Sur le T3 interne, les métriques sont de signe inverse. Comme ici
C     on a toujours le produit de deux métriques (N,x * N,x), ces inversions
C     de signe s'annulent.
C****************************************************************
      FUNCTION LMGO_T6L_KUEHSSV(KNGE,
     &                          VDJE,
     &                          VNO,
     &                          VHS,
     &                          VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_KUEHSSV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_KUEHSSV

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VHS (3, EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER NO1, NO2, NO3
      INTEGER IEL
      INTEGER KONE(3,4)

      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  DETJ, C_HS, C_ML
      REAL*8  F1, F2, F3, FX, FY
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Métriques des sous-éléments
      VKX = UN_2 * VDJE(1)
      VEX = UN_2 * VDJE(2)
      VSX = -(VKX+VEX)
      VKY = UN_2 * VDJE(3)
      VEY = UN_2 * VDJE(4)
      VSY = -(VKY+VEY)
      DETJ = UN_4*VDJE(5)

C---     Coefficients
      C_HS = UN_2 / DETJ
      C_ML = UN_6 * DETJ

C---     Table KONE des connectivités des T3
      KONE(1,1) = KNGE(1)
      KONE(2,1) = KNGE(2)
      KONE(3,1) = KNGE(6)

      KONE(1,2) = KNGE(2)
      KONE(2,2) = KNGE(3)
      KONE(3,2) = KNGE(4)

      KONE(1,3) = KNGE(6)
      KONE(2,3) = KNGE(4)
      KONE(3,3) = KNGE(5)

      KONE(1,4) = KNGE(4)
      KONE(2,4) = KNGE(6)
      KONE(3,4) = KNGE(2)

C---     Boucle sur les sous éléments T3
      DO IEL = 1,4
         NO1 = KONE(1,IEL)
         NO2 = KONE(2,IEL)
         NO3 = KONE(3,IEL)

C---        Degrés de liberté
         F1 = VNO(NO1)
         F2 = VNO(NO2)
         F3 = VNO(NO3)

C---        Assemble le hessien
         FX = C_HS * (VKX*(F2-F1) + VEX*(F3-F1))   ! F,x
         FY = C_HS * (VKY*(F2-F1) + VEY*(F3-F1))   ! F,y
         VHS(1,NO1) = VHS(1,NO1) - VSX*FX      ! F,xx
         VHS(2,NO1) = VHS(2,NO1) - VSX*FY      ! F,xy
         VHS(3,NO1) = VHS(3,NO1) - VSY*FY      ! F,yy
         VHS(1,NO2) = VHS(1,NO2) - VKX*FX
         VHS(2,NO2) = VHS(2,NO2) - VKX*FY
         VHS(3,NO2) = VHS(3,NO2) - VKY*FY
         VHS(1,NO3) = VHS(1,NO3) - VEX*FX
         VHS(2,NO3) = VHS(2,NO3) - VEX*FY
         VHS(3,NO3) = VHS(3,NO3) - VEY*FY

C---        Matrice masse lumpée
         VML(NO1) = VML(NO1) + C_ML
         VML(NO2) = VML(NO2) + C_ML
         VML(NO3) = VML(NO3) + C_ML
      ENDDO

      LMGO_T6L_KUEHSSV = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_KUEHSSS
C
C Description:
C     Contribution élémentaire de surface du Hessien.
C
C Entrée:
C
C Sortie:
C
C Notes:
C  Les termes de contour:
C     {N} . d/dx(du/dx) = - {N,x}<N,x>{u} + {N}<N,x>{u}.nx
C     {N} . d/dx(du/dy) = - {N,x}<N,y>{u} + {N}<N,y>{u}.nx
C     {N} . d/dy(du/dy) = - {N,y}<N,x>{u} + {N}<N,x>{u}.ny
C     {N} . d/dy(du/dy) = - {N,y}<N,y>{u} + {N}<N,y>{u}.ny
C
C****************************************************************
      FUNCTION LMGO_T6L_KUEHSSS(KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VNO,
     &                          VHS,
     &                          VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_KUEHSSS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_KUEHSSS

      INTEGER  KNGV(EG_CMMN_NCELV)
      INTEGER  KNGS(EG_CMMN_NCELS)
      REAL*8   VDJV(EG_CMMN_NDJV)
      REAL*8   VDJS(EG_CMMN_NDJS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VHS (3, EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'lmgeom_subt3.fi'
      INCLUDE 'err.fi'

      REAL*8  VDJX(4), VDJY(4)
      REAL*8  VKX, VEX, VNX
      REAL*8  VKY, VEY, VNY
      REAL*8  DJT3, DJL2, C_HS, C_ML
      REAL*8  F1, F2, F3, FX, FY

      INTEGER ICT, IL2, IT3
      INTEGER NO1, NO2, NO3
      INTEGER NP1, NP2
      INTEGER KONE(2, 2)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Côté
      ICT = KNGS(5)

C---     Monte les tables aux. pour permuter les métriques
      VDJX(1) = VDJV(1)
      VDJX(2) = VDJV(2)
      VDJX(3) = -(VDJX(1)+VDJX(2))
      VDJX(4) = VDJX(1)
      VDJY(1) = VDJV(3)
      VDJY(2) = VDJV(4)
      VDJY(3) = -(VDJY(1)+VDJY(2))
      VDJY(4) = VDJY(1)

C---     Métriques permutées du sous-élément T3
      VKX  = UN_2*VDJX(ICT)
      VEX  = UN_2*VDJX(ICT+1)
      VKY  = UN_2*VDJY(ICT)
      VEY  = UN_2*VDJY(ICT+1)
      DJT3 = UN_4*VDJV(5)

C---     Métriques de l'élément - composantes de la normale extérieure
      VNY  = -VDJS(1)
      VNX  =  VDJS(2)
      DJL2 =  UN_2*VDJS(3)

C---     Coefficients
      C_HS = DJL2 / DJT3
      C_ML = DJL2

C---     Table KONE des connectivités des L2
      KONE(1,1) = KNGS(1)
      KONE(2,1) = KNGS(2)
      KONE(1,2) = KNGS(2)
      KONE(2,2) = KNGS(3)

C---     Boucle sur les sous éléments L2
      DO IL2 = 1,2
         IT3 = KT3(IL2, ICT)

         NP1 = KONE(1,IL2)
         NP2 = KONE(2,IL2)

         NO1 = KNGV(KNET3(1, IL2, ICT))
         NO2 = KNGV(KNET3(2, IL2, ICT))
         NO3 = KNGV(KNET3(3, IL2, ICT))

C---        Degrés de liberté du T3
         F1 = VNO(NO1)
         F2 = VNO(NO2)
         F3 = VNO(NO3)
         FX = C_HS*(VKX*(F2-F1) + VEX*(F3-F1))   ! F,x
         FY = C_HS*(VKY*(F2-F1) + VEY*(F3-F1))   ! F,y

C---        Assemble le hessien
         VHS(1,NP1) = VHS(1,NP1) + VNX*FX      ! F,x . nx
         VHS(2,NP1) = VHS(2,NP1) + VNX*FY      ! F,y . ny
         VHS(3,NP1) = VHS(3,NP1) + VNY*FY      ! F,y . ny
         VHS(1,NP2) = VHS(1,NP2) + VNX*FX
         VHS(2,NP2) = VHS(2,NP2) + VNX*FY
         VHS(3,NP2) = VHS(3,NP2) + VNY*FY

C---        Matrice masse lumpée
         VML(NP1) = VML(NP1) + C_ML
         VML(NP2) = VML(NP2) + C_ML
      ENDDO

      LMGO_T6L_KUEHSSS = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_KUEGRDV
C
C Description:
C     Contribution élémentaire du gradient
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_KUEGRDV(KNGE,
     &                          VDJE,
     &                          VNO,
     &                          VDX,
     &                          VDY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_KUEGRDV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_KUEGRDV

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  VKX, VEX
      REAL*8  VKY, VEY
      REAL*8  F1, F2, F3, F4, F5, F6, FX, FY
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Métriques
      VKX = UN_12*VDJE(1)     ! 1/6 * 1/2
      VEX = UN_12*VDJE(2)
      VKY = UN_12*VDJE(3)
      VEY = UN_12*VDJE(4)

C---     Connectivités
      NO1  = KNGE(1)
      NO2  = KNGE(2)
      NO3  = KNGE(3)
      NO4  = KNGE(4)
      NO5  = KNGE(5)
      NO6  = KNGE(6)

C---     Degrés de liberté
      F1 = VNO(NO1)
      F2 = VNO(NO2)
      F3 = VNO(NO3)
      F4 = VNO(NO4)
      F5 = VNO(NO5)
      F6 = VNO(NO6)

C---     Assemble
      FX = VKX*(F2-F1) + VEX*(F3-F1)   ! F,x
      FY = VKY*(F2-F1) + VEY*(F3-F1)   ! F,y
      VDX(NO1) = VDX(NO1) + FX
      VDX(NO2) = VDX(NO2) + FX
      VDX(NO3) = VDX(NO3) + FX
      VDY(NO1) = VDY(NO1) + FY
      VDY(NO2) = VDY(NO2) + FY
      VDY(NO3) = VDY(NO3) + FY

C---     Triangle 1-2-6
      FX = VKX*(F2-F1) + VEX*(F6-F1)   ! F,y
      FY = VKY*(F2-F1) + VEY*(F6-F1)   ! F,y
      VDX(NO1) = VDX(NO1) + FX
      VDX(NO2) = VDX(NO2) + FX
      VDX(NO6) = VDX(NO6) + FX
      VDY(NO1) = VDY(NO1) + FY
      VDY(NO2) = VDY(NO2) + FY
      VDY(NO6) = VDY(NO6) + FY

C---     Triangle 2-3-4
      FX = VKX*(F3-F2) + VEX*(F4-F2)   ! F,y
      FY = VKY*(F3-F2) + VEY*(F4-F2)   ! F,y
      VDX(NO2) = VDX(NO2) + FX
      VDX(NO3) = VDX(NO3) + FX
      VDX(NO4) = VDX(NO4) + FX
      VDY(NO2) = VDY(NO2) + FY
      VDY(NO3) = VDY(NO3) + FY
      VDY(NO4) = VDY(NO4) + FY

C---     Triangle 6-4-5
      FX = VKX*(F4-F6) + VEX*(F5-F6)   ! F,y
      FY = VKY*(F4-F6) + VEY*(F5-F6)   ! F,y
      VDX(NO6) = VDX(NO6) + FX
      VDX(NO4) = VDX(NO4) + FX
      VDX(NO5) = VDX(NO5) + FX
      VDY(NO6) = VDY(NO6) + FY
      VDY(NO4) = VDY(NO4) + FY
      VDY(NO5) = VDY(NO5) + FY

C---     Triangle 4-6-2
      FX = VKX*(F6-F4) + VEX*(F2-F4)   ! F,y
      FY = VKY*(F6-F4) + VEY*(F2-F4)   ! F,y
      VDX(NO4) = VDX(NO4) - FX    ! - car métriques inversées
      VDX(NO6) = VDX(NO6) - FX
      VDX(NO2) = VDX(NO2) - FX
      VDY(NO4) = VDY(NO4) - FY    ! - car métriques inversées
      VDY(NO6) = VDY(NO6) - FY
      VDY(NO2) = VDY(NO2) - FY

      LMGO_T6L_KUEGRDV = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_KUEDDX
C
C Description:
C     Contribution élémentaire de la dérivée en x
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_KUEDDX(KNGE,
     &                         VDJE,
     &                         VNO,
     &                         VDX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_KUEDDX
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_KUEDDX

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  VKX, VEX
      REAL*8  F1, F2, F3, F4, F5, F6, FX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Métriques
      VKX = UN_12*VDJE(1)     ! 1/6 * 1/2
      VEX = UN_12*VDJE(2)

C---     Connectivités
      NO1  = KNGE(1)
      NO2  = KNGE(2)
      NO3  = KNGE(3)
      NO4  = KNGE(4)
      NO5  = KNGE(5)
      NO6  = KNGE(6)

C---     Degrés de liberté
      F1 = VNO(NO1)
      F2 = VNO(NO2)
      F3 = VNO(NO3)
      F4 = VNO(NO4)
      F5 = VNO(NO5)
      F6 = VNO(NO6)

C---     Triangle 1-2-6
      FX = VKX*(F2-F1) + VEX*(F6-F1)   ! F,y
      VDX(NO1) = VDX(NO1) + FX
      VDX(NO2) = VDX(NO2) + FX
      VDX(NO6) = VDX(NO6) + FX

C---     Triangle 2-3-4
      FX = VKX*(F3-F2) + VEX*(F4-F2)   ! F,y
      VDX(NO2) = VDX(NO2) + FX
      VDX(NO3) = VDX(NO3) + FX
      VDX(NO4) = VDX(NO4) + FX

C---     Triangle 6-4-5
      FX = VKX*(F4-F6) + VEX*(F5-F6)   ! F,y
      VDX(NO6) = VDX(NO6) + FX
      VDX(NO4) = VDX(NO4) + FX
      VDX(NO5) = VDX(NO5) + FX

C---     Triangle 4-6-2
      FX = VKX*(F6-F4) + VEX*(F2-F4)   ! F,y
      VDX(NO4) = VDX(NO4) - FX    ! - car métriques inversées
      VDX(NO6) = VDX(NO6) - FX
      VDX(NO2) = VDX(NO2) - FX

      LMGO_T6L_KUEDDX = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_KUEDDY
C
C Description:
C     Contribution élémentaire de la dérivée en y
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_KUEDDY(KNGE,
     &                         VDJE,
     &                         VNO,
     &                         VDY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_KUEDDY
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_KUEDDY

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  VKY, VEY
      REAL*8  F1, F2, F3, F4, F5, F6, FY
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Métriques
      VKY = UN_12*VDJE(3)     ! 1/6 * 1/2
      VEY = UN_12*VDJE(4)

C---     Connectivités
      NO1  = KNGE(1)
      NO2  = KNGE(2)
      NO3  = KNGE(3)
      NO4  = KNGE(4)
      NO5  = KNGE(5)
      NO6  = KNGE(6)

C---     Degrés de liberté
      F1 = VNO(NO1)
      F2 = VNO(NO2)
      F3 = VNO(NO3)
      F4 = VNO(NO4)
      F5 = VNO(NO5)
      F6 = VNO(NO6)

C---     Triangle 1-2-6
      FY = VKY*(F2-F1) + VEY*(F6-F1)   ! F,y
      VDY(NO1) = VDY(NO1) + FY
      VDY(NO2) = VDY(NO2) + FY
      VDY(NO6) = VDY(NO6) + FY

C---     Triangle 2-3-4
      FY = VKY*(F3-F2) + VEY*(F4-F2)   ! F,y
      VDY(NO2) = VDY(NO2) + FY
      VDY(NO3) = VDY(NO3) + FY
      VDY(NO4) = VDY(NO4) + FY

C---     Triangle 6-4-5
      FY = VKY*(F4-F6) + VEY*(F5-F6)   ! F,y
      VDY(NO6) = VDY(NO6) + FY
      VDY(NO4) = VDY(NO4) + FY
      VDY(NO5) = VDY(NO5) + FY

C---     Triangle 4-6-2
      FY = VKY*(F6-F4) + VEY*(F2-F4)   ! F,y
      VDY(NO4) = VDY(NO4) - FY    ! - car métriques inversées
      VDY(NO6) = VDY(NO6) - FY
      VDY(NO2) = VDY(NO2) - FY

      LMGO_T6L_KUEDDY = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_KUEML
C
C Description:
C     Contribution élémentaire de matrice masse lumpée
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_KUEML(KNGE,
     &                        VDJE,
     &                        VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_KUEML
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_KUEML

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  DJT3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Métriques
      DJT3= UN_24*VDJE(5)     ! 1/6 * 1/4

C---     Connectivités
      NO1  = KNGE(1)
      NO2  = KNGE(2)
      NO3  = KNGE(3)
      NO4  = KNGE(4)
      NO5  = KNGE(5)
      NO6  = KNGE(6)

C---     Triangle 1-2-6
      VML(NO1) = VML(NO1) + DJT3
      VML(NO2) = VML(NO2) + DJT3
      VML(NO6) = VML(NO6) + DJT3

C---     Triangle 2-3-4
      VML(NO2) = VML(NO2) + DJT3
      VML(NO3) = VML(NO3) + DJT3
      VML(NO4) = VML(NO4) + DJT3

C---     Triangle 6-4-5
      VML(NO6) = VML(NO6) + DJT3
      VML(NO4) = VML(NO4) + DJT3
      VML(NO5) = VML(NO5) + DJT3

C---     Triangle 4-6-2
      VML(NO4) = VML(NO4) + DJT3
      VML(NO6) = VML(NO6) + DJT3
      VML(NO2) = VML(NO2) + DJT3

      LMGO_T6L_KUEML = ERR_TYP()
      RETURN
      END
