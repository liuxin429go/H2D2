C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

      SUBMODULE(LM_GOT6L_M) LM_GOT6L_CLCERR_M
      
      IMPLICIT NONE

      CONTAINS
      
C**************************************************************
C Sommaire: LMGO_T6_CLCERR
C
C Description:
C     Calcul de l'erreur due à VFRM pour le raffinement de maillage
C     EQUATION : NORME DE LA 2. DERIVEE
C
C Entrée:
C     VCORG       Table des COoRdonnées Globales
C     KNGV        Table des coNectivités Globales de Volume
C     KNGS        Table des coNectivités Globales de Surface
C     VDJV        Table des métriques de l'élément de Volume
C     VDJS        Table des métriques de l'élément de Surface
C     VFRM        Table des valeurs nodales de la fonction
C     ITPCLC      Bit-set du type de calcul
C
C Sortie:
C     VHESS       Table du hessien ou de l'ellipse
C     VHNRM       Table de la norme du hessien
C
C Notes:
C
C****************************************************************
      MODULE INTEGER 
     &FUNCTION LM_GOT6L_CLCERR(SELF,
     &                         VFRM,
     &                         VHESS,
     &                         VHNRM,
     &                         ITPCLC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT6L_CLCERR
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LG_LGCY_M

      CLASS(LM_GOT6L_T), INTENT(IN), TARGET :: SELF
      REAL*8,  INTENT(IN)  :: VFRM  (:)
      REAL*8,  INTENT(OUT) :: VHESS (:, :)
      REAL*8,  INTENT(OUT) :: VHNRM (:)
      INTEGER, INTENT(IN)  :: ITPCLC

!      INCLUDE 'eacnst.fi'
!      INCLUDE 'egtperr.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'err.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      CLASS(LG_LGCY_T), POINTER :: OGEO
      REAL*8 TSIM
      INTEGER IERR
      INTEGER HCONF, HGRID, HLIMT

      INTEGER LM_GOT6L_ERRHESS
      INTEGER LM_GOT6L_ERRDXDX
      INTEGER LM_GOT6L_ERRRCOV
C-----------------------------------------------------------------------

      OGEO => NULL()
      
C---     Construis un élément legacy et dispatch le calcul
      HCONF = SELF%REQHCONF()
      HGRID = SELF%REQHGRID()
      HLIMT = SELF%REQHLIMT()
      TSIM  = 0.0D0
      IF (ERR_GOOD()) OGEO => LG_LGCY_CTR(EG_TPGEO_T6L)
      IF (ERR_GOOD()) IERR = OGEO%INI   (HCONF, HGRID, HLIMT)
      IF (ERR_GOOD()) IERR = OGEO%PASDEB(TSIM)
      IF (ERR_GOOD()) IERR = OGEO%CLCERR(VFRM, VHESS, VHNRM, ITPCLC)

      IF (ASSOCIATED(OGEO)) THEN
         CALL ERR_PUSH()
         IERR = DEL(OGEO)
         CALL ERR_POP()
      ENDIF

!!!C---     Récupère les attributs
!!!      GDTA => LM_GOT6L_REQGDTA(HOBJ)
!!!D     CALL ERR_PRE(GDTA%NNL   .GE. 6)
!!!D     CALL ERR_PRE(GDTA%NCELV .EQ. 6)
!!!
!!!C---     Initialise
!!!      VHESS(1:3,1:GDTA%NNL) = ZERO  ! CALL DINIT (3*EG_CMMN_NNL, ZERO, VHESS, 1)
!!!      VHNRM(    1:GDTA%NNL) = ZERO  ! CALL DINIT (  EG_CMMN_NNL, ZERO, VHNRM, 1)
!!!
!!!C---     Calcule le Hessien
!!!      IF     (BTEST(ITPCLC, EG_TPERR_HESSGREEN)) THEN
!!!         IERR = LM_GOT6L_ERRHESS(GDTA%KNGV,
!!!     &                           GDTA%KNGS,
!!!     &                           GDTA%VDJV,
!!!     &                           GDTA%VDJS,
!!!     &                           VFRM,
!!!     &                           VHESS,
!!!     &                           VHNRM)
!!!      ELSEIF (BTEST(ITPCLC, EG_TPERR_HESSDXDX)) THEN
!!!         IERR = LM_GOT6L_ERRDXDX(GDTA%KNGV,
!!!     &                           GDTA%KNGS,
!!!     &                           GDTA%VDJV,
!!!     &                           GDTA%VDJS,
!!!     &                           VFRM,
!!!     &                           VHESS,
!!!     &                           VHNRM)
!!!      ELSEIF (BTEST(ITPCLC, EG_TPERR_HESSRECOV)) THEN
!!!         IERR = LM_GOT6L_ERRRCOV(GDTA%VCORG,
!!!     &                           GDTA%KNGV,
!!!     &                           VFRM,
!!!     &                           VHESS)
!!!      ELSE
!!!         CALL ERR_ASR(.FALSE.)
!!!      ENDIF
!!!
!!!C---     Transforme le Hessien
!!!      IF     (BTEST(ITPCLC, EG_TPERR_NRMHESS)) THEN
!!!         DO IN=1,GDTA%NNL
!!!            VHNRM(IN) = SQRT(VHESS(1,IN)*VHESS(1,IN) +
!!!     &                       VHESS(2,IN)*VHESS(2,IN)*DEUX +
!!!     &                       VHESS(3,IN)*VHESS(3,IN))
!!!         ENDDO
!!!      ELSEIF (BTEST(ITPCLC, EG_TPERR_ELLIPSE)) THEN
!!!         CALL SP_ELEM_HESSELLPS(3, GDTA%NNL, VHESS)
!!!      ENDIF

      LM_GOT6L_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  LM_GOT6L_ERRHESS
C
C Description:
C     La fonction LM_GOT6L_ERRHESS calcule l'erreur d'approximation pour
C     des éléments de volume de type T6L.
C     EQUATION : NORME DE LA 2. DERIVEE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Appel à l'ancien élément
C************************************************************************
      FUNCTION LM_GOT6L_ERRHESS(KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VFRM,
     &                          VHESS,
     &                          VTRV)

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT6L_ERRHESS

      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VFRM  (EG_CMMN_NNL)
      REAL*8   VHESS (3, EG_CMMN_NNL)
      REAL*8   VTRV  (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'err.fi'

      INTEGER IERR

      INTEGER LMGO_T6L_CLCHESS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Calcule le Hessien
      IERR = LMGO_T6L_CLCHESS(KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VFRM,
     &                        VHESS,
     &                        VTRV)

      LM_GOT6L_ERRHESS = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT6L_ERRDXDX
C
C Description:
C     Calcul de l'erreur due à VFRM pour le raffinement de maillage
C     EQUATION : NORME DE LA 2. DERIVEE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La table est reçue inversée (nnl,3) plutôt que (3,nnl) pour
C     faciliter les calculs. A la fin elle est rebasculée.
C****************************************************************
      FUNCTION LM_GOT6L_ERRDXDX(KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VFRM,
     &                          VHESS,
     &                          VTRV)

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT6L_ERRDXDX

      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VFRM  (EG_CMMN_NNL)
      REAL*8   VHESS (EG_CMMN_NNL, 3)
      REAL*8   VTRV  (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IN

      INTEGER LMGO_T6L_CLCDDX
      INTEGER LMGO_T6L_CLCDDY
      INTEGER LMGO_T6L_ASMDDX
      INTEGER LMGO_T6L_ASMDDY
      INTEGER LMGO_T6L_ASMML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Calcule f,y dans VHESS(.,1)
      IERR = LMGO_T6L_CLCDDY(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VFRM,
     &                       VHESS(1,1),
     &                       VTRV)

C---     Assemble f,yy dans VHESS(.,3)
      IERR = LMGO_T6L_ASMDDY(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VHESS(1,1),
     &                       VHESS(1,3))

C---     Calcule f,x dans VTRV
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VTRV, 1)
      IERR = LMGO_T6L_CLCDDX(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VFRM,
     &                       VTRV,
     &                       VHESS(1,2))

C---     Assemble f,xx dans VHESS(.,1)
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VHESS(1,1), 1)
      IERR = LMGO_T6L_ASMDDX(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VTRV,
     &                       VHESS(1,1))

C---     Assemble f,xy dans VHESS(.,2)
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VHESS(1,2), 1)
      IERR = LMGO_T6L_ASMDDY(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VTRV,
     &                       VHESS(1,2))

C---     Assemble Ml dans VTRV
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VTRV, 1)
      IERR = LMGO_T6L_ASMML (KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VTRV)

C---     Finalise
      DO IN=1,EG_CMMN_NNL
         VTRV(IN) = UN / VTRV(IN)
      ENDDO
      DO IN=1,EG_CMMN_NNL
         VHESS(IN,1) = VHESS(IN,1) * VTRV(IN)
      ENDDO
      DO IN=1,EG_CMMN_NNL
         VHESS(IN,2) = VHESS(IN,2) * VTRV(IN)
      ENDDO
      DO IN=1,EG_CMMN_NNL
         VHESS(IN,3) = VHESS(IN,3) * VTRV(IN)
      ENDDO

C---     Transpose la table VHESS
      CALL TRANS(VHESS,EG_CMMN_NNL, 3, 3*EG_CMMN_NNL,
     &           VTRV, EG_CMMN_NNL, IERR)
      IF (IERR .NE. 0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_TRANSPOSE_TABLE')
      ENDIF

      LM_GOT6L_ERRDXDX = ERR_TYP()
      RETURN
      END

      END SUBMODULE LM_GOT6L_CLCERR_M
      