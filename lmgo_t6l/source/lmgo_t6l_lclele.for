C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T6L_LCLELE
C
C Description:
C     La fonction LMGO_T6L_LCLELE localise l'élément sous les points
C     de la liste VCORP. Elle retourne dans KELEP les numéros d'élément
C     et dans VCORE les coordonnées sur l'élément de référence du point.
C     Si un point n'est pas trouvé son numéro d'élément est 0.
C
C Entrée:
C     NPNT        Nombre de points
C     VCORP       Coordonnées des points
C
C Sortie:
C     KELEP       Table des numéros d'élément
C     VCORE       Coordonnées sur l'élément de référence
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_LCLELE (TOL,
     &                          NDIM,
     &                          NNT,
     &                          NCELV,
     &                          NELV,
     &                          NDJV,
     &                          VCORG,
     &                          KNGV,
     &                          VDJV,
     &                          NPNT,
     &                          VCORP,
     &                          KELEP,
     &                          VCORE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_LCLELE
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  TOL
      INTEGER NDIM
      INTEGER NNT
      INTEGER NCELV
      INTEGER NELV
      INTEGER NDJV
      REAL*8  VCORG(NDIM, NNT)
      INTEGER KNGV (NCELV,NELV)
      REAL*8  VDJV (NDJV, NELV)
      INTEGER NPNT
      REAL*8  VCORP(NDIM, NPNT)
      INTEGER KELEP(NPNT)
      REAL*8  VCORE(NDIM, NPNT)

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmgo_t3.fi'

      INTEGER IERR
C----------------------------------------------------------------------

C---     Sous-traite au T3
      IERR = LMGO_T3_LCLELE (TOL,
     &                       NDIM,
     &                       NNT,
     &                       NCELV,
     &                       NELV,
     &                       NDJV,
     &                       VCORG,
     &                       KNGV,
     &                       VDJV,
     &                       NPNT,
     &                       VCORP,
     &                       KELEP,
     &                       VCORE)

      LMGO_T6L_LCLELE = ERR_TYP()
      RETURN
      END
