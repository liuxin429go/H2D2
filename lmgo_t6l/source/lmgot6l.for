C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_GOT6L
C         FTN (Sub)Module: LM_GOT6L_M
C            Public:
C               SUBROUTINE LM_GOT6L_CTR
C            Private:
C            
C            FTN Type: LM_GOT6L_T
C               Public:
C                  INTEGER LM_GOT6L_DTR
C               Private:
C                  INTEGER LM_GOT6L_INIPRMS
C
C************************************************************************

      MODULE LM_GOT6L_M

      USE LM_GEOM_M, ONLY: LM_GEOM_T
      IMPLICIT NONE

      PUBLIC

      !========================================================================
      ! ---   Classe
      !========================================================================
      TYPE, EXTENDS(LM_GEOM_T) :: LM_GOT6L_T
!        pass
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR      => LM_GOT6L_DTR
         PROCEDURE, PUBLIC :: ASMESCL  => LM_GOT6L_ASMESCL
         !!!PROCEDURE, PUBLIC :: ASMPEAU  => LM_GOT6L_ASMPEAU
         PROCEDURE, PUBLIC :: CLCERR   => LM_GOT6L_CLCERR
         PROCEDURE, PUBLIC :: CLCJELS  => LM_GOT6L_CLCJELS
         PROCEDURE, PUBLIC :: CLCJELV  => LM_GOT6L_CLCJELV
         PROCEDURE, PUBLIC :: CLCSPLT  => LM_GOT6L_CLCSPLT
         PROCEDURE, PUBLIC :: INTRP    => LM_GOT6L_INTRP
         PROCEDURE, PUBLIC :: LCLELV   => LM_GOT6L_LCLELV

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: INIPRMS => LM_GOT6L_INIPRMS
      END TYPE LM_GOT6L_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: LM_GOT6L_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: LM_GOT6L_DTR
      END INTERFACE DEL

      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
         MODULE INTEGER FUNCTION LM_GOT6L_ASMESCL(SELF)
            CLASS(LM_GOT6L_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_GOT6L_ASMESCL

!!!         MODULE INTEGER FUNCTION LM_GOT6L_ASMPEAU(SELF)
!!!            IMPORT :: LM_GOT6L_T
!!!            CLASS(LM_GOT6L_T), INTENT(IN) :: SELF
!!!         END FUNCTION LM_GOT6L_ASMPEAU

         MODULE INTEGER FUNCTION LM_GOT6L_CLCERR(SELF,
     &                              VFRM, VHESS, VHNRM, ITPCLC)
            CLASS(LM_GOT6L_T), INTENT(IN), TARGET :: SELF
            REAL*8,  INTENT(IN)  :: VFRM (:)
            REAL*8,  INTENT(OUT) :: VHESS(:,:)
            REAL*8,  INTENT(OUT) :: VHNRM(:)
            INTEGER, INTENT(IN)  :: ITPCLC
         END FUNCTION LM_GOT6L_CLCERR

         MODULE INTEGER FUNCTION LM_GOT6L_CLCJELS(SELF)
            CLASS(LM_GOT6L_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_GOT6L_CLCJELS

         MODULE INTEGER FUNCTION LM_GOT6L_CLCJELV(SELF)
            CLASS(LM_GOT6L_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_GOT6L_CLCJELV

         MODULE INTEGER FUNCTION LM_GOT6L_CLCSPLT(SELF, KNGZ)
            CLASS(LM_GOT6L_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(OUT) :: KNGZ(:, :)
         END FUNCTION LM_GOT6L_CLCSPLT

         MODULE INTEGER FUNCTION LM_GOT6L_INTRP(SELF,
     &                              KELE, VCORE,
     &                              INDXS, VSRC,
     &                              INDXD, VDST)
            CLASS(LM_GOT6L_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN)  :: KELE (:)
            REAL*8,  INTENT(IN)  :: VCORE(:, :)
            INTEGER, INTENT(IN)  :: INDXS
            REAL*8,  INTENT(IN)  :: VSRC (:, :)
            INTEGER, INTENT(IN)  :: INDXD
            REAL*8,  INTENT(OUT) :: VDST (:, :)
         END FUNCTION LM_GOT6L_INTRP

         MODULE INTEGER FUNCTION LM_GOT6L_LCLELV(SELF,
     &                              TOL, VCORP, KELEP, VCORE)
            CLASS(LM_GOT6L_T), INTENT(IN), TARGET :: SELF
            REAL*8,  INTENT(IN)  :: TOL
            REAL*8,  INTENT(IN)  :: VCORP(:, :)
            INTEGER, INTENT(OUT) :: KELEP(:)
            REAL*8,  INTENT(OUT) :: VCORE(:, :)
         END FUNCTION LM_GOT6L_LCLELV

      END INTERFACE


      CONTAINS

C************************************************************************
C Sommaire:
C
C Description: Constructeur de la classe
C
C Entrée:
C     HKID     Handle sur l'héritier qui a alloué la structure
C
C Sortie:
C     HOBJ     Handle sur l'objet créé
C
C Notes:
C     l'élément est concret
C************************************************************************
      FUNCTION LM_GOT6L_CTR() RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT6L_CTR
CDEC$ ENDIF

      USE LM_GEOM_M, ONLY: LM_GEOM_CTR

      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      CLASS(LM_GEOM_T),  POINTER :: OPRNT
      TYPE (LM_GOT6L_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Construis le parent
      IF (ERR_GOOD()) OPRNT => LM_GEOM_CTR(SELF)

C---     Initialise les dimensions
      IF (ERR_GOOD()) IERR = SELF%INIPRMS()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION LM_GOT6L_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur LM_GOT6L_DTR est la partie virtuelle du processus
C     de destruction. Il est appelé par le parent sur appel de DEL.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOT6L_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT6L_DTR
CDEC$ ENDIF

      USE LM_GEOM_M, ONLY: LM_GEOM_DTR

      CLASS(LM_GOT6L_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(LM_GOT6L_T), POINTER :: SELF_P
C------------------------------------------------------------------------

C---     Détruis le parent
      IERR = LM_GEOM_DTR(SELF)

C---     Désalloue la structure
      IF (ERR_GOOD()) THEN
         SELF_P => SELF
         DEALLOCATE(SELF_P)
         SELF_P => NULL()
      ENDIF

      LM_GOT6L_DTR = ERR_TYP()
      RETURN
      END  FUNCTION LM_GOT6L_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode LM_GOT6L_INIPRMS initialise les paramètres de
C     dimension de l'objet. Ils sont vus comme des attributs statiques.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOT6L_INIPRMS(SELF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      
      CLASS(LM_GOT6L_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'egtpgeo.fi'
      INCLUDE 'err.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA

C---     Initialise les paramètres invariant
      GDTA%NDIM  = 2

      GDTA%ITPEV = EG_TPGEO_T6L
      GDTA%NNELV = 6
      GDTA%NCELV = 6        ! NNELV
      GDTA%NDJV  = 2*2 + 1  ! NDIM*2 + 1

      GDTA%ITPES = EG_TPGEO_L3L
      GDTA%NNELS = 3
      GDTA%NCELS = 3 + 2    ! NNELS + 2
      GDTA%NDJS  = 3        ! NDIM + 1

      GDTA%ITPEZ = EG_TPGEO_T3
      GDTA%NNELZ = 3
      GDTA%NCELZ = 3        ! NNELZ
      GDTA%NDJZ  = 2*2 + 1  ! NDIM*2 + 1
      GDTA%NEV2EZ= 4

      LM_GOT6L_INIPRMS = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT6L_INIPRMS

      END MODULE LM_GOT6L_M
