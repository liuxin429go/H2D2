C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

      SUBMODULE(LM_GOT6L_M) LM_GOT6L_ASMPEAU_M
      
      IMPLICIT NONE

      CONTAINS
      
C************************************************************************
C Sommaire:  LM_GOT6L_ASMPEAU
C
C Description:
C     La fonction LM_GOT6L_ASMPEAU assemble la peau pour des
C     éléments de volume de type T6L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER 
     &FUNCTION LM_GOT6L_ASMPEAU(SELF,
     &                          KPNT,
     &                          NLIEN,
     &                          KLIEN,
     &                          INEXT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT6L_ASMPEAU
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      CLASS(LM_GOT6L_T), INTENT(IN), TARGET :: SELF
      INTEGER KPNT (:)
      INTEGER NLIEN
      INTEGER KLIEN(4,:)
      INTEGER INEXT

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER, DIMENSION(:,:), POINTER :: KNGV
      INTEGER NNL, NCELV, NELLV

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF
      INTEGER INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     CALL ERR_PRE(GDTA%NNL   .GE. 1)
D     CALL ERR_PRE(GDTA%NELLV .GE. 1)
D     CALL ERR_PRE(GDTA%NCELV .EQ. 6)
      NNL   = GDTA%NNL
      NELLV = GDTA%NNELV
      KNGV  => GDTA%KNGV

C---     Boucle sur les éléments
      NVLNK = .FALSE.
      INFO(2) = 0
      DO IE=1,NELLV

C---        SIDE 1 -- NODES 1-3
         IF (KNGV(1,IE) .LT. KNGV(2,IE)) THEN
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNL, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 2 -- NODES 3-5
         IF (KNGV(2,IE) .LT. KNGV(3,IE)) THEN
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(5, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(5, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNL, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 3 -- NODES 5-1
         IF (KNGV(3,IE) .LT. KNGV(1,IE)) THEN
            ICLEF   = KNGV(5, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(5, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNL, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

      ENDDO

9999  CONTINUE
      LM_GOT6L_ASMPEAU = ERR_TYP()
      RETURN
      END

      END SUBMODULE LM_GOT6L_ASMPEAU_M
