C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D DE BASE SANS CINÉTIQUES.
C                FORMULATION NON-CONSERVATIVE POUR (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C     Élément de convection-diffusion sans cinétiques à 5 ddls par noeuds
C     Élément réservé aux tests de contrôle de programmation
C
C************************************************************************

C************************************************************************
C Sommaire : CD2D$TST$CLCPRNO
C
C Description:
C     CALCUL DES PROPRIETES NODALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D$TST$CLCPRNO (VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             VDLG,
     &                             IERR)

      IMPLICIT REAL*8 (A-H, O-Z)
      INCLUDE 'lmcmmn.fc'

      REAL*8  VCORG(JJ$NDIM,  JJ$NNT)
      INTEGER KNGV (JJ$NCELV, JJ$NELV)
      REAL*8  VDJV (JJ$NDJV,  JJ$NELV)
      REAL*8  VPRGL(JJ$NPRGL)
      REAL*8  VPRNO(JJ$NPRNO, JJ$NNT)
      REAL*8  VDLG (JJ$NDLN,  JJ$NNT)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL CD2D$BSE$CLCPRNO(VCORG,KNGV,VDJV,VPRGL,VPRNO,VDLG,IERR)

      RETURN
      END

