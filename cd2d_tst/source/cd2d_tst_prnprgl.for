C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D DE BASE SANS CINÉTIQUES.
C                FORMULATION NON-CONSERVATIVE POUR (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C     Élément de convection-diffusion sans cinétiques à 5 ddls par noeuds
C     Élément réservé aux tests de contrôle de programmation
C
C************************************************************************

C************************************************************************
C Sommaire : CD2D$TST$PRNPRGL
C
C Description:
C     IMPRESSION DES PROPRIETES GLOBALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D$TST$PRNPRGL(NPRGL,
     &                            VPRGL)

      INTEGER NPRGL
      REAL*8  VPRGL(NPRGL)
C-----------------------------------------------------------------------

      CALL CD2D$BSE$PRNPRGL(NPRGL, VPRGL)

      RETURN
      END

