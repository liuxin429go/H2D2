C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Coliformes fécaux (CLF)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: Assigne les valeurs au common.
C
C Description:
C     La fonction CD2D_CLF_ASGCMN permet d'assigner les valeurs au common.
C
C     Chaque DLL possède une copie propre du common. Avant d'appeler une
C     fonctionnalité de la DLL, il faut mettre à jour le common. Cette
C     mécanique ne fonctionne que dans un contexte ou la DLL n'est utilisée
C     que par un seul process. De plus en multi-tâche, il faut que toute les
C     tâches utilises les mêmes valeurs de common.
C
C Entrée:
C     KA    Vecteur des valeurs du common
C
C Sortie:
C
C Notes:
C     Comme on utilise la DLL du parent (CD2D_BSE_nnn), celle-ci doit
C     également être mise à jour.
C************************************************************************
      FUNCTION CD2D_CLF_ASGCMN(EG_KA, EA_KA, EA_VA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_CLF_ASGCMN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER CD2D_CLF_ASGCMN
      INTEGER EG_KA(*)
      INTEGER EA_KA(*)
      REAL*8  EA_VA(*)

      INCLUDE 'err.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER I
      INTEGER IERR
      INTEGER CD2D_B1L_ASGCMN
C-----------------------------------------------------------------------

      DO I=1, EG_CMMN_KA_DIM
         EG_CMMN_KA(I) = EG_KA(I)
      ENDDO

      DO I=1, LM_CMMN_KA_DIM
         LM_CMMN_KA(I) = EA_KA(I)
      ENDDO

      DO I=1, LM_CMMN_VA_DIM
         LM_CMMN_VA(I) = EA_VA(I)
      ENDDO

      IERR = CD2D_B1L_ASGCMN(EG_KA, EA_KA, EA_VA)

      CD2D_CLF_ASGCMN = ERR_TYP()
      RETURN
      END
