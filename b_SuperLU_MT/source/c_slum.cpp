//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************
/*
      SuperLU (Version 3.0)
      =====================

Copyright (c) 2003, The Regents of the University of California, through
Lawrence Berkeley National Laboratory (subject to receipt of any required
approvals from U.S. Dept. of Energy)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

(1) Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
(2) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
(3) Neither the name of Lawrence Berkeley National Laboratory, U.S. Dept. of
Energy nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
//*****************************************************************************
// Fichier: $Id$
// Classe:
//*****************************************************************************
#include "c_slum.h"

#include "slu_mt_ddefs.h"
#include "slu_mt_util.h"

#include <assert.h>
#include <limits>
#include <string>
#include <sstream>

#if defined(_MSC_VER)
#  pragma warning( push )
#  pragma warning( disable : 1195 )  // conversion from integer to smaller pointer
#  pragma warning( disable : 1684 )  // conversion from pointer to same-sized integral type
#endif

struct SuperLUSystem
{
   SuperMatrix A;
   SuperMatrix AC;
   SuperMatrix L;
   SuperMatrix U;

   int *rowind;      //
   int *colptr;      //
   int *etree;       // elimination tree
   int *perm_r;      // row permutations from partial pivoting
   int *perm_c;      // column permutation vector

   superlumt_options_t opts;
   double drop_tol;
   int panel_size;
   int relax;
   int lwork;

   int  n;
   bool ccs;

   SuperLUSystem()
   : rowind    (NULL)
   , colptr    (NULL)
   , etree     (NULL)
   , perm_r    (NULL)
   , perm_c    (NULL)
   , drop_tol  (0.0)
   , panel_size(-1)
   , relax     (-1)
   , lwork     (0)
   , n         (0)
   , ccs       (false)
   {
      A.Store  = NULL;
      AC.Store = NULL;
      L.Store  = NULL;
      U.Store  = NULL;
   }
};

// --- code d'erreur
static std::string strErr = "";


// ---  Redéfinition de xerbla pour intercepter le message d'erreur
extern "C"
int xerbla_(char *srname, int *info)
{
   std::ostringstream os;
   os << "SuperLU_MT: On entry to " << srname << " parameter number " << *info << " had an illegal value";
   strErr = os.str();
   return 0;
}

// ---  Fonction utilitaires
template <bool b>
int* c_slum_copyTbl(fint_t , fint_t*);

template <>
int* c_slum_copyTbl<true> (fint_t, fint_t* k)
{
   return reinterpret_cast<int*>(k);
}
template <>
int* c_slum_copyTbl<false> (fint_t n, fint_t* k)
{
   int* rP = new int[n];
   for (fint_t i = 0; i < n; ++i) rP[i] = static_cast<int>(k[i]);
   return rP;
}

template <bool b>
void c_slum_deleteTbl(int*);

template <>
void c_slum_deleteTbl<true> (int*)
{
   return;
}
template <>
void c_slum_deleteTbl<false> (int* rP)
{
   delete[] rP;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction privée <code>c_slum_initmat</code> initialise la matrice
//    à partir des arguments.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
void c_slum_initmat (SuperLUSystem *sP,
                     fint_t *n,
                     fint_t *nnz,
                     double *values,
                     fint_t *rowind,
                     fint_t *colptr)
{
#ifdef MODE_DEBUG
   assert(sP != NULL);
   assert(sP->n == *n);
#endif

   // ---  If int size are not equals, copy
   if (sP->opts.fact == DOFACT)
   {
      const bool sameIntSize = (sizeof(fint_t) == sizeof(int));
      sP->rowind = c_slum_copyTbl<sameIntSize>(*nnz, rowind);
      sP->colptr = c_slum_copyTbl<sameIntSize>(*n+1, colptr);
   }

   // ---  Create SuperLU Matrix
   if (sP->opts.fact == DOFACT)
   {
      dCreate_CompCol_Matrix(&(sP->A),
                             static_cast<int>(*n),
                             static_cast<int>(*n),
                             static_cast<int>(*nnz),
                             values,
                             sP->rowind,
                             sP->colptr,
                             SLU_NC,
                             SLU_D,
                             SLU_GE);
   }

   // ---  Permutation vectors
   if (sP->opts.fact == DOFACT)
   {
      get_perm_c(sP->opts.ColPerm, &(sP->A), sP->perm_c);
   }

   // ---  Permutes the columns and build the elimination tree
   if (sP->opts.fact == DOFACT)
   {
      sp_preorder(&(sP->opts), &(sP->A), sP->perm_c, sP->etree, &(sP->AC));
   }

   return;
}


//************************************************************************
// Sommaire:   Assigne un message d'erreur
//
// Description:
//    La fonction <code>C_SLUM_ASGMSG(...)</code> assigne un message
//    d'erreur à <code>msgErr</code> à partir du code d'erreur <code>ierr</code>
//
// Entrée:
//
// Sortie:
//    F2C_CONF_STRING msgErr   Message d'erreur
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_ASGMSG(F2C_CONF_STRING  msgErr
                                                            F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_ASGMSG(F2C_CONF_STRING  msgErr)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* msgErrP = msgErr;
   int   l  = len;
#else
   char* msgErrP = msgErr->strP;
   int   l  = str->len;
#endif

   memset(msgErrP, ' ', l);
   if (l >= static_cast<int> (strErr.length()))
   {
      memcpy(msgErrP, strErr.c_str(), strErr.length());
   }

   strErr = "";

   return 0;
}

//************************************************************************
// Sommaire:   Initialise la matrice
//
// Description:
//    La fonction <code>C_SLUM_INIT</code> dimensionne la matrice.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_INIT (hndl_t* hndl,
                                                           fint_t* n,
                                                           fint_t* ccs)
{
#ifdef MODE_DEBUG
   assert(*n > 0);
#endif

   if (*n <= 0)
   {
      strErr = "SuperLU: matrix order negative";
      return -1;
   }
   if (*n >= std::numeric_limits<int>::max())
   {
      strErr = "SuperLU: matrix order larger than int capacity";
      return -1;
   }

   SuperLUSystem* sP = new (SuperLUSystem);

/* Default options (Ref: SuperLU-3.0/SRC/util.c)
   options->Fact = DOFACT;
   options->Equil = YES;
   options->ColPerm = COLAMD;
   options->DiagPivotThresh = 1.0;
   options->Trans = NOTRANS;
   options->IterRefine = NOREFINE;
   options->SymmetricMode = NO;
   options->PivotGrowth = NO;
   options->ConditionNumber = NO;
   options->PrintStat = YES;
*/
   set_default_options(&sP->opts);
   sP->opts.equil = NO;             //
   sP->opts.diag_pivot_thresh = 0.0;  // no pivoting
   sP->opts.trans = (*ccs != 0) ? NOTRANS : TRANS;

   sP->drop_tol   = 0.0;            // drop tolerance
   sP->panel_size = sp_ienv(1);     // panel size
   sP->relax      = sp_ienv(2);     // no of columns in a relaxed snodes
   sP->lwork      = 0;              // allocate dynamically

   sP->n          = static_cast<int>(*n);
   sP->etree      = intMalloc(sP->n);
   sP->perm_r     = intMalloc(sP->n);
   sP->perm_c     = intMalloc(sP->n);

   *hndl = reinterpret_cast<hndl_t>(sP);

#ifdef MODE_DEBUG
   assert(reinterpret_cast<SuperLUSystem*>(*hndl) != NULL);
#endif
   return 0;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_RESET(hndl_t* hndl)
{
#ifdef MODE_DEBUG
   assert(reinterpret_cast<SuperLUSystem*>(*hndl) != NULL);
#endif

   SuperLUSystem* sP = reinterpret_cast<SuperLUSystem*>(*hndl);

   SUPERLU_FREE (sP->perm_r);
   SUPERLU_FREE (sP->perm_c);
   SUPERLU_FREE (sP->etree);

   if (sP->A.Store)  Destroy_SuperMatrix_Store(&(sP->A));
   if (sP->AC.Store) Destroy_CompCol_Permuted (&(sP->AC));
   if (sP->L.Store)  Destroy_SuperNode_Matrix (&(sP->L));
   if (sP->U.Store)  Destroy_CompCol_Matrix   (&(sP->U));

   const bool sameIntSize = (sizeof(fint_t) == sizeof(int));
   c_slum_deleteTbl<sameIntSize>(sP->colptr);
   c_slum_deleteTbl<sameIntSize>(sP->rowind);

   delete sP;

   *hndl = 0;
   return 0;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_FACT (hndl_t *hndl,
                                                           fint_t *n,
                                                           fint_t *nnz,
                                                           double *values,
                                                           fint_t *rowind,
                                                           fint_t *colptr)
{
#ifdef MODE_DEBUG
   assert(reinterpret_cast<SuperLUSystem*>(*hndl) != NULL);
   assert(reinterpret_cast<SuperLUSystem*>(*hndl)->n == *n);
#endif

   if (*n <= 0)
   {
      strErr = "SuperLU: matrix nnz negative";
      return -1;
   }
   if (*n >= std::numeric_limits<int>::max())
   {
      strErr = "SuperLU: matrix nnz larger than int capacity";
      return -1;
   }

   SuperLUSystem* sP = reinterpret_cast<SuperLUSystem*>(*hndl);
   GlobalLU_t glu;

   // ---  Adjust to 0-based indexing
   for (fint_t i = 0; i < *nnz; ++i) --rowind[i];
   for (fint_t i = 0; i <=  *n; ++i) --colptr[i];

   // ---  Initialise la matrice
   c_slum_initmat(sP, n, nnz, values, rowind, colptr);

   // ---  Initialize stats
   Gstat_t stat;
   StatInit(&stat);

   // ---  Factorize
   int info = 0;
   dgstrf(&(sP->opts),
          &(sP->AC),
          sP->relax,
          sP->panel_size,
          sP->etree,
          NULL,
          sP->lwork,
          sP->perm_c,
          sP->perm_r,
          &(sP->L),
          &(sP->U),
          &glu,
          &stat,
          &info);

   // ---  Debug print out
//   dPrint_CompCol_Matrix("A", &(sP->A));
//   dPrint_CompCol_Matrix("U", &(sP->U));
//   dPrint_SuperNode_Matrix("L", &(sP->L));

   // ---  Reset stats
   StatFree(&stat);

   // ---  Restore to 1-based indexing
   for (fint_t i = 0; i < *nnz; ++i) ++rowind[i];
   for (fint_t i = 0; i <=  *n; ++i) ++colptr[i];

   // ---  Flag factorize done
   if (info == 0)
   {
      sP->opts.fact = SamePattern;
   }

   return info;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_SOLVE (hndl_t *hndl,
                                                            fint_t *n,     /* Matrix order */
                                                            fint_t *nrhs,  /* Number of right-hand sides */
                                                            double *b)     /* Right-hand side */
{
#ifdef MODE_DEBUG
   assert(reinterpret_cast<SuperLUSystem*>(*hndl) != NULL);
   assert(reinterpret_cast<SuperLUSystem*>(*hndl)->n == *n);
   assert(reinterpret_cast<SuperLUSystem*>(*hndl)->opts.fact != DOFACT);
#endif

   SuperLUSystem* sP = reinterpret_cast<SuperLUSystem*>(*hndl);

   // --- Create Dense SuperLU Matrix for the RHS
   SuperMatrix B;
   dCreate_Dense_Matrix (&B,
                         static_cast<int>(*n),
                         static_cast<int>(*nrhs),
                         b,
                         static_cast<int>(*n),
                         SLU_DN,
                         SLU_D,
                         SLU_GE);

   // ---  Initialize stats
   SuperLUStat_t stat;
   StatInit(&stat);

   // ---  Solve
   int info = 0;
   dgstrs(sP->opts.Trans,
          &(sP->L),
          &(sP->U),
          sP->perm_c,
          sP->perm_r,
          &B,
          &stat,
          &info);

   // ---  Reset stats
   StatFree(&stat);

   // ---  Reclaim memory
   Destroy_SuperMatrix_Store(&B);

   return info;
}

#if defined(_MSC_VER)
#  pragma warning( pop )
#endif

