C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C
C Description :
C     Fournit les routines pour obtenir les point de Gauss
C     et les poids associés pour un élément P12L.
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     Le bloc de données NI_SSP3P3 initialise les point de Gauss et les
C     poids associés pour l'intégration sur un P12. C'est une combinaison
C     d'une intégration à 1 point sur chaque sous-triangle pour l'horizontal,
C     et à 2 point sur la verticale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Dhatt, Touzot, Lefrançois, p 352,367.
C     La coordonnée est obtenue par l'expression suivante
C        WRITE(*,'(1PE25.17)') 1.0D0 / SQRT(3.0D0)
C     la vraie val étant:
C         0.5 7735 0269 1896 2576 450914878050195745564760)
C************************************************************************
      BLOCK DATA NI_SSP3P3

      IMPLICIT NONE

      INCLUDE 'ni_ssp3p3.fc'
      INCLUDE 'eacnst.fi'

   !   REAL*8 R_1_8
      REAL*8 R_1_SQRT3

      REAL*8 R_1_6
      REAL*8 R_4_6
      REAL*8 R_1_3


      REAL*8 R_1_10
      REAL*8 R_2_10

      REAL*8 R_3_10
      REAL*8 R_4_10
      REAL*8 R_6_10

      REAL*8 R_8_10

      REAL*8 R_27_96
      REAL*8 R_25_96


      PARAMETER (R_1_6 = 1.6666666666666666667D-01,
     &           R_1_3 = 3.3333333333333333333D-01,
     &           R_4_6 = 6.6666666666666666667D-01,
     &           R_1_10 = 1.000000000000000000d-01,
     &           R_2_10 = 2.000000000000000000d-01,
     &           R_3_10 = 3.0000000000000000000D-01,
     &           R_4_10 = 4.0000000000000000000D-01,
     &           R_6_10 = 6.0000000000000000000D-01,
     &           R_8_10 = 8.0000000000000000000D-01,
     &           R_27_96 = 27.0D0 / 384.0D0,  ! (27/96) * 2
     &           R_25_96 = 25.0D0 / 384.0D0,  ! (25/96) * 2
     & !          R_1_8 = 1.0D-00 / 8.0D-00,
     &           R_1_SQRT3 = 5.7735 0269 1896 2584 2D-01 )
C------------------------------------------------------------------------

      DATA NI_SSP3P3_CPG
     &     /
     &     R_1_6, R_1_6, R_1_SQRT3, !
     &     R_1_10, R_1_10, R_1_SQRT3,
     &     R_3_10, R_1_10, R_1_SQRT3,
     &     R_1_10, R_3_10, R_1_SQRT3,
     &     R_1_6,  R_4_6, R_1_SQRT3, !
     &     R_1_10, R_6_10, R_1_SQRT3,
     &     R_3_10, R_6_10, R_1_SQRT3,
     &     R_1_10, R_8_10, R_1_SQRT3,
     &     R_4_6, R_1_6, R_1_SQRT3, !
     &     R_6_10, R_1_10, R_1_SQRT3,
     &     R_8_10, R_1_10, R_1_SQRT3,
     &     R_6_10, R_3_10, R_1_SQRT3,
     &     R_1_3, R_1_3, R_1_SQRT3, !?
     &     R_4_10, R_4_10, R_1_SQRT3, !?
     &     R_2_10, R_4_10, R_1_SQRT3,!?
     &     R_4_10, R_2_10, R_1_SQRT3,!?
     &     R_1_6, R_1_6, -R_1_SQRT3,
     &     R_1_10, R_1_10, -R_1_SQRT3,
     &     R_3_10, R_1_10, -R_1_SQRT3,
     &     R_1_10, R_3_10, -R_1_SQRT3,
     &     R_1_6,  R_4_6, -R_1_SQRT3, !
     &     R_1_10, R_6_10, -R_1_SQRT3,
     &     R_3_10, R_6_10, -R_1_SQRT3,
     &     R_1_10, R_8_10, -R_1_SQRT3,
     &     R_4_6, R_1_6, -R_1_SQRT3, !
     &     R_6_10, R_1_10, -R_1_SQRT3,
     &     R_8_10, R_1_10, -R_1_SQRT3,
     &     R_6_10, R_3_10, -R_1_SQRT3,
     &     R_1_3, R_1_3, -R_1_SQRT3, !?
     &     R_4_10, R_4_10, -R_1_SQRT3, !?
     &     R_2_10, R_4_10, -R_1_SQRT3,!?
     &     R_4_10, R_2_10, -R_1_SQRT3!?
     &     /

      DATA NI_SSP3P3_WPG
     &      /
     &      -R_27_96,
     &       R_25_96,
     &       R_25_96,
     &       R_25_96,
     &      -R_27_96,
     &       R_25_96,
     &       R_25_96,
     &       R_25_96,
     &      -R_27_96,
     &       R_25_96,
     &       R_25_96,
     &       R_25_96,
     &      -R_27_96,
     &       R_25_96,
     &       R_25_96,
     &       R_25_96,
     &      -R_27_96,
     &       R_25_96,
     &       R_25_96,
     &       R_25_96,
     &      -R_27_96,
     &       R_25_96,
     &       R_25_96,
     &       R_25_96,
     &      -R_27_96,
     &       R_25_96,
     &       R_25_96,
     &       R_25_96,
     &      -R_27_96,
     &       R_25_96,
     &       R_25_96,
     &       R_25_96
     &      /

      END

C************************************************************************
C Sommaire: NI_SSP3P3_REQNPG
C
C Description:
C     La fonction NI_SSP3P3_REQNPTS retourne le nombre de points de Gauss.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NI_SSP3P3_REQNPG()
CDEC$ATTRIBUTES DLLEXPORT :: NI_SSP3P3_REQNPG

      IMPLICIT NONE

      INCLUDE 'ni_ssp3p3.fi'
      INCLUDE 'ni_ssp3p3.fc'
C------------------------------------------------------------------------

      NI_SSP3P3_REQNPG = NI_SSP3P3_NPG
      RETURN
      END

C************************************************************************
C Sommaire: NI_SSP3P3_REQCPG
C
C Description:
C     La fonction NI_SSP3P3_REQCPG(...) retourne les coordonnées du point
C     d'intégration d'indice "I".
C
C Entrée:
C     I    : indice du point pour lequel on souhaite obtenir la coordonnée
C     NDIM : nombre de dimensions attendues pour la coordonnée
C
C Sortie:
C     VCPG : Coordonnée du point sous la forme (xi, eta, zeta)
C
C Notes:
C************************************************************************
      FUNCTION  NI_SSP3P3_REQCPG(I, NDIM, VCPG)
CDEC$ATTRIBUTES DLLEXPORT :: NI_SSP3P3_REQCPG

      IMPLICIT NONE

      INTEGER I
      INTEGER NDIM
      REAL*8  VCPG(NDIM)
      REAL*8  TEST

      INCLUDE 'ni_ssp3p3.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ni_ssp3p3.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GE. 1 .AND. I .LE. NI_SSP3P3_NPG)
D     CALL ERR_PRE(NDIM .GE. NI_SSP3P3_NDIM)
C------------------------------------------------------------------------

C      test = NI_SSP3P3_CPG(3,I) - 1.0D00/sqrt(3.0D00)
      VCPG(1) = NI_SSP3P3_CPG(1,I)
      VCPG(2) = NI_SSP3P3_CPG(2,I)
      VCPG(3) = NI_SSP3P3_CPG(3,I)

      NI_SSP3P3_REQCPG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NI_SSP3P3_REQWPG(W,I)
C
C Description:
C     La fonction NI_SSP3P3_REQWPG(..._ retourne le poids du point I
C     pour effectuer l'intégration numérique.
C
C Entrée:
C     I : indice du point d'intégration concerné
C
C Sortie:
C     W : poids du point d'intégration I
C
C Notes:
C************************************************************************
      FUNCTION NI_SSP3P3_REQWPG(I, W)
CDEC$ATTRIBUTES DLLEXPORT :: NI_SSP3P3_REQWPG

      IMPLICIT NONE

      INTEGER I
      REAL*8  W

      INCLUDE 'ni_ssp3p3.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ni_ssp3p3.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GE. 1 .AND. I .LE. NI_SSP3P3_NPG)
C------------------------------------------------------------------------

      W = NI_SSP3P3_WPG(I)

      NI_SSP3P3_REQWPG = ERR_TYP()
      RETURN
      END
