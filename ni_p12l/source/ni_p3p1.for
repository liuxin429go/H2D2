C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C
C Description :
C     Fournit les routines pour obtenir les point de Gauss
C     et les poids associés pour un élément P12.
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     Le bloc de données NI_P3P1 initialise les point de Gauss et les
C     poids associés pour l'intégration sur un P12L. C'est une combinaison
C     d'une intégration à 4 points sur un triangle pour l'horizontal,
C     et à 1 point sur la verticale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Dhatt, Touzot, Lefrançois, p 352,367.
C************************************************************************
      BLOCK DATA NI_P3P1

      IMPLICIT NONE

      INCLUDE 'ni_p3p1.fc'

      REAL*8 ZERO
      REAL*8 R_1_3
      REAL*8 R_1_5
      REAL*8 R_3_5
      REAL*8 R_27_48_M
      REAL*8 R_25_48

      PARAMETER (ZERO  = 0.0000000000000000000D+00,
     &           R_1_3 = 3.3333333333333333333D-01,
     &           R_1_5 = 2.0000000000000000000D-01,
     &           R_3_5 = 6.0000000000000000000D-01,
     &           R_27_48_M = - 27.0D0 / 48.0D0,  ! (27/96) * 2
     &           R_25_48   =   25.0D0 / 48.0D0)  ! (25/96) * 2
C------------------------------------------------------------------------

      DATA NI_P3P1_CPG
     &     /
     &     R_1_3, R_1_3, ZERO,
     &     R_1_5, R_1_5, ZERO,
     &     R_3_5, R_1_5, ZERO,
     &     R_1_5, R_3_5, ZERO
     &     /

      DATA NI_P3P1_WPG
     &      /
     &       R_27_48_M,
     &       R_25_48,
     &       R_25_48,
     &       R_25_48
     &      /

      END

C************************************************************************
C Sommaire: NI_P3P1_REQNPG
C
C Description:
C     La fonction NI_P3P1_REQNPTS retourne le nombre de points de Gauss.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NI_P3P1_REQNPG()
CDEC$ATTRIBUTES DLLEXPORT :: NI_P3P1_REQNPG

      IMPLICIT NONE

      INCLUDE 'ni_p3p1.fi'
      INCLUDE 'ni_p3p1.fc'
C------------------------------------------------------------------------

      NI_P3P1_REQNPG = NI_P3P1_NPG
      RETURN
      END

C************************************************************************
C Sommaire: NI_P3P1_REQCPG
C
C Description:
C     La fonction NI_P3P1_REQCPG(...) retourne les coordonnées du point
C     d'intégration d'indice "I".
C
C Entrée:
C     I    : indice du point pour lequel on souhaite obtenir la coordonnée
C     NDIM : nombre de dimensions attendues pour la coordonnée
C
C Sortie:
C     VCPG : Coordonnée du point sous la forme (xi, eta, zeta)
C
C
C Notes:
C************************************************************************
      FUNCTION  NI_P3P1_REQCPG(I, NDIM, VCPG)
CDEC$ATTRIBUTES DLLEXPORT :: NI_P3P1_REQCPG

      IMPLICIT NONE

      INTEGER I
      INTEGER NDIM
      REAL*8  VCPG(NDIM)

      INCLUDE 'ni_p3p1.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ni_p3p1.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GE. 1 .AND. I .LE. NI_P3P1_NPG)
D     CALL ERR_PRE(NDIM .GE. NI_P3P1_NDIM)
C------------------------------------------------------------------------

      VCPG(1) = NI_P3P1_CPG(1,I)
      VCPG(2) = NI_P3P1_CPG(2,I)
      VCPG(3) = NI_P3P1_CPG(3,I)

      NI_P3P1_REQCPG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NI_P3P1_REQWPG(W,I)
C
C Description:
C     La fonction NI_P3P1_REQWPG retourne le poids du point I
C     pour effectuer l'intégration numérique.
C
C Entrée:
C     I : indice du point d'intégration concerné
C
C Sortie:
C     W : poids du point d'intégration I
C
C Notes:
C************************************************************************
      FUNCTION NI_P3P1_REQWPG(I, W)
CDEC$ATTRIBUTES DLLEXPORT :: NI_P3P1_REQWPG

      IMPLICIT NONE

      INTEGER I
      REAL*8  W

      INCLUDE 'ni_p3p1.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ni_p3p1.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GE. 1 .AND. I .LE. NI_P3P1_NPG)
C------------------------------------------------------------------------

      W = NI_P3P1_WPG(I)

      NI_P3P1_REQWPG = ERR_TYP()
      RETURN
      END

