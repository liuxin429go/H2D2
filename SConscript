# -*- coding: UTF-8 -*-
Import('env')

def conf_has(key):
    """
    Utilitary function
    Return the logical value associated with key, 
    and False if key does not exist.
    """
    return env['MUC_GLBDEFS'].get(key, False)
    
# ---  Read configuration from file "h2d2.conf"
with open('h2d2.conf', 'rt') as ifs:
    for line in ifs:
        if line.strip()[0] == '#': continue
        var, val = [ x.strip() for x in line.split('=', 2) ]
        env['MUC_GLBDEFS'][var] = eval(val)

# --- H2D2 core
main = [ 'h2d2' ]
tool = [ ]
if conf_has('H2D2_HAS_TOOL_GBP'):
    tool.extend( [ 'b_gbp', 'tool_gbp' ] )
h2d2 = [ 'h2d2_algo', 'h2d2_gpu', 'h2d2_elem', 'h2d2_krnl', 'h2d2_post', 'h2d2_svc', 'h2d2_umef', 'libh2d2' ]
lmgo = [ 'lmgo_l2', 'lmgo_l3l', 'lmgo_t3', 'lmgo_t6l']


# --- Renumbering, etc
numr = [ ]
numr.extend( [ 'b_ParMetis', 'numr_pmetis' ] )
numr.extend( [ 'b_Scotch',   'numr_scotch' ] )
numr.extend( [ 'numr_front' ] )

# --- Solver, etc
algo = [ ]
if (env['MUC_TOOL'] in ['intel', 'itlX86', 'itlX64', 'itlI64', 'cmc', 'cmcitl']):
    algo.extend( [ 'slvr_mkl' ] )
algo.extend( [ 'b_MUMPS', 'slvr_mumps' ] )
algo.extend( [ 'b_SuperLU_SP', 'slvr_slus' ] )
if conf_has('H2D2_HAS_SOLVER_SUPERLU_DS'):
    algo.extend( [ 'b_SuperLU_DS', 'slvr_slud' ] )
if conf_has('H2D2_HAS_SOLVER_PETSC'):
    algo.extend( [ 'slvr_ptsc' ] )

# ---  Elements
elements = []
elements.extend( [ 'cd2d_bse', 'cd2d_b1l', 'cd2d_ccn', 'cd2d_clf', 'cd2d_age', 'cd2d_mes', 'cd2d_tmp' ] )
elements.extend( [ 'sv2d' ] )
if conf_has('H2D2_HAS_ELEMENT_NSMACRO3D'):
    lmgo.extend( [ 'lmgo_p12l', 'ni_t6l', 'ni_p12l' ] )
    elements.extend( [ 'ns_macro3d' ] )
if conf_has('H2D2_HAS_ELEMENT_SED2D'):
    elements.extend( [ 'sed2d' ] )

# ---  Adaptation
remesh = [ ]
if conf_has('H2D2_HAS_REMESH'):
   remesh.extend( [ 'b_Remesh', 'algo_remesh', '../H2D2-Remesh' ] )

# --- Submit
env.SConscript(dirs = h2d2 + tool + lmgo + elements + numr + algo + remesh)

"""
Pour le link static, il faut que h2d2 soit en dernier
pour avoir la liste complète des LIBS et LIBPATH
qui est récoltée au passage des autres modules
"""
env.SConscript(dirs = main)
