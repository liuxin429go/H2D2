C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_TR_CHRN_XEQCTR
C     SUBROUTINE IC_TR_CHRN_XEQMTH
C     INTEGER IC_TR_CHRN_OPBDOT
C     CHARACTER*(32) IC_TR_CHRN_REQCLS
C     INTEGER IC_TR_CHRN_REQHDL
C   Private:
C     SUBROUTINE IC_TR_CHRN_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TR_CHRN_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TR_CHRN_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'trchrn_ic.fi'
      INCLUDE 'trchrn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      
      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_TR_CHRN_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     CONTROLE LES PARAM
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
         HOBJ = IC_TR_CHRN_REQHDL()
C        <comment>Return value: Handle on the timer system</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>timer</b> constructs an object and returns a handle on this object.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_TR_CHRN_AID()

9999  CONTINUE
      IC_TR_CHRN_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TR_CHRN_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TR_CHRN_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'trchrn_ic.fi'
C------------------------------------------------------------------------

      IC_TR_CHRN_XEQMTH = IC_TR_CHRN_OPBDOT(HOBJ, IMTH, IPRM)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TR_CHRN_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TR_CHRN_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'trchrn_ic.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fi'

      REAL*8       RVAL
      INTEGER      IERR
      INTEGER      IVAL
      CHARACTER*64 PROP, SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_TR_CHRN_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>add_zone</b> adds a zone to the timer system</comment>
      IF (IMTH .EQ. 'add_zone') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Name of the zone to add</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = TR_CHRN_AJTZONE(SVAL)

C     <comment>The method <b>del_zone</b> deletes a timer zone.</comment>
      ELSEIF (IMTH .EQ. 'del_zone') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Name of the zone to delete</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = TR_CHRN_EFFZONE(SVAL)

C     <comment>The method <b>start_zone</b> starts the timer in a zone.</comment>
      ELSEIF (IMTH .EQ. 'start_zone') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Name of the zone to start</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         CALL TR_CHRN_START(SVAL)

C     <comment>The method <b>stop_zone</b> stops the timer in a zone.</comment>
      ELSEIF (IMTH .EQ. 'stop_zone') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Name of the zone to stop</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         CALL TR_CHRN_STOP(SVAL)

C     <comment>The method <b>get_time_zone</b> returns the time spent in a zone.</comment>
      ELSEIF (IMTH .EQ. 'get_time_zone') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Name of the zone</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = TR_CHRN_REQTEMPS(SVAL, RVAL)
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C     <comment>
C     The method <b>log_zone</b> logs the actual status of the timer in a zone.
C     </comment>
      ELSEIF (IMTH .EQ. 'log_zone') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Name of the zone to log</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = TR_CHRN_LOGZONE(SVAL)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = TR_CHRN_LOGZONE('')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_TR_CHRN_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_TR_CHRN_AID()

9999  CONTINUE
      IC_TR_CHRN_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TR_CHRN_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TR_CHRN_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'trchrn_ic.fi'
      INCLUDE 'trchrn.fi'
C-------------------------------------------------------------------------

C<comment>
C The class <b>timer</b> represents the timer system used to time various sections
C of H2D2. The timer define zones which are hierarchic in a tree like manner:
C <ul>
C   <li>h2d2</li>
C   <li>h2d2.grid</li>
C   <li>h2d2.grid.coor</li>
C </ul>
C Active timer zones are:
C <ul>
C    <li>h2d2</li>
C    <ul>
C       <li>h2d2.grid</li>
C       <ul>
C          <li>h2d2.grid.color</li>
C          <li>h2d2.grid.elem</li>
C          <li>h2d2.grid.part</li>
C          <li>h2d2.grid.remesh</li>
C          <li>h2d2.grid.renum</li>
C       </ul>
C       <li>h2d2.io</li>
C       <ul>
C          <li>h2d2.io.cond</li>
C          <ul>
C             <li>h2d2.io.cond.read</li>
C          </ul>
C          <li>h2d2.io.dump</li>
C          <li>h2d2.io.elem</li>
C          <ul>
C             <li>h2d2.io.elem.read</li>
C             <li>h2d2.io.elem.write</li>
C          </ul>
C          <li>h2d2.io.limt</li>
C          <ul>
C             <li>h2d2.io.limt.read</li>
C             <li>h2d2.io.limt.write</li>
C          </ul>
C          <li>h2d2.io.numr</li>
C          <ul>
C             <li>h2d2.io.numr.read</li>
C             <li>h2d2.io.numr.write</li>
C          </ul>
C          <li>h2d2.io.prno</li>
C          <ul>
C             <li>h2d2.io.prno.ascii</li>
C             <ul>
C                <li>h2d2.io.prno.ascii.read</li>
C                <li>h2d2.io.prno.ascii.write</li>
C             </ul>
C             <li>h2d2.io.prno.binary</li>
C             <ul>
C                <li>h2d2.io.prno.binary.read</li>
C                <li>h2d2.io.prno.binary.write</li>
C             </ul>
C             <li>h2d2.io.prno.prec8</li>
C             <ul>
C                <li>h2d2.io.prno.prec8.read</li>
C                <li>h2d2.io.prno.prec8.write</li>
C             </ul>
C             <li>h2d2.io.prno.read</li>
C             <li>h2d2.io.prno.write</li>
C          </ul>
C       </ul>
C       <li>h2d2.reso</li>
C       <ul>
C          <li>h2d2.reso.mat</li>
C          <ul>
C             <li>h2d2.reso.mat.assemblage</li>
C             <li>h2d2.reso.mat.factorisation</li>
C             <li>h2d2.reso.mat.resolution</li>
C          </ul>
C          <li>h2d2.reso.residu</li>
C       </ul>
C    </ul>
C </ul>
C</comment>
      IC_TR_CHRN_REQCLS = 'timer'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TR_CHRN_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TR_CHRN_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'trchrn_ic.fi'
C-------------------------------------------------------------------------

      IC_TR_CHRN_REQHDL = 1999005000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_TR_CHRN_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('trchrn_ic.hlp')

      RETURN
      END
