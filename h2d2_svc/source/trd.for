C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER TRD_ASGFIC
C     SUBROUTINE TRD_TRADUIS
C   Private:
C     INTEGER TRD_ASGUNFIC
C     LOGICAL TRD_TRADTOK
C
C************************************************************************

C************************************************************************
C Sommaire: Block d'initialisation.
C
C Description:
C     La fonction <code>BLOCK DATA TRD_000()</code> initialise l'adresse
C     du map à 0.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA TRD_000

      INCLUDE 'trd.fc'

      DATA TRD_XMAP /0/

      END

C************************************************************************
C Sommaire: Assigne un fichier de traduction
C
C Description:
C     La fonction <code>TRD_ASGFIC()</code> assigne et ajoute un
C     fichier de traduction. Toutes les paires "Token=Valeur"
C     contenues dans le fichier seront ajoutées à la liste des
C     tokens connus et pouvant être traduis.
C
C Entrée:
C     CHARACTER*(*) NOMFIC       Nom du fichier à ajouter
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRD_ASGFIC(NOMFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRD_ASGFIC
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) NOMFIC

      INCLUDE 'trd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trd.fc'

      INTEGER IERR
      CHARACTER*(1024) LIGNE

      INTEGER MR
C-----------------------------------------------------------------------

      IERR = ERR_OK

      MR = -1
      IF (NOMFIC(1:1) .EQ. '@') THEN
         MR = IO_UTIL_FREEUNIT()
         OPEN(UNIT  = MR,
     &        FILE  = NOMFIC(2:SP_STRN_LEN(NOMFIC)),
     &        STATUS= 'OLD',
     &        ERR   = 9900)
100      CONTINUE
            READ(MR,'(A)', ERR = 9901, END = 199) LIGNE
            IERR = TRD_ASGUNFIC(LIGNE(1:SP_STRN_LEN(LIGNE)))
         IF (ERR_GOOD()) GOTO 100
199      CONTINUE
      ELSE
         IERR = TRD_ASGUNFIC(NOMFIC)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                        NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_FICHIER',': ',
     &                        NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (NOMFIC(1:1) .EQ. '@') CLOSE(MR)

      TRD_ASGFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne un fichier de traduction
C
C Description:
C     La fonction privée <code>TRD_ASGUNFIC()</code> assigne et ajoute un
C     fichier de traduction. Toutes les paires "Token=Valeur"
C     contenues dans le fichier seront ajoutées à la liste des
C     tokens connus et pouvant être traduis.
C
C Entrée:
C     CHARACTER*(*) NOMFIC       Nom du fichier à ajouter
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRD_ASGUNFIC (NOMFIC)

      IMPLICIT NONE

      CHARACTER*(*) NOMFIC

      INCLUDE 'trd.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trd.fc'

      INTEGER IERR
      INTEGER IDX
      CHARACTER*(1024) LIGNE
      CHARACTER*(256)  VAR
      CHARACTER*(256)  VAL

      INTEGER MR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     AU BESOIN CRÉE LA MAP
      MR = 0
      IF (TRD_XMAP .EQ. 0) TRD_XMAP = C_MAP_CTR()
      IF (TRD_XMAP .EQ. 0) GOTO 9900

C---     ITÈRE POUR STOKER L'INFORMATION
      MR = IO_UTIL_FREEUNIT()
      OPEN (UNIT = MR,
     &      FILE = NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &      STATUS = 'OLD',
     &      ERR  = 9901)
200   CONTINUE
         READ(MR, '(A)', END=299, ERR=9902) LIGNE
         CALL SP_STRN_TRM(LIGNE)
         IDX = INDEX(LIGNE, '=')
         IF (IDX .LE. 1 .OR. IDX .GE. SP_STRN_LEN(LIGNE)) GOTO 200

         VAR = LIGNE(1:IDX-1)
         VAL = LIGNE(IDX+1:SP_STRN_LEN(LIGNE))
         CALL SP_STRN_UCS(VAR)
         CALL SP_STRN_TRM(VAR)
         CALL SP_STRN_TRM(VAL)

         IERR = C_MAP_ASGVAL(TRD_XMAP,
     &                       VAR(1:SP_STRN_LEN(VAR)),
     &                       VAL(1:SP_STRN_LEN(VAL)))
         IF (IERR .NE. 0) GOTO 9903
      GOTO 200
299   CONTINUE

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_CONSTRUCTION_MAP'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                       NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(3A)') 'ERR_ECRITURE_FICHIER',': ',
     &                       NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(3A)') 'ERR_AJOUT_CLEF_MAP',': ',
     &                       VAR(1:SP_STRN_LEN(VAR))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (MR. NE. 0) CLOSE(MR)

      TRD_ASGUNFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Traduis une chaîne
C
C Description:
C     La fonction <code>TRD_TRADUIS(...)</code) traduis la chaîne.
C     Elle découpe la chaîne en tokens et tente de traduire chacun
C     d'eux.
C     <p>
C     Les séquences '#<nn>#' sont interprétées comme une indentation
C     de 'nn' positions.
C
C Entrée:
C     CHARACTER*(*) STR       La chaîne à traduire
C
C Sortie:
C     CHARACTER*(*) STR       La chaîne traduite
C
C Notes:
C************************************************************************
      SUBROUTINE TRD_TRADUIS(STR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRD_TRADUIS
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) STR

      INCLUDE 'spstrn.fi'
      INCLUDE 'trd.fc'

      INTEGER I
      INTEGER IERR
      INTEGER LSTR
      INTEGER LTOK
      INTEGER LRES
      INTEGER NBLK
      LOGICAL ADDC
      LOGICAL IQUOT
      LOGICAL ITRAD
      CHARACTER*(1024) RES
      CHARACTER*( 256) TOK
      CHARACTER C
C-----------------------------------------------------------------------

      RES  = ' '
      TOK  = ' '
      LTOK = 0
      LRES = 0
      IQUOT= .FALSE.
      ITRAD= .FALSE.
      LSTR = SP_STRN_LEN(STR)

      I = 1
100   IF (I .GT. LSTR) GOTO 199
         C = STR(I:I)

         IF (IQUOT) THEN
            ADDC = .TRUE.
         ELSEIF (SP_STRN_AZS(C)) THEN
            ADDC = .FALSE.
            IF (LTOK .EQ. 0) THEN
               TOK = C
            ELSE
               TOK = TOK(1:LTOK) // C
            ENDIF
            LTOK = LTOK + 1
         ELSEIF (LTOK .GT. 0 .AND. SP_STRN_INT(C)) THEN
            ADDC = .FALSE.
            TOK = TOK(1:LTOK) // C
            LTOK = LTOK + 1
         ELSEIF (C .EQ. '''') THEN
            ADDC = .TRUE.
            IQUOT = .NOT. IQUOT
         ELSE
            ADDC = .TRUE.
            IF (LTOK .GT. 0) THEN
               ITRAD = TRD_TRADTOK(TOK)
               LTOK = SP_STRN_LEN(TOK)
            ENDIF
         ENDIF

         IF (ADDC) THEN
            IF (LTOK .GT. 0) THEN
               IF (LRES .EQ. 0) THEN
                  RES  = TOK
               ELSE
                  RES  = RES(1:LRES) // TOK
               ENDIF
               LRES = LRES + LTOK
               LTOK = 0
               TOK  = ' '
            ENDIF

            NBLK = -1
            IF (.NOT. IQUOT .AND. STR(I:I+1) .EQ. '#<') THEN
               IERR = SP_STRN_TKI(STR(I+2:LSTR), '>#', 1, NBLK)
               IF (IERR .EQ. 0 .AND. NBLK .GT. 0) THEN
                  I = I + INDEX(STR(I+2:LSTR), '>#') + 2
                  IF (NBLK .GT. LRES) LRES = NBLK
               ENDIF
            ENDIF
            IF (NBLK .LE. 0) THEN
               IF (LRES .EQ. 0) THEN
                  RES = C
                  LRES = 1
               ELSE
                  RES = RES(1:LRES) // C
                  LRES = LRES + 1
               ENDIF
            ENDIF
         ENDIF

         I = I + 1
      GOTO 100
199   CONTINUE

      IF (SP_STRN_LEN(TOK) .GT. 0) THEN
         ITRAD = TRD_TRADTOK(TOK)
         IF (LRES .EQ. 0) THEN
            RES = TOK
         ELSE
            RES = RES(1:LRES) // TOK
         ENDIF
      END IF

      STR = RES
      RETURN
      END

C************************************************************************
C Sommaire: Traduis un token.
C
C Description:
C     La fonction privée <code>TRD_TRADTOK(...)</code> traduis le token
C     qui lui est passé. Si le token n'est pas trouvé dans le liste
C     des tokens connus, il est retourné inchangé.
C
C Entrée:
C     CHARACTER*(*) TOK       Le token à traduire
C
C Sortie:
C     CHARACTER*(*) TOK       Le token traduis
C
C Notes:
C************************************************************************
      FUNCTION TRD_TRADTOK(TOK)

      IMPLICIT NONE

      CHARACTER*(*) TOK

      INCLUDE 'c_ds.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trd.fc'

      INTEGER IERR
      INTEGER LSTR
      LOGICAL ESTTRAD
      CHARACTER*256 STR
C-----------------------------------------------------------------------

      ESTTRAD = .FALSE.
      IF (TRD_XMAP .NE. 0) THEN
         IERR = C_MAP_REQVAL(TRD_XMAP, TOK(1:SP_STRN_LEN(TOK)), STR)
         IF (IERR .EQ. 0) THEN
            LSTR = MIN(SP_STRN_LEN(STR), LEN(TOK))
            TOK = STR(1:LSTR)
            ESTTRAD = .TRUE.
         ENDIF
      ENDIF

      TRD_TRADTOK = ESTTRAD
      RETURN
      END
