C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Subroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrées:
C
C Sorties:
C
C Notes:
C
C************************************************************************
C$NOREFERENCE

C---     FONCTIONS PRIVEES
      INTEGER IC_ICMD_ERRVAL
      INTEGER IC_ICMD_ASGSMB
      INTEGER IC_ICMD_ASGSMBGLB
      INTEGER IC_ICMD_ASGSMBSMB
      INTEGER IC_ICMD_ASGSMBBRK
      INTEGER IC_ICMD_ASGSMBDOT
      INTEGER IC_ICMD_CMDPIL
      INTEGER IC_ICMD_EXT2INT
      INTEGER IC_ICMD_INT2EXT
      LOGICAL IC_ICMD_DOPARS         !
      LOGICAL IC_ICMD_ESTFNC         ! Test pour une fonction de module
      LOGICAL IC_ICMD_ESTFNI         ! Test pour une fonction de l'IC
      INTEGER IC_ICMD_PRSFIC
      INTEGER IC_ICMD_PRSPIL
      INTEGER IC_ICMD_PRXPIL
      INTEGER IC_ICMD_PRS999
      INTEGER IC_ICMD_REQHFNCFNC
      INTEGER IC_ICMD_REQHFNCFNI
      INTEGER IC_ICMD_REQHFNCCLS
      INTEGER IC_ICMD_REQHNDLCLS
      INTEGER IC_ICMD_REQSMBGLB
      INTEGER IC_ICMD_REQSMBSMB
      INTEGER IC_ICMD_REQSMBBRK
      INTEGER IC_ICMD_REQSMBDOT
      INTEGER IC_ICMD_UNWNDSTK
      INTEGER IC_ICMD_XEQFIC
      INTEGER IC_ICMD_XEQFNC
      INTEGER IC_ICMD_XEQFNCFNC
      INTEGER IC_ICMD_XEQFNCFNI
      INTEGER IC_ICMD_XEQFNCFNI_END
      INTEGER IC_ICMD_XEQFNCKWD
      INTEGER IC_ICMD_XEQFNCDOT
      INTEGER IC_ICMD_XEQKWDEL
      INTEGER IC_ICMD_XEQKWEXECF
      INTEGER IC_ICMD_XEQKWEVAL
      INTEGER IC_ICMD_XEQKWEXIST
      INTEGER IC_ICMD_XEQKWGOTO
      INTEGER IC_ICMD_XEQKWHELP
      INTEGER IC_ICMD_XEQKWIMPORT
      INTEGER IC_ICMD_XEQKWIF
      INTEGER IC_ICMD_XEQKWELSE
      INTEGER IC_ICMD_XEQKWELIF
      INTEGER IC_ICMD_XEQKWENDIF
      INTEGER IC_ICMD_XEQKWFNC
      INTEGER IC_ICMD_XEQKWENDFNC
      INTEGER IC_ICMD_XEQKWFOR
      INTEGER IC_ICMD_XEQKWPRINT
      INTEGER IC_ICMD_XEQKWINT
      INTEGER IC_ICMD_XEQKWFLOAT
      INTEGER IC_ICMD_XEQKWSTR
      INTEGER IC_ICMD_XEQKWSTOP
      INTEGER IC_ICMD_XEQKWWHILE
      INTEGER IC_ICMD_XEQKWENDWHILE
      INTEGER IC_ICMD_XEQOPB
      INTEGER IC_ICMD_XEQOPBARI
      INTEGER IC_ICMD_XEQOPBASG
      INTEGER IC_ICMD_XEQOPBBRK
      INTEGER IC_ICMD_XEQOPBCMP
      INTEGER IC_ICMD_XEQOPBDOT
      INTEGER IC_ICMD_XEQOPBLGL
      INTEGER IC_ICMD_XEQOPBPAR
      INTEGER IC_ICMD_XEQOPBVRG
      INTEGER IC_ICMD_XEQOPU
      INTEGER IC_ICMD_XEQOPULST
      INTEGER IC_ICMD_XEQOPUARI
      INTEGER IC_ICMD_XEQOPUKW
      INTEGER IC_ICMD_XEQPIL
      INTEGER IC_ICMD_XTRFNC
      INTEGER IC_ICMD_XTRLBL

      INTEGER IC_ICMD_PKL_OBJ
      INTEGER IC_ICMD_PKL_SLF
      INTEGER IC_ICMD_PKL_ONE
      INTEGER IC_ICMD_UPK_OBJ
      INTEGER IC_ICMD_UPK_SLF
      INTEGER IC_ICMD_UPK_ONE
      INTEGER IC_ICMD_UPK_RST

C------------------------------------------------------------------------

C---     ATTRIBUTS DE GESTION DES OBJETS
      INTEGER IC_ICMD_NOBJMAX
      INTEGER IC_ICMD_HBASE
      PARAMETER (IC_ICMD_NOBJMAX = 2)

C---     COMMON DE GESTION DES OBJETS
      COMMON /IC_ICMD_CGO/ IC_ICMD_HBASE

C------------------------------------------------------------------------

C---     ATTRIBUTS PRIVES
      INTEGER IC_ICMD_STS        ! Status : état d’exécution
      INTEGER IC_ICMD_CLSDIC     ! Map des classes
      INTEGER IC_ICMD_FNCDIC     ! Map des fonctions de modules
      INTEGER IC_ICMD_FNIDIC     ! Map des fonctions de l'IC
      INTEGER IC_ICMD_LBLDIC     ! Map des labels
      INTEGER IC_ICMD_GLBDIC     ! Map des symboles globaux
      INTEGER IC_ICMD_SMBDIC     ! Map des symboles
      INTEGER IC_ICMD_FILSTK     ! Pile des fichiers
      INTEGER IC_ICMD_SCTSTK     ! Pile des structures de contrôle
      INTEGER IC_ICMD_PRSSTK     ! Pile de parsing
      INTEGER IC_ICMD_XEQSTK     ! Pile d’exécution

C---     COMMON DES DONNEES
      COMMON /IC_ICMD_CDT/ IC_ICMD_STS   (IC_ICMD_NOBJMAX),
     &                     IC_ICMD_CLSDIC(IC_ICMD_NOBJMAX),
     &                     IC_ICMD_FNCDIC(IC_ICMD_NOBJMAX),
     &                     IC_ICMD_FNIDIC(IC_ICMD_NOBJMAX),
     &                     IC_ICMD_LBLDIC(IC_ICMD_NOBJMAX),
     &                     IC_ICMD_GLBDIC(IC_ICMD_NOBJMAX),
     &                     IC_ICMD_SMBDIC(IC_ICMD_NOBJMAX),
     &                     IC_ICMD_FILSTK(IC_ICMD_NOBJMAX),
     &                     IC_ICMD_SCTSTK(IC_ICMD_NOBJMAX),
     &                     IC_ICMD_PRSSTK(IC_ICMD_NOBJMAX),
     &                     IC_ICMD_XEQSTK(IC_ICMD_NOBJMAX)

C------------------------------------------------------------------------

C---     ATTRIBUTS STATIQUES
      INTEGER IC_ICMD_ADDKW         ! Ajoute les mots clefs
      LOGICAL IC_ICMD_ESTKW         ! Test pour un mot clef
      LOGICAL IC_ICMD_ESTCW         ! Test pour un mot de contrôle
      LOGICAL IC_ICMD_ESTIF         ! Test pour le mot de contrôle 'if'
      LOGICAL IC_ICMD_ESTELIF       ! Test pour le mot de contrôle 'elif'
      LOGICAL IC_ICMD_ESTWHILE      ! Test pour le mot de contrôle 'while'

      CHARACTER*(64) IC_ICMD_KW     ! Liste des mots clefs
      CHARACTER*(96) IC_ICMD_CW     ! Liste des mots de contrôle

      COMMON /IC_ICMD_STC/ IC_ICMD_KW, IC_ICMD_CW

C---     BIT-SET DES ETATS
      INTEGER IC_ICMD_STS_XEQ       ! Execute
      INTEGER IC_ICMD_STS_XEQFNC    ! Execute a function
      INTEGER IC_ICMD_STS_PRS       ! Parsing
      INTEGER IC_ICMD_STS_PRSFNC    ! Parsing a function
      INTEGER IC_ICMD_STS_SKPSTA    ! Skip rest of statement
      INTEGER IC_ICMD_STS_SKPSTR    ! Skip to end of structure
      INTEGER IC_ICMD_STS_SST       ! Single statement (eval)
      INTEGER IC_ICMD_STS_MST       ! Multiple statements (xeq file)

      PARAMETER (IC_ICMD_STS_XEQ    = 1)
      PARAMETER (IC_ICMD_STS_XEQFNC = 2)
      PARAMETER (IC_ICMD_STS_PRS    = 3)
      PARAMETER (IC_ICMD_STS_PRSFNC = 4)
      PARAMETER (IC_ICMD_STS_SKPSTA = 5)
      PARAMETER (IC_ICMD_STS_SKPSTR = 6)
      PARAMETER (IC_ICMD_STS_SST    = 7)
      PARAMETER (IC_ICMD_STS_MST    = 8)

C---     Jetons de la pile des structures de contrôles
      INTEGER IC_ICMD_SCT_FNC
      INTEGER IC_ICMD_SCT_IF
      INTEGER IC_ICMD_SCT_FOR
      INTEGER IC_ICMD_SCT_WHILE
      PARAMETER (IC_ICMD_SCT_FNC   = -1)
      PARAMETER (IC_ICMD_SCT_IF    = -2)
      PARAMETER (IC_ICMD_SCT_FOR   = -3)
      PARAMETER (IC_ICMD_SCT_WHILE = -4)

C$REFERENCE
