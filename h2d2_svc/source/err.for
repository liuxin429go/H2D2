C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     SUBROUTINE ERR_ASG
C     LOGICAL ERR_ESTMSG
C     SUBROUTINE ERR_PUSH
C     LOGICAL ERR_BAD
C     INTEGER ERR_OMP_RDC
C     INTEGER ERR_OMP_END
C     INTEGER ERR_TYP
C     SUBROUTINE ERR_PST
C     CHARACTER*(256) ERR_MSG
C     INTEGER ERR_DUMMY
C     SUBROUTINE ERR_PRE
C     SUBROUTINE ERR_ASR
C     SUBROUTINE ERR_AJT
C     SUBROUTINE ERR_POP
C     SUBROUTINE ERR_RESET
C     INTEGER ERR_NBR
C     LOGICAL ERR_GOOD
C   Private:
C     SUBROUTINE ERR_STOP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise le système d'erreur.
C
C Description:
C     La fonction <code>ERR_INI()</code> initialise le système d'erreur.
C     Elle doit être appelée en dehors des zones parallèles.
C
C Entrée:
C
C Sortie:
C
C Notes:
C  gfortran ne permet pas que la déclaration 
C     !$omp threadprivate(/ERR_CDT/)
C  apparaisse dans un BLOCK DATA.
C  A l'opposé, sun exige
C     !$omp threadprivate(/ERR_CDT/)
C  dans toutes les utilisations y compris BLOCK DATA.
C  Copier le contenu du fichier "err.fc" dans BLOCK DATA
C  ne fonctionne donc pas pour tous.
C  La fonction ERR_INI remplace le BLOCK DATA.
C************************************************************************
      SUBROUTINE ERR_INI()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

C-----------------------------------------------------------------------
!$    LOGICAL OMP_IN_PARALLEL
      LOGICAL IN_OMP
      IN_OMP = .FALSE.
!$    IN_OMP = OMP_IN_PARALLEL()
      CALL ERR_ASR(.NOT. IN_OMP)
C-------------------------------------------------------------------------
      
      ERR_XSTK = 0
      ERR_ITP  = ERR_OK
      ERR_IND  = 0
      ERR_EOB  = 0
      ERR_TXT  = '|'

      RDC_ITP  = ERR_OK
      RDC_EOB  = 0
      RDC_IND  = 0
      RDC_PND  = .FALSE.
      RDC_TXT  = '|'

      RETURN      
      END

C************************************************************************
C Sommaire: Ajoute un message à l'erreur
C
C Description:
C     La fonction ERR_AJT(...) ajoute un message d'erreur.
C
C Entrée:
C     CHARACTER*(*) MSG       Texte du message
C
C Sortie:
C
C Notes:
C     Les messages sont concaténés et séparés par des '|'.
C     Pas besoin ici d'un ERR_OMP_RDC car il doit y avoir eu
C     un ERR_ASG avant un ERR_AJT
C************************************************************************
      SUBROUTINE ERR_AJT(MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_AJT
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) MSG

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'err.fc'

      INTEGER LMSG
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_EOB .GT. 0)
D     CALL ERR_PRE(ERR_TXT(ERR_EOB:ERR_EOB) .EQ. '|')
C-----------------------------------------------------------------------

      LMSG = SP_STRN_LEN(MSG)
      LMSG = MIN(LMSG, LEN(ERR_TXT)-ERR_EOB-1)
      IF (LMSG .GT. 0) THEN
         ERR_TXT = ERR_TXT(1:ERR_EOB) // MSG(1:LMSG) // '|'
         ERR_EOB = ERR_EOB + LMSG + 1
         ERR_IND = ERR_IND + 1
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Assigne l'erreur
C
C Description:
C     La fonction ERR_ASG(...) assigne le type d'erreur et le
C     message associé. Elle met le système dans l'état d'erreur
C     demandé.
C
C Entrée:
C     INTEGER       ITYP      Type de message
C     CHARACTER*(*) MSG       Texte du message
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE ERR_ASG(ITYP, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_ASG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       ITYP
      CHARACTER*(*) MSG

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'err.fc'

      INTEGER IERR
      INTEGER LMSG
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ITYP .GE. ERR_OK)
D     CALL ERR_PRE(ITYP .LE. ERR_FTL)
C-----------------------------------------------------------------------

      IF (RDC_PND) IERR = ERR_OMP_END()

      LMSG = SP_STRN_LEN(MSG)
      LMSG = MIN(LMSG, LEN(ERR_TXT)-1)

      ERR_TXT = '|'
      IF (LMSG .GT. 0) ERR_TXT = ERR_TXT(1:1) // MSG(1:LMSG)
      ERR_TXT = ERR_TXT(1:LMSG+1) // '|'
      ERR_EOB = SP_STRN_LEN(ERR_TXT)
      ERR_IND = 1
      ERR_ITP = ITYP

      RETURN
      END

C************************************************************************
C Sommaire: Test le système pour une erreur.
C
C Description:
C     La fonction ERR_BAD() retourne .TRUE. si le système est
C     en erreur.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION ERR_BAD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_BAD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (RDC_PND) IERR = ERR_OMP_END()
      ERR_BAD = (ERR_ITP .GT. ERR_WRN)
      RETURN
      END

C************************************************************************
C Sommaire: Test le système.
C
C Description:
C     La fonction ERR_GOOD() retourne .TRUE. si le système n'est
C     pas en erreur.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION ERR_GOOD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_GOOD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (RDC_PND) IERR = ERR_OMP_END()
      ERR_GOOD = (ERR_ITP .LE. ERR_WRN)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si le message est identique
C
C Description:
C     La fonction ERR_ESTMSG(...) retourne .TRUE. si le message
C     passé en argument est identique au message actuel du système
C     d'erreur.
C
C Entrée:
C     CHARACTER*(*) MSG       Message à vérifier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION ERR_ESTMSG(MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_ESTMSG
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) MSG

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'err.fc'

      INTEGER IERR
      INTEGER LMSG
      INTEGER LERR
      INTEGER INDX
      CHARACTER*256 ERR
C-----------------------------------------------------------------------

      IF (RDC_PND) IERR = ERR_OMP_END()

      ERR = ' '
      IF (ERR_IND .GE. 1) ERR = ERR_MSG(1)
      LERR = SP_STRN_LEN(ERR)
      LMSG = SP_STRN_LEN(MSG)

      IF (LERR .LT. LMSG) THEN
         ERR_ESTMSG = .FALSE.
      ELSEIF (LERR .GT. LMSG) THEN
         INDX = INDEX(' :,;[(=', ERR(LMSG+1:LMSG+1))
         IF (INDX .NE. 0) THEN
            ERR_ESTMSG = (MSG(1:LMSG) .EQ. ERR(1:LMSG))
         ELSE
            ERR_ESTMSG = .FALSE.
         ENDIF
      ELSEIF (LERR .EQ. 0) THEN
         ERR_ESTMSG = .TRUE.
      ELSE
         ERR_ESTMSG = (MSG(1:LMSG) .EQ. ERR(1:LERR))
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Retourne le type d'erreur.
C
C Description:
C     La fonction ERR_TYP() retourne le type d'erreur
C
C Entrée:
C
C Sortie:
C     INTEGER ITYP      Le type
C
C Notes:
C************************************************************************
      FUNCTION ERR_TYP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_TYP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (RDC_PND) IERR = ERR_OMP_END()
      ERR_TYP = ERR_ITP
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de message d'erreur.
C
C Description:
C     La fonction ERR_NBR() retourne le nombre de message d'erreur.
C
C Entrée:
C
C Sortie:
C     INTEGER ERR_NBR      Le nombre
C
C Notes:
C************************************************************************
      FUNCTION ERR_NBR()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_NBR
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (RDC_PND) IERR = ERR_OMP_END()
      ERR_NBR = ERR_IND
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le message d'erreur.
C
C Description:
C     La fonction ERR_MSG(...) retourne le texte du message d'erreur à
C     l'indice passé en argument.
C
C Entrée:
C     INTEGER IND                L'indice du message demandé
C
C Sortie:
C     CHARACTER*256 ERR_MSG      Le message
C
C Notes:
C************************************************************************
      FUNCTION ERR_MSG(IND)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_MSG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IND

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'err.fc'

      INTEGER IERR
      INTEGER I, IDEB, IFIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IND .LE. ERR_IND)
C-----------------------------------------------------------------------

      IF (RDC_PND) IERR = ERR_OMP_END()

      IDEB = 0
      DO I=1,IND
         IDEB = IDEB+1 + INDEX(ERR_TXT(IDEB+1:ERR_EOB), '|')
      ENDDO
      IFIN = IDEB + INDEX(ERR_TXT(IDEB:ERR_EOB), '|') - 2

      IF (IFIN .GE. IDEB .AND. (IFIN-IDEB) .LT. 256) THEN
         ERR_MSG = ERR_TXT(IDEB:IFIN)
      ELSE
         ERR_MSG = ' '
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Pousse la pile des messages
C
C Description:
C     La fonction ERR_PUSH() pousse le message courant sur la pile des
C     message et réinitialise le message courant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE ERR_PUSH()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_PUSH
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'err.fc'

      INTEGER   IRET
      INTEGER*1 COD
C-----------------------------------------------------------------------

C---     Au besoin, construis la pile
      IF (ERR_XSTK .EQ. 0) THEN
         ERR_XSTK = C_LST_CTR()
      ENDIF

C---     Encode le 1er char
      COD = ISHFT(ERR_IND, 2) + ERR_ITP
      ERR_TXT(1:1) = CHAR(COD)
      
C---     Pousse la valeur
      IRET = C_LST_AJTVAL(ERR_XSTK, ERR_TXT(1:SP_STRN_LEN(ERR_TXT)))

C---     Reset le message courant
      CALL ERR_RESET()

      RETURN
      END

C************************************************************************
C Sommaire: Dépile la pile des messages
C
C Description:
C     La fonction ERR_POP() dépile le message courant de la pile des
C     message et restaure le message précédant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE ERR_POP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_POP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'err.fc'

      INTEGER   IRET
      INTEGER   NVAL
      INTEGER*1 COD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_XSTK .NE. 0)
D     CALL ERR_PRE(C_LST_REQDIM(ERR_XSTK) .GT. 0)
C-----------------------------------------------------------------------

      NVAL = C_LST_REQDIM(ERR_XSTK)

C---     Pop la valeur
      IRET = C_LST_REQVAL(ERR_XSTK, NVAL, ERR_TXT)
      IRET = C_LST_EFFVAL(ERR_XSTK, NVAL)

C---     Décode le 1er char
      COD = ICHAR(ERR_TXT(1:1))
      ERR_IND = ISHFT(COD, -2)
      ERR_ITP = COD - ISHFT(ERR_IND, 2)
      ERR_TXT(1:1) = '|'

      ERR_EOB = SP_STRN_LEN(ERR_TXT)

      RETURN
      END

C************************************************************************
C Sommaire: Test pour une précondition.
C
C Description:
C     La fonction ERR_PRE(...) teste pour une précondition. Si
C     le test échoue, donc si (.NOT. CONDITION) est .TRUE. le
C     traitement est arrêté.
C
C Entrée:
C     LOGICAL CONDITION       La condition à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE ERR_PRE(CONDITION)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_PRE
CDEC$ ENDIF

      IMPLICIT NONE

      LOGICAL CONDITION
C-----------------------------------------------------------------------

      IF (.NOT. CONDITION) THEN
         WRITE(6,*) ' '
         WRITE(6,*) '!!!----------------------------'
         WRITE(6,*) '!!!      PRECONDITION'
         WRITE(6,*) '!!!----------------------------'
         CALL ERR_STOP()
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Test pour une postcondition.
C
C Description:
C     La fonction ERR_PST(...) teste pour une postcondition. Si
C     le test échoue, donc si (.NOT. CONDITION) est .TRUE. le
C     traitement est arrêté.
C
C Entrée:
C     LOGICAL CONDITION       La condition à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE ERR_PST(CONDITION)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_PST
CDEC$ ENDIF

      IMPLICIT NONE

      LOGICAL CONDITION
C-----------------------------------------------------------------------

      IF (.NOT. CONDITION) THEN
         WRITE(6,*) ' '
         WRITE(6,*) '!!!----------------------------'
         WRITE(6,*) '!!!      POSTCONDITION'
         WRITE(6,*) '!!!----------------------------'
         CALL ERR_STOP()
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Test pour une assertion.
C
C Description:
C     La fonction ERR_PRE(...) teste pour une assertion. Si
C     le test échoue, donc si (.NOT. CONDITION) est .TRUE. le
C     traitement est arrêté.
C
C Entrée:
C     LOGICAL CONDITION       La condition à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE ERR_ASR(CONDITION)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_ASR
CDEC$ ENDIF

      IMPLICIT NONE

      LOGICAL CONDITION
C-----------------------------------------------------------------------

      IF (.NOT. CONDITION) THEN
         WRITE(6,*) ' '
         WRITE(6,*) '!!!----------------------------'
         WRITE(6,*) '!!!      ASSERTION'
         WRITE(6,*) '!!!----------------------------'
         CALL ERR_STOP()
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Reset le système d'erreur
C
C Description:
C     La fonction ERR_RESET reset le système d'erreur, donc
C     assigne le type à ERR_OK avec un message vide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE ERR_RESET()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_RESET
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (RDC_PND) IERR = ERR_OMP_END()

      ERR_ITP = ERR_OK
      ERR_EOB = 0
      ERR_IND = 0
      ERR_TXT = '|'

      RETURN
      END

C************************************************************************
C Sommaire: Stoppe le système d'erreur
C
C Description:
C     La fonction ERR_STOP() stoppe le système. En multi process, on attend
C     que tous les process soient rendus.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE ERR_STOP()

      IMPLICIT NONE

      INCLUDE 'c_os.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_RANK
      INTEGER I_SIZE
      INTEGER I_ERROR
      LOGICAL I_CONTINUE

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)
C-----------------------------------------------------------------------

D     CALL MPI_COMM_SIZE(MP_UTIL_REQCOMM(), I_SIZE, I_ERROR)

C---     EN MULTI-PROCESS, ATTEND
D     IF (I_SIZE .GT. 1) THEN
D        CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
D        IF (I_RANK .EQ. I_MASTER) THEN
D           I_CONTINUE = .FALSE.
D100        CONTINUE
D              CALL SLEEP(2)
D           IF (.NOT. I_CONTINUE) GOTO 100
D        ENDIF
D        CALL MPI_BARRIER(MP_UTIL_REQCOMM(), I_ERROR)
D     ENDIF

      I_ERROR = C_OS_ABORT()
      STOP
      END

C************************************************************************
C Sommaire: Fonction vide
C
C Description:
C     La fonction ERR_DUMMY() est une fonction vide qui peut servir
C     de paramètre inutilisé.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION ERR_DUMMY()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_DUMMY
CDEC$ ENDIF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      CALL ERR_ASR(.FALSE.)

      ERR_DUMMY = ERR_FTL
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la réduction des erreurs des tâches OMP.
C
C Description:
C     La fonction ERR_OMP_INI initialise la réduction des messages des tâches
C     OMP. La fonction doit être appelée immédiatement avant le début de la
C     section parallèle:
C     <pre>
C      IERR = ERR_OMP_INI()
C     !$omp parallel
C        IERR = ERR_OMP_DST()
C        ...  travail parallélisé
C        IERR = ERR_OMP_RDC()
C     !$omp end parallel
C      IERR = ERR_OMP_END()
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les données ERR_ sont privées (!$omp threadprivate(/ERR_CDT/)
C     à chaque tâche. Le jeux RDC_ est partagé.
C     Ici, on copie les variables privées de la tâche principale vers les 
C     données partagées.
C************************************************************************
      FUNCTION ERR_OMP_INI()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_OMP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

      LOGICAL IN_OMP
!$    LOGICAL OMP_IN_PARALLEL
C-----------------------------------------------------------------------

      IN_OMP = .FALSE.
!$    IN_OMP = OMP_IN_PARALLEL()
      IF (.NOT. IN_OMP) THEN
         RDC_ITP = ERR_ITP
         RDC_EOB = ERR_EOB
         RDC_IND = ERR_IND
         RDC_TXT = ERR_TXT(1:ERR_EOB)
      ENDIF

      ERR_OMP_INI = ERR_ITP
      RETURN
      END
      
C************************************************************************
C Sommaire: Finalise la réduction des erreurs des tâches OMP.
C
C Description:
C     La fonction ERR_OMP_END finalise la réduction des messages des tâches
C     OMP. La fonction doit être appelée immédiatement après la fin de la
C     section parallèle:
C     <pre>
C      IERR = ERR_OMP_INI()
C     !$omp parallel
C        IERR = ERR_OMP_DST()
C        ...  travail parallélisé
C        IERR = ERR_OMP_RDC()
C     !$omp end parallel
C      IERR = ERR_OMP_END()
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les données ERR_ sont privées (!$omp threadprivate(/ERR_CDT/)
C     à chaque tâche. Le jeux RDC_ est partagé.
C     Ici, on copie les variables partagées vers les données privées
C     de la tâche principale.
C************************************************************************
      FUNCTION ERR_OMP_END()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_OMP_END
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

      LOGICAL IN_OMP
!$    LOGICAL OMP_IN_PARALLEL
C-----------------------------------------------------------------------

      IN_OMP = .FALSE.
!$    IN_OMP = OMP_IN_PARALLEL()
      IF (.NOT. IN_OMP) THEN
         IF (RDC_PND) THEN
            ERR_ITP = RDC_ITP
            ERR_EOB = RDC_EOB
            ERR_IND = RDC_IND
            ERR_TXT = RDC_TXT(1:RDC_EOB)

            RDC_ITP = ERR_OK
            RDC_EOB = 0
            RDC_IND = 0
            RDC_TXT = '|'
            RDC_PND = .FALSE.
         ENDIF
      ENDIF

      ERR_OMP_END = ERR_ITP
      RETURN
      END

C************************************************************************
C Sommaire: Distribue les erreurs vers les tâches OMP.
C
C Description:
C     La fonction ERR_OMP_DST distribue à chaque tâche l'état du système
C     d'erreur. La fonction doit être appelée immédiatement après le début de la
C     section parallèle:
C     <pre>
C      IERR = ERR_OMP_INI()
C     !$omp parallel
C        IERR = ERR_OMP_DST()
C        ...  travail parallélisé
C        IERR = ERR_OMP_RDC()
C     !$omp end parallel
C      IERR = ERR_OMP_END()
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les données ERR_ sont privées (!$omp threadprivate(/ERR_CDT/)
C     à chaque tâche. Le jeux RDC_ est partagé.
C************************************************************************
      FUNCTION ERR_OMP_DST()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_OMP_DST
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

!$    LOGICAL OMP_IN_PARALLEL
      LOGICAL IN_OMP
C-----------------------------------------------------------------------

      IN_OMP = .FALSE.
!$    IN_OMP = OMP_IN_PARALLEL()
      IF (IN_OMP) THEN
         ERR_ITP = RDC_ITP
         ERR_EOB = RDC_EOB
         ERR_IND = RDC_IND
         ERR_TXT = RDC_TXT(1:RDC_EOB)
      ENDIF

      ERR_OMP_DST = ERR_ITP
      RETURN
      END

C************************************************************************
C Sommaire: Réduis les erreurs des tâches OMP.
C
C Description:
C     La fonction ERR_OMP_RDC réduis les messages de chacune des tâches
C     OMP. La fonction doit être appelée immédiatement avant la fin de la
C     section parallèle:
C     <pre>
C      IERR = ERR_OMP_INI()
C     !$omp parallel
C        IERR = ERR_OMP_DST()
C        ...  travail parallélisé
C        IERR = ERR_OMP_RDC()
C     !$omp end parallel
C      IERR = ERR_OMP_END()
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les données ERR_ sont privées (!$omp threadprivate(/ERR_CDT/)
C     à chaque tâche. Le jeux RDC_ est partagé.
C************************************************************************
      FUNCTION ERR_OMP_RDC()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: ERR_OMP_RDC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'err.fc'

!$    LOGICAL OMP_IN_PARALLEL
      LOGICAL IN_OMP
C-----------------------------------------------------------------------

      IN_OMP = .FALSE.
!$    IN_OMP = OMP_IN_PARALLEL()
      IF (IN_OMP) THEN
!$omp critical(OMP_CS_ERR)
         IF (ERR_ITP .GT. RDC_ITP) THEN
            RDC_ITP = ERR_ITP
            RDC_EOB = ERR_EOB
            RDC_IND = ERR_IND
            RDC_TXT = ERR_TXT(1:ERR_EOB)
         ENDIF
         RDC_PND = .TRUE.
!$omp end critical(OMP_CS_ERR)
      ENDIF

      ERR_OMP_RDC = ERR_ITP
      RETURN
      END

