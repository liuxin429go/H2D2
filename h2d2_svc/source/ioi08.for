C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IO_I08_000
C     INTEGER IO_I08_999
C     INTEGER IO_I08_CTR
C     INTEGER IO_I08_DTR
C     INTEGER IO_I08_INI
C     INTEGER IO_I08_RST
C     INTEGER IO_I08_REQHBASE
C     LOGICAL IO_I08_HVALIDE
C     INTEGER IO_I08_OPEN
C     INTEGER IO_I08_CLOSE
C     INTEGER IO_I08_ENDL
C     INTEGER IO_I08_SKIP
C     INTEGER IO_I08_GETPOS
C     INTEGER IO_I08_SETPOS
C     INTEGER IO_I08_WHDR
C     INTEGER IO_I08_WS
C     INTEGER IO_I08_WI_1
C     INTEGER IO_I08_WI_N
C     INTEGER IO_I08_WD_1
C     INTEGER IO_I08_WD_N
C     INTEGER IO_I08_RHDR
C     INTEGER IO_I08_RS
C     INTEGER IO_I08_RI_1
C     INTEGER IO_I08_RI_N
C     INTEGER IO_I08_RD_1
C     INTEGER IO_I08_RD_N
C   Private:
C     INTEGER IO_I08_WI1
C     INTEGER IO_I08_WI4
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_I08_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioi08.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(IO_I08_NOBJMAX,
     &                   IO_I08_HBASE,
     &                   'Lossy writer 8 bits')

      IO_I08_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_I08_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioi08.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioi08.fc'

      INTEGER  IERR
      EXTERNAL IO_I08_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(IO_I08_NOBJMAX,
     &                   IO_I08_HBASE,
     &                   IO_I08_DTR)

      IO_I08_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   IO_I08_NOBJMAX,
     &                   IO_I08_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(IO_I08_HVALIDE(HOBJ))
         IOB = HOBJ - IO_I08_HBASE

         IO_I08_NCOL(IOB) = 0
         IO_I08_UNIT(IOB) = 0
         IO_I08_ISTT(IOB) = IO_MODE_INDEFINI
         IO_I08_IPOS(IOB) = -1
         IO_I08_BUFF(IOB) = ' '
      ENDIF

      IO_I08_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioi08.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioi08.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = IO_I08_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   IO_I08_NOBJMAX,
     &                   IO_I08_HBASE)

      IO_I08_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = IO_I08_RST(HOBJ)

      IO_I08_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioi08.fc'

      INTEGER   IERR
      INTEGER   IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_I08_HBASE

C---     FERME LE FICHIER
      IERR = IO_I08_CLOSE(HOBJ)

C---     RESET LES ATTRIBUTS
      IO_I08_NCOL(IOB) = 0
      IO_I08_UNIT(IOB) = 0
      IO_I08_ISTT(IOB) = IO_MODE_INDEFINI
      IO_I08_IPOS(IOB) = -1
      IO_I08_BUFF(IOB) = ' '

      IO_I08_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction IO_I08_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_I08_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioi08.fi'
      INCLUDE 'ioi08.fc'
C------------------------------------------------------------------------

      IO_I08_REQHBASE = IO_I08_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction IO_I08_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_I08_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioi08.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ioi08.fc'
C------------------------------------------------------------------------

      IO_I08_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  IO_I08_NOBJMAX,
     &                                  IO_I08_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     FNAME       Nom du fichier
C     ISTAT       IO_LECTURE ou IO_ECRITURE, mais pas IO_AJOUT
C     IPRM        Paramètres supplémentaires propre à la classe:
C                    NLIN: Nombre de lignes
C                    NCOL: Nombre de colonnes
C                    TEMP: Temps
C                    VMIN: La valeur min à représenter
C                    VMAX: La valeur max à représenter
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_OPEN(HOBJ, FNAME, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_OPEN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME
      INTEGER       ISTAT

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER M
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FNAME) .GT. 0)
C------------------------------------------------------------------------

      IERR = 0

C---     Ouvre le fichier
      M = IO_UTIL_FREEUNIT()
      IF     (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) THEN
         OPEN(UNIT  = M,
     &        FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &        FORM  = 'FORMATTED', !  'UNFORMATTED',
     &        STATUS= 'OLD',
     &        ERR   = 9901)
      ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) THEN
         OPEN(UNIT  = M,
     &        FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &        FORM  = 'FORMATTED', !  'UNFORMATTED',
     &        STATUS= 'REPLACE',
     &        ERR   = 9901)
      ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_AJOUT)) THEN
         OPEN(UNIT  = M,
     &        FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &        FORM  = 'FORMATTED', !  'UNFORMATTED',
     &        STATUS= 'UNKNOWN',
     &        ACCESS='APPEND',     ! Non standard
C    &        POSITION='APPEND',   ! Pas implanté par g77
     &        ERR   = 9901)
      ELSE
         GOTO 9900
      ENDIF

C---     Stocke les attributs
      IOB = HOBJ - IO_I08_HBASE
      IO_I08_UNIT(IOB) = M
      IO_I08_ISTT(IOB) = ISTAT
      IO_I08_IPOS(IOB) = 0    ! Faux si APPEND

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_MODE_ACCES_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER', ': ',
     &                      FNAME(1:SP_STRN_LEN(FNAME))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_OPEN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_CLOSE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_CLOSE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - IO_I08_HBASE

      MW = IO_I08_UNIT(IOB)
      IF (MW .NE. 0) THEN
         CLOSE(MW)
      ENDIF

      IO_I08_UNIT(IOB) = 0
      IO_I08_ISTT(IOB) = IO_MODE_INDEFINI
      IO_I08_IPOS(IOB) = -1

      IO_I08_CLOSE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_ENDL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_ENDL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER MW, LB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - IO_I08_HBASE

      MW = IO_I08_UNIT(IOB)
      LB = SP_STRN_LEN(IO_I08_BUFF(IOB))
      IF (LB .GT. 0) THEN
         WRITE(MW, '(A)', ERR=9900) IO_I08_BUFF(IOB)(1:LB)
      ELSE
         WRITE(MW, ERR=9900)
      ENDIF
      IO_I08_BUFF(IOB) = ' '
      IO_I08_IPOS(IOB) = IO_I08_IPOS(IOB) + 1   ! Pourquoi? CR? CR/LF?

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_ECRITURE_BUFFER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_ENDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Skip N lines.
C
C Description:
C     The method IO_I08_SKIP do skip N lines from the object
C     file unit that must be open for reading.
C
C Entrée:
C     N     Number of lines to skip
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_I08_SKIP(HOBJ, N)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_SKIP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER I
      INTEGER IOB
      INTEGER MR
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      IF (N .LT. 0) GOTO 9900

      IOB = HOBJ-IO_I08_HBASE
      MR = IO_I08_UNIT(IOB)

      DO I=1,N
         READ(MR, *, ERR=9900, END=9900) BUF
!         READ(MR, ERR=9901, END=9902) BUF
      ENDDO

      IO_I08_IPOS(IOB) = IO_I08_IPOS(IOB) + N

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SAUT_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_SKIP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Get the current file position.
C
C Description:
C     The method IO_I08_GETPOS do skip N lines from the object
C     file unit that must be open for reading.
C
C Entrée:
C     N     Number of lines to skip
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_I08_GETPOS(HOBJ, XPOS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_GETPOS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XPOS

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      XPOS = IO_I08_IPOS(HOBJ-IO_I08_HBASE)
      IO_I08_GETPOS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Set the current file position.
C
C Description:
C     The method IO_I08_SETPOS
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_I08_SETPOS(HOBJ, XPOS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_SETPOS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XPOS

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NSKP
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      IOB = HOBJ - IO_I08_HBASE
      NSKP = INT(XPOS - IO_I08_IPOS(IOB))
      IERR = IO_I08_SKIP(HOBJ, NSKP)

      IO_I08_SETPOS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IO_I08_WI1 écris N valeurs de la table
C     INTEGER*1 K dans le buffer BUF.
C
C Entrée:
C     BUF      Le buffer
C     N        Nombre d'entrées
C     K        Table des entrées
C
C Sortie:
C     BUF      Le buffer
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_WI1(BUF, N, K)

      CHARACTER*(*) BUF
      INTEGER   N
      INTEGER*1 K(*)

      INCLUDE 'err.fi'
      INCLUDE 'ioi08.fc'

      INTEGER   I
C------------------------------------------------------------------------

      IF (LEN(BUF) .LT. N*4) GOTO 9900
      WRITE(BUF, '(1X, I3)', ERR=9900) (K(I),I=1,N)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_BUFFER_OVERFLOW'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_ECRITURE_INT1'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_WI1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IO_I08_WI4 écris N valeurs de la table INTEGER*4 K
C     dans le buffer BUF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_WI4(BUF, N, K)

      CHARACTER*(*) BUF
      INTEGER       N
      INTEGER       K(*)

      INCLUDE 'err.fi'
      INCLUDE 'ioi08.fc'

      INTEGER       I
C------------------------------------------------------------------------

      WRITE(BUF, '(1X, I12)', ERR=9900) (K(I),I=1,N)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9)') 'ERR_ECRITURE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_WI4 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NLIN        Nombre de lignes
C     NCOL        Nombre de colonnes
C     TEMP        Temps
C     VMIN        La valeur min à représenter
C     VMAX        La valeur max à représenter
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_WHDR(HOBJ, NLIN, NCOL, TMPS, VMIN, VMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_WHDR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NLIN
      INTEGER NCOL
      REAL*8  TMPS
      REAL*8  VMIN(*)
      REAL*8  VMAX(*)

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER MW
      INTEGER IC
      REAL*8  VDEL
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - IO_I08_HBASE
      MW = IO_I08_UNIT(IOB)

C---     Écris l'entête
      WRITE(MW, '(2I12,1PE26.17E3)', ERR=9900) NLIN, NCOL, TMPS
      DO IC=1, NCOL
         VDEL = (VMAX(IC)-VMIN(IC)) / 255.0D0
         WRITE(MW, '(3(1PE26.17E3))', ERR=9900) VMIN(IC), VMAX(IC), VDEL
      ENDDO

C---     Stocke les attributs
      IOB = HOBJ - IO_I08_HBASE
      DO IC=1, NCOL
         VDEL = (VMAX(IC)-VMIN(IC)) / 255.0D0
         IO_I08_VMIN(IC,IOB) = VMIN(IC)
         IO_I08_VMIN(IC,IOB) = VMAX(IC)
         IO_I08_VDEL(IC,IOB) = VDEL
      ENDDO
      IO_I08_NCOL(IOB) = NCOL
      IO_I08_IPOS(IOB) = IO_I08_IPOS(IOB) + 1

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ECRITURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_WHDR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_WS(HOBJ, S)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_WS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) S

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER MW
      INTEGER LS
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_I08_UNIT(HOBJ-IO_I08_HBASE)

      LS = SP_STRN_LEN(S)
      WRITE(MW, '(A)', ERR=9900) S(1:LS)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_ECRITURE_STRING', ': ', S(1:LS)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_WS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Write a single INTEGER
C
C Description:
C     The function IO_I08_WI_1 writes one INTEGER value to the file. Writing is
C     buffered and will only be flushed to file with a call to IO_I08_ENDL.
C
C Entrée:
C     HOBJ     Handle on the current object
C     I        The value to write
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_WI_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_WI_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER SOB, EOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      IOB = HOBJ - IO_I08_HBASE
      SOB = SP_STRN_LEN(IO_I08_BUFF(IOB)) + 1
      EOB = LEN(IO_I08_BUFF(IOB))
      IERR = IO_I08_WI4 (IO_I08_BUFF(IOB)(SOB:EOB), 1, I)

      IO_I08_WI_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Write an INTEGER table
C
C Description:
C     The function IO_I08_WI_N writes N INTEGER values from table KX with
C     stride IX to the file. Writing is buffered and will only be
C     flushed to file with a call to IO_I08_ENDL.
C
C Entrée:
C     HOBJ     Handle on the current object
C     N        Number of values to write
C     KX       Integer table holding  the values
C     IX       The stride
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_WI_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_WI_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      INTEGER KX(*)
      INTEGER IX

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER I, JX
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_I08_WI_1(HOBJ, KX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_I08_WI_1(HOBJ, KX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      IO_I08_WI_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Write a single REAL*8
C
C Description:
C     The function IO_I08_WD_1 writes one REAL*8 value to the file. Writing is
C     buffered and will only be flushed to file with a call to IO_I08_ENDL.
C
C Entrée:
C     HOBJ     Handle on the current object
C     ICOL     Column index of the value
C     V        The value to write
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_WD_1(HOBJ, ICOL, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_WD_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER ICOL
      REAL*8  V

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      REAL*8    VMIN, VDEL
      INTEGER   IERR
      INTEGER   IOB
      INTEGER   SOB, EOB
      INTEGER*1 IV(1)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_ECRITURE))
D     CALL ERR_PRE(ICOL .GE. 0)
D     CALL ERR_PRE(ICOL .LE. IO_I08_NCOL(HOBJ-IO_I08_HBASE))
C------------------------------------------------------------------------

      IOB = HOBJ - IO_I08_HBASE

      VMIN  = IO_I08_VMIN(ICOL, IOB)
      VDEL  = IO_I08_VDEL(ICOL, IOB)
      IV(1) = NINT( (V-VMIN) * VDEL )

      SOB = SP_STRN_LEN(IO_I08_BUFF(IOB)) + 1
      EOB = LEN(IO_I08_BUFF(IOB))
      IERR = IO_I08_WI1 (IO_I08_BUFF(IOB)(SOB:EOB), 1, IV)

      IO_I08_WD_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_WD_N(HOBJ, N, VX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_WD_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  VX(*)
      INTEGER IX

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER I, JX
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_ECRITURE))
D     CALL ERR_PRE(N .EQ. IO_I08_NCOL(HOBJ-IO_I08_HBASE))
C------------------------------------------------------------------------

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_I08_WD_1(HOBJ, I, VX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_I08_WD_1(HOBJ, I, VX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      IO_I08_WD_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_RHDR(HOBJ, N1, N2, T, VMIN, VMAX, VDEL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_RHDR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N1
      INTEGER N2
      REAL*8  T
      REAL*8  VMIN(*)
      REAL*8  VMAX(*)
      REAL*8  VDEL(*)

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I
      INTEGER MR
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - IO_I08_HBASE
      MR = IO_I08_UNIT(IOB)

C---     Lis les données
      READ (MR, *, ERR=9900, END=9901) N1, N2, T
      DO I=1, N2
         READ (MR, *, ERR=9900, END=9901) VMIN(I), VMAX(I), VDEL(I)
      ENDDO

C---     Stocke les attributs
      DO I=1,N2
         IO_I08_VMIN(I,IOB) = VMIN(I)
         IO_I08_VMIN(I,IOB) = VMAX(I)
         IO_I08_VDEL(I,IOB) = VDEL(I)
      ENDDO
      IO_I08_IPOS(IOB) = IO_I08_IPOS(IOB) + N2+1

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_RHDR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_RS(HOBJ, S)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_RS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) S

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER MR
      INTEGER LB, LS
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      MR = IO_I08_UNIT(HOBJ-IO_I08_HBASE)

      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)

      IF (BUF(1:8) .NE. '<string>') GOTO 9901
      IF (BUF(LB-8:LB) .NE. '</string>') GOTO 9901

      BUF = BUF(9:LB-9)
      LB = INDEX(BUF,':')
      READ(BUF(1:LB-1), *) LS
      IF (LS .GT. LEN(S)) GOTO 9902
      S = BUF(LB+1:LB+LS)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_STRING'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_TAG_XML_INVALIDE', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_STRING', ': ', BUF(9:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_RS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_RI_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_RI_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER MR
      INTEGER LB
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      MR = IO_I08_UNIT(HOBJ-IO_I08_HBASE)

      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)

      IF (BUF(1:9) .NE. '<integer>') GOTO 9901
      IF (BUF(LB-9:LB) .NE. '</integer>') GOTO 9901

      READ(BUF(10:LB-10), *, ERR=9902) I

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_INTEGER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_TAG_XML_INVALIDE', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_INTEGER', ': ', BUF(10:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_RI_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_RI_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_RI_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      INTEGER KX(*)
      INTEGER IX

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER I, JX
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_I08_RI_1(HOBJ, KX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_I08_RI_1(HOBJ, KX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      IO_I08_RI_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_RD_1(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_RD_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  V

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER MR
      INTEGER LB
      CHARACTER*(64) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      MR = IO_I08_UNIT(HOBJ-IO_I08_HBASE)

      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)

      IF (BUF(1:8) .NE. '<double>') GOTO 9901
      IF (BUF(LB-8:LB) .NE. '</double>') GOTO 9901

      READ(BUF(9:LB-9), *, ERR=9902) V

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_DOUBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_TAG_XML_INVALIDE', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_DOUBLE', ': ', BUF(9:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_I08_RD_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_I08_RD_N(HOBJ, N, VX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_I08_RD_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  VX(*)
      INTEGER IX

      INCLUDE 'ioi08.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioi08.fc'

      INTEGER IERR
      INTEGER I, JX
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_I08_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_I08_UNIT(HOBJ-IO_I08_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_I08_ISTT(HOBJ-IO_I08_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_I08_RD_1(HOBJ, VX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_I08_RD_1(HOBJ, VX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      IO_I08_RD_N = ERR_TYP()
      RETURN
      END
