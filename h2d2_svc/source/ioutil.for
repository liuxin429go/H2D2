C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C      FUNCTION IO_UTIL_FREEUNIT()
C      FUNCTION IO_UTIL_FICDUM()
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un numéro d'unité.
C
C Description:
C     La fonction statique IO_UTIL_FREEUNIT retourne un numéro d'unité
C     libre qui peut être utilisé pour l'ouverture d'un fichier.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_UTIL_FREEUNIT()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_UTIL_FREEUNIT
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioutil.fi'

      INTEGER M
      LOGICAL ISOPEN
C----------------------------------------------------------------------------

      DO M = 10, 99
         ISOPEN = .FALSE.
         INQUIRE(UNIT=M, ERR=10, OPENED=ISOPEN)
         IF (.NOT. ISOPEN) GOTO 19
10       CONTINUE
      ENDDO
      M = -1
19    CONTINUE

      IO_UTIL_FREEUNIT = M
      RETURN
      END

C************************************************************************
C Sommaire: IO_UTIL_FICDUM
C
C Description:
C     La fonction statique IO_UTIL_FICDUM retourne le nom à
C     utiliser pour un fichier inexistant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_UTIL_FICDUM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_UTIL_FICDUM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioutil.fi'
C-----------------------------------------------------------------------

      IO_UTIL_FICDUM = 'DummyFilePlaceHolder'

      RETURN
      END

C************************************************************************
C Sommaire: Contrôle la validité du mode.
C
C Description:
C     La fonction statique IO_UTIL_MODVALIDE retourne .TRUE. si le mode
C     de fichier est valide et .FALSE. sinon.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_UTIL_MODVALIDE(ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_UTIL_MODVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ISTAT

      INCLUDE 'ioutil.fi'

      INTEGER N
      LOGICAL RES
C----------------------------------------------------------------------------

      RES = .TRUE.

      IF (ISTAT .EQ. IO_MODE_INDEFINI) RES = .FALSE.

      IF (RES) THEN
         N = 0
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE))  N = N + 1
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) N = N + 1
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_AJOUT))    N = N + 1
         IF (N .NE. 1) RES = .FALSE.
      ENDIF

      IF (RES) THEN
         N = 0
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_BINAIRE))  N = N + 1
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ASCII))    N = N + 1
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_PREC_64))  N = N + 1
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_PREC_32))  N = N + 1
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_PREC_16))  N = N + 1
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_PREC_08))  N = N + 1
         IF (N .NE. 1) RES = .FALSE.
      ENDIF

      IO_UTIL_MODVALIDE = RES
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction statique IO_UTIL_MODACTIF retourne VRAI si les modes
C     donnés par MOD sont activés dans ISTAT.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_UTIL_MODACTIF(ISTAT, MOD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_UTIL_MODACTIF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ISTAT
      INTEGER MOD

      INCLUDE 'ioutil.fi'
C----------------------------------------------------------------------------

      IO_UTIL_MODACTIF = (IAND(ISTAT, MOD) .NE. 0)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction statique IO_UTIL_STR2MOD
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_UTIL_STR2MOD(M)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_UTIL_STR2MOD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) M

      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'

      INTEGER ISTAT
C----------------------------------------------------------------------------

      ISTAT = IO_MODE_INDEFINI

      IF (SP_STRN_LEN(M) .LT. 1) GOTO 9999
      IF (SP_STRN_LEN(M) .GT. 2) GOTO 9999

      IF     (M(1:1) .EQ. 'r') THEN
         ISTAT = ISTAT + IO_MODE_LECTURE
      ELSEIF (M(1:1) .EQ. 'w') THEN
         ISTAT = ISTAT + IO_MODE_ECRITURE
      ELSEIF (M(1:1) .EQ. 'a') THEN
         ISTAT = ISTAT + IO_MODE_AJOUT
      ELSE
         GOTO 9999
      ENDIF

      IF     (SP_STRN_LEN(M) .EQ. 1) THEN
         ISTAT = ISTAT + IO_MODE_ASCII
      ELSEIF (M(2:2) .EQ. 'b') THEN
         ISTAT = ISTAT + IO_MODE_BINAIRE
      ELSEIF (M(2:2) .EQ. 't') THEN
         ISTAT = ISTAT + IO_MODE_ASCII
      ELSEIF (M(2:2) .EQ. '1') THEN
         ISTAT = ISTAT + IO_MODE_PREC_08
      ELSEIF (M(2:2) .EQ. '8') THEN
         ISTAT = ISTAT + IO_MODE_PREC_64
      ELSE
         GOTO 9999
      ENDIF

9999  CONTINUE
      IO_UTIL_STR2MOD = ISTAT
      RETURN
      END
