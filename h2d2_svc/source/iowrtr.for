C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C  L'écriture est bufferisée, les écritures sont faites dans un buffer
C  qui est vidé par appel à ENDL
C
C Sousroutines:
C      INTEGER IO_WRTR_DTR
C      LOGICAL IO_WRTR_CTRLH
C      INTEGER IO_WRTR_OPEN
C      INTEGER IO_WRTR_CLOSE
C      INTEGER IO_WRTR_ENDL
C      INTEGER IO_WRTR_WS
C      INTEGER IO_WRTR_WI_1
C      INTEGER IO_WRTR_WI_N
C      INTEGER IO_WRTR_WD_1
C      INTEGER IO_WRTR_WD_N
C      INTEGER IO_WRTR_RS
C      INTEGER IO_WRTR_RI_1
C      INTEGER IO_WRTR_RI_N
C      INTEGER IO_WRTR_RD_1
C      INTEGER IO_WRTR_RD_N
C************************************************************************

C************************************************************************
C Sommaire: IO_WRTR_DTR
C
C Description:
C     La fonction IO_WRTR_DTR agit comme destructeur virtuel de la
C     structure de classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_DTR(HOBJ)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_DTR(HOBJ)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_DTR(HOBJ)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_DTR(HOBJ)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_DTR(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_WRTR_CTRLH permet de valider un objet de la
C     hiérarchie virtuelle. Elle retourne .TRUE. si le handle qui
C     lui est passé est valide comme handle de l'objet ou d'un
C     héritier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_WRTR_CTRLH(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_CTRLH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'iowrtr.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      LOGICAL VALIDE
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      VALIDE = .FALSE.
      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         VALIDE = .TRUE.
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         VALIDE = .TRUE.
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         VALIDE = .TRUE.
      ENDIF

      IO_WRTR_CTRLH = VALIDE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_WRTR_OPEN connecte l'objet au fichier FNAME, soit
C     en lecture, soit en écriture.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     FNAME       Nom du fichier
C     ISTAT       IO_LECTURE ou IO_ECRITURE, mais pas IO_AJOUT
C     IPRM        Paramètres supplémentaires propre à la classe
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_OPEN(HOBJ, FNAME, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_OPEN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME
      INTEGER       ISTAT

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_OPEN(HOBJ, FNAME, ISTAT)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_OPEN(HOBJ, FNAME, ISTAT)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_OPEN(HOBJ, FNAME, ISTAT)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_OPEN(HOBJ, FNAME, ISTAT)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_OPEN(HOBJ, FNAME, ISTAT)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_OPEN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_CLOSE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_CLOSE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_CLOSE(HOBJ)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_CLOSE(HOBJ)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_CLOSE(HOBJ)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_CLOSE(HOBJ)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_CLOSE(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_CLOSE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_WRTR_ENDL écrit dans le fichier le contenu du buffer
C     de l'objet. Le buffer en vidé.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_ENDL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_ENDL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_ENDL(HOBJ)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_ENDL(HOBJ)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_ENDL(HOBJ)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_ENDL(HOBJ)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_ENDL(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_ENDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IO_WRTR_WS
C
C Description:
C     La fonction IO_WRTR_WS écrit une chaîne de caratères.
C
C Entrée:
C      HOBJ    Handle sur l'objet courant
C      S       La chaîne
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_WS (HOBJ, S)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_WS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) S

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_WS(HOBJ, S)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_WS(HOBJ, S)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_WS(HOBJ, S)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_WS(HOBJ, S)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_WS(HOBJ, S)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_WS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La métode virtuelle IO_WRTR_WI_1 écrit un INTEGER.
C
C Entrée:
C      HOBJ    Handle sur l'objet courant
C      S       L'entier
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_WI_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_WI_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_WI_1(HOBJ, I)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_WI_1(HOBJ, I)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_WI_1(HOBJ, I)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_WI_1(HOBJ, I)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_WI_1(HOBJ, I)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_WI_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La métode virtuelle IO_WRTR_WI_N écrit une table INTEGER.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     N        Le nombre d'entier à écrire
C     KX       La table entière
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_WI_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_WI_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      INTEGER KX(*)
      INTEGER IX

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_WI_N(HOBJ, N, KX, IX)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_WI_N(HOBJ, N, KX, IX)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_WI_N(HOBJ, N, KX, IX)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_WI_N(HOBJ, N, KX, IX)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_WI_N(HOBJ, N, KX, IX)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_WI_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_WD_1(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_WD_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  V

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

!!! I08_WD_1 prend 1 param de plus (la colonne) et n'est par compatible

!      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
!         IERR = IO_PRXY_WD_1(HOBJ, V)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_WD_1(HOBJ, V)
!      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
!         IERR = IO_I08_WD_1(HOBJ, V)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_WD_1(HOBJ, V)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_WD_1(HOBJ, V)
!      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
!      ENDIF

      IO_WRTR_WD_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_WD_N(HOBJ, N, VX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_WD_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  VX(*)
      INTEGER IX

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_WD_N(HOBJ, N, VX, IX)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_WD_N(HOBJ, N, VX, IX)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_WD_N(HOBJ, N, VX, IX)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_WD_N(HOBJ, N, VX, IX)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_WD_N(HOBJ, N, VX, IX)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_WD_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_RS(HOBJ, S)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_RS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) S

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_RS(HOBJ, S)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_RS(HOBJ, S)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_RS(HOBJ, S)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_RS(HOBJ, S)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_RS(HOBJ, S)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_RS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_RI_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_RI_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_RI_1(HOBJ, I)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_RI_1(HOBJ, I)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_RI_1(HOBJ, I)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_RI_1(HOBJ, I)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_RI_1(HOBJ, I)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_RI_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_RI_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_RI_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      INTEGER KX(*)
      INTEGER IX

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_RI_N(HOBJ, N, KX, IX)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_RI_N(HOBJ, N, KX, IX)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_RI_N(HOBJ, N, KX, IX)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_RI_N(HOBJ, N, KX, IX)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_RI_N(HOBJ, N, KX, IX)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_RI_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_RD_1(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_RD_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  V

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_RD_1(HOBJ, V)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_RD_1(HOBJ, V)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_RD_1(HOBJ, V)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_RD_1(HOBJ, V)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_RD_1(HOBJ, V)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_RD_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_WRTR_RD_N(HOBJ, N, VX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_WRTR_RD_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  VX(*)
      INTEGER IX

      INCLUDE 'iowrtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fi'
!      INCLUDE 'ioxdr.fi'
      INCLUDE 'ioi08.fi'
!      INCLUDE 'ioi16.fi'
!      INCLUDE 'ioi32.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (IO_PRXY_HVALIDE(HOBJ)) THEN
         IERR = IO_PRXY_RD_N(HOBJ, N, VX, IX)
!      ELSEIF (IO_XDR_HVALIDE(HOBJ)) THEN
!         IERR = IO_XDR_RD_N(HOBJ, N, VX, IX)
      ELSEIF (IO_I08_HVALIDE(HOBJ)) THEN
         IERR = IO_I08_RD_N(HOBJ, N, VX, IX)
!      ELSEIF (IO_I16_HVALIDE(HOBJ)) THEN
!         IERR = IO_I16_RD_N(HOBJ, N, VX, IX)
!      ELSEIF (IO_I32_HVALIDE(HOBJ)) THEN
!         IERR = IO_I32_RD_N(HOBJ, N, VX, IX)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      IO_WRTR_RD_N = ERR_TYP()
      RETURN
      END
