C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_EXPR_000
C     INTEGER IC_EXPR_999
C     INTEGER IC_EXPR_PKL
C     INTEGER IC_EXPR_UPK
C     INTEGER IC_EXPR_CTR
C     INTEGER IC_EXPR_DTR
C     INTEGER IC_EXPR_INI
C     INTEGER IC_EXPR_RST
C     INTEGER IC_EXPR_REQHBASE
C     LOGICAL IC_EXPR_HVALIDE
C     INTEGER IC_EXPR_XEQ
C     INTEGER IC_EXPR_XEQ_L
C     INTEGER IC_EXPR_ASGCTX
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>IC_EXPR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icexpr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(IC_EXPR_NOBJMAX,
     &                   IC_EXPR_HBASE,
     &                   'Command Interpreter Expression')

      IC_EXPR_HSTC = 0

      IC_EXPR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icexpr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
      EXTERNAL IC_EXPR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(IC_EXPR_NOBJMAX,
     &                   IC_EXPR_HBASE,
     &                   IC_EXPR_DTR)

      IC_EXPR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_EXPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - IC_EXPR_HBASE

      IF (ERR_GOOD()) IERR = IO_XML_WS  (HXML, IC_EXPR_EXPR(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, IC_EXPR_HCTX(IOB))

      IC_EXPR_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_EXPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - IC_EXPR_HBASE

      IF (ERR_GOOD()) IERR = IO_XML_RS  (HXML, IC_EXPR_EXPR(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, IC_EXPR_HCTX(IOB))

      IC_EXPR_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icexpr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   IC_EXPR_NOBJMAX,
     &                   IC_EXPR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(IC_EXPR_HVALIDE(HOBJ))
         IOB = HOBJ - IC_EXPR_HBASE

         IC_EXPR_EXPR(IOB) = ' '
         IC_EXPR_HCTX(IOB) = 0
      ENDIF

      IC_EXPR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icexpr.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_EXPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = IC_EXPR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   IC_EXPR_NOBJMAX,
     &                   IC_EXPR_HBASE)

      IC_EXPR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_EXPR_INI initialise l'expression en lui passant son
C     contexte d'exécution HCTX ainsi que l'expression EXPR. Le handle du
C     contexte peut être nul, auquel cas un contexte local est utilisé.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HCTX     Handle sur le contexte d'exécution, un interpréteur
C     EXPR     Chaîne de l'expression
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_INI(HOBJ, HCTX, EXPR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HCTX
      CHARACTER*(*) EXPR

      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HTMP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_EXPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôles
      IF (SP_STRN_LEN(EXPR) .LE. 0) GOTO 9900
      IF (.NOT. (HCTX .EQ. 0 .OR. IC_ICMD_HVALIDE(HCTX))) GOTO 9901

C---     Reset les données
      IERR = IC_EXPR_RST(HOBJ)

C---     Pas interpréteur, assigne l'interpréteur statique
      HTMP = HCTX
      IF (HTMP .EQ. 0) HTMP = IC_EXPR_HSTC
D     CALL ERR_ASR(IC_ICMD_HVALIDE(HTMP))

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - IC_EXPR_HBASE
         IC_EXPR_EXPR(IOB) = EXPR(1:SP_STRN_LEN(EXPR))
         IC_EXPR_HCTX(IOB) = HTMP
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_EXPR_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_EXPR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_EXPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_EXPR_HBASE

C---     RESET LES PARAMETRES
      IC_EXPR_EXPR (IOB) = ' '
      IC_EXPR_HCTX (IOB) = 0

      IC_EXPR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction IC_EXPR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icexpr.fi'
      INCLUDE 'icexpr.fc'
C------------------------------------------------------------------------

      IC_EXPR_REQHBASE = IC_EXPR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction IC_EXPR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icexpr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icexpr.fc'
C------------------------------------------------------------------------

      IC_EXPR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  IC_EXPR_NOBJMAX,
     &                                  IC_EXPR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: IC_EXPR_XEQ
C
C Description:
C     La fonction IC_EXPR_XEQ appelle l'interpréteur de commandes HCTX pour
C     évaluer l'expression de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_XEQ (HOBJ, RSLT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) RSLT

      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HCTX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_EXPR_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - IC_EXPR_HBASE
      HCTX = IC_EXPR_HCTX(IOB)

C---     Évalue l'expression
      IERR = IC_ICMD_EVAL(HCTX, IC_EXPR_EXPR(IOB), RSLT)

      IC_EXPR_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_EXPR_XEQ_L
C
C Description:
C     La fonction IC_EXPR_XEQ appelle l'interpréteur de commandes HCTX pour
C     évaluer l'expression de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_XEQ_L (HOBJ, LRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_XEQ_L
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LRES

      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
      CHARACTER*(64) RSLT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_EXPR_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      CALL LOG_TODO('IC_EXPR_XEQ_L: EXPERIMENTAL !!!')
      IERR = IC_EXPR_XEQ(HOBJ, RSLT)
      IF (ERR_GOOD()) READ(RSLT, *, ERR=9900) LRES

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_LOGICAL', ': ',
     &                        RSLT(1:SP_STRN_LEN(RSLT))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_EXPR_XEQ_L = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_EXPR_ASGCTX
C
C Description:
C     La fonction statique IC_EXPR_ASGCTX assigne le contexte d'exécution
C     (interpréteur de commandes) par défaut HCTX pour évaluer les
C     expressions.
C
C Entrée:
C     HCTX     Handle sur le contexte par défaut
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_EXPR_ASGCTX (HCTX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_EXPR_ASGCTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HCTX

      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'icexpr.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

      IF (IC_ICMD_HVALIDE(HCTX)) THEN
         IC_EXPR_HSTC = HCTX
      ELSE
         WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ':', HCTX
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ENDIF

      IC_EXPR_ASGCTX = ERR_TYP()
      RETURN
      END
