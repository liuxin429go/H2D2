C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SO_MDUL_000
C     INTEGER SO_MDUL_999
C     INTEGER SO_MDUL_CTR
C     INTEGER SO_MDUL_DTR
C     INTEGER SO_MDUL_INI
C     INTEGER SO_MDUL_RST
C     INTEGER SO_MDUL_REQHBASE
C     LOGICAL SO_MDUL_HVALIDE
C     INTEGER SO_MDUL_CCHFNC
C     INTEGER SO_MDUL_REQFNC
C     INTEGER SO_MDUL_REQMTH
C     CHARACTER*64 SO_MDUL_REQNOM
C     CHARACTER*64 SO_MDUL_REQNOMTYP
C     CHARACTER*64 SO_MDUL_REQVER
C   Private:
C     SUBROUTINE SO_MDUL_REQSELF
C     INTEGER SO_MDUL_RAZ
C
C************************************************************************

      MODULE SO_MDUL_M

      IMPLICIT NONE

C---     Attributs statiques
      INTEGER, SAVE :: SO_MDUL_HBASE = 0
      INTEGER, SAVE :: SO_MDUL_NOBJ  = 0

C---     Attributs privés
      INTEGER, PARAMETER :: ILFIL = 32
      INTEGER, PARAMETER :: ILPRM = 48

      TYPE SO_MDUL_SELF_T
         CHARACTER*(ILPRM) FRML
         CHARACTER*(ILFIL) FDLL
         CHARACTER*(ILFIL) ROOT
         INTEGER*8         XDLL       ! handle eXterne
         INTEGER*8         XCCH       ! handle eXterne
         INTEGER           CODE
         INTEGER           ITYP
         INTEGER           HBSE
      END TYPE SO_MDUL_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée SO_MDUL_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (SO_MDUL_SELF_T), POINTER :: SO_MDUL_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      SO_MDUL_REQSELF => SELF
      RETURN
      END FUNCTION SO_MDUL_REQSELF

C------------------------------------------------------------------------
C------------------------------------------------------------------------
      END MODULE SO_MDUL_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction SO_MDUL_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_000
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INCLUDE 'somdul.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(SO_MDUL_HBASE,
     &                   'Shared-Object Module')

      SO_MDUL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction SO_MDUL_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_999
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INCLUDE 'somdul.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL SO_MDUL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(SO_MDUL_HBASE, SO_MDUL_DTR)

      SO_MDUL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction SO_MDUL_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_CTR
CDEC$ ENDIF

      USE ISO_C_BINDING
      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'somdul.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fc'

      INTEGER IERR, IRET
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   SO_MDUL_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = SO_MDUL_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. SO_MDUL_HVALIDE(HOBJ))
      IF (ERR_GOOD()) SO_MDUL_NOBJ = SO_MDUL_NOBJ + 1

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      SO_MDUL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode SO_MDUL_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_RAZ(HOBJ)

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER SO_MDUL_RAZ
      INTEGER HOBJ

      INCLUDE 'c_ds.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'spstrn.fi'

      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => SO_MDUL_REQSELF(HOBJ)

C---     Remise à zéro
      SELF%FRML = ' '
      SELF%FDLL = ' '
      SELF%ROOT = ' '
      SELF%CODE = -1
      SELF%ITYP = SO_MDUL_TYP_INDEFINI
      SELF%HBSE = 0
      SELF%XDLL = 0
      SELF%XCCH = 0

      SO_MDUL_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction SO_MDUL_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_DTR
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'somdul.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = SO_MDUL_RST(HOBJ)

C---     Dé-alloue
      SELF => SO_MDUL_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, SO_MDUL_HBASE)
      
      SO_MDUL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction SO_MDUL_INI initialise l'objet à l'aide des
C     paramètres. On charge la DLL pour assurer qu'elle reste chargée.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     ITYP        Type de module
C     ICOD        ID du module (facultatif)
C     FRML        Nom du module
C     FDLL        Nom du fichier DLL
C     ROOT        Racine des noms de fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_INI(HOBJ, ITYP, ICOD, FRML, FDLL, ROOT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_INI
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       ITYP
      INTEGER       ICOD
      CHARACTER*(*) FRML
      CHARACTER*(*) FDLL
      CHARACTER*(*) ROOT

      INCLUDE 'somdul.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER*8 XDLL
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
D     CALL ERR_PRE(ITYP .NE. SO_MDUL_TYP_INDEFINI)
D     CALL ERR_PRE(SP_STRN_LEN(FRML) .GT. 0)
D     CALL ERR_PRE(SP_STRN_LEN(FDLL) .GT. 0)
D     CALL ERR_PRE(SP_STRN_LEN(ROOT) .GT. 0)
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => SO_MDUL_REQSELF(HOBJ)

C---     CONTRÔLES
      IF (SP_STRN_LEN(FRML) .GT. ILPRM) GOTO 9900
      IF (SP_STRN_LEN(FDLL) .GT. ILFIL) GOTO 9901
      IF (SP_STRN_LEN(ROOT) .GT. ILFIL) GOTO 9902

C---     RESET LES DONNEES
      IERR = SO_MDUL_RST(HOBJ)

C---     CHARGE LE MODULE
      XDLL = 0
      IF (ERR_GOOD()) THEN
         IERR = C_SO_LOADMDL(XDLL, FDLL(1:SP_STRN_LEN(FDLL)))
         IF (IERR .NE. 0) GOTO 9903
      ENDIF

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         SELF%FRML = FRML(1:SP_STRN_LEN(FRML))
         SELF%FDLL = FDLL(1:SP_STRN_LEN(FDLL))
         SELF%ROOT = ROOT(1:SP_STRN_LEN(ROOT))
         SELF%CODE = ICOD
         SELF%ITYP = ITYP
         SELF%HBSE = 0
         SELF%XDLL = XDLL
         SELF%XCCH = 0
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_TROP_LONG', ': ',
     &                       FRML(1:SP_STRN_LEN(FRML))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_TROP_LONG', ': ',
     &                       FDLL(1:SP_STRN_LEN(FDLL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_TROP_LONG', ': ',
     &                       ROOT(1:SP_STRN_LEN(ROOT))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A)') 'ERR_CHARGE_DLL', ': ',
     &                       FDLL(1:SP_STRN_LEN(FDLL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_MDUL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_RST
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'somdul.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'somdul.fc'

      INTEGER       IERR, IRET
      INTEGER       I
      INTEGER       HFNC
      INTEGER*8     XCCH
      CHARACTER*256 BUF
      CHARACTER*12  VAL
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => SO_MDUL_REQSELF(HOBJ)

C---     Détruis la cache et son contenu
      IF (SELF%XCCH .NE. 0) THEN

         CALL ERR_PUSH()
         XCCH = SELF%XCCH
         DO I=1,C_MAP_REQDIM(XCCH)
            IRET = C_MAP_REQCLF(XCCH, I, BUF)
            IRET = C_MAP_REQVAL(XCCH, BUF(1:SP_STRN_LEN(BUF)), VAL)
            IF (IRET .EQ. 0) THEN
               READ(VAL, *, END=109, ERR=109) HFNC
               IF (SO_FUNC_HVALIDE(HFNC)) IERR = SO_FUNC_DTR(HFNC)
109            CONTINUE
            ENDIF
         ENDDO
         CALL ERR_POP()

         IRET = C_MAP_DTR(XCCH)
         IF (IRET .NE. 0) THEN
            BUF = SELF%FRML
            WRITE(ERR_BUF, '(3A)') 'ERR_DECHARGE_MODULE:',': ',
     &                              BUF(1:SP_STRN_LEN(BUF))
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ENDIF

      ENDIF

C---     Décharge le module
      IF (SELF%XDLL .NE. 0) THEN
         IRET = C_SO_FREEMDL(SELF%XDLL)
         IF (IRET .NE. 0) THEN
            BUF = SELF%FRML
            WRITE(ERR_BUF, '(3A)') 'ERR_DECHARGE_MODULE:',': ',
     &                              BUF(1:SP_STRN_LEN(BUF))
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ENDIF
      ENDIF

C---     Reset les paramètres
      IERR = SO_MDUL_RAZ(HOBJ)

      SO_MDUL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SO_MDUL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_REQHBASE
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER SO_MDUL_REQHBASE
C------------------------------------------------------------------------

      SO_MDUL_REQHBASE = SO_MDUL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SO_MDUL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_HVALIDE
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'somdul.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      SO_MDUL_HVALIDE = OB_OBJN_HVALIDE(HOBJ, SO_MDUL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne la fonction du module.
C
C Description:
C     La fonction SO_MDUL_CCHFNC retourne la fonction dont le
C     nom NOMF est passé en argument sous la forme d'un objet
C     SO_FUNC_M de handle HFNC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMF        Nom de la fonction
C
C Sortie:
C     HFNC        Handle sur la fonction
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_CCHFNC(HOBJ, NOMF, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_CCHFNC
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMF
      INTEGER       HFNC

      INCLUDE 'somdul.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sofunc.fi'

      INTEGER       IERR, IRET
      INTEGER*8     XCCH
      LOGICAL       LHIT
      CHARACTER*256 NOMC
      CHARACTER*256 ROOT
      CHARACTER*12  VAL
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => SO_MDUL_REQSELF(HOBJ)
      ROOT = SELF%ROOT
      XCCH = SELF%XCCH

C---     CONTROLES
      IF (SP_STRN_LEN(NOMF) .LE. 0) GOTO 9900
      IF (SP_STRN_LEN(ROOT) .LE. 0) GOTO 9901

C---     Monte le nom de la fonction
      NOMC = ROOT(1:SP_STRN_LEN(ROOT)) // NOMF(1:SP_STRN_LEN(NOMF))

C---     Crée la cache, ou cherche dans la cache
      LHIT = .FALSE.
      IF (XCCH .EQ. 0) THEN
         XCCH = C_MAP_CTR(XCCH)
         IF (XCCH .EQ. 0) GOTO 9902
         SELF%XCCH = XCCH
      ELSE
         IRET = C_MAP_REQVAL(XCCH, NOMC(1:SP_STRN_LEN(NOMC)), VAL)
         LHIT = (IRET .EQ. 0)
      ENDIF

      HFNC = 0
C---     Cache hit, décode le handle
      IF (LHIT) THEN
         READ(VAL, *, END=9903, ERR=9903) HFNC

C---     Cache miss, crée et charge la fonction
      ELSE
         IERR = SO_MDUL_REQFNC(HOBJ, NOMF, HFNC)
         IF (ERR_GOOD()) THEN
            WRITE(VAL, '(I12)') HFNC
            CALL SP_STRN_TRM(VAL)
            IRET = C_MAP_ASGVAL(XCCH,
     &                          NOMC(1:SP_STRN_LEN(NOMC)),
     &                          VAL(1:SP_STRN_LEN(VAL)))
         ENDIF
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_NOM_FNCT_VIDE')
      GOTO 9999
9901  CALL ERR_ASG(ERR_ERR, 'ERR_CHARGE_FNCT_DLL')
      CALL ERR_AJT('MSG_DLL_NON_CHARGEE')
      GOTO 9999
9902  CALL ERR_ASG(ERR_ERR, 'ERR_CTR_MAP')
      GOTO 9999
9903  CALL ERR_ASG(ERR_ERR, 'ERR_READ_VAL')
      GOTO 9999

9999  CONTINUE
      SO_MDUL_CCHFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne la fonction du module.
C
C Description:
C     La fonction SO_MDUL_REQFNC retourne la fonction dont le
C     nom NOMF est passé en argument sous la forme d'un objet
C     SO_FUNC_M de handle HFNC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMF        Nom de la fonction
C
C Sortie:
C     HFNC        Handle sur la fonction
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_REQFNC(HOBJ, NOMF, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_REQFNC
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMF
      INTEGER       HFNC

      INCLUDE 'somdul.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sofunc.fi'

      INTEGER       IERR
      CHARACTER*256 FDLL
      CHARACTER*256 NOMC
      CHARACTER*256 ROOT
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => SO_MDUL_REQSELF(HOBJ)
      FDLL = SELF%FDLL
      ROOT = SELF%ROOT

C---     CONTROLES
      IF (SP_STRN_LEN(NOMF) .LE. 0) GOTO 9900
      IF (SELF%XDLL .EQ. 0) GOTO 9901
      IF (SP_STRN_LEN(FDLL) .LE. 0) GOTO 9901
      IF (SP_STRN_LEN(ROOT) .LE. 0) GOTO 9901

C---     MONTE LE NOM DE LA FONCTION
      NOMC = ROOT(1:SP_STRN_LEN(ROOT)) // NOMF(1:SP_STRN_LEN(NOMF))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFSO(HFNC,
     &                                       FDLL(1:SP_STRN_LEN(FDLL)),
     &                                       NOMC(1:SP_STRN_LEN(NOMC)))
      IF (ERR_BAD() .AND. SO_FUNC_HVALIDE(HFNC)) THEN
         IERR = SO_FUNC_DTR(HFNC)
         HFNC = 0
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_NOM_FNCT_VIDE')
      GOTO 9999
9901  CALL ERR_ASG(ERR_ERR, 'ERR_CHARGE_FNCT_DLL')
      CALL ERR_AJT('MSG_DLL_NON_CHARGEE')
      GOTO 9999

9999  CONTINUE
      SO_MDUL_REQFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne la fonction du module.
C
C Description:
C     La fonction SO_MDUL_REQMTH retourne la fonction dont le
C     nom NOMF est passé en argument sous la forme d'un objet
C     SO_FUNC_M de handle HFNC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMF        Nom de la méthode
C     HSLF        Handle sur l'objet de la méthode
C
C Sortie:
C     HFNC        Handle sur la fonction
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_REQMTH(HOBJ, NOMF, HSLF, HMTH)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_REQMTH
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMF
      INTEGER       HSLF
      INTEGER       HMTH

      INCLUDE 'somdul.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sofunc.fi'

      INTEGER       IERR
      CHARACTER*256 FDLL
      CHARACTER*256 NOMC
      CHARACTER*256 ROOT
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => SO_MDUL_REQSELF(HOBJ)
      FDLL = SELF%FDLL
      ROOT = SELF%ROOT

C---     Contrôles
      IF (SP_STRN_LEN(NOMF) .LE. 0) GOTO 9900
      IF (SELF%XDLL .EQ. 0) GOTO 9901
      IF (SP_STRN_LEN(FDLL) .LE. 0) GOTO 9901
      IF (SP_STRN_LEN(ROOT) .LE. 0) GOTO 9901

C---     Monte le nom complet
      NOMC = ROOT(1:SP_STRN_LEN(ROOT)) // NOMF(1:SP_STRN_LEN(NOMF))

C---     Charge la méthode
      HMTH = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HMTH)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMSO(HMTH,
     &                                      HSLF,
     &                                      FDLL(1:SP_STRN_LEN(FDLL)),
     &                                      NOMC(1:SP_STRN_LEN(NOMC)))
      IF (ERR_BAD() .AND. SO_FUNC_HVALIDE(HMTH)) THEN
         IERR = SO_FUNC_DTR(HMTH)
         HMTH = 0
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_NOM_FNCT_VIDE')
      GOTO 9999
9901  CALL ERR_ASG(ERR_ERR, 'ERR_CHARGE_FNCT_DLL')
      CALL ERR_AJT('MSG_DLL_NON_CHARGEE')
      GOTO 9999

9999  CONTINUE
      SO_MDUL_REQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_MDUL_REQNOM retourne le nom du module.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_REQNOM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_REQNOM
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'somdul.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      CHARACTER*256 NOM
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_MDUL_REQSELF(HOBJ)
      NOM = SELF%FRML
      CALL SP_STRN_CLP(NOM, 64)

      SO_MDUL_REQNOM = NOM(1:SP_STRN_LEN(NOM))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_MDUL_REQNOMTYP retourne le nom du type du module.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_REQNOMTYP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_REQNOMTYP
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'somdul.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER ITP
      CHARACTER*256 NOM
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => SO_MDUL_REQSELF(HOBJ)
      ITP = SELF%ITYP

      IF (ITP .EQ. SO_MDUL_TYP_INDEFINI)  NOM = 'SO_MDUL_TYP_INDEFINI'
      IF (ITP .EQ. SO_MDUL_TYP_CMDMDL)    NOM = 'SO_MDUL_TYP_CMDMDL'
      IF (ITP .EQ. SO_MDUL_TYP_CMDOBJ)    NOM = 'SO_MDUL_TYP_CMDOBJ'
      IF (ITP .EQ. SO_MDUL_TYP_CMDFNC)    NOM = 'SO_MDUL_TYP_CMDFNC'
      IF (ITP .EQ. SO_MDUL_TYP_OBJECT)    NOM = 'SO_MDUL_TYP_OBJECT'
      IF (ITP .EQ. SO_MDUL_TYP_ELEMENT)   NOM = 'SO_MDUL_TYP_ELEMENT'
      IF (ITP .EQ. SO_MDUL_TYP_ELEMENT_BC)NOM = 'SO_MDUL_TYP_ELEMENT_BC'
      IF (ITP .EQ. SO_MDUL_TYP_GEOMETRIE) NOM = 'SO_MDUL_TYP_GEOMETRIE'
      IF (ITP .EQ. SO_MDUL_TYP_NUMINTEGRATION) NOM =
     &                                      'SO_MDUL_TYP_NUMINTEGRATION'

      CALL SP_STRN_CLP(NOM, 64)
      SO_MDUL_REQNOMTYP = NOM(1:SP_STRN_LEN(NOM))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_MDUL_REQTYP retourne le type du module.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_REQTYP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_REQTYP
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'somdul.fi'
      INCLUDE 'err.fi'

      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => SO_MDUL_REQSELF(HOBJ)
      SO_MDUL_REQTYP = SELF%ITYP
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_MDUL_REQVER retourne la version du module.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_MDUL_REQVER(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_MDUL_REQVER
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'somdul.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'c_os.fi'

      INTEGER IERR
      CHARACTER*256 DLL
      CHARACTER*256 VER
      TYPE (SO_MDUL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => SO_MDUL_REQSELF(HOBJ)

C---     DEMANDE LA VERSION
      DLL = SELF%FDLL
      IERR = C_OS_VERSION(DLL(1:SP_STRN_LEN(DLL)), VER)
      IF (IERR .EQ. 0) THEN
         CALL SP_STRN_CLP(VER, 64)
      ELSE
         VER = ' '
      ENDIF

      SO_MDUL_REQVER = VER(1:SP_STRN_LEN(VER))
      RETURN
      END
