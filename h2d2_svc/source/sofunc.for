C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C
C************************************************************************

      MODULE SO_FUNC_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: SO_FUNC_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: SO_FUNC_SELF_T
         INTEGER*8 XDLL       ! handle eXterne
         INTEGER*8 XFNC       ! handle eXterne
         INTEGER   HPRM       ! handle sur le paramètre
      END TYPE SO_FUNC_SELF_T

C---     Cache
      INTEGER, PARAMETER :: SO_FUNC_CACHE_SIZE = 4
      TYPE :: SO_FUNC_CACHE_T
         INTEGER :: HOBJ = 0
         TYPE (SO_FUNC_SELF_T), POINTER :: SELF => NULL()
      END TYPE SO_FUNC_CACHE_T
      TYPE(SO_FUNC_CACHE_T), SAVE, DIMENSION(:,:), ALLOCATABLE :: CACHE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée SO_FUNC_CCH_FIND(...) retourne le pointeur à
C     SELF associé à HOBJ. Elle retourne NULL() si HOBJ n'est pas dans
C     la cache (cache miss).
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_CCH_FIND(HOBJ)

      IMPLICIT NONE

      TYPE (SO_FUNC_SELF_T), POINTER :: SO_FUNC_CCH_FIND
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'

      TYPE (SO_FUNC_SELF_T), POINTER :: SELF

      INTEGER ID, IC
!$    INTEGER OMP_GET_THREAD_NUM
C-----------------------------------------------------------------------

      ID = 1
!$    ID = OMP_GET_THREAD_NUM() + 1
      SELF => NULL()
      DO IC=1,SO_FUNC_CACHE_SIZE
         IF (CACHE(IC,ID)%HOBJ .EQ. HOBJ) THEN
            SELF => CACHE(IC,ID)%SELF
            EXIT
         ENDIF
      ENDDO

      SO_FUNC_CCH_FIND => SELF
      RETURN
      END FUNCTION SO_FUNC_CCH_FIND

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée SO_FUNC_CCH_ADD(...) ajoute l'entrée dans la
C     cache en éliminant, si besoin est, l'entrée la plus vieille.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CCH_ADD(HOBJ, SELF)

      IMPLICIT NONE

      INTEGER, INTENT(IN):: HOBJ
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IC, ID
!$    INTEGER OMP_GET_THREAD_NUM
C-----------------------------------------------------------------------

      ID = 1
!$    ID = OMP_GET_THREAD_NUM() + 1
      DO IC=SO_FUNC_CACHE_SIZE,2,-1
         CACHE(IC,ID)%HOBJ =  CACHE(IC-1,ID)%HOBJ
         CACHE(IC,ID)%SELF => CACHE(IC-1,ID)%SELF
      ENDDO
      CACHE(1,ID)%HOBJ =  HOBJ
      CACHE(1,ID)%SELF => SELF

      SO_FUNC_CCH_ADD = 0
      RETURN
      END FUNCTION SO_FUNC_CCH_ADD

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée SO_FUNC_CCH_RMV(...) retire l'entrée de la cache.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CCH_RMV(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INTEGER IC, ID, II
!$    INTEGER OMP_GET_THREAD_NUM
C-----------------------------------------------------------------------

      ID = 1
!$    ID = OMP_GET_THREAD_NUM() + 1
      DO II=1,SO_FUNC_CACHE_SIZE
         IF (CACHE(II,ID)%HOBJ .EQ. HOBJ) EXIT
      ENDDO

      IF (II .LE. SO_FUNC_CACHE_SIZE) THEN
         DO IC=II,SO_FUNC_CACHE_SIZE-1
            CACHE(IC,ID)%HOBJ =  CACHE(IC+1,ID)%HOBJ
            CACHE(IC,ID)%SELF => CACHE(IC+1,ID)%SELF
         ENDDO
         CACHE(SO_FUNC_CACHE_SIZE,ID)%HOBJ = 0
         NULLIFY( CACHE(SO_FUNC_CACHE_SIZE,ID)%SELF )
      ENDIF

      SO_FUNC_CCH_RMV = 0
      RETURN
      END FUNCTION SO_FUNC_CCH_RMV

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée SO_FUNC_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C     La fonction enregistre automatiquement l'objet dans la cache.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (SO_FUNC_SELF_T), POINTER :: SO_FUNC_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      SELF => SO_FUNC_CCH_FIND(HOBJ)
      IF (.NOT. ASSOCIATED(SELF)) THEN
         CALL ERR_PUSH()
         IERR = OB_OBJN_REQDTA(HOBJ, CELF)
         CALL ERR_ASR(ERR_GOOD())
         CALL ERR_POP()
         CALL C_F_POINTER(CELF, SELF)
         IERR = SO_FUNC_CCH_ADD(HOBJ, SELF)
      ENDIF

      SO_FUNC_REQSELF => SELF
      RETURN
      END FUNCTION SO_FUNC_REQSELF

C************************************************************************
C Sommaire: Appel générique de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL fait un appel d'une fonction à nombre de
C     paramètres variable.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      RECURSIVE
     &INTEGER
     &FUNCTION SO_FUNC_CALL(HOBJ, B01, B02, B03, B04, B05,
     &                            B06, B07, B08, B09) RESULT(IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL
CDEC$ ENDIF

      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      BYTE, INTENT(IN), OPTIONAL :: B01(:)
      BYTE, INTENT(IN), OPTIONAL :: B02(:)
      BYTE, INTENT(IN), OPTIONAL :: B03(:)
      BYTE, INTENT(IN), OPTIONAL :: B04(:)
      BYTE, INTENT(IN), OPTIONAL :: B05(:)
      BYTE, INTENT(IN), OPTIONAL :: B06(:)
      BYTE, INTENT(IN), OPTIONAL :: B07(:)
      BYTE, INTENT(IN), OPTIONAL :: B08(:)
      BYTE, INTENT(IN), OPTIONAL :: B09(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      BYTE, POINTER :: B00(:)
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      IF (SELF%HPRM .NE. 0) THEN
D        CALL ERR_ASR(.NOT. PRESENT(B09))
         B00 => SO_ALLC_CST2B(SELF%HPRM)
         IERR = SO_FUNC_CALL(HOBJ, B00, B01, B02, B03, B04,
     &                             B05, B06, B07, B08)
      ELSE
         IF (PRESENT(B09)) THEN
            IERR = C_SO_CALL9(SELF%XFNC, B01, B02, B03, B04, B05,
     &                                   B06, B07, B08, B09)
         ELSEIF (PRESENT(B08)) THEN
            IERR = C_SO_CALL8(SELF%XFNC, B01, B02, B03, B04, B05,
     &                                   B06, B07, B08)
         ELSEIF (PRESENT(B07)) THEN
            IERR = C_SO_CALL7(SELF%XFNC, B01, B02, B03, B04, B05,
     &                                   B06, B07)
         ELSEIF (PRESENT(B06)) THEN
            IERR = C_SO_CALL6(SELF%XFNC, B01, B02, B03, B04, B05,
     &                                   B06)
         ELSEIF (PRESENT(B05)) THEN
            IERR = C_SO_CALL5(SELF%XFNC, B01, B02, B03, B04, B05)
         ELSEIF (PRESENT(B04)) THEN
            IERR = C_SO_CALL4(SELF%XFNC, B01, B02, B03, B04)
         ELSEIF (PRESENT(B03)) THEN
            IERR = C_SO_CALL3(SELF%XFNC, B01, B02, B03)
         ELSEIF (PRESENT(B02)) THEN
            IERR = C_SO_CALL2(SELF%XFNC, B01, B02)
         ELSE IF (PRESENT(B01)) THEN
            IERR = C_SO_CALL1(SELF%XFNC, B01)
         ELSE
            IERR = C_SO_CALL0(SELF%XFNC)
         ENDIF
      ENDIF

      IERR = ERR_TYP()
      RETURN
      END FUNCTION SO_FUNC_CALL

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL0 fait un appel d'une fonction sans
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL0(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL0
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL0 = C_SO_CALL0(SELF%XFNC)
      ELSE
         SO_FUNC_CALL0 = C_SO_CALL1(SELF%XFNC, HPRM)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL0

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL1 fait un appel d'une fonction à un
C     paramètre.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL1(HOBJ, P1)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      BYTE     P1(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL1 = C_SO_CALL1(SELF%XFNC, P1)
      ELSE
         SO_FUNC_CALL1 = C_SO_CALL2(SELF%XFNC, HPRM, P1)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL1

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL2 fait un appel d'une fonction à deux
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL2(HOBJ, P1, P2)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL2
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      BYTE     P1(:), P2(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL2 = C_SO_CALL2(SELF%XFNC, P1, P2)
      ELSE
         SO_FUNC_CALL2 = C_SO_CALL3(SELF%XFNC, HPRM, P1, P2)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL2

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL3 fait un appel d'une fonction à trois
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL3(HOBJ, P1, P2, P3)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL3
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      BYTE     P1(:), P2(:), P3(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL3 = C_SO_CALL3(SELF%XFNC, P1, P2, P3)
      ELSE
         SO_FUNC_CALL3 = C_SO_CALL4(SELF%XFNC, HPRM, P1, P2, P3)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL3

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL4 fait un appel d'une fonction à deux
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL4(HOBJ, P1, P2, P3, P4)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL4
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      BYTE     P1(:), P2(:), P3(:), P4(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL4 = C_SO_CALL4(SELF%XFNC, P1, P2, P3,
     &                                                 P4)
      ELSE
         SO_FUNC_CALL4 = C_SO_CALL5(SELF%XFNC, HPRM, P1, P2, P3,
     &                                                 P4)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL4

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL5 fait un appel d'une fonction à cinq
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL5(HOBJ, P1, P2, P3, P4, P5)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL5
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      BYTE     P1(:), P2(:), P3(:), P4(:), P5(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL5 = C_SO_CALL5(SELF%XFNC, P1, P2, P3,
     &                                                 P4, P5)
      ELSE
         SO_FUNC_CALL5 = C_SO_CALL6(SELF%XFNC, HPRM, P1, P2, P3,
     &                                                 P4, P5)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL5

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL6 fait un appel d'une fonction à six
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL6(HOBJ, P1, P2, P3, P4, P5, P6)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL6
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      BYTE     P1(:), P2(:), P3(:), P4(:), P5(:), P6(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL6 = C_SO_CALL6(SELF%XFNC, P1, P2, P3,
     &                                                 P4, P5, P6)
      ELSE
         SO_FUNC_CALL6 = C_SO_CALL7(SELF%XFNC, HPRM, P1, P2, P3,
     &                                                 P4, P5, P6)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL6

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL7 fait un appel d'une fonction à sept
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL7(HOBJ, P1, P2, P3, P4, P5, P6, P7)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL7
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      BYTE     P1(:), P2(:), P3(:), P4(:), P5(:), P6(:), P7(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL7 = C_SO_CALL7(SELF%XFNC, P1, P2, P3,
     &                                                 P4, P5, P6,
     &                                                 P7)
      ELSE
         SO_FUNC_CALL7 = C_SO_CALL8(SELF%XFNC, HPRM, P1, P2, P3,
     &                                                 P4, P5, P6,
     &                                                 P7)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL7

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL8 fait un appel d'une fonction à huit
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL8(HOBJ, P1, P2, P3, P4, P5,
     &                                     P6, P7, P8)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL8
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      BYTE     P1(:), P2(:), P3(:), P4(:), P5(:), P6(:), P7(:), P8(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL8 = C_SO_CALL8(SELF%XFNC, P1, P2, P3,
     &                                         P4, P5, P6,
     &                                         P7, P8)
      ELSE
         SO_FUNC_CALL8 = C_SO_CALL9(SELF%XFNC, HPRM, P1, P2, P3,
     &                                               P4, P5, P6,
     &                                               P7, P8)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL8

C************************************************************************
C Sommaire: Appel de fonction.
C
C Description:
C     La fonction SO_FUNC_CALL9 fait un appel d'une fonction à neuf
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     Pn          Paramètres de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CALL9(HOBJ, P1, P2, P3, P4, P5,
     &                                     P6, P7, P8, P9)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CALL9
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      BYTE     P1(:), P2(:), P3(:), P4(:), P5(:), P6(:), P7(:), P8(:)
      BYTE     P9(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CALL9 = C_SO_CALL9(SELF%XFNC, P1, P2, P3,
     &                                         P4, P5, P6,
     &                                         P7, P8, P9)
      ELSE
         CALL ERR_ASR(.FALSE.)
         SO_FUNC_CALL9 = ERR_FTL
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CALL9

C************************************************************************
C Sommaire: Appel générique de fonction via call-back.
C
C Description:
C     La fonction SO_FUNC_CBFNC fait un appel via call-back d'une fonction
C     avec paramètres variables. Elle permet de résoudre le pointeur
C     à la fonction maintenue par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C
C Sortie:
C
C Notes:
C************************************************************************
      RECURSIVE
     &INTEGER
     &FUNCTION SO_FUNC_CBFNC(HOBJ, CB, B01, B02, B03, B04, B05,
     &                                 B06, B07, B08) RESULT(IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC
CDEC$ ENDIF

      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER  :: CB
      EXTERNAL CB
      BYTE, INTENT(IN), OPTIONAL :: B01(:)
      BYTE, INTENT(IN), OPTIONAL :: B02(:)
      BYTE, INTENT(IN), OPTIONAL :: B03(:)
      BYTE, INTENT(IN), OPTIONAL :: B04(:)
      BYTE, INTENT(IN), OPTIONAL :: B05(:)
      BYTE, INTENT(IN), OPTIONAL :: B06(:)
      BYTE, INTENT(IN), OPTIONAL :: B07(:)
      BYTE, INTENT(IN), OPTIONAL :: B08(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      BYTE, POINTER :: B00(:)
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      IF (SELF%HPRM .NE. 0) THEN
D        CALL ERR_ASR(.NOT. PRESENT(B08))
         B00 => SO_ALLC_CST2B(SELF%HPRM)
         IERR = SO_FUNC_CBFNC(HOBJ, CB, B00, B01, B02, B03, B04,
     &                                  B05, B06, B07)
      ELSE
         IF (PRESENT(B08)) THEN
            IERR = C_SO_CBFNC8(SELF%XFNC, CB, B01, B02, B03, B04, B05,
     &                                        B06, B07, B08)
         ELSEIF (PRESENT(B07)) THEN
            IERR = C_SO_CBFNC7(SELF%XFNC, CB, B01, B02, B03, B04, B05,
     &                                        B06, B07)
         ELSEIF (PRESENT(B06)) THEN
            IERR = C_SO_CBFNC6(SELF%XFNC, CB, B01, B02, B03, B04, B05,
     &                                        B06)
         ELSEIF (PRESENT(B05)) THEN
            IERR = C_SO_CBFNC5(SELF%XFNC, CB, B01, B02, B03, B04, B05)
         ELSEIF (PRESENT(B04)) THEN
            IERR = C_SO_CBFNC4(SELF%XFNC, CB, B01, B02, B03, B04)
         ELSEIF (PRESENT(B03)) THEN
            IERR = C_SO_CBFNC3(SELF%XFNC, CB, B01, B02, B03)
         ELSEIF (PRESENT(B02)) THEN
            IERR = C_SO_CBFNC2(SELF%XFNC, CB, B01, B02)
         ELSE IF (PRESENT(B01)) THEN
            IERR = C_SO_CBFNC1(SELF%XFNC, CB, B01)
         ELSE
            IERR = C_SO_CBFNC0(SELF%XFNC, CB)
         ENDIF
      ENDIF

      IERR = ERR_TYP()
      RETURN
      END FUNCTION SO_FUNC_CBFNC

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_FUNC_CBFNC0 fait un appel via call-back d'une fonction
C     sans paramètres. Elle permet de résoudre le pointeur à la fonction
C     maintenue par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CBFNC0(HOBJ, CB)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC0
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  CB
      EXTERNAL CB

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CBFNC0 = C_SO_CBFNC0(CB, SELF%XFNC)
      ELSE
         SO_FUNC_CBFNC0 = C_SO_CBFNC1(CB, SELF%XFNC, HPRM)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CBFNC0

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_FUNC_CBFNC1 fait un appel d'une fonction à deux
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C     Pn          Paramètres de la fonction CB
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CBFNC1(HOBJ, CB, P1)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  CB
      EXTERNAL CB
      BYTE     P1(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      HPRM = SELF%HPRM
      IF (HPRM .EQ. 0) THEN
         SO_FUNC_CBFNC1 = C_SO_CBFNC1(CB, SELF%XFNC, P1)
      ELSE
         SO_FUNC_CBFNC1 = C_SO_CBFNC2(CB, SELF%XFNC, HPRM, P1)
      ENDIF
      RETURN
      END FUNCTION SO_FUNC_CBFNC1

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_FUNC_CBFNC2 fait un appel d'une fonction à deux
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C     Pn          Paramètres de la fonction CB
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CBFNC2(HOBJ, CB, P1, P2)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC2
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  CB
      EXTERNAL CB
      BYTE     P1(:), P2(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      IERR = C_SO_CBFNC2(CB, SELF%XFNC, P1, P2)

      SO_FUNC_CBFNC2 = ERR_TYP()
      RETURN
      END FUNCTION SO_FUNC_CBFNC2

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_FUNC_CBFNC3 fait un appel d'une fonction à trois
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C     Pn          Paramètres de la fonction CB
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CBFNC3(HOBJ, CB, P1, P2, P3)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC3
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  CB
      EXTERNAL CB
      BYTE     P1(:), P2(:), P3(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      IERR = C_SO_CBFNC3(CB, SELF%XFNC, P1, P2, P3)

      SO_FUNC_CBFNC3 = ERR_TYP()
      RETURN
      END FUNCTION SO_FUNC_CBFNC3

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_FUNC_CBFNC4 fait un appel d'une fonction à quatre
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C     Pn          Paramètres de la fonction CB
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CBFNC4(HOBJ, CB, P1, P2, P3, P4)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC4
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  CB
      EXTERNAL CB
      BYTE     P1(:), P2(:), P3(:), P4(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      IERR = C_SO_CBFNC4(CB, SELF%XFNC, P1, P2, P3, P4)

      SO_FUNC_CBFNC4 = ERR_TYP()
      RETURN
      END FUNCTION SO_FUNC_CBFNC4

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_FUNC_CBFNC5 fait un appel d'une fonction à cinq
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C     Pn          Paramètres de la fonction CB
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CBFNC5(HOBJ, CB, P1, P2, P3, P4, P5)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC5
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  CB
      EXTERNAL CB
      BYTE     P1(:), P2(:), P3(:), P4(:), P5(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      IERR = C_SO_CBFNC5(CB, SELF%XFNC, P1, P2, P3, P4, P5)

      SO_FUNC_CBFNC5 = ERR_TYP()
      RETURN
      END FUNCTION SO_FUNC_CBFNC5

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_FUNC_CBFNC6 fait un appel d'une fonction à six
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C     Pn          Paramètres de la fonction CB
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CBFNC6(HOBJ, CB, P1, P2, P3, P4, P5, P6)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC6
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  CB
      EXTERNAL CB
      BYTE     P1(:), P2(:), P3(:), P4(:), P5(:), P6(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      IERR = C_SO_CBFNC6(CB, SELF%XFNC, P1, P2, P3, P4, P5, P6)

      SO_FUNC_CBFNC6 = ERR_TYP()
      RETURN
      END FUNCTION SO_FUNC_CBFNC6

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_FUNC_CBFNC7 fait un appel d'une fonction à sept
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C     Pn          Paramètres de la fonction CB
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CBFNC7(HOBJ, CB, P1, P2, P3, P4, P5,
     &                                          P6, P7)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC7
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  CB
      EXTERNAL CB
      BYTE     P1(:), P2(:), P3(:), P4(:), P5(:), P6(:), P7(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      IERR = C_SO_CBFNC7(CB, SELF%XFNC, P1,P2,P3,P4,P5,P6,P7)

      SO_FUNC_CBFNC7 = ERR_TYP()
      RETURN
      END FUNCTION SO_FUNC_CBFNC7

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SO_FUNC_CBFNC8 fait un appel d'une fonction à huit
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     CB          Fonction call-back
C     Pn          Paramètres de la fonction CB
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_FUNC_CBFNC8(HOBJ, CB, P1, P2, P3, P4, P5,
     &                                          P6, P7, P8)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CBFNC8
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  CB
      EXTERNAL CB
      BYTE     P1(:), P2(:), P3(:), P4(:), P5(:), P6(:), P7(:), P8(:)

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      IERR = C_SO_CBFNC8(CB, SELF%XFNC, P1,P2,P3,P4,P5,P6,P7,P8)

      SO_FUNC_CBFNC8 = ERR_TYP()
      RETURN
      END FUNCTION SO_FUNC_CBFNC8

      END MODULE SO_FUNC_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction SO_FUNC_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_000
CDEC$ ENDIF

      USE SO_FUNC_M

      IMPLICIT NONE

      INCLUDE 'sofunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fc'

      INTEGER IERR
      INTEGER IRET
      INTEGER I1, I2

!$    INTEGER OMP_GET_MAX_THREADS
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(SO_FUNC_HBASE, 'Callable Function Object')

      I1 = SO_FUNC_CACHE_SIZE
      I2 = 1
!$    I2 = OMP_GET_MAX_THREADS()
      ALLOCATE ( CACHE(I1,I2), STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      SO_FUNC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction SO_FUNC_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_999
CDEC$ ENDIF

      USE SO_FUNC_M

      IMPLICIT NONE

      INCLUDE 'sofunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fc'

      INTEGER  IERR
      EXTERNAL SO_FUNC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(SO_FUNC_HBASE, SO_FUNC_DTR)

      SO_FUNC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction SO_FUNC_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C     C'est le premier appel à REQSELF qui va enregistrer l'objet dans
C     la cache. Par contre, il faut sortir l'objet explicitement dans
C     le destructeur.
C************************************************************************
      FUNCTION SO_FUNC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_CTR
CDEC$ ENDIF

      !!!USE OB_HNDL_M
      USE SO_FUNC_M
      USE ISO_C_BINDING

      IMPLICIT NONE

      INTEGER, INTENT(OUT):: HOBJ
      !!!TYPE(OB_HNDL_T), INTENT(OUT):: HOBJ

      INCLUDE 'sofunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sofunc.fc'

      INTEGER IERR, IRET
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   SO_FUNC_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = SO_FUNC_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. SO_FUNC_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      SO_FUNC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction SO_FUNC_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     C'est le premier appel à REQSELF qui va enregistrer l'objet dans
C     la cache. Par contre, il faut sortir l'objet explicitement dans
C     le destructeur.
C************************************************************************
      FUNCTION SO_FUNC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_DTR
CDEC$ ENDIF

      USE SO_FUNC_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sofunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER  IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Self avant, car REQSELF enregistre dans la cache
      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))

C---     Reset
      IERR = SO_FUNC_RST(HOBJ)
      IERR = SO_FUNC_CCH_RMV(HOBJ)

C---     Dé-alloue
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, SO_FUNC_HBASE)

      SO_FUNC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée SO_FUNC_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_RAZ(HOBJ)

      USE SO_FUNC_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sofunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fc'

      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les paramètres
      SELF => SO_FUNC_REQSELF(HOBJ)
      SELF%XDLL = 0
      SELF%XFNC = 0
      SELF%HPRM = 0

      SO_FUNC_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise une fonction de DLL.
C
C Description:
C     La fonction SO_FUNC_INIFSO initialise l'objet comme fonction d'une DLL
C     à l'aide des paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     FDLL        Nom du fichier DLL
C     FFNC        Nom de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_INIFSO(HOBJ, FDLL, FFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_INIFSO
CDEC$ ENDIF

      USE SO_FUNC_M

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FDLL
      CHARACTER*(*) FFNC

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sofunc.fc'

      INTEGER IERR
      INTEGER*8 XDLL
      INTEGER*8 XFNC
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FDLL) .GT. 0)
D     CALL ERR_PRE(SP_STRN_LEN(FFNC) .GT. 0)
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SO_FUNC_RST(HOBJ)

C---     Charge la DLL
      IF (ERR_GOOD()) THEN
         IERR = C_SO_LOADMDL(XDLL, FDLL(1:SP_STRN_LEN(FDLL)))
         IF (IERR .NE. 0) GOTO 9900
      ENDIF

C---     Charge la fonction
      IF (ERR_GOOD()) THEN
         IERR = C_SO_LOADFNC(XFNC, XDLL, FFNC(1:SP_STRN_LEN(FFNC)))
         IF (IERR .NE. 0) GOTO 9901
      ENDIF

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF => SO_FUNC_REQSELF(HOBJ)
         SELF%XDLL = XDLL
         SELF%XFNC = XFNC
         SELF%HPRM = 0
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_CHARGE_DLL', ': ',
     &                       FDLL(1:SP_STRN_LEN(FDLL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_CHARGE_FNCT_DLL',': ',
     &                       FFNC(1:SP_STRN_LEN(FFNC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_FUNC_INIFSO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise une fonction.
C
C Description:
C     La fonction SO_FUNC_INIFNC initialise l'objet comme fonction régulière
C     à l'aide des paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     F           Fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_INIFNC(HOBJ, F)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_INIFNC
CDEC$ ENDIF

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER F
      EXTERNAL F

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sofunc.fc'

      INTEGER   IERR
      INTEGER*8 XFNC
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = SO_FUNC_RST(HOBJ)

C---     CHARGE LA FONCTION
      IF (ERR_GOOD()) THEN
         IERR = C_SO_REQHFNC(XFNC, F)
         IF (IERR .NE. 0) GOTO 9900
      ENDIF

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         SELF => SO_FUNC_REQSELF(HOBJ)
         SELF%XDLL = 0
         SELF%XFNC = XFNC
         SELF%HPRM = 0
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_FNCT_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_FUNC_INIFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise une méthode.
C
C Description:
C     La fonction SO_FUNC_INIMSO initialise l'objet comme méthode d'une DLL
C     (Shared Object) à l'aide des paramètres. Une méthode est une fonction
C     qui prend comme premier paramètre le handle sur l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     H           Handle sur l'objet de la méthode
C     FDLL        Nom du fichier DLL
C     FFNC        Nom de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_INIMSO(HOBJ, H, FDLL, FFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_INIMSO
CDEC$ ENDIF

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       H
      CHARACTER*(*) FDLL
      CHARACTER*(*) FFNC

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sofunc.fc'

      INTEGER   IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     INITIALISE LA FONCTION
      IERR = SO_FUNC_INIFSO(HOBJ, FDLL, FFNC)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         SELF => SO_FUNC_REQSELF(HOBJ)
         SELF%HPRM = H
      ENDIF

      SO_FUNC_INIMSO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise une méthode.
C
C Description:
C     La fonction SO_FUNC_INIMTH initialise l'objet comme méthode régulière
C     à l'aide des paramètres. Une méthode est une fonction qui prend comme
C     premier paramètre le handle sur l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     H           Handle sur l'objet de la méthode
C     F           Pointeur sur la méthode
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_INIMTH(HOBJ, H, F)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_INIMTH
CDEC$ ENDIF

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER H
      INTEGER F
      EXTERNAL F

      INCLUDE 'sofunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fc'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     INITIALISE LA FONCTION
      IERR = SO_FUNC_INIFNC(HOBJ, F)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         SELF => SO_FUNC_REQSELF(HOBJ)
         SELF%HPRM = H
      ENDIF

      SO_FUNC_INIMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_RST
CDEC$ ENDIF

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sofunc.fc'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      SELF => SO_FUNC_REQSELF(HOBJ)

C---     DECHARGE LA FONCTION ET LE MODULE (DLL)
      IF (SELF%XDLL .NE. 0) THEN
         IF (SELF%XFNC .NE. 0) THEN
            IERR = C_SO_FREEFNC(SELF%XFNC)
            IF (IERR .NE. 0) GOTO 9900
         ENDIF
         IF (SELF%XDLL .NE. 0) THEN
            IERR = C_SO_FREEMDL(SELF%XDLL)
            IF (IERR .NE. 0) GOTO 9901
         ENDIF
      ENDIF

C---     RESET LES PARAMETRES
      IERR = SO_FUNC_RAZ(HOBJ)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DECHARGE_FONCTION'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_DECHARGE_MODULE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_FUNC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SO_FUNC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_HVALIDE
CDEC$ ENDIF

      USE SO_FUNC_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sofunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sofunc.fc'
C------------------------------------------------------------------------

      SO_FUNC_HVALIDE = OB_OBJN_HVALIDE(HOBJ,
     &                                  SO_FUNC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est une méthode
C
C Description:
C     La fonction SO_FUNC_ESTMTH permet de valider si l'objet est un
C     callable sur une méthode.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_ESTMTH(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_ESTMTH
CDEC$ ENDIF

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sofunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sofunc.fc'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      SO_FUNC_ESTMTH = (SELF%HPRM .NE. 0)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle, paramètre d'une méthode.
C
C Description:
C     La fonction SO_FUNC_REQPRM retourne le handle, paramètre d'une
C     méthode. Elle retourne 0 si l'objet est initialisé comme fonction.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_FUNC_REQHPRM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_REQHPRM
CDEC$ ENDIF

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sofunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sofunc.fc'

      INTEGER HPRM
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%XFNC .NE. 0)
      SO_FUNC_REQHPRM = SELF%HPRM
      RETURN
      END

C************************************************************************
C Sommaire: Experimental, pour simuler un callable
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SO_FUNC_REQFNC(HOBJ, F)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_FUNC_REQFNC
CDEC$ ENDIF

      USE SO_FUNC_M

      IMPLICIT NONE

      INTEGER  SO_FUNC_REQFNC
      INTEGER  HOBJ
      INTEGER  F
      EXTERNAL F

      INCLUDE 'sofunc.fi'
      INCLUDE 'c_so.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fc'

      INTEGER IERR
      TYPE (SO_FUNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SO_FUNC_REQSELF(HOBJ)
      IERR = C_SO_REQFNC(SELF%XFNC, F)

      SO_FUNC_REQFNC = ERR_TYP()
      RETURN
      END
