C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Interface de Commandes
C Objet:   PARSer
C
C Functions:
C   Public:
C     INTEGER IC_PARS_CMDPIL
C     INTEGER IC_PARS_ASGKW
C   Private:
C     INTEGER IC_PARS_CODEXP
C     INTEGER IC_PARS_DECEXP
C     INTEGER IC_PARS_LISTOK
C     INTEGER IC_PARS_E
C     INTEGER IC_PARS_P
C     INTEGER IC_PARS_P_PAR
C     INTEGER IC_PARS_P_BRK
C     INTEGER IC_PARS_POPOPR
C     INTEGER IC_PARS_PUSHOPR
C     LOGICAL IC_PARS_SUPOPR
C     INTEGER IC_PARS_PUSHVAL
C     LOGICAL IC_PARS_ESTKW
C     LOGICAL IC_PARS_ESTVAL
C     LOGICAL IC_PARS_ESTVAR
C     LOGICAL IC_PARS_ESTOPRBIN
C     LOGICAL IC_PARS_ESTOPRUNR
C     INTEGER IC_PARS_REQCODBIN
C     INTEGER IC_PARS_REQCODUNR
C     SUBROUTINE IC_PARS_REQTOKCOD
C     SUBROUTINE IC_PARS_DEBUG
C
C************************************************************************

C************************************************************************
C Sommaire: Transfert une commande dans la pile
C
C Description:
C     La fonction IC_PARS_CMDPIL parse l'expression de la chaîne CMDSTAT
C     et la retourne sous la forme d'une pile inverse.
C
C Entrée:
C      CHARACTER*(*) CMDSTAT              La chaîne à parser
C      INTEGER       PILE_MAX             La taille max de la pile
C
C Sortie:
C      INTEGER       PILE_SIZ             La dimension de la pile
C      INTEGER       PILE_TYP(PILE_MAX)   La table des operations
C      CHARACTER*(*) PILE_VAL(PILE_MAX)   La table des valeurs
C
C Notes:
C     http://www.engr.mun.ca/~theo/Misc/exp_parsing.htm
C
C     The shunting yard algorithm
C
C     The idea of the shunting yard algorithm is to keep operators on a stack
C     until we are sure we have parsed both their operands. The operands are
C     kept on a second stack. The shunting yard algorithm can be used to
C     directly evaluate expressions as they are parsed (it is commonly used in
C     electronic calculators for this task), to create a reverse Polish
C     notation translation of an infix expression, or to create an abstract
C     syntax tree. I'll create an abstract syntax tree, so my operand stacks
C     will contain trees.
C
C     When parsing for example x*y+z, we push x on the operand stack, * on the
C     operator stack, and y on the operand stack. When the + is read, we
C     compare it to the top of the operator stack, which is *. Since the + has
C     lower precedence than *, we know that both operands to the * have been
C     read and, in fact, will be on top of the operand stack. The operands are
C     popped, a new tree is built, *(a,b), and it is pushed on the operand
C     stack. Then the + is pushed on the operator stack. At the end of an
C     expression the remaining operators are put into trees with their
C     operands and that is that.
C
C************************************************************************
      FUNCTION IC_PARS_CMDPIL(CMDSTAT, HSTK)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PARS_CMDPIL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) CMDSTAT
      INTEGER       HSTK

      INCLUDE 'icpars.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi' !!tmp
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'

      INTEGER   POPR_MAX
      PARAMETER (POPR_MAX = 20)
      INTEGER   ILLIN
      PARAMETER (ILLIN = 1024)

      INTEGER           IERR
      INTEGER           IE
      INTEGER           IPSZ, ITYP, NTOK
      INTEGER           POPR_SIZ
      INTEGER           POPR_TYP(POPR_MAX)
      INTEGER           TOKDEB
      INTEGER           TOKFIN
      CHARACTER*(ILLIN) STRING
      CHARACTER*(256)   VAL
C----------------------------------------------------------------------------

C---     CONTROLES
      IF (SP_STRN_LEN(CMDSTAT) .LE. 0) GOTO 9999
      IF (SP_STRN_LEN(CMDSTAT) .GE. ILLIN) GOTO 9900

C---     PUSH UNE SENTINELLE SUR LE PILE DES OPERATEURS
      POPR_SIZ = 1
      POPR_TYP(POPR_SIZ) = IC_PILE_TYP_SENTINEL

C---     ENCODE L'EXPRESSION
      STRING = CMDSTAT(1:SP_STRN_LEN(CMDSTAT))
      CALL SP_STRN_SBC(STRING, CHAR_TAB, ' ')
      IERR = IC_PARS_CODEXP(STRING(1:SP_STRN_LEN(STRING)))
      STRING = STRING(1:SP_STRN_LEN(STRING)) // '|'

C---     PARSE L'EXPRESSION
D     CALL IC_PARS_DEBUG(' ')
D     CALL IC_PARS_DEBUG('--------------')
D     CALL IC_PARS_DEBUG('Statement: ' //
D    &                   STRING(1:MIN(128,SP_STRN_LEN(STRING))))
      IPSZ = IC_PILE_SIZ(HSTK)
      TOKDEB = 1
      TOKFIN = 0
100   CONTINUE
         IF (ERR_GOOD())
     &      IERR = IC_PARS_E(STRING(1:SP_STRN_LEN(STRING)),
     &                       TOKDEB,
     &                       TOKFIN,
     &                       POPR_MAX,
     &                       POPR_SIZ,
     &                       POPR_TYP,
     &                       HSTK)
         IF (ERR_BAD()) GOTO 199
         IF (TOKFIN+1 .GE. SP_STRN_LEN(STRING)) GOTO 199
         NTOK = IC_PILE_SIZ(HSTK) - IPSZ
         IF (NTOK .GT. 1) GOTO 9901
         IERR = IC_PILE_TOP(HSTK, VAL)
         ITYP = IC_PILE_VALTYP(VAL)
         IF (ITYP .NE. IC_PILE_TYP_MOTCLEF) GOTO 9901
         IERR = IC_PARS_PUSHVAL(HSTK, IC_PILE_TYP_OPKW, ' ')
      GOTO 100
199   CONTINUE

C---     AJOUTE DE L'INFO AUX ERREURS
      IF (ERR_BAD()) THEN
         STRING = CMDSTAT(1:SP_STRN_LEN(CMDSTAT))
         CALL SP_STRN_CLP(STRING, 80)
         CALL ERR_AJT(STRING(1:SP_STRN_LEN(STRING)))
      ENDIF

      GOTO 9999
C----------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(4A)') 'ERR_EXPRESSION_TROP_LONGUE', ': ',
     &                      CMDSTAT(1:40), '.....'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_TEXTE_SANS_SIGNIFICATION'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I6)') 'MSG_POSITION', ': ', TOKFIN+1
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(3A)') 'MSG_DANS', ': ',
     &                      STRING(1:SP_STRN_LEN(STRING))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PARS_CMDPIL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Encode l'expression
C
C Description:
C     La fonction privée IC_PARS_CODEXP encode l'expression. On encode les
C     exposants qui sont sous la forme '[eEdD][+-]' en 'd[^~]'. On encode
C     les '.' des nombres en ';'. On encode les opérations de comparaisons
C     de 2 caractères (==, !=, <=, >=).
C     <p>
C     Les expressions encodées peuvent être décodées par IC_PARS_DEXEXP.
C
C Entrée:
C
C Sortie:
C     CHARACTER*(*) STRING      Chaîne à encoder
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_CODEXP(STRING)

      IMPLICIT NONE

      CHARACTER*(*) STRING

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'

      INTEGER   IPOS, ISTRT
      INTEGER   LSTRING
      LOGICAL   INMBR, IQUOT1, IQUOT2
      CHARACTER CPREC, CACTU, CNEXT

      CHARACTER*12 TOKSEP
      PARAMETER (TOKSEP = '=,+-*/([| ')
C----------------------------------------------------------------------------

      LSTRING = SP_STRN_LEN(STRING)
      IF (LSTRING .LT. 3) GOTO 9999

      CPREC = STRING(1:1)
      CACTU = STRING(2:2)
      IPOS  = 2
      ISTRT = 1
      INMBR  = .FALSE.
      IQUOT1 = .FALSE.
      IQUOT2 = .FALSE.
100   CONTINUE
         CNEXT = STRING(IPOS+1:IPOS+1)

C---        Détecte les chaînes
         IF (CPREC .EQ. '''') IQUOT1 = .NOT. IQUOT1
         IF (CPREC .EQ. '"')  IQUOT2 = .NOT. IQUOT2
         IF (IQUOT1 .OR. IQUOT2) GOTO 199

C---        Détecte les nombres
         IF (INDEX(TOKSEP, CPREC) .GT. 0) THEN
            ISTRT = IPOS
            INMBR = .FALSE.
            IF ((CACTU .GE. '0' .AND. CACTU .LE. '9') .OR.
     &          CACTU .EQ. '.') INMBR = .TRUE.
         ENDIF

C---        Change les . dans les nombres
         IF (INMBR .AND. CACTU .EQ. '.') THEN
            STRING(IPOS:IPOS) = ';'
         ENDIF

C---        Change les exposants dans les nombres
         IF (INMBR .AND.
     &      (CPREC .GE. '0' .AND. CPREC .LE. '9') .OR.
     &       CPREC .EQ. '.') THEN
            IF (CACTU .NE. 'e' .AND. CACTU .NE. 'E' .AND.
     &          CACTU .NE. 'd' .AND. CACTU .NE. 'D') GOTO 189
            IF (CNEXT .NE. '+' .AND. CNEXT .NE. '-') GOTO 189

            CACTU = 'd'
            IF (CNEXT .EQ. '+') CNEXT = '^'
            IF (CNEXT .EQ. '-') CNEXT = '~'
            STRING(IPOS  :IPOS  ) = CACTU
            STRING(IPOS+1:IPOS+1) = CNEXT
189         CONTINUE
         ENDIF

C---        Change les opérateurs logiques
         IF (CACTU .EQ. '=' .AND. CNEXT .EQ. '=') THEN
            CACTU = '@'
            CNEXT = ' '
            STRING(IPOS  :IPOS  ) = CACTU
            STRING(IPOS+1:IPOS+1) = CNEXT
         ENDIF
         IF (CACTU .EQ. '!' .AND. CNEXT .EQ. '=') THEN
            CACTU = '%'
            CNEXT = ' '
            STRING(IPOS  :IPOS  ) = CACTU
            STRING(IPOS+1:IPOS+1) = CNEXT
         ENDIF
         IF (CACTU .EQ. '<' .AND. CNEXT .EQ. '=') THEN
            CACTU = '#'
            CNEXT = ' '
            STRING(IPOS  :IPOS  ) = CACTU
            STRING(IPOS+1:IPOS+1) = CNEXT
         ENDIF
         IF (CACTU .EQ. '>' .AND. CNEXT .EQ. '=') THEN
            CACTU = '$'
            CNEXT = ' '
            STRING(IPOS  :IPOS  ) = CACTU
            STRING(IPOS+1:IPOS+1) = CNEXT
         ENDIF
         IF (INDEX(TOKSEP, CNEXT) .GT. 0) THEN
            IF (STRING(ISTRT:IPOS) .EQ. 'and') THEN
               CACTU = ' '
               CNEXT = ' '
               STRING(IPOS-2:IPOS-2) = '&'
               STRING(IPOS-1:IPOS-1) = ' '
               STRING(IPOS  :IPOS  ) = ' '
            ENDIF
            IF (STRING(ISTRT:IPOS) .EQ. 'or') THEN
               CACTU = ' '
               CNEXT = ' '
               STRING(IPOS-1:IPOS-1) = '?'
               STRING(IPOS  :IPOS  ) = ' '
            ENDIF
            IF (STRING(ISTRT:IPOS) .EQ. 'not') THEN
               CACTU = ' '
               CNEXT = ' '
               STRING(IPOS-2:IPOS-2) = '!'
               STRING(IPOS-1:IPOS-1) = ' '
               STRING(IPOS  :IPOS  ) = ' '
            ENDIF
         ENDIF

199      CONTINUE
         CPREC = CACTU
         CACTU = CNEXT
         IPOS = IPOS + 1
      IF (IPOS .LT. LSTRING) GOTO 100

9999  CONTINUE
      IC_PARS_CODEXP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Décode l'expression
C
C Description:
C     La fonction privée IC_PARS_DECEXP décode l'expression précédemment
C     encodée par IC_PARS_ENCEXP. On décode les exposants qui sont sous
C     la forme '^[^~]' en 'E[+-]'.
C
C Entrée:
C     ITYP                      Le type de la valeur à décoder
C
C Sortie:
C     CHARACTER*(*) STRING      Chaîne à décoder
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_DECEXP(STRING, ITYP)

      IMPLICIT NONE

      CHARACTER*(*) STRING
      INTEGER       ITYP

      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'

      INTEGER I
      LOGICAL IQUOT1, IQUOT2
C----------------------------------------------------------------------------

      IF (ITYP .NE. IC_PILE_TYP_SVALEUR) THEN
         IQUOT1 = .FALSE.
         IQUOT2 = .FALSE.
         DO I=1,SP_STRN_LEN(STRING)
            IF (STRING(I:I) .EQ. '''') IQUOT1 = .NOT. IQUOT1
            IF (STRING(I:I) .EQ. '"')  IQUOT2 = .NOT. IQUOT2
            IF ((.NOT. IQUOT1) .AND. (.NOT. IQUOT2)) THEN
               IF (STRING(I:I) .EQ. ';') STRING(I:I) = '.'
               IF (STRING(I:I) .EQ. '^') STRING(I:I) = '+'
               IF (STRING(I:I) .EQ. '~') STRING(I:I) = '-'
            ENDIF
         ENDDO
      ENDIF

      IC_PARS_DECEXP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis le prochain token.
C
C Description:
C     La fonction privée IC_PARS_LISTOK lis le prochain token de la chaîne
C     XPRSTR. En entrée on spécifie la position du dernier token. En sortie,
C     ces positions sont remplacées par celle du prochain token.
C
C Entrée:
C     CHARACTER*(*) XPRSTR    Chaîne à scanner
C
C Sortie:
C     INTEGER TOKDEB          Position du début du prochain token
C     INTEGER TOKFIN          Position de la fin du prochain token
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_LISTOK(XPRSTR,
     &                        TOKDEB,
     &                        TOKFIN)

      IMPLICIT NONE

      CHARACTER*(*) XPRSTR
      INTEGER       TOKDEB
      INTEGER       TOKFIN

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'

      INTEGER IPOS
      INTEGER LSTR
      LOGICAL IQUOT1, IQUOT2
C----------------------------------------------------------------------------
D     CALL ERR_PRE(TOKDEB .LE. SP_STRN_LEN(XPRSTR))
D     CALL ERR_PRE(TOKFIN .LE. SP_STRN_LEN(XPRSTR))
D     CALL ERR_PRE(TOKFIN .EQ. 0 .OR. TOKFIN .GE. TOKDEB)
C----------------------------------------------------------------------------

      LSTR = SP_STRN_LEN(XPRSTR)
      IF (TOKFIN .EQ. LSTR) GOTO 9999

C---     RECHERCHE LE PREMIER NON BLANC
      TOKDEB = TOKFIN + 1
100   CONTINUE
         IF (XPRSTR(TOKDEB:TOKDEB) .NE. ' ') GOTO 199
         IF (TOKDEB .EQ. LSTR) GOTO 9900
         TOKDEB = TOKDEB + 1
         GOTO 100
199   CONTINUE
D     CALL ERR_ASR(TOKDEB .LE. SP_STRN_LEN(XPRSTR))

C---     OPERATEUR
      TOKFIN = TOKDEB
      IPOS = INDEX(IC_PARS_TOKSEP, XPRSTR(TOKFIN:TOKFIN))
      IF (IPOS .GT. 0) GOTO 9999

C---     LIS JUSQU'AU PROCHAIN SEPARATEUR
      IQUOT1 = .FALSE.
      IQUOT2 = .FALSE.
200   CONTINUE
         IF (TOKFIN .EQ. LSTR) GOTO 9900
         IF (XPRSTR(TOKFIN:TOKFIN) .EQ. '''') IQUOT1 = .NOT. IQUOT1
         IF (XPRSTR(TOKFIN:TOKFIN) .EQ. '"')  IQUOT2 = .NOT. IQUOT2
         IF ((.NOT. IQUOT1) .AND. (.NOT. IQUOT2)) THEN
            IPOS = INDEX(IC_PARS_TOKSEP, XPRSTR(TOKFIN+1:TOKFIN+1))
            IF (IPOS .GT. 0) GOTO 299
         ENDIF
         TOKFIN = TOKFIN + 1
         GOTO 200
299   CONTINUE
D     CALL ERR_ASR(TOKFIN .LE. SP_STRN_LEN(XPRSTR))
D     CALL IC_PARS_DEBUG('Token: ' // XPRSTR(TOKDEB:TOKFIN))

      GOTO 9999
C----------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FIN_EXPRESSION_INATTENDUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
D     CALL ERR_PST(ERR_BAD() .OR. TOKFIN .GE. TOKDEB)
      IC_PARS_LISTOK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Parse une expression.
C
C Description:
C     La fonction privée IC_PARS_E parse une expression.
C
C Entrée:
C     CHARACTER*(*) XPRSTR                La chaîne à parser
C     INTEGER       TOKDEB                   Position de début de token
C     INTEGER       TOKFIN                   Position de fin de token
C
C Sortie:
C     INTEGER       POPR_MAX              La pile des opérateurs
C     INTEGER       POPR_SIZ                 Taille
C     INTEGER       POPR_TYP(POPR_MAX)       Table des opérateurs
C     INTEGER       HSTK                  La pile des valeurs
C
C Notes:
C     Pseudo-code:
C     -----------
C     E( operators, operands ) is
C         P( operators, operands )
C         while next is a binary operator
C            pushOperator( binary(next), operators, operands )
C            consume
C            P( operators, operands )
C         while top(operators) not= sentinel
C            popOperator( operators, operands )
C
C     La fonction est ré-entrante. Les variables locales doivent
C     avoir une portée très locale car il n'est pas garanti que
C     le FORTRAN est récursif.
C************************************************************************
      RECURSIVE
     & FUNCTION IC_PARS_E(XPRSTR, TOKDEB, TOKFIN,
     &                    POPR_MAX, POPR_SIZ, POPR_TYP,
     &                    HSTK)

      IMPLICIT NONE

      CHARACTER*(*) XPRSTR
      INTEGER       TOKDEB
      INTEGER       TOKFIN
      INTEGER       POPR_MAX
      INTEGER       POPR_SIZ
      INTEGER       POPR_TYP(POPR_MAX)
      INTEGER       HSTK

      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icpars.fc'

      INTEGER IERR
      INTEGER IOPR
      INTEGER TMPDEB, TMPFIN
      LOGICAL SKIPTOK
C----------------------------------------------------------------------------

      IF (ERR_GOOD())
     &   IERR = IC_PARS_P(XPRSTR, TOKDEB, TOKFIN,
     &                    POPR_MAX, POPR_SIZ, POPR_TYP,
     &                    HSTK)

C---     TANT QUE L'OPERATEUR EST BINAIRE
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         TMPDEB = TOKDEB
         TMPFIN = TOKFIN
         IERR = IC_PARS_LISTOK(XPRSTR, TMPDEB, TMPFIN)
         IF (ERR_BAD()) GOTO 199

         IF (.NOT. IC_PARS_ESTOPRBIN(XPRSTR(TMPDEB:TMPFIN))) GOTO 199

         IF (ERR_GOOD()) THEN
            IOPR = IC_PARS_REQCODBIN(XPRSTR(TMPDEB:TMPFIN))
            SKIPTOK = (IOPR .EQ. IC_PILE_TYP_OPBRK .OR.
     &                 IOPR .EQ. IC_PILE_TYP_OPPAR)
            IF (.NOT. SKIPTOK) THEN
               IERR = IC_PARS_LISTOK(XPRSTR, TOKDEB, TOKFIN)
            ENDIF

            IERR=IC_PARS_PUSHOPR(IOPR,
     &                           POPR_MAX, POPR_SIZ, POPR_TYP,
     &                           HSTK)
         ENDIF
         IF (ERR_GOOD()) THEN
            IERR=IC_PARS_P(XPRSTR, TOKDEB, TOKFIN,
     &                     POPR_MAX, POPR_SIZ, POPR_TYP,
     &                     HSTK)
         ENDIF

      GOTO 100
199   CONTINUE

C---     DEPILE LES OPER JUSQU'A UN MARQUEUR SENTINEL
200   CONTINUE
         IF (ERR_BAD()) GOTO 299
         IF (POPR_TYP(POPR_SIZ) .EQ. IC_PILE_TYP_SENTINEL) GOTO 299
         IERR = IC_PARS_POPOPR(POPR_MAX, POPR_SIZ, POPR_TYP,
     &                         HSTK)
      GOTO 200
299   CONTINUE

      IC_PARS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_PARS_P est la fonction principale de
C     production.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Pseudo-code:
C     -----------
C     P( operators, operands ) is
C         if next is a v
C              push( operands, mkLeaf( v ) )
C              consume
C         else if next = "("
C              consume
C              push( operators, sentinel )
C              E( operators, operands )
C              expect( ")" )
C              pop( operators )
C         else if next is a unary operator
C              pushOperator( unary(next), operators, operands )
C              consume
C              P( operators, operands )
C         else
C              error
C
C     La fonction est ré-entrante. Les variables locales doivent
C     avoir une portée très locale car il n'est pas garanti que
C     le FORTRAN est récursif.
C************************************************************************
      RECURSIVE
     & FUNCTION IC_PARS_P(XPRSTR, TOKDEB, TOKFIN,
     &                    POPR_MAX, POPR_SIZ, POPR_TYP,
     &                    HSTK)

      IMPLICIT NONE

      CHARACTER*(*) XPRSTR
      INTEGER       TOKDEB
      INTEGER       TOKFIN
      INTEGER       POPR_MAX
      INTEGER       POPR_SIZ
      INTEGER       POPR_TYP(POPR_MAX)
      INTEGER       HSTK

      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'

      INTEGER IERR
      INTEGER IOPR
      INTEGER TMPDEB, TMPFIN
      LOGICAL ESTVAL, ESTVAR, ESTSTR, ESTDBL, ESTLBL
C----------------------------------------------------------------------------
      ESTVAL() = IC_PARS_ESTVAL(XPRSTR(TOKDEB:TOKFIN))
      ESTVAR() = IC_PARS_ESTVAR(XPRSTR(TOKDEB:TOKFIN))
      ESTSTR() = XPRSTR(TOKDEB:TOKDEB) .EQ. '''' .OR.
     &           XPRSTR(TOKDEB:TOKDEB) .EQ. '"'
      ESTDBL() = .NOT. ESTVAR() .AND. .NOT. ESTSTR() .AND.
     &           INDEX(XPRSTR(TOKDEB:TOKFIN), ';') .GT. 0
      ESTLBL() = ESTVAR() .AND. XPRSTR(TMPDEB:TMPFIN) .EQ. ':'
C----------------------------------------------------------------------------

10    CONTINUE

C---     TOKEN ET LOOK-AHEAD
      IERR = IC_PARS_LISTOK(XPRSTR, TOKDEB, TOKFIN)
      TMPDEB = TOKDEB
      TMPFIN = TOKFIN
      IERR = IC_PARS_LISTOK(XPRSTR, TMPDEB, TMPFIN)

C---     TOKEN TERMINAL
      IF (ESTVAL()) THEN
         IF     (ESTLBL()) THEN    ! LABEL
            IERR = IC_PARS_PUSHVAL(HSTK,
     &                             IC_PILE_TYP_LABEL,
     &                             XPRSTR(TOKDEB:TOKFIN))
            TOKFIN = TMPFIN
D           CALL IC_PARS_DEBUG('Push lbl: '//XPRSTR(TOKDEB:TOKFIN))
         ELSEIF (ESTVAR()) THEN    ! VARIABLE
            IERR = IC_PARS_PUSHVAL(HSTK,
     &                             IC_PILE_TYP_VARIABLE,
     &                             XPRSTR(TOKDEB:TOKFIN))
D           CALL IC_PARS_DEBUG('Push var: '//XPRSTR(TOKDEB:TOKFIN))
         ELSEIF (ESTSTR()) THEN    ! STRING
            IERR = IC_PARS_PUSHVAL(HSTK,
     &                             IC_PILE_TYP_SVALEUR,
     &                             XPRSTR(TOKDEB+1:TOKFIN-1))
D           CALL IC_PARS_DEBUG('Push str: '//XPRSTR(TOKDEB+1:TOKFIN-1))
         ELSEIF (ESTDBL()) THEN    ! DOUBLE
            IERR = IC_PARS_PUSHVAL(HSTK,
     &                             IC_PILE_TYP_RVALEUR,
     &                             XPRSTR(TOKDEB:TOKFIN))
D           CALL IC_PARS_DEBUG('Push dbl: '//XPRSTR(TOKDEB:TOKFIN))
         ELSE                      ! INTEGER
            IERR = IC_PARS_PUSHVAL(HSTK,
     &                             IC_PILE_TYP_IVALEUR,
     &                             XPRSTR(TOKDEB:TOKFIN))
D           CALL IC_PARS_DEBUG('Push int: '//XPRSTR(TOKDEB:TOKFIN))
         ENDIF

C---     PARENTHESE
      ELSEIF (XPRSTR(TOKDEB:TOKFIN) .EQ. '(') THEN
         IERR = IC_PARS_P_PAR(XPRSTR, TOKDEB, TOKFIN,
     &                        POPR_MAX, POPR_SIZ, POPR_TYP,
     &                        HSTK)

C---     BRACKET
      ELSEIF (XPRSTR(TOKDEB:TOKFIN) .EQ. '[') THEN
         IERR = IC_PARS_P_BRK(XPRSTR, TOKDEB, TOKFIN,
     &                        POPR_MAX, POPR_SIZ, POPR_TYP,
     &                        HSTK)

C---     OPERATEUR UNAIRE
      ELSEIF (IC_PARS_ESTOPRUNR(XPRSTR(TOKDEB:TOKFIN))) THEN
         IOPR = IC_PARS_REQCODUNR(XPRSTR(TOKDEB:TOKFIN))
         IERR = IC_PARS_PUSHOPR(IOPR,
     &                          POPR_MAX, POPR_SIZ, POPR_TYP,
     &                          HSTK)
C           ON COUPE ICI LA RÉCURSION  ...  IC_PARS_P()
         GOTO 10

      ELSE
         GOTO 9900
      ENDIF

      GOTO 9999
C----------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_ERREUR_DE_SYNTAXE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_TOKEN', ': ', XPRSTR(TOKDEB:TOKFIN)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PARS_P = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_PARS_P_PAR est la fonction de production pour
C     une expression entre parenthèses.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La fonction est ré-entrante. Les variables locales doivent
C     avoir une portée très locale car il n'est pas garanti que
C     le FORTRAN est récursif.
C************************************************************************
      RECURSIVE
     & FUNCTION IC_PARS_P_PAR(XPRSTR, TOKDEB, TOKFIN,
     &                        POPR_MAX, POPR_SIZ, POPR_TYP,
     &                        HSTK)

      IMPLICIT NONE

      CHARACTER*(*) XPRSTR
      INTEGER       TOKDEB
      INTEGER       TOKFIN
      INTEGER       POPR_MAX
      INTEGER       POPR_SIZ
      INTEGER       POPR_TYP(POPR_MAX)
      INTEGER       HSTK

      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'

      INTEGER IERR
      INTEGER TMPDEB, TMPFIN
      LOGICAL APARAM
C----------------------------------------------------------------------------
D     CALL ERR_PRE(XPRSTR(TOKDEB:TOKFIN) .EQ. '(')
C----------------------------------------------------------------------------

C---     PUSH OPER SENTINEL
      IF (ERR_GOOD()) THEN
         IF (POPR_SIZ .EQ. POPR_MAX) GOTO 9900
         POPR_SIZ = POPR_SIZ + 1
         POPR_TYP(POPR_SIZ) = IC_PILE_TYP_SENTINEL
      ENDIF

C---     LOOK-AHEAD POUR DETECTER LES PARENTHÈSES VIDES
      APARAM = .FALSE.
      IF (ERR_GOOD()) THEN
         TMPDEB = TOKDEB
         TMPFIN = TOKFIN
         IERR = IC_PARS_LISTOK(XPRSTR, TMPDEB, TMPFIN)
         APARAM = (XPRSTR(TMPDEB:TMPFIN) .NE. ')')
      ENDIF

C---     PARSE L'EXPRESSION OU POUSSE UNE CHAINE VIDE
      IF (ERR_GOOD()) THEN
         IF (APARAM) THEN
            IERR = IC_PARS_E(XPRSTR, TOKDEB, TOKFIN,
     &                       POPR_MAX, POPR_SIZ, POPR_TYP,
     &                       HSTK)
         ELSE
            IERR = IC_PARS_PUSHVAL(HSTK, IC_PILE_TYP_0VALEUR, ' ')
D           CALL IC_PARS_DEBUG('Push dummy arg')
         ENDIF
      ENDIF

C---     CONTROLE LA FIN
      IF (ERR_GOOD()) THEN
         IERR = IC_PARS_LISTOK(XPRSTR, TOKDEB, TOKFIN)
         IF (XPRSTR(TOKDEB:TOKFIN) .NE. ')') THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARENTHESE_ATTENDUE')
         ENDIF
      ENDIF

C---     POP OPER SENTINEL
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(POPR_SIZ .GT. 1)
D        CALL ERR_ASR(POPR_TYP(POPR_SIZ) .EQ. IC_PILE_TYP_SENTINEL)
         POPR_TYP(POPR_SIZ) = IC_PILE_TYP_NOOP
         POPR_SIZ = POPR_SIZ - 1
      ENDIF

      GOTO 9999
C----------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_PILE_OPER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_PILE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PARS_P_PAR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_PARS_P_BRK est la fonction de production pour
C     une expression entre bracket '[...]'.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La fonction est ré-entrante. Les variables locales doivent
C     avoir une portée très locale car il n'est pas garanti que
C     le FORTRAN est récursif.
C************************************************************************
      RECURSIVE
     & FUNCTION IC_PARS_P_BRK(XPRSTR, TOKDEB, TOKFIN,
     &                        POPR_MAX, POPR_SIZ, POPR_TYP,
     &                        HSTK)

      IMPLICIT NONE

      CHARACTER*(*) XPRSTR
      INTEGER       TOKDEB
      INTEGER       TOKFIN
      INTEGER       POPR_MAX
      INTEGER       POPR_SIZ
      INTEGER       POPR_TYP(POPR_MAX)
      INTEGER       HSTK

      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'

      INTEGER IERR
C----------------------------------------------------------------------------
D     CALL ERR_ASR(XPRSTR(TOKDEB:TOKFIN) .EQ. '[')
C----------------------------------------------------------------------------

C---     PUSH OPER SENTINEL
      IF (ERR_GOOD()) THEN
         IF (POPR_SIZ .EQ. POPR_MAX) GOTO 9900
         POPR_SIZ = POPR_SIZ + 1
         POPR_TYP(POPR_SIZ) = IC_PILE_TYP_SENTINEL
      ENDIF

C---     PARSE L'EXPRESSION
      IF (ERR_GOOD()) THEN
         IERR = IC_PARS_E(XPRSTR, TOKDEB, TOKFIN,
     &                    POPR_MAX, POPR_SIZ, POPR_TYP,
     &                    HSTK)
      ENDIF

C---     CONTROLE LA FIN
      IF (ERR_GOOD()) THEN
         IERR = IC_PARS_LISTOK(XPRSTR, TOKDEB, TOKFIN)
         IF (XPRSTR(TOKDEB:TOKFIN) .NE. ']') THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARENTHESE_ATTENDUE')
         ENDIF
      ENDIF

C---     POP OPER SENTINEL
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(POPR_SIZ .GT. 1)
D        CALL ERR_ASR(POPR_TYP(POPR_SIZ) .EQ. IC_PILE_TYP_SENTINEL)
         POPR_TYP(POPR_SIZ) = IC_PILE_TYP_NOOP
         POPR_SIZ = POPR_SIZ - 1
      ENDIF

!!!C---     PUSH OPER
!!!      Code pour tenter de supporter des listes à la Python
!!!      mais qui casse la détection des indices
!!!      IF (ERR_GOOD()) THEN
!!!         IERR = IC_PARS_PUSHOPR(IC_PILE_TYP_OPBRK,
!!!     &                          POPR_MAX, POPR_SIZ, POPR_TYP,
!!!     &                          HSTK)
!!!      ENDIF

      GOTO 9999
C----------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_PILE_OPER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PARS_P_BRK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dépile la pile des opérateurs
C
C Description:
C     La fonction privée IC_PARS_POPOPR dépile d'un opérateur la pile des
C     opérateurs et pousse cet opérateur sur la pile des résultat.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Pseudo-code:
C     -----------
C     popOperator( operators, operands ) is
C        if top(operators) is binary
C             const t1 <- pop( operands )
C             const t0 <- pop( operands )
C             push( operands, mkNode( pop(operators), t0, t1 ) )
C        else
C             push( operands, mkNode( pop(operators), pop(operands) ) )
C
C************************************************************************
      FUNCTION IC_PARS_POPOPR(POPR_MAX, POPR_SIZ, POPR_TYP,
     &                        HSTK)

      IMPLICIT NONE

      INTEGER POPR_MAX
      INTEGER POPR_SIZ
      INTEGER POPR_TYP(POPR_MAX)
      INTEGER HSTK

      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icpars.fc'

      INTEGER IERR
C----------------------------------------------------------------------------
D     CALL ERR_PRE(POPR_SIZ .GE. 1)
C----------------------------------------------------------------------------

      IERR = IC_PARS_PUSHVAL(HSTK, POPR_TYP(POPR_SIZ), ' ')
D     CALL IC_PARS_DEBUG('Push: opr ' //
D    &                    IC_PARS_REQTOKCOD(POPR_TYP(POPR_SIZ)))

      POPR_TYP(POPR_SIZ) = IC_PILE_TYP_NOOP
      POPR_SIZ = POPR_SIZ - 1

      IC_PARS_POPOPR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Pousse un opérateur sur la pile des opérateurs.
C
C Description:
C     La fonction privée IC_PARS_PUSHOPR pousse l'opérateur IOPR sur la
C     pile des opérateurs. En premier, on dépile les opérateurs de
C     précédence supérieure à IOPR.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Pseudo-code:
C     -----------
C     pushOperator( op, operators, operands ) is
C         while top(operators) > op
C            popOperator( operators, operands )
C         push( op, operators )
C
C************************************************************************
      FUNCTION IC_PARS_PUSHOPR(IOPR,
     &                         POPR_MAX, POPR_SIZ, POPR_TYP,
     &                         HSTK)

      IMPLICIT NONE

      INTEGER IOPR
      INTEGER POPR_MAX
      INTEGER POPR_SIZ
      INTEGER POPR_TYP(POPR_MAX)
      INTEGER HSTK

      INCLUDE 'err.fi'
      INCLUDE 'icpars.fc'

      INTEGER IERR
      INTEGER TOPOPR
C----------------------------------------------------------------------------

C---     DEPILE LES OPERATEURS DE PRECEDENCE > IOPR
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         TOPOPR = POPR_TYP(POPR_SIZ)
         IF (.NOT. IC_PARS_SUPOPR(TOPOPR, IOPR)) GOTO 199
         IERR = IC_PARS_POPOPR(POPR_MAX, POPR_SIZ, POPR_TYP,
     &                         HSTK)
         GOTO 100
199   CONTINUE

C---     PILE IOPR
      IF (ERR_GOOD()) THEN
         IF (POPR_SIZ .EQ. POPR_MAX) GOTO 9900
         POPR_SIZ = POPR_SIZ + 1
         POPR_TYP(POPR_SIZ) = IOPR
      ENDIF

      GOTO 9999
C----------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_PILE_OPER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PARS_PUSHOPR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Compare deux opérateurs.
C
C Description:
C     La fonction privée IC_PARS_SUPOPR compare deux opérateurs et
C     retourne .TRUE. si l'opérateur OP1 à une précédence strictement
C     supérieure à celle de l'opérateur OP1.
C     <p>
C     Opérateurs: précédence, associativité et valeur
C        b  ^     r  7
C        b  */    l  6
C        u  +-    r  5
C        b  +-    l  4
C        b  ,     l  3
C        b  =     r  2
C        sentinel    1
C
C Entrée:
C     INTEGER OP1          Les opérateurs à comparer
C     INTEGER OP2
C
C Sortie:
C
C Notes:
C     This comparison is defined as follows:
C
C     1) binary(x) > binary(y), if x has higher precedence than y,
C        or x is left associative and x and y have equal precedence.
C     2) unary(x) > binary(y), if x has precedence higher or equal to y's
C     3) op > unary(y), never (where op is any unary or binary operator)
C     4) sentinel > op, never (where op is any unary or binary operator)
C     5) op > sentinel  (where op is any unary or binary operator):
C        This case doesn't arise.
C************************************************************************
      FUNCTION IC_PARS_SUPOPR(OP1, OP2)

      IMPLICIT NONE

      INTEGER OP1
      INTEGER OP2

      INCLUDE 'icpile.fi'
      INCLUDE 'icpars.fc'

      INTEGER OP
      INTEGER PREC1, PREC2
      INTEGER REQPREC
      LOGICAL ESTUNR
      LOGICAL ESTLFT
      LOGICAL RES
C----------------------------------------------------------------------------
      REQPREC(OP) = (OP / 10000)
      ESTUNR (OP) = (((OP/100)*100  - (OP/1000)*1000) .EQ. 1)
      ESTLFT (OP) = (((OP/1000)*1000 - (OP/10000)*10000) .EQ. 0)
C----------------------------------------------------------------------------

      IF (ESTUNR(OP2)) THEN
         RES = .FALSE.
      ELSE
         PREC1 = REQPREC(OP1)
         PREC2 = REQPREC(OP2)
         IF (PREC1 .GT. PREC2) THEN
            RES = .TRUE.
         ELSEIF (PREC1 .EQ. PREC2) THEN
            RES = ESTLFT(OP1)
         ELSE
            RES = .FALSE.
         ENDIF
      ENDIF

      IC_PARS_SUPOPR = RES
      RETURN
      END

C************************************************************************
C Sommaire: Pousse une valeur sur la pile d'exécution.
C
C Description:
C     La fonction privée IC_PARS_PUSHVAL pousse la valeur et son type
C     sur la pile d'exécution. La valeur est décodée.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_PUSHVAL(HSTK, ITP, VAL)

      IMPLICIT NONE

      INTEGER       HSTK
      INTEGER       ITP
      CHARACTER*(*) VAL

      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'

      INTEGER IERR
      INTEGER ITL
      CHARACTER*(256) BUF
C----------------------------------------------------------------------------

      ITL = ITP
      IF (SP_STRN_LEN(VAL) .GT. 0) THEN
         BUF = VAL(1:SP_STRN_LEN(VAL))
         IERR = IC_PARS_DECEXP(BUF, ITL)

         IF (ITL .EQ. IC_PILE_TYP_VARIABLE) THEN
            IF (IC_PARS_ESTKW(BUF)) THEN
               ITL = IC_PILE_TYP_MOTCLEF
            ENDIF
         ENDIF

         IERR = IC_PILE_PUSHE(HSTK, BUF(1:SP_STRN_LEN(BUF)), ITL)
      ELSE
         IERR = IC_PILE_PUSHE(HSTK, ' ', ITL)
      ENDIF

      IC_PARS_PUSHVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Teste pour un mot clef.
C
C Description:
C     La fonction privée statique <code>IC_PARS_ESTKW(...)</code> retourne
C     .TRUE. si le mot passé en argument est un mot clef.
C
C Entrée:
C     KW      Nom à tester
C
C Sortie:
C
C Notes:
C     Écriture ampoulée à cause de g77. Ce que l'on test c'est:
C     RES = ((INDEX(IC_ICMD_KW, '@' // KW(1:LKW) // '@') .GT. 0) .OR.
C    &       (INDEX(IC_ICMD_CW, '@' // KW(1:LKW) // '@') .GT. 0))
C************************************************************************
      FUNCTION IC_PARS_ESTKW(TOK)

      IMPLICIT NONE

      CHARACTER*(*) TOK

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'

      INTEGER ITK, LTK
      LOGICAL RES
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(TOK) .GT. 0)
C-----------------------------------------------------------------------

      RES = .FALSE.

      LTK = SP_STRN_LEN(TOK)
      ITK = INDEX(IC_PARS_KEYWD, TOK(1:LTK))
      IF (ITK .GT. 0) THEN
         IF (IC_PARS_KEYWD(ITK-  1:ITK-  1) .EQ. '@' .AND.
     &       IC_PARS_KEYWD(ITK+LTK:ITK+LTK) .EQ. '@') RES = .TRUE.
      ENDIF

      IC_PARS_ESTKW = RES
      RETURN
      END

C************************************************************************
C Sommaire: Test le token pour une valeur.
C
C Description:
C     La fonction privée IC_PARS_ESTVAL retourne .TRUE. si le token TOK
C     correspond à une valeur (token terminal) valide.
C
C Entrée:
C     CHARACTER*(*) TOK       Le token à valider
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_ESTVAL(TOK)

      IMPLICIT NONE

      CHARACTER*(*) TOK

      INCLUDE 'icpars.fc'

      LOGICAL RES
C----------------------------------------------------------------------------

      RES = .FALSE.
      IF (TOK(1:1) .EQ. '''') RES = .TRUE.
      IF (TOK(1:1) .EQ. '"')  RES = .TRUE.
      IF (TOK(1:1) .GE. 'a' .AND. TOK(1:1) .LE. 'z') RES = .TRUE.
      IF (TOK(1:1) .GE. 'A' .AND. TOK(1:1) .LE. 'Z') RES = .TRUE.
      IF (TOK(1:1) .EQ. '_') RES = .TRUE.
      IF (TOK(1:1) .GE. '0' .AND. TOK(1:1) .LE. '9') RES = .TRUE.
      IF (TOK(1:1) .EQ. ';') RES = .TRUE.

      IC_PARS_ESTVAL = RES
      RETURN
      END

C************************************************************************
C Sommaire: Test le token pour une variable.
C
C Description:
C     La fonction privée IC_PARS_ESTVAR retourne .TRUE. si le token TOK
C     correspond à un nom de variable valide.
C
C Entrée:
C     CHARACTER*(*) TOK       Le token à valider
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_ESTVAR(TOK)

      IMPLICIT NONE

      CHARACTER*(*) TOK

      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'
C----------------------------------------------------------------------------

      IC_PARS_ESTVAR = SP_STRN_VAR(TOK)
      RETURN
      END

C************************************************************************
C Sommaire: Test le token pour un opérateur binaire.
C
C Description:
C     La fonction privée IC_PARS_ESTOPRBIN retourne .TRUE. si le token TOK
C     correspond à une opération binaire valide.
C
C Entrée:
C     CHARACTER*(*) TOK       Le token à valider
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_ESTOPRBIN(TOK)

      IMPLICIT NONE

      CHARACTER*(*) TOK

      INCLUDE 'icpile.fi'
      INCLUDE 'icpars.fc'
C----------------------------------------------------------------------------

      IC_PARS_ESTOPRBIN = (IC_PARS_REQCODBIN(TOK) .NE. IC_PILE_TYP_NOOP)
      RETURN
      END

C************************************************************************
C Sommaire: Test le token pour un opérateur unaire.
C
C Description:
C     La fonction privée IC_PARS_ESTOPRUNR retourne .TRUE. si le token TOK
C     correspond à une opération unaire valide.
C
C Entrée:
C     CHARACTER*(*) TOK       Le token à valider
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_ESTOPRUNR(TOK)

      IMPLICIT NONE

      CHARACTER*(*) TOK

      INCLUDE 'icpile.fi'
      INCLUDE 'icpars.fc'
C----------------------------------------------------------------------------

      IC_PARS_ESTOPRUNR = (IC_PARS_REQCODUNR(TOK) .NE. IC_PILE_TYP_NOOP)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le code opération binaire d'un token
C
C Description:
C     La fonction privée IC_PARS_REQCODBIN retourne le code de
C     l'opération binaire correspondant au token TOK.
C
C Entrée:
C     CHARACTER*(*) TOK          Le token à traduire
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_REQCODBIN(TOK)

      IMPLICIT NONE

      CHARACTER*(*) TOK

      INCLUDE 'icpile.fi'
      INCLUDE 'icpars.fc'

      INTEGER COD
C----------------------------------------------------------------------------

      COD = IC_PILE_TYP_NOOP
      IF (TOK(1:1) .EQ. '=') COD = IC_PILE_TYP_OPASG
      IF (TOK(1:1) .EQ. ',') COD = IC_PILE_TYP_OPVRG
      IF (TOK(1:1) .EQ. '+') COD = IC_PILE_TYP_OPADD
      IF (TOK(1:1) .EQ. '-') COD = IC_PILE_TYP_OPSUB
      IF (TOK(1:1) .EQ. '*') COD = IC_PILE_TYP_OPMUL
      IF (TOK(1:1) .EQ. '/') COD = IC_PILE_TYP_OPDIV
      IF (TOK(1:1) .EQ. '[') COD = IC_PILE_TYP_OPBRK
      IF (TOK(1:1) .EQ. '.') COD = IC_PILE_TYP_OPDOT
      IF (TOK(1:1) .EQ. '(') COD = IC_PILE_TYP_OPPAR
      IF (TOK(1:1) .EQ. '<') COD = IC_PILE_TYP_OPLLT     ! <
      IF (TOK(1:1) .EQ. '>') COD = IC_PILE_TYP_OPLGT     ! >
      IF (TOK(1:1) .EQ. '#') COD = IC_PILE_TYP_OPLLE     ! <=
      IF (TOK(1:1) .EQ. '$') COD = IC_PILE_TYP_OPLGE     ! >=
      IF (TOK(1:1) .EQ. '@') COD = IC_PILE_TYP_OPLEQ     ! ==
      IF (TOK(1:1) .EQ. '%') COD = IC_PILE_TYP_OPLNE     ! !=
      IF (TOK(1:1) .EQ. '&') COD = IC_PILE_TYP_OPAND     ! and
      IF (TOK(1:1) .EQ. '?') COD = IC_PILE_TYP_OPOR      ! or

      IC_PARS_REQCODBIN = COD
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le code opération unaire d'un token
C
C Description:
C     La fonction privée IC_PARS_REQCODBIN retourne le code de
C     l'opération unaire correspondant au token TOK.
C
C Entrée:
C     CHARACTER*(*) TOK          Le token à traduire
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_REQCODUNR(TOK)

      IMPLICIT NONE

      CHARACTER*(*) TOK

      INCLUDE 'icpile.fi'
      INCLUDE 'icpars.fc'

      INTEGER COD
C----------------------------------------------------------------------------

      COD = IC_PILE_TYP_NOOP
      IF (TOK(1:1) .EQ. '+') COD = IC_PILE_TYP_OPPOS
      IF (TOK(1:1) .EQ. '-') COD = IC_PILE_TYP_OPNEG
      IF (TOK(1:1) .EQ. '!') COD = IC_PILE_TYP_OPNOT

      IC_PARS_REQCODUNR = COD
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le token du code
C
C Description:
C     La fonction privée IC_PARS_REQTOKCOD retourne sous la forme d'un
C     CHARACTER le token correspondant au code COD.
C
C Entrée:
C     INTEGER COD          Le code à traduire
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_REQTOKCOD(COD)

      IMPLICIT NONE

      INTEGER COD

      INCLUDE 'icpile.fi'
      INCLUDE 'icpars.fc'

      CHARACTER TOK
C----------------------------------------------------------------------------

      TOK = '0'
      IF (COD .EQ. IC_PILE_TYP_OPASG) TOK = '='
      IF (COD .EQ. IC_PILE_TYP_OPVRG) TOK = ','
      IF (COD .EQ. IC_PILE_TYP_OPADD) TOK = '+'
      IF (COD .EQ. IC_PILE_TYP_OPSUB) TOK = '-'
      IF (COD .EQ. IC_PILE_TYP_OPMUL) TOK = '*'
      IF (COD .EQ. IC_PILE_TYP_OPDIV) TOK = '/'
      IF (COD .EQ. IC_PILE_TYP_OPNEG) TOK = '~'
      IF (COD .EQ. IC_PILE_TYP_OPBRK) TOK = '['
      IF (COD .EQ. IC_PILE_TYP_OPLST) TOK = 'L'
      IF (COD .EQ. IC_PILE_TYP_OPDOT) TOK = '.'
      IF (COD .EQ. IC_PILE_TYP_OPPAR) TOK = '('

      IF (COD .EQ. IC_PILE_TYP_OPLLT) TOK = '<'
      IF (COD .EQ. IC_PILE_TYP_OPLGT) TOK = '>'
      IF (COD .EQ. IC_PILE_TYP_OPLLE) TOK = '#'
      IF (COD .EQ. IC_PILE_TYP_OPLGE) TOK = '$'
      IF (COD .EQ. IC_PILE_TYP_OPLEQ) TOK = '@'
      IF (COD .EQ. IC_PILE_TYP_OPLNE) TOK = '%'
      IF (COD .EQ. IC_PILE_TYP_OPAND) TOK = '&'
      IF (COD .EQ. IC_PILE_TYP_OPOR)  TOK = '?'
      IF (COD .EQ. IC_PILE_TYP_OPNOT) TOK = '!'

      IC_PARS_REQTOKCOD = TOK
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_PARS_DEBUG
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PARS_DEBUG(STRING)

      IMPLICIT NONE

      CHARACTER*(*) STRING

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpars.fc'
C----------------------------------------------------------------------------

      CALL LOG_DEBUG(STRING)

      RETURN
      END

C************************************************************************
C Sommaire: Assigne la liste des mots clefs.
C
C Description:
C     La fonction statique IC_PARS_ASGKW permet d'assigner la liste
C     des mots clefs du parser.
C
C Entrée:
C     CHARACTER*(*) KW     Liste des mots clefs, séparés par de '@'
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PARS_ASGKW(KW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PARS_ASGKW
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) KW

      INCLUDE 'icpars.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpars.fc'
C----------------------------------------------------------------------------

      IC_PARS_KEYWD = KW

      IC_PARS_ASGKW = ERR_TYP()
      RETURN
      END
