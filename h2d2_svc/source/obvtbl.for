C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     La classe OB_VTBL maintient à la fois une table virtuelle pour
C     les méthodes et un dictionnaire pour des paires clef/valeurs.
C     Elle permet de conserver l'information reliée à une structure
C     virtuelle, tant les méthodes (VTBL) que les héritiers et les
C     parents (dictionnaire)
C
C Functions:
C   Public:
C     INTEGER OB_VTBL_000
C     INTEGER OB_VTBL_999
C     INTEGER OB_VTBL_CTR
C     INTEGER OB_VTBL_DTR
C     INTEGER OB_VTBL_INI
C     INTEGER OB_VTBL_RST
C     LOGICAL OB_VTBL_HVALIDE
C     INTEGER OB_VTBL_AJTxxx
C     INTEGER OB_VTBL_REQxxx
C     INTEGER OB_VTBL_AJTMSO
C     INTEGER OB_VTBL_REQMTH
C   Private:
C     SUBROUTINE OB_VTBL_REQSELF
C
C************************************************************************

      MODULE OB_VTBL_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: OB_VTBL_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: OB_VTBL_T
         INTEGER HVTBL
         INTEGER HCLSS
      END TYPE OB_VTBL_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction protégée OB_VTBL_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (OB_VTBL_T), POINTER :: OB_VTBL_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (OB_VTBL_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_GOOD())
C------------------------------------------------------------------------

      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL C_F_POINTER(CELF, SELF)

      OB_VTBL_REQSELF => SELF
      RETURN
      END FUNCTION OB_VTBL_REQSELF

      END MODULE OB_VTBL_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_000
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INCLUDE 'obvtbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(OB_VTBL_HBASE, 'Class virtual structure')

      OB_VTBL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_999
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INCLUDE 'obvtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER  IERR
      EXTERNAL OB_VTBL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(OB_VTBL_HBASE, OB_VTBL_DTR)

      OB_VTBL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_CTR
CDEC$ ENDIF

      USE OB_VTBL_M
      !!!USE OB_HNDL_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(OUT):: HOBJ
      !!!TYPE(OB_HNDL_T), INTENT(OUT):: HOBJ

      INCLUDE 'obvtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sovtbl.fi'

      INTEGER IERR, IRET
      TYPE (OB_VTBL_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   OB_VTBL_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
!!      IF (ERR_GOOD()) IERR = OB_VTBL_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. OB_VTBL_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      OB_VTBL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_DTR
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'obvtbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (OB_VTBL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(OB_VTBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = OB_VTBL_RST(HOBJ)

C---     Désalloue la mémoire
      SELF => OB_VTBL_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dés-enregistre
      IERR = OB_OBJN_DTR(HOBJ, OB_VTBL_HBASE)

      OB_VTBL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode virtuelle OB_VTBL_INI(...)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_INI
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'obvtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'sovtbl.fi'

      INTEGER IERR
      INTEGER HNDL
      TYPE (OB_VTBL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(OB_VTBL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     SELF doit exister
      SELF => OB_VTBL_REQSELF(HOBJ)

C---     Construit la table
      IF (ERR_GOOD()) IERR = SO_VTBL_CTR(HNDL)
      IF (ERR_GOOD()) IERR = SO_VTBL_INI(HNDL)
      IF (ERR_GOOD()) SELF%HVTBL = HNDL

C---     Au besoin, construit la table
      IF (ERR_GOOD()) IERR = DS_MAP_CTR (HNDL)
      IF (ERR_GOOD()) IERR = DS_MAP_INI (HNDL)
      IF (ERR_GOOD()) SELF%HCLSS = HNDL

      OB_VTBL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode virtuelle OB_VTBL_RST(...)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_RST
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'obvtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'sovtbl.fi'

      INTEGER IERR
      TYPE (OB_VTBL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(OB_VTBL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Les attributs
      SELF => OB_VTBL_REQSELF(HOBJ)

C---     Désalloue la table virtuelle
      IF (SO_VTBL_HVALIDE(SELF%HVTBL)) THEN
         IF (ERR_GOOD()) IERR = SO_VTBL_DTR(SELF%HVTBL)
      ENDIF
      SELF%HVTBL = 0

C---     Désalloue le map
      IF (DS_MAP_HVALIDE(SELF%HCLSS)) THEN
         IF (ERR_GOOD()) IERR = DS_MAP_DTR(SELF%HCLSS)
      ENDIF
      SELF%HCLSS = 0

      OB_VTBL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction OB_VTBL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_HVALIDE
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'obvtbl.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      OB_VTBL_HVALIDE = OB_OBJN_HVALIDE(HOBJ, OB_VTBL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode OB_VTBL_AJTxxx(...) ajoute un couple clef/valeur
C     à la table virtuelle.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_AJTxxx(HOBJ, IKEY, IVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_AJTxxx
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IKEY
      INTEGER IVAL

      INCLUDE 'obvtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'

      INTEGER IERR
      TYPE (OB_VTBL_T), POINTER :: SELF
      CHARACTER*(32) LKEY, LVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(OB_VTBL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => OB_VTBL_REQSELF(HOBJ)
      WRITE(LKEY, *) IKEY
      WRITE(LVAL, *) IVAL
      IERR = DS_MAP_ASGVAL(SELF%HCLSS, LKEY, LVAL)

      OB_VTBL_AJTxxx = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode OB_VTBL_REQxxx(...) retourne la valeur correspondant à
C     la clef.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_REQxxx(HOBJ, IKEY, IVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_REQxxx
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IKEY
      INTEGER IVAL

      INCLUDE 'obvtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'

      INTEGER IERR
      TYPE (OB_VTBL_T), POINTER :: SELF
      CHARACTER*(32) LKEY, LVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(OB_VTBL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => OB_VTBL_REQSELF(HOBJ)
      WRITE(LKEY, *) IKEY
      IERR = DS_MAP_REQVAL(SELF%HCLSS, LKEY, LVAL)
      IF (ERR_GOOD()) READ(LVAL, *) IVAL

      OB_VTBL_REQxxx = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode OB_VTBL_AJTMSO(...) ajoute une méthode à la table
C     virtuelle de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_AJTMSO(HOBJ, IFMTH, HSLF, FDLL, FFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_AJTMSO
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       IFMTH
      INTEGER       HSLF
      CHARACTER*(*) FDLL
      CHARACTER*(*) FFNC

      INCLUDE 'obvtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'

      INTEGER IERR
      TYPE (OB_VTBL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(OB_VTBL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => OB_VTBL_REQSELF(HOBJ)
      IERR = SO_VTBL_AJTMSO(SELF%HVTBL, IFMTH, HSLF, FDLL, FFNC)

      OB_VTBL_AJTMSO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode OB_VTBL_AJTMTH(...) ajoute une méthode à la table
C     virtuelle de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_AJTMTH(HOBJ, IFMTH, HSLF, F)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_AJTMTH
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IFMTH
      INTEGER HSLF
      INTEGER F
      EXTERNAL F

      INCLUDE 'obvtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'

      INTEGER IERR
      TYPE (OB_VTBL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(OB_VTBL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => OB_VTBL_REQSELF(HOBJ)
      IERR = SO_VTBL_AJTMTH(SELF%HVTBL, IFMTH, HSLF, F)

      OB_VTBL_AJTMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode OB_VTBL_REQMTH(...) retourne la méthode associée à
C     IDFNC.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_VTBL_REQMTH(HOBJ, IDFNC, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_VTBL_REQMTH
CDEC$ ENDIF

      USE OB_VTBL_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IDFNC
      INTEGER HFNC

      INCLUDE 'obvtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'

      INTEGER IERR
      TYPE (OB_VTBL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(OB_VTBL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => OB_VTBL_REQSELF(HOBJ)
      IERR = SO_VTBL_REQMTH(SELF%HVTBL, IDFNC, HFNC)

      OB_VTBL_REQMTH = ERR_TYP()
      RETURN
      END
