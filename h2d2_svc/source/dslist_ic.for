C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DS_LIST_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DS_LIST_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dslist_ic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER HOBJ
      INTEGER IT, NTOK
      CHARACTER*(256) TOK
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DS_LIST_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Construis l'objet
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DS_LIST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = DS_LIST_INI(HOBJ)

C---     Ajoute les items, un à un
      NTOK = SP_STRN_NTOK(IPRM, ',')
      DO IT=1,NTOK
C        <comment>List of values, comma separated</comment>
         IF (ERR_GOOD()) IRET = SP_STRN_TKS(IPRM, ',', IT, TOK)
         IF (IRET .NE. 0) GOTO 9900
         IF (ERR_GOOD()) IERR = DS_LIST_AJTVAL(HOBJ, TOK)
      ENDDO

C---     Nettoie en cas d'erreur
      IF (ERR_BAD()) THEN
         CALL ERR_PUSH()
         IF (DS_LIST_HVALIDE(HOBJ)) IERR = DS_LIST_DTR(HOBJ)
         CALL ERR_POP()
      ENDIF

C---     Retourne le handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the list</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>list</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DS_LIST_AID()

9999  CONTINUE
      IC_DS_LIST_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DS_LIST_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DS_LIST_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dslist_ic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER IT, NTOK
      INTEGER IVAL, HVAL
      CHARACTER*(256) SVAL
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Dispatch les méthodes, propriétés et opérateurs
C     <comment>Get a value with its index.</comment>
      IF (IMTH .EQ. '##opb_[]_get##') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Index</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = DS_LIST_REQVAL(HOBJ, IVAL, SVAL)
         IF (ERR_GOOD()) WRITE(IPRM, '(3A)') 'S', ',', SVAL

C     <comment>Set a value with its index. If the value does not exist, it is created. If it does, it is replaced.</comment>
      ELSEIF (IMTH .EQ. '##opb_[]_set##') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Index</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
C        <comment>Value</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 2, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = DS_LIST_ASGVAL(HOBJ, IVAL, SVAL(1:SP_STRN_LEN(SVAL)))

C     <comment>The method <b>add_as_one_val</b> appends all the arguments as one single value, at the end of the list.</comment>
      ELSEIF (IMTH .EQ. 'add_as_one_val') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (SP_STRN_LEN(IPRM) .GT. LEN(SVAL)) GOTO 9901

C        <comment>Value</comment>
         IF (IERR .EQ. 0) SVAL = IPRM(1:SP_STRN_LEN(IPRM))
         IERR = DS_LIST_AJTVAL(HOBJ, SVAL(1:SP_STRN_LEN(SVAL)))

C     <comment>The method <b>append</b> appends the values specified at the end of the list.</comment>
      ELSEIF (IMTH .EQ. 'append') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (SP_STRN_LEN(IPRM) .GT. LEN(SVAL)) GOTO 9901

         NTOK = SP_STRN_NTOK(IPRM, ',')
         DO IT=1,NTOK
C           <comment>List of values, comma separated</comment>
            IF (ERR_GOOD()) IERR = SP_STRN_TKS(IPRM, ',', IT, SVAL)
            IF (IERR .NE. 0) GOTO 9900
            IF (ERR_GOOD()) IERR = DS_LIST_AJTVAL(HOBJ, SVAL)
         ENDDO

C     <comment>The method <b>extend</b> extends the list by appending all the items in the given list.</comment>
      ELSEIF (IMTH .EQ. 'extend') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (SP_STRN_NTOK(IPRM, ',') .NE. 1) GOTO 9901

C        <comment>Handle on the given list</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HVAL)
         IF (IERR .NE. 0) GOTO 9901
         IF (.NOT. DS_LIST_HVALIDE(HOBJ)) GOTO 9901

         DO IVAL=1,DS_LIST_REQDIM(HVAL)
            IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HVAL, IVAL, SVAL)
            IF (ERR_GOOD()) IERR = DS_LIST_AJTVAL(HOBJ, SVAL)
         ENDDO

C     <comment>The method <b>clear</b> will clear (empty) the list.</comment>
      ELSEIF (IMTH .EQ. 'clear') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9901
         IERR = DS_LIST_CLR(HOBJ)

C     <comment>The method <b>remove</b> removes the value specified by its index.</comment>
      ELSEIF (IMTH .EQ. 'remove') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Index</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
         IERR = DS_LIST_EFFVAL(HOBJ, IVAL)

C     <comment>The method <b>size</b> returns the size of the list.</comment>
      ELSEIF (IMTH .EQ. 'size') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IVAL = DS_LIST_REQDIM(HOBJ)
C        <comment>The size of the list.</comment>
         WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DS_LIST_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DS_LIST_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DS_LIST_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DS_LIST_AID()

9999  CONTINUE
      IC_DS_LIST_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DS_LIST_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DS_LIST_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dslist_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C   The class <b>list</b> represents a list data structure containing strings that
C   can be iterated and addressed by their index.
C</comment>
      IC_DS_LIST_REQCLS = 'list'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DS_LIST_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DS_LIST_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dslist_ic.fi'
      INCLUDE 'dslist.fi'
C-------------------------------------------------------------------------

      IC_DS_LIST_REQHDL = DS_LIST_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DS_LIST_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dslist_ic.hlp')

      RETURN
      END
