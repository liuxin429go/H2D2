C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_OS_REQHDL
C     INTEGER IC_OS_OPBDOT
C     INTEGER IC_OS_REQMDL
C     CHARACTER*(32) IC_OS_REQNOM
C   Private:
C     INTEGER IC_OS_XEQOS
C     SUBROUTINE IC_OS_AID
C     INTEGER IC_OS_XEQPTH
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_OS_REQMDL(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_OS_REQMDL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'os_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_OS_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Contrôle les param
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
         HOBJ = IC_OS_REQHDL()
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  <b>os</b> returns the handle on the module.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_OS_AID()

9999  CONTINUE
      IC_OS_REQMDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_OS_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_OS_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'os_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'os_ic.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_OS_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

      IOB = HOBJ - IC_OS_REQHDL()
      IF     (IOB .EQ. IC_OS_MDL_OS) THEN
C        <include>IC_OS_XEQOS@os_ic.for</include>
         IERR = IC_OS_XEQOS(HOBJ, IMTH, IPRM)
      ELSEIF (IOB .EQ. IC_OS_MDL_PTH) THEN
         IERR = IC_OS_XEQPTH(HOBJ, IMTH, IPRM)
      ELSE
         GOTO 9900
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HOBJ
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_OS_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_OS_XEQOS(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'os_ic.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'os_ic.fc'

      REAL*8        RVAL
      INTEGER       IERR, IRET
      INTEGER       IVAL
      CHARACTER*64  PROP
      CHARACTER*256 SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR(HOBJ .EQ. IC_OS_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_OS_AID()

C---     GET
      ELSEIF (IMTH .EQ. '##property_get##') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>The path module regroups functionalities related to path and file names.</comment>
         IF (PROP .EQ. 'path') THEN
            WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ+IC_OS_MDL_PTH
C           <include>IC_OS_XEQPTH@os_ic.for</include>

C        <comment>Hostame</comment>
         ELSEIF (PROP .EQ. 'hostname') THEN
            IRET = C_OS_HOSTNAME(SVAL)
            IF (IRET .NE. 0) GOTO 9904
            WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))
C        <comment>Platform name</comment>
         ELSEIF (PROP .EQ. 'platform') THEN
            IRET = C_OS_PLATFORM(SVAL)
            IF (IRET .NE. 0) GOTO 9904
            WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))
         ELSE
            GOTO 9902
         ENDIF

C---     Méthodes
C     <comment>getcwd</comment>
      ELSEIF (IMTH .EQ. 'getcwd') THEN
         IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

         IRET = C_OS_CWD(SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))

C     <comment>getenv</comment>
      ELSEIF (IMTH .EQ. 'getenv') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IRET .NE. 0) GOTO 9901

         IRET = C_OS_REQVARENV(PROP(1:SP_STRN_LEN(PROP)), SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(3A)') 'S', ',', SVAL

C     <comment>putenv</comment>
      ELSEIF (IMTH .EQ. 'putenv') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 2, SVAL)
         IF (IRET .NE. 0) GOTO 9901

         IRET = C_OS_ASGVARENV(PROP, SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(A)') ' '

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_APPEL_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_OS_AID()

9999  CONTINUE
      IC_OS_XEQOS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_OS_XEQPTH(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'os_ic.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'os_ic.fc'

      INTEGER IERR, IRET
      INTEGER IT, IPOS, NTOK
      CHARACTER*64  SVAL
      CHARACTER*256 BUF
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_OS_REQHDL())
D     CALL ERR_ASR(HOBJ-IC_OS_REQHDL() .EQ. IC_OS_MDL_PTH)
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C     <comment>
C     The function <b>abspath</b> returns the absolute version of a
C     path.
C     </comment>
      IF (IMTH .EQ. 'abspath') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Path</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IRET .NE. 0) GOTO 9901

         IRET = C_OS_NORMPATH(SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,A)') 'R', ',', SVAL

C     <comment>
C     The function <b>basename</b> returns the final component of a pathname
C     </comment>
      ELSEIF (IMTH .EQ. 'basename') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Path</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IRET .NE. 0) GOTO 9901

         IRET = C_OS_NORMPATH(SVAL)
         IF (IRET .EQ. 0) NTOK = SP_STRN_NTOK(IPRM, '/')
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS (IPRM, '/', NTOK, SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,A)') 'S', ',', SVAL

C     <comment>
C     The function <b>dirname</b> returns the directory component of a pathname
C     </comment>
      ELSEIF (IMTH .EQ. 'dirname') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Path</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IRET .NE. 0) GOTO 9901

         IRET = C_OS_NORMPATH(SVAL)
         IF (IRET .EQ. 0) NTOK = SP_STRN_NTOK(IPRM, '/')
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS (IPRM, '/', NTOK, SVAL)
         IF (IRET .NE. 0) GOTO 9904
         IPOS = SP_STRN_LFT(IPRM, '/' // SVAL)
         WRITE(IPRM, '(2A,A)') 'S', ',', IPRM

C     <comment>
C     The function <b>isdir</b> returns 1 if the pathname refers to an existing directory,
C     0 otherwise.
C     </comment>
      ELSEIF (IMTH .EQ. 'isdir') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Path</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IRET .NE. 0) GOTO 9901

         IRET = C_OS_ISDIR(SVAL)
         WRITE(IPRM, '(2A,L3)') 'L', ',', (IRET .NE. 0)

C     <comment>
C     The function <b>isfile</b> returns 1 if the pathname refers to an existing file,
C     0 otherwise.
C     </comment>
      ELSEIF (IMTH .EQ. 'isfile') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Path</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IRET .NE. 0) GOTO 9901

         IRET = C_OS_ISFILE(SVAL)
         WRITE(IPRM, '(2A,L3)') 'L', ',', (IRET .NE. 0)

C     <comment>
C     The function <b>join</b>
C     </comment>
      ELSEIF (IMTH .EQ. 'join') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IRET .EQ. 0) NTOK = SP_STRN_NTOK(IPRM, '/')
         IF (IRET .NE. 0) GOTO 9901
         IF (NTOK .LE. 0) GOTO 9901
C        <comment>Path</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, BUF)
         IF (IRET .EQ. 0) IRET = C_OS_NORMPATH(BUF)
         DO IT=2,NTOK
            IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', IT, SVAL)
            IF (IRET .EQ. 0) IRET = C_OS_NORMPATH(SVAL)
            IF (IRET .EQ. 0) BUF = BUF(1:SP_STRN_LEN(BUF)) // SVAL
         ENDDO
         WRITE(IPRM, '(2A,A)') 'S', ',', SVAL

C     <comment>
C     The function <b>normpath</b> normalizes the path, eliminating double slashes, etc.
C     </comment>
      ELSEIF (IMTH .EQ. 'join') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Path</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IRET .EQ. 0) IRET = C_OS_NORMPATH(SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,A)') 'S', ',', SVAL

C     <comment>
C     The function <b>extension</b> returns the extension of the path.
C     Extension is everything from the last dot to the end, ignoring
C     leading dots. Extension may be empty.
C     </comment>
      ELSEIF (IMTH .EQ. 'extension') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Path</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IRET .NE. 0) GOTO 9904

         IRET = C_OS_NORMPATH(SVAL)
         IF (IRET .EQ. 0) NTOK = SP_STRN_NTOK(IPRM, '.')
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS (IPRM, '.', NTOK, SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,A)') 'S', ',', SVAL

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_OS_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_DATE_INVALIDE', ': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_OS_AID()

9999  CONTINUE
      IC_OS_XEQPTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_OS_REQNOM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_OS_REQNOM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'os_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The module <b>os</b> regroups operating system functionalities, not
C  directly related to the finite element method.
C</comment>
      IC_OS_REQNOM = 'os'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le handle doit être unique au niveau global
C************************************************************************
      FUNCTION IC_OS_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_OS_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'os_ic.fi'
C-------------------------------------------------------------------------

      IC_OS_REQHDL = 1999004000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_OS_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('os_ic.hlp')

      RETURN
      END
