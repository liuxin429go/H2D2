//************************************************************************
// H2D2 - External declaration of public symbols
// Module: h2d2_svc
// Entry point: extern "C" void fake_dll_h2d2_svc()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:47:07.315453
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class DBG_DMP
F_PROT(DBG_DMP_DTBL, dbg_dmp_dtbl);
F_PROT(DBG_DMP_DTBL2, dbg_dmp_dtbl2);
F_PROT(DBG_DMP_ITBL, dbg_dmp_itbl);
F_PROT(DBG_DMP_ITBL2, dbg_dmp_itbl2);
 
// ---  class DS_LIST
F_PROT(DS_LIST_000, ds_list_000);
F_PROT(DS_LIST_999, ds_list_999);
F_PROT(DS_LIST_PKL, ds_list_pkl);
F_PROT(DS_LIST_UPK, ds_list_upk);
F_PROT(DS_LIST_CTR, ds_list_ctr);
F_PROT(DS_LIST_DTR, ds_list_dtr);
F_PROT(DS_LIST_INI, ds_list_ini);
F_PROT(DS_LIST_RST, ds_list_rst);
F_PROT(DS_LIST_REQHBASE, ds_list_reqhbase);
F_PROT(DS_LIST_HVALIDE, ds_list_hvalide);
F_PROT(DS_LIST_PRN, ds_list_prn);
F_PROT(DS_LIST_AJTVAL, ds_list_ajtval);
F_PROT(DS_LIST_ASGVAL, ds_list_asgval);
F_PROT(DS_LIST_CLR, ds_list_clr);
F_PROT(DS_LIST_EFFVAL, ds_list_effval);
F_PROT(DS_LIST_REQDIM, ds_list_reqdim);
F_PROT(DS_LIST_REQVAL, ds_list_reqval);
F_PROT(DS_LIST_TRVVAL, ds_list_trvval);
F_PROT(DS_LIST_POP, ds_list_pop);
F_PROT(DS_LIST_PUSH, ds_list_push);
F_PROT(DS_LIST_PUSHI, ds_list_pushi);
F_PROT(DS_LIST_TOP, ds_list_top);
F_PROT(DS_LIST_TOPI, ds_list_topi);
 
// ---  class DS_LSTI
F_PROT(DS_LSTI_000, ds_lsti_000);
F_PROT(DS_LSTI_999, ds_lsti_999);
F_PROT(DS_LSTI_CTR, ds_lsti_ctr);
F_PROT(DS_LSTI_DTR, ds_lsti_dtr);
F_PROT(DS_LSTI_INI, ds_lsti_ini);
F_PROT(DS_LSTI_RST, ds_lsti_rst);
F_PROT(DS_LSTI_REQHBASE, ds_lsti_reqhbase);
F_PROT(DS_LSTI_HVALIDE, ds_lsti_hvalide);
F_PROT(DS_LSTI_REQNXT, ds_lsti_reqnxt);
 
// ---  class DS_MAP
F_PROT(DS_MAP_000, ds_map_000);
F_PROT(DS_MAP_999, ds_map_999);
F_PROT(DS_MAP_PKL, ds_map_pkl);
F_PROT(DS_MAP_UPK, ds_map_upk);
F_PROT(DS_MAP_CTR, ds_map_ctr);
F_PROT(DS_MAP_DTR, ds_map_dtr);
F_PROT(DS_MAP_INI, ds_map_ini);
F_PROT(DS_MAP_RST, ds_map_rst);
F_PROT(DS_MAP_REQHBASE, ds_map_reqhbase);
F_PROT(DS_MAP_HVALIDE, ds_map_hvalide);
F_PROT(DS_MAP_PRN, ds_map_prn);
F_PROT(DS_MAP_ASGVAL, ds_map_asgval);
F_PROT(DS_MAP_EFFCLF, ds_map_effclf);
F_PROT(DS_MAP_ESTCLF, ds_map_estclf);
F_PROT(DS_MAP_REQDIM, ds_map_reqdim);
F_PROT(DS_MAP_REQCLF, ds_map_reqclf);
F_PROT(DS_MAP_REQVAL, ds_map_reqval);
 
// ---  class ERR_OMP
F_PROT(ERR_OMP_INI, err_omp_ini);
F_PROT(ERR_OMP_END, err_omp_end);
F_PROT(ERR_OMP_DST, err_omp_dst);
F_PROT(ERR_OMP_RDC, err_omp_rdc);
 
// ---  class IC_EXPR
F_PROT(IC_EXPR_000, ic_expr_000);
F_PROT(IC_EXPR_999, ic_expr_999);
F_PROT(IC_EXPR_PKL, ic_expr_pkl);
F_PROT(IC_EXPR_UPK, ic_expr_upk);
F_PROT(IC_EXPR_CTR, ic_expr_ctr);
F_PROT(IC_EXPR_DTR, ic_expr_dtr);
F_PROT(IC_EXPR_INI, ic_expr_ini);
F_PROT(IC_EXPR_RST, ic_expr_rst);
F_PROT(IC_EXPR_REQHBASE, ic_expr_reqhbase);
F_PROT(IC_EXPR_HVALIDE, ic_expr_hvalide);
F_PROT(IC_EXPR_XEQ, ic_expr_xeq);
F_PROT(IC_EXPR_XEQ_L, ic_expr_xeq_l);
F_PROT(IC_EXPR_ASGCTX, ic_expr_asgctx);
 
// ---  class IC_FINP
F_PROT(IC_FINP_000, ic_finp_000);
F_PROT(IC_FINP_999, ic_finp_999);
F_PROT(IC_FINP_PKL, ic_finp_pkl);
F_PROT(IC_FINP_UPK, ic_finp_upk);
F_PROT(IC_FINP_CTR, ic_finp_ctr);
F_PROT(IC_FINP_DTR, ic_finp_dtr);
F_PROT(IC_FINP_INI, ic_finp_ini);
F_PROT(IC_FINP_RST, ic_finp_rst);
F_PROT(IC_FINP_REQHBASE, ic_finp_reqhbase);
F_PROT(IC_FINP_HVALIDE, ic_finp_hvalide);
F_PROT(IC_FINP_LISCMD, ic_finp_liscmd);
F_PROT(IC_FINP_REQCRC, ic_finp_reqcrc);
F_PROT(IC_FINP_REQLCMD, ic_finp_reqlcmd);
F_PROT(IC_FINP_REQLCUR, ic_finp_reqlcur);
F_PROT(IC_FINP_REQLSRC, ic_finp_reqlsrc);
F_PROT(IC_FINP_REQNOM, ic_finp_reqnom);
 
// ---  class IC_FUNC
F_PROT(IC_FUNC_000, ic_func_000);
F_PROT(IC_FUNC_999, ic_func_999);
F_PROT(IC_FUNC_PKL, ic_func_pkl);
F_PROT(IC_FUNC_UPK, ic_func_upk);
F_PROT(IC_FUNC_CTR, ic_func_ctr);
F_PROT(IC_FUNC_DTR, ic_func_dtr);
F_PROT(IC_FUNC_INI, ic_func_ini);
F_PROT(IC_FUNC_RST, ic_func_rst);
F_PROT(IC_FUNC_REQHBASE, ic_func_reqhbase);
F_PROT(IC_FUNC_HVALIDE, ic_func_hvalide);
F_PROT(IC_FUNC_INIWRITE, ic_func_iniwrite);
F_PROT(IC_FUNC_ENDWRITE, ic_func_endwrite);
F_PROT(IC_FUNC_ADDSTAT, ic_func_addstat);
F_PROT(IC_FUNC_CHKARGS, ic_func_chkargs);
F_PROT(IC_FUNC_POP, ic_func_pop);
F_PROT(IC_FUNC_PUSH, ic_func_push);
F_PROT(IC_FUNC_REQNFNC, ic_func_reqnfnc);
F_PROT(IC_FUNC_REQFSRC, ic_func_reqfsrc);
F_PROT(IC_FUNC_REQFFNC, ic_func_reqffnc);
 
// ---  class IC_ICMD
F_PROT(IC_ICMD_000, ic_icmd_000);
F_PROT(IC_ICMD_999, ic_icmd_999);
F_PROT(IC_ICMD_PKL, ic_icmd_pkl);
F_PROT(IC_ICMD_UPK, ic_icmd_upk);
F_PROT(IC_ICMD_CTR, ic_icmd_ctr);
F_PROT(IC_ICMD_DTR, ic_icmd_dtr);
F_PROT(IC_ICMD_INI, ic_icmd_ini);
F_PROT(IC_ICMD_RST, ic_icmd_rst);
F_PROT(IC_ICMD_REQHBASE, ic_icmd_reqhbase);
F_PROT(IC_ICMD_HVALIDE, ic_icmd_hvalide);
F_PROT(IC_ICMD_EVAL, ic_icmd_eval);
F_PROT(IC_ICMD_XEQ, ic_icmd_xeq);
F_PROT(IC_ICMD_ADDFNC, ic_icmd_addfnc);
F_PROT(IC_ICMD_ADDCLS, ic_icmd_addcls);
F_PROT(IC_ICMD_ADDMDL, ic_icmd_addmdl);
F_PROT(IC_ICMD_REQSMB, ic_icmd_reqsmb);
 
// ---  class IC_LOG
F_PROT(IC_LOG_XEQCTR, ic_log_xeqctr);
F_PROT(IC_LOG_XEQMTH, ic_log_xeqmth);
F_PROT(IC_LOG_OPBDOT, ic_log_opbdot);
F_PROT(IC_LOG_REQCLS, ic_log_reqcls);
F_PROT(IC_LOG_REQHDL, ic_log_reqhdl);
 
// ---  class IC_OS
F_PROT(IC_OS_REQMDL, ic_os_reqmdl);
F_PROT(IC_OS_OPBDOT, ic_os_opbdot);
F_PROT(IC_OS_REQNOM, ic_os_reqnom);
F_PROT(IC_OS_REQHDL, ic_os_reqhdl);
 
// ---  class IC_PARS
F_PROT(IC_PARS_CMDPIL, ic_pars_cmdpil);
F_PROT(IC_PARS_ASGKW, ic_pars_asgkw);
 
// ---  class IC_PFIC
F_PROT(IC_PFIC_000, ic_pfic_000);
F_PROT(IC_PFIC_999, ic_pfic_999);
F_PROT(IC_PFIC_PKL, ic_pfic_pkl);
F_PROT(IC_PFIC_UPK, ic_pfic_upk);
F_PROT(IC_PFIC_CTR, ic_pfic_ctr);
F_PROT(IC_PFIC_DTR, ic_pfic_dtr);
F_PROT(IC_PFIC_INI, ic_pfic_ini);
F_PROT(IC_PFIC_RST, ic_pfic_rst);
F_PROT(IC_PFIC_REQHBASE, ic_pfic_reqhbase);
F_PROT(IC_PFIC_HVALIDE, ic_pfic_hvalide);
F_PROT(IC_PFIC_END, ic_pfic_end);
F_PROT(IC_PFIC_POP, ic_pfic_pop);
F_PROT(IC_PFIC_PUSH, ic_pfic_push);
F_PROT(IC_PFIC_TOP, ic_pfic_top);
F_PROT(IC_PFIC_SIZE, ic_pfic_size);
 
// ---  class IC_PILE
F_PROT(IC_PILE_000, ic_pile_000);
F_PROT(IC_PILE_999, ic_pile_999);
F_PROT(IC_PILE_PKL, ic_pile_pkl);
F_PROT(IC_PILE_UPK, ic_pile_upk);
F_PROT(IC_PILE_CTR, ic_pile_ctr);
F_PROT(IC_PILE_DTR, ic_pile_dtr);
F_PROT(IC_PILE_INI, ic_pile_ini);
F_PROT(IC_PILE_RST, ic_pile_rst);
F_PROT(IC_PILE_REQHBASE, ic_pile_reqhbase);
F_PROT(IC_PILE_HVALIDE, ic_pile_hvalide);
F_PROT(IC_PILE_CMPCT, ic_pile_cmpct);
F_PROT(IC_PILE_DUMP, ic_pile_dump);
F_PROT(IC_PILE_ESTKW, ic_pile_estkw);
F_PROT(IC_PILE_ESTLBL, ic_pile_estlbl);
F_PROT(IC_PILE_ESTNOOP, ic_pile_estnoop);
F_PROT(IC_PILE_ESTOP, ic_pile_estop);
F_PROT(IC_PILE_ESTOPB, ic_pile_estopb);
F_PROT(IC_PILE_ESTOPU, ic_pile_estopu);
F_PROT(IC_PILE_ESTVAL, ic_pile_estval);
F_PROT(IC_PILE_ESTVALCST, ic_pile_estvalcst);
F_PROT(IC_PILE_ESTVALHDL, ic_pile_estvalhdl);
F_PROT(IC_PILE_ESTVALMDL, ic_pile_estvalmdl);
F_PROT(IC_PILE_ESTVALVAL, ic_pile_estvalval);
F_PROT(IC_PILE_ESTVAR, ic_pile_estvar);
F_PROT(IC_PILE_ESTVIDE, ic_pile_estvide);
F_PROT(IC_PILE_ESTVIDEFR, ic_pile_estvidefr);
F_PROT(IC_PILE_FRBSE, ic_pile_frbse);
F_PROT(IC_PILE_FRCLR, ic_pile_frclr);
F_PROT(IC_PILE_FRPOP, ic_pile_frpop);
F_PROT(IC_PILE_FRPUSH, ic_pile_frpush);
F_PROT(IC_PILE_FRTOP, ic_pile_frtop);
F_PROT(IC_PILE_FRSIZ, ic_pile_frsiz);
F_PROT(IC_PILE_FROPDEB, ic_pile_fropdeb);
F_PROT(IC_PILE_FROPFIN, ic_pile_fropfin);
F_PROT(IC_PILE_CLR, ic_pile_clr);
F_PROT(IC_PILE_POP, ic_pile_pop);
F_PROT(IC_PILE_PUSH, ic_pile_push);
F_PROT(IC_PILE_PUSHE, ic_pile_pushe);
F_PROT(IC_PILE_SIZ, ic_pile_siz);
F_PROT(IC_PILE_TOP, ic_pile_top);
F_PROT(IC_PILE_ESTVALOK, ic_pile_estvalok);
F_PROT(IC_PILE_TYP2TXT, ic_pile_typ2txt);
F_PROT(IC_PILE_VALDEC, ic_pile_valdec);
F_PROT(IC_PILE_VALENC, ic_pile_valenc);
F_PROT(IC_PILE_VALTYP, ic_pile_valtyp);
F_PROT(IC_PILE_VALDEB, ic_pile_valdeb);
F_PROT(IC_PILE_VALFIN, ic_pile_valfin);
F_PROT(IC_PILE_VALTAG, ic_pile_valtag);
 
// ---  class IC_SYS
F_PROT(IC_SYS_REQMDL, ic_sys_reqmdl);
F_PROT(IC_SYS_OPBDOT, ic_sys_opbdot);
F_PROT(IC_SYS_REQNOM, ic_sys_reqnom);
F_PROT(IC_SYS_REQHDL, ic_sys_reqhdl);
 
// ---  class IC_TRA
F_PROT(IC_TRA_XEQCTR, ic_tra_xeqctr);
F_PROT(IC_TRA_XEQMTH, ic_tra_xeqmth);
F_PROT(IC_TRA_OPBDOT, ic_tra_opbdot);
F_PROT(IC_TRA_REQCLS, ic_tra_reqcls);
F_PROT(IC_TRA_REQHDL, ic_tra_reqhdl);
 
// ---  class IC_DS_LIST
F_PROT(IC_DS_LIST_XEQCTR, ic_ds_list_xeqctr);
F_PROT(IC_DS_LIST_XEQMTH, ic_ds_list_xeqmth);
F_PROT(IC_DS_LIST_REQCLS, ic_ds_list_reqcls);
F_PROT(IC_DS_LIST_REQHDL, ic_ds_list_reqhdl);
 
// ---  class IC_DS_LSTI
F_PROT(IC_DS_LSTI_XEQCTR, ic_ds_lsti_xeqctr);
F_PROT(IC_DS_LSTI_XEQMTH, ic_ds_lsti_xeqmth);
F_PROT(IC_DS_LSTI_REQCLS, ic_ds_lsti_reqcls);
F_PROT(IC_DS_LSTI_REQHDL, ic_ds_lsti_reqhdl);
 
// ---  class IC_DS_MAP
F_PROT(IC_DS_MAP_XEQCTR, ic_ds_map_xeqctr);
F_PROT(IC_DS_MAP_XEQMTH, ic_ds_map_xeqmth);
F_PROT(IC_DS_MAP_REQCLS, ic_ds_map_reqcls);
F_PROT(IC_DS_MAP_REQHDL, ic_ds_map_reqhdl);
 
// ---  class IC_DS_RNGE
F_PROT(IC_DS_RNGE_XEQCTR, ic_ds_rnge_xeqctr);
F_PROT(IC_DS_RNGE_XEQMTH, ic_ds_rnge_xeqmth);
F_PROT(IC_DS_RNGE_REQCLS, ic_ds_rnge_reqcls);
F_PROT(IC_DS_RNGE_REQHDL, ic_ds_rnge_reqhdl);
 
// ---  class IC_IC_EXPR
F_PROT(IC_IC_EXPR_XEQCTR, ic_ic_expr_xeqctr);
F_PROT(IC_IC_EXPR_XEQMTH, ic_ic_expr_xeqmth);
F_PROT(IC_IC_EXPR_REQCLS, ic_ic_expr_reqcls);
F_PROT(IC_IC_EXPR_REQHDL, ic_ic_expr_reqhdl);
 
// ---  class IC_TR_CDWN
F_PROT(IC_TR_CDWN_XEQCTR, ic_tr_cdwn_xeqctr);
F_PROT(IC_TR_CDWN_XEQMTH, ic_tr_cdwn_xeqmth);
F_PROT(IC_TR_CDWN_REQCLS, ic_tr_cdwn_reqcls);
F_PROT(IC_TR_CDWN_REQHDL, ic_tr_cdwn_reqhdl);
 
// ---  class IC_TR_CHRN
F_PROT(IC_TR_CHRN_XEQCTR, ic_tr_chrn_xeqctr);
F_PROT(IC_TR_CHRN_XEQMTH, ic_tr_chrn_xeqmth);
F_PROT(IC_TR_CHRN_OPBDOT, ic_tr_chrn_opbdot);
F_PROT(IC_TR_CHRN_REQCLS, ic_tr_chrn_reqcls);
F_PROT(IC_TR_CHRN_REQHDL, ic_tr_chrn_reqhdl);
 
// ---  class IO_I08
F_PROT(IO_I08_000, io_i08_000);
F_PROT(IO_I08_999, io_i08_999);
F_PROT(IO_I08_CTR, io_i08_ctr);
F_PROT(IO_I08_DTR, io_i08_dtr);
F_PROT(IO_I08_INI, io_i08_ini);
F_PROT(IO_I08_RST, io_i08_rst);
F_PROT(IO_I08_REQHBASE, io_i08_reqhbase);
F_PROT(IO_I08_HVALIDE, io_i08_hvalide);
F_PROT(IO_I08_OPEN, io_i08_open);
F_PROT(IO_I08_CLOSE, io_i08_close);
F_PROT(IO_I08_ENDL, io_i08_endl);
F_PROT(IO_I08_SKIP, io_i08_skip);
F_PROT(IO_I08_GETPOS, io_i08_getpos);
F_PROT(IO_I08_SETPOS, io_i08_setpos);
F_PROT(IO_I08_WHDR, io_i08_whdr);
F_PROT(IO_I08_WS, io_i08_ws);
F_PROT(IO_I08_WI_1, io_i08_wi_1);
F_PROT(IO_I08_WI_N, io_i08_wi_n);
F_PROT(IO_I08_WD_1, io_i08_wd_1);
F_PROT(IO_I08_WD_N, io_i08_wd_n);
F_PROT(IO_I08_RHDR, io_i08_rhdr);
F_PROT(IO_I08_RS, io_i08_rs);
F_PROT(IO_I08_RI_1, io_i08_ri_1);
F_PROT(IO_I08_RI_N, io_i08_ri_n);
F_PROT(IO_I08_RD_1, io_i08_rd_1);
F_PROT(IO_I08_RD_N, io_i08_rd_n);
 
// ---  class IO_UTIL
F_PROT(IO_UTIL_FREEUNIT, io_util_freeunit);
F_PROT(IO_UTIL_FICDUM, io_util_ficdum);
F_PROT(IO_UTIL_MODVALIDE, io_util_modvalide);
F_PROT(IO_UTIL_MODACTIF, io_util_modactif);
F_PROT(IO_UTIL_STR2MOD, io_util_str2mod);
 
// ---  class IO_XML
F_PROT(IO_XML_000, io_xml_000);
F_PROT(IO_XML_999, io_xml_999);
F_PROT(IO_XML_CTR, io_xml_ctr);
F_PROT(IO_XML_DTR, io_xml_dtr);
F_PROT(IO_XML_INI, io_xml_ini);
F_PROT(IO_XML_RST, io_xml_rst);
F_PROT(IO_XML_REQHBASE, io_xml_reqhbase);
F_PROT(IO_XML_HVALIDE, io_xml_hvalide);
F_PROT(IO_XML_OPEN, io_xml_open);
F_PROT(IO_XML_CLOSE, io_xml_close);
F_PROT(IO_XML_RO_B, io_xml_ro_b);
F_PROT(IO_XML_RO_E, io_xml_ro_e);
F_PROT(IO_XML_RH_A, io_xml_rh_a);
F_PROT(IO_XML_RH_R, io_xml_rh_r);
F_PROT(IO_XML_RD_1, io_xml_rd_1);
F_PROT(IO_XML_RD_N, io_xml_rd_n);
F_PROT(IO_XML_RD_V, io_xml_rd_v);
F_PROT(IO_XML_RI_1, io_xml_ri_1);
F_PROT(IO_XML_RI_N, io_xml_ri_n);
F_PROT(IO_XML_RI_V, io_xml_ri_v);
F_PROT(IO_XML_RX_1, io_xml_rx_1);
F_PROT(IO_XML_RX_N, io_xml_rx_n);
F_PROT(IO_XML_RX_V, io_xml_rx_v);
F_PROT(IO_XML_RL_1, io_xml_rl_1);
F_PROT(IO_XML_RL_N, io_xml_rl_n);
F_PROT(IO_XML_RS, io_xml_rs);
F_PROT(IO_XML_RT, io_xml_rt);
F_PROT(IO_XML_WO_B, io_xml_wo_b);
F_PROT(IO_XML_WO_E, io_xml_wo_e);
F_PROT(IO_XML_WH_A, io_xml_wh_a);
F_PROT(IO_XML_WH_R, io_xml_wh_r);
F_PROT(IO_XML_WD_1, io_xml_wd_1);
F_PROT(IO_XML_WD_N, io_xml_wd_n);
F_PROT(IO_XML_WD_V, io_xml_wd_v);
F_PROT(IO_XML_WI_1, io_xml_wi_1);
F_PROT(IO_XML_WI_N, io_xml_wi_n);
F_PROT(IO_XML_WI_V, io_xml_wi_v);
F_PROT(IO_XML_WX_1, io_xml_wx_1);
F_PROT(IO_XML_WX_N, io_xml_wx_n);
F_PROT(IO_XML_WX_V, io_xml_wx_v);
F_PROT(IO_XML_WL_1, io_xml_wl_1);
F_PROT(IO_XML_WL_N, io_xml_wl_n);
F_PROT(IO_XML_WS, io_xml_ws);
F_PROT(IO_XML_WT, io_xml_wt);
 
// ---  class LOG_MSG
F_PROT(LOG_MSG_ERR, log_msg_err);
 
// ---  class MP_SYNC
F_PROT(MP_SYNC_000, mp_sync_000);
F_PROT(MP_SYNC_999, mp_sync_999);
F_PROT(MP_SYNC_CTR, mp_sync_ctr);
F_PROT(MP_SYNC_DTR, mp_sync_dtr);
F_PROT(MP_SYNC_INILCL, mp_sync_inilcl);
F_PROT(MP_SYNC_INIMPI, mp_sync_inimpi);
F_PROT(MP_SYNC_RST, mp_sync_rst);
F_PROT(MP_SYNC_HVALIDE, mp_sync_hvalide);
F_PROT(MP_SYNC_RECV, mp_sync_recv);
F_PROT(MP_SYNC_SEND, mp_sync_send);
F_PROT(MP_SYNC_WAIT, mp_sync_wait);
 
// ---  class MP_TYPE
F_PROT(MP_TYPE_IN4, mp_type_in4);
F_PROT(MP_TYPE_IN8, mp_type_in8);
F_PROT(MP_TYPE_INT, mp_type_int);
F_PROT(MP_TYPE_RE8, mp_type_re8);
 
// ---  class MP_UTIL
F_PROT(MP_UTIL_ESTMPROC, mp_util_estmproc);
F_PROT(MP_UTIL_ESTMAITRE, mp_util_estmaitre);
F_PROT(MP_UTIL_ERRHNDLR, mp_util_errhndlr);
F_PROT(MP_UTIL_ASGERR, mp_util_asgerr);
F_PROT(MP_UTIL_ASGERRCTX, mp_util_asgerrctx);
F_PROT(MP_UTIL_SYNCERR, mp_util_syncerr);
F_PROT(MP_UTIL_REQCOMM, mp_util_reqcomm);
F_PROT(MP_UTIL_DSYNC, mp_util_dsync);
F_PROT(MP_UTIL_ISYNC, mp_util_isync);
F_PROT(MP_UTIL_GETOP_SIGNEDMAX, mp_util_getop_signedmax);
F_PROT(MP_UTIL_GETOP_SIGNEDMIN, mp_util_getop_signedmin);
F_PROT(MP_UTIL_SIGNEDMAX, mp_util_signedmax);
F_PROT(MP_UTIL_SIGNEDMIN, mp_util_signedmin);
 
// ---  class OB_OBJC
F_PROT(OB_OBJC_000, ob_objc_000);
F_PROT(OB_OBJC_999, ob_objc_999);
F_PROT(OB_OBJC_CTR, ob_objc_ctr);
F_PROT(OB_OBJC_DTR, ob_objc_dtr);
F_PROT(OB_OBJC_PKL, ob_objc_pkl);
F_PROT(OB_OBJC_PKLALL, ob_objc_pklall);
F_PROT(OB_OBJC_UPK, ob_objc_upk);
F_PROT(OB_OBJC_UPKALL, ob_objc_upkall);
F_PROT(OB_OBJC_ERRH, ob_objc_errh);
F_PROT(OB_OBJC_ESTHBASE, ob_objc_esthbase);
F_PROT(OB_OBJC_HVALIDE, ob_objc_hvalide);
F_PROT(OB_OBJC_TVALIDE, ob_objc_tvalide);
F_PROT(OB_OBJC_HLIBRE, ob_objc_hlibre);
F_PROT(OB_OBJC_ASGCLS, ob_objc_asgcls);
F_PROT(OB_OBJC_ASGLNK, ob_objc_asglnk);
F_PROT(OB_OBJC_EFFLNK, ob_objc_efflnk);
F_PROT(OB_OBJC_REQCLS, ob_objc_reqcls);
F_PROT(OB_OBJC_REQNOMTYPE, ob_objc_reqnomtype);
F_PROT(OB_OBJC_REQNOMCMPL, ob_objc_reqnomcmpl);
F_PROT(OB_OBJC_REQNOBJ, ob_objc_reqnobj);
F_PROT(OB_OBJC_REQIOBJ, ob_objc_reqiobj);
 
// ---  class OB_OBJN
F_PROT(OB_OBJN_000, ob_objn_000);
F_PROT(OB_OBJN_999, ob_objn_999);
F_PROT(OB_OBJN_CTR, ob_objn_ctr);
F_PROT(OB_OBJN_CPY, ob_objn_cpy);
F_PROT(OB_OBJN_REQCNT, ob_objn_reqcnt);
F_PROT(OB_OBJN_REQDTA, ob_objn_reqdta);
F_PROT(OB_OBJN_DTR, ob_objn_dtr);
F_PROT(OB_OBJN_HVALIDE, ob_objn_hvalide);
F_PROT(OB_OBJN_TVALIDE, ob_objn_tvalide);
F_PROT(OB_OBJN_HEXIST, ob_objn_hexist);
 
// ---  class OB_VTBL
F_PROT(OB_VTBL_000, ob_vtbl_000);
F_PROT(OB_VTBL_999, ob_vtbl_999);
F_PROT(OB_VTBL_CTR, ob_vtbl_ctr);
F_PROT(OB_VTBL_DTR, ob_vtbl_dtr);
F_PROT(OB_VTBL_INI, ob_vtbl_ini);
F_PROT(OB_VTBL_RST, ob_vtbl_rst);
F_PROT(OB_VTBL_HVALIDE, ob_vtbl_hvalide);
F_PROT(OB_VTBL_AJTXXX, ob_vtbl_ajtxxx);
F_PROT(OB_VTBL_REQXXX, ob_vtbl_reqxxx);
F_PROT(OB_VTBL_AJTMSO, ob_vtbl_ajtmso);
F_PROT(OB_VTBL_AJTMTH, ob_vtbl_ajtmth);
F_PROT(OB_VTBL_REQMTH, ob_vtbl_reqmth);
 
// ---  class SO_ALLC
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_CTR, so_allc_ptr_ctr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_CTR_HNDL, so_allc_ptr_ctr_hndl);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_CTR_CPY, so_allc_ptr_ctr_cpy);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_ASG, so_allc_ptr_asg);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_DTR, so_allc_ptr_dtr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_CTR, so_allc_kptr1_ctr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_CTR_LEN, so_allc_kptr1_ctr_len);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_CTR_CPY, so_allc_kptr1_ctr_cpy);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_ASG, so_allc_kptr1_asg);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_DTR, so_allc_kptr1_dtr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_CTR, so_allc_kptr2_ctr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_CTR_LEN, so_allc_kptr2_ctr_len);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_CTR_CPY, so_allc_kptr2_ctr_cpy);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_ASG, so_allc_kptr2_asg);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_DTR, so_allc_kptr2_dtr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_CTR, so_allc_vptr1_ctr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_CTR_LEN, so_allc_vptr1_ctr_len);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_CTR_CPY, so_allc_vptr1_ctr_cpy);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_ASG, so_allc_vptr1_asg);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_DTR, so_allc_vptr1_dtr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_CTR, so_allc_vptr2_ctr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_CTR_LEN, so_allc_vptr2_ctr_len);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_CTR_CPY, so_allc_vptr2_ctr_cpy);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_ASG, so_allc_vptr2_asg);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_DTR, so_allc_vptr2_dtr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1C, so_allc_cst2b_1c);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1F, so_allc_cst2b_1f);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1H, so_allc_cst2b_1h);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1B, so_allc_cst2b_1b);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1L, so_allc_cst2b_1l);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1I2, so_allc_cst2b_1i2);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1I4, so_allc_cst2b_1i4);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1I8, so_allc_cst2b_1i8);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1R8, so_allc_cst2b_1r8);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1Z8, so_allc_cst2b_1z8);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_B, so_allc_cst2b_b);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_L, so_allc_cst2b_l);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_I2, so_allc_cst2b_i2);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_I4, so_allc_cst2b_i4);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_I8, so_allc_cst2b_i8);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_R8, so_allc_cst2b_r8);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_Z8, so_allc_cst2b_z8);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_S, so_allc_cst2b_s);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_REQKPTR, so_allc_reqkptr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_REQXPTR, so_allc_reqxptr);
M_PROT(SO_ALLC_M, so_allc_m, SO_ALLC_REQVPTR, so_allc_reqvptr);
F_PROT(SO_ALLC_INI, so_allc_ini);
F_PROT(SO_ALLC_RST, so_allc_rst);
F_PROT(SO_ALLC_TEST, so_allc_test);
F_PROT(SO_ALLC_CLCHSH, so_allc_clchsh);
F_PROT(SO_ALLC_CHKCRC, so_allc_chkcrc);
F_PROT(SO_ALLC_ALLCHR, so_allc_allchr);
F_PROT(SO_ALLC_ALLINT, so_allc_allint);
F_PROT(SO_ALLC_ALLIN2, so_allc_allin2);
F_PROT(SO_ALLC_ALLIN4, so_allc_allin4);
F_PROT(SO_ALLC_ALLIN8, so_allc_allin8);
F_PROT(SO_ALLC_ALLRE4, so_allc_allre4);
F_PROT(SO_ALLC_ALLRE8, so_allc_allre8);
F_PROT(SO_ALLC_LOG, so_allc_log);
F_PROT(SO_ALLC_MEMALL, so_allc_memall);
F_PROT(SO_ALLC_MEMMAX, so_allc_memmax);
F_PROT(SO_ALLC_HEXIST, so_allc_hexist);
F_PROT(SO_ALLC_REQLEN, so_allc_reqlen);
F_PROT(SO_ALLC_REQKIND, so_allc_reqkind);
F_PROT(SO_ALLC_REQXIND, so_allc_reqxind);
F_PROT(SO_ALLC_REQVIND, so_allc_reqvind);
F_PROT(SO_ALLC_REQKHND, so_allc_reqkhnd);
F_PROT(SO_ALLC_REQVHND, so_allc_reqvhnd);
F_PROT(SO_ALLC_ASGIN4, so_allc_asgin4);
F_PROT(SO_ALLC_REQIN4, so_allc_reqin4);
F_PROT(SO_ALLC_ASGIN8, so_allc_asgin8);
F_PROT(SO_ALLC_REQIN8, so_allc_reqin8);
F_PROT(SO_ALLC_ASGRE8, so_allc_asgre8);
F_PROT(SO_ALLC_REQRE8, so_allc_reqre8);
 
// ---  class SO_FUNC
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL, so_func_call);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL0, so_func_call0);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL1, so_func_call1);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL2, so_func_call2);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL3, so_func_call3);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL4, so_func_call4);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL5, so_func_call5);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL6, so_func_call6);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL7, so_func_call7);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL8, so_func_call8);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CALL9, so_func_call9);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC, so_func_cbfnc);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC0, so_func_cbfnc0);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC1, so_func_cbfnc1);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC2, so_func_cbfnc2);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC3, so_func_cbfnc3);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC4, so_func_cbfnc4);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC5, so_func_cbfnc5);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC6, so_func_cbfnc6);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC7, so_func_cbfnc7);
M_PROT(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC8, so_func_cbfnc8);
F_PROT(SO_FUNC_000, so_func_000);
F_PROT(SO_FUNC_999, so_func_999);
F_PROT(SO_FUNC_CTR, so_func_ctr);
F_PROT(SO_FUNC_DTR, so_func_dtr);
F_PROT(SO_FUNC_INIFSO, so_func_inifso);
F_PROT(SO_FUNC_INIFNC, so_func_inifnc);
F_PROT(SO_FUNC_INIMSO, so_func_inimso);
F_PROT(SO_FUNC_INIMTH, so_func_inimth);
F_PROT(SO_FUNC_RST, so_func_rst);
F_PROT(SO_FUNC_HVALIDE, so_func_hvalide);
F_PROT(SO_FUNC_ESTMTH, so_func_estmth);
F_PROT(SO_FUNC_REQHPRM, so_func_reqhprm);
F_PROT(SO_FUNC_REQFNC, so_func_reqfnc);
 
// ---  class SO_HASH
F_PROT(SO_HASH_000, so_hash_000);
F_PROT(SO_HASH_999, so_hash_999);
F_PROT(SO_HASH_CTR, so_hash_ctr);
F_PROT(SO_HASH_DTR, so_hash_dtr);
F_PROT(SO_HASH_INIB, so_hash_inib);
F_PROT(SO_HASH_INIC, so_hash_inic);
F_PROT(SO_HASH_INIH, so_hash_inih);
F_PROT(SO_HASH_RST, so_hash_rst);
F_PROT(SO_HASH_HVALIDE, so_hash_hvalide);
F_PROT(SO_HASH_ESTEQ, so_hash_esteq);
F_PROT(SO_HASH_ESTEQB, so_hash_esteqb);
F_PROT(SO_HASH_ESTEQC, so_hash_esteqc);
F_PROT(SO_HASH_ESTEQH, so_hash_esteqh);
 
// ---  class SO_MDUL
F_PROT(SO_MDUL_000, so_mdul_000);
F_PROT(SO_MDUL_999, so_mdul_999);
F_PROT(SO_MDUL_CTR, so_mdul_ctr);
F_PROT(SO_MDUL_DTR, so_mdul_dtr);
F_PROT(SO_MDUL_INI, so_mdul_ini);
F_PROT(SO_MDUL_RST, so_mdul_rst);
F_PROT(SO_MDUL_REQHBASE, so_mdul_reqhbase);
F_PROT(SO_MDUL_HVALIDE, so_mdul_hvalide);
F_PROT(SO_MDUL_CCHFNC, so_mdul_cchfnc);
F_PROT(SO_MDUL_REQFNC, so_mdul_reqfnc);
F_PROT(SO_MDUL_REQMTH, so_mdul_reqmth);
F_PROT(SO_MDUL_REQNOM, so_mdul_reqnom);
F_PROT(SO_MDUL_REQNOMTYP, so_mdul_reqnomtyp);
F_PROT(SO_MDUL_REQTYP, so_mdul_reqtyp);
F_PROT(SO_MDUL_REQVER, so_mdul_reqver);
 
// ---  class SO_SVPT
F_PROT(SO_SVPT_000, so_svpt_000);
F_PROT(SO_SVPT_999, so_svpt_999);
F_PROT(SO_SVPT_CTR, so_svpt_ctr);
F_PROT(SO_SVPT_DTR, so_svpt_dtr);
F_PROT(SO_SVPT_INI, so_svpt_ini);
F_PROT(SO_SVPT_RST, so_svpt_rst);
F_PROT(SO_SVPT_REQHBASE, so_svpt_reqhbase);
F_PROT(SO_SVPT_HVALIDE, so_svpt_hvalide);
F_PROT(SO_SVPT_GET, so_svpt_get);
F_PROT(SO_SVPT_SAVE, so_svpt_save);
F_PROT(SO_SVPT_RBCK, so_svpt_rbck);
 
// ---  class SO_UTIL
F_PROT(SO_UTIL_MDL000, so_util_mdl000);
F_PROT(SO_UTIL_MDL999, so_util_mdl999);
F_PROT(SO_UTIL_MDLVER, so_util_mdlver);
F_PROT(SO_UTIL_MDLCTR, so_util_mdlctr);
F_PROT(SO_UTIL_REQHMDLHBASE, so_util_reqhmdlhbase);
F_PROT(SO_UTIL_REQHMDLNOM, so_util_reqhmdlnom);
F_PROT(SO_UTIL_REQHMDLCOD, so_util_reqhmdlcod);
F_PROT(SO_UTIL_MDLITR, so_util_mdlitr);
 
// ---  class SO_VTBL
F_PROT(SO_VTBL_000, so_vtbl_000);
F_PROT(SO_VTBL_999, so_vtbl_999);
F_PROT(SO_VTBL_CTR, so_vtbl_ctr);
F_PROT(SO_VTBL_DTR, so_vtbl_dtr);
F_PROT(SO_VTBL_INI, so_vtbl_ini);
F_PROT(SO_VTBL_RST, so_vtbl_rst);
F_PROT(SO_VTBL_HVALIDE, so_vtbl_hvalide);
F_PROT(SO_VTBL_AJTHFNC, so_vtbl_ajthfnc);
F_PROT(SO_VTBL_AJTHMTH, so_vtbl_ajthmth);
F_PROT(SO_VTBL_AJTFNC, so_vtbl_ajtfnc);
F_PROT(SO_VTBL_AJTFSO, so_vtbl_ajtfso);
F_PROT(SO_VTBL_AJTMTH, so_vtbl_ajtmth);
F_PROT(SO_VTBL_AJTMSO, so_vtbl_ajtmso);
F_PROT(SO_VTBL_REQFNC, so_vtbl_reqfnc);
F_PROT(SO_VTBL_REQMTH, so_vtbl_reqmth);
 
// ---  class TR_CDWN
F_PROT(TR_CDWN_000, tr_cdwn_000);
F_PROT(TR_CDWN_999, tr_cdwn_999);
F_PROT(TR_CDWN_CTR, tr_cdwn_ctr);
F_PROT(TR_CDWN_DTR, tr_cdwn_dtr);
F_PROT(TR_CDWN_INI, tr_cdwn_ini);
F_PROT(TR_CDWN_RST, tr_cdwn_rst);
F_PROT(TR_CDWN_REQHBASE, tr_cdwn_reqhbase);
F_PROT(TR_CDWN_HVALIDE, tr_cdwn_hvalide);
F_PROT(TR_CDWN_SET, tr_cdwn_set);
F_PROT(TR_CDWN_START, tr_cdwn_start);
F_PROT(TR_CDWN_STOP, tr_cdwn_stop);
F_PROT(TR_CDWN_RESET, tr_cdwn_reset);
F_PROT(TR_CDWN_ESTFINI, tr_cdwn_estfini);
F_PROT(TR_CDWN_REQRESTE, tr_cdwn_reqreste);
 
// ---  class TR_CHRN
F_PROT(TR_CHRN_AJTZONE, tr_chrn_ajtzone);
F_PROT(TR_CHRN_EFFZONE, tr_chrn_effzone);
F_PROT(TR_CHRN_START, tr_chrn_start);
F_PROT(TR_CHRN_STOP, tr_chrn_stop);
F_PROT(TR_CHRN_REQTEMPS, tr_chrn_reqtemps);
F_PROT(TR_CHRN_LOGZONE, tr_chrn_logzone);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_h2d2_svc()
{
   static char libname[] = "h2d2_svc";
 
   // ---  class DBG_DMP
   F_RGST(DBG_DMP_DTBL, dbg_dmp_dtbl, libname);
   F_RGST(DBG_DMP_DTBL2, dbg_dmp_dtbl2, libname);
   F_RGST(DBG_DMP_ITBL, dbg_dmp_itbl, libname);
   F_RGST(DBG_DMP_ITBL2, dbg_dmp_itbl2, libname);
 
   // ---  class DS_LIST
   F_RGST(DS_LIST_000, ds_list_000, libname);
   F_RGST(DS_LIST_999, ds_list_999, libname);
   F_RGST(DS_LIST_PKL, ds_list_pkl, libname);
   F_RGST(DS_LIST_UPK, ds_list_upk, libname);
   F_RGST(DS_LIST_CTR, ds_list_ctr, libname);
   F_RGST(DS_LIST_DTR, ds_list_dtr, libname);
   F_RGST(DS_LIST_INI, ds_list_ini, libname);
   F_RGST(DS_LIST_RST, ds_list_rst, libname);
   F_RGST(DS_LIST_REQHBASE, ds_list_reqhbase, libname);
   F_RGST(DS_LIST_HVALIDE, ds_list_hvalide, libname);
   F_RGST(DS_LIST_PRN, ds_list_prn, libname);
   F_RGST(DS_LIST_AJTVAL, ds_list_ajtval, libname);
   F_RGST(DS_LIST_ASGVAL, ds_list_asgval, libname);
   F_RGST(DS_LIST_CLR, ds_list_clr, libname);
   F_RGST(DS_LIST_EFFVAL, ds_list_effval, libname);
   F_RGST(DS_LIST_REQDIM, ds_list_reqdim, libname);
   F_RGST(DS_LIST_REQVAL, ds_list_reqval, libname);
   F_RGST(DS_LIST_TRVVAL, ds_list_trvval, libname);
   F_RGST(DS_LIST_POP, ds_list_pop, libname);
   F_RGST(DS_LIST_PUSH, ds_list_push, libname);
   F_RGST(DS_LIST_PUSHI, ds_list_pushi, libname);
   F_RGST(DS_LIST_TOP, ds_list_top, libname);
   F_RGST(DS_LIST_TOPI, ds_list_topi, libname);
 
   // ---  class DS_LSTI
   F_RGST(DS_LSTI_000, ds_lsti_000, libname);
   F_RGST(DS_LSTI_999, ds_lsti_999, libname);
   F_RGST(DS_LSTI_CTR, ds_lsti_ctr, libname);
   F_RGST(DS_LSTI_DTR, ds_lsti_dtr, libname);
   F_RGST(DS_LSTI_INI, ds_lsti_ini, libname);
   F_RGST(DS_LSTI_RST, ds_lsti_rst, libname);
   F_RGST(DS_LSTI_REQHBASE, ds_lsti_reqhbase, libname);
   F_RGST(DS_LSTI_HVALIDE, ds_lsti_hvalide, libname);
   F_RGST(DS_LSTI_REQNXT, ds_lsti_reqnxt, libname);
 
   // ---  class DS_MAP
   F_RGST(DS_MAP_000, ds_map_000, libname);
   F_RGST(DS_MAP_999, ds_map_999, libname);
   F_RGST(DS_MAP_PKL, ds_map_pkl, libname);
   F_RGST(DS_MAP_UPK, ds_map_upk, libname);
   F_RGST(DS_MAP_CTR, ds_map_ctr, libname);
   F_RGST(DS_MAP_DTR, ds_map_dtr, libname);
   F_RGST(DS_MAP_INI, ds_map_ini, libname);
   F_RGST(DS_MAP_RST, ds_map_rst, libname);
   F_RGST(DS_MAP_REQHBASE, ds_map_reqhbase, libname);
   F_RGST(DS_MAP_HVALIDE, ds_map_hvalide, libname);
   F_RGST(DS_MAP_PRN, ds_map_prn, libname);
   F_RGST(DS_MAP_ASGVAL, ds_map_asgval, libname);
   F_RGST(DS_MAP_EFFCLF, ds_map_effclf, libname);
   F_RGST(DS_MAP_ESTCLF, ds_map_estclf, libname);
   F_RGST(DS_MAP_REQDIM, ds_map_reqdim, libname);
   F_RGST(DS_MAP_REQCLF, ds_map_reqclf, libname);
   F_RGST(DS_MAP_REQVAL, ds_map_reqval, libname);
 
   // ---  class ERR_OMP
   F_RGST(ERR_OMP_INI, err_omp_ini, libname);
   F_RGST(ERR_OMP_END, err_omp_end, libname);
   F_RGST(ERR_OMP_DST, err_omp_dst, libname);
   F_RGST(ERR_OMP_RDC, err_omp_rdc, libname);
 
   // ---  class IC_EXPR
   F_RGST(IC_EXPR_000, ic_expr_000, libname);
   F_RGST(IC_EXPR_999, ic_expr_999, libname);
   F_RGST(IC_EXPR_PKL, ic_expr_pkl, libname);
   F_RGST(IC_EXPR_UPK, ic_expr_upk, libname);
   F_RGST(IC_EXPR_CTR, ic_expr_ctr, libname);
   F_RGST(IC_EXPR_DTR, ic_expr_dtr, libname);
   F_RGST(IC_EXPR_INI, ic_expr_ini, libname);
   F_RGST(IC_EXPR_RST, ic_expr_rst, libname);
   F_RGST(IC_EXPR_REQHBASE, ic_expr_reqhbase, libname);
   F_RGST(IC_EXPR_HVALIDE, ic_expr_hvalide, libname);
   F_RGST(IC_EXPR_XEQ, ic_expr_xeq, libname);
   F_RGST(IC_EXPR_XEQ_L, ic_expr_xeq_l, libname);
   F_RGST(IC_EXPR_ASGCTX, ic_expr_asgctx, libname);
 
   // ---  class IC_FINP
   F_RGST(IC_FINP_000, ic_finp_000, libname);
   F_RGST(IC_FINP_999, ic_finp_999, libname);
   F_RGST(IC_FINP_PKL, ic_finp_pkl, libname);
   F_RGST(IC_FINP_UPK, ic_finp_upk, libname);
   F_RGST(IC_FINP_CTR, ic_finp_ctr, libname);
   F_RGST(IC_FINP_DTR, ic_finp_dtr, libname);
   F_RGST(IC_FINP_INI, ic_finp_ini, libname);
   F_RGST(IC_FINP_RST, ic_finp_rst, libname);
   F_RGST(IC_FINP_REQHBASE, ic_finp_reqhbase, libname);
   F_RGST(IC_FINP_HVALIDE, ic_finp_hvalide, libname);
   F_RGST(IC_FINP_LISCMD, ic_finp_liscmd, libname);
   F_RGST(IC_FINP_REQCRC, ic_finp_reqcrc, libname);
   F_RGST(IC_FINP_REQLCMD, ic_finp_reqlcmd, libname);
   F_RGST(IC_FINP_REQLCUR, ic_finp_reqlcur, libname);
   F_RGST(IC_FINP_REQLSRC, ic_finp_reqlsrc, libname);
   F_RGST(IC_FINP_REQNOM, ic_finp_reqnom, libname);
 
   // ---  class IC_FUNC
   F_RGST(IC_FUNC_000, ic_func_000, libname);
   F_RGST(IC_FUNC_999, ic_func_999, libname);
   F_RGST(IC_FUNC_PKL, ic_func_pkl, libname);
   F_RGST(IC_FUNC_UPK, ic_func_upk, libname);
   F_RGST(IC_FUNC_CTR, ic_func_ctr, libname);
   F_RGST(IC_FUNC_DTR, ic_func_dtr, libname);
   F_RGST(IC_FUNC_INI, ic_func_ini, libname);
   F_RGST(IC_FUNC_RST, ic_func_rst, libname);
   F_RGST(IC_FUNC_REQHBASE, ic_func_reqhbase, libname);
   F_RGST(IC_FUNC_HVALIDE, ic_func_hvalide, libname);
   F_RGST(IC_FUNC_INIWRITE, ic_func_iniwrite, libname);
   F_RGST(IC_FUNC_ENDWRITE, ic_func_endwrite, libname);
   F_RGST(IC_FUNC_ADDSTAT, ic_func_addstat, libname);
   F_RGST(IC_FUNC_CHKARGS, ic_func_chkargs, libname);
   F_RGST(IC_FUNC_POP, ic_func_pop, libname);
   F_RGST(IC_FUNC_PUSH, ic_func_push, libname);
   F_RGST(IC_FUNC_REQNFNC, ic_func_reqnfnc, libname);
   F_RGST(IC_FUNC_REQFSRC, ic_func_reqfsrc, libname);
   F_RGST(IC_FUNC_REQFFNC, ic_func_reqffnc, libname);
 
   // ---  class IC_ICMD
   F_RGST(IC_ICMD_000, ic_icmd_000, libname);
   F_RGST(IC_ICMD_999, ic_icmd_999, libname);
   F_RGST(IC_ICMD_PKL, ic_icmd_pkl, libname);
   F_RGST(IC_ICMD_UPK, ic_icmd_upk, libname);
   F_RGST(IC_ICMD_CTR, ic_icmd_ctr, libname);
   F_RGST(IC_ICMD_DTR, ic_icmd_dtr, libname);
   F_RGST(IC_ICMD_INI, ic_icmd_ini, libname);
   F_RGST(IC_ICMD_RST, ic_icmd_rst, libname);
   F_RGST(IC_ICMD_REQHBASE, ic_icmd_reqhbase, libname);
   F_RGST(IC_ICMD_HVALIDE, ic_icmd_hvalide, libname);
   F_RGST(IC_ICMD_EVAL, ic_icmd_eval, libname);
   F_RGST(IC_ICMD_XEQ, ic_icmd_xeq, libname);
   F_RGST(IC_ICMD_ADDFNC, ic_icmd_addfnc, libname);
   F_RGST(IC_ICMD_ADDCLS, ic_icmd_addcls, libname);
   F_RGST(IC_ICMD_ADDMDL, ic_icmd_addmdl, libname);
   F_RGST(IC_ICMD_REQSMB, ic_icmd_reqsmb, libname);
 
   // ---  class IC_LOG
   F_RGST(IC_LOG_XEQCTR, ic_log_xeqctr, libname);
   F_RGST(IC_LOG_XEQMTH, ic_log_xeqmth, libname);
   F_RGST(IC_LOG_OPBDOT, ic_log_opbdot, libname);
   F_RGST(IC_LOG_REQCLS, ic_log_reqcls, libname);
   F_RGST(IC_LOG_REQHDL, ic_log_reqhdl, libname);
 
   // ---  class IC_OS
   F_RGST(IC_OS_REQMDL, ic_os_reqmdl, libname);
   F_RGST(IC_OS_OPBDOT, ic_os_opbdot, libname);
   F_RGST(IC_OS_REQNOM, ic_os_reqnom, libname);
   F_RGST(IC_OS_REQHDL, ic_os_reqhdl, libname);
 
   // ---  class IC_PARS
   F_RGST(IC_PARS_CMDPIL, ic_pars_cmdpil, libname);
   F_RGST(IC_PARS_ASGKW, ic_pars_asgkw, libname);
 
   // ---  class IC_PFIC
   F_RGST(IC_PFIC_000, ic_pfic_000, libname);
   F_RGST(IC_PFIC_999, ic_pfic_999, libname);
   F_RGST(IC_PFIC_PKL, ic_pfic_pkl, libname);
   F_RGST(IC_PFIC_UPK, ic_pfic_upk, libname);
   F_RGST(IC_PFIC_CTR, ic_pfic_ctr, libname);
   F_RGST(IC_PFIC_DTR, ic_pfic_dtr, libname);
   F_RGST(IC_PFIC_INI, ic_pfic_ini, libname);
   F_RGST(IC_PFIC_RST, ic_pfic_rst, libname);
   F_RGST(IC_PFIC_REQHBASE, ic_pfic_reqhbase, libname);
   F_RGST(IC_PFIC_HVALIDE, ic_pfic_hvalide, libname);
   F_RGST(IC_PFIC_END, ic_pfic_end, libname);
   F_RGST(IC_PFIC_POP, ic_pfic_pop, libname);
   F_RGST(IC_PFIC_PUSH, ic_pfic_push, libname);
   F_RGST(IC_PFIC_TOP, ic_pfic_top, libname);
   F_RGST(IC_PFIC_SIZE, ic_pfic_size, libname);
 
   // ---  class IC_PILE
   F_RGST(IC_PILE_000, ic_pile_000, libname);
   F_RGST(IC_PILE_999, ic_pile_999, libname);
   F_RGST(IC_PILE_PKL, ic_pile_pkl, libname);
   F_RGST(IC_PILE_UPK, ic_pile_upk, libname);
   F_RGST(IC_PILE_CTR, ic_pile_ctr, libname);
   F_RGST(IC_PILE_DTR, ic_pile_dtr, libname);
   F_RGST(IC_PILE_INI, ic_pile_ini, libname);
   F_RGST(IC_PILE_RST, ic_pile_rst, libname);
   F_RGST(IC_PILE_REQHBASE, ic_pile_reqhbase, libname);
   F_RGST(IC_PILE_HVALIDE, ic_pile_hvalide, libname);
   F_RGST(IC_PILE_CMPCT, ic_pile_cmpct, libname);
   F_RGST(IC_PILE_DUMP, ic_pile_dump, libname);
   F_RGST(IC_PILE_ESTKW, ic_pile_estkw, libname);
   F_RGST(IC_PILE_ESTLBL, ic_pile_estlbl, libname);
   F_RGST(IC_PILE_ESTNOOP, ic_pile_estnoop, libname);
   F_RGST(IC_PILE_ESTOP, ic_pile_estop, libname);
   F_RGST(IC_PILE_ESTOPB, ic_pile_estopb, libname);
   F_RGST(IC_PILE_ESTOPU, ic_pile_estopu, libname);
   F_RGST(IC_PILE_ESTVAL, ic_pile_estval, libname);
   F_RGST(IC_PILE_ESTVALCST, ic_pile_estvalcst, libname);
   F_RGST(IC_PILE_ESTVALHDL, ic_pile_estvalhdl, libname);
   F_RGST(IC_PILE_ESTVALMDL, ic_pile_estvalmdl, libname);
   F_RGST(IC_PILE_ESTVALVAL, ic_pile_estvalval, libname);
   F_RGST(IC_PILE_ESTVAR, ic_pile_estvar, libname);
   F_RGST(IC_PILE_ESTVIDE, ic_pile_estvide, libname);
   F_RGST(IC_PILE_ESTVIDEFR, ic_pile_estvidefr, libname);
   F_RGST(IC_PILE_FRBSE, ic_pile_frbse, libname);
   F_RGST(IC_PILE_FRCLR, ic_pile_frclr, libname);
   F_RGST(IC_PILE_FRPOP, ic_pile_frpop, libname);
   F_RGST(IC_PILE_FRPUSH, ic_pile_frpush, libname);
   F_RGST(IC_PILE_FRTOP, ic_pile_frtop, libname);
   F_RGST(IC_PILE_FRSIZ, ic_pile_frsiz, libname);
   F_RGST(IC_PILE_FROPDEB, ic_pile_fropdeb, libname);
   F_RGST(IC_PILE_FROPFIN, ic_pile_fropfin, libname);
   F_RGST(IC_PILE_CLR, ic_pile_clr, libname);
   F_RGST(IC_PILE_POP, ic_pile_pop, libname);
   F_RGST(IC_PILE_PUSH, ic_pile_push, libname);
   F_RGST(IC_PILE_PUSHE, ic_pile_pushe, libname);
   F_RGST(IC_PILE_SIZ, ic_pile_siz, libname);
   F_RGST(IC_PILE_TOP, ic_pile_top, libname);
   F_RGST(IC_PILE_ESTVALOK, ic_pile_estvalok, libname);
   F_RGST(IC_PILE_TYP2TXT, ic_pile_typ2txt, libname);
   F_RGST(IC_PILE_VALDEC, ic_pile_valdec, libname);
   F_RGST(IC_PILE_VALENC, ic_pile_valenc, libname);
   F_RGST(IC_PILE_VALTYP, ic_pile_valtyp, libname);
   F_RGST(IC_PILE_VALDEB, ic_pile_valdeb, libname);
   F_RGST(IC_PILE_VALFIN, ic_pile_valfin, libname);
   F_RGST(IC_PILE_VALTAG, ic_pile_valtag, libname);
 
   // ---  class IC_SYS
   F_RGST(IC_SYS_REQMDL, ic_sys_reqmdl, libname);
   F_RGST(IC_SYS_OPBDOT, ic_sys_opbdot, libname);
   F_RGST(IC_SYS_REQNOM, ic_sys_reqnom, libname);
   F_RGST(IC_SYS_REQHDL, ic_sys_reqhdl, libname);
 
   // ---  class IC_TRA
   F_RGST(IC_TRA_XEQCTR, ic_tra_xeqctr, libname);
   F_RGST(IC_TRA_XEQMTH, ic_tra_xeqmth, libname);
   F_RGST(IC_TRA_OPBDOT, ic_tra_opbdot, libname);
   F_RGST(IC_TRA_REQCLS, ic_tra_reqcls, libname);
   F_RGST(IC_TRA_REQHDL, ic_tra_reqhdl, libname);
 
   // ---  class IC_DS_LIST
   F_RGST(IC_DS_LIST_XEQCTR, ic_ds_list_xeqctr, libname);
   F_RGST(IC_DS_LIST_XEQMTH, ic_ds_list_xeqmth, libname);
   F_RGST(IC_DS_LIST_REQCLS, ic_ds_list_reqcls, libname);
   F_RGST(IC_DS_LIST_REQHDL, ic_ds_list_reqhdl, libname);
 
   // ---  class IC_DS_LSTI
   F_RGST(IC_DS_LSTI_XEQCTR, ic_ds_lsti_xeqctr, libname);
   F_RGST(IC_DS_LSTI_XEQMTH, ic_ds_lsti_xeqmth, libname);
   F_RGST(IC_DS_LSTI_REQCLS, ic_ds_lsti_reqcls, libname);
   F_RGST(IC_DS_LSTI_REQHDL, ic_ds_lsti_reqhdl, libname);
 
   // ---  class IC_DS_MAP
   F_RGST(IC_DS_MAP_XEQCTR, ic_ds_map_xeqctr, libname);
   F_RGST(IC_DS_MAP_XEQMTH, ic_ds_map_xeqmth, libname);
   F_RGST(IC_DS_MAP_REQCLS, ic_ds_map_reqcls, libname);
   F_RGST(IC_DS_MAP_REQHDL, ic_ds_map_reqhdl, libname);
 
   // ---  class IC_DS_RNGE
   F_RGST(IC_DS_RNGE_XEQCTR, ic_ds_rnge_xeqctr, libname);
   F_RGST(IC_DS_RNGE_XEQMTH, ic_ds_rnge_xeqmth, libname);
   F_RGST(IC_DS_RNGE_REQCLS, ic_ds_rnge_reqcls, libname);
   F_RGST(IC_DS_RNGE_REQHDL, ic_ds_rnge_reqhdl, libname);
 
   // ---  class IC_IC_EXPR
   F_RGST(IC_IC_EXPR_XEQCTR, ic_ic_expr_xeqctr, libname);
   F_RGST(IC_IC_EXPR_XEQMTH, ic_ic_expr_xeqmth, libname);
   F_RGST(IC_IC_EXPR_REQCLS, ic_ic_expr_reqcls, libname);
   F_RGST(IC_IC_EXPR_REQHDL, ic_ic_expr_reqhdl, libname);
 
   // ---  class IC_TR_CDWN
   F_RGST(IC_TR_CDWN_XEQCTR, ic_tr_cdwn_xeqctr, libname);
   F_RGST(IC_TR_CDWN_XEQMTH, ic_tr_cdwn_xeqmth, libname);
   F_RGST(IC_TR_CDWN_REQCLS, ic_tr_cdwn_reqcls, libname);
   F_RGST(IC_TR_CDWN_REQHDL, ic_tr_cdwn_reqhdl, libname);
 
   // ---  class IC_TR_CHRN
   F_RGST(IC_TR_CHRN_XEQCTR, ic_tr_chrn_xeqctr, libname);
   F_RGST(IC_TR_CHRN_XEQMTH, ic_tr_chrn_xeqmth, libname);
   F_RGST(IC_TR_CHRN_OPBDOT, ic_tr_chrn_opbdot, libname);
   F_RGST(IC_TR_CHRN_REQCLS, ic_tr_chrn_reqcls, libname);
   F_RGST(IC_TR_CHRN_REQHDL, ic_tr_chrn_reqhdl, libname);
 
   // ---  class IO_I08
   F_RGST(IO_I08_000, io_i08_000, libname);
   F_RGST(IO_I08_999, io_i08_999, libname);
   F_RGST(IO_I08_CTR, io_i08_ctr, libname);
   F_RGST(IO_I08_DTR, io_i08_dtr, libname);
   F_RGST(IO_I08_INI, io_i08_ini, libname);
   F_RGST(IO_I08_RST, io_i08_rst, libname);
   F_RGST(IO_I08_REQHBASE, io_i08_reqhbase, libname);
   F_RGST(IO_I08_HVALIDE, io_i08_hvalide, libname);
   F_RGST(IO_I08_OPEN, io_i08_open, libname);
   F_RGST(IO_I08_CLOSE, io_i08_close, libname);
   F_RGST(IO_I08_ENDL, io_i08_endl, libname);
   F_RGST(IO_I08_SKIP, io_i08_skip, libname);
   F_RGST(IO_I08_GETPOS, io_i08_getpos, libname);
   F_RGST(IO_I08_SETPOS, io_i08_setpos, libname);
   F_RGST(IO_I08_WHDR, io_i08_whdr, libname);
   F_RGST(IO_I08_WS, io_i08_ws, libname);
   F_RGST(IO_I08_WI_1, io_i08_wi_1, libname);
   F_RGST(IO_I08_WI_N, io_i08_wi_n, libname);
   F_RGST(IO_I08_WD_1, io_i08_wd_1, libname);
   F_RGST(IO_I08_WD_N, io_i08_wd_n, libname);
   F_RGST(IO_I08_RHDR, io_i08_rhdr, libname);
   F_RGST(IO_I08_RS, io_i08_rs, libname);
   F_RGST(IO_I08_RI_1, io_i08_ri_1, libname);
   F_RGST(IO_I08_RI_N, io_i08_ri_n, libname);
   F_RGST(IO_I08_RD_1, io_i08_rd_1, libname);
   F_RGST(IO_I08_RD_N, io_i08_rd_n, libname);
 
   // ---  class IO_UTIL
   F_RGST(IO_UTIL_FREEUNIT, io_util_freeunit, libname);
   F_RGST(IO_UTIL_FICDUM, io_util_ficdum, libname);
   F_RGST(IO_UTIL_MODVALIDE, io_util_modvalide, libname);
   F_RGST(IO_UTIL_MODACTIF, io_util_modactif, libname);
   F_RGST(IO_UTIL_STR2MOD, io_util_str2mod, libname);
 
   // ---  class IO_XML
   F_RGST(IO_XML_000, io_xml_000, libname);
   F_RGST(IO_XML_999, io_xml_999, libname);
   F_RGST(IO_XML_CTR, io_xml_ctr, libname);
   F_RGST(IO_XML_DTR, io_xml_dtr, libname);
   F_RGST(IO_XML_INI, io_xml_ini, libname);
   F_RGST(IO_XML_RST, io_xml_rst, libname);
   F_RGST(IO_XML_REQHBASE, io_xml_reqhbase, libname);
   F_RGST(IO_XML_HVALIDE, io_xml_hvalide, libname);
   F_RGST(IO_XML_OPEN, io_xml_open, libname);
   F_RGST(IO_XML_CLOSE, io_xml_close, libname);
   F_RGST(IO_XML_RO_B, io_xml_ro_b, libname);
   F_RGST(IO_XML_RO_E, io_xml_ro_e, libname);
   F_RGST(IO_XML_RH_A, io_xml_rh_a, libname);
   F_RGST(IO_XML_RH_R, io_xml_rh_r, libname);
   F_RGST(IO_XML_RD_1, io_xml_rd_1, libname);
   F_RGST(IO_XML_RD_N, io_xml_rd_n, libname);
   F_RGST(IO_XML_RD_V, io_xml_rd_v, libname);
   F_RGST(IO_XML_RI_1, io_xml_ri_1, libname);
   F_RGST(IO_XML_RI_N, io_xml_ri_n, libname);
   F_RGST(IO_XML_RI_V, io_xml_ri_v, libname);
   F_RGST(IO_XML_RX_1, io_xml_rx_1, libname);
   F_RGST(IO_XML_RX_N, io_xml_rx_n, libname);
   F_RGST(IO_XML_RX_V, io_xml_rx_v, libname);
   F_RGST(IO_XML_RL_1, io_xml_rl_1, libname);
   F_RGST(IO_XML_RL_N, io_xml_rl_n, libname);
   F_RGST(IO_XML_RS, io_xml_rs, libname);
   F_RGST(IO_XML_RT, io_xml_rt, libname);
   F_RGST(IO_XML_WO_B, io_xml_wo_b, libname);
   F_RGST(IO_XML_WO_E, io_xml_wo_e, libname);
   F_RGST(IO_XML_WH_A, io_xml_wh_a, libname);
   F_RGST(IO_XML_WH_R, io_xml_wh_r, libname);
   F_RGST(IO_XML_WD_1, io_xml_wd_1, libname);
   F_RGST(IO_XML_WD_N, io_xml_wd_n, libname);
   F_RGST(IO_XML_WD_V, io_xml_wd_v, libname);
   F_RGST(IO_XML_WI_1, io_xml_wi_1, libname);
   F_RGST(IO_XML_WI_N, io_xml_wi_n, libname);
   F_RGST(IO_XML_WI_V, io_xml_wi_v, libname);
   F_RGST(IO_XML_WX_1, io_xml_wx_1, libname);
   F_RGST(IO_XML_WX_N, io_xml_wx_n, libname);
   F_RGST(IO_XML_WX_V, io_xml_wx_v, libname);
   F_RGST(IO_XML_WL_1, io_xml_wl_1, libname);
   F_RGST(IO_XML_WL_N, io_xml_wl_n, libname);
   F_RGST(IO_XML_WS, io_xml_ws, libname);
   F_RGST(IO_XML_WT, io_xml_wt, libname);
 
   // ---  class LOG_MSG
   F_RGST(LOG_MSG_ERR, log_msg_err, libname);
 
   // ---  class MP_SYNC
   F_RGST(MP_SYNC_000, mp_sync_000, libname);
   F_RGST(MP_SYNC_999, mp_sync_999, libname);
   F_RGST(MP_SYNC_CTR, mp_sync_ctr, libname);
   F_RGST(MP_SYNC_DTR, mp_sync_dtr, libname);
   F_RGST(MP_SYNC_INILCL, mp_sync_inilcl, libname);
   F_RGST(MP_SYNC_INIMPI, mp_sync_inimpi, libname);
   F_RGST(MP_SYNC_RST, mp_sync_rst, libname);
   F_RGST(MP_SYNC_HVALIDE, mp_sync_hvalide, libname);
   F_RGST(MP_SYNC_RECV, mp_sync_recv, libname);
   F_RGST(MP_SYNC_SEND, mp_sync_send, libname);
   F_RGST(MP_SYNC_WAIT, mp_sync_wait, libname);
 
   // ---  class MP_TYPE
   F_RGST(MP_TYPE_IN4, mp_type_in4, libname);
   F_RGST(MP_TYPE_IN8, mp_type_in8, libname);
   F_RGST(MP_TYPE_INT, mp_type_int, libname);
   F_RGST(MP_TYPE_RE8, mp_type_re8, libname);
 
   // ---  class MP_UTIL
   F_RGST(MP_UTIL_ESTMPROC, mp_util_estmproc, libname);
   F_RGST(MP_UTIL_ESTMAITRE, mp_util_estmaitre, libname);
   F_RGST(MP_UTIL_ERRHNDLR, mp_util_errhndlr, libname);
   F_RGST(MP_UTIL_ASGERR, mp_util_asgerr, libname);
   F_RGST(MP_UTIL_ASGERRCTX, mp_util_asgerrctx, libname);
   F_RGST(MP_UTIL_SYNCERR, mp_util_syncerr, libname);
   F_RGST(MP_UTIL_REQCOMM, mp_util_reqcomm, libname);
   F_RGST(MP_UTIL_DSYNC, mp_util_dsync, libname);
   F_RGST(MP_UTIL_ISYNC, mp_util_isync, libname);
   F_RGST(MP_UTIL_GETOP_SIGNEDMAX, mp_util_getop_signedmax, libname);
   F_RGST(MP_UTIL_GETOP_SIGNEDMIN, mp_util_getop_signedmin, libname);
   F_RGST(MP_UTIL_SIGNEDMAX, mp_util_signedmax, libname);
   F_RGST(MP_UTIL_SIGNEDMIN, mp_util_signedmin, libname);
 
   // ---  class OB_OBJC
   F_RGST(OB_OBJC_000, ob_objc_000, libname);
   F_RGST(OB_OBJC_999, ob_objc_999, libname);
   F_RGST(OB_OBJC_CTR, ob_objc_ctr, libname);
   F_RGST(OB_OBJC_DTR, ob_objc_dtr, libname);
   F_RGST(OB_OBJC_PKL, ob_objc_pkl, libname);
   F_RGST(OB_OBJC_PKLALL, ob_objc_pklall, libname);
   F_RGST(OB_OBJC_UPK, ob_objc_upk, libname);
   F_RGST(OB_OBJC_UPKALL, ob_objc_upkall, libname);
   F_RGST(OB_OBJC_ERRH, ob_objc_errh, libname);
   F_RGST(OB_OBJC_ESTHBASE, ob_objc_esthbase, libname);
   F_RGST(OB_OBJC_HVALIDE, ob_objc_hvalide, libname);
   F_RGST(OB_OBJC_TVALIDE, ob_objc_tvalide, libname);
   F_RGST(OB_OBJC_HLIBRE, ob_objc_hlibre, libname);
   F_RGST(OB_OBJC_ASGCLS, ob_objc_asgcls, libname);
   F_RGST(OB_OBJC_ASGLNK, ob_objc_asglnk, libname);
   F_RGST(OB_OBJC_EFFLNK, ob_objc_efflnk, libname);
   F_RGST(OB_OBJC_REQCLS, ob_objc_reqcls, libname);
   F_RGST(OB_OBJC_REQNOMTYPE, ob_objc_reqnomtype, libname);
   F_RGST(OB_OBJC_REQNOMCMPL, ob_objc_reqnomcmpl, libname);
   F_RGST(OB_OBJC_REQNOBJ, ob_objc_reqnobj, libname);
   F_RGST(OB_OBJC_REQIOBJ, ob_objc_reqiobj, libname);
 
   // ---  class OB_OBJN
   F_RGST(OB_OBJN_000, ob_objn_000, libname);
   F_RGST(OB_OBJN_999, ob_objn_999, libname);
   F_RGST(OB_OBJN_CTR, ob_objn_ctr, libname);
   F_RGST(OB_OBJN_CPY, ob_objn_cpy, libname);
   F_RGST(OB_OBJN_REQCNT, ob_objn_reqcnt, libname);
   F_RGST(OB_OBJN_REQDTA, ob_objn_reqdta, libname);
   F_RGST(OB_OBJN_DTR, ob_objn_dtr, libname);
   F_RGST(OB_OBJN_HVALIDE, ob_objn_hvalide, libname);
   F_RGST(OB_OBJN_TVALIDE, ob_objn_tvalide, libname);
   F_RGST(OB_OBJN_HEXIST, ob_objn_hexist, libname);
 
   // ---  class OB_VTBL
   F_RGST(OB_VTBL_000, ob_vtbl_000, libname);
   F_RGST(OB_VTBL_999, ob_vtbl_999, libname);
   F_RGST(OB_VTBL_CTR, ob_vtbl_ctr, libname);
   F_RGST(OB_VTBL_DTR, ob_vtbl_dtr, libname);
   F_RGST(OB_VTBL_INI, ob_vtbl_ini, libname);
   F_RGST(OB_VTBL_RST, ob_vtbl_rst, libname);
   F_RGST(OB_VTBL_HVALIDE, ob_vtbl_hvalide, libname);
   F_RGST(OB_VTBL_AJTXXX, ob_vtbl_ajtxxx, libname);
   F_RGST(OB_VTBL_REQXXX, ob_vtbl_reqxxx, libname);
   F_RGST(OB_VTBL_AJTMSO, ob_vtbl_ajtmso, libname);
   F_RGST(OB_VTBL_AJTMTH, ob_vtbl_ajtmth, libname);
   F_RGST(OB_VTBL_REQMTH, ob_vtbl_reqmth, libname);
 
   // ---  class SO_ALLC
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_CTR, so_allc_ptr_ctr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_CTR_HNDL, so_allc_ptr_ctr_hndl, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_CTR_CPY, so_allc_ptr_ctr_cpy, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_ASG, so_allc_ptr_asg, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_PTR_DTR, so_allc_ptr_dtr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_CTR, so_allc_kptr1_ctr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_CTR_LEN, so_allc_kptr1_ctr_len, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_CTR_CPY, so_allc_kptr1_ctr_cpy, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_ASG, so_allc_kptr1_asg, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR1_DTR, so_allc_kptr1_dtr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_CTR, so_allc_kptr2_ctr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_CTR_LEN, so_allc_kptr2_ctr_len, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_CTR_CPY, so_allc_kptr2_ctr_cpy, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_ASG, so_allc_kptr2_asg, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_KPTR2_DTR, so_allc_kptr2_dtr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_CTR, so_allc_vptr1_ctr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_CTR_LEN, so_allc_vptr1_ctr_len, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_CTR_CPY, so_allc_vptr1_ctr_cpy, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_ASG, so_allc_vptr1_asg, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR1_DTR, so_allc_vptr1_dtr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_CTR, so_allc_vptr2_ctr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_CTR_LEN, so_allc_vptr2_ctr_len, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_CTR_CPY, so_allc_vptr2_ctr_cpy, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_ASG, so_allc_vptr2_asg, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_VPTR2_DTR, so_allc_vptr2_dtr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1C, so_allc_cst2b_1c, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1F, so_allc_cst2b_1f, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1H, so_allc_cst2b_1h, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1B, so_allc_cst2b_1b, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1L, so_allc_cst2b_1l, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1I2, so_allc_cst2b_1i2, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1I4, so_allc_cst2b_1i4, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1I8, so_allc_cst2b_1i8, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1R8, so_allc_cst2b_1r8, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_1Z8, so_allc_cst2b_1z8, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_B, so_allc_cst2b_b, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_L, so_allc_cst2b_l, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_I2, so_allc_cst2b_i2, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_I4, so_allc_cst2b_i4, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_I8, so_allc_cst2b_i8, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_R8, so_allc_cst2b_r8, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_Z8, so_allc_cst2b_z8, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_CST2B_S, so_allc_cst2b_s, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_REQKPTR, so_allc_reqkptr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_REQXPTR, so_allc_reqxptr, libname);
   M_RGST(SO_ALLC_M, so_allc_m, SO_ALLC_REQVPTR, so_allc_reqvptr, libname);
   F_RGST(SO_ALLC_INI, so_allc_ini, libname);
   F_RGST(SO_ALLC_RST, so_allc_rst, libname);
   F_RGST(SO_ALLC_TEST, so_allc_test, libname);
   F_RGST(SO_ALLC_CLCHSH, so_allc_clchsh, libname);
   F_RGST(SO_ALLC_CHKCRC, so_allc_chkcrc, libname);
   F_RGST(SO_ALLC_ALLCHR, so_allc_allchr, libname);
   F_RGST(SO_ALLC_ALLINT, so_allc_allint, libname);
   F_RGST(SO_ALLC_ALLIN2, so_allc_allin2, libname);
   F_RGST(SO_ALLC_ALLIN4, so_allc_allin4, libname);
   F_RGST(SO_ALLC_ALLIN8, so_allc_allin8, libname);
   F_RGST(SO_ALLC_ALLRE4, so_allc_allre4, libname);
   F_RGST(SO_ALLC_ALLRE8, so_allc_allre8, libname);
   F_RGST(SO_ALLC_LOG, so_allc_log, libname);
   F_RGST(SO_ALLC_MEMALL, so_allc_memall, libname);
   F_RGST(SO_ALLC_MEMMAX, so_allc_memmax, libname);
   F_RGST(SO_ALLC_HEXIST, so_allc_hexist, libname);
   F_RGST(SO_ALLC_REQLEN, so_allc_reqlen, libname);
   F_RGST(SO_ALLC_REQKIND, so_allc_reqkind, libname);
   F_RGST(SO_ALLC_REQXIND, so_allc_reqxind, libname);
   F_RGST(SO_ALLC_REQVIND, so_allc_reqvind, libname);
   F_RGST(SO_ALLC_REQKHND, so_allc_reqkhnd, libname);
   F_RGST(SO_ALLC_REQVHND, so_allc_reqvhnd, libname);
   F_RGST(SO_ALLC_ASGIN4, so_allc_asgin4, libname);
   F_RGST(SO_ALLC_REQIN4, so_allc_reqin4, libname);
   F_RGST(SO_ALLC_ASGIN8, so_allc_asgin8, libname);
   F_RGST(SO_ALLC_REQIN8, so_allc_reqin8, libname);
   F_RGST(SO_ALLC_ASGRE8, so_allc_asgre8, libname);
   F_RGST(SO_ALLC_REQRE8, so_allc_reqre8, libname);
 
   // ---  class SO_FUNC
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL, so_func_call, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL0, so_func_call0, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL1, so_func_call1, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL2, so_func_call2, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL3, so_func_call3, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL4, so_func_call4, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL5, so_func_call5, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL6, so_func_call6, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL7, so_func_call7, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL8, so_func_call8, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CALL9, so_func_call9, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC, so_func_cbfnc, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC0, so_func_cbfnc0, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC1, so_func_cbfnc1, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC2, so_func_cbfnc2, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC3, so_func_cbfnc3, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC4, so_func_cbfnc4, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC5, so_func_cbfnc5, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC6, so_func_cbfnc6, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC7, so_func_cbfnc7, libname);
   M_RGST(SO_FUNC_M, so_func_m, SO_FUNC_CBFNC8, so_func_cbfnc8, libname);
   F_RGST(SO_FUNC_000, so_func_000, libname);
   F_RGST(SO_FUNC_999, so_func_999, libname);
   F_RGST(SO_FUNC_CTR, so_func_ctr, libname);
   F_RGST(SO_FUNC_DTR, so_func_dtr, libname);
   F_RGST(SO_FUNC_INIFSO, so_func_inifso, libname);
   F_RGST(SO_FUNC_INIFNC, so_func_inifnc, libname);
   F_RGST(SO_FUNC_INIMSO, so_func_inimso, libname);
   F_RGST(SO_FUNC_INIMTH, so_func_inimth, libname);
   F_RGST(SO_FUNC_RST, so_func_rst, libname);
   F_RGST(SO_FUNC_HVALIDE, so_func_hvalide, libname);
   F_RGST(SO_FUNC_ESTMTH, so_func_estmth, libname);
   F_RGST(SO_FUNC_REQHPRM, so_func_reqhprm, libname);
   F_RGST(SO_FUNC_REQFNC, so_func_reqfnc, libname);
 
   // ---  class SO_HASH
   F_RGST(SO_HASH_000, so_hash_000, libname);
   F_RGST(SO_HASH_999, so_hash_999, libname);
   F_RGST(SO_HASH_CTR, so_hash_ctr, libname);
   F_RGST(SO_HASH_DTR, so_hash_dtr, libname);
   F_RGST(SO_HASH_INIB, so_hash_inib, libname);
   F_RGST(SO_HASH_INIC, so_hash_inic, libname);
   F_RGST(SO_HASH_INIH, so_hash_inih, libname);
   F_RGST(SO_HASH_RST, so_hash_rst, libname);
   F_RGST(SO_HASH_HVALIDE, so_hash_hvalide, libname);
   F_RGST(SO_HASH_ESTEQ, so_hash_esteq, libname);
   F_RGST(SO_HASH_ESTEQB, so_hash_esteqb, libname);
   F_RGST(SO_HASH_ESTEQC, so_hash_esteqc, libname);
   F_RGST(SO_HASH_ESTEQH, so_hash_esteqh, libname);
 
   // ---  class SO_MDUL
   F_RGST(SO_MDUL_000, so_mdul_000, libname);
   F_RGST(SO_MDUL_999, so_mdul_999, libname);
   F_RGST(SO_MDUL_CTR, so_mdul_ctr, libname);
   F_RGST(SO_MDUL_DTR, so_mdul_dtr, libname);
   F_RGST(SO_MDUL_INI, so_mdul_ini, libname);
   F_RGST(SO_MDUL_RST, so_mdul_rst, libname);
   F_RGST(SO_MDUL_REQHBASE, so_mdul_reqhbase, libname);
   F_RGST(SO_MDUL_HVALIDE, so_mdul_hvalide, libname);
   F_RGST(SO_MDUL_CCHFNC, so_mdul_cchfnc, libname);
   F_RGST(SO_MDUL_REQFNC, so_mdul_reqfnc, libname);
   F_RGST(SO_MDUL_REQMTH, so_mdul_reqmth, libname);
   F_RGST(SO_MDUL_REQNOM, so_mdul_reqnom, libname);
   F_RGST(SO_MDUL_REQNOMTYP, so_mdul_reqnomtyp, libname);
   F_RGST(SO_MDUL_REQTYP, so_mdul_reqtyp, libname);
   F_RGST(SO_MDUL_REQVER, so_mdul_reqver, libname);
 
   // ---  class SO_SVPT
   F_RGST(SO_SVPT_000, so_svpt_000, libname);
   F_RGST(SO_SVPT_999, so_svpt_999, libname);
   F_RGST(SO_SVPT_CTR, so_svpt_ctr, libname);
   F_RGST(SO_SVPT_DTR, so_svpt_dtr, libname);
   F_RGST(SO_SVPT_INI, so_svpt_ini, libname);
   F_RGST(SO_SVPT_RST, so_svpt_rst, libname);
   F_RGST(SO_SVPT_REQHBASE, so_svpt_reqhbase, libname);
   F_RGST(SO_SVPT_HVALIDE, so_svpt_hvalide, libname);
   F_RGST(SO_SVPT_GET, so_svpt_get, libname);
   F_RGST(SO_SVPT_SAVE, so_svpt_save, libname);
   F_RGST(SO_SVPT_RBCK, so_svpt_rbck, libname);
 
   // ---  class SO_UTIL
   F_RGST(SO_UTIL_MDL000, so_util_mdl000, libname);
   F_RGST(SO_UTIL_MDL999, so_util_mdl999, libname);
   F_RGST(SO_UTIL_MDLVER, so_util_mdlver, libname);
   F_RGST(SO_UTIL_MDLCTR, so_util_mdlctr, libname);
   F_RGST(SO_UTIL_REQHMDLHBASE, so_util_reqhmdlhbase, libname);
   F_RGST(SO_UTIL_REQHMDLNOM, so_util_reqhmdlnom, libname);
   F_RGST(SO_UTIL_REQHMDLCOD, so_util_reqhmdlcod, libname);
   F_RGST(SO_UTIL_MDLITR, so_util_mdlitr, libname);
 
   // ---  class SO_VTBL
   F_RGST(SO_VTBL_000, so_vtbl_000, libname);
   F_RGST(SO_VTBL_999, so_vtbl_999, libname);
   F_RGST(SO_VTBL_CTR, so_vtbl_ctr, libname);
   F_RGST(SO_VTBL_DTR, so_vtbl_dtr, libname);
   F_RGST(SO_VTBL_INI, so_vtbl_ini, libname);
   F_RGST(SO_VTBL_RST, so_vtbl_rst, libname);
   F_RGST(SO_VTBL_HVALIDE, so_vtbl_hvalide, libname);
   F_RGST(SO_VTBL_AJTHFNC, so_vtbl_ajthfnc, libname);
   F_RGST(SO_VTBL_AJTHMTH, so_vtbl_ajthmth, libname);
   F_RGST(SO_VTBL_AJTFNC, so_vtbl_ajtfnc, libname);
   F_RGST(SO_VTBL_AJTFSO, so_vtbl_ajtfso, libname);
   F_RGST(SO_VTBL_AJTMTH, so_vtbl_ajtmth, libname);
   F_RGST(SO_VTBL_AJTMSO, so_vtbl_ajtmso, libname);
   F_RGST(SO_VTBL_REQFNC, so_vtbl_reqfnc, libname);
   F_RGST(SO_VTBL_REQMTH, so_vtbl_reqmth, libname);
 
   // ---  class TR_CDWN
   F_RGST(TR_CDWN_000, tr_cdwn_000, libname);
   F_RGST(TR_CDWN_999, tr_cdwn_999, libname);
   F_RGST(TR_CDWN_CTR, tr_cdwn_ctr, libname);
   F_RGST(TR_CDWN_DTR, tr_cdwn_dtr, libname);
   F_RGST(TR_CDWN_INI, tr_cdwn_ini, libname);
   F_RGST(TR_CDWN_RST, tr_cdwn_rst, libname);
   F_RGST(TR_CDWN_REQHBASE, tr_cdwn_reqhbase, libname);
   F_RGST(TR_CDWN_HVALIDE, tr_cdwn_hvalide, libname);
   F_RGST(TR_CDWN_SET, tr_cdwn_set, libname);
   F_RGST(TR_CDWN_START, tr_cdwn_start, libname);
   F_RGST(TR_CDWN_STOP, tr_cdwn_stop, libname);
   F_RGST(TR_CDWN_RESET, tr_cdwn_reset, libname);
   F_RGST(TR_CDWN_ESTFINI, tr_cdwn_estfini, libname);
   F_RGST(TR_CDWN_REQRESTE, tr_cdwn_reqreste, libname);
 
   // ---  class TR_CHRN
   F_RGST(TR_CHRN_AJTZONE, tr_chrn_ajtzone, libname);
   F_RGST(TR_CHRN_EFFZONE, tr_chrn_effzone, libname);
   F_RGST(TR_CHRN_START, tr_chrn_start, libname);
   F_RGST(TR_CHRN_STOP, tr_chrn_stop, libname);
   F_RGST(TR_CHRN_REQTEMPS, tr_chrn_reqtemps, libname);
   F_RGST(TR_CHRN_LOGZONE, tr_chrn_logzone, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
