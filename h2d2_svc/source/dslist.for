C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DS_LIST_000
C     INTEGER DS_LIST_999
C     INTEGER DS_LIST_PKL
C     INTEGER DS_LIST_UPK
C     INTEGER DS_LIST_CTR
C     INTEGER DS_LIST_DTR
C     INTEGER DS_LIST_INI
C     INTEGER DS_LIST_RST
C     INTEGER DS_LIST_REQHBASE
C     LOGICAL DS_LIST_HVALIDE
C     INTEGER DS_LIST_PRN
C     INTEGER DS_LIST_AJTVAL
C     INTEGER DS_LIST_ASGVAL
C     INTEGER DS_LIST_CLR
C     INTEGER DS_LIST_EFFVAL
C     INTEGER DS_LIST_REQDIM
C     INTEGER DS_LIST_REQVAL
C     INTEGER DS_LIST_TRVVAL
C     INTEGER DS_LIST_POP
C     INTEGER DS_LIST_PUSH
C     INTEGER DS_LIST_PUSHI
C     INTEGER DS_LIST_TOP
C     INTEGER DS_LIST_TOPI
C   Private:
C     INTEGER DS_LIST_REQSELF
C     INTEGER DS_LIST_RAZ
C
C************************************************************************

      MODULE DS_LIST_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: DS_LIST_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: DS_LIST_SELF_T
         INTEGER*8 XLST       ! handle eXterne
      END TYPE DS_LIST_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée DS_LIST_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_REQSELF(HOBJ) !, SELF)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (DS_LIST_SELF_T), POINTER :: DS_LIST_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (DS_LIST_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      DS_LIST_REQSELF => SELF ! ERR_TYP()
      RETURN
      END FUNCTION DS_LIST_REQSELF

      END MODULE DS_LIST_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_000
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(DS_LIST_HBASE,
     &                   'List data structure')

      DS_LIST_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_999
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INCLUDE 'dslist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL DS_LIST_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(DS_LIST_HBASE,
     &                   DS_LIST_DTR)

      DS_LIST_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IV
      CHARACTER*(256) SVAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Les valeurs
      IERR = IO_XML_WI_1(HXML, DS_LIST_REQDIM(HOBJ))
      DO IV=1,DS_LIST_REQDIM(HOBJ)
         IERR = DS_LIST_REQVAL(HOBJ, IV, SVAL)
         IERR = IO_XML_WS(HXML, SVAL(1:SP_STRN_LEN(SVAL)))
         IF (ERR_BAD()) GOTO 199
      ENDDO
199   CONTINUE

      DS_LIST_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction DS_LIST_UPK restaure l'objet à partir d'une sauvegarde sous
C     format XML. Le handle passé en argument n'est pas construit, et c'est
C     le travail de la fonction de le charger.
C
C Entrée:
C     HOBJ     Handle sur l'objet non construit
C     HXML     Handle sur le reader XML
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_UPK
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dslist.fc'

      INTEGER IERR
      INTEGER IV, N
      CHARACTER*(256) SVAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Initialise l'objet
      IF (LNEW) IERR = DS_LIST_RAZ(HOBJ)
      IERR = DS_LIST_INI(HOBJ)

C---     Les valeurs
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, N)
      DO IV=1,N
         IF (ERR_GOOD()) IERR = IO_XML_RS(HXML, SVAL)
         IF (ERR_GOOD()) IERR = DS_LIST_AJTVAL(HOBJ,
     &                                        SVAL(1:SP_STRN_LEN(SVAL)))
      ENDDO
D     CALL ERR_ASR(ERR_BAD() .OR. DS_LIST_REQDIM(HOBJ) .EQ. N)

      DS_LIST_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_CTR
CDEC$ ENDIF

      USE DS_LIST_M
      USE ISO_C_BINDING

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fc'

      INTEGER IERR, IRET
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   DS_LIST_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = DS_LIST_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. DS_LIST_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      DS_LIST_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_DTR
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = DS_LIST_RST(HOBJ)

C---     Dé-alloue
      SELF => DS_LIST_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, DS_LIST_HBASE)

      DS_LIST_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée DS_LIST_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_RAZ(HOBJ)

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fc'

      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_LIST_REQSELF(HOBJ)

      SELF%XLST = 0

      DS_LIST_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_INI
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER*8 XLST       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = DS_LIST_RST(HOBJ)

C---     Construis la liste
      XLST = 0
      IF (ERR_GOOD()) XLST = C_LST_CTR()

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF => DS_LIST_REQSELF(HOBJ)
         SELF%XLST = XLST
      ENDIF

      DS_LIST_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_RST
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dslist.fc'

      INTEGER IERR
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => DS_LIST_REQSELF(HOBJ)

C---     Détruis la liste
      IF (SELF%XLST .NE. 0) IERR = C_LST_DTR(SELF%XLST)

C---     Reset les attributs
      IF (ERR_GOOD()) IERR = DS_LIST_RAZ(HOBJ)

      DS_LIST_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DS_LIST_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_REQHBASE
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INCLUDE 'dslist.fi'
C------------------------------------------------------------------------

      DS_LIST_REQHBASE = DS_LIST_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DS_LIST_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_HVALIDE
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      DS_LIST_HVALIDE = OB_OBJN_HVALIDE(HOBJ,
     &                                  DS_LIST_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Imprime l'objet.
C
C Description:
C     La fonction DS_LIST_PRN imprime l'objet dans le log.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     Utilise les log par zones
C     Prototype
C************************************************************************
      FUNCTION DS_LIST_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER I, NVAL
      CHARACTER*256 BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     En-tête
      LOG_ZNE = 'h2d2.data.list'
      WRITE (LOG_BUF, '(A)') 'MSG_LISTE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Impression du handle
      IF (ERR_GOOD()) IERR = OB_OBJC_REQNOMCMPL(BUF, HOBJ)
      IF (ERR_GOOD()) THEN
         WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                        BUF(1:SP_STRN_LEN(BUF))
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

C---     Les valeurs
      NVAL = 0
      IF (ERR_GOOD()) THEN
         NVAL = DS_LIST_REQDIM(HOBJ)
         WRITE (LOG_BUF,'(A,I12)') 'MSG_NOMBRE_VALEURS#<35>#= ', NVAL
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HOBJ, 1, BUF)
      IF (ERR_GOOD()) THEN
         CALL SP_STRN_CLP(BUF, 80)
         WRITE (LOG_BUF,'(A,A)') 'MSG_VALEURS#<35>#= ',
     &                           BUF(1:SP_STRN_LEN(BUF))
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF
      DO I=2,NVAL
         IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HOBJ, I, BUF)
         IF (ERR_GOOD()) THEN
            CALL SP_STRN_CLP(BUF, 80)
            WRITE (LOG_BUF,'(A,A)') '#<35>#   ', BUF(1:SP_STRN_LEN(BUF))
            CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         ENDIF
      ENDDO

      CALL LOG_DECIND()

      DS_LIST_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_AJTVAL
C
C Description:
C     La fonction DS_LIST_AJTVAL ajoute une valeur à la liste
C     maintenue par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAL         La valeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_AJTVAL(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_AJTVAL
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAL

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'

      INTEGER*8 XLST       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_LIST_REQSELF(HOBJ)
      XLST = SELF%XLST
D     CALL ERR_ASR(XLST .NE. 0)

C---     Assigne les données
      IERR = C_LST_AJTVAL(XLST, VAL)
      IF (IERR .NE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_LISTE_AJOUT_VALEUR'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_LIST_AJTVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_ASGVAL
C
C Description:
C     La fonction DS_LIST_ASGVAL assigne une valeur à un indice
C     de la liste maintenue par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IND         Indice demandé
C     VAL         La valeur de la clef
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_ASGVAL(HOBJ, IND, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_ASGVAL
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IND
      CHARACTER*(*) VAL

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'

      INTEGER*8 XLST       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_LIST_REQSELF(HOBJ)
      XLST = SELF%XLST
D     CALL ERR_ASR(XLST .NE. 0)

C---     Assigne les données
      IERR = C_LST_ASGVAL(XLST, IND, VAL)
      IF (IERR .NE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6)') 'ERR_INDICE_INVALIDE', ': ', IND
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_LIST_ASGVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_CLR
C
C Description:
C     La fonction DS_LIST_CLR vide la liste.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_CLR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_CLR
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'

      INTEGER*8 XLST       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_LIST_REQSELF(HOBJ)
      XLST = SELF%XLST
D     CALL ERR_ASR(XLST .NE. 0)

C---     Vide la liste
      IERR = C_LST_CLR(XLST)
      IF (IERR .NE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_LISTE_VIDAGE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_LIST_CLR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_EFFVAL
C
C Description:
C     La fonction DS_LIST_EFFVAL efface un indice de la liste
C     maintenue par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IND         Indice
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_EFFVAL(HOBJ, IND)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_EFFVAL
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IND

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'

      INTEGER*8 XLST       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_LIST_REQSELF(HOBJ)
      XLST = SELF%XLST
D     CALL ERR_ASR(XLST .NE. 0)

C---     Efface la valeur
      IERR = C_LST_EFFVAL(XLST, IND)
      IF (IERR .NE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I3,A,I3)') 'ERR_LISTE_INDICE_INVALIDE', ' : ',
     &                               IND, '/', C_LST_REQDIM(XLST)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_LIST_EFFVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_REQDIM
C
C Description:
C     La fonction DS_LIST_REQDIM retourne la dimension de la liste
C     maintenue par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_REQDIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_REQDIM
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'

      INTEGER*8 XLST       ! handle eXterne
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_LIST_REQSELF(HOBJ)
      XLST = SELF%XLST

C---     Retourne la dimension
      IF (XLST .NE. 0) THEN
         DS_LIST_REQDIM = C_LST_REQDIM(XLST)
      ELSE
         DS_LIST_REQDIM = 0
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_REQVAL
C
C Description:
C     La fonction DS_LIST_REQVAL retourne la valeur à un indice de la
C     liste maintenue par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IND         Indice demandé
C
C Sortie:
C     VAL         La valeur
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_REQVAL(HOBJ, IND, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_REQVAL
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       IND
      CHARACTER*(*) VAL

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'

      INTEGER*8 XLST       ! handle eXterne
      INTEGER   IRET
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
!!      IERR = DS_LIST_REQSELF(HOBJ, SELF)
      SELF => DS_LIST_REQSELF(HOBJ)
      XLST = SELF%XLST
D     CALL ERR_ASR(XLST .NE. 0)

C---     Demande la valeur à l'indice
      IRET = C_LST_REQVAL(XLST, IND, VAL)
      IF (IRET .EQ. -1) GOTO 9900
      IF (IRET .EQ. -2) GOTO 9901
      IF (IRET .NE.  0) GOTO 9902

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I3,A,I3)') 'ERR_LISTE_INDICE_INVALIDE', ' : ',
     &                               IND, '/', C_LST_REQDIM(XLST)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I3)') 'ERR_DEBORDEMENT_VARIABLE', ' : ', IND
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I3)') 'ERR_RECHERCHE_LISTE', ' : ', IND
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_LIST_REQVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_TRVVAL
C
C Description:
C     La fonction DS_LIST_TRVVAL retourne l'indice de la valeur dans la
C     liste maintenue par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAL         La valeur recherchée
C
C Sortie:
C     IND         Indice
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_TRVVAL(HOBJ, IND, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_TRVVAL
CDEC$ ENDIF

      USE DS_LIST_M

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       IND
      CHARACTER*(*) VAL

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'spstrn.fi'

      INTEGER*8 XLST       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_LIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
!!      IERR = DS_LIST_REQSELF(HOBJ, SELF)
      SELF => DS_LIST_REQSELF(HOBJ)
      XLST = SELF%XLST
D     CALL ERR_ASR(XLST .NE. 0)

C---     DEMANDE LA VALEUR À L'INDICE
      IERR = C_LST_TRVVAL(XLST, IND, VAL)
      IF (IERR .NE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_LISTE_VALEUR_ABSENTE', ' : ',
     &                        VAL(1:SP_STRN_LEN(VAL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_LIST_TRVVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_POP
C
C Description:
C     La fonction DS_LIST_POP retire de la liste utilisée comme une pile la
C     valeur en fin de liste donc sur le dessus la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_POP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_POP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IND  = DS_LIST_REQDIM(HOBJ)
      IERR = DS_LIST_EFFVAL(HOBJ, IND)

      DS_LIST_POP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_PUSH
C
C Description:
C     La fonction DS_LIST_PUSH ajoute à la liste utilisée comme une pile la
C     valeur en fin de liste donc sur le dessus la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAL         La valeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_PUSH(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_PUSH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAL

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'

C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      DS_LIST_PUSH = DS_LIST_AJTVAL(HOBJ, VAL)
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_PUSHI
C
C Description:
C     La fonction DS_LIST_PUSHI ajoute à la liste utilisée comme une pile la
C     valeur INTEGER en fin de liste donc sur le dessus la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAL         La valeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_PUSHI(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_PUSHI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER VAL

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'

      CHARACTER*(32) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      WRITE(BUF, *) VAL
      DS_LIST_PUSHI = DS_LIST_AJTVAL(HOBJ, BUF)
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_TOP
C
C Description:
C     La fonction DS_LIST_TOP retourne de la liste utilisée comme une pile la
C     valeur en fin de liste donc sur le dessus la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAL         La valeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_TOP(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_TOP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAL

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'

      INTEGER IND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IND = DS_LIST_REQDIM(HOBJ)

      DS_LIST_TOP = DS_LIST_REQVAL(HOBJ, IND, VAL)
      RETURN
      END

C************************************************************************
C Sommaire: DS_LIST_TOPI
C
C Description:
C     La fonction DS_LIST_TOPI retourne de la liste utilisée comme une pile la
C     valeur INTEGER en fin de liste donc sur le dessus la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAL         La valeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LIST_TOPI(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LIST_TOPI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER VAL

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IND
      CHARACTER*(32) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IND = DS_LIST_REQDIM(HOBJ)

      IERR = DS_LIST_REQVAL(HOBJ, IND, BUF)
      IF (ERR_GOOD()) READ(BUF, *) VAL

      DS_LIST_TOPI = ERR_TYP()
      RETURN
      END
