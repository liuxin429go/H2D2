C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sommaire: Table virtuelle
C
C Description:
C     la classe SO_VTBL représente une table virtuelle qui fait le map
C     entre des identifiants sous forme d'INTEGER et des pointeurs de
C     fonction SO_FUNC_M.
C
C Class: SO_VTBL
C     INTEGER SO_VTBL_000
C     INTEGER SO_VTBL_999
C     INTEGER SO_VTBL_CTR
C     INTEGER SO_VTBL_DTR
C     INTEGER SO_VTBL_INI
C     INTEGER SO_VTBL_RST
C     LOGICAL SO_VTBL_HVALIDE
C     INTEGER SO_VTBL_AJTHFNC
C     INTEGER SO_VTBL_AJTHMTH
C     INTEGER SO_VTBL_AJTFNC
C     INTEGER SO_VTBL_AJTFSO
C     INTEGER SO_VTBL_AJTMTH
C     INTEGER SO_VTBL_AJTMSO
C     INTEGER SO_VTBL_REQFNC
C     INTEGER SO_VTBL_REQMTH
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction SO_VTBL_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sovtbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SO_VTBL_NOBJMAX,
     &                   SO_VTBL_HBASE,
     &                   'Virtual Table (table of functions)')

      SO_VTBL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction SO_VTBL_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sovtbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER  IERR
      EXTERNAL SO_VTBL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SO_VTBL_NOBJMAX,
     &                   SO_VTBL_HBASE,
     &                   SO_VTBL_DTR)

      SO_VTBL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction SO_VTBL_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sovtbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SO_VTBL_NOBJMAX,
     &                   SO_VTBL_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SO_VTBL_HVALIDE(HOBJ))
         IOB = HOBJ - SO_VTBL_HBASE

         SO_VTBL_HMAP(IOB) = 0
      ENDIF

      SO_VTBL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction SO_VTBL_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sovtbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SO_VTBL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SO_VTBL_NOBJMAX,
     &                   SO_VTBL_HBASE)

      SO_VTBL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet.
C
C Description:
C     La fonction SO_VTBL_INI initialise l'objet. Elle construit le map
C     qui va contenir les fonctions.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sovtbl.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMAP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SO_VTBL_RST(HOBJ)

C---     Construit le map
      HMAP = 0
      IF (ERR_GOOD()) IERR = DS_MAP_CTR(HMAP)
      IF (ERR_GOOD()) IERR = DS_MAP_INI(HMAP)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - SO_VTBL_HBASE
         SO_VTBL_HMAP(IOB) = HMAP
      ENDIF

      SO_VTBL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sovtbl.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IKEY, NKEY
      INTEGER HMAP, HFNC
      CHARACTER*(16) MKEY, MVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère l'indice
      IOB  = HOBJ - SO_VTBL_HBASE
      HMAP = SO_VTBL_HMAP(IOB)

C---     Vide ==> sort
      IF (.NOT. DS_MAP_HVALIDE(HMAP)) GOTO 9000

C---     Vide le map
      NKEY = DS_MAP_REQDIM(HMAP)
      DO IKEY=1,NKEY
         IF (ERR_GOOD()) IERR = DS_MAP_REQCLF(HMAP, IKEY, MKEY)
         IF (ERR_GOOD()) IERR = DS_MAP_REQVAL(HMAP,
     &                                        MKEY(1:SP_STRN_LEN(MKEY)),
     &                                        MVAL)
         IF (ERR_GOOD()) THEN
            READ(MVAL, *) HFNC
            IF (SO_FUNC_HVALIDE(HFNC)) IERR = SO_FUNC_DTR(HFNC)
         ENDIF
      ENDDO

C---     Détruis le map
      IF (DS_MAP_HVALIDE(HMAP)) IERR = DS_MAP_DTR(HMAP)

C---     Reset les paramètres
9000  CONTINUE
      SO_VTBL_HMAP(IOB) = 0

      SO_VTBL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SO_VTBL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sovtbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sovtbl.fc'
C------------------------------------------------------------------------

      SO_VTBL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  SO_VTBL_NOBJMAX,
     &                                  SO_VTBL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une fonction.
C
C Description:
C     La fonction SO_VTBL_AJTHFNC ajoute à l'objet un objet de type SO_FUNC_M.
C     On prend le contrôle de cet objet qui sera détruis à la destruction
C     de la table.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IDFNC       Identificateur de la fonction
C     HFNC        Handle sur un SO_FUNC_M
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_AJTHFNC(HOBJ, IDFNC, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_AJTHFNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IDFNC
      INTEGER HFNC

      INCLUDE 'sovtbl.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HDUM
      INTEGER HMAP
      CHARACTER*(16) MKEY, MVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
D     CALL ERR_PRE(SO_VTBL_HMAP(HOBJ-SO_VTBL_HBASE) .NE. 0)
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HFNC))
C------------------------------------------------------------------------

C---     Détruis la fonction avec le même id
      IERR = SO_VTBL_REQFNC(HOBJ, IDFNC, HDUM)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SO_FUNC_HVALIDE(HDUM))
         IERR = SO_FUNC_DTR(HDUM)
      ELSE
         CALL ERR_RESET()
      ENDIF

C---     Ajoute la fonction au map
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - SO_VTBL_HBASE
         HMAP = SO_VTBL_HMAP(IOB)
         WRITE(MKEY, '(I12,I2)') IDFNC, SO_VTBL_TYP_FNC
         WRITE(MVAL, '(I12)') HFNC
         CALL SP_STRN_TRM(MKEY)
         CALL SP_STRN_TRM(MVAL)
         IERR = DS_MAP_ASGVAL(HMAP,
     &                        MKEY(1:SP_STRN_LEN(MKEY)),
     &                        MVAL(1:SP_STRN_LEN(MVAL)))
      ENDIF

      SO_VTBL_AJTHFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une méthode.
C
C Description:
C     La fonction SO_VTBL_AJTHMTH ajoute à l'objet un objet de type SO_FUNC_M.
C     On prend le contrôle de cet objet qui sera détruis à la destruction
C     de la table.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IDFNC       Identificateur de la fonction
C     HFNC        Handle sur un SO_FUNC_M
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_AJTHMTH(HOBJ, IDMTH, HMTH)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_AJTHMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IDMTH
      INTEGER HMTH

      INCLUDE 'sovtbl.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HDUM
      INTEGER HMAP
      CHARACTER*(16) MKEY, MVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
D     CALL ERR_PRE(SO_VTBL_HMAP(HOBJ-SO_VTBL_HBASE) .NE. 0)
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HMTH))
C------------------------------------------------------------------------

C---     Détruis la méthode avec le même id
      IERR = SO_VTBL_REQMTH(HOBJ, IDMTH, HDUM)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SO_FUNC_HVALIDE(HDUM))
         IERR = SO_FUNC_DTR(HDUM)
      ELSE
         CALL ERR_RESET()
      ENDIF

C---     Ajoute la méthode au map
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - SO_VTBL_HBASE
         HMAP = SO_VTBL_HMAP(IOB)
         WRITE(MKEY, '(I12,I2)') IDMTH, SO_VTBL_TYP_MTH
         WRITE(MVAL, '(I12)') HMTH
         CALL SP_STRN_TRM(MKEY)
         CALL SP_STRN_TRM(MVAL)
         IERR = DS_MAP_ASGVAL(HMAP,
     &                        MKEY(1:SP_STRN_LEN(MKEY)),
     &                        MVAL(1:SP_STRN_LEN(MVAL)))
      ENDIF

      SO_VTBL_AJTHMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une fonction.
C
C Description:
C     La fonction SO_VTBL_AJTFNC ajoute à l'objet une fonction régulière
C     sous forme d'un pointeur à une fonction.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IDFNC       Identificateur de la fonction
C     F           Fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_AJTFNC(HOBJ, IDFNC, F)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_AJTFNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IDFNC
      INTEGER F
      EXTERNAL F

      INCLUDE 'sovtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER HFNC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR    (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC (HFNC, F)
      IF (ERR_GOOD()) IERR = SO_VTBL_AJTHFNC(HOBJ, IDFNC, HFNC)

      SO_VTBL_AJTFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une fonction de DLL.
C
C Description:
C     La fonction SO_VTBL_AJTFSO ajoute à l'objet une fonction sous forme
C     d'une fonction de DLL (Shared Object).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IDFNC       Identificateur de la fonction
C     FDLL        Nom du fichier DLL
C     FFNC        Nom de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_AJTFSO(HOBJ, IDFNC, FDLL, FFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_AJTFSO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       IDFNC
      CHARACTER*(*) FDLL
      CHARACTER*(*) FFNC

      INCLUDE 'sovtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER HFNC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR    (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFSO (HFNC, FDLL, FFNC)
      IF (ERR_GOOD()) IERR = SO_VTBL_AJTHFNC(HOBJ, IDFNC, HFNC)

      SO_VTBL_AJTFSO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une méthode.
C
C Description:
C     La fonction SO_VTBL_AJTMTH ajoute à l'objet une méthode régulière
C     sous forme d'un pointeur à une fonction.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IFMTH       Identificateur de la méthode
C     HSLF        Handle de l'objet, paramètre de la méthode
C     F           Fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_AJTMTH(HOBJ, IFMTH, HSLF, F)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_AJTMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IFMTH
      INTEGER HSLF
      INTEGER F
      EXTERNAL F

      INCLUDE 'sovtbl.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER   IERR
      INTEGER   IOB
      INTEGER   HMTH
      INTEGER*8 XMAP
      CHARACTER*(16) MKEY, MVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HMTH = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR    (HMTH)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH (HMTH, HSLF, F)
      IF (ERR_GOOD()) IERR = SO_VTBL_AJTHMTH(HOBJ, IFMTH, HMTH)

      SO_VTBL_AJTMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une méthode de DLL.
C
C Description:
C     La fonction SO_VTBL_AJTMSO ajoute à l'objet une méthode sous forme
C     d'une fonction de DLL (Shared Object).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IFMTH       Identificateur de la méthode
C     HSLF        Handle de l'objet, paramètre de la méthode
C     FDLL        Nom du fichier DLL
C     FFNC        Nom de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_AJTMSO(HOBJ, IFMTH, HSLF, FDLL, FFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_AJTMSO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       IFMTH
      INTEGER       HSLF
      CHARACTER*(*) FDLL
      CHARACTER*(*) FFNC

      INCLUDE 'sovtbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER HMTH
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HMTH = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HMTH)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMSO(HMTH, HSLF, FDLL, FFNC)
      IF (ERR_GOOD()) IERR = SO_VTBL_AJTHMTH(HOBJ, IFMTH, HMTH)

      SO_VTBL_AJTMSO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne une fonction.
C
C Description:
C     La fonction SO_VTBL_REQFNC retourne l'objet de type SO_FUNC_M
C     correspondant à l'identificateur IDFNC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IDFNC       Identificateur de la fonction
C
C Sortie:
C     HFNC        Handle sur un SO_FUNC_M
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_REQFNC(HOBJ, IDFNC, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_REQFNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IDFNC
      INTEGER HFNC

      INCLUDE 'sovtbl.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMAP
      CHARACTER*(16) MKEY, MVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
D     CALL ERR_PRE(SO_VTBL_HMAP(HOBJ-SO_VTBL_HBASE) .NE. 0)
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - SO_VTBL_HBASE
      HMAP = SO_VTBL_HMAP(IOB)

C---     L'ID en clef
      WRITE(MKEY, '(I12,I2)') IDFNC, SO_VTBL_TYP_FNC
      CALL SP_STRN_TRM(MKEY)

C---     Récupère la fonction via le map
      IERR = DS_MAP_REQVAL(HMAP,
     &                     MKEY(1:SP_STRN_LEN(MKEY)),
     &                     MVAL)
      IF (ERR_BAD()) GOTO 9900

C---     Décode
      READ(MVAL, *) HFNC

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_ID_FNCT_INVALIDE', ': ', IDFNC
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_VTBL_REQFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne une fonction.
C
C Description:
C     La fonction SO_VTBL_REQMTH retourne l'objet de type SO_FUNC_M
C     correspondant à l'identificateur IDFNC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IDFNC       Identificateur de la fonction
C
C Sortie:
C     HFNC        Handle sur un SO_FUNC_M
C
C Notes:
C************************************************************************
      FUNCTION SO_VTBL_REQMTH(HOBJ, IDFNC, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_VTBL_REQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IDFNC
      INTEGER HFNC

      INCLUDE 'sovtbl.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sovtbl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMAP
      CHARACTER*(16) MKEY, MVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HOBJ))
D     CALL ERR_PRE(SO_VTBL_HMAP(HOBJ-SO_VTBL_HBASE) .NE. 0)
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - SO_VTBL_HBASE
      HMAP = SO_VTBL_HMAP(IOB)

C---     L'ID en clef
      WRITE(MKEY, '(I12,I2)') IDFNC, SO_VTBL_TYP_MTH
      CALL SP_STRN_TRM(MKEY)

C---     Récupère la fonction via le map
      IERR = DS_MAP_REQVAL(HMAP,
     &                     MKEY(1:SP_STRN_LEN(MKEY)),
     &                     MVAL)
      IF (ERR_BAD()) GOTO 9900

C---     Décode
      READ(MVAL, *) HFNC

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_ID_FNCT_INVALIDE', ': ', IDFNC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_VTBL_REQMTH = ERR_TYP()
      RETURN
      END
