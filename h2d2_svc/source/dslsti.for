C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C      FUNCTION DS_LSTI_000 ()
C      FUNCTION DS_LSTI_999 ()
C      FUNCTION DS_LSTI_CTR ()
C      FUNCTION DS_LSTI_DTR ()
C      FUNCTION DS_LSTI_INI ()
C      FUNCTION DS_LSTI_RST ()
C
C      FUNCTION DS_LSTI_CHARGE  ()
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LSTI_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LSTI_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dslsti.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslsti.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(DS_LSTI_NOBJMAX,
     &                   DS_LSTI_HBASE,
     &                   'List data iterator')

      DS_LSTI_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LSTI_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LSTI_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dslsti.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslsti.fc'

      INTEGER  IERR
      EXTERNAL DS_LSTI_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(DS_LSTI_NOBJMAX,
     &                   DS_LSTI_HBASE,
     &                   DS_LSTI_DTR)

      DS_LSTI_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LSTI_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LSTI_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslsti.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslsti.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   DS_LSTI_NOBJMAX,
     &                   DS_LSTI_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(DS_LSTI_HVALIDE(HOBJ))
         IOB = HOBJ - DS_LSTI_HBASE

         DS_LSTI_HLST(IOB) = 0
         DS_LSTI_ICUR(IOB) = 0
      ENDIF

      DS_LSTI_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LSTI_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LSTI_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslsti.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslsti.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DS_LSTI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = DS_LSTI_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   DS_LSTI_NOBJMAX,
     &                   DS_LSTI_HBASE)

      DS_LSTI_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LSTI_INI(HOBJ, HLST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LSTI_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLST

      INCLUDE 'dslsti.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dslsti.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LSTI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C--      CONTROLES DES PARAMETRES
      IF (.NOT. DS_LIST_HVALIDE(HLST)) GOTO 9900

C---     RESET LES DONNEES
      IERR = DS_LSTI_RST(HOBJ)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DS_LSTI_HBASE
         DS_LSTI_HLST(IOB) = HLST
         DS_LSTI_ICUR(IOB) = 0
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_LISTE_INVALIDE',': ', HLST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_LSTI_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LSTI_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LSTI_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslsti.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'dslsti.fc'

      INTEGER   IERR
      INTEGER   IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LSTI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DS_LSTI_HBASE

C---     RESET LES PARAMETRES
      DS_LSTI_HLST(IOB) = 0

      DS_LSTI_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DS_LSTI_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LSTI_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LSTI_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dslsti.fi'
      INCLUDE 'dslsti.fc'
C------------------------------------------------------------------------

      DS_LSTI_REQHBASE = DS_LSTI_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DS_LSTI_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_LSTI_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LSTI_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dslsti.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dslsti.fc'
C------------------------------------------------------------------------

      DS_LSTI_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  DS_LSTI_NOBJMAX,
     &                                  DS_LSTI_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: DS_LSTI_REQNXT
C
C Description:
C     La fonction DS_LSTI_REQNXT retourne dans VAL la prochaine valeur.
C     En fin de liste, retourne l'erreur ERR_END_ITER.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     VAL         La prochaine valeur
C
C Notes:
C************************************************************************
      FUNCTION DS_LSTI_REQNXT(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_LSTI_REQNXT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAL

      INCLUDE 'dslsti.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dslsti.fc'

      INTEGER IOB
      INTEGER HLST, ICUR
      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LSTI_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DS_LSTI_HBASE

C---     RECUPERE LES ATTRIBUTS
      HLST = DS_LSTI_HLST(IOB)
      ICUR = DS_LSTI_ICUR(IOB)
D     CALL ERR_ASR(HLST .NE. 0)

C---     Demande la valeur à la liste
      IF (ICUR .LT. DS_LIST_REQDIM(HLST)) THEN
         ICUR = ICUR + 1
         IERR = DS_LIST_REQVAL(HLST, ICUR, VAL)
         DS_LSTI_ICUR(IOB) = ICUR
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_END_ITER')
      ENDIF

      DS_LSTI_REQNXT = ERR_TYP()
      RETURN
      END
