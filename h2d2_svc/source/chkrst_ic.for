C************************************************************************
C --- Copyright (c) INRS 2012-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_PKL_XEQCTR
C     INTEGER IC_PKL_XEQMTH
C     CHARACTER*(32) IC_PKL_REQCLS
C     INTEGER IC_PKL_REQHDL
C   Private:
C     SUBROUTINE IC_PKL_AID
C
C************************************************************************

C pkl
C   démarre une tâche avec un timer: au bout du temps, lève un flag
C   les algos appellent pkl après chaque iter: si le flag est levé, chkp
C
C modes:
C   chkp à interval
C   chkp et stop
C   restart

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PKL_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PKL_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'chkrst_ic.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PKL_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     CONTROLE LES PARAM
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
         WRITE(IPRM, '(2A,I12)') 'H', ',', IC_PKL_REQHDL()
      ENDIF

C  <comment>
C  The constructor <b>checkpoint_restart</b> constructs an object and returns a handle on this object.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PKL_AID()

9999  CONTINUE
      IC_PKL_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PKL_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PKL_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'chkrst_ic.fi'
      INCLUDE 'chkrst.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER       IERR
      INTEGER       IVAL
      REAL*8        RVAL
      CHARACTER*256 SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_PKL_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>init_checkpoint_loop</b> </comment>
      IF (IMTH .EQ. 'init_checkpoint_loop') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Time interval between two checkpoints</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, RVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = PKL_INICHCKP(RVAL)

C     <comment>The method <b>checkpoint</b> will initiate a checkpoint if the time trigger has been enabled.</comment>
      ELSEIF (IMTH .EQ. 'checkpoint') THEN
         IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900
         IERR = PKL_DOCHCKP()

C     <comment>The method <b>reset_checkpoint_loop</b> stop the checkpoint loop and reinitializes the service.</comment>
      ELSEIF (IMTH .EQ. 'reset_checkpoint_loop') THEN
         IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900
         IERR = PKL_RSTCHCKP()

C     <comment>The method <b>restart</b> .</comment>
      ELSEIF (IMTH .EQ. 'restart') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Path to the restart data</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = PKL_DORSTRT(SVAL(1:SP_STRN_LEN(SVAL)))

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = LOG_STS(HOBJ)
         CALL LOG_ECRIS('<!-- Test LOG_STS(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PKL_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PKL_AID()

9999  CONTINUE
      IC_PKL_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PKL_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PKL_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'chkrst_ic.fi'
      INCLUDE 'log.fi'
C-------------------------------------------------------------------------

C<comment>
C The class <b>checkpoint_restart</b>
C</comment>
      IC_PKL_REQCLS = 'checkpoint_restart'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PKL_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PKL_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'chkrst_ic.fi'
C-------------------------------------------------------------------------

      IC_PKL_REQHDL = 1999091000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PKL_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('chkrst_ic.hlp')

      RETURN
      END
