C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     SUBROUTINE LOG_DBG
C     INTEGER LOG_REQNIVZ
C     SUBROUTINE LOG_ERR
C     INTEGER LOG_AJTZONE
C     INTEGER LOG_ASGNIV
C     SUBROUTINE LOG_TODO
C     SUBROUTINE LOG_INFO
C     SUBROUTINE LOG_DEBUG
C     SUBROUTINE LOG_TRSE
C     SUBROUTINE LOG_LOG
C     INTEGER LOG_ASGFIC
C     INTEGER LOG_ASGNIVZ
C     SUBROUTINE LOG_DECIND
C     SUBROUTINE LOG_MSG_ERR
C     INTEGER LOG_REQNIV
C     SUBROUTINE LOG_WRN
C     LOGICAL LOG_ESTZONEACTIVE
C     SUBROUTINE LOG_VRBS
C     INTEGER LOG_EFFZONE
C     SUBROUTINE LOG_ECRIS
C     SUBROUTINE LOG_INCIND
C     INTEGER LOG_REQMP
C     INTEGER LOG_ECRISFIC
C   Private:
C     INTEGER SP_STRN_RINDEX
C
C************************************************************************

C************************************************************************
C Sommaire: Assigne le fichier de log.
C
C Description:
C     La fonction LOG_ASGFIC assigne un nouveau fichier au système de log.
C     Le nom spécial 'STDOUT' reconnecte à l'unité 6.
C     <p>
C     En multi-process, si le niveau de log est > 3, alors on crée un fichier
C     par process. Si le niveau est <= 3, on ne crée qu'un seul fichier et
C     c'est uniquement le process maître qui écrira dans le log.
C
C Entrée:
C     CHARACTER*(*) FLOG     Nom du fichier de log
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LOG_ASGFIC(FLOG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_ASGFIC
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) FLOG

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'log.fc'

      INTEGER     I_RANK, I_ERROR
      INTEGER     I_MASTER
      LOGICAL     I_INIT
      PARAMETER (I_MASTER = 0)

      INTEGER       MP
      INTEGER       LFLOG
      CHARACTER*256 FNAME
C-----------------------------------------------------------------------

      LFLOG = SP_STRN_LEN(FLOG)

C---     DETERMINE SI ON DOIT OUVRIR ET QUEL EST LE NOM
      CALL MPI_INITIALIZED(I_INIT, I_ERROR)
      IF (I_INIT) THEN
         CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

         IF (LOG_REQNIV() .LE. LOG_LVL_DEBUG) THEN
            IF (I_RANK .EQ. I_MASTER) THEN
               IF (LFLOG .EQ. 6 .AND. FLOG(1:LFLOG) .EQ. 'STDOUT') THEN
                  MP = 6
               ELSE
                  MP = IO_UTIL_FREEUNIT()
                  FNAME = FLOG(1:LFLOG)
               ENDIF
            ELSE
               MP = -1
            ENDIF
         ELSE
            IF (LFLOG .EQ. 6 .AND. FLOG(1:LFLOG) .EQ. 'STDOUT') THEN
               MP = 6
            ELSE
               MP = IO_UTIL_FREEUNIT()
               WRITE(FNAME,'(2A,I0.3,A)') FLOG(1:LFLOG),'[',I_RANK,']'
            ENDIF
         ENDIF

      ELSE
         IF (LFLOG .EQ. 6 .AND. FLOG(1:LFLOG) .EQ. 'STDOUT') THEN
            MP = 6
         ELSE
            MP = IO_UTIL_FREEUNIT()
            FNAME = FLOG(1:LFLOG)
         ENDIF
      ENDIF

C---     OUVRE LE NOUVEAU FICHIER
      IF (MP .NE. 6 .AND. MP .GE. 0) THEN
         OPEN(UNIT  = MP,
     &        FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &        FORM  = 'FORMATTED',
     &        STATUS= 'UNKNOWN',
     &        ERR   = 198)
         GOTO 199
198      CONTINUE
C---        En cas d'erreur, reconnecte à 6
         WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         CALL ERR_AJT(FNAME(1:SP_STRN_LEN(FNAME)))
         MP = 6
199      CONTINUE
      ENDIF

C---     FERME L'ANCIEN FICHIER
      IF (LOG_MP .NE. 6 .AND. MP .GE. 0) CLOSE(LOG_MP)

C---     ASSIGNE L'UNITÉ
      LOG_MP = MP

      LOG_ASGFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris le message
C
C Description:
C     La fonction <code>LOG_LOG(...)</code> écris le message dans le
C     fichier de log uniquement si la zone est active pour le niveau.
C
C Entrée:
C     CHARACTER*(*) ZONE   Le message à logger
C     CHARACTER*(*) MSG    Le message à logger
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_LOG(ZONE, ILVL, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_LOG
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      INTEGER       ILVL
      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
      INCLUDE 'log.fc'

      INTEGER IERR
      INTEGER ILVL_OLD
C-----------------------------------------------------------------------

      IF (LOG_ESTZONEACTIVE(ZONE, ILVL)) THEN
         ILVL_OLD = LOG_REQNIV()
         IERR = LOG_ASGNIV(ILVL)
         CALL LOG_ECRIS(MSG)
         IERR = LOG_ASGNIV(ILVL_OLD)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Écris le message si on est au niveau ERROR.
C
C Description:
C     La fonction LOG_ERR écris le message dans le fichier de
C     log uniquement si le système de log est en mode ERROR.
C
C Entrée:
C     CHARACTER*(*) ZONE   Le message à logger
C     CHARACTER*(*) MSG    Le message à logger
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_ERR(ZONE, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_ERR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_LOG(ZONE, LOG_LVL_ERROR, MSG)

      RETURN
      END

C************************************************************************
C Sommaire: Écris le message si on est au niveau WARNING.
C
C Description:
C     La fonction LOG_WRN écris le message dans le fichier de
C     log uniquement si le système de log est en mode WARNING.
C
C Entrée:
C     CHARACTER*(*) ZONE   Le message à logger
C     CHARACTER*(*) MSG    Le message à logger
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_WRN(ZONE, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_WRN
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_LOG(ZONE, LOG_LVL_WARNING, MSG)

      RETURN
      END

C************************************************************************
C Sommaire: Écris le message si on est au niveau TERSE
C
C Description:
C     La fonction LOG_TRSE écris le message dans le fichier de
C     log uniquement si le système de log est en mode LOG_LVL_TERSE.
C
C Entrée:
C     CHARACTER*(*) ZONE   Le message à logger
C     CHARACTER*(*) MSG    Le message à logger
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_TRSE(ZONE, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_TRSE
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_LOG(ZONE, LOG_LVL_TERSE, MSG)

      RETURN
      END

C************************************************************************
C Sommaire: Écris le message si on est au niveau INFO
C
C Description:
C     La fonction LOG_INFO écris le message dans le fichier de
C     log uniquement si le système de log est en mode LOG_LVL_INFO.
C
C Entrée:
C     CHARACTER*(*) ZONE   Le message à logger
C     CHARACTER*(*) MSG    Le message à logger
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_INFO(ZONE, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_INFO
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_LOG(ZONE, LOG_LVL_INFO, MSG)

      RETURN
      END

C************************************************************************
C Sommaire: Écris le message si on est au niveau VERBOSE
C
C Description:
C     La fonction LOG_VRBS écris le message dans le fichier de
C     log uniquement si le système de log est en mode LOG_VRBS.
C
C Entrée:
C     CHARACTER*(*) ZONE   Le message à logger
C     CHARACTER*(*) MSG    Le message à logger
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_VRBS(ZONE, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_VRBS
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_LOG(ZONE, LOG_LVL_VERBOSE, MSG)

      RETURN
      END

C************************************************************************
C Sommaire: Écris le message si on est au niveau DEBUG
C
C Description:
C     La fonction LOG_DBG écris le message dans le fichier de
C     log uniquement si le système de log est en mode DEBUG.
C
C Entrée:
C     CHARACTER*(*) ZONE   Le message à logger
C     CHARACTER*(*) MSG    Le message à logger
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_DBG(ZONE, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_DBG
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_LOG(ZONE, LOG_LVL_DEBUG, MSG)

      RETURN
      END

C************************************************************************
C Sommaire: Écris le message si on est en mode debug
C
C Description:
C     La fonction LOG_DEBUG écris le message dans le fichier de
C     log uniquement si le système de log est en mode DEBUG.
C
C Entrée:
C     CHARACTER*(*) MSG    Le message à logger
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_DEBUG(MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_DEBUG
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
      INCLUDE 'log.fc'
C-----------------------------------------------------------------------

      CALL LOG_DBG('h2d2', MSG)

      RETURN
      END

C************************************************************************
C Sommaire: Écris le message
C
C Description:
C     La fonction LOG_ECRIS écris le message passé en argument dans
C     le fichier de log. Le message est traduit et mis en page en
C     fonction du niveau d'indentation.
C
C Entrée:
C     CHARACTER*(*) MSG       Le message
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_ECRIS(MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_ECRIS
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'log.fc'

      CHARACTER*8 MPIBUF
      INTEGER     I_RANK, I_ERROR
      LOGICAL     I_INIT

      INTEGER NB
      CHARACTER*(1024) STMP
      CHARACTER*(24)   BLANC
      DATA BLANC /'                        '/
C-----------------------------------------------------------------------

      IF (LOG_MP .LE. 0) RETURN

!$omp critical(OMP_CS_LOG)
      MPIBUF = ' '
      IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
         CALL MPI_INITIALIZED(I_INIT, I_ERROR)
         IF (I_INIT) THEN
            CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
            WRITE(MPIBUF, '(A,I1,A)') '[', I_RANK, ']'
         ELSE
            MPIBUF = '[*]'
         ENDIF
      ENDIF

      IF (SP_STRN_LEN(MSG) .GT. 0) THEN
         STMP = MSG(1:SP_STRN_LEN(MSG))
         CALL TRD_TRADUIS(STMP)
         IF (SP_STRN_LEN(STMP) .GT. 0) THEN
            IF (LOG_INDENT .GT. 0) THEN
               NB = MIN(24, 3*LOG_INDENT)
               IF (SP_STRN_LEN(MPIBUF) .GT. 0) THEN
                  WRITE (LOG_MP,'(3A)') MPIBUF(1:SP_STRN_LEN(MPIBUF)),
     &                                  BLANC(1:NB),
     &                                  STMP(1:SP_STRN_LEN(STMP))
               ELSE
                  WRITE (LOG_MP,'(2A)') BLANC(1:NB),
     &                                  STMP(1:SP_STRN_LEN(STMP))
               ENDIF
            ELSE
               IF (SP_STRN_LEN(MPIBUF) .GT. 0) THEN
                  WRITE (LOG_MP,'(2A)') MPIBUF(1:SP_STRN_LEN(MPIBUF)),
     &                                  STMP(1:SP_STRN_LEN(STMP))
               ELSE
                  WRITE (LOG_MP,'(A)') STMP(1:SP_STRN_LEN(STMP))
               ENDIF
            ENDIF
         ELSE
            WRITE (LOG_MP, *)
         ENDIF
      ELSE
         WRITE (LOG_MP, *)
      ENDIF
!$omp end critical(OMP_CS_LOG)

      RETURN
      END

C************************************************************************
C Sommaire: Écris le contenu du fichier.
C
C Description:
C     La fonction LOG_ECRISFIC écris le contenu du fichier dont le
C     nom est passé en argument dans le fichier de log. On tente d'ouvrir
C     le fichier tel que passé et en cas d'erreur on fait une tentative
C     dans le répertoire de l'exécutable. En cas d'erreur on complète
C     le message déjà présent s'il existe.
C
C Entrée:
C     CHARACTER*(*) FNAME    Le nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LOG_ECRISFIC(FNAME)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_ECRISFIC
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) FNAME

      INCLUDE 'log.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'

      INTEGER MP
      INTEGER IERR
      CHARACTER*(256) LIGNE
C-----------------------------------------------------------------------

      MP = IO_UTIL_FREEUNIT()
C---     1.ER ESSAI AVEC LE NOM PASSE EN ARGUMENT
      OPEN(UNIT  = MP,
     &     FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &     FORM  = 'FORMATTED',
     &     STATUS= 'OLD',
     &     ERR   = 199)
      GOTO 200

C---     2.EME ESSAI AVEC LE NOM PASSE EN ARGUMENT
199   CONTINUE
      IERR = C_OS_REPEXE(LIGNE)
      LIGNE = LIGNE(1:SP_STRN_LEN(LIGNE)) //
     &        '/../doc/' //
     &        FNAME(1:SP_STRN_LEN(FNAME))
      OPEN(UNIT  = MP,
     &     FILE  = LIGNE(1:SP_STRN_LEN(LIGNE)),
     &     FORM  = 'FORMATTED',
     &     STATUS= 'OLD',
     &     ERR   = 9900)
      GOTO 200

C---     LECTURE DU FICHIER
200   CALL LOG_ECRIS(' ')
210   CONTINUE
         READ (MP, '(A)', ERR=9901, END=299) LIGNE
         CALL LOG_ECRIS(LIGNE)
      GOTO 210
299   CONTINUE

      CLOSE(MP)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      IF (ERR_GOOD()) THEN
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ELSE
         CALL ERR_AJT(ERR_BUF)
      ENDIF
      CALL ERR_AJT(FNAME(1:SP_STRN_LEN(FNAME)))
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_LECTURE_FICHIER'
      IF (ERR_GOOD()) THEN
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ELSE
         CALL ERR_AJT(ERR_BUF)
      ENDIF
      CALL ERR_AJT(FNAME(1:SP_STRN_LEN(FNAME)))
      GOTO 9999

9999  CONTINUE
      LOG_ECRISFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Log le message d'erreur
C
C Description:
C     La fonction LOG_MSG_ERR log le message d'erreur courant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_MSG_ERR()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_MSG_ERR
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'log.fc'

      INTEGER IE
      INTEGER NERR
C-----------------------------------------------------------------------

      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('****************************************')
      IF     (ERR_TYP() .EQ. ERR_WRN) THEN
         CALL LOG_ECRIS('MSG_AVERTISSEMENT' // ':')
      ELSEIF (ERR_TYP() .EQ. ERR_ERR) THEN
         CALL LOG_ECRIS('MSG_ERREUR' // ':')
      ELSEIF (ERR_TYP() .EQ. ERR_FTL) THEN
         CALL LOG_ECRIS('MSG_ERREUR_FATALE' // ':')
      ENDIF

      CALL LOG_INCIND()
      NERR = ERR_NBR()
      DO IE=1,NERR
         CALL LOG_ECRIS(ERR_MSG(IE))
      ENDDO
      CALL LOG_DECIND()
      CALL LOG_ECRIS('****************************************')

      RETURN
      END

C************************************************************************
C Sommaire: Écris un message TODO
C
C Description:
C     La fonction LOG_TODO écris le message dans le fichier de
C     log en le formatant pour le faire ressortir.
C
C Entrée:
C     CHARACTER*(*) MSG    Le message TODO
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_TODO(MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_TODO
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) MSG

      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER LBUF
C-----------------------------------------------------------------------

      LOG_BUF = MSG
      LBUF = SP_STRN_LEN(LOG_BUF)
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('--- TO DO --------------------------------------')
      IF (LBUF .GT. 0) THEN
         CALL LOG_ECRIS('   ' // LOG_BUF(1:LBUF))
      ELSE
         CALL LOG_ECRIS(' ')
      ENDIF
      CALL LOG_ECRIS('------------------------------------------------')
      CALL LOG_ECRIS(' ')

      RETURN
      END

C************************************************************************
C Sommaire: Décrémente l'indentation.
C
C Description:
C     La fonction LOG_INCIND décrémente le niveau d'indentation
C     dans le niveau de log.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_DECIND()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_DECIND
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fc'
C-----------------------------------------------------------------------

      IF (LOG_INDENT .GT. 0) LOG_INDENT = LOG_INDENT - 1

      RETURN
      END

C************************************************************************
C Sommaire: Incrémente l'indentation.
C
C Description:
C     La fonction LOG_INCIND incrémente le niveau d'indentation
C     dans le niveau de log.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE LOG_INCIND()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_INCIND
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fc'
C-----------------------------------------------------------------------

      LOG_INDENT = LOG_INDENT + 1

      RETURN
      END

C************************************************************************
C Sommaire: Assigne le niveau de log
C
C Description:
C     La fonction LOG_ASGNIV assigne le niveau de log
C
C Entrée:
C     INTEGER N;     Le niveau demandé
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LOG_ASGNIV(N)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_ASGNIV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER N

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fc'
C-----------------------------------------------------------------------

      IF (N .LT. LOG_LVL_MIN) GOTO 9900
      IF (N .GT. LOG_LVL_MAX) GOTO 9900

      LOG_NIVEAU = N

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NIVEAU_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_NIVEAU_MIN', ': ', 'error'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_NIVEAU_MAX', ': ', 'debug'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LOG_ASGNIV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le niveau de log
C
C Description:
C     La fonction LOG_REQNIV retourne le niveau de log.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LOG_REQNIV()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_REQNIV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'log.fc'
C-----------------------------------------------------------------------

      LOG_REQNIV = LOG_NIVEAU
      RETURN
      END

C************************************************************************
C Sommaire: Initialise le COMMON
C
C Description:
C     Le block data LOG initialise le COMMON propre au module de log.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA LOG

      IMPLICIT NONE

      INCLUDE 'log.fc'

      DATA LOG_ZONES    /'@'/
      DATA LOG_MP       /6/
      DATA LOG_INDENT   /0/
      DATA LOG_NIVEAU   /0/

      END

C************************************************************************
C Sommaire: Ajoute une zone de log.
C
C Description:
C     La fonctions <code>LOG_AJTZONE(...)</code> ajoute la zone
C     <code>ZONE</code> aux zones pour lesquelles le log est actif. Le
C     niveau associé à la zone est donné par ILVL.
C     Les messages destinés à cette zone et dont le niveau de log est suffisant
C     seront donc affichés.
C     <p>
C     Les zones sont organisées de manière hiérarchiques. Par exemple,
C     'a' est le parent de 'a.b', lui même parent de 'a.b.c1' et 'a.b.c2'.
C     L'activation d'un parent active également ses enfants à moins que
C     ceux-ci aient été activés séparément.
C
C Entrée:
C     CHARACTER*(*) ZONE      Nom hiérarchique de la zone
C     INTEGER       ILVL      Niveau de log de la zone
C
C Sortie:
C
C Notes:
C     Les zone sont encodées sous la forme @nom-de-la-zone@niv@
C     Le niveau est écris sur 3 positions.
C************************************************************************
      FUNCTION LOG_AJTZONE(ZONE, ILVL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_AJTZONE
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      INTEGER       ILVL

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'log.fc'

      INTEGER IDX, IERR
      INTEGER LZ, LZS
      CHARACTER*(8) BUF
C-----------------------------------------------------------------------

      LZ = SP_STRN_LEN(ZONE)
      IF (LZ .GT. 0) THEN

         IDX = INDEX(LOG_ZONES, ZONE(1:LZ))
         IF (IDX .GT. 0) IERR = LOG_EFFZONE(ZONE)

         LZS = SP_STRN_LEN(LOG_ZONES)
         IF (LEN(LOG_ZONES) .LT. (LZS+LZ+1)) GOTO 9900
         BUF = ' '
         WRITE(BUF, '(I3)') MAX(LOG_LVL_INFO,MIN(LOG_LVL_DEBUG,ILVL))
         LOG_ZONES = LOG_ZONES(1:LZS) // ZONE(1:LZ) // '@' //
     &               BUF(1:3) // '@'
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DEBORDEMENT_BUFFER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LOG_AJTZONE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Change le niveau de log d'une zone
C
C Description:
C     La fonction <code>LOG_ASGNIVZ(...)</code> change le niveau de log
C     de la zone <code>ZONE</zone>. Si la zone n'existe pas elle est
C     ajoutée.
C
C Entrée:
C     CHARACTER*(*) ZONE      Nom hiérarchique de la zone
C     INTEGER       ILVL      Niveau de log de la zone
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LOG_ASGNIVZ(ZONE, ILVL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_ASGNIVZ
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      INTEGER       ILVL

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = LOG_EFFZONE(ZONE)
      IF (ERR_GOOD()) IERR = LOG_AJTZONE(ZONE, ILVL)

      LOG_ASGNIVZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Supprime une zone de log
C
C Description:
C     La fonctions <code>LOG_EFFZONE()</code> supprime la zone ZONE du
C     système de log. Les messages destinés à cette zone ne seront donc
C     plus affichés.
C
C Entrée:
C     CHARACTER*(*) ZONE      Nom hiérarchique de la zone
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LOG_EFFZONE(ZONE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_EFFZONE
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'log.fc'

      INTEGER LZ, LZS, IDX
      INTEGER LBUF
      CHARACTER*(64) BUF
C-----------------------------------------------------------------------

      LZ = SP_STRN_LEN(ZONE)
      IF (LZ .GT. 0) THEN

         LBUF = SP_STRN_LEN(ZONE)
         BUF = '@' // ZONE(1:LBUF) // '@'
         LBUF = LBUF + 2

         IDX = INDEX(LOG_ZONES, BUF(1:LBUF))
         IF (IDX .GT. 0) THEN
            LZS = SP_STRN_LEN(LOG_ZONES)
            IF (IDX+LZ+5+1 .LE. LZS) THEN
               LOG_ZONES = LOG_ZONES(1:IDX) //
     &                     LOG_ZONES(IDX+LZ+5+1:LZS)
            ELSE
               LOG_ZONES = LOG_ZONES(1:IDX)
            ENDIF
         ENDIF
      ENDIF

      LOG_EFFZONE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la zone est active.
C
C Description:
C     La fonction <code>LOG_ESTZONEACTIVE(...)</code> return .TRUE. si la zone
C     <code>ZONE</code> est active pour le niveau <code>ILVL</code>.
C
C Entrée:
C     CHARACTER*(*) ZONE      Nom hiérarchique de la zone
C     INTEGER       ILVL      Niveau de log de la zone
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LOG_ESTZONEACTIVE(ZONE, ILVL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_ESTZONEACTIVE
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE
      INTEGER       ILVL

      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'log.fc'

      LOGICAL        TROUVE
      INTEGER        LBUF, IDX, I
      CHARACTER*(64) BUF

      INTEGER SP_STRN_RINDEX
C-----------------------------------------------------------------------

      LBUF = SP_STRN_LEN(ZONE)
      BUF = '@' // ZONE(1:LBUF) // '@'
      LBUF = LBUF + 2

      TROUVE = .FALSE.
100   CONTINUE
         IDX = INDEX(LOG_ZONES, BUF(1:LBUF))
         IF (IDX .GT. 0) THEN
            IDX = IDX + LBUF
            READ(LOG_ZONES(IDX:IDX+2), *) I
            IF (ILVL .LE. I) TROUVE = .TRUE.
            GOTO 199
         ENDIF
         IDX = SP_STRN_RINDEX(BUF(1:LBUF), '.')
         IF (IDX .LE. 0) GOTO 199
         BUF(IDX:IDX) = '@'
         LBUF = IDX
      GOTO 100
199   CONTINUE

      LOG_ESTZONEACTIVE = TROUVE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le niveau de log de la zone.
C
C Description:
C     La fonction <code>LOG_REQNIVZ(...)</code> return le niveau de log
C     de la zone <code>ZONE</code>.
C
C Entrée:
C     CHARACTER*(*) ZONE      Nom hiérarchique de la zone
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LOG_REQNIVZ(ZONE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_REQNIVZ
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE

      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'log.fc'

      INTEGER        LBUF, IDX, ILVL
      CHARACTER*(64) BUF

      INTEGER SP_STRN_RINDEX
C-----------------------------------------------------------------------

      LBUF = SP_STRN_LEN(ZONE)
      BUF = '@' // ZONE(1:LBUF) // '@'
      LBUF = LBUF + 2

      ILVL = -1
100   CONTINUE
         IDX = INDEX(LOG_ZONES, BUF(1:LBUF))
         IF (IDX .GT. 0) THEN
            IDX = IDX + LBUF
            READ(LOG_ZONES(IDX:IDX+2), *) ILVL
            GOTO 199
         ENDIF
         IDX = SP_STRN_RINDEX(BUF(1:LBUF), '.')
         IF (IDX .LE. 0) GOTO 199
         BUF(IDX:IDX) = '@'
         LBUF = IDX
      GOTO 100
199   CONTINUE

      LOG_REQNIVZ = ILVL
      RETURN
      END

C************************************************************************
C Sommaire: Retourne l'unité d'écriture
C
C Description:
C     La fonction <code>LOG_REQMP(...)</code> retourne l'unité d'écriture
C     du système de log.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LOG_REQMP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LOG_REQMP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'log.fc'
C-----------------------------------------------------------------------

      LOG_REQMP = LOG_MP
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_RINDEX
C
C Description:
C     La fonction SP_STRN_RINDEX retourne l'indice de TOK dans la chaîne
C     en scannant à partir de la fin.
C
C Entrée:
C     CHARACTER*(*) TOK    Le token recherché
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_RINDEX(STRING, TOK)

      IMPLICIT NONE
      INTEGER SP_STRN_RINDEX
      CHARACTER*(*) STRING
      CHARACTER*(*) TOK

      INCLUDE 'spstrn.fi'

      INTEGER I, LTOK
C-----------------------------------------------------------------------

      LTOK = LEN(TOK)
      I = LEN(STRING) + 1
20    CONTINUE
         I = I - 1
      IF ((I .GT. 0) .AND. (STRING(I:I) .NE. TOK(1:1))) GOTO 20

      SP_STRN_RINDEX = I
      RETURN
      END

