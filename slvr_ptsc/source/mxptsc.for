C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MX_PTSC_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PTSC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MX_PTSC_NOBJMAX,
     &                   MX_PTSC_HBASE,
     &                   'PETSc Matrix')

      MX_PTSC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PTSC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER  IERR
      EXTERNAL MX_PTSC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MX_PTSC_NOBJMAX,
     &                   MX_PTSC_HBASE,
     &                   MX_PTSC_DTR)

      MX_PTSC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PTSC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MX_PTSC_NOBJMAX,
     &                   MX_PTSC_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MX_PTSC_HVALIDE(HOBJ))
         IOB = HOBJ - MX_PTSC_HBASE

         MX_PTSC_XMAT (IOB) = 0
         MX_PTSC_HPRNT(IOB) = 0
      ENDIF

      MX_PTSC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PTSC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MX_PTSC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MX_PTSC_NOBJMAX,
     &                   MX_PTSC_HBASE)

      MX_PTSC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PTSC_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = MX_PTSC_RST(HOBJ)

C---     Construit et initialise le parent
      IF (ERR_GOOD()) THEN
         IERR = MX_DIST_CTR(HPRNT)
         IERR = MX_DIST_INI(HPRNT, MX_DIST_NUM_PETSC, .FALSE.) ! Sans données
      ENDIF

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - MX_PTSC_HBASE
         MX_PTSC_XMAT (IOB) = 0
         MX_PTSC_HPRNT(IOB) = HPRNT
      ENDIF

      MX_PTSC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PTSC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxptsc.fi'
      INCLUDE 'c_petsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER*8 XMAT
      INTEGER   IOB
      INTEGER   IERR
      INTEGER   HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_PTSC_HBASE

C---     RECUPERE LES ATTRIBUTS
      XMAT = MX_PTSC_XMAT (IOB)
      HPRNT= MX_PTSC_HPRNT(IOB)

C---     DETRUIS LA MATRICE PETSc
      IF (XMAT .NE. 0) THEN
         IERR = C_PTSC_MAT_DESTROY(XMAT)
      ENDIF

C---     DETRUIS LE PARENT
      IF (HPRNT .NE. 0) THEN
         IERR = MX_DIST_DTR(HPRNT)
      ENDIF

C---     RESET LES ATTRIBUTS
      MX_PTSC_XMAT (IOB) = 0
      MX_PTSC_HPRNT(IOB) = 0

      MX_PTSC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MX_PTSC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PTSC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mxptsc.fc'
C------------------------------------------------------------------------

      MX_PTSC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MX_PTSC_NOBJMAX,
     &                                  MX_PTSC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PTSC_ASMKE(HOBJ, NDLE, KLOCE, VDLE, VKE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_ASMKE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLE
      INTEGER KLOCE(*)
      REAL*8  VDLE (*)
      REAL*8  VKE  (*)

      INCLUDE 'mxptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'c_petsc.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER*8 XMAT
      INTEGER   IOB
      INTEGER   IERR
      INTEGER   HPRNT
      INTEGER   LCOLG, LLING
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB   = HOBJ - MX_PTSC_HBASE
      XMAT  = MX_PTSC_XMAT (IOB)
      HPRNT = MX_PTSC_HPRNT(IOB)
      LCOLG = MX_DIST_REQLCOLG(HPRNT)
      LLING = MX_DIST_REQLLING(HPRNT)

C---        ASSEMBLAGE DANS LA MATRICE
      IERR = C_PTSC_MAT_ASMKE(XMAT,
     &                        KA(SO_ALLC_REQKIND(KA,LLING)),
     &                        KA(SO_ALLC_REQKIND(KA,LCOLG)),
     &                        NDLE,
     &                        KLOCE,
     &                        VKE)

      MX_PTSC_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     DIMENSIONNE
C     CALCUL DES POINTEURS DU STOCKAGE MORSE DU TYPE
C     COMPRESSION DE LIGNE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PTSC_DIMMAT(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_DIMMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_petsc.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER*8 XMAT
      INTEGER   IOB
      INTEGER   IERR
      INTEGER   ILU
      INTEGER   HPRNT
      INTEGER   HMMR
      INTEGER   LIAP, LJAP
      INTEGER   LCOLG, LLING
      INTEGER   NEQ
      INTEGER   NKGP
C----------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_PTSC_HBASE
      XMAT  = MX_PTSC_XMAT (IOB)
      HPRNT = MX_PTSC_HPRNT(IOB)
D     CALL ERR_ASR(MX_DIST_HVALIDE(HPRNT))

C---     AU BESOIN, CREE LA MATRICE
      IF (XMAT .EQ. 0) THEN

C---        DIMENSIONNE LE TABLEAU DE LOCALISATION GLOBALE
         IERR = MX_DIST_DIMMAT(HPRNT, HSIM)
         LCOLG = MX_DIST_REQLCOLG(HPRNT)
         LLING = MX_DIST_REQLLING(HPRNT)

C---        ASSEMBLE LES INDICES SUR UNE MATRICE MORSE
         HMMR = 0
         ILU  = 0
         IF (ERR_GOOD()) IERR = MX_MORS_CTR   (HMMR)
         IF (ERR_GOOD()) IERR = MX_MORS_INI   (HMMR, ILU)
         IF (ERR_GOOD()) IERR = MX_MORS_DIMIND(HMMR, HSIM)

C---        RECUPERE LES ATTRIBUTS DE LA MATRICE MORSE
         IF (ERR_GOOD()) THEN
            NEQ  = MX_MORS_REQNEQL(HMMR)
D           CALL ERR_ASR(NEQ .EQ. MX_DIST_REQNEQL(HPRNT))
            NKGP = MX_MORS_REQNKGP(HMMR)
            LIAP = MX_MORS_REQLIAP(HMMR)
            LJAP = MX_MORS_REQLJAP(HMMR)
         ENDIF

C---        CREE LA MATRICE PETSc
         IF (ERR_GOOD()) THEN
            IERR = C_PTSC_MAT_CREATE(XMAT,
     &                               NEQ,
     &                               NKGP,
     &                               KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                               KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                               KA(SO_ALLC_REQKIND(KA,LCOLG)),
     &                               KA(SO_ALLC_REQKIND(KA,LLING)))
C           IERR = F_PTSC_ERRMSG(IERR)
         ENDIF

C---        DETRUIS LA MATRICE MORSE
         IF (MX_MORS_HVALIDE(HMMR)) IERR = MX_MORS_DTR(HMMR)

C---        STOKE LES ATTRIBUTS
         IF (ERR_GOOD()) THEN
            MX_PTSC_XMAT(IOB) = XMAT
         ENDIF

      ENDIF

C---     INITIALISE LA MATRICE
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_MAT_INITZERO(XMAT)
C         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

      MX_PTSC_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PTSC_MULVAL(HOBJ, A)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_MULVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  A

      INCLUDE 'mxptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_PTSC_HBASE

      CALL LOG_TODO('MX_PTSC_MULVAL NON IMPLANTE')
      CALL ERR_ASR(.FALSE.)

      MX_PTSC_MULVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PTSC_MULVEC(HOBJ, N, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_MULVEC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)
      REAL*8  R(*)

      INCLUDE 'mxptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER IOB
      INTEGER NEQ
C      INTEGER LKG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_PTSC_HBASE
      NEQ = MX_PTSC_REQNEQL(HOBJ)
C      LKG  = MX_PTSC_LKG (IOB)

      CALL LOG_TODO('MX_PTSC_MULVEC NON IMPLANTE')
      CALL ERR_ASR(.FALSE.)

      MX_PTSC_MULVEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PTSC_REQNEQL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_REQNEQL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxptsc.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER HPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = MX_PTSC_HPRNT(HOBJ-MX_PTSC_HBASE)
      MX_PTSC_REQNEQL = MX_DIST_REQNEQL(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PTSC_REQLCOLG(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_REQLCOLG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxptsc.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER HPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = MX_PTSC_HPRNT(HOBJ-MX_PTSC_HBASE)
      MX_PTSC_REQLCOLG = MX_DIST_REQLCOLG(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PTSC_REQLLING(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_REQLLING
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxptsc.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mxptsc.fc'

      INTEGER HPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = MX_PTSC_HPRNT(HOBJ-MX_PTSC_HBASE)
      MX_PTSC_REQLLING = MX_DIST_REQLLING(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PTSC_REQXMAT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PTSC_REQXMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxptsc.fi'
      INCLUDE 'mxptsc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_PTSC_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_PTSC_XMAT(HOBJ-MX_PTSC_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_PTSC_REQXMAT = MX_PTSC_XMAT(HOBJ-MX_PTSC_HBASE)
      RETURN
      END
