C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER F_MMPS_INITIALIZE
C     INTEGER F_MMPS_FINALIZE
C     INTEGER F_MMPS_ERRMSG
C     INTEGER F_MMPS_MAT_CREATE
C     INTEGER F_MMPS_MAT_DESTROY
C     INTEGER F_MMPS_MAT_ANALIZE
C     INTEGER F_MMPS_MAT_FACT
C     INTEGER F_MMPS_MAT_SOLVE
C     INTEGER F_MMPS_MAT_DMP
C   Private:
C     LOGICAL F_MMPS_HVALIDE
C     INTEGER F_MMPS_GTHRRHS
C     INTEGER F_MMPS_SCTRRHS
C     INTEGER F_MMPS_INITIALIZE_M
C     INTEGER F_MMPS_FINALIZE_M
C     INTEGER F_MMPS_ERRMSG_M
C     INTEGER F_MMPS_MAT_CREATE_M
C     INTEGER F_MMPS_MAT_DESTROY_M
C     INTEGER F_MMPS_MAT_ANALIZE_M
C     INTEGER F_MMPS_MAT_FACT_M
C     INTEGER F_MMPS_MAT_SOLVE_M
C     INTEGER F_MMPS_MAT_DMP_M
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les variables statiques.
C
C Description:
C     Le block data initialise le compte de référence pour l'initialisation.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE F_MUMPS_M

      IMPLICIT NONE
      INCLUDE 'dmumps_struc.h'
      PRIVATE

      INTEGER, PARAMETER :: F_MMPS_HBASE = 99001000
      INTEGER, PARAMETER :: F_MMPS_NMAX = 4

C---     Les tables de pointeurs n'existent pas
C        Elles sont simulées avec une structure
C        qui contient le pointeur
      TYPE :: F_MMPS_MAT_T
         TYPE (DMUMPS_STRUC),   POINTER :: mstruc
         REAL*8,  DIMENSION(:), POINTER :: rhsg
         INTEGER ierr
      END TYPE F_MMPS_MAT_T

      TYPE (F_MMPS_MAT_T), DIMENSION(:), POINTER :: F_MMPS_MATS
      INTEGER :: F_MMPS_NINIT = 0

C---     Codes d'erreur
      INTEGER, PARAMETER :: F_MMPS_ERR_OK   =  0
      INTEGER, PARAMETER :: F_MMPS_ERR_FTN  = -1
      INTEGER, PARAMETER :: F_MMPS_ERR_MPI  = -2
      INTEGER, PARAMETER :: F_MMPS_ERR_MMPS = -3

C---     Déclaration des fonctions
      PRIVATE :: F_MMPS_HVALIDE
      PRIVATE :: F_MMPS_GTHRRHS
      PRIVATE :: F_MMPS_SCTRRHS

      PUBLIC  :: F_MMPS_INITIALIZE_M
      PUBLIC  :: F_MMPS_FINALIZE_M
      PUBLIC  :: F_MMPS_ERRMSG_M
      PUBLIC  :: F_MMPS_MAT_CREATE_M
      PUBLIC  :: F_MMPS_MAT_DESTROY_M
      PUBLIC  :: F_MMPS_MAT_ANALIZE_M
      PUBLIC  :: F_MMPS_MAT_FACT_M
      PUBLIC  :: F_MMPS_MAT_SOLVE_M
      PUBLIC  :: F_MMPS_MAT_DMP_M

      CONTAINS

C************************************************************************
C Sommaire: Valide un handle.
C
C Description:
C     La fonction privée <code>F_MMPS_HVALIDE(...)</code>
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION F_MMPS_HVALIDE(HMAT)

      LOGICAL F_MMPS_HVALIDE
      INTEGER HMAT

      LOGICAL L
C------------------------------------------------------------------------

      L = .TRUE.
      IF (HMAT-F_MMPS_HBASE .LE. 0) L = .FALSE.
      IF (HMAT-F_MMPS_HBASE .GT. F_MMPS_NMAX) L = .FALSE.

      F_MMPS_HVALIDE = L
      RETURN
      END FUNCTION F_MMPS_HVALIDE

C************************************************************************
C Sommaire: Assemble le membre de droite global
C
C Description:
C     La fonction privée <code>F_MMPS_GTHHRHS(...)</code> gather
C     le membre de droite global VRHS à partir des membres de droite
C     locaux VFG.
C
C Entrée:
C     NEQ         Nombre d'équations locales
C     NEQT        Nombre d'équations totales
C     VFG         Membre de droite local
C     KLING       Table des DDL privés au process
C
C Sortie:
C     VRHS        Membre de droite global
C
C Notes:
C     L'expression VRHS(KLCGL(1:NEQL)) = VFG(1:NEQL) génère des
C     vecteurs temporaires (pour les indices ?) qui peuvent
C     faire sauter le stack. Il faudrait jouer avec:
C         !DIR$ IVDEP
C         !DIR$ ASSUME_ALIGNED VRHS:64 VFG:64
C     Il faut avant s'assurer de l'alignement, i.e. revoir les flags
C     de compilation et les allocations de mémoire (c_mm et autres)
C
C************************************************************************
      FUNCTION F_MMPS_GTHRRHS(NEQL,
     &                        NEQT,
     &                        VFG,
     &                        VRHS,
     &                        KLCGL,
     &                        I_COMM)

      INTEGER F_MMPS_GTHRRHS
      INTEGER, INTENT(IN) :: NEQL
      INTEGER, INTENT(IN) :: NEQT
      REAL*8,  INTENT(IN) :: VFG  (NEQL)
      REAL*8,  INTENT(INOUT) :: VRHS (NEQT)
      INTEGER, INTENT(IN) :: KLCGL(NEQL)
      INTEGER, INTENT(IN) :: I_COMM

      include 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_ERROR
      INTEGER I_RANK
      INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER IERR
      INTEGER IG, IL
      REAL*8  VDUM
C------------------------------------------------------------------------
D     CALL ERR_ASR(NEQL .GT. 0)
D     CALL ERR_ASR(NEQT .GE. NEQL)
C------------------------------------------------------------------------

      IERR = F_MMPS_ERR_OK

C---     Copie la portion locale
      VRHS(1:NEQT) = 0.0D0
      ! VRHS(KLCGL(1:NEQL)) = VFG(1:NEQL) c.f. note
!dir$ IVDEP
      DO IL=1,NEQL
         IG = KLCGL(IL)
D        CALL ERR_ASR(IG .GE. 1 .AND. IG .LE. NEQT)
         VRHS(IG) = VFG(IL)
      ENDDO

C---     Réduis globalement
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
      IF (I_RANK .EQ. I_MASTER) THEN
         CALL MPI_REDUCE(MPI_IN_PLACE, VRHS, NEQT, MP_TYPE_RE8(),
     &                   MPI_SUM, I_MASTER, I_COMM, I_ERROR)
      ELSE
         CALL MPI_REDUCE(VRHS, VDUM, NEQT, MP_TYPE_RE8(),
     &                   MPI_SUM, I_MASTER, I_COMM, I_ERROR)
      ENDIF
      IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901

      GOTO 9999
C------------------------------------------------------------------------
9900  IERR = F_MMPS_ERR_FTN      ! Erreur FTN
      GOTO 9999
9901  IERR = F_MMPS_ERR_MPI      ! Erreur MPI
      GOTO 9999
9902  IERR = F_MMPS_ERR_MMPS     ! Erreur MUMPS
      GOTO 9999

9999  CONTINUE
      F_MMPS_GTHRRHS = IERR
      RETURN
      END FUNCTION F_MMPS_GTHRRHS

C************************************************************************
C Sommaire: Assemble le membre de droite global
C
C Description:
C     La fonction privée <code>F_MMPS_SCTRRHS(...)</code> scatter
C     le membre de droite global VRHS vers les membres de droite
C     locaux VFG.
C
C Entrée:
C     NEQL        Nombre d'équations locales
C     NEQT        Nombre d'équations totales
C     VFG         Membre de droite local
C     KLCGL       Table des transfert local-global
C
C Sortie:
C     VFG         Membre de droite local
C
C Notes:
C
C************************************************************************
      FUNCTION F_MMPS_SCTRRHS(NEQL,
     &                        NEQT,
     &                        VFG,
     &                        VRHS,
     &                        KLCGL,
     &                        I_COMM)

      INTEGER F_MMPS_SCTRRHS
      INTEGER NEQL
      INTEGER NEQT
      REAL*8  VFG  (NEQL)
      REAL*8  VRHS (NEQT)
      INTEGER KLCGL(NEQL)
      INTEGER I_COMM

      include 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_ERROR
      INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER IERR
      INTEGER IG, IL
C------------------------------------------------------------------------
D     CALL ERR_ASR(NEQL .GT. 0)
D     CALL ERR_ASR(NEQT .GE. NEQL)
C------------------------------------------------------------------------

      IERR = F_MMPS_ERR_OK

      CALL MPI_BCAST(VRHS, NEQT, MP_TYPE_RE8(),
     &               I_MASTER, I_COMM, I_ERROR)
      IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901

C---     Transfert la solution
      VFG(1:NEQL) = VRHS(KLCGL(1:NEQL))

      GOTO 9999
C------------------------------------------------------------------------
9900  IERR = F_MMPS_ERR_FTN      ! Erreur FTN
      GOTO 9999
9901  IERR = F_MMPS_ERR_MPI      ! Erreur MPI
      GOTO 9999
9902  IERR = F_MMPS_ERR_MMPS     ! Erreur MUMPS
      GOTO 9999

9999  CONTINUE
      F_MMPS_SCTRRHS = IERR
      RETURN
      END FUNCTION F_MMPS_SCTRRHS

C************************************************************************
C Sommaire:    Initialise le système.
C
C Description:
C     La fonction F_MMPS_INITIALIZE() fait l'initialisation de bas niveau
C     nécessaire au fonctionnement du module. Elle doit obligatoirement être
C     appelée avant toute autre fonction. Son pendant est F_MMPS_FINALIZE.
C     <p>
C     La fonction peut être appelée de multiples fois. Il est de la
C     responsabilité de l'utilisateur de faire les appels symétriques à
C     FF_MMPS_FINALIZE.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION F_MMPS_INITIALIZE_M()

      INTEGER F_MMPS_INITIALIZE_M

      INTEGER IERR, IMAT
C------------------------------------------------------------------------

      IERR = F_MMPS_ERR_OK

      IF (F_MMPS_NINIT .EQ. 0) THEN
         ALLOCATE (F_MMPS_MATS(F_MMPS_NMAX), STAT=IERR)
         IF (IERR .NE. 0) GOTO 9900

         DO IMAT=1,F_MMPS_NMAX
            NULLIFY (F_MMPS_MATS(IMAT)%mstruc)
         ENDDO
      ENDIF
      F_MMPS_NINIT = F_MMPS_NINIT + 1

      GOTO 9999
C------------------------------------------------------------------------
9900  IERR = F_MMPS_ERR_FTN      ! Erreur FTN
      GOTO 9999

9999  CONTINUE
      F_MMPS_INITIALIZE_M = IERR
      RETURN
      END FUNCTION F_MMPS_INITIALIZE_M

C************************************************************************
C Sommaire: Finalise le système
C
C Description:
C     La fonction FF_MMPS_FINALIZE() fait la finalisation de bas niveau
C     nécessaire au fonctionnement du module. Elle est le pendant de
C     F_MMPS_INITIALIZE.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION F_MMPS_FINALIZE_M()

      INTEGER F_MMPS_FINALIZE_M

      INTEGER IERR, IRET, IMAT
C------------------------------------------------------------------------

      IERR = F_MMPS_ERR_OK

      IF (F_MMPS_NINIT .EQ. 1) THEN
         DO IMAT=1,F_MMPS_NMAX
            IF ( ASSOCIATED(F_MMPS_MATS(IMAT)%mstruc) ) THEN
               IRET = F_MMPS_MAT_DESTROY_M(IMAT+F_MMPS_HBASE)
            ENDIF
         ENDDO
         DEALLOCATE (F_MMPS_MATS, STAT=IERR)
      ENDIF
      IF (F_MMPS_NINIT .GT. 0) F_MMPS_NINIT = F_MMPS_NINIT - 1

      F_MMPS_FINALIZE_M = IERR
      RETURN
      END FUNCTION F_MMPS_FINALIZE_M

C************************************************************************
C Sommaire: Retourne le message d'erreur
C
C Description:
C     La fonction <code>F_MMPS_ERRMSG(...)</code> retourne la chaîne de
C     caractères du message d'erreur actuel.
C
C Entrée:
C     HMAT           Handle sur l'objet courant
C
C Sortie:
C     MSG            Le message d'erreur
C
C Notes:
C************************************************************************
      FUNCTION F_MMPS_ERRMSG_M(HMAT, MSG)

      INTEGER       F_MMPS_ERRMSG_M
      INTEGER       HMAT
      CHARACTER*(*) MSG

      INCLUDE 'mpif.h'
      INCLUDE 'spstrn.fi'

      INTEGER*8 ISIZE
      INTEGER   I_ERROR, L_ERRSTR
      INTEGER   ICOD, IMAT
      TYPE (DMUMPS_STRUC), POINTER :: mumps_par
      CHARACTER*(64)  PRE
C------------------------------------------------------------------------
D     CALL ERR_PRE(F_MMPS_HVALIDE(HMAT))
C------------------------------------------------------------------------

C---     Récupère la structure
      IMAT = HMAT - F_MMPS_HBASE
      mumps_par => F_MMPS_MATS(IMAT)%mstruc

      PRE = ' '
      MSG = ' '

C---     Erreur Fortran
1000  CONTINUE
      IF (F_MMPS_MATS(IMAT)%ierr .NE. F_MMPS_ERR_FTN) GOTO 2000
      MSG = 'Fortran run-time error'
      GOTO 9999

C---     Erreur MPI
2000  CONTINUE
      IF (F_MMPS_MATS(IMAT)%ierr .NE. F_MMPS_ERR_MPI) GOTO 3000
      MSG = 'MPI run-time error'
      GOTO 9999

C---     Les erreurs MUMPS
3000  CONTINUE
      IF (F_MMPS_MATS(IMAT)%ierr .NE. F_MMPS_ERR_MMPS) GOTO 4000
      IF (mumps_par%INFOG(1) .EQ. 0) GOTO 4000
      ICOD = mumps_par%INFO(1)
      IF (ICOD .EQ. -1) THEN
         WRITE(PRE,'(A,I,A)') 'An error occurred on processor ',
     &                         mumps_par%INFO(2), '.'
      ENDIF

      ICOD = mumps_par%INFOG(1)
      IF      (ICOD .EQ. -2) THEN
         MSG = 'NZ is out of range.'
      ELSE IF (ICOD .EQ. -3) THEN
         MSG = 'MUMPS was called with an invalid value for JOB'
      ELSE IF (ICOD .EQ. -4) THEN
         MSG = 'Error in user-provided permutation array PERM_IN.'
      ELSE IF (ICOD .EQ. -5) THEN
         MSG = 'Problem of REAL allocation during analysis.'
      ELSE IF (ICOD .EQ. -6) THEN
         MSG = 'Matrix is singular in structure.'
      ELSE IF (ICOD .EQ. -7) THEN
         MSG = 'Problem of INTEGER allocation during analysis.'
      ELSE IF (ICOD .EQ. -8) THEN
         MSG = 'MAXIS too small for factorization.'
      ELSE IF (ICOD .EQ. -9) THEN
         MSG = 'MAXS too small for factorization.'
      ELSE IF (ICOD .EQ. -10) THEN
         MSG = 'Numerically singular matrix.'
      ELSE IF (ICOD .EQ. -11) THEN
         MSG = 'MAXS too small for solution.'
      ELSE IF (ICOD .EQ. -12) THEN
         MSG = 'MAXS too small for iterative refinement.'
      ELSE IF (ICOD .EQ. -13) THEN
         ISIZE = mumps_par%INFOG(2)
         IF (ISIZE .LT. 0) ISIZE = ABS(ISIZE)*1000000
         WRITE(MSG,'(2A,I12)')
     &      'Error in a Fortran ALLOCATE statement. ',
     &      'Size requested: ', ISIZE
      ELSE IF (ICOD .EQ. -14) THEN
         MSG = 'MAXIS too small for solution.'
      ELSE IF (ICOD .EQ. -15) THEN
         MSG = 'MAXIS too small for iterative refinement ' //
     &         'and/or error analysis.'
      ELSE IF (ICOD .EQ. -16) THEN
         MSG = 'N is out of range.'
      ELSE IF (ICOD .EQ. -17) THEN
         MSG = 'The internal send buffer is too small.'
      ELSE IF (ICOD .EQ. -18) THEN
         MSG = 'MAXIS too small to process root node.'
      ELSE IF (ICOD .EQ. -19) THEN
         MSG = 'MAXS too small to process root node.'
      ELSE IF (ICOD .EQ. -20) THEN
         MSG = 'The internal reception buffer is too small.'
      ELSE IF (ICOD .EQ. -21) THEN
         MSG = 'Value of PAR=0 is not allowed with only one processor.'
      ELSE IF (ICOD .EQ. -22) THEN
         MSG = 'A pointer array provided by the user is invalid.'
      ELSE IF (ICOD .EQ. -23) THEN
         MSG = 'MPI was not initialized.'
      ELSE IF (ICOD .EQ. -24) THEN
         MSG = 'NELT is out of range.'
      ELSE IF (ICOD .EQ. -25) THEN
         MSG = 'A problem has occurred in the initialization of BLACS.'
      ELSE IF (ICOD .EQ. -26) THEN
         MSG = 'LRHS is out of range.'
      ELSE IF (ICOD .EQ. -27) THEN
         MSG = 'NZ RHS and IRHS PTR(NRHS+1) do not match.'
      ELSE IF (ICOD .EQ. -28) THEN
         MSG = 'IRHS_PTR(1) is not equal to 1.'
      ELSE IF (ICOD .EQ. -29) THEN
         MSG = 'LSOL_LOC is smaller than KEEP(89).'
      ELSE IF (ICOD .EQ. -30) THEN
         MSG = 'SCHUR LLD is out of range.'
      ELSE IF (ICOD .EQ. -31) THEN
         MSG = '2D block cyclic Schur incompatible with process grid.'
      ELSE IF (ICOD .EQ. -32) THEN
         MSG = 'Incompatible values of NRHS and ICNTL(25).'
      ELSE IF (ICOD .EQ. -33) THEN
         MSG = 'Schur complement not computed during factorization.'
      ELSE IF (ICOD .EQ. -34) THEN
         MSG = 'LREDRHS is out of range.'
      ELSE IF (ICOD .EQ. -35) THEN
         MSG = 'Reduction phase (ICNTL(26)=1) was not called.'
      ELSE IF (ICOD .EQ. -36) THEN
         MSG = 'Incompatible values of ICNTL(25) and INFOG(28).'

C---     Les avertissements
      ELSE IF (ICOD .GT. 0) THEN
         MSG = 'Warning: '
         IF (ICOD .GE. 8)  THEN
            MSG = MSG(1:SP_STRN_LEN(MSG)) //
     &        'Warning return from the iterative refinement routine.'
            ICOD = ICOD - 8
         ENDIF
         IF (ICOD .GE. 4)  THEN
            MSG = MSG(1:SP_STRN_LEN(MSG)) //
     &        'User data JCN has been corrupted.'
            ICOD = ICOD - 4
         ENDIF
         IF (ICOD .GE. 2)  THEN
            MSG = MSG(1:SP_STRN_LEN(MSG)) //
     &        'During error analysis the max-norm of the ' //
     &        'computed solution was found to be zero.'
            ICOD = ICOD - 2
         ENDIF
         IF (ICOD .GE. 1)  THEN
            MSG = MSG(1:SP_STRN_LEN(MSG)) //
     &        'Index (in IRN or JCN) out of range. Action taken ' //
     &        'is to ignore any such entries and continue.'
            ICOD = ICOD - 1
         ENDIF
      ENDIF

4000  CONTINUE
      IF (SP_STRN_LEN(PRE) .GT. 0) THEN
         MSG = PRE(1:SP_STRN_LEN(PRE)) // ' ' // MSG(1:SP_STRN_LEN(MSG))
      ENDIF

9999  CONTINUE
      F_MMPS_ERRMSG_M = 0
      RETURN
      END FUNCTION F_MMPS_ERRMSG_M

C************************************************************************
C Sommaire:   Crée une matrice distribuée MUMPS
C
C Description:
C     La fonction <code>F_MMPS_MAT_CREATE(...)</code> crée une matrice
C     distribuée MUMPS qui devient disponible pour les opérations d'analyse,
C     de factorisation et de résolution.
C
C Entrée:
C     NNZ               Nombre de termes non nuls (local)
C     ICOMM             Communicateur MPI
C
C Sortie:
C     HMAT              Handle sur la matrice créée
C
C Notes:
C     Voir commentaires options dans la fonction F_MMPS_SOLVE
C
C-DEBUT OPTION2
C     Vérifier que MPI_DATATYPE_NULL est standard.
C-FIN OPTION2
C
C************************************************************************
      FUNCTION F_MMPS_MAT_CREATE_M(HMAT, NNZ, ICOMM)

      INTEGER F_MMPS_MAT_CREATE_M
      INTEGER HMAT
      INTEGER NNZ
      INTEGER ICOMM

      INCLUDE 'c_os.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER ILVL
      INTEGER IMAT
      INTEGER I_SIZE, I_ERROR
      INTEGER INDMIN, NVAL
      TYPE (DMUMPS_STRUC), POINTER :: mumps_par
!      TYPE (TYPES_STRUC),  POINTER :: types_par
!      INTEGER, DIMENSION(:), POINTER :: types
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_ASR( ASSOCIATED(F_MMPS_MATS) )
C------------------------------------------------------------------------

      IERR = F_MMPS_ERR_OK

C---     Alloue une nouvelle structure
      HMAT = 0
      NULLIFY(mumps_par)
      DO IMAT=1,F_MMPS_NMAX
         IF (.NOT. ASSOCIATED(F_MMPS_MATS(IMAT)%mstruc)) THEN
            ALLOCATE (F_MMPS_MATS(IMAT)%mstruc, STAT=IERR)
            IF (IERR .NE. 0) GOTO 9900
            NULLIFY  (F_MMPS_MATS(IMAT)%rhsg)
            F_MMPS_MATS(IMAT)%ierr = F_MMPS_ERR_OK
!            ALLOCATE (F_MMPS_MATS(IMAT)%tstruc, STAT=IERR)
!            IF (IERR .NE. 0) GOTO 9900
            GOTO 199
         ENDIF
      ENDDO
199   CONTINUE
      IF (IMAT .GT. F_MMPS_NMAX) GOTO 9900

C---     Valgrind reports following values used uninitialized
      mumps_par => F_MMPS_MATS(IMAT)%mstruc
      INDMIN = LBOUND(mumps_par%KEEP, 1)
      NVAL   = UBOUND(mumps_par%KEEP, 1) - indmin + 1
      CALL IINIT(NVAL,      0, mumps_par%KEEP(INDMIN),  1)
!      INDMIN = LBOUND(mumps_par%KEEP8, 1)
!      NVAL   = UBOUND(mumps_par%KEEP8, 1) - INDMIN + 1
!      CALL DINIT(NVAL, 0.0d0, mumps_par%KEEP8(INDMIN), 1)
      mumps_par%N = 0

C---     Initialize la structure MUMPS
      mumps_par => F_MMPS_MATS(IMAT)%mstruc
      mumps_par%JOB = -1
      mumps_par%COMM = ICOMM
      mumps_par%SYM  = 0      ! Non symmetric
      mumps_par%PAR  = 1      ! Host involved in factorization/solve
      CALL DMUMPS(mumps_par)
      if (mumps_par%INFOG(1) .NE. 0) GOTO 9902
      mumps_par%NZ_loc = NNZ

C---     Adjust the content
      IRET = C_OS_REQVARENV('H2D2_MUMPS_ERRLVL', BUF)
      IF (IRET .EQ. 0) THEN
         READ(BUF, *) ILVL
         mumps_par%ICNTL(1) = 6  ! Errors on unit 6
         mumps_par%ICNTL(2) = 6  ! Warnings on unit 6
         mumps_par%ICNTL(3) = 6  ! Collected data on unit 6
         mumps_par%ICNTL(4) = ILVL ! Level
      ELSE
         mumps_par%ICNTL(1) = 0  ! No output for errors
         mumps_par%ICNTL(2) = 0  ! No output for warnings
         mumps_par%ICNTL(3) = 0  ! No output for collected data
         mumps_par%ICNTL(4) = 0  ! Errors, warnings, and main statistics
      ENDIF

!C---     Initialise la structure des types
!      types_par => F_MMPS_MATS(IMAT)%tstruc
!      NULLIFY(types_par%types)
!      NULLIFY(types_par%klcglex)
!      NULLIFY(types_par%kneq)
!      NULLIFY(types_par%uns)
!
!C---     Alloue la table des types MPI sur le master
!      IF (mumps_par%myid .EQ. 0) THEN
!         CALL MPI_COMM_SIZE(mumps_par%COMM, I_SIZE, I_ERROR)
!         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!         ALLOCATE(types(I_SIZE-1), STAT=IERR)
!         IF (IERR .NE. 0) GOTO 9900
!         types(1:I_SIZE-1) = MPI_DATATYPE_NULL
!         types_par%types => types
!      ENDIF

C---     Le handle
      HMAT = IMAT + F_MMPS_HBASE

      GOTO 9999
C------------------------------------------------------------------------
9900  IERR = F_MMPS_ERR_FTN      ! Erreur FTN
      GOTO 9999
9901  IERR = F_MMPS_ERR_MPI      ! Erreur MPI
      GOTO 9999
9902  IERR = F_MMPS_ERR_MMPS     ! Erreur MUMPS
      GOTO 9999

9999  CONTINUE
      F_MMPS_MATS(IMAT)%ierr = IERR
      F_MMPS_MAT_CREATE_M = IERR
      RETURN
      END FUNCTION F_MMPS_MAT_CREATE_M

C**********************************************************************
C Sommaire: Détruis une matrice distribuée MUMPS
C
C Description:
C    La fonction <code>F_MMPS_MAT_DESTROY(...)</code> détruis la matrice
C    distribuée MUMPS passée en argument.
C
C Entrée:
C     HMAT     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C     Voir commentaires options dans la fonction F_MMPS_SOLVE
C
C-DEBUT OPTION2
C     Vérifier que MPI_DATATYPE_NULL est standard.
C-FIN OPTION2
C
C************************************************************************
      FUNCTION F_MMPS_MAT_DESTROY_M(HMAT)

      INTEGER F_MMPS_MAT_DESTROY_M
      INTEGER HMAT

      INCLUDE 'mpif.h'

      INTEGER IERR
      INTEGER IMAT
      INTEGER I, I_SIZE, I_ERROR
      TYPE (F_MMPS_MAT_T), POINTER :: mat
      TYPE (DMUMPS_STRUC), POINTER :: mumps_par
!      TYPE (TYPES_STRUC),  POINTER :: types_par
C------------------------------------------------------------------------
D     CALL ERR_PRE(F_MMPS_HVALIDE(HMAT))
C------------------------------------------------------------------------

C---     Récupère la structure
      IMAT = HMAT - F_MMPS_HBASE
      mat => F_MMPS_MATS(IMAT)
      mumps_par => mat%mstruc
!      types_par => mat%tstruc

C---     Libère les types et le tableau de types
!      IF (ASSOCIATED(types_par%types)) THEN
!         CALL MPI_COMM_SIZE(mumps_par%COMM, I_SIZE, I_ERROR)
!         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!         DO I=1,(I_SIZE-1)
!            IF (types_par%types(I) .NE. MPI_DATATYPE_NULL) THEN
!               CALL MPI_TYPE_FREE(types_par%types(I), I_ERROR)
!               IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!            ENDIF
!         ENDDO
!         DEALLOCATE(types_par%types, STAT=IERR)
!         IF (IERR .NE. 0) GOTO 9900
!         NULLIFY(types_par%types)
!      ENDIF

C---     Libère les tables servant à créer les types
!      IF (ASSOCIATED(types_par%KNEQ)) THEN
!         DEALLOCATE(types_par%KNEQ, STAT=IERR)
!         IF (IERR .NE. 0) GOTO 9900
!         NULLIFY(types_par%KNEQ)
!      ENDIF
!      IF (ASSOCIATED(types_par%KCOLGEX)) THEN
!         DEALLOCATE(types_par%KCOLGEX, STAT=IERR)
!         IF (IERR .NE. 0) GOTO 9900
!         NULLIFY(types_par%KCOLGEX)
!      ENDIF
!      IF (ASSOCIATED(types_par%UNS)) THEN
!         DEALLOCATE(types_par%UNS, STAT=IERR)
!         IF (IERR .NE. 0) GOTO 9900
!         NULLIFY(types_par%UNS)
!      ENDIF
!      DEALLOCATE(types_par, STAT=IERR)
!      IF (IERR .NE. 0) GOTO 9900
!      NULLIFY(F_MMPS_MATS(IMAT)%tstruc)

C---     Détruis la structure MUMPS
      mumps_par%JOB = -2
      CALL DMUMPS(mumps_par)
      if (mumps_par%INFOG(1) .NE. 0) GOTO 9902

C---     Désalloue la structure MUMPS
      IF (ASSOCIATED(mumps_par%irn_loc)) THEN
         DEALLOCATE (mumps_par%irn_loc, STAT=IERR)
         IF (IERR .NE. 0) GOTO 9900
         NULLIFY(mumps_par%irn_loc)
      ENDIF
      IF (ASSOCIATED(mumps_par%jcn_loc)) THEN
         DEALLOCATE (mumps_par%jcn_loc, STAT=IERR)
         IF (IERR .NE. 0) GOTO 9900
         NULLIFY(mumps_par%jcn_loc)
      ENDIF
      IF (ASSOCIATED(mumps_par%RHS)) THEN
         DEALLOCATE (mumps_par%RHS, STAT=IERR)
         IF (IERR .NE. 0) GOTO 9900
         NULLIFY(mumps_par%RHS)
      ENDIF
      DEALLOCATE (mumps_par, STAT=IERR)
      IF (IERR .NE. 0) GOTO 9900
      NULLIFY(F_MMPS_MATS(IMAT)%mstruc)

C---     Désalloue la table RHS globale
      IF (ASSOCIATED(mat%rhsg)) THEN
         DEALLOCATE (mat%rhsg, STAT=IERR)
         IF (IERR .NE. 0) GOTO 9900
         NULLIFY(mat%rhsg)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  IERR = F_MMPS_ERR_FTN      ! Erreur FTN
      GOTO 9999
9901  IERR = F_MMPS_ERR_MPI      ! Erreur MPI
      GOTO 9999
9902  IERR = F_MMPS_ERR_MMPS     ! Erreur MUMPS
      GOTO 9999

9999  CONTINUE
      F_MMPS_MATS(IMAT)%ierr = IERR
      F_MMPS_MAT_DESTROY_M = IERR
      RETURN
      END FUNCTION F_MMPS_MAT_DESTROY_M

C************************************************************************
C Sommaire:   Phase d'analyse d'une matrice distribuée MUMPS.
C
C Description:
C     La fonction <code>F_MMPS_MAT_ANALYSE(...)</code> analyse la matrice
C     distribuée décrite par les paramètres.
C
C Entrée:
C     HMAT        Handle sur l'objet
C     NEQL        Nombre d'équations local
C     NEQT        Nombre d'équations global
C     NNZ         Nombre de termes non nuls
C     IAP         Table cumulative des pointeurs aux indices en j
C     JAP         Table des indices en j
C     KLCGL       Table local-global
C
C Sortie:
C
C Notes:
C    On n'arrête pas pour un warning car on utilise des indices non
C    valides (0) dans jcn_loc pour éviter d'ajouter plusieurs fois
C    les mêmes entrées (car on a des noeuds partagés).
C
C    Voir commentaires options dans la fonction F_MMPS_SOLVE
C
C-DEBUT OPTION2
C     Sécuriser les appels à MPI (vérifier le code d'erreur)
C     Factoriser la création des types dans une fonction?
C     Vérifier pourquoi il est nécessaire d'allouer de l'espace pour
C     KCOLGEX, KNEQ et UNS même pour les process non master (les variables
C     ne sont pas utilisées).
C-FIN OPTION2
C
C************************************************************************
      FUNCTION F_MMPS_MAT_ANALIZE_M(HMAT,
     &                              NEQL,
     &                              NEQT,
     &                              NNZ,
     &                              IAP,
     &                              JAP,
     &                              KLCGL)

      INTEGER F_MMPS_MAT_ANALIZE_M
      INTEGER HMAT
      INTEGER NEQL
      INTEGER NEQT
      INTEGER NNZ
      INTEGER IAP  (NEQL+1)
      INTEGER JAP  (NNZ)
      INTEGER KLCGL(NEQL)

      include 'c_os.fi'
      include 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER IMAT
      INTEGER IR, IG, IL, NC
      TYPE (DMUMPS_STRUC),   POINTER :: mumps_par
      INTEGER, DIMENSION(:), POINTER :: irn_loc
      INTEGER, DIMENSION(:), POINTER :: jcn_loc
      CHARACTER*(256) BUF

C-DEBUT OPTION2
!       INCLUDE 'log.fi'
!       INCLUDE 'mpif.h'
!       INTEGER IP, I_SIZE, I_ERROR, I_TAG, NEQMAX
!       INTEGER, DIMENSION(:), POINTER :: types, KNEQ, UNS
!       PARAMETER (I_TAG = 123)
!       INTEGER, DIMENSION(:,:), POINTER :: KCOLGEX
C-FIN OPTION2
C------------------------------------------------------------------------
D     CALL ERR_PRE(F_MMPS_HVALIDE(HMAT))
D     CALL ERR_PRE(NEQL .GT. 0)
D     CALL ERR_PRE(NEQT .GE. NEQL)
D     CALL ERR_PRE(NNZ  .GE. NEQL)
D     CALL ERR_PRE( ASSOCIATED(F_MMPS_MATS) )
C------------------------------------------------------------------------

      IERR = F_MMPS_ERR_OK

C---     Récupère les attributs
      IMAT = HMAT - F_MMPS_HBASE
D     CALL ERR_ASR( ASSOCIATED(F_MMPS_MATS(IMAT)%mstruc) )
      mumps_par => F_MMPS_MATS(IMAT)%mstruc
D     CALL ERR_ASR(mumps_par%NZ_loc .EQ. NNZ)

C---     Alloue la mémoire
      IF (.NOT. ASSOCIATED(mumps_par%irn_loc)) THEN
         ALLOCATE (mumps_par%irn_loc(NNZ), STAT=IERR)
         IF (IERR .NE. 0) GOTO 9900
         mumps_par%irn_loc(:) = 0
      ENDIF
      IF (.NOT. ASSOCIATED(mumps_par%jcn_loc)) THEN
         ALLOCATE (mumps_par%jcn_loc(NNZ), STAT=IERR)
         IF (IERR .NE. 0) GOTO 9900
         mumps_par%jcn_loc(:) = -1
      ENDIF
      irn_loc => mumps_par%irn_loc
      jcn_loc => mumps_par%jcn_loc

C---     Monte la table IRN en numérotation globale
      IG = 0
      DO IL=1,NEQL
         NC = IAP(IL+1) - IAP(IL)
         IF (NC .GT. 0) THEN
            irn_loc(IG+1:IG+NC) = KLCGL(IL)
            IG = IG + NC
         ENDIF
      ENDDO
D     CALL ERR_ASR(IG .EQ. NNZ)

C---     Monte la table JCN en numérotation globale
      jcn_loc(1:NNZ) = KLCGL(JAP(1:NNZ))

C---     Dump to file
      IRET = C_OS_REQVARENV('H2D2_MUMPS_DUMPMTX', BUF)
      IF (IRET .EQ. 0) THEN
         mumps_par%WRITE_PROBLEM = BUF(1:SP_STRN_LEN(BUF))
      ENDIF

C---    Analyse
      mumps_par%JOB = 1
      mumps_par%ICNTL( 5) = 0    ! Input in assembled form
      mumps_par%ICNTL( 7) = 7    ! Seq ord (3,5,7 = Scotch,Metis,Auto)
      mumps_par%ICNTL(18) = 3    ! Distributed data
      mumps_par%ICNTL(28) = 2    ! Par ou Seq (0,1,2 = Auto,Seq,Par)
      mumps_par%ICNTL(29) = 0    ! Par ord (0,1,2 = Auto,PT-Scotch,ParMetis)
      mumps_par%N      = NEQT
      mumps_par%NZ_loc = NNZ
      CALL DMUMPS(mumps_par)

C---     Gère les erreurs
      IF (mumps_par%INFOG(1) .LT. 0) GOTO 9902
      IF (IERR .NE. 0) GOTO 9900

C-DEBUT OPTION2
!       CALL LOG_TODO('F_MMPS_MAT_ANALYZE: Sécuriser les appels à MPI')
! C---     Envoie les tables de localisation au master et commit
! C        les types MPI pour la synchronisation du vecteur global
!       types   => F_MMPS_MATS(IMAT)%types
!       KCOLGEX => F_MMPS_MATS(IMAT)%KCOLGEX
!       KNEQ    => F_MMPS_MATS(IMAT)%KNEQ
!       UNS     => F_MMPS_MATS(IMAT)%UNS
! D     IF(mumps_par%myid .EQ. 0) CALL ERR_ASR(ASSOCIATED(types))
!
!       CALL MPI_COMM_SIZE(mumps_par%COMM, I_SIZE, I_ERROR)
!       IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!
! C---     Va chercher table du nombre d'équations
!       IF (mumps_par%myid .EQ. 0) THEN
!          ALLOCATE(KNEQ(I_SIZE), STAT=IERR)
!       ELSE
!          ALLOCATE(KNEQ(1), STAT=IERR)
!       ENDIF
!
!       CALL MPI_GATHER(NEQ, 1, MP_TYPE_INT(), KNEQ, 1, MP_TYPE_INT(), 0,
!      &                mumps_par%COMM, I_ERROR)
!         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!
! C---     Alloue la table de localisation des process externes
! C        et la table de longueurs de blocs pour MPI
!       IF (mumps_par%myid .EQ. 0) THEN
!          NEQMAX = MAXVAL(KNEQ)
!          ALLOCATE(KCOLGEX(NEQMAX,(I_SIZE-1)))
!          ALLOCATE(UNS(NEQMAX))
!          UNS(:) = 1
!       ELSE
!          ALLOCATE(KCOLGEX(1,1))
!          ALLOCATE(UNS(1))
!       ENDIF
!
!       IF (mumps_par%myid .NE. 0) THEN
!          CALL MPI_SEND(KLCGL, NEQ, MP_TYPE_INT(), 0, I_TAG,
!      &                 mumps_par%COMM, I_ERROR)
!         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!       ELSE
!          DO IP=1,(I_SIZE-1)
!             CALL MPI_RECV(KCOLGEX(1,IP), KNEQ(IP+1),
!      &                    MP_TYPE_INT(), IP, I_TAG,
!      &                    mumps_par%COMM, I_ERROR)
!         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!             KCOLGEX(:,IP) = KCOLGEX(:,IP) - 1
!             CALL MPI_TYPE_INDEXED(KNEQ(IP+1),
!      &                            UNS,
!      &                            KCOLGEX(1,IP),
!      &                            MP_TYPE_RE8(),
!      &                            types(IP),
!      &                            I_ERROR)
!         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!             CALL MPI_TYPE_COMMIT(types(IP), I_ERROR)
!         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!          ENDDO
!       ENDIF
C-FIN OPTION2

      GOTO 9999
C------------------------------------------------------------------------
9900  IERR = F_MMPS_ERR_FTN      ! Erreur FTN
      GOTO 9999
9901  IERR = F_MMPS_ERR_MPI      ! Erreur MPI
      GOTO 9999
9902  IERR = F_MMPS_ERR_MMPS     ! Erreur MUMPS
      GOTO 9999

9999  CONTINUE
      F_MMPS_MATS(IMAT)%ierr = IERR
      F_MMPS_MAT_ANALIZE_M = IERR
      RETURN
      END FUNCTION F_MMPS_MAT_ANALIZE_M

C************************************************************************
C Sommaire:   Factorise une matrice distribuée MUMPS
C
C Description:
C    La fonction <code>F_MMPS_MAT_FACT(...)</code> factorise une matrice
C    distribuée MUMPS.
C
C Entrée:
C     HMAT        Handle sur l'objet
C     NNZ         Nombre de termes non nuls (local)
C     VKG         Tables des valeurs
C
C Sortie:
C
C Notes:
C     Dans au moins un cas (problème de manning avec sv2d), MUMPS
C     nécessite beaucoup plus de mémoire à la factorisation que ce qui a
C     été estimé à l'analyse. Nous avons donc mis une boucle autour de
C     la factorisation pour réessayer avec de plus en plus de mémoire.
C     Cela règle le problème pour de petits maillages, mais pour les
C     plus gros, ça n'est toujours pas assez, même qu'on peut faire
C     sauter la mémoire.
C
C************************************************************************
      FUNCTION F_MMPS_MAT_FACT_M(HMAT,
     &                           NNZ,
     &                           VKG)

      INTEGER F_MMPS_MAT_FACT_M
      INTEGER HMAT
      INTEGER NNZ
      REAL*8, TARGET :: VKG(NNZ)

      include 'mpif.h'

      INTEGER IERR
      INTEGER IMAT
      INTEGER COMPTE, I_ERROR
      LOGICAL MEMINS, ENCORE
      TYPE (DMUMPS_STRUC), POINTER :: mumps_par
C------------------------------------------------------------------------
D     CALL ERR_PRE(F_MMPS_HVALIDE(HMAT))
D     CALL ERR_PRE(NNZ .GT. 0)
D     CALL ERR_PRE( ASSOCIATED(F_MMPS_MATS) )
C------------------------------------------------------------------------

      IERR = F_MMPS_ERR_OK

C---     Récupère la structure MUMPS
      IMAT = HMAT - F_MMPS_HBASE
      mumps_par => F_MMPS_MATS(IMAT)%MSTRUC
D     CALL ERR_ASR(mumps_par%NZ_loc .EQ. NNZ)

C---    Factorise
      mumps_par%A_loc => VKG
      mumps_par%JOB = 2

      COMPTE = 0
1000  CONTINUE
         COMPTE = COMPTE + 1
         CALL DMUMPS(mumps_par)
         IF (mumps_par%INFOG(1) .EQ. 0 .OR. COMPTE .GT. 5) GOTO 1999

C---        Gère un potentiel manque de mémoire
C---        Si un process manque de mémoire, demande plus et recommence
         MEMINS = .FALSE.
         IF (mumps_par%INFO(1) .EQ. -8  .OR.
     &       mumps_par%INFO(1) .EQ. -9  .OR.
     &       mumps_par%INFO(1) .EQ. -17 .OR.
     &       mumps_par%INFO(1) .EQ. -20) MEMINS = .TRUE.

         CALL MPI_ALLREDUCE(MEMINS, ENCORE, 1, MPI_LOGICAL, MPI_LOR,
     &                      mumps_par%COMM, I_ERROR)
         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901

         mumps_par%ICNTL(14) = MIN(500, 2*mumps_par%ICNTL(14))
         IF (ENCORE) GOTO 1000
1999  CONTINUE

C---     Reset dump to file
      mumps_par%WRITE_PROBLEM = ' '

C---     Déconnecte VKG
!!      NULLIFY(mumps_par%A_loc)

C---     Gère les erreurs
      IF (mumps_par%INFOG(1) .NE. 0) GOTO 9902

      GOTO 9999
C------------------------------------------------------------------------
9900  IERR = F_MMPS_ERR_FTN      ! Erreur FTN
      GOTO 9999
9901  IERR = F_MMPS_ERR_MPI      ! Erreur MPI
      GOTO 9999
9902  IERR = F_MMPS_ERR_MMPS     ! Erreur MUMPS
      GOTO 9999

9999  CONTINUE
      F_MMPS_MATS(IMAT)%ierr = IERR
      F_MMPS_MAT_FACT_M = IERR
      RETURN
      END FUNCTION F_MMPS_MAT_FACT_M

C************************************************************************
C Sommaire:   Résous le système distribué MUMPS
C
C Description:
C     La fonction <code>F_MMPS_MAT_SOLVE(...)</code> résout le système
C     matriciel pour une matrice distribuée MUMPS.
C
C Entrée:
C     HMAT        Handle sur l'objet
C     NITR        Max steps of iterative refinement
C     NEQL        Nombre d'Equations Local
C     KLCGL       Table local-global
C
C Sortie:
C     VFG         Second membre et résultat
C
C Notes:
C     Vérifier pourquoi il est nécessaire d'allouer de l'espace pour VRHS
C     même pour les process non master (les variables ne sont pas
C     utilisées).
C
C     Pour actualiser la valeur de VFG (distribuer VRHS aux process), deux
C     options sont envisagées.
C     OPTION1: Tous les process allouent assez de mémoire pour recevoir le
C              vecteur global et le master fait un broadcast (nécessite
C              plus de mémoire). Ensuite chaque process utilise sa table
C              de localisation pour actualiser VFG.
C
C     OPTION2: à l'analyse, chaque process envoie sa table de localisation
C              au master, qui crée un type MPI indexé pour chaque process.
C              une fois la solution obtenue, le master envoie à chaque
C              process la partie de VRHS qu'il a besoin. Cette solution est
C              plus complexe au niveau du code et requiert plus de travail
C              et de mémoire de la part du master et requiert plus de
C              communication de la part du master. Cette option n'est pas
C              fonctionnelle (bug à la réception de KCOLGEX dans l'analyse).
C
C     Pour choisir l'option X, il suffit de dé-commenter les parties entre les
C     balises C-DEBUT OPTIONX et C-FIN OPTIONX et de commenter les parties
C     entres les autres balises. Il y a des modifications à faire dans:
C     f_mumps.fc, F_MMPS_MAT_SOLVE, F_MMPS_MAT_CREATE, F_MMPS_MAT_DESTROY,
C     F_MMPS_MAT_ANALYZE
C
C     Solution distribuée: dans tous les cas, rhs doit être d'abord centralisé,
C     la question est de savoir si la solution reste distribuée. La distribution
C     de la solution ne correspond pas à celle de la matrice. Il y a alors une
C     complexité certaine dans la redistribution. Il est plus simple de faire
C     un bcast et que chaque process MPI prenne ce qui l'intéresse.
C************************************************************************
      FUNCTION F_MMPS_MAT_SOLVE_M(HMAT,
     &                            NITR,
     &                            NEQL,
     &                            KLCGL,
     &                            VFG)

      INTEGER F_MMPS_MAT_SOLVE_M
      INTEGER HMAT
      INTEGER NITR
      INTEGER NEQL
      INTEGER KLCGL(NEQL)
      REAL*8, TARGET :: VFG(NEQL)

      include 'mpif.h'

      INTEGER I_ERROR
      INTEGER I_MASTER, I_COMM
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IMAT
      INTEGER IL, IG
      INTEGER NEQT
      TYPE (F_MMPS_MAT_T), POINTER :: mat
      TYPE (DMUMPS_STRUC), POINTER :: mumps_par

C-DEBUT OPTION2
!       INTEGER IP, I_SIZE, I_TAG, T_NEW, NEQMAX
!       INTEGER, DIMENSION(:), POINTER :: KCOLGEX, KNEQ, UNS
!       PARAMETER (I_TAG = 123)
!       INTEGER, DIMENSION(:), POINTER :: types
C-FIN OPTION2
C------------------------------------------------------------------------
D     CALL ERR_PRE(F_MMPS_HVALIDE(HMAT))
D     CALL ERR_ASR(NITR .GE. 0)
D     CALL ERR_ASR(NEQL .GT. 0)
D     CALL ERR_ASR(ASSOCIATED(F_MMPS_MATS))
C------------------------------------------------------------------------

      IERR = F_MMPS_ERR_OK

C---     Récupère la structure MUMPS
      IMAT = HMAT - F_MMPS_HBASE
      mat => F_MMPS_MATS(IMAT)
      mumps_par => mat%mstruc

C-DEBUT OPTION1
C---     Alloue la mémoire
      NEQT = mumps_par%N
      IF (.NOT. ASSOCIATED(mat%rhsg)) THEN
         ALLOCATE (mat%rhsg(NEQT), STAT=IERR)
         IF (IERR .NE. 0) GOTO 9900
      ENDIF
C-FIN OPTION1

C-DEBUT OPTION2
!       CALL LOG_TODO('F_MMPS_MAT_SOLVE: Option 2 non fonctionnelle')
!       types => F_MMPS_MATS(IMAT)%types
! D     IF(mumps_par%myid .EQ. 0) CALL ERR_ASR(ASSOCIATED(types))
!
! C---     Alloue la mémoire
!       IF (mumps_par%myid .EQ. 0) THEN
!          ALLOCATE (VRHS(NEQT), STAT=IERR)
!       ELSE
!          ALLOCATE (VRHS(1), STAT=IERR)
!       ENDIF
!       IF (IERR .NE. 0) GOTO 9900
C-FIN OPTION2

C---     Dans tous les cas, RHS est centralisé
      I_COMM = mumps_par%COMM
      IERR = F_MMPS_GTHRRHS(NEQL, NEQT, VFG, mat%rhsg, KLCGL, I_COMM)
      IF (IERR .NE. F_MMPS_ERR_OK) GOTO 9999

C---    Résous
      mumps_par%JOB = 3
      mumps_par%ICNTL(10) = NITR ! Max steps of iterative refinement
      mumps_par%ICNTL(11) = 0    ! Error analysis
      mumps_par%ICNTL(20) = 0    ! Dense centralized RHS
      mumps_par%ICNTL(21) = 0    ! Dense centralized solution (cf. note)
!!!   mumps_par%ICNTL(21) = 1    ! Dense distributed solution
      mumps_par%NRHS = 1
      mumps_par%RHS => mat%rhsg
      CALL DMUMPS(mumps_par)
      NULLIFY(mumps_par%RHS)

C---     Gère les erreurs
      IF (mumps_par%INFOG(1) .NE. 0) GOTO 9902

C-DEBUT OPTION2
! C---     Actualise VFG pour le master
!       IF (ERR_GOOD() .AND. mumps_par%myid .EQ. 0) THEN
!          VFG(1:NEQ) = VRHS(KLCGL(1:NEQ)) !! Attention, c.f. note
!       ENDIF
!
! C---     Actualise VFG pour les autres process
!       IF (mumps_par%myid .NE. 0) THEN
!          CALL MPI_RECV(VFG, NEQ, MP_TYPE_RE8(), 0, I_TAG,
!      &                 mumps_par%COMM, I_ERROR)
!         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!       ELSE
!          CALL MPI_COMM_SIZE(mumps_par%COMM, I_SIZE, I_ERROR)
!          DO IP=1,(I_SIZE-1)
!             CALL MPI_SEND(VRHS, 1, types(IP), IP, I_TAG, mumps_par%COMM,
!      &                    I_ERROR)
!         IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901
!          ENDDO
!       ENDIF
C-FIN OPTION2

C-DEBUT OPTION1
      IERR = F_MMPS_SCTRRHS(NEQL, NEQT, VFG, mat%rhsg, KLCGL, I_COMM)
      IF (IERR .NE. F_MMPS_ERR_OK) GOTO 9999
C-FIN OPTION1

      GOTO 9999
C------------------------------------------------------------------------
9900  IERR = F_MMPS_ERR_FTN      ! Erreur FTN
      GOTO 9999
9901  IERR = F_MMPS_ERR_MPI      ! Erreur MPI
      GOTO 9999
9902  IERR = F_MMPS_ERR_MMPS     ! Erreur MUMPS
      GOTO 9999

9999  CONTINUE
      F_MMPS_MATS(IMAT)%ierr = IERR
      F_MMPS_MAT_SOLVE_M = IERR
      RETURN
      END FUNCTION F_MMPS_MAT_SOLVE_M

C************************************************************************
C Sommaire:   Écris la matrice locale dans un fichier
C
C Description:
C     La fonction <code>F_MMPS_MAT_DMP(...)</code> écris la matrice locale
C     dans un fichier. Comme globalement les lignes ne sont pas partagées,
C     il est possible de concaténer les fichiers de chaque process, de trier
C     le résultat et de la comparer avec celui d'un seul process.
C
C Entrée:
C     HMAT        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION F_MMPS_MAT_DMP_M(HMAT)

      INTEGER F_MMPS_MAT_DMP_M
      INTEGER HMAT

      include 'dbg.fi'

      INTEGER IERR
      INTEGER I
      INTEGER IMAT
      INTEGER MP
      INTEGER NNZ
      INTEGER, DIMENSION(:), POINTER :: irn
      INTEGER, DIMENSION(:), POINTER :: jcn
      REAL*8,  DIMENSION(:), POINTER :: val
      TYPE (DMUMPS_STRUC), POINTER :: mumps_par
C------------------------------------------------------------------------
D     CALL ERR_PRE(F_MMPS_HVALIDE(HMAT))
C------------------------------------------------------------------------

      IERR = F_MMPS_ERR_OK

C---     Récupère la structure MUMPS
      IMAT = HMAT - F_MMPS_HBASE
      mumps_par => F_MMPS_MATS(IMAT)%MSTRUC

C---     Récupère les données
      nnz = mumps_par%NZ_loc
      irn => mumps_par%irn_loc
      jcn => mumps_par%jcn_loc
      val => mumps_par%A_loc

C---     Écris la matrice en format composantes
      mp = 0
      ierr = dbg_ouvre(mp)
      do i=1,nnz
         write(mp,'(2I12,1PE25.17E3)') irn(i), jcn(i), val(i)
      enddo
      if (mp .ne. 0) close(mp)

      F_MMPS_MAT_DMP_M = IERR
      RETURN
      END FUNCTION F_MMPS_MAT_DMP_M

      END MODULE F_MUMPS_M



C************************************************************************
C     Interface F77 au code F90
C************************************************************************
      FUNCTION F_MMPS_INITIALIZE ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: F_MMPS_INITIALIZE
CDEC$ ENDIF
      USE F_MUMPS_M
      INCLUDE 'f_mumps.fi'
      F_MMPS_INITIALIZE = F_MMPS_INITIALIZE_M()
      RETURN
      END
C------------------------------------------------------------------------
      FUNCTION F_MMPS_FINALIZE ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: F_MMPS_FINALIZE
CDEC$ ENDIF
      USE F_MUMPS_M
      INCLUDE 'f_mumps.fi'
      F_MMPS_FINALIZE = F_MMPS_FINALIZE_M()
      RETURN
      END
C------------------------------------------------------------------------
      FUNCTION F_MMPS_ERRMSG(HMAT, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: F_MMPS_ERRMSG
CDEC$ ENDIF
      USE F_MUMPS_M
      INTEGER       HMAT
      CHARACTER*(*) MSG
      INCLUDE 'f_mumps.fi'
      F_MMPS_ERRMSG = F_MMPS_ERRMSG_M(HMAT, MSG)
      RETURN
      END
C------------------------------------------------------------------------
      FUNCTION F_MMPS_MAT_CREATE(HMAT, NNZ, ICOMM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: F_MMPS_MAT_CREATE
CDEC$ ENDIF
      USE F_MUMPS_M
      INTEGER HMAT
      INTEGER NNZ
      INTEGER ICOMM
      INCLUDE 'f_mumps.fi'
      F_MMPS_MAT_CREATE = F_MMPS_MAT_CREATE_M(HMAT, NNZ, ICOMM)
      RETURN
      END
C------------------------------------------------------------------------
      FUNCTION F_MMPS_MAT_DESTROY(HMAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: F_MMPS_MAT_DESTROY
CDEC$ ENDIF
      USE F_MUMPS_M
      INTEGER HMAT
      INCLUDE 'f_mumps.fi'
      F_MMPS_MAT_DESTROY = F_MMPS_MAT_DESTROY_M(HMAT)
      RETURN
      END
C------------------------------------------------------------------------
      FUNCTION F_MMPS_MAT_ANALIZE(HMAT,
     &                            NEQL,
     &                            NEQT,
     &                            NNZ,
     &                            IAP,
     &                            JAP,
     &                            KLCGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: F_MMPS_MAT_ANALIZE
CDEC$ ENDIF
      USE F_MUMPS_M
      INTEGER HMAT
      INTEGER NEQL
      INTEGER NEQT
      INTEGER NNZ
      INTEGER IAP  (NEQL+1)
      INTEGER JAP  (NNZ)
      INTEGER KLCGL(NEQL)
      INCLUDE 'f_mumps.fi'
      F_MMPS_MAT_ANALIZE = F_MMPS_MAT_ANALIZE_M(HMAT,NEQL,NEQT,
     &                                          NNZ,IAP,JAP,KLCGL)
      RETURN
      END
C------------------------------------------------------------------------
      FUNCTION F_MMPS_MAT_FACT(HMAT, NNZ, VKG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: F_MMPS_MAT_FACT
CDEC$ ENDIF
      USE F_MUMPS_M
      INTEGER HMAT
      INTEGER NNZ
      REAL*8  VKG(NNZ)
      INCLUDE 'f_mumps.fi'
      F_MMPS_MAT_FACT = F_MMPS_MAT_FACT_M(HMAT, NNZ, VKG)
      RETURN
      END
C------------------------------------------------------------------------
      FUNCTION F_MMPS_MAT_SOLVE(HMAT, NITR, NEQL, KLCGL, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: F_MMPS_MAT_SOLVE
CDEC$ ENDIF
      USE F_MUMPS_M
      INTEGER HMAT
      INTEGER NITR
      INTEGER NEQL
      INTEGER KLCGL(NEQL)
      REAL*8  VFG(NEQL)
      INCLUDE 'f_mumps.fi'
      F_MMPS_MAT_SOLVE = F_MMPS_MAT_SOLVE_M(HMAT,NITR,NEQL,KLCGL,VFG)
      RETURN
      END
C------------------------------------------------------------------------
      FUNCTION F_MMPS_MAT_DMP(HMAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: F_MMPS_MAT_DMP
CDEC$ ENDIF
      USE F_MUMPS_M
      INTEGER HMAT
      INCLUDE 'f_mumps.fi'
      F_MMPS_MAT_DMP = F_MMPS_MAT_DMP_M(HMAT)
      RETURN
      END
