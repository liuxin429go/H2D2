C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_L2_CLCJELV
C
C Description:
C     La fonction LMGO_L2_CLCJELV calcule les métriques pour des
C     éléments de volume de type L2.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_L2_CLCJELV(NDIM,
     &                         NNT,
     &                         NCELV,
     &                         NELV,
     &                         NDJV,
     &                         VCORG,
     &                         KNGV,
     &                         VDJV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_L2_CLCJELV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDIM
      INTEGER NNT
      INTEGER NCELV
      INTEGER NELV
      INTEGER NDJV
      REAL*8  VCORG(NDIM,  NNT)
      INTEGER KNGV (NCELV, NELV)
      REAL*8  VDJV (NDJV,  NELV)

      INCLUDE 'lmgo_l2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IE
      INTEGER NO1,  NO2

      REAL*8 ZERO
      PARAMETER (ZERO = 0.0000000000000000000D0)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNT   .GE. 2)
D     CALL ERR_PRE(NCELV .EQ. 2)
C-----------------------------------------------------------------------

      CALL LOG_TODO('LMGO_L2_CLCJELV jamais teste, SUREMENT FAUX!!!')
      CALL ERR_ASR(.FALSE.)

C---     BOUCLE SUR LES ÉLÉMENTS
      DO IE=1,NELV
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         VDJV(1,IE) = VCORG(2,NO1)
         VDJV(2,IE) = VCORG(2,NO1)

C---        IMPRIME LES ELEMENTS A DETERMINANT NEGATIF
         IF (VDJV(2,IE) .LE. ZERO) THEN
            WRITE(ERR_BUF, '(A)') 'ERR_DETJ_NEGATIF'
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
            WRITE(LOG_BUF, '(A,I9)') 'ERR_DETJ_NEGATIF:', IE
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
      ENDDO

      LMGO_L2_CLCJELV = ERR_TYP()
      RETURN
      END

