C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER GP_CLCL_000
C     INTEGER GP_CLCL_999
C     INTEGER GP_CLCL_CTR
C     INTEGER GP_CLCL_DTR
C     INTEGER GP_CLCL_INI
C     INTEGER GP_CLCL_RST
C     INTEGER GP_CLCL_REQHBASE
C     LOGICAL GP_CLCL_HVALIDE
C     INTEGER GP_CLCL_XEQ
C   Private:
C     SUBROUTINE GP_CLCL_REQSELF
C     INTEGER GP_CLCL_RAZ
C
C************************************************************************

!Chaque tâche GPU est responsable d'assembler des noeuds (lignes) consécutives de la matrice.
!Elle a toute l'information pour y arriver. La charge de travaille est répartie en équilibrant
!le nombre d'éléments par tâche GPU.
!
! Les tâches sont toutes synchrones (SIMD). Elles travaillent sur des blocs de lignes distincts
! et donc sont complètement indépendantes, sans possibilité de race.
!
!Chaque tâche OMP gère un certains nombre de blocs de tâches GPU, qui globalement représente
!des noeuds (lignes) consécutives de la matrice. Chaque tâche OMP pagine le travail, en fonction
!de la taille de la mémoire globale du device GPU.
!
!L'information est transférée de table nodales vers des tables élémentaires. Il y a répétition
!de l'information des noeuds communs. De plus, pour avoir des accès mémoire coalescents,
!les informations des tâches se suivent:
!   VDLG_P(1:NDLN, IN, ) = VDLG(1:NDLN, KNE(IN))
!
!


! Le module GP_CLCL_M correspond au travail global par tous les OMP
      MODULE GP_CLCL_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: GP_CLCL_HBASE = 0

C---     Type de donnée associé à la classe
      INTEGER, PARAMETER :: NCL1_MAX = 16
      TYPE :: GP_CLCL_SELF_T
         INTEGER HCNF            ! Handle sur la configuration
         INTEGER NCL1            ! Nombre de tâcherons
         INTEGER HCL1(NCL1_MAX)
      END TYPE GP_CLCL_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée GP_CLCL_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (GP_CLCL_SELF_T), POINTER :: GP_CLCL_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (GP_CLCL_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      GP_CLCL_REQSELF => SELF
      RETURN
      END FUNCTION GP_CLCL_REQSELF

      END MODULE GP_CLCL_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction GP_CLCL_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLCL_000
CDEC$ ENDIF

      USE GP_CLCL_M
      IMPLICIT NONE

      INCLUDE 'gpclcl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(GP_CLCL_HBASE, 'GPU - Calculator')

      GP_CLCL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction GP_CLCL_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLCL_999
CDEC$ ENDIF

      USE GP_CLCL_M
      IMPLICIT NONE

      INCLUDE 'gpclcl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL GP_CLCL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(GP_CLCL_HBASE, GP_CLCL_DTR)

      GP_CLCL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction GP_CLCL_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLCL_CTR
CDEC$ ENDIF

      USE GP_CLCL_M
      USE ISO_C_BINDING

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gpclcl.fc'

      INTEGER IERR, IRET
      TYPE (GP_CLCL_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   GP_CLCL_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = GP_CLCL_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. GP_CLCL_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_CLCL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction GP_CLCL_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLCL_DTR
CDEC$ ENDIF

      USE GP_CLCL_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclcl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      TYPE (GP_CLCL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CLCL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = GP_CLCL_RST(HOBJ)

C---     Dé-alloue
      SELF => GP_CLCL_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, GP_CLCL_HBASE)

      GP_CLCL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée GP_CLCL_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_RAZ(HOBJ)

      USE GP_CLCL_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpclcl.fc'

      TYPE (GP_CLCL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CLCL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les paramètres
      SELF => GP_CLCL_REQSELF(HOBJ)
      SELF%HCNF = 0

      GP_CLCL_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction GP_CLCL_INI initialise l'objet à l'aide des paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_INI(HOBJ, HCNF, HDST, GDTA, EDTA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLCL_INI
CDEC$ ENDIF

      USE GP_CLCL_M
      USE LM_GDTA_M
      USE LM_EDTA_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCNF      ! Handle sur la configuration
      INTEGER HDST      ! Handle sur la distribution
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA

      INCLUDE 'gpclcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpclc1.fi'
      INCLUDE 'gpconf.fi'
      INCLUDE 'gpdevc.fi'

      INTEGER OMP_GET_MAX_THREADS

      INTEGER IERR
      INTEGER I, IOMP, ICPU, ITSK
      INTEGER HDEV(NCL1_MAX)
      INTEGER HCL1(NCL1_MAX)
      INTEGER HCLC
      INTEGER NOMP, NTSK
      TYPE (GP_CLCL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CLCL_HVALIDE(HOBJ))
D     CALL ERR_PRE(GP_CONF_HVALIDE(HCNF))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = GP_CLCL_RST(HOBJ)

C---     Dimension
      NOMP = GP_CONF_REQNCPU(HCNF)
!$    CALL ERR_ASR(NOMP .LE. OMP_GET_MAX_THREADS ())
D     CALL ERR_ASR(NOMP .LE. NCL1_MAX)

C---     Gather les devices de chaque OMP
      DO IOMP=1,NOMP
         HDEV(IOMP) = GP_CONF_REQHDEV(HCNF, -1, IOMP)
      ENDDO

C---     Construis les calculateurs
!$omp parallel num_threads(NOMP)
!$omp do private(i, itsk, icpu)
      DO IOMP=1,NOMP
         ICPU = 1
         ITSK = 1
         DO I=1,IOMP-1
            IF (HDEV(I) .EQ. HDEV(IOMP)) ICPU = ICPU + 1
         ENDDO
         IERR = GP_CLC1_CTR(HCLC)
         IERR = GP_CLC1_INI(HCLC,
     &                      HDST, ITSK,
     &                      HDEV(IOMP), ICPU,
     &                      GDTA, EDTA)
         HCL1(IOMP) = HCLC
         NTSK = GP_DEVC_REQNTSK(HDEV(IOMP))/GP_DEVC_REQNCPU(HDEV(IOMP))
         ITSK = ITSK + NTSK
      ENDDO
!$omp end do
!$omp end parallel

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF => GP_CLCL_REQSELF(HOBJ)
         SELF%NCL1 = NOMP
         SELF%HCL1(:) = HCL1(1:NOMP)
      ENDIF

      GP_CLCL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLCL_RST
CDEC$ ENDIF

      USE GP_CLCL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpconf.fi'
      INCLUDE 'gpclcl.fc'

      INTEGER IERR
      TYPE (GP_CLCL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CLCL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => GP_CLCL_REQSELF(HOBJ)

C---     Désalloue la mémoire
      IF (GP_CONF_HVALIDE(SELF%HCNF)) IERR = GP_CONF_DTR(SELF%HCNF)
D     CALL LOG_TODO('DELETE Calculators')

C---     Reset les paramètres
      IERR = GP_CLCL_RAZ(HOBJ)

      GP_CLCL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GP_CLCL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLCL_REQHBASE
CDEC$ ENDIF

      USE GP_CLCL_M
      IMPLICIT NONE

      INTEGER GP_CLCL_REQHBASE
C------------------------------------------------------------------------

      GP_CLCL_REQHBASE = GP_CLCL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GP_CLCL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLCL_HVALIDE
CDEC$ ENDIF

      USE GP_CLCL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclcl.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      GP_CLCL_HVALIDE = OB_OBJN_HVALIDE(HOBJ, GP_CLCL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: GP_CLCL_XEQ
C
C Description:
C     Transfert les données en page.
C     Les données des tables nodales sont copiés
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HAPP     Handle sur l'approximation (élément)
C     VRES     Table du résidu
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_XEQ(HOBJ, FNC, XARGS, VARGS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLCL_XEQ
CDEC$ ENDIF

      USE GP_CLCL_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   EXTERNAL   :: FNC
      INTEGER*8, INTENT(IN) :: XARGS(*)
      REAL*8,    INTENT(IN) :: VARGS(*)

      INCLUDE 'gpclcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpclc1.fi'

      INTEGER IERR
      INTEGER ICL
      INTEGER NCL1
      INTEGER HCL1
      TYPE (GP_CLCL_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
C     CALL ERR_PRE(GP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => GP_CLCL_REQSELF(HOBJ)

C---     Appel les calculateurs
      NCL1 = SELF%NCL1
!$omp  parallel num_threads(NCL1)
!$omp& default(shared)
!$omp& private(IERR)

!$omp  do private (hcl1)
      DO ICL=1, NCL1
         HCL1 = SELF%HCL1(ICL)
         IERR = GP_CLC1_XEQ(HCL1, FNC, XARGS, VARGS)
      ENDDO
!$omp end do

      IERR = ERR_OMP_RDC()
!$omp end parallel

      GP_CLCL_XEQ = ERR_TYP()
      RETURN
      END
