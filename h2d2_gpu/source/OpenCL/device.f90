!---------------------------------------------------------------------------!
! Copyright (c) 2013 Kyle Lutz <kyle.r.lutz@gmail.com>
!
! Distributed under the Boost Software License, Version 1.0
! See accompanying file LICENSE_1_0.txt or copy at
! http:!www.boost.org/LICENSE_1_0.txt
!
! See http:!boostorg.github.com/compute for more information.
!---------------------------------------------------------------------------!

! \class device
! \brief A compute device.
!
! Typical compute devices include GPUs and multi-core CPUs. A list
! of all compute devices available on a platform can be obtained
! via the platform::devices() method.
!
! The default compute device for the system can be obtained with
! the system::default_device() method. For example:
!
! \snippet test/test_device.cpp default_gpu
!
! \see platform, context, command_queue

MODULE OCL_DEVICE_M

   USE clfortran
   USE ISO_C_BINDING
   USE OCL_PLATFORM_M, ONLY : OCL_PLATFORM_T

   IMPLICIT NONE

   TYPE :: OCL_DEVICE_T
      INTEGER(C_INTPTR_T),  PRIVATE :: M_DEVICE = 0
      TYPE(OCL_PLATFORM_T), PRIVATE :: M_PLATFORM

      CONTAINS
      PROCEDURE :: GET
      PROCEDURE :: GET_PLATFORM
      PROCEDURE :: GET_ADDRESS_BITS
      PROCEDURE :: GET_CLOCK_FREQUENCY
      PROCEDURE :: GET_DRIVER_VERSION
      PROCEDURE :: GET_EXTENSIONS
      PROCEDURE :: GET_GLOBAL_MEM_SIZE
      PROCEDURE :: GET_LOCAL_MEMORY_SIZE
      PROCEDURE :: GET_MIN_WORK_ITEM_PER_WORK_GROUP
      PROCEDURE :: GET_MAX_COMPUTE_UNITS
      PROCEDURE :: GET_MAX_MEMORY_ALLOC_SIZE
      PROCEDURE :: GET_MAX_WORK_GROUP_SIZE
      PROCEDURE :: GET_MAX_WORK_ITEM_DIMENSIONS
      PROCEDURE :: GET_NAME
      PROCEDURE :: GET_PROFILE
      PROCEDURE :: GET_TYPE
      PROCEDURE :: GET_VENDOR
      PROCEDURE :: GET_VERSION

      PROCEDURE :: DO_SUPPORTS_EXTENSION
      PROCEDURE :: PRINT_DEV

      PROCEDURE, PRIVATE :: PRINT_BLN
      PROCEDURE, PRIVATE :: PRINT_I32
      PROCEDURE, PRIVATE :: PRINT_I64
      PROCEDURE, PRIVATE :: PRINT_ISZ
      PROCEDURE, PRIVATE :: PRINT_1NS
      PROCEDURE, PRIVATE :: PRINT_STR
   END TYPE OCL_DEVICE_T

   CONTAINS

!!!    enum type {
!!!        cpu = CL_DEVICE_TYPE_CPU,
!!!        gpu = CL_DEVICE_TYPE_GPU,
!!!        accelerator = CL_DEVICE_TYPE_ACCELERATOR
!!!    };

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Creates a new device object for \p id. If \p retain is \c true,
!!!    ! the reference count for the device will be incremented.
!
! Entrée:
!     PLTF     Platform
!     IDX      Device index
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_DEVICE_CTR(SELF, PLTF, IDX)

   USE OCL_UTIL_M

   TYPE(OCL_DEVICE_T),   INTENT(OUT) :: SELF
   TYPE(OCL_PLATFORM_T), INTENT(IN)  :: PLTF
   INTEGER, INTENT(IN) :: IDX

   INTEGER :: IRET
   INTEGER(C_INT) :: NIDS
   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INT32_T) :: V32
   INTEGER(C_INT64_T) :: INFO64
   INTEGER(C_INTPTR_T):: PID
   INTEGER(C_INTPTR_T), ALLOCATABLE :: DIDS(:)
   TYPE(OCL_DEVICE_T) :: SELF0
!---------------------------------------------------------------------

   PID = PLTF%GET()

   ! ---  Device count
   INFO64= CL_DEVICE_TYPE_ALL
   CLRET = clGetDeviceIDs(PID, INFO64, 0, C_NULL_PTR, NIDS)
   IF (CLRET .EQ. CL_SUCCESS) THEN
      IF (NIDS .LT.    1) CLRET = OCL_INVALID_DEVICE_COUNT
      IF (IDX  .LE.    0) CLRET = OCL_INVALID_DEVICE_INDEX
      IF (IDX  .GT. NIDS) CLRET = OCL_INVALID_DEVICE_INDEX
   ENDIF

   ! --- Get devices
   IF (CLRET .EQ. CL_SUCCESS) THEN
      ALLOCATE( DIDS(NIDS) )
      INFO64= CL_DEVICE_TYPE_ALL
      CLRET = clGetDeviceIDs(PID, INFO64, NIDS, C_LOC(DIDS), V32)
      IF (CLRET .EQ. CL_SUCCESS) THEN
         IF (V32 .LT. NIDS) CLRET = OCL_INVALID_DEVICE_COUNT
      ENDIF
   ENDIF

   ! ---  Assigne les attributs
   IF (CLRET .EQ. CL_SUCCESS) THEN
      SELF = SELF0
      SELF%m_platform = PLTF
      SELF%m_device   = DIDS(IDX)
   ENDIF

   ! ---  Désalloue la table des ID
   IF (ALLOCATED(DIDS)) DEALLOCATE(DIDS)

   OCL_DEVICE_CTR = CLRET
   RETURN
   END FUNCTION OCL_DEVICE_CTR

!************************************************************************
! Sommaire:
!
! Description:
    !!!! Destroys the device object.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_DEVICE_DTR(SELF)

   TYPE(OCL_DEVICE_T), INTENT(INOUT) :: SELF
!---------------------------------------------------------------------

   SELF%m_device   = 0

   OCL_DEVICE_DTR = CL_SUCCESS
   RETURN
   END FUNCTION OCL_DEVICE_DTR



!************************************************************************
! Sommaire:
!
! Description:
!    ! Returns the underlying OpenCL device (ID).
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INTPTR_T) FUNCTION GET(SELF)

   CLASS(OCL_DEVICE_T), INTENT(IN) :: SELF

   GET = SELF%m_device
   RETURN
   END FUNCTION GET

!************************************************************************
! Sommaire:
!
! Description:
!    ! Returns the platform associated to the device.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION GET_PLATFORM(SELF)

   TYPE(OCL_PLATFORM_T) :: GET_PLATFORM
   CLASS(OCL_DEVICE_T) :: SELF

   GET_PLATFORM = SELF%m_platform
   RETURN
   END FUNCTION GET_PLATFORM

!************************************************************************
! Sommaire:
!
! Description:
!    ! Returns the type of the device.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INT64_T) FUNCTION GET_TYPE(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_TYPE
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_TYPE

!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the name of the device.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION GET_NAME(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_NAME
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_NAME

!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the name of the vendor for the device.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION GET_VENDOR(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_VENDOR
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_VENDOR

!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the profile string for the device.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION GET_PROFILE(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_PROFILE
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_PROFILE

!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the version string for the device.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION GET_VERSION(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_VERSION
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_VERSION

!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the driver version string for the device.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION GET_DRIVER_VERSION(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DRIVER_VERSION
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_DRIVER_VERSION

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns a list of extensions supported by the device.
!        tab or space separated
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(1024) FUNCTION GET_EXTENSIONS(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_EXTENSIONS
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_EXTENSIONS


!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns \c true if the device supports the extension with
!!!    ! \p name.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   LOGICAL FUNCTION DO_SUPPORTS_EXTENSION(SELF, NAME) RESULT(VAL)

   CLASS(OCL_DEVICE_T) :: SELF
   CHARACTER*(*) :: NAME

   INTEGER IDX
!------------------------------------------------------------------------------

   IDX = INDEX( SELF%GET_EXTENSIONS(), NAME(1:LEN_TRIM(NAME)) )
   VAL = IDX .GT. 0
   RETURN
   END FUNCTION DO_SUPPORTS_EXTENSION

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the number of address bits.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INT32_T) FUNCTION GET_ADDRESS_BITS(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_ADDRESS_BITS
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_ADDRESS_BITS

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the global memory size in bytes.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INT64_T) FUNCTION GET_GLOBAL_MEM_SIZE(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_GLOBAL_MEM_SIZE
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_GLOBAL_MEM_SIZE

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the local memory size in bytes.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(c_int64_t) &
      FUNCTION GET_LOCAL_MEMORY_SIZE(SELF) &
      RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_LOCAL_MEM_SIZE
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_LOCAL_MEMORY_SIZE

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the clock frequency for the device's compute units.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INT32_T) &
      FUNCTION GET_CLOCK_FREQUENCY(SELF) &
      RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_MAX_CLOCK_FREQUENCY
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_CLOCK_FREQUENCY

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the number of compute units in the device.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INT32_T) &
      FUNCTION GET_MAX_COMPUTE_UNITS(SELF) &
      RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_MAX_COMPUTE_UNITS
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_MAX_COMPUTE_UNITS

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! \internal_
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(c_int64_t) &
      FUNCTION GET_MAX_MEMORY_ALLOC_SIZE(SELF) &
      RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_MAX_MEM_ALLOC_SIZE
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_MAX_MEMORY_ALLOC_SIZE

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! \internal_
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(c_int64_t) &
      FUNCTION GET_MAX_WORK_GROUP_SIZE(SELF) &
      RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_MAX_WORK_GROUP_SIZE
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_MAX_WORK_GROUP_SIZE

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! \internal_
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(c_int64_t) &
      FUNCTION GET_MAX_WORK_ITEM_DIMENSIONS(SELF) &
      RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   RETURN
   END FUNCTION GET_MAX_WORK_ITEM_DIMENSIONS

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! \internal_
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(c_int32_t) &
      FUNCTION GET_MIN_WORK_ITEM_PER_WORK_GROUP(SELF) &
      RESULT(VAL)

   USE OCL_UTIL_M

   CLASS(OCL_DEVICE_T) :: SELF

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INT32_T) :: INFO
!------------------------------------------------------------------------------

   VAL = 0
   INFO = CL_DEVICE_WARP_SIZE_NV
   CLRET  = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   IF (CLRET .NE. CL_SUCCESS) THEN
      VAL = 0
      INFO = CL_DEVICE_WAVEFRONT_WIDTH_AMD
      CLRET  = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VAL)
   ENDIF
   IF (CLRET .NE. CL_SUCCESS) THEN
      VAL = 1
   ENDIF

   RETURN
   END FUNCTION GET_MIN_WORK_ITEM_PER_WORK_GROUP

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns \c true if the device is a sub-device.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
!!!    bool is_subdevice() const
!!!    {
!!!    #if defined(CL_VERSION_1_2)
!!!        try {
!!!            return get_info<cl_device_id>(CL_DEVICE_PARENT_DEVICE) != 0;
!!!        }
!!!        catch(opencl_error&){
!!!            ! the get_info() call above will throw if the device's opencl version
!!!            ! is less than 1.2 (in which case it can't be a sub-device).
!!!            return false;
!!!        }
!!!    #else
!!!        return false;
!!!    #endif
!!!    }
!!!
!!!    ! Returns information about the device.
!!!    !
!!!    ! For example, to get the number of compute units:
!!!    ! \code
!!!    ! device.get_info<cl_uint>(CL_DEVICE_MAX_COMPUTE_UNITS);
!!!    ! \endcode
!!!    !
!!!    ! Alternatively, the template-specialized version can be used which
!!!    ! automatically determines the result type:
!!!    ! \code
!!!    ! device.get_info<CL_DEVICE_MAX_COMPUTE_UNITS>();
!!!    ! \endcode
!!!    !
!!!    ! \see_opencl_ref{clGetDeviceInfo}
!!!    template<class T>
!!!    T get_info(cl_device_info info) const
!!!    {
!!!        return detail::get_object_info<T>(clGetDeviceInfo, m_id, info);
!!!    }
!!!
!!!    ! \overload
!!!    template<int Enum>
!!!    typename detail::get_object_info_type<device, Enum>::type
!!!    get_info() const;
!!!
!!!    #if defined(CL_VERSION_1_2) || defined(BOOST_COMPUTE_DOXYGEN_INVOKED)
!!!    ! Partitions the device into multiple sub-devices according to
!!!    ! \p properties.
!!!    !
!!!    ! \opencl_version_warning{1,2}
!!!    std::vector<device>
!!!    partition(const cl_device_partition_property *properties) const
!!!    {
!!!        ! get sub-device count
!!!        INTEGER(C_INT32_T) count = 0;
!!!        int_ ret = clCreateSubDevices(m_id, properties, 0, 0, &count);
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        ! get sub-device ids
!!!        std::vector<cl_device_id> ids(count);
!!!        ret = clCreateSubDevices(m_id, properties, count, &ids[0], 0);
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        ! convert ids to device objects
!!!        std::vector<device> devices(count);
!!!        for(size_t i = 0; i < count; i++){
!!!            devices[i] = device(ids[i], false);
!!!        }
!!!
!!!        return devices;
!!!    }
!!!
!!!    ! \opencl_version_warning{1,2}
!!!    std::vector<device> partition_equally(size_t count) const
!!!    {
!!!        cl_device_partition_property properties[] = {
!!!            CL_DEVICE_PARTITION_EQUALLY,
!!!            static_cast<cl_device_partition_property>(count),
!!!            0
!!!        };
!!!
!!!        return partition(properties);
!!!    }
!!!
!!!    ! \opencl_version_warning{1,2}
!!!    std::vector<device>
!!!    partition_by_counts(const std::vector<size_t> &counts) const
!!!    {
!!!        std::vector<cl_device_partition_property> properties;
!!!
!!!        properties.push_back(CL_DEVICE_PARTITION_BY_COUNTS);
!!!        for(size_t i = 0; i < counts.size(); i++){
!!!            properties.push_back(
!!!                static_cast<cl_device_partition_property>(counts[i]));
!!!        }
!!!        properties.push_back(CL_DEVICE_PARTITION_BY_COUNTS_LIST_END);
!!!        properties.push_back(0);
!!!
!!!        return partition(&properties[0]);
!!!    }
!!!
!!!    ! \opencl_version_warning{1,2}
!!!    std::vector<device>
!!!    partition_by_affinity_domain(cl_device_affinity_domain domain) const
!!!    {
!!!        cl_device_partition_property properties[] = {
!!!            CL_DEVICE_PARTITION_BY_AFFINITY_DOMAIN,
!!!            static_cast<cl_device_partition_property>(domain),
!!!            0
!!!        };
!!!
!!!        return partition(properties);
!!!    }
!!!    #endif ! CL_VERSION_1_2
!!!
!!!    ! Returns \c true if the device is the same at \p other.
!!!    bool operator==(const device &other) const
!!!    {
!!!        return m_id == other.m_id;
!!!    }
!!!
!!!    ! Returns \c true if the device is different from \p other.
!!!    bool operator!=(const device &other) const
!!!    {
!!!        return m_id != other.m_id;
!!!    }
!!!
!!!    ! \internal_
!!!    bool check_version(int major, int minor) const
!!!    {
!!!        std::stringstream stream;
!!!        stream << version();
!!!
!!!        int actual_major, actual_minor;
!!!        stream.ignore(7); ! 'OpenCL '
!!!        stream >> actual_major;
!!!        stream.ignore(1); ! '.'
!!!        stream >> actual_minor;
!!!
!!!        return actual_major > major ||
!!!               (actual_major == major && actual_minor >= minor);
!!!    }
!!!
!!!private:
!!!    cl_device_id m_id;
!!!};
!!!
!!!! \internal_
!!!template<>
!!!inline INTEGER(C_INT32_T) device::preferred_vector_width<short_>() const
!!!{
!!!    return get_info<INTEGER(C_INT32_T)>(CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT);
!!!}
!!!
!!!! \internal_
!!!template<>
!!!inline INTEGER(C_INT32_T) device::preferred_vector_width<int_>() const
!!!{
!!!    return get_info<INTEGER(C_INT32_T)>(CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT);
!!!}
!!!
!!!! \internal_
!!!template<>
!!!inline INTEGER(C_INT32_T) device::preferred_vector_width<long_>() const
!!!{
!!!    return get_info<INTEGER(C_INT32_T)>(CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG);
!!!}
!!!
!!!! \internal_
!!!template<>
!!!inline INTEGER(C_INT32_T) device::preferred_vector_width<float_>() const
!!!{
!!!    return get_info<INTEGER(C_INT32_T)>(CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT);
!!!}
!!!
!!!! \internal_
!!!template<>
!!!inline INTEGER(C_INT32_T) device::preferred_vector_width<double_>() const
!!!{
!!!    return get_info<INTEGER(C_INT32_T)>(CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE);
!!!}
!!!

!************************************************************************
! Sommaire:
!
! Description:
!     La fonction privée PRINT_BLN imprime les données d'un device.
!
! Entrée:
!     DID      OpenCL handle on device to print-out.
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION PRINT_BLN(SELF, IUNIT, INFO, TXT)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T),INTENT(IN) :: SELF
   INTEGER, INTENT(IN) :: IUNIT
   INTEGER(C_INT32_T), INTENT(IN) :: INFO
   CHARACTER*(*),      INTENT(IN) :: TXT

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_SIZE_T)  :: LRET
   INTEGER(C_INT32_T) :: V32
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, V32)
   IF (CLRET .EQ. CL_SUCCESS) THEN
      WRITE(IUNIT,'(2A,L6)') TXT, ': ', (V32 .NE. 0)
   ELSEIF (CLRET .EQ. CL_INVALID_DEVICE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid device ---'
   ELSEIF (CLRET .EQ. CL_INVALID_VALUE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid value ---'
   ELSE
      WRITE(IUNIT,'(2A)') TXT,': --- Error querying device ---'
   ENDIF

   PRINT_BLN = CL_SUCCESS
   RETURN
   END

!************************************************************************
! Sommaire:
!
! Description:
!     La fonction privée PRINT_I32 imprime les données d'un device.
!
! Entrée:
!     DID      OpenCL handle on device to print-out.
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION PRINT_I32(SELF, IUNIT, INFO, TXT)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T),INTENT(IN) :: SELF
   INTEGER, INTENT(IN) :: IUNIT
   INTEGER(c_int32_t), INTENT(IN) :: INFO
   CHARACTER*(*),      INTENT(IN) :: TXT

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_SIZE_T)  :: LRET
   INTEGER(C_INT32_T) :: V32
!------------------------------------------------------------------------------

   V32 = 0
   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, V32)
   SELECT CASE(CLRET)
      CASE (CL_SUCCESS)
         WRITE(IUNIT,'(2A,I18)') TXT, ': ', V32
      CASE (CL_INVALID_DEVICE)
         WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid device ---'
      CASE (CL_INVALID_VALUE)
         WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid value ---'
      CASE DEFAULT
         WRITE(IUNIT,'(2A)') TXT,': --- Error querying device ---'
   END SELECT

   PRINT_I32 = CL_SUCCESS
   RETURN
   END FUNCTION PRINT_I32

!************************************************************************
! Sommaire:
!
! Description:
!     La fonction privée PRINT_I64 imprime les données d'un device.
!
! Entrée:
!     DID      OpenCL handle on device to print-out.
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION PRINT_I64(SELF, IUNIT, INFO, TXT)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T), INTENT(IN) :: SELF
   INTEGER, INTENT(IN) :: IUNIT
   INTEGER(c_int32_t), INTENT(IN) :: INFO
   CHARACTER*(*),      INTENT(IN) :: TXT

   INTEGER(C_INT32_T):: CLRET
   INTEGER(c_size_t) :: LRET
   INTEGER(c_int64_t):: V64
!------------------------------------------------------------------------------

   V64 = 0
   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, V64)
   IF (CLRET .EQ. CL_SUCCESS) THEN
      IF (V64 .LT. 0) THEN
         WRITE(IUNIT,'(2A,I18)') TXT, ': ', V64
      ELSEIF (V64 .LT. 2**12) THEN
         WRITE(IUNIT,'(2A,B16.16)') TXT, ': 0b', V64
      ELSE
         WRITE(IUNIT,'(2A,Z16.16)') TXT, ': 0x', V64
      ENDIF
   ELSEIF (CLRET .EQ. CL_INVALID_DEVICE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid device ---'
   ELSEIF (CLRET .EQ. CL_INVALID_VALUE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid value ---'
   ELSE
      WRITE(IUNIT,'(2A)') TXT,': --- Error querying device ---'
   ENDIF

   PRINT_I64 = CL_SUCCESS
   RETURN
   END FUNCTION PRINT_I64

!************************************************************************
! Sommaire:
!
! Description:
!     La fonction privée PRINT_ISZ imprime les données d'un device.
!
! Entrée:
!     DID      OpenCL handle on device to print-out.
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION PRINT_ISZ(SELF, IUNIT, INFO, TXT)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T),INTENT(IN) :: SELF
   INTEGER, INTENT(IN) :: IUNIT
   INTEGER(c_int32_t), INTENT(IN) :: INFO
   CHARACTER*(*),      INTENT(IN) :: TXT

   INTEGER(C_INT32_T) :: CLRET

   INTEGER, PARAMETER :: UN_KB = 1024
   INTEGER, PARAMETER :: UN_MB = 1024*UN_KB
   INTEGER, PARAMETER :: UN_GB = 1024*UN_MB

   INTEGER(c_size_t) :: LRET
   INTEGER(c_size_t) :: VSZ
!------------------------------------------------------------------------------

   VSZ = 0
   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VSZ)
   IF (CLRET .EQ. CL_SUCCESS) THEN
      IF (VSZ .GT. UN_MB) THEN
         WRITE(IUNIT,'(2A,F18.3,A)') TXT, ': ', DBLE(VSZ)/UN_GB, ' (GB)'
      ELSEIF (VSZ .GT. UN_KB) THEN
         WRITE(IUNIT,'(2A,F18.3,A)') TXT, ': ', DBLE(VSZ)/UN_KB, ' (KB)'
      ELSE
         WRITE(IUNIT,'(2A,I18)') TXT, ': ', VSZ
      ENDIF
   ELSEIF (CLRET .EQ. CL_INVALID_DEVICE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid device ---'
   ELSEIF (CLRET .EQ. CL_INVALID_VALUE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid value ---'
   ELSE
      WRITE(IUNIT,'(2A)') TXT,': --- Error querying device ---'
   ENDIF

   PRINT_ISZ = CL_SUCCESS
   RETURN
   END FUNCTION PRINT_ISZ

!************************************************************************
! Sommaire:
!
! Description:
!     La fonction privée PRINT_1NS imprime les données d'un device.
!
! Entrée:
!     DID      OpenCL handle on device to print-out.
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION PRINT_1NS(SELF, IUNIT, INFO, TXT)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T), INTENT(IN) :: SELF
   INTEGER, INTENT(IN) :: IUNIT
   INTEGER(c_int32_t), INTENT(IN) :: INFO
   CHARACTER*(*),      INTENT(IN) :: TXT

   INTEGER(C_INT32_T) :: CLRET

   INTEGER(c_size_t) :: LRET, LSIZ
   INTEGER(c_size_t), TARGET :: VSZ(3)
!------------------------------------------------------------------------------

   VSZ = 1
   LSIZ = c_size_t * 3
   CLRET = clGetDeviceInfo(SELF%GET(), INFO, LSIZ, C_LOC(VSZ), LRET)
   IF (CLRET .EQ. CL_SUCCESS) THEN
      WRITE(IUNIT,'(2A,3I6)') TXT, ': ', VSZ(1), VSZ(2), VSZ(3)
   ELSEIF (CLRET .EQ. CL_INVALID_DEVICE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid device ---'
   ELSEIF (CLRET .EQ. CL_INVALID_VALUE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid value ---'
   ELSE
      WRITE(IUNIT,'(2A)') TXT,': --- Error querying device ---'
   ENDIF

   PRINT_1NS = CL_SUCCESS
   RETURN
   END FUNCTION PRINT_1NS

!************************************************************************
! Sommaire:
!
! Description:
!     La fonction privée PRINT_STR imprime les données d'un device.
!
! Entrée:
!     DID      OpenCL handle on device to print-out.
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION PRINT_STR(SELF, IUNIT, INFO, TXT)

   USE OCL_UTIL_M, ONLY : OCL_INFO_DEVICE, GET_INFO

   CLASS(OCL_DEVICE_T), INTENT(IN) :: SELF
   INTEGER, INTENT(IN) :: IUNIT
   INTEGER(c_int32_t), INTENT(IN) :: INFO
   CHARACTER*(*),      INTENT(IN) :: TXT

   INTEGER, PARAMETER :: BUFSIZ = 1024
   INTEGER(C_INT32_T) :: CLRET
   INTEGER(c_size_t)  :: LRET
   INTEGER            :: LCH, IDX1, IDX2
   CHARACTER*(BUFSIZ) :: VCH
   CHARACTER*(16)     :: FMT
!------------------------------------------------------------------------------

   VCH = ' '
   CLRET = GET_INFO(SELF%GET(), OCL_INFO_DEVICE, INFO, VCH)

   IF (CLRET .EQ. CL_SUCCESS) THEN
      LCH = LEN_TRIM(VCH)
      IF (LCH .LT. 128) THEN
         WRITE(IUNIT,'(3A)') TXT,': ', VCH(1:LCH)
      ELSE
         LCH = LCH + 1
         IDX1 = 1
         IDX2 = SCAN(VCH(IDX1:LCH), ' ') - 1
         WRITE(IUNIT,'(3A)') TXT,': ', VCH(IDX1:IDX2)
         IDX1 = IDX2 + 2
         IDX2 = IDX1 + SCAN(VCH(IDX1:LCH), ' ') - 2
         WRITE(FMT,'(A,I0,A)') '(', LEN(TXT)+2, 'X,2A)'
         DO WHILE (IDX2 .GT. IDX1)
            WRITE(IUNIT,FMT) VCH(IDX1:IDX2)
            IDX1 = IDX2 + 2
            IDX2 = IDX1 + SCAN(VCH(IDX1:LCH), ' ') - 2
         ENDDO
      ENDIF
   ELSEIF (CLRET .EQ. CL_INVALID_DEVICE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid device ---'
   ELSEIF (CLRET .EQ. CL_INVALID_VALUE) THEN
      WRITE(IUNIT,'(2A)') TXT,': --- Error: Invalid value ---'
   ELSE
      WRITE(IUNIT,'(2A)') TXT,': --- Error querying device ---'
   ENDIF

   PRINT_STR = CL_SUCCESS
   RETURN
   END FUNCTION PRINT_STR

!************************************************************************
! Sommaire:
!
! Description:
!     La fonction publique PRINT_DEV imprime les données d'un device.
!
! Entrée:
!     DID      OpenCL handle on device to print-out.
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION PRINT_DEV(SELF, IUNIT, GLB_INDT)

   USE OCL_UTIL_M

   CLASS(OCL_DEVICE_T), INTENT(IN) :: SELF
   INTEGER, INTENT(IN) :: IUNIT
   INTEGER, INTENT(IN), OPTIONAL :: GLB_INDT

   INTEGER, PARAMETER :: UN_KB = 1024
   INTEGER, PARAMETER :: UN_MB = 1024*UN_KB
   INTEGER, PARAMETER :: UN_GB = 1024*UN_MB
   INTEGER, PARAMETER :: BUFSIZ = 256

   INTEGER :: INDT
   INTEGER(C_INT32_T) :: CLRET, IE
   INTEGER(C_SIZE_T)  :: LRET
   INTEGER(C_SIZE_T),  TARGET :: VSZ
   INTEGER(C_INT64_T), TARGET :: V64
   INTEGER(C_INT32_T), TARGET :: V32
   INTEGER(C_INT32_T) :: INFO32
   INTEGER(C_INTPTR_T):: DID

   CHARACTER(LEN=BUFSIZ) :: BUF
   CHARACTER(LEN=16) :: LEAD
!------------------------------------------------------------------------------
   INTEGER DI_BL, DI_32, DI_64, DI_SZ, DI_NS, DI_CH
   INTEGER(c_int32_t) :: I
   CHARACTER*(32)     :: Z
   CHARACTER*(30)     :: N
   Z(N) = LEAD(1:INDT) // N(1:LEN_TRIM(N))
   DI_BL(I, N) = SELF%PRINT_BLN(IUNIT, I, Z(N))
   DI_32(I, N) = SELF%PRINT_I32(IUNIT, I, Z(N))
   DI_64(I, N) = SELF%PRINT_I64(IUNIT, I, Z(N))
   DI_SZ(I, N) = SELF%PRINT_ISZ(IUNIT, I, Z(N))
   DI_NS(I, N) = SELF%PRINT_1NS(IUNIT, I, Z(N))
   DI_CH(I, N) = SELF%PRINT_STR(IUNIT, I, Z(N))
!------------------------------------------------------------------------------

   LEAD = ' '
   INDT = 0
   IF (PRESENT(GLB_INDT)) INDT = GLB_INDT
   DID = SELF%GET()

   ! ---  Info
   INFO32 = CL_DEVICE_NAME
   CLRET = GET_INFO(DID, OCL_INFO_DEVICE, INFO32, BUF)
   IF (CLRET .NE. CL_SUCCESS) GOTO 9999
   WRITE(IUNIT,'(3A)') LEAD(1:INDT),'MSG_NAME: ',BUF(1:LEN_TRIM(BUF))

   INFO32  = CL_DEVICE_TYPE
   CLRET = GET_INFO(DID, OCL_INFO_DEVICE, INFO32, V64)
   IF (CLRET .NE. CL_SUCCESS) GOTO 9999
   BUF = 'UNDEFINED'
   IF (V64 .EQ. CL_DEVICE_TYPE_DEFAULT)     BUF = 'TYPE_DEFAULT'
   IF (V64 .EQ. CL_DEVICE_TYPE_CPU)         BUF = 'TYPE_CPU'
   IF (V64 .EQ. CL_DEVICE_TYPE_GPU)         BUF = 'TYPE_GPU'
   IF (V64 .EQ. CL_DEVICE_TYPE_ACCELERATOR) BUF = 'TYPE_ACCEL'
   IF (V64 .EQ. CL_DEVICE_TYPE_CUSTOM)      BUF = 'TYPE_CUSTOM'
   IF (V64 .EQ. CL_DEVICE_TYPE_ALL)         BUF = 'TYPE_ALL'
   WRITE(IUNIT,'(3A)') LEAD(1:INDT),'MSG_TYPE: ',BUF(1:LEN_TRIM(BUF))

   INFO32 = CL_DEVICE_MAX_COMPUTE_UNITS
   CLRET = GET_INFO(DID, OCL_INFO_DEVICE, INFO32, V32)
   IF (CLRET .NE. CL_SUCCESS) GOTO 9999
   WRITE(IUNIT,'(2A,I6)') Z('MAX_COMPUTE_UNITS'), ': ',  V32

   INFO32 = CL_DEVICE_MAX_WORK_GROUP_SIZE
   CLRET = GET_INFO(DID, OCL_INFO_DEVICE, INFO32, VSZ)
   IF (CLRET .NE. CL_SUCCESS) GOTO 9999
   WRITE(IUNIT,'(2A,I6)') Z('MAX_WORK_GROUP_SIZE'), ': ', VSZ

   INFO32 = CL_DEVICE_GLOBAL_MEM_SIZE
   CLRET = clGetDeviceInfo(DID, INFO32, c_int64_t, C_LOC(V64), LRET)
   IF (CLRET .NE. CL_SUCCESS) GOTO 9999
   WRITE(IUNIT,'(2A,F12.3,A)') Z('GLOBAL_MEM_SIZE'), ': ', DBLE(V64)/UN_GB, ' (GB)'

   ! ---  Complete device info
   WRITE(IUNIT,'(2A)') Z('Complete device info'), ':'
   INDT = INDT + 3
   IE=DI_64(CL_DEVICE_TYPE, 'TYPE')
   IE=DI_32(CL_DEVICE_VENDOR_ID, 'VENDOR_ID')
   IE=DI_32(CL_DEVICE_MAX_COMPUTE_UNITS, 'MAX_COMPUTE_UNITS')
   IE=DI_32(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, 'MAX_WORK_ITEM_DIM')
   IE=DI_SZ(CL_DEVICE_MAX_WORK_GROUP_SIZE, 'MAX_WORK_GROUP_SIZE')
   IE=DI_NS(CL_DEVICE_MAX_WORK_ITEM_SIZES, 'MAX_WORK_ITEM_SIZES')
!      IE=DI_32(CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, 'PREF_VECTOR_WIDTH_CHAR')
!      IE=DI_32(CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, 'PREF_VECTOR_WIDTH_SHORT')
!      IE=DI_32(CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, 'PREF_VECTOR_WIDTH_INT')
!      IE=DI_32(CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, 'PREF_VECTOR_WIDTH_LONG')
!      IE=DI_32(CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, 'PREF_VECTOR_WIDTH_FLOAT')
!      IE=DI_32(CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, 'PREF_VECTOR_WIDTH_DOUBLE')
   IE=DI_32(CL_DEVICE_MAX_CLOCK_FREQUENCY, 'MAX_CLOCK_FREQUENCY')
   IE=DI_32(CL_DEVICE_ADDRESS_BITS, 'ADDRESS_BITS')
   IE=DI_32(CL_DEVICE_MAX_READ_IMAGE_ARGS, 'MAX_READ_IMAGE_ARGS')
   IE=DI_32(CL_DEVICE_MAX_WRITE_IMAGE_ARGS, 'MAX_WRITE_IMAGE_ARGS')
   IE=DI_SZ(CL_DEVICE_MAX_MEM_ALLOC_SIZE, 'MAX_MEM_ALLOC_SIZE')
   IE=DI_SZ(CL_DEVICE_IMAGE2D_MAX_WIDTH, 'IMAGE2D_MAX_WIDTH')
   IE=DI_SZ(CL_DEVICE_IMAGE2D_MAX_HEIGHT, 'IMAGE2D_MAX_HEIGHT')
   IE=DI_SZ(CL_DEVICE_IMAGE3D_MAX_WIDTH, 'IMAGE3D_MAX_WIDTH')
   IE=DI_SZ(CL_DEVICE_IMAGE3D_MAX_HEIGHT, 'IMAGE3D_MAX_HEIGHT')
   IE=DI_SZ(CL_DEVICE_IMAGE3D_MAX_DEPTH, 'IMAGE3D_MAX_DEPTH')
   IE=DI_BL(CL_DEVICE_IMAGE_SUPPORT, 'IMAGE_SUPPORT')
   IE=DI_SZ(CL_DEVICE_MAX_PARAMETER_SIZE, 'MAX_PARAMETER_SIZE')
   IE=DI_32(CL_DEVICE_MAX_SAMPLERS, 'MAX_SAMPLERS')
   IE=DI_32(CL_DEVICE_MEM_BASE_ADDR_ALIGN, 'MEM_BASE_ADDR_ALIGN')
!      IE=DI_SZ(CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE, 'MIN_DATA_TYPE_ALIGN_SIZE')
   IE=DI_64(CL_DEVICE_SINGLE_FP_CONFIG, 'SINGLE_FP_CONFIG')
   IE=DI_32(CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, 'GLOBAL_MEM_CACHE_TYPE')
!      IE=DI_SZ(CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, 'GLOBAL_MEM_CACHELINE_SIZE')
   IE=DI_SZ(CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, 'GLOBAL_MEM_CACHE_SIZE')
   IE=DI_SZ(CL_DEVICE_GLOBAL_MEM_SIZE, 'GLOBAL_MEM_SIZE')
   IE=DI_SZ(CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, 'MAX_CONST_BUF_SIZE')
   IE=DI_32(CL_DEVICE_MAX_CONSTANT_ARGS, 'MAX_CONSTANT_ARGS')
   IE=DI_32(CL_DEVICE_LOCAL_MEM_TYPE, 'LOCAL_MEM_TYPE')
   IE=DI_SZ(CL_DEVICE_LOCAL_MEM_SIZE, 'LOCAL_MEM_SIZE')
!      IE=DI_32(CL_DEVICE_ERROR_CORRECTION_SUPPORT, 'ERR_CORRECTION_SUPPORT')
   IE=DI_SZ(CL_DEVICE_PROFILING_TIMER_RESOLUTION, 'TIMER_RESOLUTION')
   IE=DI_BL(CL_DEVICE_ENDIAN_LITTLE, 'ENDIAN_LITTLE')
   IE=DI_BL(CL_DEVICE_AVAILABLE, 'AVAILABLE')
   IE=DI_32(CL_DEVICE_COMPILER_AVAILABLE, 'COMPILER_AVAILABLE')
   IE=DI_64(CL_DEVICE_EXECUTION_CAPABILITIES, 'EXEC_CAPABILITIES')
   IE=DI_64(CL_DEVICE_QUEUE_PROPERTIES, 'QUEUE_PROPERTIES')
   IE=DI_CH(CL_DEVICE_NAME, 'NAME')
   IE=DI_CH(CL_DEVICE_VENDOR, 'VENDOR')
   IE=DI_CH(CL_DRIVER_VERSION, 'DRIVER_VERSION')
   IE=DI_CH(CL_DEVICE_PROFILE, 'PROFILE')
   IE=DI_CH(CL_DEVICE_VERSION, 'VERSION')
   IE=DI_CH(CL_DEVICE_EXTENSIONS, 'EXTENSIONS')
   IE=DI_64(CL_DEVICE_PLATFORM, 'PLATFORM')
   IE=DI_64(CL_DEVICE_DOUBLE_FP_CONFIG, 'DOUBLE_FP_CONFIG')
   ! 0x1033 reserved for CL_DEVICE_HALF_FP_CONFIG, 'HALF_FP_CONFIG')
!      IE=DI_32(CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF, 'PREFERRED_VECTOR_WIDTH_HALF')
   IE=DI_32(CL_DEVICE_HOST_UNIFIED_MEMORY, 'HOST_UNIFIED_MEMORY')
!      IE=DI_32(CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR, 'NATIVE_VECTOR_WIDTH_CHAR')
!      IE=DI_32(CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT, 'NATIVE_VECTOR_WIDTH_SHORT')
!      IE=DI_32(CL_DEVICE_NATIVE_VECTOR_WIDTH_INT, 'NATIVE_VECTOR_WIDTH_INT')
!      IE=DI_32(CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG, 'NATIVE_VECTOR_WIDTH_LONG')
!      IE=DI_32(CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT, 'NATIVE_VECTOR_WIDTH_FLOAT')
!      IE=DI_32(CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE, 'NATIVE_VECTOR_WIDTH_DOUBLE')
!      IE=DI_32(CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF, 'NATIVE_VECTOR_WIDTH_HALF')
   IE=DI_CH(CL_DEVICE_OPENCL_C_VERSION, 'OPENCL_C_VERSION')
   IE=DI_BL(CL_DEVICE_LINKER_AVAILABLE, 'LINKER_AVAILABLE')
   IE=DI_CH(CL_DEVICE_BUILT_IN_KERNELS, 'BUILT_IN_KERNELS')
   IE=DI_SZ(CL_DEVICE_IMAGE_MAX_BUFFER_SIZE, 'IMAGE_MAX_BUFFER_SIZE')
   IE=DI_SZ(CL_DEVICE_IMAGE_MAX_ARRAY_SIZE, 'IMAGE_MAX_ARRAY_SIZE')
   IE=DI_64(CL_DEVICE_PARENT_DEVICE, 'PARENT_DEVICE')
!      IE=DI_32(CL_DEVICE_PARTITION_MAX_SUB_DEVICES, 'PARTITION_MAX_SUB_DEVICES')
!!!      IE=DI_32(CL_DEVICE_PARTITION_PROPERTIES, 'PARTITION_PROPERTIES')
!      IE=DI_32(CL_DEVICE_PARTITION_AFFINITY_DOMAIN, 'PARTITION_AFFINITY_DOMAIN')
!!!      IE=DI_32(CL_DEVICE_PARTITION_TYPE, 'PARTITION_TYPE')
   IE=DI_SZ(CL_DEVICE_REFERENCE_COUNT, 'REFERENCE_COUNT')
!      IE=DI_32(CL_DEVICE_PREFERRED_INTEROP_USER_SYNC, 'PREFERRED_INTEROP_USER_SYNC')
   IE=DI_SZ(CL_DEVICE_PRINTF_BUFFER_SIZE, 'PRINTF_BUFFER_SIZE')

   INDT = INDT - 3
   WRITE(IUNIT,'(2A)') Z('NVIDIA specific'), ': '
   INDT = INDT + 3
   IE=DI_32(CL_DEVICE_REGISTERS_PER_BLOCK_NV, 'REGISTERS_PER_BLOCK_NV')
   IE=DI_32(CL_DEVICE_WARP_SIZE_NV, 'WARP_SIZE_NV')
   IE=DI_BL(CL_DEVICE_GPU_OVERLAP_NV, 'GPU_OVERLAP_NV')
   IE=DI_BL(CL_DEVICE_INTEGRATED_MEMORY_NV, 'INTEGRATED_MEMORY_NV')

   INDT = INDT - 3
   WRITE(IUNIT,'(2A)') Z('AMD specific'), ': '
   INDT = INDT + 3
   IE=DI_SZ(CL_DEVICE_GLOBAL_FREE_MEMORY_AMD, 'GLOBAL_FREE_MEM_AMD')
   IE=DI_SZ(CL_DEVICE_SIMD_PER_COMPUTE_UNIT_AMD, 'SIMD_PER_CU_AMD')
   IE=DI_SZ(CL_DEVICE_SIMD_WIDTH_AMD, 'SIMD_WIDTH_AMD')
   IE=DI_SZ(CL_DEVICE_SIMD_INSTRUCTION_WIDTH_AMD, 'SIMD_INSTRUCTION_WIDTH_AMD')
   IE=DI_SZ(CL_DEVICE_WAVEFRONT_WIDTH_AMD, 'WAVEFRONT_WIDTH_AMD')

9999  CONTINUE
   PRINT_DEV = CLRET
   RETURN
   END FUNCTION PRINT_DEV

END MODULE OCL_DEVICE_M
