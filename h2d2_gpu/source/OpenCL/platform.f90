!---------------------------------------------------------------------------
! Copyright (c) 2013 Kyle Lutz <kyle.r.lutz@gmail.com>
!
! Distributed under the Boost Software License, Version 1.0
! See accompanying file LICENSE_1_0.txt or copy at
! http://www.boost.org/LICENSE_1_0.txt
!
! See http://boostorg.github.com/compute for more information.
!---------------------------------------------------------------------------

! \class platform
! \brief A compute platform.
!
! The platform class provides an interface to an OpenCL platform.
!
! To obtain a list of all platforms on the system use the
! system::platforms() method.
!
! \see device, context

MODULE OCL_PLATFORM_M

   USE clfortran
   USE ISO_C_BINDING

   IMPLICIT NONE

   TYPE :: OCL_PLATFORM_T
      INTEGER(C_INTPTR_T), PRIVATE :: m_platform = 0

      CONTAINS
      PROCEDURE :: GET
      PROCEDURE :: GET_DEVICE_COUNT
      PROCEDURE :: GET_NAME
      PROCEDURE :: GET_PROFILE
      PROCEDURE :: GET_VENDOR
      PROCEDURE :: GET_VERSION
      PROCEDURE :: PRINT_INFO
   END TYPE OCL_PLATFORM_T

   CONTAINS

!************************************************************************
! Sommaire:
!
! Description:
!        Static
!!!    ! Returns the number of compute platforms on the system.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_PLATFORM_COUNT()

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INT32_T) :: NPLTF

   NPLTF = 0
   CLRET = clGetPlatformIDs(0, C_NULL_PTR, NPLTF)

   OCL_PLATFORM_COUNT = NPLTF
   RETURN
   END FUNCTION OCL_PLATFORM_COUNT


!************************************************************************
! Sommaire:
!
! Description:
   ! ---  Creates a new platform object for id.
!
! Entrée:
!     IDX      Platform index [1, N]
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_PLATFORM_CTR(SELF, IDX)

   USE OCL_UTIL_M

   TYPE(OCL_PLATFORM_T), INTENT(OUT) :: SELF
   INTEGER,              INTENT(IN)  :: IDX

   INTEGER :: IRET
   INTEGER :: IP
   INTEGER(C_INT32_T) :: CLRET
   INTEGER(c_int) :: NIDS, NTMP
   INTEGER(c_intptr_t), ALLOCATABLE :: PIDS(:)
   TYPE(OCL_PLATFORM_T) :: SELF0
!---------------------------------------------------------------------

   !---     Get platforms number
   NIDS = 0
   CLRET = clGetPlatformIDs(0, C_NULL_PTR, NIDS)
   IF (CLRET .EQ. CL_SUCCESS) THEN
      IF (NIDS .LT.    1) CLRET = OCL_INVALID_PLATFORM_COUNT
      IF (IDX  .LE.    0) CLRET = OCL_INVALID_PLATFORM_INDEX
      IF (IDX  .GT. NIDS) CLRET = OCL_INVALID_PLATFORM_INDEX
   ENDIF

   !---     Get platforms IDs
   IF (CLRET .EQ. CL_SUCCESS) THEN
      ALLOCATE( PIDS(NIDS) )
      CLRET = clGetPlatformIDs(NIDS, C_LOC(PIDS), NTMP)
   ENDIF

   !---     Alloue la structure
   IF (CLRET .EQ. CL_SUCCESS) THEN
      SELF = SELF0
      SELF%m_platform = PIDS(IDX)
   ENDIF

   !---     Désalloue la table des ID
   IF (ALLOCATED(PIDS)) DEALLOCATE(PIDS)

   OCL_PLATFORM_CTR = CLRET
   RETURN
   END FUNCTION OCL_PLATFORM_CTR

!************************************************************************
! Sommaire:
!
! Description:
    !!!! Destroys the platform object.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_PLATFORM_DTR(SELF)

   TYPE(OCL_PLATFORM_T), INTENT(INOUT) :: SELF
!---------------------------------------------------------------------

   SELF%m_platform = 0

   OCL_PLATFORM_DTR = CL_SUCCESS
   RETURN
   END FUNCTION OCL_PLATFORM_DTR



!************************************************************************
! Sommaire:
!
! Description:
!    ! Returns the underlying OpenCL platform (ID).
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INTPTR_T) FUNCTION GET(SELF)

   CLASS(OCL_PLATFORM_T) :: SELF
!---------------------------------------------------------------------

   GET = SELF%M_PLATFORM
   RETURN
   END FUNCTION GET

!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the name of the platform.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION GET_NAME(SELF)  RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_PLATFORM, GET_INFO

   CLASS(OCL_PLATFORM_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_PLATFORM_NAME
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_PLATFORM, INFO, VAL)
   RETURN
   END FUNCTION GET_NAME

!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the profile string for the platform.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION GET_PROFILE(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_PLATFORM, GET_INFO

   CLASS(OCL_PLATFORM_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_PLATFORM_PROFILE
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_PLATFORM, INFO, VAL)
   RETURN
   END FUNCTION GET_PROFILE

!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the name of the vendor for the platform.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION GET_VENDOR(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_PLATFORM, GET_INFO

   CLASS(OCL_PLATFORM_T) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_PLATFORM_VENDOR
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_PLATFORM, INFO, VAL)
   RETURN
   END FUNCTION GET_VENDOR

!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the version string for the platform.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION GET_VERSION(SELF) RESULT(VAL)

   USE OCL_UTIL_M, ONLY : OCL_INFO_PLATFORM, GET_INFO

   CLASS(OCL_PLATFORM_T) :: SELF

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_PLATFORM_VERSION
!------------------------------------------------------------------------------

   CLRET = GET_INFO(SELF%GET(), OCL_INFO_PLATFORM, INFO, VAL)
   RETURN
   END FUNCTION GET_VERSION

!     ! Returns a list of extensions supported by the platform.
!     std::vector<std::string> extensions() const
!     {
!         std::string extensions_string =
!             get_info<std::string>(CL_PLATFORM_EXTENSIONS);
!         std::vector<std::string> extensions_vector;
!         boost::split(extensions_vector,
!                      extensions_string,
!                      boost::is_any_of("\t "),
!                      boost::token_compress_on);
!         return extensions_vector;
!     }

!     ! Returns \c true if the platform supports the extension with
!     ! \p name.
!     bool supports_extension(const std::string &name) const
!     {
!         const std::vector<std::string> extensions = this->extensions();

!         return std::find(
!             extensions.begin(), extensions.end(), name) != extensions.end();
!     }


!************************************************************************
! Sommaire:
!
! Description:
!     ! Returns the number of devices on the platform.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION GET_DEVICE_COUNT(SELF, PTYPE)

   USE ISO_C_BINDING

   CLASS(OCL_PLATFORM_T), INTENT(IN) :: SELF
   INTEGER(C_INT64_T), OPTIONAL      :: PTYPE

   INTEGER(C_INT32_T) :: NDEVC
   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INT64_T) :: ITYPE
!---------------------------------------------------------------------

   ITYPE = CL_DEVICE_TYPE_ALL
   IF (PRESENT(PTYPE)) ITYPE = PTYPE

   NDEVC = 0
   CLRET = clGetDeviceIDs(SELF%m_platform, &
                          ITYPE, &
                          0, &
                          C_NULL_PTR, &
                          NDEVC)
   IF (CLRET .NE. CL_SUCCESS) THEN
      IF (CLRET .EQ. CL_DEVICE_NOT_FOUND) THEN
         NDEVC = 0 ! no devices for this platform
      ELSE
!                 ! something else went wrong
!                 BOOST_THROW_EXCEPTION(opencl_error(ret));
      ENDIF
   ENDIF

   GET_DEVICE_COUNT = NDEVC
   RETURN
   END FUNCTION GET_DEVICE_COUNT

!     ! Requests that the platform unload any compiler resources.
!     void unload_compiler()
!     {
!         #ifdef CL_VERSION_1_2
!         clUnloadPlatformCompiler(m_platform);
!         #else
!         clUnloadCompiler();
!         #endif
!     }

!************************************************************************
! Sommaire:
!
! Description:
!     La méthode PRINT_INFO imprime les données d'une plate-forme.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION PRINT_INFO(SELF, IUNIT)

   CLASS(OCL_PLATFORM_T), INTENT(IN) :: SELF
   INTEGER, INTENT(IN) :: IUNIT

   INTEGER IERR
   INTEGER I

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_SIZE_T)  :: LRET
   INTEGER(C_INT32_T) :: INFO32, V32
   INTEGER(C_INT64_T) :: INFO64
   INTEGER(C_INT32_T) :: NIDS
   INTEGER(C_INTPTR_T):: PID
   INTEGER(C_INTPTR_T), ALLOCATABLE :: DIDS(:)

   INTEGER, PARAMETER :: BUFSIZ = 256
   CHARACTER(LEN=BUFSIZ), TARGET :: BUF
!------------------------------------------------------------------------------

   PID = SELF%GET()

   !---     Name
   INFO32= CL_PLATFORM_NAME
   CLRET = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(BUF), LRET)
   IF (CLRET .EQ. CL_SUCCESS .AND. LRET .LT. BUFSIZ) THEN
      WRITE(IUNIT,'(2A)')   'name             : ', BUF(1:LRET-1)
   ENDIF

   !---     Vendor
   INFO32= CL_PLATFORM_VENDOR
   CLRET = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(BUF), LRET)
   IF (CLRET .EQ. CL_SUCCESS .AND. LRET .LT. BUFSIZ) THEN
      WRITE(IUNIT,'(2A)')   'vendor           : ', BUF(1:LRET-1)
   ENDIF

   !---     Version
   INFO32= CL_PLATFORM_VERSION
   CLRET = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(BUF), LRET)
   IF (CLRET .EQ. CL_SUCCESS .AND. LRET .LT. BUFSIZ) THEN
      WRITE(IUNIT,'(2A)')   'version          : ', BUF(1:LRET-1)
   ENDIF

   !---     Profile
   INFO32= CL_PLATFORM_PROFILE
   CLRET = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(BUF), LRET)
   IF (CLRET .EQ. CL_SUCCESS .AND. LRET .LT. BUFSIZ) THEN
      WRITE(IUNIT,'(2A)')   'profile          : ', BUF(1:LRET-1)
   ENDIF

   !---     Extensions
   INFO32= CL_PLATFORM_EXTENSIONS
   CLRET = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(BUF), LRET)
   IF (CLRET .EQ. CL_SUCCESS .AND. LRET .LT. BUFSIZ) THEN
      WRITE(IUNIT,'(2A)')   'extensions       : ', BUF(1:LRET-1)
   ENDIF

   !---     Device count
   NIDS = 0
   INFO64= CL_DEVICE_TYPE_ALL
   CLRET = clGetDeviceIDs(PID, INFO64, 0, C_NULL_PTR, NIDS)
   IF (CLRET .EQ. CL_SUCCESS) THEN
      WRITE(IUNIT,'(A,I6)') 'number of devices: ', NIDS
   ENDIF
   IF (NIDS .LT. 1) GOTO 9999

9999 CONTINUE
   IF (ALLOCATED(DIDS)) DEALLOCATE(DIDS)
   PRINT_INFO = CLRET
   RETURN
   END FUNCTION PRINT_INFO


END MODULE OCL_PLATFORM_M
