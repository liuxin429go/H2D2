!---------------------------------------------------------------------------!
! Copyright (c) 2013 Kyle Lutz <kyle.r.lutz@gmail.com>
!
! Distributed under the Boost Software License, Version 1.0
! See accompanying file LICENSE_1_0.txt or copy at
! http:!www.boost.org/LICENSE_1_0.txt
!
! See http:!boostorg.github.com/compute for more information.
!---------------------------------------------------------------------------!

! \class command_queue
! \brief A command queue.
!
! Command queues provide the interface for interacting with compute
! devices. The command_queue class provides methods to copy data to
! and from a compute device as well as execute compute kernels.
!
! Command queues are created for a compute device within a compute
! context.
!
! For example, to create a context and command queue for the default device
! on the system (this is the normal set up code used by almost all OpenCL
! programs):
! \code
! #include <boost/compute/core.hpp>
!
! ! get the default compute device
! boost::compute::device device = boost::compute::system::default_device();
!
! ! set up a compute context and command queue
! boost::compute::context context(device);
! boost::compute::command_queue queue(context, device);
! \endcode
!
! The default command queue for the system can be obtained with the
! system::default_queue() method.
!
! \see buffer, context, kernel
MODULE OCL_CMDQUEUE_M

   USE clfortran
   USE ISO_C_BINDING
   USE OCL_CONTEXT_M, ONLY: OCL_CONTEXT_T
   USE OCL_DEVICE_M,  ONLY: OCL_DEVICE_T

   IMPLICIT NONE

   TYPE :: OCL_CMDQUEUE_T
      INTEGER(C_INTPTR_T), PRIVATE :: M_CMDQUEUE = 0
      TYPE(OCL_CONTEXT_T), PRIVATE :: M_CONTEXT
      TYPE(OCL_DEVICE_T),  PRIVATE :: M_DEVICE

      CONTAINS
      PROCEDURE :: GET
      PROCEDURE :: GET_CONTEXT
      PROCEDURE :: GET_DEVICE
      PROCEDURE :: ENQUEUE_READ_BUFFER
      PROCEDURE :: ENQUEUE_READ_BUFFER_ASYNC
      PROCEDURE :: ENQUEUE_WRITE_BUFFER
      PROCEDURE :: ENQUEUE_ND_RANGE_KERNEL
      PROCEDURE :: ENQUEUE_1D_RANGE_KERNEL

      PROCEDURE :: FLUSH_QUEUE
      PROCEDURE :: FINISH
   END TYPE OCL_CMDQUEUE_T

CONTAINS

   !!!public:
!!!    enum properties {
!!!        enable_profiling = CL_QUEUE_PROFILING_ENABLE,
!!!        enable_out_of_order_execution = CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
!!!    };
!!!
!!!    enum map_flags {
!!!        map_read = CL_MAP_READ,
!!!        map_write = CL_MAP_WRITE
!!!        #ifdef CL_VERSION_2_0
!!!        ,
!!!        map_write_invalidate_region = CL_MAP_WRITE_INVALIDATE_REGION
!!!        #endif
!!!    };
!!!

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Creates a command queue in \p context for \p device with
!!!    ! \p properties.
!!!    !
!!!    ! \see_opencl_ref{clCreateCommandQueue}
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_CMDQUEUE_CTR(SELF, CTXT, DEVC, PROP)

   USE OCL_CONTEXT_M
   USE OCL_DEVICE_M

   TYPE(OCL_CMDQUEUE_T), INTENT(OUT):: SELF
   TYPE(OCL_CONTEXT_T),  INTENT(IN) :: CTXT
   TYPE(OCL_DEVICE_T),   INTENT(IN) :: DEVC
   INTEGER(C_INT64_T),   INTENT(IN), OPTIONAL   :: PROP

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT64_T)  :: PROPS
   INTEGER(C_INTPTR_T) :: QID
   TYPE(OCL_CMDQUEUE_T):: SELF0
!---------------------------------------------------------------------

   PROPS = 0
   IF (PRESENT(PROP)) PROPS = PROP

   CLRET = CL_SUCCESS
   QID = clCreateCommandQueue(CTXT%GET(), &
                              DEVC%GET(), &
                              PROPS, &
                              CLRET)

   IF (CLRET .EQ. CL_SUCCESS) THEN
      SELF = SELF0
      SELF%m_cmdqueue = QID
      SELF%m_context  = CTXT
      SELF%m_device   = DEVC
   ENDIF

   OCL_CMDQUEUE_CTR = CLRET
   RETURN
   END FUNCTION OCL_CMDQUEUE_CTR

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Destroys the command queue.
!!!    !
!!!    ! \see_opencl_ref{clReleaseCommandQueue}
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_CMDQUEUE_DTR(SELF)

   TYPE(OCL_CMDQUEUE_T), INTENT(INOUT) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
!---------------------------------------------------------------------

   CLRET = clReleaseCommandQueue(SELF%GET())

   SELF%m_cmdqueue = 0

   OCL_CMDQUEUE_DTR = CLRET
   RETURN
   END FUNCTION OCL_CMDQUEUE_DTR


!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the underlying OpenCL command queue.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INTPTR_T) FUNCTION GET(SELF)

   CLASS(OCL_CMDQUEUE_T) :: SELF

   GET = SELF%M_CMDQUEUE
   RETURN
   END FUNCTION GET

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the context for the command queue.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION GET_CONTEXT(SELF)

   TYPE(OCL_CONTEXT_T) :: GET_CONTEXT
   CLASS(OCL_CMDQUEUE_T) :: SELF

!------------------------------------------------------------------------------

   GET_CONTEXT = SELF%M_CONTEXT
   RETURN
   END FUNCTION GET_CONTEXT

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the device that the command queue issues commands to.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION GET_DEVICE(SELF)

   TYPE(OCL_DEVICE_T) :: GET_DEVICE
   CLASS(OCL_CMDQUEUE_T) :: SELF

!------------------------------------------------------------------------------

   GET_DEVICE = SELF%M_DEVICE
   RETURN
   END FUNCTION GET_DEVICE


!!!    ! Returns information about the command queue.
!!!    !
!!!    ! \see_opencl_ref{clGetCommandQueueInfo}
!!!    template<class T>
!!!    T get_info(cl_command_queue_info info) const
!!!    {
!!!        return detail::get_object_info<T>(clGetCommandQueueInfo, m_queue, info);
!!!    }
!!!
!!!    ! \overload
!!!    template<int Enum>
!!!    typename detail::get_object_info_type<command_queue, Enum>::type
!!!    get_info() const;
!!!
!!!    ! Returns the properties for the command queue.
!!!    cl_command_queue_properties get_properties() const
!!!    {
!!!        return get_info<cl_command_queue_properties>(CL_QUEUE_PROPERTIES);
!!!    }

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Enqueues a command to read data from \p buffer to host memory.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueReadBuffer}
!!!    !
!!!    ! \see copy()
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION ENQUEUE_READ_BUFFER(SELF, BUFF, B, L)

   USE OCL_BUFFER_M

   CLASS(OCL_CMDQUEUE_T) :: SELF
   TYPE(OCL_BUFFER_T), INTENT(IN)    :: BUFF
   BYTE,               INTENT(INOUT) :: B(:)
   INTEGER(8),         INTENT(IN),  OPTIONAL :: L

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INT32_T) :: EVENT
   INTEGER(C_SIZE_T)  :: IOFF
   INTEGER(C_SIZE_T)  :: ISIZ
!------------------------------------------------------------------------

   EVENT = 0
   IOFF = 0
   ISIZ = SIZE(B)
   IF (PRESENT(L)) ISIZ = L

   CLRET = clEnqueueReadBuffer (SELF%GET(), &
                                BUFF%GET(), &
                                CL_TRUE, &
                                ioff, &
                                isiz, &
                                C_LOC(B), &
                                0, &            ! events.size(),
                                C_NULL_PTR, &   ! events.get_event_ptr(),
                                C_NULL_PTR)     ! event_.get())

   ENQUEUE_READ_BUFFER = CLRET
   RETURN
   END FUNCTION ENQUEUE_READ_BUFFER

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Enqueues a command to read data from \p buffer to host memory. The
!!!    ! copy will be performed asynchronously.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueReadBuffer}
!!!    !
!!!    ! \see copy_async()
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION ENQUEUE_READ_BUFFER_ASYNC(SELF, BUFF, B, L)

   USE OCL_BUFFER_M

   CLASS(OCL_CMDQUEUE_T) :: SELF
   TYPE(OCL_BUFFER_T), INTENT(IN)    :: BUFF
   BYTE,               INTENT(INOUT) :: B(:)
   INTEGER(8),         INTENT(IN), OPTIONAL :: L

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INT32_T) :: EVENT
   INTEGER(C_SIZE_T)  :: IOFF
   INTEGER(C_SIZE_T)  :: ISIZ
!------------------------------------------------------------------------

   EVENT = 0
   IOFF = 0
   ISIZ = SIZE(B)
   IF (PRESENT(L)) ISIZ = L

   CLRET = clEnqueueReadBuffer (SELF%GET(), &
                                BUFF%GET(), &
                                CL_FALSE, &
                                ioff, &
                                isiz, &
                                C_LOC(B), &
                                0, &            ! events.size(),
                                C_NULL_PTR, &   ! events.get_event_ptr(),
                                C_NULL_PTR)     ! event_.get())

   ENQUEUE_READ_BUFFER_ASYNC = CLRET
   RETURN
   END FUNCTION ENQUEUE_READ_BUFFER_ASYNC

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Enqueues a command to write data from host memory to \p buffer.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueWriteBuffer}
!!!    !
!!!    ! \see copy()
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION ENQUEUE_WRITE_BUFFER(SELF, BUFF, B, L)

   USE OCL_BUFFER_M

   CLASS(OCL_CMDQUEUE_T) :: SELF
   TYPE(OCL_BUFFER_T), INTENT(IN) :: BUFF
   BYTE,               INTENT(IN) :: B(:)
   INTEGER(8),         INTENT(IN), OPTIONAL :: L

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INT32_T) :: EVENT
   INTEGER(c_size_t)  :: IOFF
   INTEGER(c_size_t)  :: ISIZ
!------------------------------------------------------------------------

!!!    event enqueue_write_buffer(const buffer &buffer,
!!!                               size_t offset,
!!!                               size_t size,
!!!                               const void *host_ptr,
!!!                               const wait_list &events = wait_list())

   EVENT = 0
   IOFF = 0
   ISIZ = SIZE(B)
   IF (PRESENT(L)) ISIZ = L

   CLRET = clEnqueueWriteBuffer(SELF%GET(), &
                                BUFF%GET(), &
                                CL_TRUE, &
                                ioff, &
                                isiz, &
                                C_LOC(B), &
                                0, &            ! events.size(),
                                C_NULL_PTR, &   ! events.get_event_ptr(),
                                C_NULL_PTR)     ! event_.get())

   ENQUEUE_WRITE_BUFFER = CLRET
   RETURN
   END FUNCTION ENQUEUE_WRITE_BUFFER

!!!    ! Enqueues a command to write data from host memory to \p buffer.
!!!    ! The copy is performed asynchronously.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueWriteBuffer}
!!!    !
!!!    ! \see copy_async()
!!!    event enqueue_write_buffer_async(const buffer &buffer,
!!!                                     size_t offset,
!!!                                     size_t size,
!!!                                     const void *host_ptr,
!!!                                     const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!        BOOST_ASSERT(size <= buffer.size());
!!!        BOOST_ASSERT(buffer.get_context() == this->get_context());
!!!        BOOST_ASSERT(host_ptr != 0);
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueWriteBuffer(
!!!            m_queue,
!!!            buffer.get(),
!!!            CL_FALSE,
!!!            offset,
!!!            size,
!!!            host_ptr,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! Enqueues a command to copy data from \p src_buffer to
!!!    ! \p dst_buffer.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueCopyBuffer}
!!!    !
!!!    ! \see copy()
!!!    event enqueue_copy_buffer(const buffer &src_buffer,
!!!                              const buffer &dst_buffer,
!!!                              size_t src_offset,
!!!                              size_t dst_offset,
!!!                              size_t size,
!!!                              const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!        BOOST_ASSERT(src_offset + size <= src_buffer.size());
!!!        BOOST_ASSERT(dst_offset + size <= dst_buffer.size());
!!!        BOOST_ASSERT(src_buffer.get_context() == this->get_context());
!!!        BOOST_ASSERT(dst_buffer.get_context() == this->get_context());
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueCopyBuffer(
!!!            m_queue,
!!!            src_buffer.get(),
!!!            dst_buffer.get(),
!!!            src_offset,
!!!            dst_offset,
!!!            size,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    #if defined(CL_VERSION_1_2) || defined(BOOST_COMPUTE_DOXYGEN_INVOKED)
!!!    ! Enqueues a command to fill \p buffer with \p pattern.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueFillBuffer}
!!!    !
!!!    ! \opencl_version_warning{1,2}
!!!    !
!!!    ! \see fill()
!!!    event enqueue_fill_buffer(const buffer &buffer,
!!!                              const void *pattern,
!!!                              size_t pattern_size,
!!!                              size_t offset,
!!!                              size_t size,
!!!                              const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!        BOOST_ASSERT(offset + size <= buffer.size());
!!!        BOOST_ASSERT(buffer.get_context() == this->get_context());
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueFillBuffer(
!!!            m_queue,
!!!            buffer.get(),
!!!            pattern,
!!!            pattern_size,
!!!            offset,
!!!            size,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!    #endif ! CL_VERSION_1_2
!!!
!!!    ! Enqueues a command to map \p buffer into the host address space.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueMapBuffer}
!!!    void* enqueue_map_buffer(const buffer &buffer,
!!!                             cl_map_flags flags,
!!!                             size_t offset,
!!!                             size_t size,
!!!                             const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!        BOOST_ASSERT(offset + size <= buffer.size());
!!!        BOOST_ASSERT(buffer.get_context() == this->get_context());
!!!
!!!        cl_int ret = 0;
!!!        void *pointer = clEnqueueMapBuffer(
!!!            m_queue,
!!!            buffer.get(),
!!!            CL_TRUE,
!!!            flags,
!!!            offset,
!!!            size,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            0,
!!!            &ret
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return pointer;
!!!    }
!!!
!!!    ! Enqueues a command to unmap \p buffer from the host memory space.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueUnmapMemObject}
!!!    event enqueue_unmap_buffer(const buffer &buffer,
!!!                               void *mapped_ptr,
!!!                               const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(buffer.get_context() == this->get_context());
!!!
!!!        return enqueue_unmap_mem_object(buffer.get(), mapped_ptr, events);
!!!    }
!!!
!!!    ! Enqueues a command to unmap \p mem from the host memory space.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueUnmapMemObject}
!!!    event enqueue_unmap_mem_object(cl_mem mem,
!!!                                   void *mapped_ptr,
!!!                                   const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueUnmapMemObject(
!!!            m_queue,
!!!            mem,
!!!            mapped_ptr,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! Enqueues a command to read data from \p image to host memory.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueReadImage}
!!!    event enqueue_read_image(const image_object& image,
!!!                             const size_t *origin,
!!!                             const size_t *region,
!!!                             size_t row_pitch,
!!!                             size_t slice_pitch,
!!!                             void *host_ptr,
!!!                             const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueReadImage(
!!!            m_queue,
!!!            image.get(),
!!!            CL_TRUE,
!!!            origin,
!!!            region,
!!!            row_pitch,
!!!            slice_pitch,
!!!            host_ptr,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! \overload
!!!    template<size_t N>
!!!    event enqueue_read_image(const image_object& image,
!!!                             const extents<N> origin,
!!!                             const extents<N> region,
!!!                             void *host_ptr,
!!!                             size_t row_pitch = 0,
!!!                             size_t slice_pitch = 0,
!!!                             const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(image.get_context() == this->get_context());
!!!
!!!        size_t origin3[3] = { 0, 0, 0 };
!!!        size_t region3[3] = { 1, 1, 1 };
!!!
!!!        std::copy(origin.data(), origin.data() + N, origin3);
!!!        std::copy(region.data(), region.data() + N, region3);
!!!
!!!        return enqueue_read_image(
!!!            image, origin3, region3, row_pitch, slice_pitch, host_ptr, events
!!!        );
!!!    }
!!!
!!!    ! Enqueues a command to write data from host memory to \p image.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueWriteImage}
!!!    event enqueue_write_image(image_object& image,
!!!                              const size_t *origin,
!!!                              const size_t *region,
!!!                              const void *host_ptr,
!!!                              size_t input_row_pitch = 0,
!!!                              size_t input_slice_pitch = 0,
!!!                              const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueWriteImage(
!!!            m_queue,
!!!            image.get(),
!!!            CL_TRUE,
!!!            origin,
!!!            region,
!!!            input_row_pitch,
!!!            input_slice_pitch,
!!!            host_ptr,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! \overload
!!!    template<size_t N>
!!!    event enqueue_write_image(image_object& image,
!!!                              const extents<N> origin,
!!!                              const extents<N> region,
!!!                              const void *host_ptr,
!!!                              const size_t input_row_pitch = 0,
!!!                              const size_t input_slice_pitch = 0,
!!!                              const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(image.get_context() == this->get_context());
!!!
!!!        size_t origin3[3] = { 0, 0, 0 };
!!!        size_t region3[3] = { 1, 1, 1 };
!!!
!!!        std::copy(origin.data(), origin.data() + N, origin3);
!!!        std::copy(region.data(), region.data() + N, region3);
!!!
!!!        return enqueue_write_image(
!!!            image, origin3, region3, host_ptr, input_row_pitch, input_slice_pitch, events
!!!        );
!!!    }
!!!
!!!    ! Enqueues a command to copy data from \p src_image to \p dst_image.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueCopyImage}
!!!    event enqueue_copy_image(const image_object& src_image,
!!!                             image_object& dst_image,
!!!                             const size_t *src_origin,
!!!                             const size_t *dst_origin,
!!!                             const size_t *region,
!!!                             const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueCopyImage(
!!!            m_queue,
!!!            src_image.get(),
!!!            dst_image.get(),
!!!            src_origin,
!!!            dst_origin,
!!!            region,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! \overload
!!!    template<size_t N>
!!!    event enqueue_copy_image(const image_object& src_image,
!!!                             image_object& dst_image,
!!!                             const extents<N> src_origin,
!!!                             const extents<N> dst_origin,
!!!                             const extents<N> region,
!!!                             const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(src_image.get_context() == this->get_context());
!!!        BOOST_ASSERT(dst_image.get_context() == this->get_context());
!!!        BOOST_ASSERT_MSG(src_image.format() == dst_image.format(),
!!!                         "Source and destination image formats must match.");
!!!
!!!        size_t src_origin3[3] = { 0, 0, 0 };
!!!        size_t dst_origin3[3] = { 0, 0, 0 };
!!!        size_t region3[3] = { 1, 1, 1 };
!!!
!!!        std::copy(src_origin.data(), src_origin.data() + N, src_origin3);
!!!        std::copy(dst_origin.data(), dst_origin.data() + N, dst_origin3);
!!!        std::copy(region.data(), region.data() + N, region3);
!!!
!!!        return enqueue_copy_image(
!!!            src_image, dst_image, src_origin3, dst_origin3, region3, events
!!!        );
!!!    }
!!!
!!!    ! Enqueues a command to copy data from \p src_image to \p dst_buffer.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueCopyImageToBuffer}
!!!    event enqueue_copy_image_to_buffer(const image_object& src_image,
!!!                                       memory_object& dst_buffer,
!!!                                       const size_t *src_origin,
!!!                                       const size_t *region,
!!!                                       size_t dst_offset,
!!!                                       const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueCopyImageToBuffer(
!!!            m_queue,
!!!            src_image.get(),
!!!            dst_buffer.get(),
!!!            src_origin,
!!!            region,
!!!            dst_offset,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! Enqueues a command to copy data from \p src_buffer to \p dst_image.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueCopyBufferToImage}
!!!    event enqueue_copy_buffer_to_image(const memory_object& src_buffer,
!!!                                       image_object& dst_image,
!!!                                       size_t src_offset,
!!!                                       const size_t *dst_origin,
!!!                                       const size_t *region,
!!!                                       const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueCopyBufferToImage(
!!!            m_queue,
!!!            src_buffer.get(),
!!!            dst_image.get(),
!!!            src_offset,
!!!            dst_origin,
!!!            region,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    #if defined(CL_VERSION_1_2) || defined(BOOST_COMPUTE_DOXYGEN_INVOKED)
!!!    ! Enqueues a command to fill \p image with \p fill_color.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueFillImage}
!!!    !
!!!    ! \opencl_version_warning{1,2}
!!!    event enqueue_fill_image(image_object& image,
!!!                             const void *fill_color,
!!!                             const size_t *origin,
!!!                             const size_t *region,
!!!                             const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueFillImage(
!!!            m_queue,
!!!            image.get(),
!!!            fill_color,
!!!            origin,
!!!            region,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! \overload
!!!    template<size_t N>
!!!    event enqueue_fill_image(image_object& image,
!!!                             const void *fill_color,
!!!                             const extents<N> origin,
!!!                             const extents<N> region,
!!!                             const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(image.get_context() == this->get_context());
!!!
!!!        size_t origin3[3] = { 0, 0, 0 };
!!!        size_t region3[3] = { 1, 1, 1 };
!!!
!!!        std::copy(origin.data(), origin.data() + N, origin3);
!!!        std::copy(region.data(), region.data() + N, region3);
!!!
!!!        return enqueue_fill_image(
!!!            image, fill_color, origin3, region3, events
!!!        );
!!!    }
!!!
!!!    ! Enqueues a command to migrate \p mem_objects.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueMigrateMemObjects}
!!!    !
!!!    ! \opencl_version_warning{1,2}
!!!    event enqueue_migrate_memory_objects(uint_ num_mem_objects,
!!!                                         const cl_mem *mem_objects,
!!!                                         cl_mem_migration_flags flags,
!!!                                         const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueMigrateMemObjects(
!!!            m_queue,
!!!            num_mem_objects,
!!!            mem_objects,
!!!            flags,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!    #endif ! CL_VERSION_1_2

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Enqueues a kernel for execution.
!!!    !
!!!    ! \see_opencl_ref{clEnqueueNDRangeKernel}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INT32_T) &
   FUNCTION ENQUEUE_ND_RANGE_KERNEL(SELF, &
                                    KRNL, &
                                    GLOBAL_WORK_SIZE, &
                                    LOCAL_WORK_SIZE)

   USE OCL_KERNEL_M

   CLASS(OCL_CMDQUEUE_T) :: SELF
   TYPE(OCL_KERNEL_T), INTENT(IN) :: KRNL
   INTEGER(C_SIZE_T),  INTENT(IN) :: GLOBAL_WORK_SIZE(:)
   INTEGER(C_SIZE_T),  INTENT(IN), OPTIONAL :: LOCAL_WORK_SIZE(:)

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INT32_T) :: EVENT
   INTEGER(C_INT32_T) :: WORK_DIM
   TYPE(C_PTR) :: L_GWS, L_LWS
!------------------------------------------------------------------------

   L_GWS = C_LOC(GLOBAL_WORK_SIZE)
   L_LWS = C_NULL_PTR
   IF (PRESENT(LOCAL_WORK_SIZE)) THEN
      L_LWS = C_LOC(LOCAL_WORK_SIZE)
   ENDIF

   WORK_DIM = SIZE(GLOBAL_WORK_SIZE)
   CLRET = clEnqueueNDRangeKernel(SELF%GET(), &
                                  KRNL%GET(), &
                                  WORK_DIM, &
                                  C_NULL_PTR, &
                                  L_GWS, &
                                  L_LWS, &
                                  0, &            ! events.size(),
                                  C_NULL_PTR, &   ! events.get_event_ptr(),
                                  C_NULL_PTR)     ! event_.get())

   ENQUEUE_ND_RANGE_KERNEL = CLRET
   RETURN
   END FUNCTION ENQUEUE_ND_RANGE_KERNEL

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Convenience method which calls enqueue_nd_range_kernel() with a
!!!    ! one-dimensional range.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INT32_T) &
   FUNCTION ENQUEUE_1D_RANGE_KERNEL(SELF, &
                                    KRNL, &
                                    GLOBAL_WORK_SIZE, &
                                    LOCAL_WORK_SIZE)

   USE OCL_KERNEL_M

   CLASS(OCL_CMDQUEUE_T) :: SELF
   TYPE(OCL_KERNEL_T), INTENT(IN) :: KRNL
   INTEGER(C_SIZE_T),  INTENT(IN) :: GLOBAL_WORK_SIZE
   INTEGER(C_SIZE_T),  INTENT(IN), OPTIONAL :: LOCAL_WORK_SIZE

   INTEGER(C_INT32_T):: CLRET
   INTEGER(C_SIZE_T) :: GWS(1)
   INTEGER(C_SIZE_T) :: LWS(1)
!------------------------------------------------------------------------

   GWS(1) = GLOBAL_WORK_SIZE
   IF (PRESENT(LOCAL_WORK_SIZE)) THEN
      LWS(1) = LOCAL_WORK_SIZE
      CLRET = SELF%enqueue_nd_range_kernel(KRNL, GWS, LWS)
   ELSE
      CLRET = SELF%enqueue_nd_range_kernel(KRNL, GWS)
   ENDIF

   ENQUEUE_1D_RANGE_KERNEL = CLRET
   RETURN
   END FUNCTION ENQUEUE_1D_RANGE_KERNEL

!!!    ! Enqueues a function to execute on the host.
!!!    event enqueue_native_kernel(void (BOOST_COMPUTE_CL_CALLBACK *user_func)(void *),
!!!                                void *args,
!!!                                size_t cb_args,
!!!                                uint_ num_mem_objects,
!!!                                const cl_mem *mem_list,
!!!                                const void **args_mem_loc,
!!!                                const wait_list &events = wait_list())
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!        cl_int ret = clEnqueueNativeKernel(
!!!            m_queue,
!!!            user_func,
!!!            args,
!!!            cb_args,
!!!            num_mem_objects,
!!!            mem_list,
!!!            args_mem_loc,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! Convenience overload for enqueue_native_kernel() which enqueues a
!!!    ! native kernel on the host with a nullary function.
!!!    event enqueue_native_kernel(void (BOOST_COMPUTE_CL_CALLBACK *user_func)(void),
!!!                                const wait_list &events = wait_list())
!!!    {
!!!        return enqueue_native_kernel(
!!!            detail::nullary_native_kernel_trampoline,
!!!            reinterpret_cast<void *>(&user_func),
!!!            sizeof(user_func),
!!!            0,
!!!            0,
!!!            0,
!!!            events
!!!        );
!!!    }

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Flushes the command queue.
!!!    !
!!!    ! \see_opencl_ref{clFlush}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INT32_T) &
   FUNCTION FLUSH_QUEUE(SELF)

   CLASS(OCL_CMDQUEUE_T) :: SELF
   INTEGER(C_INT32_T)  :: CLRET
!------------------------------------------------------------------------

   CLRET = clFlush( SELF%GET() )

   FLUSH_QUEUE = CLRET
   RETURN
   END FUNCTION FLUSH_QUEUE

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Blocks until all outstanding commands in the queue have finished.
!!!    !
!!!    ! \see_opencl_ref{clFinish}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INT32_T) &
   FUNCTION FINISH(SELF)

   CLASS(OCL_CMDQUEUE_T) :: SELF
   INTEGER(C_INT32_T)  :: CLRET
!------------------------------------------------------------------------

   CLRET = clFinish( SELF%GET() )

   FINISH = CLRET
   RETURN
   END FUNCTION FINISH

!!!    ! Enqueues a barrier in the queue.
!!!    void enqueue_barrier()
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!        cl_int ret = CL_SUCCESS;
!!!
!!!        #ifdef CL_VERSION_1_2
!!!        if(get_device().check_version(1, 2)){
!!!            ret = clEnqueueBarrierWithWaitList(m_queue, 0, 0, 0);
!!!        } else
!!!        #endif ! CL_VERSION_1_2
!!!        {
!!!            ret = clEnqueueBarrier(m_queue);
!!!        }
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!    }
!!!
!!!    #if defined(CL_VERSION_1_2) || defined(BOOST_COMPUTE_DOXYGEN_INVOKED)
!!!    ! Enqueues a barrier in the queue after \p events.
!!!    !
!!!    ! \opencl_version_warning{1,2}
!!!    event enqueue_barrier(const wait_list &events)
!!!    {
!!!        BOOST_ASSERT(m_queue != 0);
!!!
!!!        event event_;
!!!        cl_int ret = CL_SUCCESS;
!!!
!!!        ret = clEnqueueBarrierWithWaitList(
!!!            m_queue, events.size(), events.get_event_ptr(), &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!    #endif ! CL_VERSION_1_2
!!!
!!!    ! Enqueues a marker in the queue and returns an event that can be
!!!    ! used to track its progress.
!!!    event enqueue_marker()
!!!    {
!!!        event event_;
!!!        cl_int ret = CL_SUCCESS;
!!!
!!!        #ifdef CL_VERSION_1_2
!!!        if(get_device().check_version(1, 2)){
!!!            ret = clEnqueueMarkerWithWaitList(m_queue, 0, 0, &event_.get());
!!!        } else
!!!        #endif
!!!        {
!!!            ret = clEnqueueMarker(m_queue, &event_.get());
!!!        }
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    #if defined(CL_VERSION_1_2) || defined(BOOST_COMPUTE_DOXYGEN_INVOKED)
!!!    ! Enqueues a marker after \p events in the queue and returns an
!!!    ! event that can be used to track its progress.
!!!    !
!!!    ! \opencl_version_warning{1,2}
!!!    event enqueue_marker(const wait_list &events)
!!!    {
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueMarkerWithWaitList(
!!!            m_queue, events.size(), events.get_event_ptr(), &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!    #endif ! CL_VERSION_1_2
!!!
!!!    #if defined(CL_VERSION_2_0) || defined(BOOST_COMPUTE_DOXYGEN_INVOKED)
!!!    ! Enqueues a command to copy \p size bytes of data from \p src_ptr to
!!!    ! \p dst_ptr.
!!!    !
!!!    ! \opencl_version_warning{2,0}
!!!    !
!!!    ! \see_opencl2_ref{clEnqueueSVMMemcpy}
!!!    event enqueue_svm_memcpy(void *dst_ptr,
!!!                             const void *src_ptr,
!!!                             size_t size,
!!!                             const wait_list &events = wait_list())
!!!    {
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueSVMMemcpy(
!!!            m_queue,
!!!            CL_TRUE,
!!!            dst_ptr,
!!!            src_ptr,
!!!            size,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! Enqueues a command to copy \p size bytes of data from \p src_ptr to
!!!    ! \p dst_ptr. The operation is performed asynchronously.
!!!    !
!!!    ! \opencl_version_warning{2,0}
!!!    !
!!!    ! \see_opencl2_ref{clEnqueueSVMMemcpy}
!!!    event enqueue_svm_memcpy_async(void *dst_ptr,
!!!                                   const void *src_ptr,
!!!                                   size_t size,
!!!                                   const wait_list &events = wait_list())
!!!    {
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueSVMMemcpy(
!!!            m_queue,
!!!            CL_FALSE,
!!!            dst_ptr,
!!!            src_ptr,
!!!            size,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! Enqueues a command to fill \p size bytes of data at \p svm_ptr with
!!!    ! \p pattern.
!!!    !
!!!    ! \opencl_version_warning{2,0}
!!!    !
!!!    ! \see_opencl2_ref{clEnqueueSVMMemFill}
!!!    event enqueue_svm_fill(void *svm_ptr,
!!!                           const void *pattern,
!!!                           size_t pattern_size,
!!!                           size_t size,
!!!                           const wait_list &events = wait_list())
!!!
!!!    {
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueSVMMemFill(
!!!            m_queue,
!!!            svm_ptr,
!!!            pattern,
!!!            pattern_size,
!!!            size,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! Enqueues a command to free \p svm_ptr.
!!!    !
!!!    ! \opencl_version_warning{2,0}
!!!    !
!!!    ! \see_opencl2_ref{clEnqueueSVMFree}
!!!    !
!!!    ! \see svm_free()
!!!    event enqueue_svm_free(void *svm_ptr,
!!!                           const wait_list &events = wait_list())
!!!    {
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueSVMFree(
!!!            m_queue,
!!!            1,
!!!            &svm_ptr,
!!!            0,
!!!            0,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! Enqueues a command to map \p svm_ptr to the host memory space.
!!!    !
!!!    ! \opencl_version_warning{2,0}
!!!    !
!!!    ! \see_opencl2_ref{clEnqueueSVMMap}
!!!    event enqueue_svm_map(void *svm_ptr,
!!!                          size_t size,
!!!                          cl_map_flags flags,
!!!                          const wait_list &events = wait_list())
!!!    {
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueSVMMap(
!!!            m_queue,
!!!            CL_TRUE,
!!!            flags,
!!!            svm_ptr,
!!!            size,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!
!!!    ! Enqueues a command to unmap \p svm_ptr from the host memory space.
!!!    !
!!!    ! \opencl_version_warning{2,0}
!!!    !
!!!    ! \see_opencl2_ref{clEnqueueSVMUnmap}
!!!    event enqueue_svm_unmap(void *svm_ptr,
!!!                            const wait_list &events = wait_list())
!!!    {
!!!        event event_;
!!!
!!!        cl_int ret = clEnqueueSVMUnmap(
!!!            m_queue,
!!!            svm_ptr,
!!!            events.size(),
!!!            events.get_event_ptr(),
!!!            &event_.get()
!!!        );
!!!
!!!        if(ret != CL_SUCCESS){
!!!            BOOST_THROW_EXCEPTION(opencl_error(ret));
!!!        }
!!!
!!!        return event_;
!!!    }
!!!    #endif ! CL_VERSION_2_0
!!!
!!!    ! Returns \c true if the command queue is the same at \p other.
!!!    bool operator==(const command_queue &other) const
!!!    {
!!!        return m_queue == other.m_queue;
!!!    }
!!!
!!!    ! Returns \c true if the command queue is different from \p other.
!!!    bool operator!=(const command_queue &other) const
!!!    {
!!!        return m_queue != other.m_queue;
!!!    }
!!!
!!!    ! \internal_
!!!    operator cl_command_queue() const
!!!    {
!!!        return m_queue;
!!!    }
!!!
!!!    ! \internal_
!!!    bool check_device_version(int major, int minor) const
!!!    {
!!!        return get_device().check_version(major, minor);
!!!    }
!!!
!!!private:
!!!    cl_command_queue m_queue;
!!!};
!!!
!!!inline buffer buffer::clone(command_queue &queue) const
!!!{
!!!    buffer copy(get_context(), size(), get_memory_flags());
!!!    queue.enqueue_copy_buffer(*this, copy, 0, 0, size());
!!!    return copy;
!!!}
!!!
!!!inline image1d image1d::clone(command_queue &queue) const
!!!{
!!!    image1d copy(
!!!        get_context(), width(), format(), get_memory_flags()
!!!    );
!!!
!!!    queue.enqueue_copy_image(*this, copy, origin(), copy.origin(), size());
!!!
!!!    return copy;
!!!}
!!!
!!!inline image2d image2d::clone(command_queue &queue) const
!!!{
!!!    image2d copy(
!!!        get_context(), width(), height(), format(), get_memory_flags()
!!!    );
!!!
!!!    queue.enqueue_copy_image(*this, copy, origin(), copy.origin(), size());
!!!
!!!    return copy;
!!!}
!!!
!!!inline image3d image3d::clone(command_queue &queue) const
!!!{
!!!    image3d copy(
!!!        get_context(), width(), height(), depth(), format(), get_memory_flags()
!!!    );
!!!
!!!    queue.enqueue_copy_image(*this, copy, origin(), copy.origin(), size());
!!!
!!!    return copy;
!!!}

END MODULE OCL_CMDQUEUE_M
