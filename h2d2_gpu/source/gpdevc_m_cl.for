C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     INTEGER GP_DVOCL_CTR
C     INTEGER GP_DVOCL_INI
C     INTEGER GP_DVOCL_PRN
C     INTEGER GP_DVOCL_ALLOC
C     INTEGER GP_DVOCL_START
C     INTEGER GP_DVOCL_CP2CPU
C     INTEGER GP_DVOCL_CP2DEV
C     INTEGER GP_DVOCL_ARGBUF
C     INTEGER GP_DVOCL_LOAD
C     INTEGER GP_DVOCL_XEQ
C     INTEGER GP_DVOCL_ERRMSG
C
C************************************************************************

      MODULE GP_DVOCL_M

      USE GP_DEVC_M
      USE OCL_PLATFORM_M
      USE OCL_DEVICE_M
      USE OCL_CONTEXT_M
      USE OCL_CMDQUEUE_M
      USE OCL_PROGRAM_M
      USE OCL_KERNEL_M
      USE OCL_BUFFER_M

      IMPLICIT NONE
      PUBLIC

      INTEGER, PRIVATE, PARAMETER :: NBUF_MAX = 16

C---     Type de donnée associé à un device
      TYPE, EXTENDS(GP_DEVC_PTR_T) :: GP_DVOCL_SELF_T
         INTEGER, PRIVATE  :: IBUF_MAX = 0
         TYPE(OCL_PLATFORM_T), PRIVATE :: PLTF
         TYPE(OCL_DEVICE_T),   PRIVATE :: DEVC
         TYPE(OCL_CONTEXT_T),  PRIVATE :: CTXT
         TYPE(OCL_CMDQUEUE_T), PRIVATE :: CMDQ
         TYPE(OCL_PROGRAM_T),  PRIVATE :: PRGM
         TYPE(OCL_KERNEL_T),   PRIVATE :: KRNL
         TYPE(OCL_BUFFER_T),   PRIVATE :: BUFF(NBUF_MAX)

         CONTAINS
         PROCEDURE :: GP_DEVC_INI_VIRT    => GP_DVOCL_INI
         PROCEDURE :: GP_DEVC_PRN_VIRT    => GP_DVOCL_PRN
         PROCEDURE :: GP_DEVC_START_VIRT  => GP_DVOCL_START
         !PROCEDURE :: GP_DEVC_STOP_VIRT   => GP_DVOCL_STOP

         PROCEDURE :: GP_DEVC_ALLOC_VIRT  => GP_DVOCL_ALLOC
         PROCEDURE :: GP_DEVC_CP2CPU_VIRT => GP_DVOCL_CP2CPU
         PROCEDURE :: GP_DEVC_CP2DEV_VIRT => GP_DVOCL_CP2DEV

         PROCEDURE :: GP_DEVC_ARGBUF_VIRT => GP_DVOCL_ARGBUF
         PROCEDURE :: GP_DEVC_LOADK_VIRT  => GP_DVOCL_LOAD
         PROCEDURE :: GP_DEVC_XEQK_VIRT   => GP_DVOCL_XEQ
      END TYPE GP_DVOCL_SELF_T

      CONTAINS

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction GP_DVOCL_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_CTR(SELF)

      CLASS(GP_DEVC_PTR_T), POINTER, INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER CLRET
      TYPE(GP_DVOCL_SELF_T), POINTER :: DVOCL
C------------------------------------------------------------------------

C---     Alloue la structure
      DVOCL => NULL()
      ALLOCATE (DVOCL, STAT=CLRET)
      IF (CLRET .NE. 0) GOTO 9900
      SELF => DVOCL

C---     Initialise
!!!      IF (ERR_GOOD()) IERR = GP_DVOCL_RAZ(DVOCL)

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_DVOCL_CTR = ERR_TYP()
      RETURN
      END FUNCTION GP_DVOCL_CTR

C************************************************************************
C Sommaire: Initialise un device OpenCL
C
C Description:
C     La fonction GP_DVOCL_INI initialise le device d'indice ID
C     sur la plate-forme d'indice IP.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IP       Indice de la plate-forme OpenCL
C     ID       Indice du device OpenCL
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_INI(SELF, INFO, IP, ID)

      CLASS(GP_DVOCL_SELF_T), INTENT(INOUT) :: SELF
      TYPE(GP_DEVICE_INFO_T), INTENT(INOUT) :: INFO
      INTEGER, INTENT(IN) :: IP
      INTEGER, INTENT(IN) :: ID

      INCLUDE 'err.fi'

      INTEGER CLRET
      TYPE(OCL_DEVICE_T) :: DEVC
C------------------------------------------------------------------------

      CLRET = CL_SUCCESS

C---     Create platform and device
      CLRET = OCL_PLATFORM_CTR(SELF%PLTF, IP)
      CLRET = OCL_DEVICE_CTR  (SELF%DEVC, SELF%PLTF, ID)

C---     Assigne les attributs
      INFO%MIN_WITM_PER_WGRP =
     &    SELF%DEVC%GET_MIN_WORK_ITEM_PER_WORK_GROUP()
      INFO%MAX_WITM_PER_WGRP = SELF%DEVC%GET_MAX_WORK_GROUP_SIZE()
      INFO%MIN_WGRP_PER_CU   = 1
      INFO%MIN_CU_PER_DEV    = 1
      INFO%MAX_CU_PER_DEV    = SELF%DEVC%GET_MAX_COMPUTE_UNITS()
      INFO%MAX_WITM_PER_DEV  = SELF%DEVC%GET_MAX_WORK_GROUP_SIZE()
      INFO%MEM_SIZE          = SELF%DEVC%GET_GLOBAL_MEM_SIZE()
      INFO%MAX_WGRP_PER_CU   = INFO%MAX_WITM_PER_WGRP /
     &                         INFO%MIN_WITM_PER_WGRP

      GP_DVOCL_INI = GP_DVOCL_ERRMSG(CLRET)
      RETURN
      END FUNCTION GP_DVOCL_INI

C************************************************************************
C Sommaire: Imprime OpenCL
C
C Description:
C     La fonction GP_OPNCL_PRN imprime les plates-formes OpenCL et leurs
C     devices.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_PRN(SELF, IUNIT)

      CLASS(GP_DVOCL_SELF_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: IUNIT

      INCLUDE 'err.fi'

      INTEGER(C_INT32_T)  :: CLRET
      TYPE(OCL_DEVICE_T), POINTER :: DEVC
C------------------------------------------------------------------------

      WRITE(IUNIT, *) 'OpenCL configuration'
      WRITE(IUNIT, *) '===================='
      CLRET = SELF%DEVC%PRINT_DEV(IUNIT)
      WRITE(IUNIT, *)

      GP_DVOCL_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DVOCL_ALLOC
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_ALLOC(SELF, ISIZ, BID)

      USE OCL_BUFFER_M

      CLASS(GP_DVOCL_SELF_T), INTENT(INOUT)  :: SELF
      INTEGER(8), INTENT(IN)  :: ISIZ
      INTEGER(8), INTENT(OUT) :: BID

      INCLUDE 'err.fi'

      INTEGER(8), PARAMETER :: IFLGS = 0
      INTEGER :: CLRET
      TYPE (OCL_BUFFER_T) :: BUFF
C------------------------------------------------------------------------
D     CALL ERR_PRE(SELF%IBUF_MAX .LE. NBUF_MAX)
C------------------------------------------------------------------------

      CLRET = OCL_BUFFER_CTR(BUFF, SELF%CTXT, IFLGS, ISIZ)
      BID  = BUFF%GET()
      SELF%IBUF_MAX = SELF%IBUF_MAX + 1
      SELF%BUFF(SELF%IBUF_MAX) = BUFF

      GP_DVOCL_ALLOC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DVOCL_START
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_START(SELF)

      CLASS(GP_DVOCL_SELF_T), INTENT(INOUT) :: SELF

      INTEGER CLRET
C------------------------------------------------------------------------

      CLRET = CL_SUCCESS

C---     Create context and command queue
      CLRET = OCL_CONTEXT_CTR (SELF%CTXT, SELF%DEVC)
      CLRET = OCL_CMDQUEUE_CTR(SELF%CMDQ, SELF%CTXT, SELF%DEVC)

      GP_DVOCL_START = GP_DVOCL_ERRMSG(CLRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DVOCL_K2CPU copie une table INTEGER depuis le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_CP2CPU(SELF, ISIZ, BUFF, BID)

      CLASS(GP_DVOCL_SELF_T), INTENT(IN) :: SELF
      INTEGER(8),INTENT(IN)    :: ISIZ
      BYTE,      INTENT(INOUT) :: BUFF(:)
      INTEGER(8),INTENT(IN)    :: BID

      INCLUDE 'err.fi'

      INTEGER CLRET
      INTEGER IBUF
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Cherche le buffer
      DO IBUF=1, SELF%IBUF_MAX
         IF (SELF%BUFF(IBUF)%GET() .EQ. BID) EXIT
      ENDDO
D     CALL ERR_ASR(IBUF .LE. SELF%IBUF_MAX)

C---     Copie les données
      CLRET = SELF%CMDQ%ENQUEUE_READ_BUFFER(SELF%BUFF(IBUF),
     &                                      BUFF,
     &                                      ISIZ)

      GP_DVOCL_CP2CPU = GP_DVOCL_ERRMSG(CLRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DVOCL_K2DEV copie une table INTEGER vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_CP2DEV(SELF, ISIZ, BUFF, BID)

      CLASS(GP_DVOCL_SELF_T), INTENT(IN) :: SELF
      INTEGER(8),INTENT(IN) :: ISIZ
      BYTE,      INTENT(IN) :: BUFF(:)
      INTEGER(8),INTENT(IN) :: BID

      INCLUDE 'err.fi'

      INTEGER CLRET
      INTEGER IBUF
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Cherche le buffer
      DO IBUF=1, SELF%IBUF_MAX
         IF (SELF%BUFF(IBUF)%GET() .EQ. BID) EXIT
      ENDDO
D     CALL ERR_ASR(IBUF .LE. SELF%IBUF_MAX)

C---     Copie les données
      CLRET = SELF%CMDQ%ENQUEUE_WRITE_BUFFER(SELF%BUFF(IBUF),
     &                                       BUFF,
     &                                       ISIZ)

      GP_DVOCL_CP2DEV = GP_DVOCL_ERRMSG(CLRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DVOCL_ARGBUF assigne l'argument I comme buffer.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_ARGBUF(SELF, I, V)

      CLASS(GP_DVOCL_SELF_T), INTENT(IN) :: SELF
      INTEGER,   INTENT(IN) :: I
      INTEGER(8),INTENT(IN) :: V

      INCLUDE 'err.fi'

      INTEGER CLRET
      INTEGER IBUF
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Cherche le buffer
      DO IBUF=1, SELF%IBUF_MAX
         IF (SELF%BUFF(IBUF)%GET() .EQ. V) EXIT
      ENDDO
D     CALL ERR_ASR(IBUF .LE. SELF%IBUF_MAX)

C---     Copie les données
      CLRET = SELF%KRNL%SET_ARG_BUF(I, SELF%BUFF(IBUF))

      GP_DVOCL_ARGBUF = GP_DVOCL_ERRMSG(CLRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode GP_DVOCL_LOAD charge un kernel.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     FNAM     Nom du fichier
C     KNAM     Nom du kernel
C
C Sortie:
C     INFO(1) = LOCAL_MEM_SIZE
C     INFO(2) = PRIVATE_MEM_SIZE
C     INFO(3) = PREFERRED_WORK_GROUP_SIZE_MULTIPLE
C     INFO(4) = WORK_GROUP_SIZE
C
C Notes:
C     prgm devrait stocker un crc32 du nom ou du code et charger au besoin
C     krnl devrait stocker un crc32 du nom ou du code et charger au besoin
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_LOAD(SELF, FNAM, KNAM, INFO)

      CLASS(GP_DVOCL_SELF_T), INTENT(INOUT) :: SELF
      CHARACTER*(*), INTENT(IN) :: FNAM
      CHARACTER*(*), INTENT(IN) :: KNAM
      INTEGER,       INTENT(OUT):: INFO(4)

      INCLUDE 'c_fa.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'

      INTEGER*8 :: XFIC
      INTEGER   :: IRET, MR
      INTEGER(C_INT32_T)  :: CLRET
      TYPE(OCL_DEVICE_T)  :: DEVC
      TYPE(OCL_PROGRAM_T) :: PRGM
      TYPE(OCL_KERNEL_T)  :: KRNL
      !CHARACTER*(2), PARAMETER :: OPTS = '  '
      LOGICAL ESTMAITRE
      CHARACTER*(1024) :: OPTS
      CHARACTER*(256)  :: BUF
C------------------------------------------------------------------------
      LOGICAL CL_OK
      CL_OK() = (CLRET .EQ. CL_SUCCESS)
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.gpu.device.cl'
      
      CLRET = CL_SUCCESS

C---     SEUL I_MASTER LIS LES ARGUMENTS
      ESTMAITRE = MP_UTIL_ESTMAITRE()
!!      IF (.NOT. ESTMAITRE) GOTO 9999

C---     Build options list
      BUF = FNAM
      IRET = C_OS_DIRNAME(BUF(1:SP_STRN_LEN(BUF)))
      OPTS = '-I "' // BUF(1:SP_STRN_LEN(BUF)) // '"'
      BUF = FNAM(1:SP_STRN_LEN(FNAM)) // '.options'
      XFIC = 0
      IF (ESTMAITRE) THEN
         XFIC = C_FA_OUVRE(BUF(1:SP_STRN_LEN(BUF)), C_FA_LECTURE)
         IF (XFIC .EQ. 0) THEN
            WRITE(LOG_BUF, '(A)') 'MSG_FICHIER_CFG_ABSENT'
            CALL LOG_WRN(LOG_ZNE, LOG_BUF)
            WRITE(LOG_BUF, '(2A)') 'MSG_FIC:', BUF(1:SP_STRN_LEN(BUF))
            CALL LOG_WRN(LOG_ZNE, LOG_BUF)
         ENDIF
      ENDIF
      IF (XFIC .NE. 0) THEN
100      CONTINUE
            IRET = C_FA_LISLN(XFIC, BUF)
            IF (IRET .EQ. -1) GOTO 109
            IF (IRET .NE.  0) GOTO 9901
            
            CALL SP_STRN_TCM(BUF)
            CALL SP_STRN_TRM(BUF)
            IF (SP_STRN_LEN(BUF) .GT. 0)
     &         OPTS = OPTS(1:SP_STRN_LEN(OPTS)) //
     &                ' ' // 
     &                BUF(1:SP_STRN_LEN(BUF))
         GOTO 100
109      CONTINUE
      ENDIF
      IF (XFIC . NE. 0) THEN
         IRET = C_FA_FERME(XFIC)
      ENDIF
      
C---     Load the program
      PRGM = SELF%PRGM
!      IF (CL_OK() .AND. PRGM%GET() .NE. 0) CLRET = OCL_PRGM_DTR(PRGM)
!      IF (CL_OK()) CLRET = OCL_PRGM_CTR_WITH_BINARY_FILE(PRGM,
!     &                                                   SELF%CTXT,
!     &                                                   MDL)
      IF (CL_OK()) CLRET = OCL_PRGM_CTR_WITH_FILE(PRGM, SELF%CTXT, FNAM)
      IF (CL_OK()) CLRET = PRGM%BUILD  (OPTS(1:SP_STRN_LEN(OPTS)))
      SELF%PRGM = PRGM

C---     Load the kernel
      KRNL = SELF%KRNL
      IF (CL_OK() .AND. KRNL%GET() .NE. 0) CLRET = OCL_KERNEL_DTR(KRNL)
      IF (CL_OK()) CLRET = OCL_KERNEL_CTR(KRNL, PRGM, KNAM)
      SELF%KRNL = KRNL

C---     Get kernel info
      IF (CL_OK()) THEN
         DEVC = SELF%DEVC
         INFO(1) = KRNL%GET_LOCAL_MEM_SIZE  (DEVC)
         INFO(2) = KRNL%GET_PRIVATE_MEM_SIZE(DEVC)
         INFO(3) = KRNL%GET_PREFERRED_WORK_GROUP_SIZE_MULTIPLE(DEVC)
         INFO(4) = KRNL%GET_WORK_GROUP_SIZE(DEVC)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER_CFG'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)

9999  CONTINUE
      GP_DVOCL_LOAD = GP_DVOCL_ERRMSG(CLRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_XEQ(SELF, NTSK)

      CLASS(GP_DVOCL_SELF_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NTSK

      INCLUDE 'err.fi'

      INTEGER(C_INT32_T) :: CLRET
      INTEGER(C_SIZE_T)  :: GWS
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      GWS = NTSK
      CLRET = SELF%CMDQ%ENQUEUE_1D_RANGE_KERNEL(SELF%KRNL, GWS)
D     IF (CLRET .EQ. CL_SUCCESS) CLRET = SELF%CMDQ%FINISH()

      GP_DVOCL_XEQ = GP_DVOCL_ERRMSG(CLRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_DVOCL_ERRMSG traduis le message d'erreur OpenCL
C     en H2D2.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GP_DVOCL_ERRMSG(CLRET)

      USE clfortran
      USE ISO_C_BINDING
      USE OCL_UTIL_M

      INTEGER, INTENT(IN) :: CLRET

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      CHARACTER*(64) MSG
C------------------------------------------------------------------------------

      IF (CLRET .EQ. CL_SUCCESS) GOTO 9999

      MSG = ' '
      SELECT CASE (CLRET)

         CASE (OCL_MEMORY_ALLOCATION)
            MSG = 'OCL_MEMORY_ALLOCATION'
         CASE (OCL_INVALID_PLATFORM_COUNT)
            MSG = 'OCL_INVALID_PLATFORM_COUNT'
         CASE (OCL_INVALID_PLATFORM_INDEX)
            MSG = 'OCL_INVALID_PLATFORM_INDEX'
         CASE (OCL_INVALID_DEVICE_COUNT)
            MSG = 'OCL_INVALID_DEVICE_COUNT'
         CASE (OCL_INVALID_DEVICE_INDEX)
            MSG = 'OCL_INVALID_DEVICE_INDEX'

         CASE (CL_DEVICE_NOT_FOUND)
            MSG = 'CL_DEVICE_NOT_FOUND'
         CASE (CL_DEVICE_NOT_AVAILABLE)
            MSG = 'CL_DEVICE_NOT_AVAILABLE'
         CASE (CL_COMPILER_NOT_AVAILABLE)
            MSG = 'CL_COMPILER_NOT_AVAILABLE'
         CASE (CL_MEM_OBJECT_ALLOCATION_FAILURE)
            MSG = 'CL_MEM_OBJECT_ALLOCATION_FAILURE'
         CASE (CL_OUT_OF_RESOURCES)
            MSG = 'CL_OUT_OF_RESOURCES'
         CASE (CL_OUT_OF_HOST_MEMORY)
            MSG = 'CL_OUT_OF_HOST_MEMORY'
         CASE (CL_PROFILING_INFO_NOT_AVAILABLE)
            MSG = 'CL_PROFILING_INFO_NOT_AVAILABLE'
         CASE (CL_MEM_COPY_OVERLAP)
            MSG = 'CL_MEM_COPY_OVERLAP'
         CASE (CL_IMAGE_FORMAT_MISMATCH)
            MSG = 'CL_IMAGE_FORMAT_MISMATCH'
         CASE (CL_IMAGE_FORMAT_NOT_SUPPORTED)
            MSG = 'CL_IMAGE_FORMAT_NOT_SUPPORTED'
         CASE (CL_BUILD_PROGRAM_FAILURE)
            MSG = 'CL_BUILD_PROGRAM_FAILURE'
         CASE (CL_MAP_FAILURE)
            MSG = 'CL_MAP_FAILURE'
         CASE (CL_MISALIGNED_SUB_BUFFER_OFFSET)
            MSG = 'CL_MISALIGNED_SUB_BUFFER_OFFSET'
         CASE (CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST)
            MSG = 'CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST'
         CASE (CL_COMPILE_PROGRAM_FAILURE)
            MSG = 'CL_COMPILE_PROGRAM_FAILURE'
         CASE (CL_LINKER_NOT_AVAILABLE)
            MSG = 'CL_LINKER_NOT_AVAILABLE'
         CASE (CL_LINK_PROGRAM_FAILURE)
            MSG = 'CL_LINK_PROGRAM_FAILURE'
         CASE (CL_DEVICE_PARTITION_FAILED)
            MSG = 'CL_DEVICE_PARTITION_FAILED'
         CASE (CL_KERNEL_ARG_INFO_NOT_AVAILABLE)
            MSG = 'CL_KERNEL_ARG_INFO_NOT_AVAILABLE'
         CASE (CL_INVALID_VALUE)
            MSG = 'CL_INVALID_VALUE'
         CASE (CL_INVALID_DEVICE_TYPE)
            MSG = 'CL_INVALID_DEVICE_TYPE'
         CASE (CL_INVALID_PLATFORM)
            MSG = 'CL_INVALID_PLATFORM'
         CASE (CL_INVALID_DEVICE)
            MSG = 'CL_INVALID_DEVICE'
         CASE (CL_INVALID_CONTEXT)
            MSG = 'CL_INVALID_CONTEXT'
         CASE (CL_INVALID_QUEUE_PROPERTIES)
            MSG = 'CL_INVALID_QUEUE_PROPERTIES'
         CASE (CL_INVALID_COMMAND_QUEUE)
            MSG = 'CL_INVALID_COMMAND_QUEUE'
         CASE (CL_INVALID_HOST_PTR)
            MSG = 'CL_INVALID_HOST_PTR'
         CASE (CL_INVALID_MEM_OBJECT)
            MSG = 'CL_INVALID_MEM_OBJECT'
         CASE (CL_INVALID_IMAGE_FORMAT_DESCRIPTOR)
            MSG = 'CL_INVALID_IMAGE_FORMAT_DESCRIPTOR'
         CASE (CL_INVALID_IMAGE_SIZE)
            MSG = 'CL_INVALID_IMAGE_SIZE'
         CASE (CL_INVALID_SAMPLER)
            MSG = 'CL_INVALID_SAMPLER'
         CASE (CL_INVALID_BINARY)
            MSG = 'CL_INVALID_BINARY'
         CASE (CL_INVALID_BUILD_OPTIONS)
            MSG = 'CL_INVALID_BUILD_OPTIONS'
         CASE (CL_INVALID_PROGRAM)
            MSG = 'CL_INVALID_PROGRAM'
         CASE (CL_INVALID_PROGRAM_EXECUTABLE)
            MSG = 'CL_INVALID_PROGRAM_EXECUTABLE'
         CASE (CL_INVALID_KERNEL_NAME)
            MSG = 'CL_INVALID_KERNEL_NAME'
         CASE (CL_INVALID_KERNEL_DEFINITION)
            MSG = 'CL_INVALID_KERNEL_DEFINITION'
         CASE (CL_INVALID_KERNEL)
            MSG = 'CL_INVALID_KERNEL'
         CASE (CL_INVALID_ARG_INDEX)
            MSG = 'CL_INVALID_ARG_INDEX'
         CASE (CL_INVALID_ARG_VALUE)
            MSG = 'CL_INVALID_ARG_VALUE'
         CASE (CL_INVALID_ARG_SIZE)
            MSG = 'CL_INVALID_ARG_SIZE'
         CASE (CL_INVALID_KERNEL_ARGS)
            MSG = 'CL_INVALID_KERNEL_ARGS'
         CASE (CL_INVALID_WORK_DIMENSION)
            MSG = 'CL_INVALID_WORK_DIMENSION'
         CASE (CL_INVALID_WORK_GROUP_SIZE)
            MSG = 'CL_INVALID_WORK_GROUP_SIZE'
         CASE (CL_INVALID_WORK_ITEM_SIZE)
            MSG = 'CL_INVALID_WORK_ITEM_SIZE'
         CASE (CL_INVALID_GLOBAL_OFFSET)
            MSG = 'CL_INVALID_GLOBAL_OFFSET'
         CASE (CL_INVALID_EVENT_WAIT_LIST)
            MSG = 'CL_INVALID_EVENT_WAIT_LIST'
         CASE (CL_INVALID_EVENT)
            MSG = 'CL_INVALID_EVENT'
         CASE (CL_INVALID_OPERATION)
            MSG = 'CL_INVALID_OPERATION'
         CASE (CL_INVALID_GL_OBJECT)
            MSG = 'CL_INVALID_GL_OBJECT'
         CASE (CL_INVALID_BUFFER_SIZE)
            MSG = 'CL_INVALID_BUFFER_SIZE'
         CASE (CL_INVALID_MIP_LEVEL)
            MSG = 'CL_INVALID_MIP_LEVEL'
         CASE (CL_INVALID_GLOBAL_WORK_SIZE)
            MSG = 'CL_INVALID_GLOBAL_WORK_SIZE'
         CASE (CL_INVALID_PROPERTY)
            MSG = 'CL_INVALID_PROPERTY'
         CASE (CL_INVALID_IMAGE_DESCRIPTOR)
            MSG = 'CL_INVALID_IMAGE_DESCRIPTOR'
         CASE (CL_INVALID_COMPILER_OPTIONS)
            MSG = 'CL_INVALID_COMPILER_OPTIONS'
         CASE (CL_INVALID_LINKER_OPTIONS)
            MSG = 'CL_INVALID_LINKER_OPTIONS'
         CASE (CL_INVALID_DEVICE_PARTITION_COUNT)
            MSG = 'CL_INVALID_DEVICE_PARTITION_COUNT'
      END SELECT

      IF (SP_STRN_LEN(MSG) .GT. 0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_' // MSG(1:SP_STRN_LEN(MSG)))
      ENDIF

9999  CONTINUE
      GP_DVOCL_ERRMSG = ERR_TYP()
      RETURN
      END

      END MODULE GP_DVOCL_M
