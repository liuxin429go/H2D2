C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  GPu
C Objet:   DEVICE INFOrmation
C Type:    Concret
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************
C$NOREFERENCE

C---     Type de donnée associé à un device
C        Elle permet de faire l'échange de données
C        entre les codes couplés au device (OpenCL, CUDA) et
C        le code plus générique GP_DEVC
      MODULE GP_DEVICE_INFO_M

      IMPLICIT NONE

      TYPE :: GP_DEVICE_INFO_T
         SEQUENCE
         INTEGER(8) :: MIN_WITM_PER_WGRP  = 0
         INTEGER(8) :: MAX_WITM_PER_WGRP  = 0
         INTEGER(8) :: MIN_WGRP_PER_CU    = 0
         INTEGER(8) :: MAX_WGRP_PER_CU    = 0
         INTEGER(8) :: MIN_CU_PER_DEV     = 0
         INTEGER(8) :: MAX_CU_PER_DEV     = 0
         INTEGER(8) :: MAX_WITM_PER_DEV   = 0
         INTEGER(8) :: MEM_SIZE           = 0
      END TYPE GP_DEVICE_INFO_T

      END MODULE GP_DEVICE_INFO_M

C$REFERENCE
