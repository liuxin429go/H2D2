C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER GP_CLCL_000
C     INTEGER GP_CLCL_999
C     INTEGER GP_CLCL_CTR
C     INTEGER GP_CLCL_DTR
C     INTEGER GP_CLCL_INI
C     INTEGER GP_CLCL_RST
C     INTEGER GP_CLCL_REQHBASE
C     LOGICAL GP_CLCL_HVALIDE
C     INTEGER GP_CLCL_XEQ
C   Private:
C     SUBROUTINE GP_CLCL_REQSELF
C     INTEGER GP_CLCL_RAZ
C
C************************************************************************

!Chaque tâche GPU est responsable d'assembler des noeuds (lignes) consécutives de la matrice.
!Elle a toute l'information pour y arriver. La charge de travaille est répartie en équilibrant
!le nombre d'éléments par tâche GPU.
!
! Les tâches sont toutes synchrones (SIMD). Elles travaillent sur des blocs de lignes distincts
! et donc sont complètement indépendantes, sans possibilité de race.
!
!Chaque tâche OMP gère un certains nombre de blocs de tâches GPU, qui globalement représente
!des noeuds (lignes) consécutives de la matrice. Chaque tâche OMP pagine le travail, en fonction
!de la taille de la mémoire globale du device GPU.
!
!L'information est transférée de table nodales vers des tables élémentaires. Il y a répétition
!de l'information des noeuds communs. De plus, pour avoir des accès mémoire coalescents,
!les informations des tâches se suivent:
!   VDLG_P(1:NDLN, IN, ) = VDLG(1:NDLN, KNE(IN))
!
!


! Le module GP_CLCL_M correspond au travail global par tous les OMP
      MODULE GP_CLCL_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: GP_CLCL_HBASE = 0

C---     Type de donnée associé à la classe
      INTEGER, PARAMETER :: NCL1_MAX = 16
      TYPE :: GP_CLCL_SELF_T
         INTEGER HCNF            ! Handle sur la configuration
         INTEGER NCL1            ! Nombre de tâcherons
         INTEGER HCL1(NCL1_MAX)
      END TYPE GP_CLCL_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée GP_CLCL_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLCL_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (GP_CLCL_SELF_T), POINTER :: GP_CLCL_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (GP_CLCL_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      GP_CLCL_REQSELF => SELF
      RETURN
      END FUNCTION GP_CLCL_REQSELF

      END MODULE GP_CLCL_M
