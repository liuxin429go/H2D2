C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     INTEGER GP_DEVC_CU_INI
C     INTEGER GP_DEVC_CU_PRN
C     INTEGER GP_DEVC_CU_ALLRE8
C     INTEGER GP_DEVC_CU_ALLINT
C     INTEGER GP_DEVC_CU_START
C     INTEGER GP_DEVC_CU_K2DEV
C     INTEGER GP_DEVC_CU_V2DEV
C
C************************************************************************

C************************************************************************
C Sommaire: Enregistre des devices
C
C Description:
C     La fonction GP_DEVC_INICL enregistre à l'aide du call-back de handle HCB
C     les devices de la plate-forme CUDA d'indice IP.
C
C Entrée:
C     HCB      Handle sur le call-back pour enregistrer les devices
C     IP       Indice de la plate-forme CUDA
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CU_INI(HOBJ, IP, ID)

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IP
      INTEGER ID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevt.fi'
      INCLUDE 'gpcuda.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      TYPE (GP_DEVICE_T) :: DEVC
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Le device
      IERR = GP_CUDA_REQDEV(DEVC, IP, ID)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF => GP_DEVC_REQSELF(HOBJ)
         SELF%PLATFORM_ID        = DEVC%PLATFORM_ID
         SELF%DEVICE_ID          = DEVC%DEVICE_ID
         SELF%MIN_WITM_PER_WGRP  = DEVC%MIN_WITM_PER_WGRP
         SELF%MAX_WITM_PER_WGRP  = DEVC%MAX_WITM_PER_WGRP
         SELF%MIN_WGRP_PER_CU    = DEVC%MIN_WGRP_PER_CU
         SELF%MAX_WGRP_PER_CU    = DEVC%MAX_WGRP_PER_CU
         SELF%MIN_CU_PER_DEV     = DEVC%MIN_CU_PER_DEV
         SELF%MAX_CU_PER_DEV     = DEVC%MAX_CU_PER_DEV
         SELF%MAX_WITM_PER_DEV   = DEVC%MAX_WITM_PER_DEV
         SELF%GLOBAL_MEM_SIZE    = DEVC%MEM_SIZE
      ENDIF

      GP_DEVC_CU_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Imprime CUDA
C
C Description:
C     La fonction GP_CUDA_PRN imprime les plateformes CUDA et leurs
C     devices.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CU_PRN()

      IMPLICIT NONE

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpcuda.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      WRITE(LOG_BUF, *) 'CUDA configuration'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = GP_CUDA_PRN()
      CALL LOG_DECIND()

      GP_DEVC_CU_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_CU_ALLRE8
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CU_ALLRE8(HOBJ, BID, ILEN)

      USE ISO_C_BINDING
      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 BID
      INTEGER   ILEN

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER   IERR
      INTEGER*8 CTX
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      CTX = SELF%CONTEXT_ID

      IERR = GP_CUDA_ALLRE8(CTX, BID, ILEN)

      GP_DEVC_CU_ALLRE8 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_CU_ALLINT
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CU_ALLINT(HOBJ, BID, ILEN)

      USE ISO_C_BINDING
      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 BID
      INTEGER   ILEN

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER   IERR
      INTEGER*8 CTX
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      CTX = SELF%CONTEXT_ID

      IERR = GP_CUDA_ALLINT(CTX, BID, ILEN)

      GP_DEVC_CU_ALLINT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_CU_START
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CU_START(HOBJ)


      USE GP_DEVC_M
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER   IERR
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)

C---     Create a context
!      num_devices = 1
!      ctx_props(1) = CL_CONTEXT_PLATFORM
!      ctx_props(2) = SELF%PLATFORM_ID
!      ctx_props(3) = 0
!      device_id = SELF%DEVICE_ID

!      ctxt = clCreateContext(C_LOC(ctx_props),
!     &                       1, ! num_devices,
!     &                       C_LOC(device_id),
!     &                       C_NULL_FUNPTR,
!     &                       C_NULL_PTR, IRET)

C---     Create a command queue.
!      cmd_queue_props = 0
!      cmd_queue = clCreateCommandQueue(ctxt, device_id, cmd_queue_props, IRET)

      GP_DEVC_CU_START = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_CU_K2DEV copie une table INTEGER vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CU_K2DEV(HOBJ, N, K, BID)

      USE GP_DEVC_M
      IMPLICIT NONE
      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   INTENT(IN) :: N
      INTEGER,   INTENT(IN) :: K(*)
      INTEGER*8, INTENT(IN) :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER   IERR
      INTEGER*8 CQ
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Les attributs
      SELF => GP_DEVC_REQSELF(HOBJ)
      CQ = SELF%CMDQUEUE_ID

      IERR = GP_CUDA_K2DEV(CQ, N, K, BID)

      GP_DEVC_CU_K2DEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_CU_V2DEV copie une table REAL*8 vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CU_V2DEV(HOBJ, N, V, BID)

      USE GP_DEVC_M
      IMPLICIT NONE
      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   INTENT(IN) :: N
      REAL*8,    INTENT(IN) :: V(*)
      INTEGER*8, INTENT(IN) :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER   IERR
      INTEGER*8 CQ
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Les attributs
      SELF => GP_DEVC_REQSELF(HOBJ)
      CQ = SELF%CMDQUEUE_ID

      IERR = GP_CUDA_V2DEV(CQ, N, V, BID)

      GP_DEVC_CU_V2DEV = ERR_TYP()
      RETURN
      END
