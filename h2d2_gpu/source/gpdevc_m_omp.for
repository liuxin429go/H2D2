C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     INTEGER GP_DEVC_OMP_INI
C     INTEGER GP_DEVC_OMP_PRN
C     INTEGER GP_DEVC_OMP_ALLRE8
C     INTEGER GP_DEVC_OMP_ALLINT
C     INTEGER GP_DEVC_OMP_START
C     INTEGER GP_DEVC_OMP_K2DEV
C     INTEGER GP_DEVC_OMP_V2DEV
C
C************************************************************************

C************************************************************************
C Sommaire: Enregistre des devices
C
C Description:
C     La fonction GP_DEVC_OMP_INI
C
C Entrée:
C     IP       Indice de la plate-forme OMP
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_OMP_INI(HOBJ, IP, ID)

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IP
      INTEGER ID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF

      INTEGER OMP_GET_MAX_THREADS
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (IP .NE. 1) GOTO 9900
      IF (ID .NE. 1) GOTO 9901


C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF => GP_DEVC_REQSELF(HOBJ)
         SELF%PLATFORM_ID        = IP
         SELF%DEVICE_ID          = ID
         SELF%MIN_WITM_PER_WGRP  = 1
         SELF%MAX_WITM_PER_WGRP  = 1   !! 1024
         SELF%MIN_WGRP_PER_CU    = 1
         SELF%MAX_WGRP_PER_CU    = 1
         SELF%MIN_CU_PER_DEV     = 1
         SELF%MAX_CU_PER_DEV     = 1
!$       SELF%MAX_CU_PER_DEV     = OMP_GET_MAX_THREADS()
!!!         SELF%GLOBAL_MEM_SIZE    = DEVC%MEM_SIZE
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_OMP_INVALID_PLATFORM_INDEX')
      GOTO 9999
9901  CALL ERR_ASG(ERR_ERR, 'ERR_OMP_INVALID_DEVICE_INDEX')
      GOTO 9999

9999  CONTINUE
      GP_DEVC_OMP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Imprime OpenCL
C
C Description:
C     La fonction GP_OPNCL_PRN imprime les plates-formes OpenCL et leurs
C     devices.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_OMP_PRN()

      IMPLICIT NONE

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      WRITE(LOG_BUF, *) 'OMP configuration'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
!!      IERR = GP_OPNCL_PRN()
      CALL LOG_DECIND()

      GP_DEVC_OMP_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_OMP_ALLRE8
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_OMP_ALLRE8(HOBJ, BID, ILEN)

      USE ISO_C_BINDING
      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 BID
      INTEGER   ILEN

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER L
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SO_ALLC_ALLRE8(ILEN, L)
      BID = L

      GP_DEVC_OMP_ALLRE8 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_OMP_ALLINT
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_OMP_ALLINT(HOBJ, BID, ILEN)

      USE ISO_C_BINDING
      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 BID
      INTEGER   ILEN

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER L
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SO_ALLC_ALLINT(ILEN, L)
      BID = L

      GP_DEVC_OMP_ALLINT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_OMP_START
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_OMP_START(HOBJ)

      USE GP_DEVC_M
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      GP_DEVC_OMP_START = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_OMP_K2DEV copie une table INTEGER vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_OMP_K2DEV(HOBJ, N, K, BID)

      USE GP_DEVC_M
      IMPLICIT NONE
      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   INTENT(IN) :: N
      INTEGER,   INTENT(IN) :: K(*)
      INTEGER*8, INTENT(IN) :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER L
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      L = BID
      CALL ICOPY(N, K, 1, KA(SO_ALLC_REQKIND(KA,L)), 1)

      GP_DEVC_OMP_K2DEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_OMP_V2DEV copie une table REAL*8 vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_OMP_V2DEV(HOBJ, N, V, BID)

      USE GP_DEVC_M
      IMPLICIT NONE
      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   INTENT(IN) :: N
      REAL*8,    INTENT(IN) :: V(*)
      INTEGER*8, INTENT(IN) :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER L
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      L = BID
      CALL DCOPY(N, V, 1, VA(SO_ALLC_REQVIND(VA,L)), 1)

      GP_DEVC_OMP_V2DEV = ERR_TYP()
      RETURN
      END
