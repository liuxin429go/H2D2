C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUS_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrslus.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslus.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_SLUS_NOBJMAX,
     &                   MR_SLUS_HBASE,
     &                   'SuperLU - Single Process')

      MR_SLUS_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUS_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrslus.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslus.fc'

      INTEGER  IERR
      EXTERNAL MR_SLUS_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_SLUS_NOBJMAX,
     &                   MR_SLUS_HBASE,
     &                   MR_SLUS_DTR)

      MR_SLUS_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SLUS_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslus.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslus.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_SLUS_NOBJMAX,
     &                   MR_SLUS_HBASE)
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_SLUS_HBASE

         MR_SLUS_XSLU(IOB) = 0
         MR_SLUS_HMTX(IOB) = 0
         MR_SLUS_HASM(IOB) = 0
         MR_SLUS_ICRC(IOB) = 0
      ENDIF

      MR_SLUS_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SLUS_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslus.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslus.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_SLUS_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_SLUS_NOBJMAX,
     &                   MR_SLUS_HBASE)

      MR_SLUS_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SLUS_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslus.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mrslus.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ILU
      INTEGER HASM
      INTEGER HMTX
      PARAMETER (ILU = 0)
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MR_SLUS_RST(HOBJ)

C---     INITIALISE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_MORS_INI(HMTX, ILU)

C---     INITIALISE L'ASSEMBLEUR
      IF (ERR_GOOD()) IERR = MA_MORS_CTR(HASM)
      IF (ERR_GOOD()) IERR = MA_MORS_INI(HASM, HMTX)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_SLUS_HBASE
         MR_SLUS_XSLU(IOB) = 0
         MR_SLUS_HMTX(IOB) = HMTX
         MR_SLUS_HASM(IOB) = HASM
         MR_SLUS_ICRC(IOB) = 0
      ENDIF

      MR_SLUS_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SLUS_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslus.fi'
      INCLUDE 'c_slus.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mrslus.fc'

      INTEGER*8 XSLU       ! handle eXterne
      INTEGER   IOB
      INTEGER   IERR
      INTEGER   HASM
      INTEGER   HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MR_SLUS_HBASE

      HASM = MR_SLUS_HASM(IOB)
      IF (MA_MORS_HVALIDE(HASM)) IERR = MA_MORS_DTR(HASM)

      HMTX = MR_SLUS_HMTX(IOB)
      IF (MX_MORS_HVALIDE(HMTX)) IERR = MX_MORS_DTR(HMTX)

      XSLU = MR_SLUS_XSLU(IOB)
      IF (XSLU .NE. 0) IERR = C_SLUS_RESET(XSLU)

      MR_SLUS_XSLU(IOB) = 0
      MR_SLUS_HMTX(IOB) = 0
      MR_SLUS_HASM(IOB) = 0
      MR_SLUS_ICRC(IOB) = 0

      MR_SLUS_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MR_SLUS_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUS_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrslus.fi'
      INCLUDE 'mrslus.fc'
C------------------------------------------------------------------------

      MR_SLUS_REQHBASE = MR_SLUS_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_SLUS_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUS_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslus.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrslus.fc'
C------------------------------------------------------------------------

      MR_SLUS_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_SLUS_NOBJMAX,
     &                                  MR_SLUS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la méthode de résolution est directe
C
C Description:
C     La fonction MR_SLUS_ESTDIRECTE retourne .TRUE. si la méthode de
C     résolution est directe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUS_ESTDIRECTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_ESTDIRECTE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslus.fi'
C------------------------------------------------------------------------

      MR_SLUS_ESTDIRECTE = .TRUE.
      RETURN
      END

C************************************************************************
C Sommaire: Assemble le système matriciel.
C
C Description:
C     La fonction MR_SLUS_ASMMTX assemble le système matriciel.
C     La matrice est dimensionnée et assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUS_ASMMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_K,
     &                        F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_ASMMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'mrslus.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mrslus.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HASM
      INTEGER HMTX
      INTEGER NEQ
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.assemblage')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_SLUS_HBASE
      HMTX  = MR_SLUS_HMTX(IOB)
      HASM  = MR_SLUS_HASM(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))

C---     DIMENSIONNE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_DIMMAT(HMTX, HSIM)

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = MA_MORS_ASMMTX(HASM, HSIM, HALG, F_K)

      CALL TR_CHRN_STOP('h2d2.reso.mat.assemblage')
      MR_SLUS_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise le système matriciel
C
C Description:
C     La fonction MR_SLUS_FCTMTX factorise le système matriciel. La matrice
C     doit être assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUS_FCTMTX(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_FCTMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslus.fi'
      INCLUDE 'c_slus.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrslus.fc'

      INTEGER*8 XSLU       ! handle eXterne
      INTEGER   IERR
      INTEGER   IOB
      INTEGER   HMTX
      INTEGER   ICRC
      INTEGER   LIAP, LJAP, LKG
      INTEGER   NEQL, NKGP
      LOGICAL   DODIM, ESTCCS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.factorisation')

C---     Récupère les attributs
      IOB = HOBJ - MR_SLUS_HBASE
      HMTX = MR_SLUS_HMTX(IOB)
      XSLU = MR_SLUS_XSLU(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     Récupère les données de la matrice
      ICRC = MX_MORS_REQCRC32(HMTX)
      NEQL = MX_MORS_REQNEQL (HMTX)
      NKGP = MX_MORS_REQNKGP (HMTX)
      LIAP = MX_MORS_REQLIAP (HMTX)
      LJAP = MX_MORS_REQLJAP (HMTX)
      LKG  = MX_MORS_REQLKG  (HMTX)

C---     Test si la matrice doit être dimensionnée
      DODIM = (ICRC .NE. MR_SLUS_ICRC(IOB))

C---     Reset SuperLU si redimensionnement
      IF (DODIM .AND. XSLU .NE. 0) THEN
         IERR = C_SLUS_RESET(XSLU)
         IF (IERR .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_SUPERLU_INIT')
         XSLU = 0
         IF (ERR_GOOD()) MR_SLUS_XSLU(IOB) = XSLU
      ENDIF

C---     Initialise SuperLU
      IF (XSLU .EQ. 0) THEN
         ESTCCS = .FALSE.
         IERR = C_SLUS_INIT(XSLU, NEQL, ESTCCS)
         IF (IERR .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_SUPERLU_INIT')
         IF (ERR_GOOD()) MR_SLUS_XSLU(IOB) = XSLU
      ENDIF

C---     Factorise
      IF (ERR_GOOD()) THEN
         IERR = C_SLUS_FACT (XSLU,
     &                       NEQL,
     &                       NKGP,
     &                       VA(SO_ALLC_REQVIND(VA,LKG)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)))
         IF (IERR .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_SUPERLU_FACT')
      ENDIF

C---     Conserve le CRC
      IF (ERR_GOOD()) THEN
         MR_SLUS_ICRC(IOB) = ICRC
      ENDIF

      CALL TR_CHRN_STOP('h2d2.reso.mat.factorisation')
      MR_SLUS_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel
C
C Description:
C     La fonction MR_SLUS_RESMTX résout le système matriciel avec
C     un second membre. Le second membre est assemblé si les handles
C     sur la simulation HSIM et sur l'algorithme HALG sont tous deux non nuls,
C     sinon on résout directement avec VSOL.
C     La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUS_RESMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_F,
     &                        VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_RESMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_F

      INCLUDE 'mrslus.fi'
      INCLUDE 'c_slus.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mrslus.fc'

      INTEGER*8 XSLU       ! handle eXterne
      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX, HASM
D     INTEGER ICRC
      INTEGER NEQL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.resolution')

C---     Récupère les attributs
      IOB = HOBJ - MR_SLUS_HBASE
      HMTX = MR_SLUS_HMTX(IOB)
      HASM = MR_SLUS_HASM(IOB)
      XSLU = MR_SLUS_XSLU(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))
D     CALL ERR_ASR(XSLU .NE. 0)

C---     Récupère les données de la matrice
      NEQL = MX_MORS_REQNEQL(HMTX)
D     ICRC = MX_MORS_REQCRC32(HMTX)
D     CALL ERR_ASR(ICRC .EQ. MR_SLUS_ICRC(IOB))

C---     Assemble le membre de droite
      IF (ERR_GOOD() .AND. HSIM .NE. 0 .AND. HALG .NE. 0)
     &   IERR = MA_MORS_ASMRHS(HASM, HSIM, HALG, F_F, VSOL)

C---     RESOUD
      IF (ERR_GOOD()) THEN
         IERR = C_SLUS_SOLVE (XSLU,
     &                        NEQL,
     &                        1,
     &                        VSOL)
         IF (IERR .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_SUPERLU_SOLVE')
      ENDIF

      CALL TR_CHRN_STOP('h2d2.reso.mat.resolution')
      MR_SLUS_RESMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel.
C
C Description:
C     La fonction MR_SLUS_XEQ résous complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUS_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_SLUS_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrslus.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslus.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ASSEMBLE LA MATRICE
      IF(ERR_GOOD())IERR=MR_SLUS_ASMMTX(HOBJ,HSIM,HALG,F_K,F_KU)

C---     FACTORISE LA MATRICE
      IF(ERR_GOOD())IERR=MR_SLUS_FCTMTX(HOBJ)

C---     RESOUS
      IF(ERR_GOOD())IERR=MR_SLUS_RESMTX(HOBJ,HSIM,HALG,F_F,VSOL)

      MR_SLUS_XEQ = ERR_TYP()
      RETURN
      END
