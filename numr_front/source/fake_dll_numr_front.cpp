//************************************************************************
// H2D2 - External declaration of public symbols
// Module: numr_front
// Entry point: extern "C" void fake_dll_numr_front()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:04.067000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_NM_FRNT
F_PROT(IC_NM_FRNT_XEQCTR, ic_nm_frnt_xeqctr);
F_PROT(IC_NM_FRNT_XEQMTH, ic_nm_frnt_xeqmth);
F_PROT(IC_NM_FRNT_REQCLS, ic_nm_frnt_reqcls);
F_PROT(IC_NM_FRNT_REQHDL, ic_nm_frnt_reqhdl);
 
// ---  class NM_FRNT
F_PROT(NM_FRNT_000, nm_frnt_000);
F_PROT(NM_FRNT_999, nm_frnt_999);
F_PROT(NM_FRNT_CTR, nm_frnt_ctr);
F_PROT(NM_FRNT_DTR, nm_frnt_dtr);
F_PROT(NM_FRNT_INI, nm_frnt_ini);
F_PROT(NM_FRNT_RST, nm_frnt_rst);
F_PROT(NM_FRNT_REQHBASE, nm_frnt_reqhbase);
F_PROT(NM_FRNT_HVALIDE, nm_frnt_hvalide);
F_PROT(NM_FRNT_RENUM, nm_frnt_renum);
F_PROT(NM_FRNT_GENNUM, nm_frnt_gennum);
F_PROT(NM_FRNT_SAUVE, nm_frnt_sauve);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_numr_front()
{
   static char libname[] = "numr_front";
 
   // ---  class IC_NM_FRNT
   F_RGST(IC_NM_FRNT_XEQCTR, ic_nm_frnt_xeqctr, libname);
   F_RGST(IC_NM_FRNT_XEQMTH, ic_nm_frnt_xeqmth, libname);
   F_RGST(IC_NM_FRNT_REQCLS, ic_nm_frnt_reqcls, libname);
   F_RGST(IC_NM_FRNT_REQHDL, ic_nm_frnt_reqhdl, libname);
 
   // ---  class NM_FRNT
   F_RGST(NM_FRNT_000, nm_frnt_000, libname);
   F_RGST(NM_FRNT_999, nm_frnt_999, libname);
   F_RGST(NM_FRNT_CTR, nm_frnt_ctr, libname);
   F_RGST(NM_FRNT_DTR, nm_frnt_dtr, libname);
   F_RGST(NM_FRNT_INI, nm_frnt_ini, libname);
   F_RGST(NM_FRNT_RST, nm_frnt_rst, libname);
   F_RGST(NM_FRNT_REQHBASE, nm_frnt_reqhbase, libname);
   F_RGST(NM_FRNT_HVALIDE, nm_frnt_hvalide, libname);
   F_RGST(NM_FRNT_RENUM, nm_frnt_renum, libname);
   F_RGST(NM_FRNT_GENNUM, nm_frnt_gennum, libname);
   F_RGST(NM_FRNT_SAUVE, nm_frnt_sauve, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
