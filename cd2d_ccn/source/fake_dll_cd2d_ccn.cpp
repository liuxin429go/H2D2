//************************************************************************
// H2D2 - External declaration of public symbols
// Module: cd2d_ccn
// Entry point: extern "C" void fake_dll_cd2d_ccn()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:46:56.038629
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class CD2D_CCN
F_PROT(CD2D_CCN_ASGCMN, cd2d_ccn_asgcmn);
F_PROT(CD2D_CCN_HLPPRGL, cd2d_ccn_hlpprgl);
F_PROT(CD2D_CCN_HLPPRNO, cd2d_ccn_hlpprno);
F_PROT(CD2D_CCN_REQNFN, cd2d_ccn_reqnfn);
F_PROT(CD2D_CCN_REQPRM, cd2d_ccn_reqprm);
 
// ---  class CD2D_CCNC
F_PROT(CD2D_CCNC_000, cd2d_ccnc_000);
F_PROT(CD2D_CCNC_REQHBASE, cd2d_ccnc_reqhbase);
F_PROT(CD2D_CCNC_ASGCMN, cd2d_ccnc_asgcmn);
F_PROT(CD2D_CCNC_REQNFN, cd2d_ccnc_reqnfn);
F_PROT(CD2D_CCNC_REQPRM, cd2d_ccnc_reqprm);
 
// ---  class CD2D_CCNN
F_PROT(CD2D_CCNN_000, cd2d_ccnn_000);
F_PROT(CD2D_CCNN_REQHBASE, cd2d_ccnn_reqhbase);
F_PROT(CD2D_CCNN_ASGCMN, cd2d_ccnn_asgcmn);
F_PROT(CD2D_CCNN_REQNFN, cd2d_ccnn_reqnfn);
F_PROT(CD2D_CCNN_REQPRM, cd2d_ccnn_reqprm);
 
// ---  class IC_CD2D
F_PROT(IC_CD2D_CCNC_XEQCTR, ic_cd2d_ccnc_xeqctr);
F_PROT(IC_CD2D_CCNC_XEQMTH, ic_cd2d_ccnc_xeqmth);
F_PROT(IC_CD2D_CCNC_OPBDOT, ic_cd2d_ccnc_opbdot);
F_PROT(IC_CD2D_CCNC_REQCLS, ic_cd2d_ccnc_reqcls);
F_PROT(IC_CD2D_CCNC_REQHDL, ic_cd2d_ccnc_reqhdl);
F_PROT(IC_CD2D_CCNN_XEQCTR, ic_cd2d_ccnn_xeqctr);
F_PROT(IC_CD2D_CCNN_XEQMTH, ic_cd2d_ccnn_xeqmth);
F_PROT(IC_CD2D_CCNN_OPBDOT, ic_cd2d_ccnn_opbdot);
F_PROT(IC_CD2D_CCNN_REQCLS, ic_cd2d_ccnn_reqcls);
F_PROT(IC_CD2D_CCNN_REQHDL, ic_cd2d_ccnn_reqhdl);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_cd2d_ccn()
{
   static char libname[] = "cd2d_ccn";
 
   // ---  class CD2D_CCN
   F_RGST(CD2D_CCN_ASGCMN, cd2d_ccn_asgcmn, libname);
   F_RGST(CD2D_CCN_HLPPRGL, cd2d_ccn_hlpprgl, libname);
   F_RGST(CD2D_CCN_HLPPRNO, cd2d_ccn_hlpprno, libname);
   F_RGST(CD2D_CCN_REQNFN, cd2d_ccn_reqnfn, libname);
   F_RGST(CD2D_CCN_REQPRM, cd2d_ccn_reqprm, libname);
 
   // ---  class CD2D_CCNC
   F_RGST(CD2D_CCNC_000, cd2d_ccnc_000, libname);
   F_RGST(CD2D_CCNC_REQHBASE, cd2d_ccnc_reqhbase, libname);
   F_RGST(CD2D_CCNC_ASGCMN, cd2d_ccnc_asgcmn, libname);
   F_RGST(CD2D_CCNC_REQNFN, cd2d_ccnc_reqnfn, libname);
   F_RGST(CD2D_CCNC_REQPRM, cd2d_ccnc_reqprm, libname);
 
   // ---  class CD2D_CCNN
   F_RGST(CD2D_CCNN_000, cd2d_ccnn_000, libname);
   F_RGST(CD2D_CCNN_REQHBASE, cd2d_ccnn_reqhbase, libname);
   F_RGST(CD2D_CCNN_ASGCMN, cd2d_ccnn_asgcmn, libname);
   F_RGST(CD2D_CCNN_REQNFN, cd2d_ccnn_reqnfn, libname);
   F_RGST(CD2D_CCNN_REQPRM, cd2d_ccnn_reqprm, libname);
 
   // ---  class IC_CD2D
   F_RGST(IC_CD2D_CCNC_XEQCTR, ic_cd2d_ccnc_xeqctr, libname);
   F_RGST(IC_CD2D_CCNC_XEQMTH, ic_cd2d_ccnc_xeqmth, libname);
   F_RGST(IC_CD2D_CCNC_OPBDOT, ic_cd2d_ccnc_opbdot, libname);
   F_RGST(IC_CD2D_CCNC_REQCLS, ic_cd2d_ccnc_reqcls, libname);
   F_RGST(IC_CD2D_CCNC_REQHDL, ic_cd2d_ccnc_reqhdl, libname);
   F_RGST(IC_CD2D_CCNN_XEQCTR, ic_cd2d_ccnn_xeqctr, libname);
   F_RGST(IC_CD2D_CCNN_XEQMTH, ic_cd2d_ccnn_xeqmth, libname);
   F_RGST(IC_CD2D_CCNN_OPBDOT, ic_cd2d_ccnn_opbdot, libname);
   F_RGST(IC_CD2D_CCNN_REQCLS, ic_cd2d_ccnn_reqcls, libname);
   F_RGST(IC_CD2D_CCNN_REQHDL, ic_cd2d_ccnn_reqhdl, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
