C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de convection-diffusion eulérienne 2-D
C     Contaminant conservatif (CCN)
C     Formulation conservative pour (QC).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CD2D_CCNC_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CD2D_CCNC_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'cd2d_ccnc_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'lmlgcy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER HOBJ, HKID, HPRNT
      INTEGER HGRID
      INTEGER HDLIB
      INTEGER HCLIM
      INTEGER HSOLC
      INTEGER HSOLR
      INTEGER HPRGL
      INTEGER HPRNO
      INTEGER HPREL
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_CD2D_CCNC_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_CD2D_CCN_FNC'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the mesh</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HGRID)
C     <comment>Handle on the degrees of freedom (unknowns)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HDLIB)
C     <comment>Handle on the boundary conditions</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HCLIM)
C     <comment>Handle on the concentrated solicitations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, HSOLC)
C     <comment>Handle on the distributed solicitations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 5, HSOLR)
C     <comment>Handle on the global properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 6, HPRGL)
C     <comment>Handle on the nodal properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 7, HPRNO)
C     <comment>Handle on the elemental properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 8, HPREL)
      IF (IERR .NE. 0) GOTO 9901

C---     Construis l'objet
      HKID = 0
      HOBJ = 0
      IF (ERR_GOOD()) IERR = LM_LGCY_CTR(HOBJ,
     &                                   HKID,
     &                                   'cd2d_fc_conservatif',
     &                                   EG_TPGEO_T3)
      IF (ERR_GOOD()) IERR = LM_LGCY_INI(HOBJ,
     &                                   HGRID,
     &                                   HDLIB,
     &                                   HCLIM,
     &                                   HSOLC,
     &                                   HSOLR,
     &                                   HPRGL,
     &                                   HPRNO,
     &                                   HPREL)

C---     Impression des paramètres du bloc
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Retourne la handle sur le parent virtuel
      IF (ERR_GOOD()) THEN
         HPRNT = LM_LGCY_REQHPRN(HOBJ)
C        <comment>Return value: Handle on the element</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HPRNT
      ENDIF

C<comment>
C  The constructor <b>cd2d_ccn_cf</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CD2D_CCNC_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_CD2D_CCNC_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_CD2D_CCNC_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type CD2D_CCNN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CD2D_CCNC_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CD2D_CCNC_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'cd2d_ccnc_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmlgcy.fi'
      INCLUDE 'lmelem_ic.fi'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_LGCY_DTR(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_CD2D_CCNC_AID()

      ELSE
C        <include>IC_LM_ELEM_XEQMTH@lmelem_ic.for</include>
         IERR = IC_LM_ELEM_XEQMTH(HOBJ, IMTH, IPRM)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CD2D_CCNC_AID()

9999  CONTINUE
      IC_CD2D_CCNC_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_CD2D_CCNC_OPBDOT(...) exécute les méthodes statiques
C     sur la classe CD2D_CCNN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CD2D_CCNC_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CD2D_CCNC_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'cd2d_ccnc_ic.fi'
      INCLUDE 'cd2d_ccnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmlgcy.fi'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(HOBJ .EQ. CD2D_CCNC_REQHBASE() .OR.
D    &             HOBJ .EQ. LM_LGCY_REQHBASE())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

         CALL IC_CD2D_CCNC_AID()
         CALL CD2D_CCN_HLPPRGL()
         CALL CD2D_CCN_HLPPRNO()
!!!         CALL CD2D_AGE_HLPCLIM()

C     <comment>The method <b>help_prgl</b> prints the global properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prgl') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL CD2D_CCN_HLPPRGL()

C     <comment>The method <b>help_prno</b> prints the nodal properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prno') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL CD2D_CCN_HLPPRNO()

!!!C     <comment>The method <b>help_bc</b> prints the boundary conditions of the class.</comment>
!!!      ELSEIF (IMTH .EQ. 'help_bc') THEN
!!!         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
!!!         CALL CD2D_CCN_HLPCLIM()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CD2D_CCNC_AID()

9999  CONTINUE
      IC_CD2D_CCNC_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CD2D_CCNC_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CD2D_CCNC_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_ccnc_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>cd2d_ccn_cf</b> is a finite element for the 2D
C  advection-diffusion equations for a conservative contaminant.
C  The formulation is conservative, i.e. the unknown
C  is Q.C the specific discharge x concentration.
C</comment>
      IC_CD2D_CCNC_REQCLS = 'cd2d_ccn_cf'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CD2D_CCNC_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CD2D_CCNC_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_ccnc_ic.fi'
      INCLUDE 'lmlgcy.fi'
      INCLUDE 'cd2d_ccnc.fi'
C-------------------------------------------------------------------------

      IC_CD2D_CCNC_REQHDL = CD2D_CCNC_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_HMCOMP_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_CD2D_CCNC_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('cd2d_ccnc_ic.hlp')
      RETURN
      END
      