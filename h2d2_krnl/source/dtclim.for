C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DT_CLIM_000
C     INTEGER DT_CLIM_999
C     INTEGER DT_CLIM_PKL
C     INTEGER DT_CLIM_UPK
C     INTEGER DT_CLIM_CTR
C     INTEGER DT_CLIM_DTR
C     INTEGER DT_CLIM_INI
C     INTEGER DT_CLIM_RST
C     INTEGER DT_CLIM_REQHBASE
C     LOGICAL DT_CLIM_HVALIDE
C     INTEGER DT_CLIM_CHARGE
C     INTEGER DT_CLIM_REQHLIMT
C     INTEGER DT_CLIM_REQNCLCND
C     INTEGER DT_CLIM_REQLCLCND
C     INTEGER DT_CLIM_REQNCLCNV
C     INTEGER DT_CLIM_REQLCLCNV
C     INTEGER DT_CLIM_REQNCLLIM
C     INTEGER DT_CLIM_REQNCLNOD
C     INTEGER DT_CLIM_REQNCLELE
C     INTEGER DT_CLIM_REQLCLLIM
C     INTEGER DT_CLIM_REQLCLNOD
C     INTEGER DT_CLIM_REQLCLELE
C     INTEGER DT_CLIM_REQLCLDST
C   Private:
C     INTEGER DT_CLIM_RAZ
C     INTEGER DT_CLIM_TRANSF
C     INTEGER DT_CLIM_LIELIMCND
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>DT_CLIM_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtclim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(DT_CLIM_NOBJMAX,
     &                   DT_CLIM_HBASE,
     &                   'Boundary Condition')

      DT_CLIM_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtclim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtclim.fc'

      INTEGER  IERR
      EXTERNAL DT_CLIM_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(DT_CLIM_NOBJMAX,
     &                   DT_CLIM_HBASE,
     &                   DT_CLIM_DTR)

      DT_CLIM_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I, L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_CLIM_HBASE

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WD_1(HXML, DT_CLIM_TEMPS (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, DT_CLIM_HLIMT (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_CLIM_NHCND (IOB))
      DO I=1,DT_CLIM_NHCND(IOB)
         IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, DT_CLIM_KHCND(IOB,I))
      ENDDO
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_CLIM_NCLCND(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_CLIM_NCLCNV(IOB))

C---     La table KCLCND
      IF (ERR_GOOD()) THEN
         N = 4 *        DT_CLIM_NCLCND(IOB)
         NX= 4 * MAX(1, DT_CLIM_NCLCND(IOB))
         L = DT_CLIM_LCLCND(IOB)
         IERR = IO_XML_WI_V(HXML, NX, N, L, 1)
      ENDIF

C---     La table VCLCNV
      IF (ERR_GOOD()) THEN
         N =        DT_CLIM_NCLCNV(IOB)
         NX= MAX(1, DT_CLIM_NCLCNV(IOB))
         L = DT_CLIM_LCLCNV(IOB)
         IERR = IO_XML_WD_V(HXML, NX, N, L, 1)
      ENDIF

      DT_CLIM_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I, L, N, NX
      LOGICAL LEXIST
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_CLIM_HBASE

C---     Initialise l'objet
      IF (LNEW) IERR = DT_CLIM_RAZ(HOBJ)
      IERR = DT_CLIM_RST(HOBJ)

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RD_1(HXML, DT_CLIM_TEMPS (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, DT_CLIM_HLIMT (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_CLIM_NHCND (IOB))
      DO I=1,DT_CLIM_NHCND(IOB)
         IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, DT_CLIM_KHCND(IOB,I))
      ENDDO
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_CLIM_NCLCND(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_CLIM_NCLCNV(IOB))

C---     La table KCLCND
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RI_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. 4*MAX(1,DT_CLIM_NCLCND(IOB)))
      DT_CLIM_LCLCND(IOB) = L

C---     La table VNOD
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. MAX(1,DT_CLIM_NCLCNV(IOB)))
      DT_CLIM_LCLCNV(IOB) = L

      DT_CLIM_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   DT_CLIM_NOBJMAX,
     &                                   DT_CLIM_HBASE)
      IF (ERR_GOOD()) IERR = DT_CLIM_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. DT_CLIM_HVALIDE(HOBJ))

      DT_CLIM_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode DT_COND_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IOB = HOBJ - DT_CLIM_HBASE

      DT_CLIM_TEMPS (IOB) = -1.0D0
      DT_CLIM_HLIMT (IOB) = 0
      DT_CLIM_NHCND (IOB) = 0
      DT_CLIM_NCLCND(IOB) = 0
      DT_CLIM_LCLCND(IOB) = 0
      DT_CLIM_NCLCNV(IOB) = 0
      DT_CLIM_LCLCNV(IOB) = 0

      DT_CLIM_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtclim.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = DT_CLIM_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   DT_CLIM_NOBJMAX,
     &                   DT_CLIM_HBASE)

      DT_CLIM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le nombre de condition ne peut être nul. On devrait peut-être le
C     permettre pour des cas avec des CL naturelles partout (bassin
C     adiabatique par exemple).
C************************************************************************
      FUNCTION DT_CLIM_INI(HOBJ, HLIMT, NHCND, KHCND)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLIMT
      INTEGER NHCND
      INTEGER KHCND(*)

      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtcond.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER J
      INTEGER NCLCND, NCLLIM
      INTEGER NCLCNV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLES DES PARAMETRES
      IF (NHCND .LE. 0) GOTO 9900
      IF (.NOT. DT_LIMT_HVALIDE(HLIMT)) GOTO 9901
      DO J=1, NHCND
         IF ( .NOT. DT_COND_HVALIDE(KHCND(J))) GOTO 9902
      ENDDO

C---     GATHER LES DIMENSIONS
      NCLCND = 0
      NCLCNV = 0
      DO J = 1,NHCND
         NCLCND = NCLCND + DT_COND_REQNCLCND(KHCND(J))
         NCLCNV = NCLCNV + DT_COND_REQNCLCNV(KHCND(J))
      ENDDO
      NCLLIM = DT_LIMT_REQNCLLIM(HLIMT)

C---     CONTROLES
      IF (NCLLIM .NE. NCLCND) GOTO 9903

C---     RESET LES DONNEES
      IERR = DT_CLIM_RST(HOBJ)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_CLIM_HBASE
         DT_CLIM_HLIMT(IOB) = HLIMT
         DT_CLIM_NHCND(IOB) = NHCND
         DO J = 1,NHCND
            DT_CLIM_KHCND(IOB, J) = KHCND(J)
         ENDDO
         DT_CLIM_NCLCND(IOB) = NCLCND
         DT_CLIM_LCLCND(IOB) = 0
         DT_CLIM_NCLCNV(IOB) = NCLCNV
         DT_CLIM_LCLCNV(IOB) = 0
         DT_CLIM_TEMPS (IOB) = -1.0D0
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
C9900  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_NBR_PROP_NODALE_INVALIDE',': ',
C     &   NCLIML_TMP,' /',NCLIML
C      CALL ERR_ASG(ERR_ERR, ERR_BUF)
C      GOTO 9999
9900  WRITE(ERR_BUF,'(2A,I3)')'ERR_NBR_HANDLE_INVALIDE',':',NHCND
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',':',HLIMT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',':',KHCND(J)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(A,2(A,I6))') 'ERR_CLIM_INCOHERENT',':',
     &                        NCLLIM, ' / ', NCLCND
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_CLIM_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LCLCND
      INTEGER LCLCNV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DT_CLIM_HBASE

C---     RECUPERE LES DONNEES
      LCLCND = DT_CLIM_LCLCND(IOB)
      LCLCNV = DT_CLIM_LCLCNV(IOB)

C---     DESALLOUE LA MEMOIRE
      IF (LCLCNV .NE. 0) IERR = SO_ALLC_ALLRE8(0, LCLCNV)
      IF (LCLCND .NE. 0) IERR = SO_ALLC_ALLINT(0, LCLCND)

C---     RESET LES PARAMETRES
      IERR = DT_CLIM_RAZ(HOBJ)

      DT_CLIM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DT_CLIM_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtclim.fc'
C------------------------------------------------------------------------

      DT_CLIM_REQHBASE = DT_CLIM_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DT_CLIM_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtclim.fc'
C------------------------------------------------------------------------

      DT_CLIM_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  DT_CLIM_NOBJMAX,
     &                                  DT_CLIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: DT_CLIM_CHARGE
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_CHARGE (HOBJ, HNUMR, TNEW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      REAL*8  TNEW

      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtcond.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtclim.fc'

      INTEGER ICLCND
      INTEGER ICLCNV
      INTEGER IERR
      INTEGER IOB
      INTEGER HCOND
      INTEGER HLIMT
      INTEGER LCLCND
      INTEGER LCLCNV
      INTEGER LCLLIM
      INTEGER NCLCND
      INTEGER NCLCNV
      INTEGER NCLLIM
      INTEGER NHCND
      INTEGER J
      INTEGER LCLCND_S
      INTEGER LCLCNV_S
      INTEGER NCLCND_S
      INTEGER NCLCNV_S
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère l'indice
      IOB = HOBJ - DT_CLIM_HBASE

C---     Récupère les données de l'objet
      HLIMT  = DT_CLIM_HLIMT (IOB)
      NHCND  = DT_CLIM_NHCND (IOB)
      NCLCND = DT_CLIM_NCLCND(IOB)
      LCLCND = DT_CLIM_LCLCND(IOB)
      NCLCNV = DT_CLIM_NCLCNV(IOB)
      LCLCNV = DT_CLIM_LCLCNV(IOB)

C---     (Ré)-alloue l'espace
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(4*MAX(1,NCLCND), LCLCND)
      IF (ERR_GOOD()) DT_CLIM_LCLCND(IOB) = LCLCND
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(  MAX(1,NCLCNV), LCLCNV)
      IF (ERR_GOOD()) DT_CLIM_LCLCNV(IOB) = LCLCNV

C---     Lis les limites
      IF (ERR_GOOD()) IERR = DT_LIMT_CHARGE(HLIMT, HNUMR, TNEW)

C---     Lis les conditions de limites et transfert
      IF (ERR_BAD()) GOTO 199
      ICLCND = 0
      ICLCNV = 0
      DO J = 1,NHCND
         HCOND = DT_CLIM_KHCND(IOB,J)
         IERR  = DT_COND_CHARGE(HCOND, HNUMR, TNEW)
         IF (ERR_BAD()) GOTO 199

         NCLCND_S = DT_COND_REQNCLCND(HCOND)
         LCLCND_S = DT_COND_REQLCLCND(HCOND)
         NCLCNV_S = DT_COND_REQNCLCNV(HCOND)
         LCLCNV_S = DT_COND_REQLCLCNV(HCOND)

         IERR = DT_CLIM_TRANSF(NCLCND_S,
     &                         KA(SO_ALLC_REQKIND(KA, LCLCND_S)),
     &                         NCLCNV_S,
     &                         VA(SO_ALLC_REQVIND(VA, LCLCNV_S)),
     &                         ICLCND,
     &                         NCLCND,
     &                         KA(SO_ALLC_REQKIND(KA, LCLCND)),
     &                         ICLCNV,
     &                         NCLCNV,
     &                         VA(SO_ALLC_REQVIND(VA, LCLCNV)))

         IF (ERR_BAD()) GOTO 199
         ICLCND = ICLCND + NCLCND_S
         ICLCNV = ICLCNV + NCLCNV_S
      ENDDO
199   CONTINUE

C---     Conserve le temps
      IF (ERR_GOOD()) THEN
         DT_CLIM_TEMPS(IOB) = TNEW
      ENDIF

C---     Fait les liens limites-conditions
      IF (ERR_GOOD() .AND. NCLCND .GT. 0) THEN
         NCLLIM = DT_CLIM_REQNCLLIM(HOBJ)
         LCLLIM = DT_CLIM_REQLCLLIM(HOBJ)
         IERR = DT_CLIM_LIELIMCND(NCLCND,
     &                            KA(SO_ALLC_REQKIND(KA, LCLCND)),
     &                            NCLLIM,
     &                            KA(SO_ALLC_REQKIND(KA, LCLLIM)))
      ENDIF

      DT_CLIM_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_TRANSF(NCLCND_S,
     &                        KCLCND_S,
     &                        NCLCNV_S,
     &                        VCLCNV_S,
     &                        ICLCND_D,
     &                        NCLCND_D,
     &                        KCLCND_D,
     &                        ICLCNV_D,
     &                        NCLCNV_D,
     &                        VCLCNV_D)

      IMPLICIT NONE

      INTEGER DT_CLIM_TRANSF

      INTEGER NCLCND_S
      INTEGER NCLCNV_S
      INTEGER KCLCND_S(4,NCLCND_S)
      REAL*8  VCLCNV_S(  NCLCNV_S)
      INTEGER ICLCND_D
      INTEGER ICLCNV_D
      INTEGER NCLCND_D
      INTEGER NCLCNV_D
      INTEGER KCLCND_D(4, NCLCND_D)
      REAL*8  VCLCNV_D(   NCLCNV_D)

      INCLUDE 'err.fi'

      INTEGER IC
C-----------------------------------------------------------------------

      DO IC=1, NCLCND_S
         KCLCND_D(1,IC+ICLCND_D) = KCLCND_S(1,IC)
         KCLCND_D(2,IC+ICLCND_D) = KCLCND_S(2,IC)
         KCLCND_D(3,IC+ICLCND_D) = KCLCND_S(3,IC) + ICLCNV_D
         KCLCND_D(4,IC+ICLCND_D) = KCLCND_S(4,IC) + ICLCNV_D
      ENDDO

      DO IC=1, NCLCNV_S
         VCLCNV_D(IC+ICLCNV_D) = VCLCNV_S(IC)
      ENDDO

      DT_CLIM_TRANSF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_LIELIMCND(NCLCND,
     &                           KCLCND,
     &                           NCLLIM,
     &                           KCLLIM)

      IMPLICIT NONE

      INTEGER DT_CLIM_LIELIMCND

      INTEGER NCLCND
      INTEGER KCLCND(4, NCLCND)
      INTEGER NCLLIM
      INTEGER KCLLIM(7, NCLLIM)

      INCLUDE 'err.fi'

      INTEGER IC
      INTEGER IL
      INTEGER INOM
C-----------------------------------------------------------------------

C---     Fait les liens
      DO IL=1, NCLLIM
         INOM = KCLLIM(1,IL)
         DO IC=1, NCLCND
            IF (KCLCND(1,IC) .EQ. INOM) GOTO 199
         ENDDO
199      CONTINUE
         IF (IC .GT. NCLCND) GOTO 9900
         KCLLIM(2,IL) = IC
      ENDDO

C---     Contrôle
      DO IC=1, NCLCND
         INOM = KCLCND(1,IC)
         DO IL=1, NCLLIM
            IF (KCLLIM(1,IL) .EQ. INOM) GOTO 299
         ENDDO
299      CONTINUE
         IF (IL .GT. NCLLIM) GOTO 9901
      ENDDO

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A,I2)') 'ERR_LIMITE_SANS_CONDITION: ',IL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A,I2)') 'ERR_CONDITION_SANS_LIMITE: ',IC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_CLIM_LIELIMCND = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQHLIMT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQHLIMT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtclim.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_CLIM_REQHLIMT = DT_CLIM_HLIMT(HOBJ-DT_CLIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQNCLCND(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQNCLCND
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtclim.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_CLIM_REQNCLCND = DT_CLIM_NCLCND(HOBJ-DT_CLIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: DT_CLIM_REQLCLCND
C
C Description:
C     La fonction DT_CLIM_REQLCLCND retourne le pointeur à la table des
C     conditions maintenues par l'objet. La table est de dimensions
C     (7, NCLNCD). Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQLCLCND(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQLCLCND
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtclim.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_CLIM_REQLCLCND = DT_CLIM_LCLCND(HOBJ-DT_CLIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQNCLCNV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQNCLCNV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtclim.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_CLIM_REQNCLCNV = DT_CLIM_NCLCNV(HOBJ-DT_CLIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: DT_CLIM_REQLCLCNV
C
C Description:
C     La fonction DT_CLIM_REQLCLCNV retourne le pointeur à la table des
C     valeurs maintenues par l'objet. La table est de dimensions
C     (NCLNCV). Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQLCLCNV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQLCLCNV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtclim.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_CLIM_REQLCLCNV = DT_CLIM_LCLCNV(HOBJ-DT_CLIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQNCLLIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQNCLLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_CLIM_HLIMT(HOBJ-DT_CLIM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      IOB = HOBJ - DT_CLIM_HBASE
      DT_CLIM_REQNCLLIM = DT_LIMT_REQNCLLIM(DT_CLIM_HLIMT(IOB))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQNCLNOD(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQNCLNOD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_CLIM_HLIMT(HOBJ-DT_CLIM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      IOB = HOBJ - DT_CLIM_HBASE
      DT_CLIM_REQNCLNOD = DT_LIMT_REQNCLNOD(DT_CLIM_HLIMT(IOB))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQNCLELE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQNCLELE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_CLIM_HLIMT(HOBJ-DT_CLIM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      IOB = HOBJ - DT_CLIM_HBASE
      DT_CLIM_REQNCLELE = DT_LIMT_REQNCLELE(DT_CLIM_HLIMT(IOB))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQLCLLIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQLCLLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_CLIM_HLIMT(HOBJ-DT_CLIM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      IOB = HOBJ - DT_CLIM_HBASE
      DT_CLIM_REQLCLLIM = DT_LIMT_REQLCLLIM(DT_CLIM_HLIMT(IOB))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQLCLNOD(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQLCLNOD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_CLIM_HLIMT(HOBJ-DT_CLIM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      IOB = HOBJ - DT_CLIM_HBASE
      DT_CLIM_REQLCLNOD = DT_LIMT_REQLCLNOD(DT_CLIM_HLIMT(IOB))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQLCLELE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQLCLELE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_CLIM_HLIMT(HOBJ-DT_CLIM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      IOB = HOBJ - DT_CLIM_HBASE
      DT_CLIM_REQLCLELE = DT_LIMT_REQLCLELE(DT_CLIM_HLIMT(IOB))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_CLIM_REQLCLDST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_CLIM_REQLCLDST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtclim.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtclim.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_CLIM_HLIMT(HOBJ-DT_CLIM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      IOB = HOBJ - DT_CLIM_HBASE
      DT_CLIM_REQLCLDST = DT_LIMT_REQLCLDST(DT_CLIM_HLIMT(IOB))
      RETURN
      END

