C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Interface Commandes : TriGger
C Objet:   trigger INTeGer
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TG_INTG_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TG_INTG_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'tgintg_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tgintg_ic.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER NTRG
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_TG_INTG_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Trigger time step</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, NTRG)
      IF (IERR .NE. 0) GOTO 9901

C---     VALIDE
      IF (NTRG .LE. 0) GOTO 9902

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = TG_INTG_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = TG_INTG_INI(HOBJ, NTRG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_TG_INTG_PRN(HOBJ)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the trigger</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>trigger_integer</b> constructs an object, with the
C  given arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_INTERVAL_INVALIDE',': ', NTRG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_TG_INTG_AID()

9999  CONTINUE
      IC_TG_INTG_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TG_INTG_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TG_INTG_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'tgintg_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tgintg.fc'
      INCLUDE 'tgintg_ic.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
!      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_ASR(TG_INTG_HVALIDE(HOBJ))
         IOB = HOBJ - TG_INTG_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Trigger interval</comment>
         IF (PROP .EQ. 'ntrg') THEN
            IVAL = TG_INTG_ITRG(IOB)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

C---     SET
      ELSEIF (IMTH .EQ. '##property_set##') THEN
D        CALL ERR_ASR(TG_INTG_HVALIDE(HOBJ))
         IOB = HOBJ - TG_INTG_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

         IF (PROP .EQ. 'ntrg') THEN
            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
            IF (IERR .NE. 0) GOTO 9901
            TG_INTG_ITRG(IOB) = IVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(TG_INTG_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = TG_INTG_PRN(HOBJ)
         IERR = IC_TG_INTG_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_TG_INTG_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_TG_INTG_AID()

9999  CONTINUE
      IC_TG_INTG_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TG_INTG_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TG_INTG_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgintg_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>trigger_integer</b> represents the trigger based on an integer
C  value. The trigger is incremented at each time step and will be
C  activated when the modulo division of the count by the step is 0. For
C  example, if the integer step is 2 and the simulation time step is 5
C  minutes, the post-treatment would be activated every 10 minutes (2*5).
C</comment>
      IC_TG_INTG_REQCLS = 'trigger_integer'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TG_INTG_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TG_INTG_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgintg_ic.fi'
      INCLUDE 'tgintg.fi'
C-------------------------------------------------------------------------

      IC_TG_INTG_REQHDL = TG_INTG_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_TG_INTG_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_TG_INTG_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('tgintg_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TG_INTG_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'tgintg.fi'
      INCLUDE 'tgintg.fc'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tgintg_ic.fc'

      INTEGER IERR
      INTEGER IOB
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_TRIGGER_INTEGER'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      IOB = HOBJ - TG_INTG_HBASE

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      WRITE (LOG_BUF,'(2A,I12)')  'MSG_INTERVAL#<35>#', '= ',
     &                         TG_INTG_ITRG(IOB)
      CALL LOG_ECRIS(LOG_BUF)


      CALL LOG_DECIND()

      IC_TG_INTG_PRN = ERR_TYP()
      RETURN
      END
