C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: FD_LIMT
C     INTEGER FD_LIMT_000
C     INTEGER FD_LIMT_999
C     INTEGER FD_LIMT_CTR
C     INTEGER FD_LIMT_DTR
C     INTEGER FD_LIMT_INI
C     INTEGER FD_LIMT_RST
C     LOGICAL FD_LIMT_HVALIDE
C     INTEGER FD_LIMT_LIS
C     INTEGER FD_LIMT_REQNCLLIM
C     INTEGER FD_LIMT_REQNCLNOD
C     CHARACTER*256 FD_LIMT_REQNOMF
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les attributs static de la classe
C
C Description:
C     Le block data FD_LIMT_DATA_000 initialise les attributs
C     statics de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA FD_LIMT_DATA_000

      IMPLICIT NONE

      INCLUDE 'fdlimt.fc'

      DATA FD_LIMT_FCRC /' '/

      END

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction FD_LIMT_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(FD_LIMT_NOBJMAX,
     &                   FD_LIMT_HBASE,
     &                   'Fichier de Limite')

      FD_LIMT_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction FD_LIMT_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER  IERR
      EXTERNAL FD_LIMT_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(FD_LIMT_NOBJMAX,
     &                   FD_LIMT_HBASE,
     &                   FD_LIMT_DTR)

      FD_LIMT_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'fdlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_LIMT_HBASE

C---     Pickle
      IERR = IO_XML_WS  (HXML, FD_LIMT_FNAME (IOB))
      IERR = IO_XML_WI_1(HXML, FD_LIMT_NCLBLK(IOB))
      IERR = IO_XML_WI_1(HXML, FD_LIMT_NCLLIM(IOB))
      IERR = IO_XML_WI_1(HXML, FD_LIMT_NCLNOD(IOB))

      FD_LIMT_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'fdlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - FD_LIMT_HBASE

C---     Efface les attributs
      IERR = FD_LIMT_RAZ(HOBJ)

C---     Un-pickle
      IERR = IO_XML_RS  (HXML, FD_LIMT_FNAME (IOB))
      IERR = IO_XML_RI_1(HXML, FD_LIMT_NCLBLK(IOB))
      IERR = IO_XML_RI_1(HXML, FD_LIMT_NCLLIM(IOB))
      IERR = IO_XML_RI_1(HXML, FD_LIMT_NCLNOD(IOB))

      FD_LIMT_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction FD_LIMT_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdlimt.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)
      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER I_COMM, I_RANK, I_ERROR
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   FD_LIMT_NOBJMAX,
     &                                   FD_LIMT_HBASE)
      IF (ERR_GOOD()) IERR = FD_LIMT_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. FD_LIMT_HVALIDE(HOBJ))

C---     Crée le fichier du map CRC32-Nom de limites
      IF (ERR_GOOD() .AND. SP_STRN_LEN(FD_LIMT_FCRC) .EQ. 0) THEN
         I_COMM = MP_UTIL_REQCOMM()
         CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
         IF (ERR_GOOD() .AND. (I_RANK .EQ. I_MASTER)) THEN
            IRET = C_OS_FICTMP(FD_LIMT_FCRC)
            IF (IRET .NE. 0) THEN
               CALL ERR_ASG(ERR_ERR, 'ERR_CREE_FICTMP')
            ENDIF
         ENDIF
      ENDIF

      FD_LIMT_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction FD_LIMT_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = FD_LIMT_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   FD_LIMT_NOBJMAX,
     &                   FD_LIMT_HBASE)

      FD_LIMT_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode FD_LIMT_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdlimt.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_ASR(FD_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - FD_LIMT_HBASE

      FD_LIMT_FNAME (IOB) = ' '
      FD_LIMT_ISTAT (IOB) = IO_MODE_INDEFINI
      FD_LIMT_NCLBLK(IOB) = 0
      FD_LIMT_NCLLIM(IOB) = 0
      FD_LIMT_NCLNOD(IOB) = 0
      FD_LIMT_ESTLU (IOB) = .FALSE.

      FD_LIMT_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction FD_LIMT_INI initialise l'objet à l'aide des
C     paramètres. Le nom de fichier pour être égal à la valeur
C     retournée par IO_UTIL_FICDUM(), auquel cas le fichier est
C     amorphe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier de coordonnées
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_INI(HOBJ, NOMFIC, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT

      INCLUDE 'fdlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER FNAML
      INTEGER FDUML
      CHARACTER*256 FDUM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(NOMFIC) .GT. 0)
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.limt')

C---     Contrôle les paramètres
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9900
      IF (.NOT. IO_UTIL_MODVALIDE(ISTAT)) GOTO 9901

C---     Reset les données
      IERR = FD_LIMT_RST(HOBJ)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - FD_LIMT_HBASE
         FD_LIMT_FNAME(IOB) = NOMFIC(1:SP_STRN_LEN(NOMFIC))
         FD_LIMT_ISTAT(IOB) = ISTAT
      ENDIF

C---     Initialise la structure interne en lecture
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) THEN
         FNAML = SP_STRN_LEN(NOMFIC)
         FDUM  = IO_UTIL_FICDUM()
         FDUML = SP_STRN_LEN(FDUM)
         IF (ERR_GOOD() .AND. NOMFIC(1:FNAML) .NE. FDUM(1:FDUML)) THEN
            IERR = FD_LIMT_INIDIM(HOBJ, NOMFIC)
         ENDIF
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A, I6)') 'ERR_MODE_ACCES_INVALIDE',': ', ISTAT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.limt')
      FD_LIMT_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction FD_LIMT_RST reset l'objet, donc le remet dans
C     un état valide non initialisé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Efface les attributs
      IF (ERR_GOOD()) IERR = FD_LIMT_RAZ(HOBJ)

      FD_LIMT_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction FD_LIMT_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdlimt.fi'
      INCLUDE 'fdlimt.fc'
C------------------------------------------------------------------------

      FD_LIMT_REQHBASE = FD_LIMT_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction FD_LIMT_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdlimt.fc'
C------------------------------------------------------------------------

      FD_LIMT_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  FD_LIMT_NOBJMAX,
     &                                  FD_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: FD_LIMT_ECRIS
C
C Description:
C     La fonction FD_LIMT_ECRIS est la fonction principale d'écriture
C     d'un fichier de type LIMiTe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation des noeuds
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_ECRIS(HOBJ,
     &                       HNUMR,
     &                       NCLLIM,
     &                       KCLLIM,
     &                       NCLNOD,
     &                       KCLNOD,
     &                       TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_ECRIS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NCLLIM
      INTEGER KCLLIM(7, NCLLIM)
      INTEGER NCLNOD
      INTEGER KCLNOD(NCLNOD)
      REAL*8  TSIM

      INCLUDE 'fdlimt.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER I_MASTER
      INTEGER I_STATUS(MPI_STATUS_SIZE)
      PARAMETER (I_MASTER = 0)

      INTEGER LBLK_MAX
      PARAMETER (LBLK_MAX = 50)

      INTEGER IERR
      INTEGER IOB
      INTEGER ISTAT
      INTEGER IL, IB, IN
      INTEGER INDEB, INFIN
      INTEGER INOM
      INTEGER I_RANK, I_SIZE, I_ERROR, I_TAG, I_COMM
      INTEGER LFNAM, LNOM
      INTEGER M1
      INTEGER NBLK, LBLK, KTMP(LBLK_MAX)
      CHARACTER*(256) NOM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NCLLIM .GT. 0)
D     CALL ERR_PRE(NCLNOD .GT. 0)
D     CALL ERR_PRE(TSIM .GE. 0.0D0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.limt.write')

C---     Attributs
      IOB  = HOBJ - FD_LIMT_HBASE
      ISTAT = FD_LIMT_ISTAT(IOB)
      LFNAM = SP_STRN_LEN(FD_LIMT_FNAME(IOB))

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)

C---     Ouvre le fichier
      IERR = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         M1 = IO_UTIL_FREEUNIT()
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) THEN
            OPEN (UNIT  = M1,
     &            ERR   = 109,
     &            FILE  = FD_LIMT_FNAME(IOB)(1:LFNAM),
     &            FORM  = 'FORMATTED',
     &            STATUS= 'REPLACE')
         ELSE
            IERR = -2
         ENDIF
         GOTO 110
109      CONTINUE
         IERR = -1
110      CONTINUE
      ENDIF
C---     Broadcast le code d'erreur
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9900
      IF (IERR .EQ. -2) GOTO 9901
D     CALL ERR_ASR(IERR .EQ. 0)

C---     Compte le nombre de blocs, à 50 noeuds par bloc
      NBLK = 0
      DO IL=1, NCLLIM
         INDEB = KCLLIM(3,IL)
         INFIN = KCLLIM(4,IL)
         DO IN=INDEB, INFIN, LBLK_MAX
            NBLK = NBLK + 1
         ENDDO
      ENDDO

C---     Dispatch suivant le nombre process
      IF (I_SIZE .GT. 1) GOTO 1000

C---     Si on a un seul process
      WRITE (M1,'(I12)', ERR=9902) NBLK    ! Écris l'entête
      DO IL=1, NCLLIM
         INOM  = KCLLIM(1,IL)
         INDEB = KCLLIM(3,IL)
         INFIN = KCLLIM(4,IL)
         IERR = FD_LIMT_FCRC_REQVAL(INOM, NOM)
         LNOM = SP_STRN_LEN(NOM)
         DO IB=INDEB, INFIN, LBLK_MAX
            LBLK = MIN(LBLK_MAX,INFIN-IB+1)
            DO IN=1, LBLK
               KTMP(IN) = NM_NUMR_REQNINT(HNUMR, KCLNOD(IB+IN-1))
            ENDDO
            WRITE (M1,'(A,1X,50I12)',ERR=9902)
     &         NOM(1:LNOM), (KTMP(IN), IN=1,LBLK)
         ENDDO
      ENDDO
      GOTO 1999

C---     ECRIS LA SEQUENCE DES PROPRIETES NODALES
1000  CONTINUE
!!!      I_TAG = 12
!!!      DO IN=1,NCL
!!!         INL = NM_NUMR_REQNINT(HNUMR, IN)
!!!         IR  = -1
!!!         IF (INL .GT. 0) THEN
!!!            IF (NM_NUMR_ESTNOPP(HNUMR, INL)) IR = I_RANK
!!!         ENDIF
!!!         CALL MPI_ALLREDUCE(IR, IRSND, 1, MP_TYPE_INT(), MPI_MAX,
!!!     &                      I_COMM, I_ERROR)
!!!         IF (ERR_BAD()) GOTO 1999
!!!
!!!         IF (I_RANK .EQ. I_MASTER) THEN
!!!            IF (IRSND .EQ. I_RANK) THEN
!!!D              CALL ERR_ASR(INL .GT. 0 .AND. INL .LE. NNL)
!!!               CALL DCOPY(NPRN, VPRNO(1,INL), 1, VTMP, 1)
!!!            ELSE
!!!               CALL MPI_RECV(VTMP, NPRN, MP_TYPE_RE8(),
!!!     &                       IRSND,
!!!     &                       I_TAG, I_COMM, I_STATUS,I_ERROR)
!!!               IF (ERR_BAD()) GOTO 1999
!!!            ENDIF
!!!            IERR = 0
!!!            WRITE (M1,'(25(1PE26.17E3))',ERR=1009)
!!!     &               (VTMP(ID),ID=1,NPRN)
!!!            GOTO 1010
!!!1009        CONTINUE
!!!            IERR = -1
!!!1010        CONTINUE
!!!
!!!         ELSEIF (IRSND .EQ. I_RANK) THEN
!!!D           CALL ERR_ASR(INL .GT. 0 .AND. INL .LE. NNL)
!!!            CALL MPI_SEND(VPRNO(1,INL), NPRN, MP_TYPE_RE8(),
!!!     &                    I_MASTER,
!!!     &                    I_TAG, I_COMM, I_ERROR)
!!!            IF (ERR_BAD()) GOTO 1999
!!!         ENDIF
!!!
!!!C---        BROADCAST LE CODE D'ERREUR A CAUSE DU WRITE
!!!         CALL MPI_BCAST(IERR, 1, MP_TYPE_INT(), I_MASTER, I_COMM, I_ERROR)
!!!         IF (IERR .EQ. -1) GOTO 9902
!!!D        CALL ERR_ASR(IERR .EQ. 0)
!!!
!!!      ENDDO

C---     FERME LE FICHIER
1999  CONTINUE
      IF (I_RANK .EQ. I_MASTER) CLOSE(M1)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                       FD_LIMT_FNAME(IOB)(1:LFNAM)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_MODE_ACCES_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(3A)') 'ERR_ECRITURE_FICHIER',': ',
     &                       FD_LIMT_FNAME(IOB)(1:LFNAM)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.limt.write')
      FD_LIMT_ECRIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_FCND_LIS
C
C Description:
C     La fonction SP_FCND_LIS est la fonction principale de lecture
C     d'un fichier de type LIMiTe de condition limite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_LIS(HOBJ,
     &                     HNUMR,
     &                     NCLLIM,
     &                     KCLLIM,
     &                     NCLNOD,
     &                     KCLNOD,
     &                     TOLD,
     &                     TNEW,
     &                     MODIF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_LIS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NCLLIM
      INTEGER KCLLIM(7, NCLLIM)
      INTEGER NCLNOD
      INTEGER KCLNOD(NCLNOD)
      REAL*8  TOLD
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'fdlimt.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER FNAML
      INTEGER FDUML
      INTEGER NCLBLK
      LOGICAL ESTLU
      CHARACTER*256 FNAM
      CHARACTER*256 FDUM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NCLLIM .EQ. FD_LIMT_NCLLIM(HOBJ-FD_LIMT_HBASE))
D     CALL ERR_PRE(NCLNOD .EQ. FD_LIMT_NCLNOD(HOBJ-FD_LIMT_HBASE))
D     CALL ERR_PRE(TNEW .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      MODIF = .FALSE.

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.limt.read')

C---     Récupère l'indice
      IOB  = HOBJ - FD_LIMT_HBASE

C---     Récupère les attributs
      FNAM   = FD_LIMT_FNAME (IOB)
      NCLBLK = FD_LIMT_NCLBLK(IOB)
      ESTLU  = FD_LIMT_ESTLU (IOB)
      IF (ESTLU) GOTO 9999

C---     Lis la section
      FNAML = SP_STRN_LEN(FNAM)
      FDUM  = IO_UTIL_FICDUM()
      FDUML = SP_STRN_LEN(FDUM)
      IF (ERR_GOOD() .AND. FNAM(1:FNAML) .NE. FDUM(1:FDUML)) THEN
         IERR = FD_LIMT_LISSCT(HNUMR,
     &                         NCLBLK,
     &                         NCLLIM,
     &                         KCLLIM,
     &                         NCLNOD,
     &                         KCLNOD,
     &                         TOLD,
     &                         TNEW,
     &                         FNAM(1:FNAML))
      ENDIF

C---     Flag comme lu
      IF (ERR_GOOD()) FD_LIMT_ESTLU(IOB) = .TRUE.
      MODIF = .TRUE.

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.limt.read')
      FD_LIMT_LIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_LIMT_LISSCT
C
C Description:
C     La fonction privée FD_LIMT_LISSCT est la fonction principale
C     de lecture d'un fichier de type LIMiTe de condition limite.
C     <p>
C     En MPROC, pour que la table de noeuds reste compatible avec la
C     table des valeurs de la condition, la table des noeuds comprend
C     tous les noeuds en numérotation locale. Les noeuds qui ne sont pas
C     du process auront un NNO <= 0.
C
C Entrée:
C     HNUMR       Handle sur la renumérotation
C     NCLBLK      Nombre de blocs à lire
C     NCLLIM      Nombre de limites
C     NCLNOD      Nombre de noeuds de la tables des noeuds
C     TOLD        Temps initial : non utilisé
C     TNEW        Temps demandé : non utilisé
C     NOMFIC      Nom du fichier
C
C Sortie:
C     KCLLIM      Tables des limites
C     KCLNOD      Table des noeuds des limites
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_LISSCT(HNUMR,
     &                        NCLBLK,
     &                        NCLLIM,
     &                        KCLLIM,
     &                        NCLNOD,
     &                        KCLNOD,
     &                        TOLD,
     &                        TNEW,
     &                        NOMFIC)

      IMPLICIT NONE

      INTEGER        HNUMR
      INTEGER        NCLBLK
      INTEGER        NCLLIM
      INTEGER        KCLLIM(7, NCLLIM)
      INTEGER        NCLNOD
      INTEGER        KCLNOD(NCLNOD)
      REAL*8         TOLD
      REAL*8         TNEW
      CHARACTER*(*)  NOMFIC

      INCLUDE 'fdlimt.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER*8 XFIC       ! handle eXterne sur le fichier
      INTEGER   I, IC, IN, IL
      INTEGER   ICLNOD
      INTEGER   IERR, IRET
      INTEGER   ILGN
      INTEGER   ILBUF
      INTEGER   INDEB, INFIN
      INTEGER   INOM, ILNOM
      INTEGER   I_RANK, I_ERROR, I_COMM
D     INTEGER   NCLBLK_TMP
      INTEGER, PARAMETER :: LBUF = 1024
      CHARACTER*(LBUF) BUF
      CHARACTER*(256)  NOMCND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(TNEW .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     CONTROLES PRIMAIRES
      IF (TOLD .GE. 0) THEN
         IF (TNEW .EQ. TOLD) GOTO 9999
      ENDIF

C---     INITIALISE LES COMPTEURS
      ILGN = 0
      BUF  = ' '
      INDEB = 0
      INFIN = 0
      ICLNOD = 0

C---     INITIALISE LA TABLE
      CALL IINIT(7*NCLLIM, 0, KCLLIM, 1)

C---     OUVRE LE FICHIER
      IERR = 0
      XFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         XFIC = C_FA_OUVRE(NOMFIC(1:SP_STRN_LEN(NOMFIC)), C_FA_LECTURE)
         IF (XFIC .EQ. 0) IERR = -1
      ENDIF
C---     BROADCAST LE CODE D'ERREUR
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9900
D     CALL ERR_ASR(IERR .EQ. 0)

C---     LIS L'ENTETE
      IRET = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         ILGN = ILGN + 1
         IRET = C_FA_LISLN(XFIC, BUF)
         IF (IRET .NE. 0) GOTO 108
D        READ(BUF, *, ERR=108, END=109) NCLBLK_TMP
D        CALL ERR_ASR(NCLBLK_TMP .EQ. NCLBLK)
         GOTO 110
108      CONTINUE    ! ERR
         IERR = -1
         GOTO 110
109      CONTINUE    ! EOR
         IERR = -2
         GOTO 110
110      CONTINUE
      ENDIF
C---     BROADCAST LE CODE D'ERREUR
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9901   ! ERR
      IF (IERR .EQ. -2) GOTO 9902   ! EOR
D     CALL ERR_ASR(IERR .EQ. 0)


C---     POUR CHAQUE LIMITE
      IL = 0
      DO IC=1, NCLBLK

C---        LIS UNE LIGNE
         IERR = 0
         IRET = 0
         IF (I_RANK .EQ. I_MASTER) THEN
            ILGN = ILGN + 1
            IRET = C_FA_LISLN(XFIC, BUF)
            IF (IRET .NE. 0) GOTO 208
            IF (SP_STRN_LEN(BUF) .GT. 0) THEN
               CALL SP_STRN_TRM(BUF)
               CALL SP_STRN_SBC(BUF, CHAR_TAB, ' ')
               CALL SP_STRN_DMC(BUF, ' ')
            ENDIF
            IF (SP_STRN_LEN(BUF) .EQ. 0) GOTO 209
            GOTO 210
208         CONTINUE
            IERR = -1    ! ERR
            GOTO 210
209         CONTINUE
            IERR = -2    ! EOR
            GOTO 210
210         CONTINUE
         ENDIF

C---        BROADCAST LE CODE D'ERREUR
         CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         IF (IERR .EQ. -1) GOTO 9901
         IF (IERR .EQ. -2) GOTO 9902
D        CALL ERR_ASR(IERR .EQ. 0)

C---        BROADCAST LES DONNÉES
         ILBUF = SP_STRN_LEN(BUF)
         CALL MPI_BCAST(ILBUF, 1, MP_TYPE_INT(),
     &                  I_MASTER, I_COMM, I_ERROR)
         CALL MPI_BCAST(BUF(1:ILBUF), ILBUF, MPI_CHARACTER,
     &                  I_MASTER, I_COMM, I_ERROR)

C---        EXTRAIS LE NOM DE LA LIMITE
         ILNOM = 0
         IF (ERR_GOOD()) THEN
            IERR = SP_STRN_TKS(BUF(1:ILBUF), ' ', 1, NOMCND)
            IF (IERR .NE. 0) GOTO 9901
            CALL SP_STRN_LCS(NOMCND)
            ILNOM = SP_STRN_LEN(NOMCND)
            IRET = C_ST_CRC32(NOMCND(1:ILNOM), INOM)
         ENDIF

C---        EXTRAIS LES NOEUDS
         IF (ERR_GOOD()) THEN
            INDEB = ICLNOD + 1
            I = 1             ! SAUTE LE NOM
300         CONTINUE
               I = I + 1
               IERR = SP_STRN_TKI(BUF(1:ILBUF), ' ', I, IN)
               IF (IERR .EQ. -1) GOTO 399    ! EOR
               IF (IERR .EQ. -2) GOTO 9901   ! ERR
               IF (IN .LE. 0) GOTO 9903
C              IF (IN .GT. NNT) GOTO 9903       !! On ne connaît pas NNT
               IN = NM_NUMR_REQNINT(HNUMR, IN)  !! On prend tous les noeuds
               ICLNOD = ICLNOD + 1              !!    pour compat. avec les
D              CALL ERR_ASR(ICLNOD .LE. NCLNOD) !!    valeurs
               KCLNOD(ICLNOD) = IN
            GOTO 300
399         CONTINUE
            INFIN = ICLNOD
         ENDIF

C---        CONSERVE LES DONNEES
         IF (ERR_GOOD()) THEN
            IF (IL .GE. 1 .AND. KCLLIM(1,IL) .EQ. INOM) THEN
               KCLLIM( 4,IL) = INFIN
            ELSE
               DO I=1,IL
                  IF (KCLLIM(1,I) .EQ. INOM) GOTO 9904
               ENDDO
               IL = IL + 1
               KCLLIM(1,IL) = INOM
               KCLLIM(2,IL) = 0
               KCLLIM(3,IL) = INDEB
               KCLLIM(4,IL) = INFIN
               KCLLIM(5,IL) = 0
               KCLLIM(6,IL) = 0
               KCLLIM(7,IL) = EG_TPLMT_INDEFINI
               IF (I_RANK .EQ. I_MASTER) THEN
                  IERR = FD_LIMT_FCRC_AJTVAL(INOM, NOMCND(1:ILNOM))
               ENDIF
            ENDIF
         ENDIF

         IF (ERR_BAD()) GOTO 1099
      ENDDO
1099  CONTINUE

      GOTO 9989
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                       NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9989
9901  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (IRET .EQ. -1) CALL ERR_AJT('MSG_FIN_FICHIER')
         IF (IRET .EQ. -2) CALL ERR_AJT('MSG_LECTURE_FICHIER')
         IF (IRET .EQ. -3) CALL ERR_AJT('MSG_BUFFER_OVERFLOW')
      ENDIF
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_FIN_ENREGISTREMENT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NOEUD_INVALIDE',': ',IN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_REUTILISATION_NOM_LIMITE',': ',
     &                       NOMCND(1:SP_STRN_LEN(NOMCND))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      IF (SP_STRN_LEN(BUF) .GT. 0) THEN
         CALL SP_STRN_CLP(BUF, 80)
         CALL ERR_AJT(BUF(1:SP_STRN_LEN(BUF)))
      ENDIF
      IF (ILGN .GT. 0) THEN
         WRITE(ERR_BUF, '(2A,I6,A)') NOMFIC, '(', ILGN, ')'
         CALL SP_STRN_DLC(ERR_BUF, ' ')
         CALL ERR_AJT(ERR_BUF)
      ENDIF
      GOTO 9989

9989  CONTINUE
      IF (XFIC .NE. 0) IERR = C_FA_FERME(XFIC)
      GOTO 9999

9999  CONTINUE
      FD_LIMT_LISSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise les dimensions
C
C Description:
C     La fonction privée FD_LIMT_INIDIM initialise les dimensions à partir
C     du fichier dont le nom est passé en argument.
C
C Entrée:
C     HOBJ                    Handle sur l'objet
C     NOMFIC                  Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_INIDIM (HOBJ, NOMFIC)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) NOMFIC

      INCLUDE 'fdlimt.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER*8 XFIC       ! handle eXterne sur le fichier
      INTEGER   IC, IN
      INTEGER   INOM, ITMP
      INTEGER   IERR, IRET
      INTEGER   ILGN
      INTEGER   IOB
      INTEGER   I_RANK, I_ERROR, I_COMM
      INTEGER   NCLBLK
      INTEGER   NCLLIM
      INTEGER   NCLNOD
      INTEGER   NNO
      INTEGER   LBUF
      PARAMETER (LBUF = 1024)
      CHARACTER*(LBUF)  BUF
      CHARACTER*(256)   NOMCND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_LIMT_HBASE

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     INITIALISE
      ILGN = 0
      BUF  = ' '
      NCLBLK = 0
      NCLLIM = 0
      NCLNOD = 0

C---     OUVRE LE FICHIER
      IRET = 0
      XFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         XFIC = C_FA_OUVRE(NOMFIC(1:SP_STRN_LEN(NOMFIC)), C_FA_LECTURE)
         IF (XFIC .EQ. 0) IRET = -1
      ENDIF
C---     BROADCAST LE CODE D'ERREUR
      CALL MPI_BCAST(IRET, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IRET .EQ. -1) GOTO 9900
D     CALL ERR_ASR(IRET .EQ. 0)

C---     LIS L'ENTETE
      IRET = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         ILGN = ILGN + 1
         IRET = C_FA_LISLN(XFIC, BUF)
         IF (IRET .NE. 0) GOTO 107
         READ(BUF, *, ERR=108, END=109) NCLBLK
         GOTO 110
107      CONTINUE    ! EOF
         IERR = -1
         GOTO 110
108      CONTINUE    ! ERR
         IERR = -2
         GOTO 110
109      CONTINUE    ! EOR
         IERR = -3
         GOTO 110
110      CONTINUE
      ENDIF
C---     BROADCAST LE CODE D'ERREUR
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 299
      IF (IERR .EQ. -2) GOTO 9901
      IF (IERR .EQ. -3) GOTO 9902
D     CALL ERR_ASR(IERR .EQ. 0)

C---     BROADCAST LE NOMBRE DE BLOCS DE LIMITES À LIRE
      CALL MPI_BCAST(NCLBLK, 1, MP_TYPE_INT(),
     &               I_MASTER, I_COMM, I_ERROR)

C---     POUR CHAQUE LIMITE
      INOM = 0
      DO IC=1, NCLBLK
         IF (.NOT. ERR_GOOD()) GOTO 299

C---        LIS UNE LIGNE
         IF (I_RANK .EQ. I_MASTER) THEN
            ILGN = ILGN + 1
            IRET = C_FA_LISLN(XFIC, BUF)
            IF (IRET .EQ. -1) GOTO 207
            IF (IRET .NE.  0) GOTO 208
            IF (SP_STRN_LEN(BUF) .GT. 0) THEN
               CALL SP_STRN_TRM(BUF)
               CALL SP_STRN_SBC(BUF, CHAR_TAB, ' ')
               CALL SP_STRN_DMC(BUF, ' ')
            ENDIF
            IF (SP_STRN_LEN(BUF) .EQ. 0) GOTO 208

C---           EXTRAIS LE NOM DE LA LIMITE
            IF (ERR_GOOD()) THEN
               IRET = SP_STRN_TKS(BUF, ' ', 1, NOMCND)
               IF (IRET .NE. 0) GOTO 209
            ENDIF
C---           COMPTE LE NOMBRE DE LIMITES DIFFÉRENTES
            IF (ERR_GOOD()) THEN
               ITMP = 0
               IRET = C_ST_CRC32(NOMCND(1:SP_STRN_LEN(NOMCND)), ITMP)
               IF (ITMP .NE. INOM) THEN
                  NCLLIM = NCLLIM + 1
                  INOM = ITMP
               ENDIF
            ENDIF
C---           COMPTE LE NOMBRE DE NOEUDS
            IF (ERR_GOOD()) THEN
               NNO = SP_STRN_NTOK(BUF, ' ') - 1
               IF (NNO .LT. 1) GOTO 209
               NCLNOD = NCLNOD + NNO
            ENDIF
            GOTO 210

207         CONTINUE
            IERR = -1    ! EOF
            GOTO 210
208         CONTINUE
            IERR = -2    ! EOR
            GOTO 210
209         CONTINUE
            IERR = -3    ! ERR
            GOTO 210
210         CONTINUE
         ENDIF

C---        BROADCAST LE CODE D'ERREUR
         CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         IF (IERR .EQ. -1) GOTO 9902
         IF (IERR .EQ. -2) GOTO 9901
         IF (IERR .EQ. -3) GOTO 9904
D        CALL ERR_ASR(IERR .EQ. 0)

      ENDDO
299   CONTINUE

C---     BROADCAST LE NOMBRE DE LIMITES DIFFÉRENTES
      CALL MPI_BCAST(NCLLIM, 1, MP_TYPE_INT(),
     &               I_MASTER, I_COMM, I_ERROR)

C---     BROADCAST LES NOMBRE DE NOEUDS
      CALL MPI_BCAST(NCLNOD, 1, MP_TYPE_INT(),
     &               I_MASTER, I_COMM, I_ERROR)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         FD_LIMT_NCLBLK(IOB) = NCLBLK
         FD_LIMT_NCLLIM(IOB) = NCLLIM
         FD_LIMT_NCLNOD(IOB) = NCLNOD
      ENDIF

      GOTO 9989
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                       NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9989
9901  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (IRET .EQ. -1) CALL ERR_AJT('MSG_FIN_FICHIER')
         IF (IRET .EQ. -2) CALL ERR_AJT('MSG_LECTURE_FICHIER')
         IF (IRET .EQ. -3) CALL ERR_AJT('MSG_BUFFER_OVERFLOW')
      ENDIF
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_LIM_CL_INVALIDE',': ',NCLLIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_NOD_CL_INVALIDE',': ',NCLLIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      IF (SP_STRN_LEN(BUF) .GT. 0) THEN
         CALL SP_STRN_CLP(BUF, 80)
         CALL ERR_AJT(BUF(1:SP_STRN_LEN(BUF)))
      ENDIF
      IF (ILGN .GT. 0) THEN
         WRITE(ERR_BUF, '(2A,I6,A)') NOMFIC, '(', ILGN, ')'
         CALL SP_STRN_DLC(ERR_BUF, ' ')
         CALL ERR_AJT(ERR_BUF)
      ENDIF
      GOTO 9989

9989  CONTINUE
      IF (XFIC .NE. 0) IRET = C_FA_FERME(XFIC)
      GOTO 9999

9999  CONTINUE
      FD_LIMT_INIDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de limites
C
C Description:
C     La fonction FD_LIMT_REQNCLLIM retourne le nombre de limites
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_REQNCLLIM (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_REQNCLLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdlimt.fi'
      INCLUDE 'fdlimt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      FD_LIMT_REQNCLLIM = FD_LIMT_NCLLIM(HOBJ-FD_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de noeuds
C
C Description:
C     La fonction FD_LIMT_REQNCLNOD retourne le nombre de noeuds des limites
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_REQNCLNOD (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_REQNCLNOD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdlimt.fi'
      INCLUDE 'fdlimt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      FD_LIMT_REQNCLNOD = FD_LIMT_NCLNOD(HOBJ-FD_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nom du fichier.
C
C Description:
C     La fonction FD_LIMT_REQNOMF retourne le nom du fichier
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_REQNOMF (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_LIMT_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdlimt.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER LFNAME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LFNAME = SP_STRN_LEN(FD_LIMT_FNAME(HOBJ-FD_LIMT_HBASE))
      LFNAME = MIN(LFNAME, 256)
      IF (LFNAME .EQ. 0) THEN
         FD_LIMT_REQNOMF  = ' '
      ELSE
         FD_LIMT_REQNOMF  = FD_LIMT_FNAME(HOBJ-FD_LIMT_HBASE)(1:LFNAME)
      ENDIF
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute un item dans le map crc32/nom de cl.
C
C Description:
C     La fonction statique privée FD_LIMT_FCRC_AJTIT ajoute un item
C     dans le fichier de map entre les crc32 et les noms de limite.
C
C Entrée:
C     KEY      Clef, code crc32
C     VAL      Valeur associée
C
C Sortie:
C
C Notes:
C     Il n'y a aucun contrôle de dédoublement.
C************************************************************************
      FUNCTION FD_LIMT_FCRC_AJTVAL(KEY, VAL)

      IMPLICIT NONE

      INTEGER       KEY
      CHARACTER*(*) VAL

      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER M1
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(FD_LIMT_FCRC) .GT. 0)
D     CALL ERR_PRE(KEY .NE. 0)
D     CALL ERR_PRE(SP_STRN_LEN(VAL) .GT. 0)
C------------------------------------------------------------------------

      M1 = IO_UTIL_FREEUNIT()
      OPEN (UNIT  = M1,
     &      ERR   = 9900,
     &      FILE  = FD_LIMT_FCRC(1:SP_STRN_LEN(FD_LIMT_FCRC)),
     &      FORM  = 'FORMATTED',
     &      STATUS= 'UNKNOWN',
     &      ACCESS= 'APPEND')
      WRITE(M1, '(I12,1X,A)', ERR=9900) KEY, VAL(1:SP_STRN_LEN(VAL))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_AJOUT_CRC32')
      GOTO 9999

9999  CONTINUE
      CLOSE(M1)
      FD_LIMT_FCRC_AJTVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nom de cl associé à la clef.
C
C Description:
C     La fonction statique privée FD_LIMT_FCRC_REQVAL retourne la
C     valeur associée à la clef KEY.
C
C Entrée:
C     KEY      Clef, code crc32
C     VAL
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_LIMT_FCRC_REQVAL(KEY, VAL)

      IMPLICIT NONE

      INTEGER       KEY
      CHARACTER*(*) VAL

      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdlimt.fc'

      INTEGER K, FL
      INTEGER M1
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(FD_LIMT_FCRC) .GT. 0)
D     CALL ERR_PRE(KEY .NE. 0)
D     CALL ERR_PRE(SP_STRN_LEN(VAL) .GT. 0)
C------------------------------------------------------------------------

      FL = SP_STRN_LEN(FD_LIMT_FCRC)
      M1 = IO_UTIL_FREEUNIT()
      OPEN (UNIT  = M1,
     &      ERR   = 9900,
     &      FILE  = FD_LIMT_FCRC(1:FL),
     &      FORM  = 'FORMATTED',
     &      STATUS= 'OLD')

100   CONTINUE
         READ(M1, *, ERR=9901, END=9901) K, VAL
         IF (K .EQ. KEY) GOTO 109
      GOTO 100
      GOTO 9901
109   CONTINUE

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_OUVERTURE_FICHIER_CRC32'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_FTMP', ': ', FD_LIMT_FCRC(1:FL)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_REQ_CRC32'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I12)') 'MSG_CLEF', ': ', KEY
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_FTMP', ': ', FD_LIMT_FCRC(1:FL)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CLOSE(M1, ERR=9900)
      FD_LIMT_FCRC_REQVAL = ERR_TYP()
      RETURN
      END
