C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   IDENtité
C Type:    Concret
C
C Class: NM_IDEN
C     INTEGER NM_IDEN_000
C     INTEGER NM_IDEN_999
C     INTEGER NM_IDEN_CTR
C     INTEGER NM_IDEN_DTR
C     INTEGER NM_IDEN_INI
C     INTEGER NM_IDEN_RST
C     SUBROUTINE NM_IDEN_REQHBASE
C     LOGICAL NM_IDEN_HVALIDE
C     INTEGER NM_IDEN_CHARGE
C     INTEGER NM_IDEN_DSYNC
C     INTEGER NM_IDEN_ISYNC
C     INTEGER NM_IDEN_GENDSYNC
C     INTEGER NM_IDEN_GENISYNC
C     INTEGER NM_IDEN_REQNPRT
C     INTEGER NM_IDEN_REQNNL
C     INTEGER NM_IDEN_REQNNP
C     INTEGER NM_IDEN_REQNNT
C     LOGICAL NM_IDEN_ESTNOPP
C     INTEGER NM_IDEN_REQNEXT
C     INTEGER NM_IDEN_REQNINT
C
C************************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! Note: 2018-01-02
!!!
!!! Les SUBMODULE(s) ne sont supportés que pas Intel >= 16.4 et GCC >= 6.0
!!! Pour rester compatible avec les versions Intel antérieures (CMC) et
!!! pour pouvoir compiler Sun, le choix est fait de ne pas utiliser de
!!! SUBMODULE. Pour simuler/conserver la séparation déclaration/définition,
!!! le SUBMODULE est transformé en fichier inclus dans le section
!!! CONTAINS du module.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      !!!SUBMODULE(NM_IDEN_M) NM_IDEN_M_SRC1

      !!!CONTAINS

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction NM_IDEN_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_CTR_SELF()

      TYPE(NM_IDEN_T), POINTER :: NM_IDEN_CTR_SELF

      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise
      SELF%NNT = 0

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      NM_IDEN_CTR_SELF => SELF
      RETURN
      END FUNCTION NM_IDEN_CTR_SELF

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction NM_IDEN_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION NM_IDEN_DTR(SELF)

      CLASS(NM_IDEN_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = SELF%RST()
!!!      DEALLOCATE(SELF)

      NM_IDEN_DTR = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_IDEN_INI(SELF, NNT)

      INTEGER :: NM_IDEN_INI
      CLASS(NM_IDEN_T), INTENT(INOUT) :: SELF
      INTEGER, INTENT(IN) :: NNT

      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(NNT .GT. 0)
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%NNT = NNT
      ENDIF

      NM_IDEN_INI = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_IDEN_RST(SELF)

      INTEGER :: NM_IDEN_RST
      CLASS(NM_IDEN_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

C---     Reset les attributs
      SELF%NNT = 0

      NM_IDEN_RST = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_RST

C************************************************************************
C Sommaire: Charge les données
C
C Description:
C     La fonction NM_IDEN_CHARGE va charger les données pour le
C     temps demandé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     TEMPS       Temps pour lequel les données sont à charger
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_CHARGE(SELF)

      INTEGER :: NM_IDEN_CHARGE
      CLASS(NM_IDEN_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      NM_IDEN_CHARGE = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_CHARGE

C************************************************************************
C Sommaire: Synchronise les données REAL*8
C
C Description:
C     La fonction NM_IDEN_DSYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table VVAL
C     NNL         Dimension 2 de la table VVAL
C     VVAL        Table (NVAL, NNL) REAL*8 à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_DSYNC(SELF, NVAL, NNL, VVAL)

      INTEGER :: NM_IDEN_DSYNC
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VVAL(:,:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      NM_IDEN_DSYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_DSYNC

C************************************************************************
C Sommaire: Synchronise les données INTEGER
C
C Description:
C     La fonction NM_IDEN_ISYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table KVAL
C     NNL         Dimension 2 de la table KVAL
C     KVAL        Table INTEGER à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_ISYNC(SELF, NVAL, NNL, KVAL)

      INTEGER :: NM_IDEN_ISYNC
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: KVAL(:,:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      NM_IDEN_ISYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_ISYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_IDEN_GENDSYNC génère un objet de synchronisation pour
C     une table REAL*8 de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_GENDSYNC(SELF, NVAL, NNL, HSNC)

      INTEGER :: NM_IDEN_GENDSYNC
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = MP_SYNC_CTR   (HSNC)
      IF (ERR_GOOD()) IERR = MP_SYNC_INILCL(HSNC)

      NM_IDEN_GENDSYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_GENDSYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_IDEN_GENISYNC génère un objet de synchronisation pour
C     une table INTEGER de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_GENISYNC(SELF, NVAL, NNL, HSNC)

      INTEGER :: NM_IDEN_GENISYNC
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = MP_SYNC_CTR   (HSNC)
      IF (ERR_GOOD()) IERR = MP_SYNC_INILCL(HSNC)

      NM_IDEN_GENISYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_GENISYNC

C************************************************************************
C Sommaire: NM_IDEN_REQNPRT
C
C Description:
C     La fonction NM_IDEN_REQNPRT retourne le nombre de sous-domaines de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNPRT(SELF)

      INTEGER :: NM_IDEN_REQNPRT
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      NM_IDEN_REQNPRT = 1
      RETURN
      END FUNCTION NM_IDEN_REQNPRT

C************************************************************************
C Sommaire: NM_IDEN_REQNNL
C
C Description:
C     La fonction NM_IDEN_REQNNL retourne le nombre de noeuds local de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNNL(SELF)

      INTEGER :: NM_IDEN_REQNNL
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      NM_IDEN_REQNNL = SELF%NNT
      RETURN
      END FUNCTION NM_IDEN_REQNNL

C************************************************************************
C Sommaire: NM_IDEN_REQNNP
C
C Description:
C     La fonction NM_IDEN_REQNNP retourne le nombre de noeuds privés de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNNP(SELF)

      INTEGER :: NM_IDEN_REQNNP
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      NM_IDEN_REQNNP = SELF%NNT
      RETURN
      END FUNCTION NM_IDEN_REQNNP

C************************************************************************
C Sommaire: NM_IDEN_REQNNT
C
C Description:
C     La fonction NM_IDEN_REQNNT retourne le nombre de noeuds total de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNNT(SELF)

      INTEGER :: NM_IDEN_REQNNT
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      NM_IDEN_REQNNT = SELF%NNT
      RETURN
      END FUNCTION NM_IDEN_REQNNT

C************************************************************************
C Sommaire: NM_IDEN_ESTNOPP
C
C Description:
C     La fonction NM_IDEN_ESTNOPP retourne .TRUE. si le noeud passé en
C     argument est privé au process.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_ESTNOPP(SELF, INLOC)

      LOGICAL :: NM_IDEN_ESTNOPP
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'err.fi'
C------------------------------------------------------------------------
D     CALL ERR_PRE(INLOC .GT. 0)
C------------------------------------------------------------------------

      NM_IDEN_ESTNOPP = .TRUE.
      RETURN
      END FUNCTION NM_IDEN_ESTNOPP

C************************************************************************
C Sommaire: NM_IDEN_REQNEXT
C
C Description:
C     La fonction NM_IDEN_REQNEXT retourne le numéro de noeud global
C     correspondant au numéro local passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local à traduire en numéro global
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNEXT(SELF, INLOC)

      INTEGER :: NM_IDEN_REQNEXT
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'err.fi'
C------------------------------------------------------------------------
D     CALL ERR_PRE(INLOC .GT. 0)
C------------------------------------------------------------------------

      NM_IDEN_REQNEXT = INLOC
      RETURN
      END FUNCTION NM_IDEN_REQNEXT

C************************************************************************
C Sommaire: NM_IDEN_REQNINT
C
C Description:
C     La fonction NM_IDEN_REQNINT retourne le numéro de noeud local
C     correspondant au numéro global passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INGLB    Numéro de noeud global à traduire en numéro local
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNINT(SELF, INGLB)

      INTEGER :: NM_IDEN_REQNINT
      CLASS(NM_IDEN_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: INGLB

      INCLUDE 'err.fi'
C------------------------------------------------------------------------
D     CALL ERR_PRE(INGLB .GT. 0)
C------------------------------------------------------------------------

      NM_IDEN_REQNINT = INGLB
      RETURN
      END FUNCTION NM_IDEN_REQNINT

      !!!END SUBMODULE NM_IDEN_M_SRC1
