C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: FD_NUMR
C     INTEGER FD_NUMR_000
C     INTEGER FD_NUMR_999
C     INTEGER FD_NUMR_CTR
C     INTEGER FD_NUMR_DTR
C     INTEGER FD_NUMR_INI
C     INTEGER FD_NUMR_RST
C     LOGICAL FD_NUMR_HVALIDE
C     INTEGER FD_NUMR_ECRIS
C     INTEGER FD_NUMR_LIS
C     INTEGER FD_NUMR_REQNNTG
C     INTEGER FD_NUMR_REQNNTL
C     INTEGER FD_NUMR_REQNPROC
C
C     Structure du fichier:
C        NNT NPROC
C        NNL_PR1 NNL_PR2 ...  NNL_PRn           ! Nbr Noeuds locaux
C        NNO_PR1 NNO_PR2 ...  NNO_PRn IPROC     ! .LE. 0 si absent
C        NNO_PR1 NNO_PR2 ...  NNO_PRn IPROC     ! .LE. 0 si absent
C
C     Structure des tables:
C        Lecture: Structure par ligne avec tous les noeuds du process
C           NNO_PR1 NNO_PR2 ...  NNO_PRn NNO_GLB IPROC ! .LE. 0 si absent
C        Écriture: Structure par colonne avec les colonnes complètes des
C           sous-domaines associés au process.
C           NNO_PR1 NNO_PR2 ...                        ! .LE. 0 si absent
C
C Note:
C     Le temps n'est pas supporté. Les connectivités ne peuvent pas
C     changer dans le temps!!
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction FD_NUMR_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdnumr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(FD_NUMR_NOBJMAX,
     &                   FD_NUMR_HBASE,
     &                   'FD_NUMR')

      FD_NUMR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction FD_NUMR_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdnumr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER  IERR
      EXTERNAL FD_NUMR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(FD_NUMR_NOBJMAX,
     &                   FD_NUMR_HBASE,
     &                   FD_NUMR_DTR)

      FD_NUMR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction FD_NUMR_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdnumr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   FD_NUMR_NOBJMAX,
     &                   FD_NUMR_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(FD_NUMR_HVALIDE(HOBJ))
         IOB = HOBJ - FD_NUMR_HBASE

         FD_NUMR_FNAME(IOB) = ' '
         FD_NUMR_ISTAT(IOB) = IO_MODE_INDEFINI
         FD_NUMR_NNTG (IOB) = 0
         FD_NUMR_NNTL (IOB) = 0
         FD_NUMR_NPROC(IOB) = 0
      ENDIF

      FD_NUMR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction FD_NUMR_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdnumr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_NUMR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = FD_NUMR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   FD_NUMR_NOBJMAX,
     &                   FD_NUMR_HBASE)

      FD_NUMR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction FD_NUMR_INI initialise l'objet à l'aide des
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier de coordonnées
C     ISTAT       IO_LECTURE, IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_INI(HOBJ, NOMFIC, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT

      INCLUDE 'fdnumr.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_NUMR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     DEMARRE LE CHRONO
      CALL TR_CHRN_START('h2d2.io.numr')

C---     CONTROLE LES PARAMETRES
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9900
      IF (.NOT. IO_UTIL_MODVALIDE(ISTAT)) GOTO 9901
      IF (.NOT. IO_UTIL_MODACTIF(ISTAT, IO_MODE_ASCII)) GOTO 9901

C---     RESET LES DONNEES
      IERR = FD_NUMR_RST(HOBJ)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - FD_NUMR_HBASE
         FD_NUMR_FNAME(IOB) = NOMFIC(1:SP_STRN_LEN(NOMFIC))
         FD_NUMR_ISTAT(IOB) = ISTAT
         FD_NUMR_NNTG (IOB) = 0
         FD_NUMR_NNTL (IOB) = 0
         FD_NUMR_NPROC(IOB) = 0
      ENDIF

C---     INITIALISE LA STRUCTURE INTERNE EN LECTURE
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) THEN
         IF (ERR_GOOD()) IERR = FD_NUMR_INISTR(HOBJ, NOMFIC)
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A, I3)') 'ERR_PARAMETRE_INVALIDE',': ', ISTAT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.numr')
      FD_NUMR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction FD_NUMR_RST reset l'objet, donc le remet dans
C     un état valide non initialisé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_NUMR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_NUMR_HBASE

C---     RESET LES ATTRIBUTS
      FD_NUMR_FNAME(IOB) = ' '
      FD_NUMR_ISTAT(IOB) = IO_MODE_INDEFINI
      FD_NUMR_NNTG (IOB) = 0
      FD_NUMR_NNTL (IOB) = 0
      FD_NUMR_NPROC(IOB) = 0

      FD_NUMR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction FD_NUMR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdnumr.fi'
      INCLUDE 'fdnumr.fc'
C------------------------------------------------------------------------

      FD_NUMR_REQHBASE = FD_NUMR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction FD_NUMR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdnumr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdnumr.fc'
C------------------------------------------------------------------------

      FD_NUMR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  FD_NUMR_NOBJMAX,
     &                                  FD_NUMR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: FD_NUMR_ECRIS
C
C Description:
C     La fonction FD_NUMR_ECRIS est la fonction principale d'écriture
C     d'un fichier de type NUMeRotation.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NNT         Nombre de noeuds total
C     NPRTG       Nombre de sous-domaines total
C     NPRTL       Nombre de sous-domaines associés au process local
C     IPRTL       Indice du premier sous-domaine local
C     KDSTR       Table de distribution organisée en colonnes
C
C Sortie:
C
C Notes:
C     Que se passe-t-il si send fait une erreur? recv peut-il rester bloqué?
C************************************************************************
      FUNCTION FD_NUMR_ECRIS(HOBJ,
     &                       NNT,
     &                       NPRTG,
     &                       NPRTL,
     &                       IPRTL,
     &                       KDSTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_ECRIS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NNT
      INTEGER NPRTG
      INTEGER NPRTL
      INTEGER IPRTL
      INTEGER KDSTR(NPRTL+1, NNT)

      INCLUDE 'fdnumr.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER I_MASTER
      INTEGER I_STATUS(MPI_STATUS_SIZE)
      PARAMETER (I_MASTER = 0)

      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER ISTAT
      INTEGER IDUM
      INTEGER I_RANK, I_SIZE, I_ERROR, I_COMM
      INTEGER M1
      INTEGER L
      INTEGER IN, ID, I
      INTEGER, ALLOCATABLE :: KTRV (:)
      INTEGER, ALLOCATABLE :: KRECV(:), KDSPL(:)
      CHARACTER*(16) FRMT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_NUMR_HVALIDE(HOBJ))
D     CALL ERR_PRE(NNT   .GT. 1)
D     CALL ERR_PRE(NPRTG .GT. 0)
D     CALL ERR_PRE(NPRTL .GE. 0)
D     CALL ERR_PRE(IPRTL .GT. 0)
D     CALL ERR_PRE(NPRTL .LE. NPRTG)
D     CALL ERR_PRE(IPRTL-1 .LE. NPRTG)
D     CALL ERR_PRE(IPRTL+NPRTL-1 .LE. NPRTG)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     DEMARRE LE CHRONO
      CALL TR_CHRN_START('h2d2.io.numr.write')

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_NUMR_HBASE

C---     PARAMETRES
      ISTAT = FD_NUMR_ISTAT(IOB)
      L = SP_STRN_LEN(FD_NUMR_FNAME(IOB))
D     CALL ERR_ASR(L .GT. 0)

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)

C---     Alloue l'espace de travail
      IRET = 0
      IF (IRET .EQ. 0) ALLOCATE(KTRV (NPRTG+1), STAT=IRET)
      IF (IRET .EQ. 0) ALLOCATE(KRECV(NPRTG+0), STAT=IRET)
      IF (IRET .EQ. 0) ALLOCATE(KDSPL(NPRTG+0), STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Monte le format
      WRITE(FRMT, '(A,I0,A)') '(', (NPRTG+1), 'I9)'

C---     COMPTE LES NNL
      IERR = FD_NUMR_REQNNL(NNT,
     &                      NPRTG,
     &                      NPRTL,
     &                      IPRTL,
     &                      KDSTR,
     &                      KRECV,     ! les nnl
     &                      KTRV)      ! table de travail

C---     OUVRE LE FICHIER
      IERR = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         M1 = IO_UTIL_FREEUNIT()
         IF (IO_UTIL_MODACTIF(ISTAT,IO_MODE_ECRITURE)) THEN
            OPEN (UNIT  = M1,
     &            ERR   = 109,
     &            FILE  = FD_NUMR_FNAME(IOB)(1:L),
     &            FORM  = 'FORMATTED',
     &            STATUS= 'REPLACE')
            FD_NUMR_ISTAT(IOB) = FD_NUMR_ISTAT(IOB)      ! Pour la suite,
     &               - IO_MODE_ECRITURE + IO_MODE_AJOUT  ! IO_MODE_AJOUT
         ELSEIF (IO_UTIL_MODACTIF(ISTAT,IO_MODE_AJOUT)) THEN
            OPEN (UNIT  = M1,
     &            ERR   = 109,
     &            FILE  = FD_NUMR_FNAME(IOB)(1:L),
     &            FORM  = 'FORMATTED',
     &            STATUS= 'UNKNOWN',
     &            ACCESS= 'APPEND')    ! Non standard
C     &            POSITION='APPEND')  ! Pas implanté par g77
         ELSE
            IERR = -2
         ENDIF
         GOTO 110
109      CONTINUE
         IERR = -1
110      CONTINUE
      ENDIF
C---     BROADCAST LE CODE D'ERREUR
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9901
D     CALL ERR_ASR(IERR .EQ. 0)

C---     SI ON A UN SEUL PROCESS
      IF (I_SIZE .GT. 1) GOTO 1000
      WRITE (M1,FRMT,ERR=9902) NNT, NPRTG
      WRITE (M1,FRMT,ERR=9902) (KRECV(ID),ID=1,NPRTG)
      DO IN=1,NNT
         WRITE (M1,FRMT,ERR=9902) (KDSTR(ID,IN),ID=1,NPRTG+1)
      ENDDO
      GOTO 1999

C---     ECRIS LA SEQUENCE
1000  CONTINUE

C---     BROADCAST NNT COMME CONTROLE QUE TOUS ONT LE MÊME
D     IF (ERR_GOOD()) THEN
D        IF (I_RANK .EQ. I_MASTER) IDUM = NNT
D        CALL MPI_BCAST(IDUM, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
D        CALL ERR_ASR(IDUM .EQ. NNT)
D     ENDIF

C---     ECRIS L'ENTETE
      IF (ERR_GOOD() .AND. I_RANK .EQ. I_MASTER) THEN
         WRITE (M1,FRMT,ERR=301) NNT, NPRTG
         WRITE (M1,FRMT,ERR=301) (KRECV(ID),ID=1,NPRTG)
         IERR = 0
         GOTO 309
301      CONTINUE ! ERR
         IERR = -1
         GOTO 309
309      CONTINUE
      ENDIF

C---     BROADCAST THE ERROR CODE
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         IF (IERR .EQ. -1) GOTO 9902
      ENDIF
D     CALL ERR_ASR(IERR .EQ. 0)

C---     STRUCTURES POUR LE GATHERV
      IF (ERR_GOOD()) CALL MPI_GATHER(NPRTL, 1, MP_TYPE_INT(),
     &                                KRECV, 1, MP_TYPE_INT(),
     &                                I_MASTER, I_COMM, I_ERROR)
      IF (ERR_GOOD()) CALL MPI_GATHER(IPRTL-1, 1, MP_TYPE_INT(),
     &                                KDSPL, 1, MP_TYPE_INT(),
     &                                I_MASTER, I_COMM, I_ERROR)

C---     BOUCLE SUR LES NOEUDS GLOBAUX
      DO IN=1,NNT
         IF (ERR_BAD()) GOTO 399

C---        GATHER UNE LIGNE
         CALL MPI_GATHERV(KDSTR(1,IN), NPRTL, MP_TYPE_INT(),
     &                    KTRV, KRECV, KDSPL, MP_TYPE_INT(),
     &                    I_MASTER, I_COMM, I_ERROR)

C---           ECRIS LA LIGNE
         IF (ERR_GOOD() .AND. I_RANK .EQ. I_MASTER) THEN
            KTRV(NPRTG+1) = KDSTR(NPRTL+1,IN)
            WRITE (M1,FRMT,ERR=321) (KTRV(ID),ID=1,NPRTG+1)
            IERR = 0
            GOTO 329
321         CONTINUE ! ERR
            IERR = -1
            GOTO 329
329         CONTINUE
         ENDIF

C---        BROADCAST LE CODE D'ERREUR D'ECRITURE
         IF (ERR_GOOD()) THEN ! Si gatherv en err, on ne rentre pas ici!!
            CALL MPI_BCAST(IERR, 1, MP_TYPE_INT(),
     &                     I_MASTER, I_COMM, I_ERROR)
            IF (IERR .EQ. -1) GOTO 9902
D           CALL ERR_ASR(IERR .EQ. 0)
         ENDIF

      ENDDO
399   CONTINUE

C---     FERME LE FICHIER
1999  CONTINUE
      IF (I_RANK .EQ. I_MASTER) CLOSE(M1)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, *) 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                       FD_NUMR_FNAME(IOB)(1:L)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(3A)') 'ERR_ECRITURE_FICHIER',': ',
     &                       FD_NUMR_FNAME(IOB)(1:L)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (ALLOCATED(KTRV))  DEALLOCATE(KTRV)
      IF (ALLOCATED(KRECV)) DEALLOCATE(KRECV)
      IF (ALLOCATED(KDSPL)) DEALLOCATE(KDSPL)
      CALL TR_CHRN_STOP('h2d2.io.numr.write')
      FD_NUMR_ECRIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_NUMR_LIS
C
C Description:
C     La fonction FD_NUMR_LIS est la fonction principale de lecture
C     d'un fichier de type NUMeRotation.
C
C Entrée:
C     HOBJ                 Handle sur l'objet
C     NPROC                Nombre de process
C     NCOL                 Nombre de colonnes
C     NNTL                 Nombre de noeuds local
C
C Sortie:
C     KDISTR(NCOL, NNTL)  Table de renumérotation
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_LIS(HOBJ,
     &                     NPROC,
     &                     NCOL,
     &                     NNTL,
     &                     KDISTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_LIS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPROC
      INTEGER NCOL
      INTEGER NNTL
      INTEGER KDISTR(NCOL,NNTL)

      INCLUDE 'fdnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L
      INTEGER L_TRV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_NUMR_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPROC .EQ. FD_NUMR_REQNPROC(HOBJ))
D     CALL ERR_PRE(NCOL  .GT. NPROC)
D     CALL ERR_PRE(NNTL  .EQ. FD_NUMR_REQNNTL (HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     DEMARRE LE CHRONO
      CALL TR_CHRN_START('h2d2.io.numr.read')

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_NUMR_HBASE

C---     ALLOUE L'ESPACE DE TRAVAIL
      L_TRV = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NCOL, L_TRV)

C---     LIS LES VALEURS
      L = SP_STRN_LEN(FD_NUMR_FNAME(IOB))
      IERR = FD_NUMR_LISVAL(NPROC,
     &                      NCOL,
     &                      NNTL,
     &                      KDISTR,
     &                      KA(SO_ALLC_REQKIND(KA, L_TRV)),
     &                      FD_NUMR_FNAME(IOB)(1:L))

C---     RECUPERE L'ESPACE DE TRAVAIL
      IF (L_TRV .NE. 0) IERR = SO_ALLC_ALLINT(0, L_TRV)

      CALL TR_CHRN_STOP('h2d2.io.numr.read')
      FD_NUMR_LIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction privée FD_NUMR_INISTR initialise la structure interne à
C     partir du fichier dont le nom est passé en argument. En plus des valeurs
C     globales, on lis le nombre de valeurs de chaque process pour permettre
C     le dimensionnement des tables.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier de coordonnées
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_INISTR(HOBJ, NOMFIC)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC

      INCLUDE 'fdnumr.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER I_MASTER
      INTEGER I_STATUS(MPI_STATUS_SIZE)
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER IP
      INTEGER I_RANK, I_PROC, I_SIZE, I_TAG, I_ERROR, I_COMM
      INTEGER NPROC, NPROC_MAX
      INTEGER NNTG, NNTL
      PARAMETER (NPROC_MAX = 256)
      INTEGER KNNL (NPROC_MAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_NUMR_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(NOMFIC) .GT. 0)
C------------------------------------------------------------------------

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      I_PROC = I_RANK + 1

C---     LIS LES DIMENSIONS DU FICHIER
      IF (ERR_GOOD()) THEN
         IF (I_RANK .EQ. I_MASTER) THEN
            IERR = FD_NUMR_LISDIM(NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &                            NNTG,
     &                            NPROC,
     &                            KNNL)
         ENDIF
         IERR = MP_UTIL_SYNCERR()
      ENDIF

C---     BROADCAST LE NOMBRE DE PROC
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(NPROC, 1, MP_TYPE_INT(),
     &                  I_MASTER,
     &                  I_COMM, I_ERROR)
      ENDIF

C---     BROADCAST LE NOMBRE DE NOEUDS GLOBAL
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(NNTG, 1, MP_TYPE_INT(),
     &                  I_MASTER,
     &                  I_COMM, I_ERROR)
      ENDIF

C---     CONTROLES
      IF (ERR_GOOD() .AND. NPROC .NE. I_SIZE)    GOTO 9900
      IF (ERR_GOOD() .AND. NPROC .GT. NPROC_MAX) GOTO 9901

C---     BROADCAST LES NOMBRES DE NOEUDS LOCAUX
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(KNNL, NPROC, MP_TYPE_INT(),
     &                  I_MASTER,
     &                  I_COMM, I_ERROR)
      ENDIF
      IF (ERR_GOOD()) NNTL = KNNL(I_PROC)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - FD_NUMR_HBASE
         FD_NUMR_NNTG (IOB) = NNTG
         FD_NUMR_NNTL (IOB) = NNTL
         FD_NUMR_NPROC(IOB) = I_SIZE
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I3,A,I3)') 'ERR_NBR_PROCESS_INVALIDE',': ',
     &                       NPROC, '/', I_SIZE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_NUMR_INISTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_NUMR_LISVAL
C
C Description:
C     La fonction FD_NUMR_LISVAL est la fonction principale de lecture
C     d'un fichier de type NUMeRotation.
C
C Entrée:
C     NPROC          Nombre de process
C     NCOL           Nombre de colonnes de la table
C     NNTL           Nombre de valeurs à lire pour le process
C     KDISTR         Table de distribution : KDISTR(NCOL, NNTL)
C     NOMFIC         Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_LISVAL(NPROC,
     &                        NCOL,
     &                        NNTL,
     &                        KDISTR,
     &                        KTRV,
     &                        NOMFIC)

      IMPLICIT NONE

      INTEGER        NPROC
      INTEGER        NCOL
      INTEGER        NNTL
      INTEGER        KDISTR(NCOL, NNTL)
      INTEGER        KTRV  (NCOL)
      CHARACTER*(*)  NOMFIC

      INCLUDE 'fdnumr.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER*8 XFIC       ! handle eXterne sur le fichier
      INTEGER   IERR
      INTEGER   IG, IN, IP, INGLB, IPPNO
      INTEGER   I_RANK, I_ERROR, I_PROC, I_COMM
      INTEGER   NNTG
D     INTEGER   NPROC_TMP
      CHARACTER*(1024) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNTL  .GT. 1)
D     CALL ERR_PRE(NPROC .GT. 0)
D     CALL ERR_PRE(NCOL  .GT. NPROC)
D     CALL ERR_PRE(SP_STRN_LEN(NOMFIC) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      INGLB = NPROC + 1       ! Indice de noeud global
      IPPNO = NPROC + 2       ! Process propriétaire du noeud

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      I_PROC = I_RANK + 1

C---     OUVRE LE FICHIER
      XFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         XFIC = C_FA_OUVRE(NOMFIC(1:SP_STRN_LEN(NOMFIC)), C_FA_LECTURE)
         IF (XFIC .EQ. 0) GOTO 9900
      ENDIF

C---     LIS ET BROADCAST L'ENTETE
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = C_FA_LISLN(XFIC, BUF)
         IF (IERR .NE. 0) GOTO 107
         READ(BUF, *, ERR=108, END=109) NNTG
D    &                                  , NPROC_TMP
D        CALL ERR_ASR(NNTG  .GE. NNTL)
D        CALL ERR_ASR(NPROC_TMP .EQ. NPROC)
         IERR = C_FA_LISLN(XFIC, BUF) ! Saute la deuxième ligne d'en-tête
         IF (IERR .NE. 0) GOTO 107
         GOTO 110
107      CONTINUE ! EOF
         IERR = -1
         GOTO 110
108      CONTINUE ! ERR
         IERR = -2
         GOTO 110
109      CONTINUE ! END
         IERR = -3
         GOTO 110
110      CONTINUE
      ENDIF

C---        BROADCAST THE ERROR CODE
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9901
      IF (IERR .EQ. -2) GOTO 9901
      IF (IERR .EQ. -3) GOTO 9902
D     CALL ERR_ASR(IERR .EQ. 0)

      CALL MPI_BCAST(NNTG, 1, MP_TYPE_INT(),
     &               I_MASTER,
     &               I_COMM, I_ERROR)

C---     LIS ET BROADCAST LES DONNÉES
      IN = 0
      DO IG=1,NNTG
         IF (ERR_BAD()) GOTO 299

         IF (I_RANK .EQ. I_MASTER) THEN
            IERR = C_FA_LISLN(XFIC, BUF)
            IF (IERR .NE. 0) GOTO 207
            READ(BUF,*, ERR=208, END=209) (KTRV(IP),IP=1,NPROC),
     &                                     KTRV(IPPNO)
            KTRV(INGLB) = IG
            GOTO 210
207         CONTINUE ! EOF
            IERR = -1
            GOTO 210
208         CONTINUE ! ERR
            IERR = -2
            GOTO 210
209         CONTINUE ! END
            IERR = -3
            GOTO 210
210         CONTINUE
         ENDIF

C---        BROADCAST THE ERROR CODE
         CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         IF (IERR .EQ. -1) GOTO 9901
         IF (IERR .EQ. -2) GOTO 9901
         IF (IERR .EQ. -3) GOTO 9902
D        CALL ERR_ASR(IERR .EQ. 0)

         CALL MPI_BCAST(KTRV, NPROC+2, MP_TYPE_INT(),
     &                  I_MASTER,
     &                  I_COMM, I_ERROR)

         IF (ERR_GOOD()) THEN
            IF (KTRV(I_PROC) .GT. 0) THEN
               IN = IN + 1
               CALL ICOPY(NPROC+2, KTRV, 1, KDISTR(1,IN), 1)
            ENDIF
         ENDIF

      ENDDO

C---     FERME LE FICHIER
299   CONTINUE
      IF (XFIC .NE. 0) THEN
         IERR = C_FA_FERME(XFIC)
      ENDIF
      IF (IN .NE. NNTL) GOTO 9903

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                       NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_LECTURE_FICHIER',': ',
     &                       NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_NBR_VALEURS_LUES',': ',
     &      IN, ' / ', NNTL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IERR = MP_UTIL_SYNCERR()
      FD_NUMR_LISVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis les dimensions
C
C Description:
C     La fonction privée FD_NUMR_LISDIM lis les dimensions à partir du fichier.
C
C Entrée:
C     NOMFIC      Nom du fichier
C
C Sortie:
C     NNTG        Nombre de noeuds total global
C     NPROC       Nombre de process
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_LISDIM(NOMFIC, NNTG, NPROC, KNNL)

      IMPLICIT NONE

      CHARACTER*(*) NOMFIC
      INTEGER       NNTG
      INTEGER       NPROC
      INTEGER       KNNL(*)

      INCLUDE 'fdnumr.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER*8 XFIC       ! handle eXterne sur le fichier
      INTEGER   IERR
      INTEGER   IP
      CHARACTER*(1024) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(NOMFIC) .GT. 0)
C------------------------------------------------------------------------

C---     OUVRE LE FICHIER
      XFIC = C_FA_OUVRE(NOMFIC(1:SP_STRN_LEN(NOMFIC)), C_FA_LECTURE)
      IF (XFIC .EQ. 0) GOTO 9900

C---     LIS LES VALEURS
      IERR = C_FA_LISLN(XFIC, BUF)
      IF (IERR .NE. 0) GOTO 9901
      READ(BUF, *, ERR=9901, END=9901) NNTG, NPROC
      IERR = C_FA_LISLN(XFIC, BUF)
      IF (IERR .NE. 0) GOTO 9901
      READ(BUF, *, ERR=9901, END=9901) (KNNL(IP),IP=1,NPROC)

C---        FERME LE FICHIER
      IF (XFIC .NE. 0) IERR = C_FA_FERME(XFIC)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                       NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_LECTURE_FICHIER',': ',
     &                       NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)

9999  CONTINUE
      FD_NUMR_LISDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre de Noeuds Total global
C
C Description:
C     La fonction FD_NUMR_REQNNT retourne le nombre de noeuds total
C     de l'objet pour tous les process.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_REQNNTG(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_REQNNTG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdnumr.fi'
      INCLUDE 'fdnumr.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_NUMR_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_NUMR_NNTG(HOBJ-FD_NUMR_HBASE) .NE. 0)
C------------------------------------------------------------------------

      FD_NUMR_REQNNTG = FD_NUMR_NNTG(HOBJ-FD_NUMR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre de Noeuds Total local au process
C
C Description:
C     La fonction FD_NUMR_REQNNT retourne le nombre de noeuds total
C     de l'objet pour le process courant.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_REQNNTL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_REQNNTL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdnumr.fi'
      INCLUDE 'fdnumr.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_NUMR_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_NUMR_NNTL(HOBJ-FD_NUMR_HBASE) .NE. 0)
C------------------------------------------------------------------------

      FD_NUMR_REQNNTL = FD_NUMR_NNTL(HOBJ-FD_NUMR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de dimension
C
C Description:
C     La fonction FD_NUMR_REQNPROC retourne le nombre de dimensions
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_NUMR_REQNPROC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_NUMR_REQNPROC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdnumr.fi'
      INCLUDE 'fdnumr.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_NUMR_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_NUMR_NPROC(HOBJ-FD_NUMR_HBASE) .NE. 0)
C------------------------------------------------------------------------

      FD_NUMR_REQNPROC = FD_NUMR_NPROC(HOBJ-FD_NUMR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: NM_NBSE_PA_NNL
C
C Description:
C     La fonction NM_NBSE_PA_NNL retourne le nombre de noeuds locaux pour
C     le process courant. De la table organisée en colonnes, on compte les
C     noeuds locaux des sous-domaines, synchronise les valeurs et retourne
C     le nombre de noeuds du process courant.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     La version de MPI_ALLREDUCE avec MPI_IN_PLACE ne semble pas
C     fonctionner comme je le comprends (YSe). Remplacée par l'utilisation
C     d'un buffer temporaire.
C************************************************************************
      FUNCTION FD_NUMR_REQNNL(NNT,
     &                        NPRTG,
     &                        NPRTL,
     &                        IPRTL,
     &                        KDSTR,
     &                        KNNL,
     &                        KTRV)

      IMPLICIT NONE

      INTEGER NNT
      INTEGER NPRTG
      INTEGER NPRTL
      INTEGER IPRTL
      INTEGER KDSTR(NPRTL+1, NNT)
      INTEGER KNNL (NPRTG)
      INTEGER KTRV (NPRTG)

      INCLUDE 'fdnumr.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'fdnumr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IDUM
      INTEGER I_SIZE, I_ERROR, I_COMM
      INTEGER IN, IP
      INTEGER NNL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNT   .GT. 1)
D     CALL ERR_PRE(NPRTG .GT. 0)
D     CALL ERR_PRE(NPRTL .GE. 0)
D     CALL ERR_PRE(IPRTL .GT. 0)
D     CALL ERR_PRE(NPRTL .LE. NPRTG)
D     CALL ERR_PRE(IPRTL-1 .LE. NPRTG)
D     CALL ERR_PRE(IPRTL+NPRTL-1 .LE. NPRTG)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.io.numr'

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)

C---     COMPTE LES NNL POUR CHAQUE SOUS-DOMAINE
      WRITE(LOG_BUF, '(A,8I6)') 'NNL param: ', IPRTL, NPRTL, NPRTG
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      IF (ERR_GOOD()) THEN
         CALL IINIT (NPRTG, 0, KTRV, 1)
         DO IP=1,NPRTL
            NNL = 0
            DO IN=1,NNT
               IF (KDSTR(IP, IN) .GT. 0) NNL = NNL + 1
            ENDDO
            KTRV(IPRTL+IP-1) = NNL
         ENDDO
         WRITE(LOG_BUF, '(A,8I6)') 'NNL locaux (max 8): ',
     &                             (KTRV(IP),IP=1,MIN(8,NPRTG))
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

C---     REDUIS LES NNL
      CALL IINIT (NPRTG, 0, KNNL, 1)
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(KTRV, KNNL, NPRTG, MP_TYPE_INT(),
     &                      MPI_MAX, I_COMM, I_ERROR)
         WRITE(LOG_BUF, '(A,8I6)') 'NNL globaux(max 8): ',
     &                             (KNNL(IP),IP=1,MIN(8,NPRTG))
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

      FD_NUMR_REQNNL = ERR_TYP()
      RETURN
      END

