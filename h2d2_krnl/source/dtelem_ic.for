C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C      FUNCTION IC_DT_ELEM_XEQCTR ()
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_ELEM_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_ELEM_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtelem_ic.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtelem_ic.fc'

      INTEGER         IERR
      INTEGER         HOBJ
      CHARACTER*(256) NOMFIC
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_ELEM_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the connectivities file</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
      CALL SP_STRN_TRM(NOMFIC)

C---     VALIDE
      IF (ERR_GOOD() .AND. SP_STRN_LEN(NOMFIC) .LE. 0) THEN
         WRITE(ERR_BUF, '(A)') 'ERR_ARGUMENT_INVALIDE'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ENDIF

C---     CONSTRUIT UN OBJET ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_ELEM_CTR   (HOBJ)
      IF (ERR_GOOD())
     &   IERR = DT_ELEM_INIFIC(HOBJ,
     &                         NOMFIC,
     &                         IO_MODE_LECTURE + IO_MODE_ASCII)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_DT_ELEM_PRN(HOBJ)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the connectivities</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>elem</b> constructs an object, with the given arguments,
C  and return a handle on this object.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_ELEM_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_DT_ELEM_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_DT_ELEM_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_ELEM_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_ELEMENTS'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Corps
      CALL LOG_INCIND()
      IERR = DT_ELEM_PRN(HOBJ)
      CALL LOG_DECIND()

      IC_DT_ELEM_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_ELEM_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_ELEM_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dtelem_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'spstrn.fi'

      INCLUDE 'dtelem_ic.fc'

      INTEGER IERR
!      INTEGER        HVAL
      INTEGER        IVAL
      REAL*8         RVAL
      CHARACTER*(64) PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Number of nodes per element</comment>
         IF (PROP .EQ. 'nodes_per_element') THEN
            IVAL = DT_ELEM_REQNNEL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Total number of elements</comment>
         ELSEIF (PROP .EQ. 'elements_total') THEN
            IVAL = DT_ELEM_REQNELT(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of elements local to the process</comment>
         ELSEIF (PROP .EQ. 'elements_local') THEN
            IVAL = DT_ELEM_REQNELL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_ELEM_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_ELEM_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DT_ELEM_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_ELEM_AID()

9999  CONTINUE
      IC_DT_ELEM_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_ELEM_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_ELEM_REQCLS
CDEC$ ENDIF

      INCLUDE 'dtelem_ic.fi'
C------------------------------------------------------------------------

C<comment>
C  The class <b>elem</b> represents the connectivities of a finite element mesh.
C  The connectivities refer to the lists of all the node numbers belonging
C  to an element.
C</comment>
      IC_DT_ELEM_REQCLS = 'elem'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_ELEM_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_ELEM_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtelem_ic.fi'
      INCLUDE 'dtelem.fi'
C-------------------------------------------------------------------------

      IC_DT_ELEM_REQHDL = DT_ELEM_REQHBASE()
      RETURN
      END


C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_LMTR_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_ELEM_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtelem_ic.hlp')

      RETURN
      END
