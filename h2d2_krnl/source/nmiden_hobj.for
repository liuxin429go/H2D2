C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Numérotation
C Objet:   IDENtité
C Type:
C
C Class: NM_LCGL
C     INTEGER NM_IDEN_000
C     INTEGER NM_IDEN_999
C     INTEGER NM_IDEN_CTR
C     INTEGER NM_IDEN_DTR
C     INTEGER NM_IDEN_INI
C     INTEGER NM_IDEN_RST
C     INTEGER NM_IDEN_REQHBASE
C     INTEGER NM_IDEN_REQHBASE
C     LOGICAL NM_IDEN_HVALIDE
C     INTEGER NM_IDEN_CHARGE
C     INTEGER NM_IDEN_LCLGLB
C     INTEGER NM_IDEN_PRCECR
C     INTEGER NM_IDEN_DIMTBL
C     INTEGER NM_IDEN_TBLECR
C     INTEGER NM_IDEN_PRNDST
C     LOGICAL NM_IDEN_ESTNOPP
C     INTEGER NM_IDEN_REQNPRT
C     INTEGER NM_IDEN_REQNNL
C     INTEGER NM_IDEN_REQNNP
C     INTEGER NM_IDEN_REQNNT
C     INTEGER NM_IDEN_REQNEXT
C     INTEGER NM_IDEN_REQNINT
C     INTEGER NM_IDEN_REQGLB
C     INTEGER NM_IDEN_REQLCL
C     INTEGER NM_IDEN_DSYNC
C     INTEGER NM_IDEN_ISYNC
C     INTEGER NM_IDEN_GENDSYNC
C     INTEGER NM_IDEN_GENISYNC
C     INTEGER NM_IDEN_GENSYNC
C
C************************************************************************

      MODULE NM_IDEN_HOBJ_M

      USE NM_IDEN_M, ONLY : NM_IDEN_T
      IMPLICIT NONE

C---     Attributs statiques
      INTEGER, SAVE :: NM_IDEN_HBASE = 0

      CONTAINS
      
C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée NM_IDEN_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQSELF(HOBJ)
         
      USE ISO_C_BINDING

      TYPE (NM_IDEN_T), POINTER :: NM_IDEN_REQSELF
      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (NM_IDEN_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

D     CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
D     CALL ERR_ASR(ERR_GOOD())
D     CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      NM_IDEN_REQSELF => SELF
      RETURN
      END FUNCTION NM_IDEN_REQSELF

      END MODULE NM_IDEN_HOBJ_M
      
C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction NM_IDEN_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_CTR
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      USE NM_IDEN_M, ONLY : NM_IDEN_T, NM_IDEN_CTR_SELF
      USE ISO_C_BINDING, ONLY : C_LOC
      IMPLICIT NONE

      INTEGER, INTENT(OUT) :: HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NM_IDEN_CTR_SELF()

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   NM_IDEN_HBASE,
     &                                   C_LOC(SELF))

      NM_IDEN_CTR = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_CTR

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction NM_IDEN_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_DTR
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      USE NM_IDEN_M, ONLY: DEL
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (NM_IDEN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      IERR = DEL(SELF)

      IERR = OB_OBJN_DTR(HOBJ, NM_IDEN_HBASE)

      NM_IDEN_DTR = ERR_TYP()
      RETURN
      END FUNCTION NM_IDEN_DTR

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C
C Entrée:
C     NNT_P       Le nombre d'items globaux
C
C Sortie:
C     HOBJ        Le handle de l'objet initialisé
C
C Notes:
C     La table est passée par pointeur pour permettre un pointeur nul.
C************************************************************************
      FUNCTION NM_IDEN_INI(HOBJ, NNT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_INI
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NNT

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'

      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_INI = SELF%INI(NNT)
      RETURN
      END FUNCTION NM_IDEN_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_IDEN_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_RST
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'

      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_RST = SELF%RST()
      RETURN
      END FUNCTION NM_IDEN_RST

C************************************************************************
C Sommaire: Charge les données
C
C Description:
C     La fonction NM_IDEN_CHARGE charge les tables internes des
C     paramètres de configuration.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     NPROC          Nombre de process
C     NNL            Nombre de noeuds locaux
C     KDISTR         Table de distribution
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_CHARGE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_CHARGE
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'

      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_CHARGE = SELF%CHARGE()
      RETURN
      END FUNCTION NM_IDEN_CHARGE
      
C************************************************************************
C Sommaire: Synchronise les données REAL*8
C
C Description:
C     La fonction NM_IDEN_DSYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table VVAL
C     NNL         Dimension 2 de la table VVAL
C     VVAL        Table (NVAL, NNL) REAL*8 à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_DSYNC(HOBJ, NVAL, NNL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_DSYNC
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VVAL(NVAL, NNL)

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'

      TYPE (NM_IDEN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_DSYNC = SELF%DSYNC(NVAL, NNL, VVAL)
      RETURN
      END FUNCTION NM_IDEN_DSYNC

C************************************************************************
C Sommaire: Synchronise les données INTEGER
C
C Description:
C     La fonction NM_IDEN_ISYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension de la table KVAL
C     KVAL        Table INTEGER à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_ISYNC(HOBJ, NVAL, NNL, KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_ISYNC
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: KVAL(NVAL, NNL)

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'

      TYPE (NM_IDEN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_ISYNC = SELF%ISYNC(NVAL, NNL, KVAL)
      RETURN
      END FUNCTION NM_IDEN_ISYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_IDEN_GENDSYNC génère un objet de synchronisation pour
C     une table REAL*8 de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_GENDSYNC(HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_GENDSYNC
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (NM_IDEN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_GENDSYNC = SELF%GENDSYNC(NVAL, NNL, HSNC)
      RETURN
      END FUNCTION NM_IDEN_GENDSYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_IDEN_GENISYNC génère un objet de synchronisation pour
C     une table INTEGER de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_GENISYNC(HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_GENISYNC
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'

      TYPE (NM_IDEN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------
      
      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_GENISYNC = SELF%GENISYNC(NVAL, NNL, HSNC)
      RETURN
      END FUNCTION NM_IDEN_GENISYNC
      
C************************************************************************
C Sommaire: NM_IDEN_ESTNOPP
C
C Description:
C     La fonction NM_IDEN_ESTNOPP retourne .TRUE. si le noeud passé en
C     argument est privé au process.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_ESTNOPP(HOBJ, INLOC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_ESTNOPP
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'

      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_ESTNOPP = SELF%ESTNOPP(INLOC)
      RETURN
      END FUNCTION NM_IDEN_ESTNOPP

C************************************************************************
C Sommaire: NM_IDEN_REQNPRT
C
C Description:
C     La fonction NM_IDEN_REQNPRT retourne le nombre de sous-domaines de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNPRT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_REQNPRT
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_REQNPRT = SELF%REQNPRT()
      RETURN
      END FUNCTION NM_IDEN_REQNPRT

C************************************************************************
C Sommaire: NM_IDEN_REQNNL
C
C Description:
C     La fonction NM_IDEN_REQNNL retourne le nombre de noeuds local de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNNL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_REQNNL
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_REQNNL = SELF%REQNNL()
      RETURN
      END FUNCTION NM_IDEN_REQNNL

C************************************************************************
C Sommaire: NM_IDEN_REQNNP
C
C Description:
C     La fonction NM_IDEN_REQNNP retourne le nombre de noeuds privés de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNNP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_REQNNP
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_REQNNP = SELF%REQNNP()
      RETURN
      END FUNCTION NM_IDEN_REQNNP

C************************************************************************
C Sommaire: NM_IDEN_REQNNT
C
C Description:
C     La fonction NM_IDEN_REQNNT retourne le nombre de noeuds total de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNNT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_REQNNT
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_REQNNT = SELF%REQNNT()
      RETURN
      END FUNCTION NM_IDEN_REQNNT

C************************************************************************
C Sommaire: NM_IDEN_REQNEXT
C
C Description:
C     La fonction NM_IDEN_REQNEXT retourne le numéro de noeud global
C     correspondant au numéro local passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local à traduire en numéro global
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQNEXT(HOBJ, INLOC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_REQNEXT
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_REQNEXT = SELF%REQNEXT(INLOC)
      RETURN 
      END FUNCTION NM_IDEN_REQNEXT

C************************************************************************
C Sommaire: NM_IDEN_REQNINT
C
C Description:
C     La fonction NM_IDEN_REQNINT retourne le numéro de noeud local
C     correspondant au numéro global passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INGLB    Numéro de noeud global à traduire en numéro local
C
C Sortie:
C
C Notes:
C     En fait, pour accélérer, il faudrait monter
C     la table inverse.
C************************************************************************
      FUNCTION NM_IDEN_REQNINT(HOBJ, INGLB)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_REQNINT
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: INGLB

      INCLUDE 'nmiden.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_IDEN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_IDEN_REQSELF(HOBJ)
      NM_IDEN_REQNINT = SELF%REQNINT(INGLB)
      RETURN
      END FUNCTION NM_IDEN_REQNINT

     
C************************************************************************
C************************************************************************
C************************************************************************
C************************************************************************
      
      
C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction NM_IDEN_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_000
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'nmiden.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(NM_IDEN_HBASE,
     &                   'Renumbering - Local to Global')

      NM_IDEN_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction NM_IDEN_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_999
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'nmiden.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL NM_IDEN_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(NM_IDEN_HBASE,
     &                   NM_IDEN_DTR)

      NM_IDEN_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_IDEN_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_REQHBASE
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'nmiden.fi'
C------------------------------------------------------------------------

      NM_IDEN_REQHBASE = NM_IDEN_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_IDEN_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_HVALIDE
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      NM_IDEN_HVALIDE = OB_OBJN_HVALIDE(HOBJ, NM_IDEN_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si le type de l'objet est celui de la classe
C
C Description:
C     La fonction NM_IDEN_TVALIDE permet de valider le type d'un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé à le même type que
C     la classe. C'est un test plus léger mais moins poussé que HVALIDE.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_IDEN_TVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_IDEN_TVALIDE
CDEC$ ENDIF

      USE NM_IDEN_HOBJ_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmiden.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      NM_IDEN_TVALIDE = OB_OBJN_TVALIDE(HOBJ, NM_IDEN_HBASE)
      RETURN
      END
