C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Numérotation
C Objet:   COMPosite
C Type:    Concret
C
C Class: NM_COMP
C     INTEGER NM_COMP_000
C     INTEGER NM_COMP_999
C     INTEGER NM_COMP_CTR
C     INTEGER NM_COMP_DTR
C     INTEGER NM_COMP_INI
C     INTEGER NM_COMP_RST
C     SUBROUTINE NM_COMP_REQHBASE
C     LOGICAL NM_COMP_HVALIDE
C     INTEGER NM_COMP_CHARGE
C     INTEGER NM_COMP_REQNPRT
C     INTEGER NM_COMP_REQNNL
C     INTEGER NM_COMP_REQNNP
C     INTEGER NM_COMP_REQNNT
C     INTEGER NM_COMP_REQNEXT
C     INTEGER NM_COMP_REQNINT
C     INTEGER NM_COMP_DSYNC
C     INTEGER NM_COMP_ISYNC
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmcomp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(NM_COMP_NOBJMAX,
     &                   NM_COMP_HBASE,
     &                   'Renumbering - Compose')

      NM_COMP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmcomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

      INTEGER  IERR
      EXTERNAL NM_COMP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(NM_COMP_NOBJMAX,
     &                   NM_COMP_HBASE,
     &                   NM_COMP_DTR)

      NM_COMP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Retourne un handle sur l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_COMP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   NM_COMP_NOBJMAX,
     &                   NM_COMP_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NM_COMP_HVALIDE(HOBJ))
         IOB = HOBJ - NM_COMP_HBASE

         NM_COMP_NALGO(IOB) = 0
      ENDIF

      NM_COMP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_COMP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = NM_COMP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   NM_COMP_NOBJMAX,
     &                   NM_COMP_HBASE)

      NM_COMP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_COMP_INI(HOBJ, NALGO, HALGO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NALGO
      INTEGER HALGO(*)

      INCLUDE 'nmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER IALG
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
D     CALL ERR_PRE(NALGO .GT. 0)
D     DO IALG = 1, NALGO
CD        CALL ERR_PRE(???? HVALIDE N'EST PAS VIRTUEL)
D     ENDDO
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = NM_COMP_RST(HOBJ)

      IOB = HOBJ - NM_COMP_HBASE
      DO IALG = 1, NALGO
         NM_COMP_HALGO(IOB, IALG) = HALGO(IALG)
      ENDDO
      NM_COMP_NALGO(IOB) = NALGO

      NM_COMP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_COMP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - NM_COMP_HBASE
      NM_COMP_NALGO(IOB) = 0

      NM_COMP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_COMP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmcomp.fc'
C------------------------------------------------------------------------

      NM_COMP_REQHBASE = NM_COMP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_COMP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmcomp.fc'
C------------------------------------------------------------------------

      NM_COMP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  NM_COMP_NOBJMAX,
     &                                  NM_COMP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si le type de l'objet est celui de la classe
C
C Description:
C     La fonction NM_COMP_TVALIDE permet de valider le type d'un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé à le même type que
C     la classe. C'est un test plus léger mais moins poussé que HVALIDE.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_TVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_TVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmcomp.fc'
C------------------------------------------------------------------------

      NM_COMP_TVALIDE = OB_OBJC_TVALIDE(HOBJ,
     &                                  NM_COMP_NOBJMAX,
     &                                  NM_COMP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_COMP_CHARGE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IALG
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - NM_COMP_HBASE

C---     BOUCLE SUR LES ALGORITHMES
      DO IALG = 1, NM_COMP_NALGO(IOB)
         IERR = NM_NUMR_CHARGE(NM_COMP_HALGO(IOB, IALG))
      ENDDO

      NM_COMP_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NM_COMP_REQNPRT
C
C Description:
C     La fonction NM_COMP_REQNPRT retourne le nombre de sous-domaines du
C     partitionnement.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_REQNPRT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_REQNPRT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_COMP_REQNPRT')
      CALL ERR_ASR(.FALSE.)

      NM_COMP_REQNPRT = -1
      RETURN
      END

C************************************************************************
C Sommaire: NM_COMP_REQNNL
C
C Description:
C     La fonction NM_COMP_REQNNL retourne le nombre de noeuds local de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_REQNNL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_REQNNL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_COMP_REQNNL')
      CALL ERR_ASR(.FALSE.)

      NM_COMP_REQNNL = -1
      RETURN
      END

C************************************************************************
C Sommaire: NM_COMP_REQNNP
C
C Description:
C     La fonction NM_COMP_REQNNP retourne le nombre de noeuds privés de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_REQNNP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_REQNNP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_COMP_REQNNP')
      CALL ERR_ASR(.FALSE.)

      NM_COMP_REQNNP = -1
      RETURN
      END

C************************************************************************
C Sommaire: NM_COMP_REQNNT
C
C Description:
C     La fonction NM_COMP_REQNNT retourne le nombre de noeuds total de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_REQNNT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_REQNNT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_COMP_REQNNT')
      CALL ERR_ASR(.FALSE.)

      NM_COMP_REQNNT = -1
      RETURN
      END

C************************************************************************
C Sommaire: NM_COMP_REQNEXT
C
C Description:
C     La fonction NM_COMP_REQNEXT retourne le numéro de noeud global
C     correspondant au numéro local passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local à traduire en numéro global
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_REQNEXT(HOBJ, INLOC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_REQNEXT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER INLOC

      INCLUDE 'nmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmlcgl.fi'
      INCLUDE 'nmcomp.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_COMP_REQNEXT')
      CALL ERR_ASR(.FALSE.)

      NM_COMP_REQNEXT = -1
      RETURN
      END

C************************************************************************
C Sommaire: NM_COMP_REQNINT
C
C Description:
C     La fonction NM_COMP_REQNINT retourne le numéro de noeud local
C     correspondant au numéro global passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INGLB    Numéro de noeud global à traduire en numéro local
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_REQNINT(HOBJ, INGLB)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_REQNINT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER INGLB

      INCLUDE 'nmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmlcgl.fi'
      INCLUDE 'nmcomp.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_COMP_REQNINT')
      CALL ERR_ASR(.FALSE.)

      NM_COMP_REQNINT = -1
      RETURN
      END

C************************************************************************
C Sommaire: Synchronise les données REAL*8
C
C Description:
C     La fonction NM_COMP_DSYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table VVAL
C     NNL         Dimension 2 de la table VVAL
C     VVAL        Table (NVAL, NNL) REAL*8 à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_DSYNC(HOBJ, NVAL, NNL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_DSYNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      REAL*8  VVAL(NVAL, NNL)

      INCLUDE 'nmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_COMP_DSYNC')
      CALL ERR_ASR(.FALSE.)

      NM_COMP_DSYNC = ERR_ERR
      RETURN
      END

C************************************************************************
C Sommaire: Synchronise les données INTEGER
C
C Description:
C     La fonction NM_COMP_ISYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension de la table KVAL
C     KVAL        Table INTEGER à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_COMP_ISYNC(HOBJ, NVAL, KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_COMP_ISYNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER KVAL(NVAL)

      INCLUDE 'nmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_COMP_ISYNC')
      CALL ERR_ASR(.FALSE.)

      NM_COMP_ISYNC = ERR_ERR
      RETURN
      END

