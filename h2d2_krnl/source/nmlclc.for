C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   LoCal-LoCal
C Type:    Concret
C Interface:
C   H2D2 Module: NM
C      H2D2 Class: NM_LCLC
C         TYPE(NM_LCLC_T), POINTER NM_LCLC_CTR_SELF
C         INTEGER NM_LCLC_DTR
C         INTEGER NM_LCLC_INI
C         INTEGER NM_LCLC_RST
C         INTEGER NM_LCLC_RAZ
C         INTEGER NM_LCLC_CHARGE
C         INTEGER NM_LCLC_DSYNC
C         INTEGER NM_LCLC_ISYNC
C         INTEGER NM_LCLC_GENDSYNC
C         INTEGER NM_LCLC_GENISYNC
C         INTEGER NM_LCLC_REQNPRT
C         INTEGER NM_LCLC_REQNNL
C         INTEGER NM_LCLC_REQNNP
C         INTEGER NM_LCLC_REQNNT
C         LOGICAL NM_LCLC_ESTNOPP
C         INTEGER NM_LCLC_REQNEXT
C         INTEGER NM_LCLC_REQNINT
C         INTEGER NM_LCLC_CLCORI
C
C************************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! Note: 2018-01-02
!!!
!!! Les SUBMODULE(s) ne sont supportés que par Intel >= 16.4 et GCC >= 6.0
!!! Pour rester compatible avec les versions Intel antérieures (CMC) et
!!! pour pouvoir compiler Sun, le choix est fait de ne pas utiliser de
!!! SUBMODULE. Pour simuler/conserver la séparation déclaration/définition,
!!! le SUBMODULE est transformé en fichier inclus dans le section
!!! CONTAINS du module.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      !!!SUBMODULE(NM_LCLC_M) NM_LCLC_M_SRC1

      !!!CONTAINS

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction NM_LCLC_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_CTR_SELF()

      TYPE(NM_LCLC_T), POINTER :: NM_LCLC_CTR_SELF

      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise
      IERR = SELF%RAZ()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      NM_LCLC_CTR_SELF => SELF
      RETURN
      END FUNCTION NM_LCLC_CTR_SELF

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction NM_LCLC_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_DTR(SELF)

      INTEGER :: NM_LCLC_DTR
      CLASS(NM_LCLC_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = SELF%RST()
!!!      DEALLOCATE(SELF)

      NM_LCLC_DTR = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_DTR


C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La méthode NM_LCLC_INI initialise l'objet à partir soit du nom de
C     fichier, soit des dimensions et de la table. Pour initialiser
C     l'objet avec le nom de fichier, il faut mettre les autres
C     paramètres à 0. Sinon, il faut donner une chaîne vide pour le nom
C     de fichier.
C
C Entrée:
C     NOMFIC      Nom du fichier contenant la table
C     NNT         Le nombre d'items
C     KDIST       La table de distribution des items
C
C Sortie:
C     HOBJ        Le handle de l'objet initialisé
C
C Notes:
C     Le chargement par fichier n'est pas supporté car FD_NUMR demande
C     une table contenant le même nombre de sous-domaines que le
C     nombre de process, ce qui n'est pas valide pour une numérotation
C     locale.
C
C     La table est passée par pointeur pour permettre un pointeur nul.
C************************************************************************
      FUNCTION NM_LCLC_INI(SELF,
     &                     NOMFIC,
     &                     NNT_P,
     &                     LDIST_P)

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1, SO_ALLC_KPTR2

      INTEGER :: NM_LCLC_INI
      CLASS(NM_LCLC_T), INTENT(INOUT) :: SELF
      CHARACTER*(*), INTENT(IN) :: NOMFIC
      INTEGER, INTENT(IN) :: NNT_P
      INTEGER, INTENT(IN) :: LDIST_P

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'fdnumr.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HFNUM
      INTEGER NSSDM
      INTEGER NNT
      LOGICAL MODFIC
      TYPE(SO_ALLC_KPTR2_T) :: KDIST
C------------------------------------------------------------------------

C--      Contrôles des paramètres
      MODFIC = SP_STRN_LEN(NOMFIC) .GT. 0
      IF (MODFIC) THEN
         IF (NNT_P   .NE. 0) GOTO 9900
         IF (LDIST_P .NE. 0) GOTO 9900
      ELSE
         IF (NNT_P   .LE. 0) GOTO 9900
      ENDIF

C---     Reset les données
      IERR = SELF%RST()

C---     Charge la table à partir du fichier si nécessaire
      IF (MODFIC) THEN

C---        !!! NON FONCTIONNEL !!!
         CALL LOG_TODO('NM_LCLC_INI: Chargement par fichier invalide')
         CALL ERR_ASR(.FALSE.)

C---        Construis le fichier de données
         HFNUM = 0
         IF (ERR_GOOD()) IERR = FD_NUMR_CTR(HFNUM)
         IF (ERR_GOOD()) THEN
            IERR = FD_NUMR_INI(HFNUM,
     &                         NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &                         IO_MODE_LECTURE + IO_MODE_ASCII)
         ENDIF

C---       Demande les dimensions
D        NSSDM = 0
D        NNT  = 0
         IF (ERR_GOOD()) NSSDM = FD_NUMR_REQNPROC(HFNUM)
         IF (ERR_GOOD()) NNT   = FD_NUMR_REQNNTG (HFNUM)
D        CALL ERR_ASR(ERR_BAD() .OR. NSSDM .GT. 0)
D        CALL ERR_ASR(ERR_BAD() .OR. NNT   .GT. 0)

C---        Alloue l'espace de travail et la table globale des process
         KDIST = SO_ALLC_KPTR2()
         IF (ERR_GOOD()) KDIST = SO_ALLC_KPTR2(NSSDM+2, NNT)

C---        Lis la table de distribution
         IF (ERR_GOOD())
     &      IERR = FD_NUMR_LIS (HFNUM,
     &                          NSSDM,
     &                          NSSDM+2,
     &                          NNT, ! attention, nnl !!!!!!!!!!!!!IJOIJ
     &                          KDIST%KPTR)

C---        Détruis le fichier de données
         IF (ERR_GOOD()) IERR = FD_NUMR_DTR(HFNUM)
      ELSE

C---        Les dimensions
         NNT   = NNT_P

C---        Alloue l'espace de travail et la table globale des process
         NSSDM = 1
         IF (ERR_GOOD()) KDIST = SO_ALLC_KPTR2(NSSDM+2, NNT)

C---        Copie la table passée comme paramètres
         IF (ERR_GOOD())
     &      CALL ICOPY((NSSDM+2)*NNT,
     &                 KA(SO_ALLC_REQKIND(KA, LDIST_P)),
     &                 1,
     &                 KDIST%KPTR,
     &                 1)
      ENDIF

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%KDIST = KDIST
         SELF%NNT   = NNT
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_INVALIDES'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_LCLC_INI = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La mémoire est désallouée automatiquement dans RAZ
C************************************************************************
      FUNCTION NM_LCLC_RST(SELF)

      INTEGER :: NM_LCLC_RST
      CLASS(NM_LCLC_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

C---     Remise à  zéro
      IERR = SELF%RAZ()

      NM_LCLC_RST = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_RST

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_LCLC_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1, SO_ALLC_KPTR2

      INTEGER :: NM_LCLC_RAZ
      CLASS(NM_LCLC_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

C---     Reset les attributs
      SELF%KDIST   = SO_ALLC_KPTR2()
      SELF%KCOR    = SO_ALLC_KPTR1()
      SELF%KCORINV = SO_ALLC_KPTR1()
      SELF%NNT     = 0

      NM_LCLC_RAZ = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_RAZ

C************************************************************************
C Sommaire: Charge les données
C
C Description:
C     La fonction NM_LCLC_CHARGE va charger les données pour le
C     temps demandé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     TEMPS       Temps pour lequel les données sont à charger
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_CHARGE(SELF)

      USE SO_ALLC_M

      INTEGER :: NM_LCLC_CHARGE
      CLASS(NM_LCLC_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue l'espace
      IF (ERR_GOOD()) SELF%KCOR    = SO_ALLC_KPTR1(SELF%NNT)
      IF (ERR_GOOD()) SELF%KCORINV = SO_ALLC_KPTR1(SELF%NNT)

C---     Copie les correspondances
      IF (ERR_GOOD()) SELF%KCOR%KPTR(1:SELF%NNT) = SELF%KDIST%KPTR(1,:)

C---     Trouve les correspondances inverses
      IF (ERR_GOOD()) IERR = SELF%CLCORI()

C---     Libère la mémoire de la table de distribution
      SELF%KDIST = SO_ALLC_KPTR2()

      NM_LCLC_CHARGE = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_CHARGE

C************************************************************************
C Sommaire: Synchronise les données REAL*8
C
C Description:
C     La fonction NM_LCLC_DSYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table VVAL
C     NNL         Dimension 2 de la table VVAL
C     VVAL        Table (NVAL, NNL) REAL*8 à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_DSYNC(SELF, NVAL, NNL, VVAL)

      INTEGER :: NM_LCLC_DSYNC
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VVAL(:,:)

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_LCLC_DSYNC')
      CALL ERR_ASR(.FALSE.)

      NM_LCLC_DSYNC = ERR_ERR
      RETURN
      END FUNCTION NM_LCLC_DSYNC

C************************************************************************
C Sommaire: Synchronise les données INTEGER
C
C Description:
C     La fonction NM_LCLC_ISYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table KVAL
C     NNL         Dimension 2 de la table KVAL
C     KVAL        Table INTEGER à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_ISYNC(SELF, NVAL, NNL, KVAL)

      INTEGER :: NM_LCLC_ISYNC
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: KVAL(:,:)

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      CALL LOG_TODO('NM_LCLC_ISYNC')
      CALL ERR_ASR(.FALSE.)

      NM_LCLC_ISYNC = ERR_ERR
      RETURN
      END FUNCTION NM_LCLC_ISYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_LCLC_DSYNC génère un objet de synchronisation pour
C     une table REAL*8 de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_GENDSYNC(SELF, NVAL, NNL, HSNC)

      INTEGER :: NM_LCLC_GENDSYNC
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = MP_SYNC_CTR   (HSNC)
      IF (ERR_GOOD()) IERR = MP_SYNC_INILCL(HSNC)

      NM_LCLC_GENDSYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_GENDSYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_LCLC_GENISYNC génère un objet de synchronisation pour
C     une table INTEGER de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_GENISYNC(SELF, NVAL, NNL, HSNC)

      INTEGER :: NM_LCLC_GENISYNC
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = MP_SYNC_CTR   (HSNC)
      IF (ERR_GOOD()) IERR = MP_SYNC_INILCL(HSNC)

      NM_LCLC_GENISYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_GENISYNC

C************************************************************************
C Sommaire: NM_LCLC_REQNPRT
C
C Description:
C     La fonction NM_LCLC_REQNPRT retourne le nombre de sous-domaines de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNPRT(SELF)

      INTEGER :: NM_LCLC_REQNPRT
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      NM_LCLC_REQNPRT = 1
      RETURN
      END FUNCTION NM_LCLC_REQNPRT

C************************************************************************
C Sommaire: NM_LCLC_REQNNL
C
C Description:
C     La fonction NM_LCLC_REQNNL retourne le nombre de noeuds local de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNNL(SELF)

      INTEGER :: NM_LCLC_REQNNL
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      NM_LCLC_REQNNL = SELF%NNT
      RETURN
      END FUNCTION NM_LCLC_REQNNL

C************************************************************************
C Sommaire: NM_LCLC_REQNNP
C
C Description:
C     La fonction NM_LCLC_REQNNP retourne le nombre de noeuds privés de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNNP(SELF)

      INTEGER :: NM_LCLC_REQNNP
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      NM_LCLC_REQNNP = SELF%NNT
      RETURN
      END FUNCTION NM_LCLC_REQNNP

C************************************************************************
C Sommaire: NM_LCLC_REQNNT
C
C Description:
C     La fonction NM_LCLC_REQNNT retourne le nombre de noeuds total de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNNT(SELF)

      INTEGER :: NM_LCLC_REQNNT
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      NM_LCLC_REQNNT = SELF%NNT
      RETURN
      END FUNCTION NM_LCLC_REQNNT

C************************************************************************
C Sommaire: NM_LCLC_ESTNOPP
C
C Description:
C     La fonction NM_LCLC_ESTNOPP retourne .TRUE. si le noeud passé en
C     argument est privé au process.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_ESTNOPP(SELF, INLOC)

      LOGICAL :: NM_LCLC_ESTNOPP
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'err.fi'
C------------------------------------------------------------------------
D     CALL ERR_PRE(INLOC .GT. 0)
D     CALL ERR_PRE(INLOC .LE. SELF%NNT)
C------------------------------------------------------------------------

      NM_LCLC_ESTNOPP = .TRUE.
      RETURN
      END FUNCTION NM_LCLC_ESTNOPP

C************************************************************************
C Sommaire: NM_LCLC_REQNEXT
C
C Description:
C     La fonction NM_LCLC_REQNEXT retourne le numéro de noeud dans la
C     nouvelle numérotation correspondant au numéro dans la numérotation
C     originale passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local à traduire en numéro global
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNEXT(SELF, INLOC)

      INTEGER :: NM_LCLC_REQNEXT
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: INLOC
C------------------------------------------------------------------------

      NM_LCLC_REQNEXT = SELF%KCOR%KPTR(INLOC)
      RETURN
      END FUNCTION NM_LCLC_REQNEXT

C************************************************************************
C Sommaire: NM_LCLC_REQNINT
C
C Description:
C     La fonction NM_LCLC_REQNINT retourne le numéro de noeud dans la
C     numérotation originale correspondant au numéro dans la nouvelle
C     numérotation passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INGLB    Numéro de noeud global à traduire en numéro local
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNINT(SELF, INGLB)

      INTEGER :: NM_LCLC_REQNINT
      CLASS(NM_LCLC_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: INGLB
C------------------------------------------------------------------------

      NM_LCLC_REQNINT = SELF%KCORINV%KPTR(INGLB)
      RETURN
      END FUNCTION NM_LCLC_REQNINT

C************************************************************************
C Sommaire: Trouve les correspondances inverses
C
C Description:
C     La fonction privée NM_LCLC_CLCORI trouve les correspondances
C     inverses à partir du tableau de correspondances
C
C Entrée:
C     NNT         Dimension de la table
C     KCOR        Table de correspondances
C
C Sortie:
C     KCORINV     Table de correspondances inverses
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_CLCORI(SELF)

      INTEGER :: NM_LCLC_CLCORI
      CLASS(NM_LCLC_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IN
      INTEGER INNOUV
      INTEGER, POINTER :: KDIR_P(:)
      INTEGER, POINTER :: KINV_P(:)
C------------------------------------------------------------------------

      KDIR_P => SELF%KCOR%KPTR
      KINV_P => SELF%KCOR%KPTR
      DO IN=1,SELF%NNT
         INNOUV = KDIR_P(IN)
         KINV_P(INNOUV) = IN
      ENDDO

      NM_LCLC_CLCORI = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_CLCORI

      !!! END SUBMODULE NM_LCLC_M_SRC1
