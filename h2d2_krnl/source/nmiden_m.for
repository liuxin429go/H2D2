C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   IDENtité
C Type:
C Interface:
C   H2D2 Module: NM
C      H2D2 Class: NM_IDEN
C         FTN (Sub)Module: NM_IDEN_M
C            Public:
C            Private:
C               TYPE(NM_IDEN_T), POINTER NM_IDEN_CTR_SELF
C            
C            FTN Type: NM_IDEN_T
C               Public:
C                  INTEGER NM_IDEN_DTR
C                  INTEGER NM_IDEN_INI
C                  INTEGER NM_IDEN_RST
C                  INTEGER NM_IDEN_CHARGE
C                  INTEGER NM_IDEN_DSYNC
C                  INTEGER NM_IDEN_ISYNC
C                  INTEGER NM_IDEN_GENDSYNC
C                  INTEGER NM_IDEN_GENISYNC
C                  INTEGER NM_IDEN_REQNPRT
C                  INTEGER NM_IDEN_REQNNL
C                  INTEGER NM_IDEN_REQNNP
C                  INTEGER NM_IDEN_REQNNT
C                  LOGICAL NM_IDEN_ESTNOPP
C                  INTEGER NM_IDEN_REQNEXT
C                  INTEGER NM_IDEN_REQNINT
C               Private:
C
C************************************************************************

      MODULE NM_IDEN_M

      USE NM_NUMR_M, ONLY: NM_NUMR_T
      IMPLICIT NONE
      PUBLIC

C========================================================================
C========================================================================
C---     Type de donnée associé à la classe
      TYPE, EXTENDS (NM_NUMR_T) :: NM_IDEN_T
         INTEGER, PRIVATE :: NNT
      CONTAINS
         ! ---  Méthodes de la classe
         !!!PROCEDURE, PUBLIC :: NM_IDEN_CTR
         PROCEDURE, PUBLIC :: DTR      => NM_IDEN_DTR
         PROCEDURE, PUBLIC :: INI      => NM_IDEN_INI
         PROCEDURE, PUBLIC :: RST      => NM_IDEN_RST

         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: CHARGE   => NM_IDEN_CHARGE
         PROCEDURE, PUBLIC :: DSYNC    => NM_IDEN_DSYNC
         PROCEDURE, PUBLIC :: ISYNC    => NM_IDEN_ISYNC
         PROCEDURE, PUBLIC :: GENDSYNC => NM_IDEN_GENDSYNC
         PROCEDURE, PUBLIC :: GENISYNC => NM_IDEN_GENISYNC

         PROCEDURE, PUBLIC :: REQNPRT  => NM_IDEN_REQNPRT
         PROCEDURE, PUBLIC :: REQNNL   => NM_IDEN_REQNNL
         PROCEDURE, PUBLIC :: REQNNP   => NM_IDEN_REQNNP
         PROCEDURE, PUBLIC :: REQNNT   => NM_IDEN_REQNNT

         PROCEDURE, PUBLIC :: ESTNOPP  => NM_IDEN_ESTNOPP
         PROCEDURE, PUBLIC :: REQNEXT  => NM_IDEN_REQNEXT
         PROCEDURE, PUBLIC :: REQNINT  => NM_IDEN_REQNINT
      END TYPE NM_IDEN_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: NM_IDEN_CTR_SELF
      INTERFACE DEL
         PROCEDURE NM_IDEN_DTR
      END INTERFACE DEL

C========================================================================
C========================================================================
      CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! Note: 2018-01-02
!!!
!!! Les SUBMODULE(s) ne sont supportés que par Intel >= 16.4 et GCC >= 6.0
!!! Pour rester compatible avec les versions Intel antérieures (CMC) et
!!! pour pouvoir compiler Sun, le choix est fait de ne pas utiliser de
!!! SUBMODULE. Pour simuler/conserver la séparation déclaration/définition,
!!! le SUBMODULE est transformé en fichier inclus dans le section
!!! CONTAINS du module.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      INCLUDE 'nmiden.for'

      END MODULE NM_IDEN_M
