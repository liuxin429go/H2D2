C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Interface Commandes: NUMeRotation
C Objet:   PRoXY
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Fonction vide qui ne devrait jamais être appelée.
C
C Description:
C     La fonction IC_NM_PRXY_XEQCTR(IPRM) est une fonction vide car il
C     n'est pas prévu de construire un proxy via une commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_NM_PRXY_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_PRXY_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'nmprxy_ic.fi'
      INCLUDE 'err.fi'

C------------------------------------------------------------------------
      CALL ERR_PRE(.FALSE.)
C------------------------------------------------------------------------

      IPRM = ' '

      IC_NM_PRXY_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_NM_PRXY_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type NM_PRXY.
C     Le proxy résout directement les appels aux méthodes virtuelles.
C     Pour toutes les autres méthodes, on retourne le handle de l'objet
C     géré.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_NM_PRXY_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_PRXY_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'nmprxy_ic.fi'
!!      INCLUDE 'icsolv.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HVAL
C------------------------------------------------------------------------
      CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_TODO('IC_NM_LCGL_XEQMTH: ajoute les methodes virt.')

!!C     <comment>Reads and loads a previously computed renumbering</comment>
      IF (IMTH .EQ. 'load') THEN
!!C        <include>IC_SOLV_XEQ@icsolv.for</include>
!!         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>Class destructor</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = NM_PRXY_DTR(HOBJ)

      ELSE
         IOB = HOBJ - NM_PRXY_HBASE
         HVAL = NM_PRXY_HNUM(IOB)
         WRITE(IPRM, '(2A,I12)') 'X', ',', HVAL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_NM_PRXY_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_PRXY_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_PRXY_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmprxy_ic.fi'
C-------------------------------------------------------------------------

      IC_NM_PRXY_REQCLS = '#__dummy_placeholder__#__NM_PROXY__'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_PRXY_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_PRXY_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmprxy_ic.fi'
      INCLUDE 'nmprxy.fi'
C-------------------------------------------------------------------------

      IC_NM_PRXY_REQHDL = NM_PRXY_REQHBASE()
      RETURN
      END
