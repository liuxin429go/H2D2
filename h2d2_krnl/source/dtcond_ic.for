C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_COND_XEQCTR
C     INTEGER IC_DT_COND_XEQMTH
C     CHARACTER*(32) IC_DT_COND_REQCLS
C     INTEGER IC_DT_COND_REQHDL
C   Private:
C     SUBROUTINE IC_DT_COND_XEQCTRFIC
C     SUBROUTINE IC_DT_COND_XEQCTRDIR
C     INTEGER IC_DT_COND_XEQCTRLST
C     INTEGER IC_DT_COND_REQMOD
C     SUBROUTINE IC_DT_COND_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_COND_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_COND_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtcond_ic.fi'
      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtcond_ic.fc'

      INTEGER IERR
      INTEGER IMOD
      INTEGER HOBJ
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_COND_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Extrait le mode
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IERR = IC_DT_COND_REQMOD(IPRM, IMOD)

C---     Appel le constructeur spécialisé
      IF (IMOD .EQ. IC_DT_COND_MOD_FIC) THEN
C        <include>IC_DT_COND_XEQCTRFIC@dtcond_ic.for</include>
         IERR = IC_DT_COND_XEQCTRFIC(IPRM, HOBJ)
      ELSEIF (IMOD .EQ. IC_DT_COND_MOD_DIR) THEN
C        <include>IC_DT_COND_XEQCTRDIR@dtcond_ic.for</include>
         IERR = IC_DT_COND_XEQCTRDIR(IPRM, HOBJ)
      ELSEIF (IMOD .EQ. IC_DT_COND_MOD_LST) THEN
C        <include>IC_DT_COND_XEQCTRLST@dtcond_ic.for</include>
         IERR = IC_DT_COND_XEQCTRLST(IPRM, HOBJ)
      ENDIF

C---     Imprime l'objet
      IF (ERR_GOOD()) IERR = DT_COND_PRN(HOBJ)

      IF (ERR_BAD()) GOTO 9988
      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_COND_AID()

9999  CONTINUE
      IC_DT_COND_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ est retourné 2 fois, en argument et dans IPRM.
C     Il faut IPRM pour que la commande soit extraite, et le retour de HOBJ
C     est pour éviter à l'appelant de l'extraire.
C************************************************************************
      FUNCTION IC_DT_COND_XEQCTRFIC(IPRM, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       HOBJ

      INCLUDE 'dtcond_ic.fi'
      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtcond_ic.fc'

      INTEGER         IERR, IRET
      INTEGER         HLST
      CHARACTER*(256) NOMFIC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Lis les paramètres
C     <comment>Name of the condition file</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IRET .NE. 0) GOTO 9901
      CALL SP_STRN_TRM(NOMFIC)

C---     Valide
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902

C---     Construis et initialise l'objet
      HOBJ = 0
      HLST = 0
      IF (ERR_GOOD()) IERR = DT_COND_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = DT_COND_INI(HOBJ, HLST, NOMFIC)

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the condition</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>condition</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C  </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_FICHIER_INVALIDE',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_DT_COND_XEQCTRFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ est retourné 2 fois, en argument et dans IPRM.
C     Il faut IPRM pour que la commande soit extraite, et le retour de HOBJ
C     est pour éviter à l'appelant de l'extraire.
C************************************************************************
      FUNCTION IC_DT_COND_XEQCTRDIR(IPRM, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       HOBJ

      INCLUDE 'dtcond_ic.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_lc.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtcond_ic.fc'

      INTEGER IERR, IRET
      INTEGER IRCR
      INTEGER MR, L
      INTEGER HLST
      CHARACTER*(256) NOMDIR, SPATRN, NOMTMP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      IRET = 0
      HLST = 0
      NOMTMP = ' '

C---     Lis les paramètres
C     <comment>Name of the root directory</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, NOMDIR)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Search pattern (as in "*.cnd")</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 2, SPATRN)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Do recursive search [0, 1] (default 1 == True) </comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 3, IRCR)
      IF (IRET .NE. 0) IRCR = 1
      CALL SP_STRN_TRM(NOMDIR)
      CALL SP_STRN_TRM(SPATRN)

C---     Valide
      !!IF (SP_STRN_LEN(NOMDIR) .LE. 0) GOTO 9902
      IF (SP_STRN_LEN(SPATRN) .LE. 0) GOTO 9903

C---     Nom de fichier temporaire
      IF (ERR_GOOD()) THEN
         IRET = C_OS_FICTMP(NOMTMP)
         IF (IRET .NE. 0) GOTO 9905
      ENDIF

C---     Charge le fichier temporaire avec les noms de fichier
      IF (ERR_GOOD()) THEN
         IRCR = F_LC_L2I( F_LC_I2L(IRCR) )   ! Normalise
         IRET = C_OS_NORMPATH(NOMDIR)
         IRET = C_OS_DIR(NOMDIR(1:SP_STRN_LEN(NOMDIR)),
     &                   SPATRN(1:SP_STRN_LEN(SPATRN)),
     &                   NOMTMP(1:SP_STRN_LEN(NOMTMP)),
     &                   IRCR)
         IF (IRET .NE. 0) GOTO 9906
      ENDIF

C---     Transfert le fichier en liste
      IF (ERR_GOOD()) THEN
         IERR = DS_LIST_CTR(HLST)
         IERR = DS_LIST_INI(HLST)
      ENDIF

C---     Transfert le fichier en liste
      IF (ERR_GOOD()) THEN
         MR = IO_UTIL_FREEUNIT()
         OPEN(UNIT  = MR,
     &        FILE  = NOMTMP(1:SP_STRN_LEN(NOMTMP)),
     &        STATUS= 'OLD',
     &        ERR   = 9907)
         DO WHILE(ERR_GOOD())      ! Squat NOMDIR
            READ(MR,'(A)', ERR = 9908, END = 199) NOMDIR
            IERR = DS_LIST_AJTVAL(HLST, NOMDIR)
         ENDDO
199      CONTINUE
         CLOSE(MR)
      ENDIF

C---     Construis et initialise l'objet
      HOBJ = 0
      NOMDIR = ' '      ! Squat NOMDIR
      IF (ERR_GOOD()) IERR = DT_COND_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = DT_COND_INI(HOBJ, HLST, NOMDIR)

C---     Nettoie
      CALL ERR_PUSH() 
      IF (SP_STRN_LEN(NOMTMP) .GT. 0)
     &   IRET = C_OS_DELFIC(NOMTMP(1:SP_STRN_LEN(NOMTMP)))
      IF (DS_LIST_HVALIDE(HLST))
     &   IERR = DS_LIST_DTR(HLST)
      CALL ERR_POP()

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the condition</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>condition</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C  </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_DIR_INVALIDE',': ',
     &                        NOMDIR(1:SP_STRN_LEN(NOMDIR))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A)') 'ERR_PATTERN_INVALIDE',': ',
     &                        SPATRN(1:SP_STRN_LEN(SPATRN))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF, '(A)') 'ERR_CREE_FICTMP'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF, '(A)') 'ERR_OS_DIR'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT('MSG_ROOT_DIR: ' // NOMDIR(1:SP_STRN_LEN(NOMDIR)))
      CALL ERR_AJT('MSG_PATTERN:  ' // SPATRN(1:SP_STRN_LEN(SPATRN)))
      GOTO 9999
9907  WRITE(ERR_BUF, '(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                     NOMTMP(1:SP_STRN_LEN(NOMTMP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9908  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_FICHIER',': ',
     &                     NOMTMP(1:SP_STRN_LEN(NOMTMP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
C---     Nettoie
      CALL ERR_PUSH()
      IF (DS_LIST_HVALIDE(HLST)) IERR = DS_LIST_CTR(HLST)
      L = SP_STRN_LEN(NOMTMP)
      IF (L .GT. 0) IRET = C_OS_DELFIC(NOMTMP(1:L))
      CALL ERR_POP()

      IC_DT_COND_XEQCTRDIR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ est retourné 2 fois, en argument et dans IPRM.
C     Il faut IPRM pour que la commande soit extraite, et le retour de HOBJ
C     est pour éviter à l'appelant de l'extraire.
C************************************************************************
      FUNCTION IC_DT_COND_XEQCTRLST(IPRM, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       HOBJ

      INCLUDE 'dtcond_ic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtcond_ic.fc'

      INTEGER IERR, IRET
      INTEGER HLST
      CHARACTER*(8) NOMFIC
      CHARACTER*(8) MODE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      HLST = 0
      IERR = ERR_OK
      IRET = 0

C---     Lis les paramètres
C     <comment>Handle on a list of condition file names</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HLST)
      IF (IRET .NE. 0) GOTO 9901

C---     Valide
      IF (.NOT. DS_LIST_HVALIDE(HLST)) GOTO 9902

C---     Construis et initialise l'objet
      HOBJ = 0
      NOMFIC = ' '
      IF (ERR_GOOD()) IERR = DT_COND_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = DT_COND_INI(HOBJ, HLST, NOMFIC)

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the condition</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>condition</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C  </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ', HLST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HLST, 'MSG_LISTE')
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ',
     &                        MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_DT_COND_XEQCTRLST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_COND_REQMOD(IPRM, IMOD)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       IMOD

      INCLUDE 'dslist.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtcond_ic.fc'

      INTEGER IERR, IRET
      INTEGER HLST
      INTEGER NTOK
      CHARACTER*(256) STR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IMOD = IC_DT_COND_MOD_INDEFINI

C---     Lis le 1er paramètre
      IRET = 0
      IF (IRET .EQ. 0) NTOK = SP_STRN_NTOK(IPRM, ',')
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS (IPRM, ',', 1, STR)
      IF (IRET .NE. 0) GOTO 9901
      CALL SP_STRN_TRM(STR)

C---     Valide
      IF (NTOK .LT. 1 .OR. NTOK .GT. 3) GOTO 9901

C---     Détecte une liste ou un maillage
      HLST = 0
      SELECT CASE(NTOK)
         CASE(1)
            IF (SP_STRN_LEN(STR) .LE. 0) GOTO 9902
            IF (SP_STRN_UINT(STR)) THEN
               IRET = SP_STRN_TKI(STR, ',', 1, HLST)
               IF (IRET .NE. 0) GOTO 9901
               IF (DS_LIST_HVALIDE(HLST)) THEN
                  IMOD = IC_DT_COND_MOD_LST
               ELSE
                  IMOD = IC_DT_COND_MOD_FIC
               ENDIF
            ELSE
               IMOD = IC_DT_COND_MOD_FIC
            ENDIF
         CASE(2)
            IMOD = IC_DT_COND_MOD_DIR
         CASE(3)
            IMOD = IC_DT_COND_MOD_DIR
      END SELECT

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_FICHIER_INVALIDE',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_DT_COND_REQMOD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_COND_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_COND_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dtcond_ic.fi'
      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_COND_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_COND_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DT_COND_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_COND_AID()

9999  CONTINUE
      IC_DT_COND_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_COND_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_COND_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtcond_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>condition</b> represents the conditions of the boundary
C  conditions. The boundary conditions are conditions that must be
C  satisfied by the degrees of freedom on the boundary of the simulation
C  domain. They vary in time.
C</comment>
      IC_DT_COND_REQCLS = 'condition'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_COND_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_COND_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtcond_ic.fi'
      INCLUDE 'dtcond.fi'
C-------------------------------------------------------------------------

      IC_DT_COND_REQHDL = DT_COND_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_COND_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_COND_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtcond_ic.hlp')

      RETURN
      END

