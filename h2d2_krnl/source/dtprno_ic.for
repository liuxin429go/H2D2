C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_PRNO_XEQCTR
C     INTEGER IC_DT_PRNO_XEQMTH
C     CHARACTER*(32) IC_DT_PRNO_REQCLS
C     INTEGER IC_DT_PRNO_REQHDL
C   Private:
C     INTEGER IC_DT_PRNO_PRN
C     INTEGER IC_DT_PRNO_XEQMTH_ADD
C     SUBROUTINE IC_DT_PRNO_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PRNO_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PRNO_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtprno_ic.fi'
      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprno_ic.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER I
      INTEGER NHVNO
      INTEGER NVNOMAX
      PARAMETER (NVNOMAX = 20)
      INTEGER KVNOD(NVNOMAX)
      INTEGER IDUM(1)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_PRNO_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      NHVNO = 0
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         I = 0
100      CONTINUE
            IF (I .GE. NVNOMAX) GOTO 9901
            I = I + 1
C           <comment>List of the handles on nodal values, comma separated.
C           List can be empty.</comment>
            IERR = SP_STRN_TKI(IPRM, ',', I, KVNOD(I))
            IF (IERR .EQ. -1) GOTO 199
            IF (IERR .NE.  0) GOTO 9902
         GOTO 100
199      CONTINUE
         NHVNO = I - 1
      ENDIF

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_PRNO_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = DT_PRNO_INI(HOBJ)
      DO I=1, NHVNO
         IF (ERR_GOOD()) IERR = DT_PRNO_ADDVNO(HOBJ, KVNOD(I), -1, IDUM)
      ENDDO

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) IERR = IC_DT_PRNO_PRN(HOBJ)

CC---     IMPRIME LES VALEURS
C      IF (ERR_GOOD()) THEN
C         HFRM = GR_GRID_REQHFORM(HGRD)
C         IERR = LM_ELEM_PRNPRNO(HSIM)
C      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on nodal properties</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>prno</b> constructs an object, with the given arguments, and
C  returns a handle on this object.
C  </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_PRNO_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_DT_PRNO_XEQCTR= ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_DT_PRNO_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PRNO_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprno.fc'  ! Pour DT_PRNO_NHVNO
      INCLUDE 'dtprno_ic.fc'

      INTEGER IOB
      INTEGER IERR
!      INTEGER NHVNO
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_PROPRIETES_NODALES'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IOB = HOBJ - DT_PRNO_HBASE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                          NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_NOEUDS#<35>#', '= ',
     &                            DT_PRNO_REQNNL(HOBJ)
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_PROP_NODALES#<35>#', '= ',
     &                            DT_PRNO_REQNPRN(HOBJ)
      CALL LOG_ECRIS(LOG_BUF)

!      NHVNO = DT_PRNO_NHVNO(IOB)
!      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_OBJ_PROP_NODALES#<35>#',
!     &                           '= ', NHVNO
!      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_DT_PRNO_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_PRNO_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PRNO_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dtprno_ic.fi'
      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprno_ic.fc'

      INTEGER      IERR
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Total number of nodes</comment>
         IF (PROP .EQ. 'nodes_total') THEN
            IVAL = DT_PRNO_REQNNL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
C        <comment>Number of nodal values</comment>
         ELSEIF (PROP .EQ. 'nodal_values') THEN
            IVAL = DT_PRNO_REQNPRN(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
C        <comment>Time</comment>
         ELSEIF (PROP .EQ. 'time') THEN
            RVAL = DT_PRNO_REQTEMPS(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'R', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>add</b> adds a list of columns from a single nodal value object.</comment>
      ELSEIF (IMTH .EQ. 'add') THEN
D        CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C        <include>IC_DT_PRNO_XEQMTH_ADD@dtprno_ic.for</include>
         IERR = IC_DT_PRNO_XEQMTH_ADD(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_PRNO_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = DT_PRNO_PRN(HOBJ)
         IERR = IC_DT_PRNO_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DT_PRNO_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_PRNO_AID()

9999  CONTINUE
      IC_DT_PRNO_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_PRNO_XEQMTH_ADD(HOBJ, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IPRM

      INCLUDE 'dtprno_ic.fi'
      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprno_ic.fc'

      INTEGER NIDXMAX
      PARAMETER (NIDXMAX = 20)

      INTEGER IERR
      INTEGER HVNO
      INTEGER I
      INTEGER KIDX(NIDXMAX)
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---        LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the nodal values</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HVNO)
      IF (IERR .NE. 0) GOTO 9901
      I = 0
100   CONTINUE
         IF (I .EQ. NIDXMAX) GOTO 9902
         I = I + 1
C        <comment>
C        List of indexes of column, comma separated. List can be empty,
C        in which case all the columns will be taken.
C        </comment>
         IERR = SP_STRN_TKI(IPRM, ',', I+1, KIDX(I))
         IF (IERR .EQ. -1) GOTO 199
         IF (IERR .NE.  0) GOTO 9901
      GOTO 100
199   CONTINUE
      I = I-1
      IF (I .EQ. 0) I = -1       ! Flag pour une liste vide
      IF (ERR_GOOD()) IERR = DT_PRNO_ADDVNO(HOBJ, HVNO, I, KIDX)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_PRNO_AID()

9999  CONTINUE
      IC_DT_PRNO_XEQMTH_ADD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PRNO_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PRNO_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprno_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>prno</b> represents the nodal properties as an agglomeration of
C  nodal values. The nodal properties are all the values defined on the
C  nodes of a mesh.
C</comment>
      IC_DT_PRNO_REQCLS= 'prno'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PRNO_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PRNO_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprno_ic.fi'
      INCLUDE 'dtprno.fi'
C-------------------------------------------------------------------------

      IC_DT_PRNO_REQHDL = DT_PRNO_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_PRNO_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_PRNO_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtprno_ic.hlp')

      RETURN
      END
