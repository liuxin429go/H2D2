C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DT_PRGL_000
C     INTEGER DT_PRGL_999
C     INTEGER DT_PRGL_PKL
C     INTEGER DT_PRGL_UPK
C     INTEGER DT_PRGL_CTR
C     INTEGER DT_PRGL_DTR
C     INTEGER DT_PRGL_INIVAL
C     INTEGER DT_PRGL_INIFIC
C     INTEGER DT_PRGL_RST
C     INTEGER DT_PRGL_REQHBASE
C     LOGICAL DT_PRGL_HVALIDE
C     INTEGER DT_PRGL_CHARGE
C     INTEGER DT_PRGL_GATHER
C     INTEGER DT_PRGL_ASGPRGL
C     INTEGER DT_PRGL_REQPRGL
C     INTEGER DT_PRGL_REQNPRGL
C     CHARACTER*256 DT_PRGL_REQNOMF
C   Private:
C     INTEGER DT_PRGL_PRN
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>DT_PRGL_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(DT_PRGL_NOBJMAX,
     &                   DT_PRGL_HBASE,
     &                   'Global Properties')

      DT_PRGL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER  IERR
      EXTERNAL DT_PRGL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(DT_PRGL_NOBJMAX,
     &                   DT_PRGL_HBASE,
     &                   DT_PRGL_DTR)

      DT_PRGL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER N, L
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_PRGL_HBASE

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WD_1(HXML, DT_PRGL_TEMPS (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_PRGL_NPRGL (IOB))

C---     La table
      IF (ERR_GOOD()) THEN
         N = DT_PRGL_NPRGL(IOB)
         L = DT_PRGL_LPRGL(IOB)
         IERR = IO_XML_WD_V(HXML, N, N, L, 1)
      ENDIF

      DT_PRGL_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet à restaurer
C     HXML     Handle sur le fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_ASR(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_PRGL_HBASE

C---     Reset l'objet
      IF (ERR_GOOD()) IERR = DT_PRGL_RST(HOBJ)

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RD_1(HXML, DT_PRGL_TEMPS(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_PRGL_NPRGL(IOB))

C---     La table
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. DT_PRGL_NPRGL(IOB))
      DT_PRGL_LPRGL(IOB) = L

      DT_PRGL_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   DT_PRGL_NOBJMAX,
     &                   DT_PRGL_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(DT_PRGL_HVALIDE(HOBJ))
         IOB = HOBJ - DT_PRGL_HBASE

         DT_PRGL_TEMPS(IOB) = -1.0D0
         DT_PRGL_NPRGL(IOB) = 0
         DT_PRGL_HFLCT(IOB) = 0
         DT_PRGL_HNUMR(IOB) = 0
         DT_PRGL_LPRGL(IOB) = 0
      ENDIF

      DT_PRGL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = DT_PRGL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   DT_PRGL_NOBJMAX,
     &                   DT_PRGL_HBASE)

      DT_PRGL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_INIVAL(HOBJ, NPRGL, VPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_INIVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPRGL
      REAL*8  VPRGL(*)

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LPRGL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     VALIDE
      IF (NPRGL .LE. 0) GOTO 9900

C---     RESET LES DONNEES
      IERR = DT_PRGL_RST(HOBJ)

C---     ALLOUE L'ESPACE
      LPRGL = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NPRGL, LPRGL)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_PRGL_HBASE
         DT_PRGL_TEMPS(IOB) = -1.0D0
         DT_PRGL_NPRGL(IOB) = NPRGL
         DT_PRGL_HFLCT(IOB) = 0
         DT_PRGL_HNUMR(IOB) = 0
         DT_PRGL_LPRGL(IOB) = LPRGL
         CALL DCOPY(NPRGL, VPRGL, 1, VA(SO_ALLC_REQVIND(VA,LPRGL)), 1)
      ENDIF
      GOTO 9999
C--------------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I3)') 'ERR_NBR_PRGL_INVALIDE',
     &                         ': ', NPRGL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_PRGL_INIVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise avec un fichier de données.
C
C Description:
C     La fonction <code>DT_PRGL_INIFIC(...)<code> initialise l'objet
C     avec un fichier source des données.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     FICVNO      Nom du fichier de Valeurs NOdales
C     ISTAT       IO_LECTURE, mais pas IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_INIFIC(HOBJ, FICVNO, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_INIFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FICVNO
      INTEGER       ISTAT

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFLCT, HNUMR
      INTEGER NPRN, NNT
      INTEGER LPRGL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôles
      IF (SP_STRN_LEN(FICVNO) .EQ. 0) GOTO 9900

C---     Reset les données
      IERR = DT_PRGL_RST(HOBJ)

C---     Construis le nouveau fichier de données
      HFLCT = 0
      IF (ERR_GOOD()) IERR = FD_PRNO_CTR(HFLCT)
      IF (ERR_GOOD()) IERR = FD_PRNO_INI(HFLCT,
     &                                   0,  ! HLIST
     &                                   FICVNO(1:SP_STRN_LEN(FICVNO)),
     &                                   ISTAT)

C---     Demande les nouvelles dimensions
      NPRN = 0
      IF (ERR_GOOD()) NPRN = FD_PRNO_REQNPRN(HFLCT)
      IF (ERR_GOOD()) NNT  = FD_PRNO_REQNNT (HFLCT)
      IF (ERR_GOOD() .AND. NNT .NE. 1) GOTO 9901

C---     Construis la renumérotation identité
      HNUMR = 0
      IF (ERR_GOOD()) IERR = NM_IDEN_CTR(HNUMR)
      IF (ERR_GOOD()) IERR = NM_IDEN_INI(HNUMR, NPRN)

C---     Alloue l'espace
      LPRGL = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NPRN, LPRGL)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - DT_PRGL_HBASE
         DT_PRGL_TEMPS(IOB) = -1.0D0
         DT_PRGL_NPRGL(IOB) = NPRN
         DT_PRGL_HFLCT(IOB) = HFLCT
         DT_PRGL_HNUMR(IOB) = HNUMR
         DT_PRGL_LPRGL(IOB) = LPRGL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_DIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_PRGL_INIFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFLCT, HNUMR
      INTEGER LPRGL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DT_PRGL_HBASE

C---     DESALLOUE LA MEMOIRE
      HFLCT = DT_PRGL_HFLCT(IOB)
      HNUMR = DT_PRGL_HNUMR(IOB)
      IF (FD_PRNO_HVALIDE(HFLCT)) IERR = FD_PRNO_DTR(HFLCT)
      IF (NM_IDEN_HVALIDE(HNUMR)) IERR = NM_IDEN_DTR(HNUMR)

C---     DESALLOUE LA MEMOIRE
      LPRGL = DT_PRGL_LPRGL(IOB)
      IF (LPRGL .NE. 0) IERR = SO_ALLC_ALLRE8(0, LPRGL)

C---     RESET LES PARAMETRES
      DT_PRGL_NPRGL(IOB) = 0
      DT_PRGL_HFLCT(IOB) = 0
      DT_PRGL_HNUMR(IOB) = 0
      DT_PRGL_LPRGL(IOB) = 0
      DT_PRGL_TEMPS(IOB) = -1.0D0

      DT_PRGL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DT_PRGL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprgl.fi'
      INCLUDE 'dtprgl.fc'
C------------------------------------------------------------------------

      DT_PRGL_REQHBASE = DT_PRGL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DT_PRGL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtprgl.fc'
C------------------------------------------------------------------------

      DT_PRGL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  DT_PRGL_NOBJMAX,
     &                                  DT_PRGL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction DT_PRGL_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprgl.fc'

      REAL*8  V
      INTEGER IERR
      INTEGER IOB
      INTEGER I
      INTEGER NPRGL, HFLCT
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - DT_PRGL_HBASE
      NPRGL = DT_PRGL_NPRGL(IOB)
      HFLCT = DT_PRGL_HFLCT(IOB)

C---     En-tête
      LOG_ZNE = 'h2d2.data.prgl'
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PROPRIETES_GLOBALES'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Impression du handle
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                          NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Nom du fichier
      IF (HFLCT .NE. 0) THEN
         NOM = DT_PRGL_REQNOMF(HOBJ)
      ELSE
         NOM = '---'
      ENDIF
      CALL SP_STRN_CLP(NOM, 50)
      WRITE (LOG_BUF,'(A,A)') 'MSG_FICHIER_DONNEES#<35>#= ',
     &                         NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Dimensions
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_PROP_GLOBALES#<35>#= ',
     &                           DT_PRGL_REQNPRGL(HOBJ)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     TEMPS
      WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TEMPS#<35>#= ',
     &                             DT_PRGL_TEMPS(IOB)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Les valeurs
      WRITE (LOG_BUF,'(A)') 'MSG_VALEURS#<35>#= '
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      DO I=1,DT_PRGL_REQNPRGL(HOBJ)
         IERR = DT_PRGL_REQPRGL(HOBJ, I, V)
         WRITE (LOG_BUF,'(A,I3,A,1PE25.17E3)') '#<6># ', I, ' : ', V
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDDO

      CALL LOG_DECIND()

      DT_PRGL_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_PRGL_CHARGE
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_CHARGE(HOBJ, TNEW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TNEW

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFLCT, HNUMR
      INTEGER LPRGL
      INTEGER NPRGL
      REAL*8  TOLD
      LOGICAL MODIF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - DT_PRGL_HBASE
      TOLD  = DT_PRGL_TEMPS(IOB)
      NPRGL = DT_PRGL_NPRGL(IOB)
      HFLCT = DT_PRGL_HFLCT(IOB)
      HNUMR = DT_PRGL_HNUMR(IOB)
      LPRGL = DT_PRGL_LPRGL(IOB)
D     CALL ERR_ASR(LPRGL .NE. 0)

C---     Lis les données
      IF (FD_PRNO_HVALIDE(HFLCT)) THEN
         IERR = FD_PRNO_LIS(HFLCT,
     &                      HNUMR,
     &                      1,      ! NNT
     &                      NPRGL,
     &                      VA(SO_ALLC_REQVIND(VA, LPRGL)),
     &                      TOLD,
     &                      TNEW,
     &                      MODIF)
      ENDIF

C---     Conserve le temps
      IF (ERR_GOOD()) THEN
         DT_PRGL_TEMPS(IOB) = TNEW
      ENDIF

      DT_PRGL_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_PRGL_GATHER
C
C Description:
C     La méthode DT_PRGL_GATHER rassemble les données des DT_PRGL de l'objet
C     dans la table VPRGL.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     NPRGL       Dimension de la table VPRGL
C
C Sortie:
C     VPRGL       Table mise à jour
C     TNEW        Temps
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_GATHER(HOBJ, NPRGL, VPRGL, TEMPS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_GATHER
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPRGL
      REAL*8  VPRGL(NPRGL)
      REAL*8  TEMPS

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IOB
      INTEGER LPRGL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPRGL .GE. DT_PRGL_REQNPRGL(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - DT_PRGL_HBASE
      LPRGL = DT_PRGL_LPRGL(IOB)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LPRGL))

C---     Copie les données
      CALL DCOPY(DT_PRGL_REQNPRGL(HOBJ),
     &           VA(SO_ALLC_REQVIND(VA,LPRGL)), 1,
     &           VPRGL, 1)
      TEMPS = DT_PRGL_TEMPS(IOB)

      DT_PRGL_GATHER = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_ASGPRGL(HOBJ, I, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_ASGPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I
      REAL*8  V

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LPRGL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTRÔLES
      IF (I .GT. DT_PRGL_REQNPRGL(HOBJ)) GOTO 9900

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DT_PRGL_HBASE

C---     RECUPERE LES ATTRIBUTS
      LPRGL = DT_PRGL_LPRGL(IOB)

C---     ASSIGNE LA VALEUR
      IERR = SO_ALLC_ASGRE8(LPRGL, I, V)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I3,A,I3)') 'ERR_IND_PROP_GLOBALES_INVALIDE',
     &                         ': ', I, ' / ', DT_PRGL_REQNPRGL(HOBJ)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_PRGL_ASGPRGL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_REQPRGL(HOBJ, I, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_REQPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I
      REAL*8  V

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER IOB
      INTEGER LPRGL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTRÔLES
      IF (I .GT. DT_PRGL_REQNPRGL(HOBJ)) GOTO 9900

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DT_PRGL_HBASE

C---     RECUPERE LES ATTRIBUTS
      LPRGL = DT_PRGL_LPRGL(IOB)

C---     LA VALEUR
      V = SO_ALLC_REQRE8(LPRGL, I)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I3,A,I3)') 'ERR_IND_PROP_GLOBALES_INVALIDE',
     &                         ': ', I, ' / ', DT_PRGL_REQNPRGL(HOBJ)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_PRGL_REQPRGL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_REQNPRGL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_REQNPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprgl.fi'
      INCLUDE 'dtprgl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_PRGL_REQNPRGL = DT_PRGL_NPRGL(HOBJ-DT_PRGL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le NOM du fichier de lecture.
C
C Description:
C     La fonction DT_PRGL_REQNOMF retourne le nom de fichier de lecture
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRGL_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRGL_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprgl.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'dtprgl.fc'

      INTEGER HFLCT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFLCT = DT_PRGL_HFLCT(HOBJ-DT_PRGL_HBASE)
D     CALL ERR_ASR(FD_PRNO_HVALIDE(HFLCT))

      DT_PRGL_REQNOMF = FD_PRNO_REQNOMF(HFLCT, 1)
      RETURN
      END
