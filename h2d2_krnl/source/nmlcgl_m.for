C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   LoCal-GLobal
C Type:
C
C Class: NM_LCGL
C     INTEGER NM_LCGL_000
C     INTEGER NM_LCGL_999
C     INTEGER NM_LCGL_CTR
C     INTEGER NM_LCGL_DTR
C     INTEGER NM_LCGL_INI
C     INTEGER NM_LCGL_RST
C     INTEGER NM_LCGL_REQHBASE
C     INTEGER NM_LCGL_REQHBASE
C     LOGICAL NM_LCGL_HVALIDE
C     INTEGER NM_LCGL_CHARGE
C     INTEGER NM_LCGL_LCLGLB
C     INTEGER NM_LCGL_PRCECR
C     INTEGER NM_LCGL_DIMTBL
C     INTEGER NM_LCGL_TBLECR
C     INTEGER NM_LCGL_PRNDST
C     LOGICAL NM_LCGL_ESTNOPP
C     INTEGER NM_LCGL_REQNPRT
C     INTEGER NM_LCGL_REQNNL
C     INTEGER NM_LCGL_REQNNP
C     INTEGER NM_LCGL_REQNNT
C     INTEGER NM_LCGL_REQNEXT
C     INTEGER NM_LCGL_REQNINT
C     INTEGER NM_LCGL_REQGLB
C     INTEGER NM_LCGL_REQLCL
C     INTEGER NM_LCGL_DSYNC
C     INTEGER NM_LCGL_ISYNC
C     INTEGER NM_LCGL_GENDSYNC
C     INTEGER NM_LCGL_GENISYNC
C     INTEGER NM_LCGL_GENSYNC
C
C************************************************************************

      MODULE NM_LCGL_M

      USE NM_NUMR_M, ONLY: NM_NUMR_T
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1_T, SO_ALLC_KPTR2_T
      IMPLICIT NONE
      PUBLIC

C========================================================================
C========================================================================
C---     Type de donnée associé à la classe
      TYPE, EXTENDS (NM_NUMR_T) :: NM_LCGL_T
         PRIVATE
         TYPE(SO_ALLC_KPTR2_T) :: KDIST
         TYPE(SO_ALLC_KPTR1_T) :: NLOCGLB_P
         TYPE(SO_ALLC_KPTR2_T) :: NGLBLOC_P
         TYPE(SO_ALLC_KPTR1_T) :: PRECR_P
         TYPE(SO_ALLC_KPTR1_T) :: ECRD_P
         TYPE(SO_ALLC_KPTR1_T) :: ECR_P
         TYPE(SO_ALLC_KPTR1_T) :: LISD_P
         TYPE(SO_ALLC_KPTR1_T) :: LIS_P
         INTEGER :: NPROC
         INTEGER :: NECR
         INTEGER :: NLIS
         INTEGER :: NNT
         INTEGER :: NNL
         INTEGER :: NNP
      CONTAINS
         ! ---  Méthodes de la classe
         !!!PROCEDURE, PUBLIC :: NM_LCGL_CTR
         PROCEDURE, PUBLIC :: DTR      => NM_LCGL_DTR
         PROCEDURE, PUBLIC :: INI      => NM_LCGL_INI
         PROCEDURE, PUBLIC :: RST      => NM_LCGL_RST

         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: CHARGE   => NM_LCGL_CHARGE
         PROCEDURE, PUBLIC :: DSYNC    => NM_LCGL_DSYNC
         PROCEDURE, PUBLIC :: ISYNC    => NM_LCGL_ISYNC
         PROCEDURE, PUBLIC :: GENDSYNC => NM_LCGL_GENDSYNC
         PROCEDURE, PUBLIC :: GENISYNC => NM_LCGL_GENISYNC

         PROCEDURE, PUBLIC :: REQNPRT  => NM_LCGL_REQNPRT
         PROCEDURE, PUBLIC :: REQNNL   => NM_LCGL_REQNNL
         PROCEDURE, PUBLIC :: REQNNP   => NM_LCGL_REQNNP
         PROCEDURE, PUBLIC :: REQNNT   => NM_LCGL_REQNNT

         PROCEDURE, PUBLIC :: ESTNOPP  => NM_LCGL_ESTNOPP
         PROCEDURE, PUBLIC :: REQNEXT  => NM_LCGL_REQNEXT
         PROCEDURE, PUBLIC :: REQNINT  => NM_LCGL_REQNINT

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: NM_LCGL_RAZ
         PROCEDURE, PRIVATE :: NM_LCGL_GENSYNC
      END TYPE NM_LCGL_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: NM_LCGL_CTR_SELF
      INTERFACE DEL
         PROCEDURE NM_LCGL_DTR
      END INTERFACE DEL
      
C========================================================================
C========================================================================
      CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! Note: 2018-01-02
!!!
!!! Les SUBMODULE(s) ne sont supportés que par Intel >= 16.4 et GCC >= 6.0
!!! Pour rester compatible avec les versions Intel antérieures (CMC) et
!!! pour pouvoir compiler Sun, le choix est fait de ne pas utiliser de
!!! SUBMODULE. Pour simuler/conserver la séparation déclaration/définition,
!!! le SUBMODULE est transformé en fichier inclus dans le section
!!! CONTAINS du module.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      INCLUDE 'nmlcgl.for'

      END MODULE NM_LCGL_M
