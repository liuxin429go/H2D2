C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Note:
C     Le fichier peut s'employer pour toute donnée réelle, stockée par
C     bloc pour chaque pas de temps, avec renumérotation; donc autant
C     pour des valeurs nodales que pour des valeurs élémentaires.
C     <p>
C     nlines ncols time
C     v_1 v_2 v_3 ... v_ncols
C
C Functions:
C   Public:
C     INTEGER FD_PRNO_000
C     INTEGER FD_PRNO_999
C     INTEGER FD_PRNO_PKL
C     INTEGER FD_PRNO_UPK
C     INTEGER FD_PRNO_CTR
C     INTEGER FD_PRNO_DTR
C     INTEGER FD_PRNO_INI
C     INTEGER FD_PRNO_RST
C     INTEGER FD_PRNO_REQHBASE
C     LOGICAL FD_PRNO_HVALIDE
C     INTEGER FD_PRNO_ECRIS
C     INTEGER FD_PRNO_LIS
C     INTEGER FD_PRNO_REQISTAT
C     INTEGER FD_PRNO_REQNNT
C     INTEGER FD_PRNO_REQNPRN
C     INTEGER FD_PRNO_REQNSCT
C     REAL*8 FD_PRNO_REQTMIN
C     REAL*8 FD_PRNO_REQTMAX
C     INTEGER FD_PRNO_REQNFIC
C     CHARACTER*256 FD_PRNO_REQNOMF
C   Private:
C     INTEGER FD_PRNO_RAZ
C     INTEGER FD_PRNO_ECRSCT
C     INTEGER FD_PRNO_LIS_E
C     INTEGER FD_PRNO_LISSCT
C     INTEGER FD_PRNO_ASGSCT
C     INTEGER FD_PRNO_REQSCT
C     INTEGER FD_PRNO_TRISCT
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction FD_PRNO_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(FD_PRNO_NOBJMAX,
     &                   FD_PRNO_HBASE,
     &                   'Nodal Properties File')

      FD_PRNO_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction FD_PRNO_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fc'

      INTEGER  IERR
      EXTERNAL FD_PRNO_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(FD_PRNO_NOBJMAX,
     &                   FD_PRNO_HBASE,
     &                   FD_PRNO_DTR)

      FD_PRNO_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_PRNO_HBASE

C---     Pickle
      IERR = IO_XML_WI_1(HXML, FD_PRNO_ISTAT (IOB))
      IERR = IO_XML_WH_A(HXML, FD_PRNO_HLIST (IOB))
      IERR = IO_XML_WI_1(HXML, FD_PRNO_NNT   (IOB))
      IERR = IO_XML_WI_1(HXML, FD_PRNO_NPRN  (IOB))
      IERR = IO_XML_WI_1(HXML, FD_PRNO_NSCT  (IOB))
      IERR = IO_XML_WI_1(HXML, FD_PRNO_NSMAX (IOB))

C---     La table des temps
      IF (ERR_GOOD()) THEN
         N = FD_PRNO_NSCT (IOB)
         NX= FD_PRNO_NSMAX(IOB)
         L = FD_PRNO_LTEMP(IOB)
         IERR = IO_XML_WD_V(HXML, NX, N, L, 1)
      ENDIF

C---     La table des positions
      IF (ERR_GOOD()) THEN
         N = FD_PRNO_NSCT (IOB)
         NX= FD_PRNO_NSMAX(IOB)
         L = FD_PRNO_LFPOS(IOB)
         IERR = IO_XML_WX_V(HXML, NX, N, L, 1)
      ENDIF

      FD_PRNO_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - FD_PRNO_HBASE

C---     Efface les attributs
      IERR = FD_PRNO_RAZ(HOBJ)

C---     Un-pickle
      IERR = IO_XML_RI_1(HXML, FD_PRNO_ISTAT (IOB))
      IERR = IO_XML_RH_A(HXML, FD_PRNO_HLIST (IOB))
      IERR = IO_XML_RI_1(HXML, FD_PRNO_NNT   (IOB))
      IERR = IO_XML_RI_1(HXML, FD_PRNO_NPRN  (IOB))
      IERR = IO_XML_RI_1(HXML, FD_PRNO_NSCT  (IOB))
      IERR = IO_XML_RI_1(HXML, FD_PRNO_NSMAX (IOB))

C---     La table des temps
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. FD_PRNO_NSMAX(IOB))
D     CALL ERR_ASR(ERR_BAD() .OR. N  .EQ. FD_PRNO_NSCT (IOB))
      FD_PRNO_LTEMP(IOB) = L

C---     La table des positions
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RX_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. FD_PRNO_NSMAX(IOB))
D     CALL ERR_ASR(ERR_BAD() .OR. N  .EQ. FD_PRNO_NSCT (IOB))
      FD_PRNO_LFPOS(IOB) = L

      FD_PRNO_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction FD_PRNO_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   FD_PRNO_NOBJMAX,
     &                                   FD_PRNO_HBASE)
      IF (ERR_GOOD()) IERR = FD_PRNO_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. FD_PRNO_HVALIDE(HOBJ))

      FD_PRNO_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction FD_PRNO_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = FD_PRNO_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   FD_PRNO_NOBJMAX,
     &                   FD_PRNO_HBASE)

      FD_PRNO_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode FD_PRNO_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - FD_PRNO_HBASE

      FD_PRNO_ISTAT (IOB) = IO_MODE_INDEFINI
      FD_PRNO_HLIST (IOB) = 0
      FD_PRNO_HDRV  (IOB) = 0
      FD_PRNO_NNT   (IOB) = 0
      FD_PRNO_NPRN  (IOB) = 0
      FD_PRNO_NSCT  (IOB) = 0
      FD_PRNO_NSMAX (IOB) = 0
      FD_PRNO_LIFIC (IOB) = 0
      FD_PRNO_LTEMP (IOB) = 0
      FD_PRNO_LFPOS (IOB) = 0

      FD_PRNO_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction FD_PRNO_INI initialise l'objet à l'aide des
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HLST        Handle sur une liste de fichiers
C     NOMFIC      Nom du fichier de propriétés nodales
C     ISTAT       IO_LECTURE, IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_INI(HOBJ, HFLST, NOMFIC, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HFLST
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT

      INCLUDE 'fdprno.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprna.fi'
      INCLUDE 'fdprn8.fi'
      INCLUDE 'fdprnb.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I, K, L
      INTEGER HDRV, HLIST
      CHARACTER*256 BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno')

C---     Contrôle les paramètres
      IF (.NOT. DS_LIST_HVALIDE(HFLST)) THEN
         IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9900
      ELSE
         IF (DS_LIST_REQDIM(HFLST) .LE. 0) GOTO 9901
      ENDIF
      IF (.NOT. IO_UTIL_MODVALIDE(ISTAT)) GOTO 9902

C---     Reset les données
      IERR = FD_PRNO_RST(HOBJ)

C---     Construis la liste des fichiers
      HLIST = 0
      IF (ERR_GOOD()) IERR = DS_LIST_CTR(HLIST)
      IF (ERR_GOOD()) IERR = DS_LIST_INI(HLIST)
      IF (ERR_GOOD()) THEN
         IF (DS_LIST_HVALIDE(HFLST)) THEN
            DO I=1, DS_LIST_REQDIM(HFLST)
               IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HFLST, I, BUF)
               L = SP_STRN_LEN(BUF)
               IF (ERR_GOOD()) IERR = DS_LIST_TRVVAL(HLIST,K,BUF(1:L))
               IF (ERR_GOOD()) THEN      ! Fichier existe déjà
                  GOTO 9903
               ELSEIF (ERR_ESTMSG('ERR_LISTE_VALEUR_ABSENTE')) THEN
                  CALL ERR_RESET()
               ENDIF
               IF (ERR_GOOD()) IERR = DS_LIST_AJTVAL(HLIST, BUF(1:L))
            ENDDO
         ELSE
            IERR = DS_LIST_AJTVAL(HLIST, NOMFIC(1:SP_STRN_LEN(NOMFIC)))
         ENDIF
      ENDIF

C---     Construis et initialise le vrai fichier
      HDRV = 0
      IF     (IO_UTIL_MODACTIF(ISTAT, IO_MODE_BINAIRE)) THEN
         IF (ERR_GOOD()) IERR = FD_PRNB_CTR(HDRV)
         IF (ERR_GOOD()) IERR = FD_PRNB_INI(HDRV, HOBJ, HLIST, ISTAT)
      ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ASCII)) THEN
         IF (ERR_GOOD()) IERR = FD_PRNA_CTR(HDRV)
         IF (ERR_GOOD()) IERR = FD_PRNA_INI(HDRV, HOBJ, HLIST, ISTAT)
      ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_PREC_08)) THEN
         IF (ERR_GOOD()) IERR = FD_PRN8_CTR(HDRV)
         IF (ERR_GOOD()) IERR = FD_PRN8_INI(HDRV, HOBJ, HLIST, ISTAT)
      ELSE
         GOTO 9902
      ENDIF

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - FD_PRNO_HBASE
         FD_PRNO_ISTAT(IOB) = ISTAT
         FD_PRNO_HLIST(IOB) = HLIST    ! Liste des fichiers
         FD_PRNO_HDRV (IOB) = HDRV     ! Héritier
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_LISTE_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A, I6)') 'ERR_MODE_ACCES_INVALIDE',': ', ISTAT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  CALL SP_STRN_CLP(BUF, 80)
      L = SP_STRN_LEN(BUF)
      WRITE(ERR_BUF,'(3A)') 'ERR_NOM_FICHIER_EXISTANT',': ', BUF(1:L)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.prno')
      FD_PRNO_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction FD_PRNO_RST reset l'objet, donc le remet dans
C     un état valide non initialisé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'fdprna.fi'
      INCLUDE 'fdprn8.fi'
      INCLUDE 'fdprnb.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ISTAT
      INTEGER HLIST, HDRV
      INTEGER LIFIC, LTEMP, LFPOS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_PRNO_HBASE

C---     RECUPERE LES ATTRIBUTS
      ISTAT = FD_PRNO_ISTAT(IOB)
      HLIST = FD_PRNO_HLIST(IOB)
      HDRV  = FD_PRNO_HDRV (IOB)
      LIFIC = FD_PRNO_LIFIC(IOB)
      LTEMP = FD_PRNO_LTEMP(IOB)
      LFPOS = FD_PRNO_LFPOS(IOB)

C---     Détruis la liste de fichier
      IF (DS_LIST_HVALIDE(HLIST)) IERR = DS_LIST_DTR(HLIST)

C---     Détruis le vrai fichier
      IF     (IO_UTIL_MODACTIF(ISTAT, IO_MODE_BINAIRE)) THEN
         IF (FD_PRNB_HVALIDE(HDRV)) IERR = FD_PRNB_DTR(HDRV)
      ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ASCII)) THEN
         IF (FD_PRNA_HVALIDE(HDRV)) IERR = FD_PRNA_DTR(HDRV)
      ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_PREC_08)) THEN
         IF (FD_PRN8_HVALIDE(HDRV)) IERR = FD_PRN8_DTR(HDRV)
      ENDIF

C---     DESALLOUE LA MEMOIRE
      IF (LIFIC .NE. 0) IERR = SO_ALLC_ALLINT(0, LIFIC)
      IF (LTEMP .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTEMP)
      IF (LFPOS .NE. 0) IERR = SO_ALLC_ALLIN8(0, LFPOS)

C---     Efface les attributs
      IERR = FD_PRNO_RAZ(HOBJ)

      FD_PRNO_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction FD_PRNO_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprno.fc'
C------------------------------------------------------------------------

      FD_PRNO_REQHBASE = FD_PRNO_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction FD_PRNO_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdprno.fc'
C------------------------------------------------------------------------

      FD_PRNO_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  FD_PRNO_NOBJMAX,
     &                                  FD_PRNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNO_ECRIS
C
C Description:
C     La fonction FD_PRNO_ECRIS est la fonction principale d'écriture
C     d'un fichier de type ProprietesNodales.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     NNT         Nombre de noeuds total
C     NNL         Nombre de noeuds local
C     NPRN        Nombre de propriétés nodales
C     VPRNO       Vecteur des propriétés nodales
C     TSIM        Temps des valeurs
C
C Sortie:
C
C Notes:
C     Il y a une fragilité avec la numérotation. On prend pour acquis
C     que la numérotation HNUMR est la même si les dimensions NNT, NPRN
C     ne sont pas modifiées
C************************************************************************
      FUNCTION FD_PRNO_ECRIS(HOBJ,
     &                       HNUMR,
     &                       NNT,
     &                       NNL,
     &                       NPRN,
     &                       VPRNO,
     &                       TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_ECRIS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NNT
      INTEGER NNL
      INTEGER NPRN
      REAL*8  VPRNO(NPRN, NNL)
      REAL*8  TSIM

      INCLUDE 'fdprno.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IFIC, ISTAT
D     INTEGER NNT_L, NPRN_L

      PARAMETER (IFIC = 1)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NNT  .GE. NNL)
D     CALL ERR_PRE(NNL  .GE. 0)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(TSIM .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno.write')

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNO_HBASE
      ISTAT  = FD_PRNO_ISTAT(IOB)
D     NNT_L  = FD_PRNO_NNT  (IOB)
D     NPRN_L = FD_PRNO_NPRN (IOB)
D     CALL ERR_ASR(NNT_L  .EQ. 0 .OR. NNT_L  .EQ. NNT)
D     CALL ERR_ASR(NPRN_L .EQ. 0 .OR. NPRN_L .EQ. NPRN)
D     CALL ERR_ASR(IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE) .OR.
D    &             IO_UTIL_MODACTIF(ISTAT, IO_MODE_AJOUT))

C---     Écris la section
      IERR = FD_PRNO_ECRSCT(HOBJ,
     &                      IFIC, != 1
     &                      HNUMR,
     &                      NNT,
     &                      NNL,
     &                      NPRN,
     &                      VPRNO,
     &                      TSIM)

C---     Passe en mode AJOUT
      IF (ERR_GOOD()) THEN
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) THEN
            FD_PRNO_ISTAT(IOB) = FD_PRNO_ISTAT(IOB)
     &                         - IO_MODE_ECRITURE
     &                         + IO_MODE_AJOUT
         ENDIF
      ENDIF

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         FD_PRNO_NNT  (IOB) = NNT
         FD_PRNO_NPRN (IOB) = NPRN
      ENDIF

      CALL TR_CHRN_STOP('h2d2.io.prno.write')
      FD_PRNO_ECRIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNO_ECRSCT
C
C Description:
C     La fonction virtuelle FD_PRNO_ECRSCT est la fonction principale d'écriture
C     d'un fichier de type ProprietesNodales.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     NNTL        Nombre de noeuds local
C     NPRN        Nombre de propriétés nodales
C     VPRNO       Vecteur des propriétés nodales
C     TSIM        Temps des valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_ECRSCT(HOBJ,
     &                        IFIC,
     &                        HNUMR,
     &                        NNT,
     &                        NNL,
     &                        NPRN,
     &                        VPRNO,
     &                        TSIM)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IFIC
      INTEGER HNUMR
      INTEGER NNT
      INTEGER NNL
      INTEGER NPRN
      REAL*8  VPRNO(NPRN, NNL)
      REAL*8  TSIM

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprna.fi'
      INCLUDE 'fdprn8.fi'
      INCLUDE 'fdprnb.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ISTAT
      INTEGER HDRV
      CHARACTER*(256) FNAME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNO_HBASE
      HDRV  = FD_PRNO_HDRV(IOB)
      ISTAT = FD_PRNO_ISTAT(IOB)
      FNAME = FD_PRNO_REQNOMF(HOBJ, IFIC)

C---     Dispatch
      IF (FD_PRNA_HVALIDE(HDRV)) THEN
         IERR = FD_PRNA_ECRSCT(HDRV,
     &                         FNAME,
     &                         ISTAT,
     &                         HNUMR,
     &                         NNT,
     &                         NNL,
     &                         NPRN,
     &                         VPRNO,
     &                         TSIM)
      ELSEIF (FD_PRN8_HVALIDE(HDRV)) THEN
         IERR = FD_PRN8_ECRSCT(HDRV,
     &                         FNAME,
     &                         ISTAT,
     &                         HNUMR,
     &                         NNT,
     &                         NNL,
     &                         NPRN,
     &                         VPRNO,
     &                         TSIM)
      ELSEIF (FD_PRNB_HVALIDE(HDRV)) THEN
         IERR = FD_PRNB_ECRSCT(HDRV,
     &                         FNAME,
     &                         ISTAT,
     &                         HNUMR,
     &                         NNT,
     &                         NNL,
     &                         NPRN,
     &                         VPRNO,
     &                         TSIM)
D     ELSE
D        CALL ERR_ASR(.FALSE.)
      ENDIF

      FD_PRNO_ECRSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNO_LIS
C
C Description:
C     La fonction virtuelle FD_PRNO_LIS est la fonction principale de lecture
C     d'un fichier de type ProprietesNodales.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     NNTL        Nombre de Noeuds Total Local au process
C     NPRN        Nombre de Propriétés Nodales
C     TOLD        Temps actuel des valeurs
C     TNEW        Temps demandé des nouvelles valeurs
C
C Sortie:
C     VPRNO       Vecteur des propriétés nodales
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_LIS(HOBJ,
     &                     HNUMR,
     &                     NNTL,
     &                     NPRN,
     &                     VPRNO,
     &                     TOLD,
     &                     TNEW,
     &                     MODIF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_LIS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NNTL
      INTEGER NPRN
      REAL*8  VPRNO(NPRN, NNTL)
      REAL*8  TOLD
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NSCT
      INTEGER LIFIC, LTEMP, LFPOS
D     INTEGER ISTAT, NNT_L, NPRN_L
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NNTL .GT. 0)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(TNEW .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno.read')

C---     Récupère les attributs
      IOB  = HOBJ - FD_PRNO_HBASE
      NSCT  = FD_PRNO_NSCT (IOB)
      LIFIC = FD_PRNO_LIFIC(IOB)
      LTEMP = FD_PRNO_LTEMP(IOB)
      LFPOS = FD_PRNO_LFPOS(IOB)
D     ISTAT = FD_PRNO_ISTAT(IOB)
D     NNT_L = FD_PRNO_NNT  (IOB)
D     NPRN_L= FD_PRNO_NPRN (IOB)
D     CALL ERR_ASR(IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE))
D     CALL ERR_ASR(NNT_L  .GT. 0 .AND. NNT_L  .GE. NNTL)
D     CALL ERR_ASR(NPRN_L .GT. 0 .AND. NPRN_L .EQ. NPRN)

C---     Lis la section
      IERR = FD_PRNO_LIS_E(HOBJ,
     &                     NNTL,
     &                     NPRN,
     &                     VPRNO,
     &                     TOLD,
     &                     TNEW,
     &                     HNUMR,
     &                     NSCT,
     &                     KA(SO_ALLC_REQKIND(KA,LIFIC)),
     &                     VA(SO_ALLC_REQVIND(VA,LTEMP)),
     &                     XA(SO_ALLC_REQXIND(XA,LFPOS)),
     &                     MODIF)

      CALL TR_CHRN_STOP('h2d2.io.prno.read')
      FD_PRNO_LIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNO_LIS_E
C
C Description:
C     La méthode privée FD_PRNO_LIS_E est la fonction principale de lecture
C     d'un fichier de type ProprietesNodales. Avec les temps elle détermine
C     les sections à lire et interpole au besoin.
C     C'est le process maître qui détermine les sections à lire pour éviter
C     des problèmes de précision numérique sur différents process.
C     <p>
C     La fonction permet entre autre de résoudre les tables du parent.
C
C Paramètres:
C     HOBJ        Handle sur l'objet
C     NNTL        Nombre de Noeuds Total Local au process
C     NPRN        Nombre de propriétés
C     VPRNO       Tables de propriétés lues
C     TOLD        Temps actuel
C     TNEW        Temps demandé
C     NOMFIC      Nom du fichier
C     HNUMR       Handle sur la renumérotation
C     NSCT        Nombre de section du fichier
C     KIFIC       Table des indices de fichier
C     VTEMPS      Table des temps du fichier
C     KPOS        Table des débuts de section
C     MODIF       .TRUE. s'il y a modification de VPRNO
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_LIS_E(HOBJ,
     &                       NNTL,
     &                       NPRN,
     &                       VPRNO,
     &                       TOLD,
     &                       TNEW,
     &                       HNUMR,
     &                       NSCT,
     &                       KIFIC,
     &                       VTEMPS,
     &                       KFPOS,
     &                       MODIF)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   NNTL
      INTEGER   NPRN
      REAL*8    VPRNO(NPRN, NNTL)
      REAL*8    TOLD
      REAL*8    TNEW
      INTEGER   HNUMR
      INTEGER   NSCT
      INTEGER   KIFIC (NSCT)
      REAL*8    VTEMPS(NSCT)
      INTEGER*8 KFPOS (NSCT)
      LOGICAL   MODIF

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprno.fc'

      INTEGER I_COMM, I_RANK, I_ERROR
      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      REAL*8  TRAP
      INTEGER IERR
      INTEGER IS, ISCT(2)
      INTEGER LPRNO0
      LOGICAL SAUT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
D     CALL ERR_PRE(NNTL .GT. 0)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(TNEW .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LPRNO0 = 0
      MODIF = .FALSE.

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)


      IF (I_RANK .NE. I_MASTER) GOTO 200

C---     CONTROLES PRIMAIRES
      SAUT = .TRUE.
      IF (TOLD .GE. 0) THEN
         IF (TNEW.EQ.TOLD) GOTO 119
         IF (TOLD.GE.VTEMPS(NSCT) .AND. TNEW.GE.VTEMPS(NSCT)) GOTO 119
         IF (TOLD.LE.VTEMPS(   1) .AND. TNEW.LE.VTEMPS(   1)) GOTO 119
      ENDIF
      SAUT = .FALSE.
119   CONTINUE

C---     RECHERCHE LES SECTIONS
      IS = 1
      IF (.NOT. SAUT) THEN
         DO IS=1,NSCT
            IF (VTEMPS(IS) .GT. TNEW) GOTO 120
         ENDDO
120      CONTINUE
      ENDIF
      ISCT(2) = IS
      ISCT(1) = ISCT(2) - 1

C---     Broadcast pour que tous aient les mêmes valeurs
200   CONTINUE
      CALL MPI_BCAST(SAUT, 1,MPI_LOGICAL,  I_MASTER,I_COMM,I_ERROR)
      IF (SAUT) GOTO 9999
      CALL MPI_BCAST(ISCT, 2,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)

C---     TNEW < TMIN
      IF (ERR_GOOD() .AND. ISCT(2) .EQ. 1) THEN
         IERR = FD_PRNO_LISSCT(HOBJ,
     &                         KIFIC (ISCT(2)),
     &                         KFPOS (ISCT(2)),
     &                         VTEMPS(ISCT(2)),
     &                         HNUMR,
     &                         NNTL,
     &                         NPRN,
     &                         VPRNO)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TNEW > TMAX
      IF (ERR_GOOD() .AND. ISCT(1) .EQ. NSCT) THEN
         IERR = FD_PRNO_LISSCT(HOBJ,
     &                         KIFIC (ISCT(1)),
     &                         KFPOS (ISCT(1)),
     &                         VTEMPS(ISCT(1)),
     &                         HNUMR,
     &                         NNTL,
     &                         NPRN,
     &                         VPRNO)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TNEW = TINF
      IF (ERR_GOOD() .AND. TNEW .EQ. VTEMPS(ISCT(1))) THEN
         IERR = FD_PRNO_LISSCT(HOBJ,
     &                         KIFIC (ISCT(1)),
     &                         KFPOS (ISCT(1)),
     &                         VTEMPS(ISCT(1)),
     &                         HNUMR,
     &                         NNTL,
     &                         NPRN,
     &                         VPRNO)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TNEW = TSUP
      IF (ERR_GOOD() .AND. TNEW .EQ. VTEMPS(ISCT(2))) THEN
         IERR = FD_PRNO_LISSCT(HOBJ,
     &                         KIFIC (ISCT(2)),
     &                         KFPOS (ISCT(2)),
     &                         VTEMPS(ISCT(2)),
     &                         HNUMR,
     &                         NNTL,
     &                         NPRN,
     &                         VPRNO)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TINF > TNEW > TSUP

C---     ALLOUE LA MEMOIRE
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NNTL*NPRN, LPRNO0)

C---     LIS LA VALEUR INFERIEUR
      IF (ERR_GOOD()) THEN
         IERR = FD_PRNO_LISSCT(HOBJ,
     &                         KIFIC (ISCT(1)),
     &                         KFPOS (ISCT(1)),
     &                         VTEMPS(ISCT(1)),
     &                         HNUMR,
     &                         NNTL,
     &                         NPRN,
     &                         VPRNO)
      ENDIF

C---     LIS LA VALEUR SUPERIEUR
      IF (ERR_GOOD()) THEN
         IERR = FD_PRNO_LISSCT(HOBJ,
     &                         KIFIC (ISCT(2)),
     &                         KFPOS (ISCT(2)),
     &                         VTEMPS(ISCT(2)),
     &                         HNUMR,
     &                         NNTL,
     &                         NPRN,
     &                         VA(SO_ALLC_REQVIND(VA,LPRNO0)))
      ENDIF

C---     INTERPOLE
      IF (ERR_GOOD()) THEN
         CALL DAXPY(NNTL*NPRN,
     &              -1.0D0,
     &              VPRNO,
     &              1,
     &              VA(SO_ALLC_REQVIND(VA,LPRNO0)),
     &              1)
         TRAP = (           TNEW - VTEMPS(ISCT(1))) /
     &          (VTEMPS(ISCT(2)) - VTEMPS(ISCT(1)))
         CALL DAXPY(NNTL*NPRN,
     &              TRAP,
     &              VA(SO_ALLC_REQVIND(VA,LPRNO0)),
     &              1,
     &              VPRNO,
     &              1)
         MODIF = .TRUE.
      ENDIF

8000  CONTINUE


C---     Nettoie
      CALL ERR_PUSH()
      IF (LPRNO0 .NE. 0) IERR = SO_ALLC_ALLRE8(0, LPRNO0)
      CALL ERR_POP()

9999  CONTINUE
      FD_PRNO_LIS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNO_LISSCT
C
C Description:
C     La fonction privée FD_PRNO_LISSCT lis une section d'un fichier
C     de type ProprietesNodales.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IFIC        Indice du fichier
C     XPOS        Position dans le fichier (Data TOP)
C     TEMPS       Temps
C     HNUMR       Handle sur la renumérotation
C     NNTL        Nombre de Noeuds Total Local au process
C     NPRN        Nombre de propriétés
C
C Sortie:
C     VPRNO       Table des valeurs lues
C
C Notes:
C     La signature des héritiers diffère. On leur passe directement
C     le nom de fichier plutôt que son indice.
C************************************************************************
      FUNCTION FD_PRNO_LISSCT(HOBJ,
     &                        IFIC,
     &                        XPOS,
     &                        TEMPS,
     &                        HNUMR,
     &                        NNTL,
     &                        NPRN,
     &                        VPRNO)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   IFIC      ! Indice du fichier
      INTEGER*8 XPOS      ! Offset dans le fichier
      REAL*8    TEMPS     ! Pour contrôles
      INTEGER   HNUMR
      INTEGER   NNTL
      INTEGER   NPRN
      REAL*8    VPRNO(NPRN, NNTL)

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprna.fi'
      INCLUDE 'fdprnb.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HDRV
      CHARACTER*(256) FNAME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNO_HBASE
      HDRV  = FD_PRNO_HDRV(IOB)
      FNAME = FD_PRNO_REQNOMF(HOBJ, IFIC)

C---     Dispatch
      IF (FD_PRNA_HVALIDE(HDRV)) THEN
         IERR = FD_PRNA_LISSCT(HDRV,
     &                         FNAME(1:SP_STRN_LEN(FNAME)),
     &                         XPOS,
     &                         TEMPS,
     &                         HNUMR,
     &                         NNTL,
     &                         NPRN,
     &                         VPRNO)
      ELSEIF (FD_PRNB_HVALIDE(HDRV)) THEN
         IERR = FD_PRNB_LISSCT(HDRV,
     &                         FNAME(1:SP_STRN_LEN(FNAME)),
     &                         XPOS,
     &                         TEMPS,
     &                         HNUMR,
     &                         NNTL,
     &                         NPRN,
     &                         VPRNO)
D     ELSE
D        CALL ERR_ASR(.FALSE.)
      ENDIF

      FD_PRNO_LISSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne les paramètres de section
C
C Description:
C     La méthode protégée FD_PRNO_ASGSCT ajoute à l'objet les données sur
C     une section.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IFIC        Indice du fichier dans la liste
C     TEMPS       Temps associé à la section
C     XPOS        Position du début de la section dans le fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_ASGSCT(HOBJ, IFIC, TEMPS, XPOS)

      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   IFIC
      REAL*8    TEMPS
      INTEGER*8 XPOS

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NSCT
      INTEGER NSMAX
      INTEGER LIFIC, LTEMP, LFPOS
      TYPE(SO_ALLC_PTR_T) :: PIFIC
      TYPE(SO_ALLC_PTR_T) :: PTMPS
      TYPE(SO_ALLC_PTR_T) :: PPOS
      INTEGER,   POINTER :: KIFIC(:)
      REAL*8,    POINTER :: VTMPS(:)
      INTEGER*8, POINTER :: KPOS (:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - FD_PRNO_HBASE
      NSMAX = FD_PRNO_NSMAX(IOB)
      NSCT  = FD_PRNO_NSCT (IOB)
      LIFIC = FD_PRNO_LIFIC(IOB)
      LTEMP = FD_PRNO_LTEMP(IOB)
      LFPOS = FD_PRNO_LFPOS(IOB)

C---     (Re)-dimensionne les tables
      IF (NSCT .GE. NSMAX) THEN
         NSMAX = NSMAX + NSMAX
         IF (NSMAX .EQ. 0) NSMAX = 128
         IERR = SO_ALLC_ALLINT(NSMAX, LIFIC)
         IERR = SO_ALLC_ALLRE8(NSMAX, LTEMP)
         IERR = SO_ALLC_ALLIN8(NSMAX, LFPOS)
         FD_PRNO_NSMAX(IOB) = NSMAX
         FD_PRNO_LIFIC(IOB) = LIFIC
         FD_PRNO_LTEMP(IOB) = LTEMP
         FD_PRNO_LFPOS(IOB) = LFPOS
      ENDIF

C---     Les tables
      PIFIC = SO_ALLC_PTR(LIFIC)
      PTMPS = SO_ALLC_PTR(LTEMP)
      PPOS  = SO_ALLC_PTR(LFPOS)
      KIFIC => PIFIC%CST2K()
      VTMPS => PTMPS%CST2V()
      KPOS  => PPOS %CST2X()

C---     Assigne les valeurs
      NSCT = NSCT + 1
      KIFIC(NSCT) = IFIC
      VTMPS(NSCT) = TEMPS
      KPOS (NSCT) = XPOS

C---     Conserve les attributs
      FD_PRNO_NSCT (IOB) = NSCT

      FD_PRNO_ASGSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les paramètres de section
C
C Description:
C     La fonction protégée FD_PRNO_REQSCT retourne les données sur
C     une section.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     ISCT        Indice de la section
C
C Sortie:
C     TEMPS       Temps associé à la section
C     IPOS        Position du début de la section dans le fichier
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQSCT(HOBJ, ISCT, TEMPS, XPOS)

      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   ISCT
      REAL*8    TEMPS
      INTEGER*8 XPOS

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LFPOS
      INTEGER LTEMP
      INTEGER NSCT
      TYPE(SO_ALLC_PTR_T) :: PTMPS
      TYPE(SO_ALLC_PTR_T) :: PPOS
      REAL*8,    POINTER :: VTMPS(:)
      INTEGER*8, POINTER :: KPOS (:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
D     CALL ERR_PRE(ISCT .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNO_HBASE
      NSCT  = FD_PRNO_NSCT (IOB)
      LTEMP = FD_PRNO_LTEMP(IOB)
      LFPOS = FD_PRNO_LFPOS(IOB)
D     CALL ERR_ASR(ISCT .LE. NSCT)

C---     Les tables
      PTMPS = SO_ALLC_PTR(LTEMP)
      PPOS  = SO_ALLC_PTR(LFPOS)
      VTMPS => PTMPS%CST2V()
      KPOS  => PPOS %CST2X()

C---     Les valeurs
      TEMPS = VTMPS(ISCT)
      XPOS  = KPOS (ISCT)

      FD_PRNO_REQSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Trie les paramètres de section
C
C Description:
C     La méthode protégée FD_PRNO_TRISCT trie les paramètres de section
C     par ordre croissant de temps.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_TRISCT(HOBJ)

      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER   HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NSCT
      INTEGER NSMAX
      INTEGER LIFIC, LTEMP, LFPOS
      TYPE(SO_ALLC_PTR_T) :: PIFIC
      TYPE(SO_ALLC_PTR_T) :: PTMPS
      TYPE(SO_ALLC_PTR_T) :: PPOS
      INTEGER,   POINTER :: KIFIC(:)
      REAL*8,    POINTER :: VTMPS(:)
      INTEGER*8, POINTER :: KPOS (:)
      INTEGER, ALLOCATABLE :: KIDX(:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - FD_PRNO_HBASE
!      NSMAX = FD_PRNO_NSMAX(IOB)
      NSCT  = FD_PRNO_NSCT (IOB)
      LIFIC = FD_PRNO_LIFIC(IOB)
      LTEMP = FD_PRNO_LTEMP(IOB)
      LFPOS = FD_PRNO_LFPOS(IOB)

C---     Les tables
      PIFIC = SO_ALLC_PTR(LIFIC)
      PTMPS = SO_ALLC_PTR(LTEMP)
      PPOS  = SO_ALLC_PTR(LFPOS)
      KIFIC => PIFIC%CST2K()
      VTMPS => PTMPS%CST2V()
      KPOS  => PPOS %CST2X()

C---     Alloue la table d'indexation
      ALLOCATE(KIDX(NSCT))

C---     Génère la table d'index pour le tri
      CALL DINDEX(NSCT, VTMPS, KIDX)

C---     Trie les tables
      CALL ISWAPF(1, NSCT, KIFIC, KIDX)
      CALL DSWAPF(1, NSCT, VTMPS, KIDX)
      CALL XSWAPF(1, NSCT, KPOS,  KIDX)

C---     Désalloue la table d'indexation
      IF (ALLOCATED(KIDX)) DEALLOCATE(KIDX)

      FD_PRNO_TRISCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le status
C
C Description:
C     La fonction FD_PRNO_REQISTAT retourne le status de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQISTAT (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_REQISTAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprno.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      FD_PRNO_REQISTAT  = FD_PRNO_ISTAT(HOBJ-FD_PRNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre de Noeud Total
C
C Description:
C     La fonction FD_PRNO_REQNNT retourne le nombre de noeuds total
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQNNT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_REQNNT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprno.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      FD_PRNO_REQNNT = FD_PRNO_NNT(HOBJ-FD_PRNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de dimension
C
C Description:
C     La fonction FD_PRNO_REQNPRN retourne le nombre de dimensions
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQNPRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_REQNPRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprno.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      FD_PRNO_REQNPRN  = FD_PRNO_NPRN (HOBJ-FD_PRNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de sections
C
C Description:
C     La fonction FD_PRNO_REQNSCT retourne le nombre de sections
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQNSCT (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_REQNSCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprno.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      FD_PRNO_REQNSCT  = FD_PRNO_NSCT (HOBJ-FD_PRNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le temps min des sections
C
C Description:
C     La fonction FD_PRNO_REQTMIN retourne le temps associé à la
C     première section.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQTMIN (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_REQTMIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprno.fc'

      REAL*8    TEMPS
      INTEGER*8 XPOS
      INTEGER   ISCT
      INTEGER   IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISCT = 1
      IERR = FD_PRNO_REQSCT(HOBJ, ISCT, TEMPS, XPOS)

      FD_PRNO_REQTMIN = TEMPS
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le temps max des sections
C
C Description:
C     La fonction FD_PRNO_REQTMIN retourne le temps associé à la
C     dernière section.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQTMAX (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_REQTMAX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprno.fc'

      REAL*8    TEMPS
      INTEGER*8 XPOS
      INTEGER   ISCT
      INTEGER   IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISCT = FD_PRNO_REQNSCT(HOBJ)
      IERR = FD_PRNO_REQSCT (HOBJ, ISCT, TEMPS, XPOS)

      FD_PRNO_REQTMAX = TEMPS
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de fichier.
C
C Description:
C     La fonction FD_PRNO_REQNFIC retourne le nombre de fichiers
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQNFIC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_REQNFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprno.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER HLIST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HLIST = FD_PRNO_HLIST(HOBJ-FD_PRNO_HBASE)

      FD_PRNO_REQNFIC = DS_LIST_REQDIM(HLIST)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nom du fichier.
C
C Description:
C     La fonction FD_PRNO_REQNOMF retourne le nom du fichier
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNO_REQNOMF(HOBJ, IFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNO_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IFIC

      INCLUDE 'fdprno.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'

      INTEGER IERR
      INTEGER HLIST
      INTEGER L
      CHARACTER*256 BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HOBJ))
D     CALL ERR_PRE(IFIC .GE. 1)
C------------------------------------------------------------------------

      HLIST = FD_PRNO_HLIST(HOBJ-FD_PRNO_HBASE)
D     CALL ERR_ASR(DS_LIST_HVALIDE(HLIST))
D     CALL ERR_ASR(IFIC .LE. DS_LIST_REQDIM(HLIST))
      IERR = DS_LIST_REQVAL(HLIST, IFIC, BUF)
      IF (ERR_GOOD()) THEN
         L = MIN(SP_STRN_LEN(BUF), 256)
         IF (L .EQ. 0) THEN
            FD_PRNO_REQNOMF  = ' '
         ELSE
            FD_PRNO_REQNOMF  = BUF(1:L)
         ENDIF
      ELSE
         FD_PRNO_REQNOMF  = ' '
      ENDIF

      RETURN
      END
