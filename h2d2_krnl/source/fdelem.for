C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: FD_ELEM
C     INTEGER FD_ELEM_000
C     INTEGER FD_ELEM_999
C     INTEGER FD_ELEM_PKL
C     INTEGER FD_ELEM_UPK
C     INTEGER FD_ELEM_CTR
C     INTEGER FD_ELEM_DTR
C     INTEGER FD_ELEM_INI
C     INTEGER FD_ELEM_RST
C     INTEGER FD_ELEM_REQHBASE
C     LOGICAL FD_ELEM_HVALIDE
C     INTEGER FD_ELEM_LIS
C     INTEGER FD_ELEM_LISVAL
C     INTEGER FD_ELEM_LISSCT
C     INTEGER FD_ELEM_INISTR
C     INTEGER FD_ELEM_ASGSCT
C     INTEGER FD_ELEM_ASGVALSCT
C     INTEGER FD_ELEM_REQNELL
C     INTEGER FD_ELEM_REQITPE
C     INTEGER FD_ELEM_REQNELT
C     INTEGER FD_ELEM_REQNNEL
C     INTEGER FD_ELEM_REQNSCT
C     CHARACTER*256 FD_ELEM_REQNOMF
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction FD_ELEM_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(FD_ELEM_NOBJMAX,
     &                   FD_ELEM_HBASE,
     &                   'Connectivities File')

      FD_ELEM_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction FD_ELEM_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fc'

      INTEGER  IERR
      EXTERNAL FD_ELEM_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(FD_ELEM_NOBJMAX,
     &                   FD_ELEM_HBASE,
     &                   FD_ELEM_DTR)

      FD_ELEM_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - FD_ELEM_HBASE

C---     Pickle
      IERR = IO_XML_WS  (HXML, FD_ELEM_FNAME(IOB))
      IERR = IO_XML_WI_1(HXML, FD_ELEM_ISTAT(IOB))
      IERR = IO_XML_WI_1(HXML, FD_ELEM_ITPE (IOB))
      IERR = IO_XML_WI_1(HXML, FD_ELEM_NELT (IOB))
      IERR = IO_XML_WI_1(HXML, FD_ELEM_NNEL (IOB))
      IERR = IO_XML_WI_1(HXML, FD_ELEM_NSCT (IOB))
      IERR = IO_XML_WI_1(HXML, FD_ELEM_NSMAX(IOB))

C---     La table des temps
      IF (ERR_GOOD()) THEN
         N = FD_ELEM_NSCT (IOB)
         NX= FD_ELEM_NSMAX(IOB)
         L = FD_ELEM_LTEMP(IOB)
         IERR = IO_XML_WD_V(HXML, NX, N, L, 1)
      ENDIF

C---     La table des positions
      IF (ERR_GOOD() .AND. N .GT. 0) THEN
         N = FD_ELEM_NSCT (IOB)
         NX= FD_ELEM_NSMAX(IOB)
         L = FD_ELEM_LFPOS(IOB)
         IERR = IO_XML_WX_V(HXML, NX, N, L, 1)
      ENDIF

      FD_ELEM_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, NX, N
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - FD_ELEM_HBASE

C---     Efface les attributs
      IF (ERR_GOOD()) IERR = FD_ELEM_RAZ(HOBJ)

C---     Un-pickle
      IERR = IO_XML_RS  (HXML, FD_ELEM_FNAME(IOB))
      IERR = IO_XML_RI_1(HXML, FD_ELEM_ISTAT(IOB))
      IERR = IO_XML_RI_1(HXML, FD_ELEM_ITPE (IOB))
      IERR = IO_XML_RI_1(HXML, FD_ELEM_NELT (IOB))
      IERR = IO_XML_RI_1(HXML, FD_ELEM_NNEL (IOB))
      IERR = IO_XML_RI_1(HXML, FD_ELEM_NSCT (IOB))
      IERR = IO_XML_RI_1(HXML, FD_ELEM_NSMAX(IOB))

C---     La table des temps
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. FD_ELEM_NSMAX(IOB))
D     CALL ERR_ASR(ERR_BAD() .OR. N  .EQ. FD_ELEM_NSCT (IOB))
      FD_ELEM_LTEMP(IOB) = L

C---     La table des positions
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RX_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. FD_ELEM_NSMAX(IOB))
D     CALL ERR_ASR(ERR_BAD() .OR. N  .EQ. FD_ELEM_NSCT (IOB))
      FD_ELEM_LFPOS(IOB) = L

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ' : ', HOBJ
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_ELEM_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction FD_ELEM_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   FD_ELEM_NOBJMAX,
     &                                   FD_ELEM_HBASE)
      IF (ERR_GOOD()) IERR = FD_ELEM_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. FD_ELEM_HVALIDE(HOBJ))

      FD_ELEM_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction FD_ELEM_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = FD_ELEM_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   FD_ELEM_NOBJMAX,
     &                   FD_ELEM_HBASE)

      FD_ELEM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode FD_ELEM_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_ASR(FD_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - FD_ELEM_HBASE

      FD_ELEM_FNAME (IOB) = ' '
      FD_ELEM_ISTAT (IOB) = IO_MODE_INDEFINI
      FD_ELEM_NELT  (IOB) = 0
      FD_ELEM_NNEL  (IOB) = 0
      FD_ELEM_ITPE  (IOB) = 0
      FD_ELEM_NSCT  (IOB) = 0
      FD_ELEM_NSMAX (IOB) = 0
      FD_ELEM_LTEMP (IOB) = 0
      FD_ELEM_LFPOS (IOB) = 0

      FD_ELEM_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction FD_ELEM_INI initialise l'objet à l'aide des
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     FICCOR      Nom du fichier de connectivités
C     ISTAT       IO_LECTURE, IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_INI(HOBJ, NOMFIC, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT

      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.elem')

C---     Contrôle les paramètres
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9900
      IF (.NOT. IO_UTIL_MODVALIDE(ISTAT)) GOTO 9901

C---     Reset les données
      IERR = FD_ELEM_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - FD_ELEM_HBASE
         FD_ELEM_FNAME(IOB) = NOMFIC(1:SP_STRN_LEN(NOMFIC))
         FD_ELEM_ISTAT(IOB) = ISTAT
         FD_ELEM_NELT (IOB) = 0
         FD_ELEM_NNEL (IOB) = 0
         FD_ELEM_NSCT (IOB) = 0
         FD_ELEM_NSMAX(IOB) = 0
         FD_ELEM_LTEMP(IOB) = 0
         FD_ELEM_LFPOS(IOB) = 0
      ENDIF

C---     En lecture, initialise la structure interne
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) THEN
         IF (ERR_GOOD()) IERR = FD_ELEM_INISTR(HOBJ, NOMFIC)
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A, I3)') 'ERR_MODE_ACCES_INVALIDE',': ', ISTAT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.elem')
      FD_ELEM_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction FD_ELEM_RST reset l'objet, donc le remet dans
C     un état valide non initialisé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LTEMP
      INTEGER LFPOS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère l'indice
      IOB  = HOBJ - FD_ELEM_HBASE

C---     Récupère les attributs
      LTEMP = FD_ELEM_LTEMP(IOB)
      LFPOS = FD_ELEM_LFPOS(IOB)

C---     Désalloue la mémoire
      IF (LTEMP .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTEMP)
      IF (LFPOS .NE. 0) IERR = SO_ALLC_ALLIN8(0, LFPOS)

C---     Efface les attributs
      IF (ERR_GOOD()) IERR = FD_ELEM_RAZ(HOBJ)

      FD_ELEM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction FD_ELEM_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdelem.fi'
      INCLUDE 'fdelem.fc'
C------------------------------------------------------------------------

      FD_ELEM_REQHBASE = FD_ELEM_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction FD_ELEM_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdelem.fc'
C------------------------------------------------------------------------

      FD_ELEM_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  FD_ELEM_NOBJMAX,
     &                                  FD_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: FD_ELEM_ECRIS
C
C Description:
C     La fonction FD_ELEM_ECRIS est la fonction principale d'écriture
C     d'un fichier de type ELEMents.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMC       Handle sur la renumérotation
C     NNTL        Nombre de noeuds local
C     NPRN        Nombre de propriétés nodales
C     VPRNO       Vecteur des propriétés nodales
C     TSIM        Temps des valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_ECRIS(HOBJ,
     &                       HNUMC,
     &                       HNUME,
     &                       ITPE,
     &                       NELT,
     &                       NELL,
     &                       NNEL,
     &                       KNG,
     &                       TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_ECRIS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMC
      INTEGER HNUME
      INTEGER ITPE
      INTEGER NELT
      INTEGER NELL
      INTEGER NNEL
      INTEGER KNG(NNEL, NELL)
      REAL*8  TSIM

      INCLUDE 'fdelem.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER I_MASTER
      INTEGER I_STATUS(MPI_STATUS_SIZE)
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER ISTAT
      INTEGER I_RANK, I_SIZE, I_ERROR, I_TAG, I_COMM
      INTEGER M1
      INTEGER L
      INTEGER IE, IN, IR, IRSND
      INTEGER KTMP(FD_ELEM_MAXCOL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMC))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUME))
!!D     CALL ERR_PRE(NNT  .GE. NNL)
D     CALL ERR_PRE(NELL .GE. 0)
D     CALL ERR_PRE(NNEL .GT. 0)
D     CALL ERR_PRE(TSIM .GE. 0)
D     CALL ERR_PRE(NNEL .LE. FD_ELEM_MAXCOL)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.elem.write')

C---     Récupère les attributs
      IOB = HOBJ - FD_ELEM_HBASE
      ISTAT = FD_ELEM_ISTAT(IOB)
      L = SP_STRN_LEN(FD_ELEM_FNAME(IOB))

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)

C---     Ouvre le fichier
      IERR = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         M1 = IO_UTIL_FREEUNIT()
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) THEN
            OPEN (UNIT  = M1,
     &            ERR   = 109,
     &            FILE  = FD_ELEM_FNAME(IOB)(1:L),
     &            FORM  = 'FORMATTED',
     &            STATUS= 'REPLACE')
            FD_ELEM_ISTAT(IOB) = FD_ELEM_ISTAT(IOB)      ! Pour la suite,
     &               - IO_MODE_ECRITURE + IO_MODE_AJOUT  ! IO_MODE_AJOUT
         ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_AJOUT)) THEN
            OPEN (UNIT  = M1,
     &            ERR   = 109,
     &            FILE  = FD_ELEM_FNAME(IOB)(1:L),
     &            FORM  = 'FORMATTED',
     &            STATUS= 'UNKNOWN',
     &            ACCESS='APPEND')     ! Non standard
C     &            POSITION='APPEND')  ! Pas implanté par g77
         ELSE
            IERR = -2
         ENDIF
         GOTO 110
109      CONTINUE
         IERR = -1
110      CONTINUE
      ENDIF
C---     BROADCAST LE CODE D'ERREUR
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9900
      IF (IERR .EQ. -2) GOTO 9901
D     CALL ERR_ASR(IERR .EQ. 0)

C---     Écris les connectivités
      IF (I_RANK .EQ. I_MASTER) THEN
         WRITE (M1,'(3I12,1PE26.17E3)',ERR=209) NELT,NNEL,ITPE,TSIM
         GOTO 210
209      CONTINUE
         IERR = -1
210      CONTINUE
      ENDIF
C---     Broadcast le code d'erreur à cause du write
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9902
D     CALL ERR_ASR(IERR .EQ. 0)

C---     Si on a un seul process
      IF (I_SIZE .GT. 1) GOTO 1000
D     CALL ERR_ASR(NELL .EQ. NELT)
      DO IE=1,NELL
         DO IN=1,NNEL
            KTMP(IN) = NM_NUMR_REQNINT(HNUMC, KNG(IN,IE))
         ENDDO
         WRITE (M1,'(25I12)',ERR=9902) (KTMP(IN),IN=1,NNEL)
      ENDDO
      GOTO 1999

C---     Écris la séquence des propriétés nodales
1000  CONTINUE
!!!      I_TAG = 12
!!!      DO IN=1,NNT
!!!         INL = NM_NUMR_REQNINT(HNUMR, IN)
!!!         IR  = -1
!!!         IF (INL .GT. 0) THEN
!!!            IF (NM_NUMR_ESTNOPP(HNUMR, INL)) IR = I_RANK
!!!         ENDIF
!!!         CALL MPI_ALLREDUCE(IR, IRSND, 1, MP_TYPE_INT(), MPI_MAX,
!!!     &                      I_COMM, I_ERROR)
!!!         IF (ERR_BAD()) GOTO 1999
!!!
!!!         IF (I_RANK .EQ. I_MASTER) THEN
!!!            IF (IRSND .EQ. I_RANK) THEN
!!!D              CALL ERR_ASR(INL .GT. 0 .AND. INL .LE. NNL)
!!!               CALL DCOPY(NPRN, VPRNO(1,INL), 1, VTMP, 1)
!!!            ELSE
!!!               CALL MPI_RECV(VTMP, NPRN, MP_TYPE_RE8(),
!!!     &                       IRSND,
!!!     &                       I_TAG, I_COMM, I_STATUS,I_ERROR)
!!!               IF (ERR_BAD()) GOTO 1999
!!!            ENDIF
!!!            IERR = 0
!!!            WRITE (M1,'(25(1PE26.17E3))',ERR=1009)
!!!     &               (VTMP(ID),ID=1,NPRN)
!!!            GOTO 1010
!!!1009        CONTINUE
!!!            IERR = -1
!!!1010        CONTINUE
!!!
!!!         ELSEIF (IRSND .EQ. I_RANK) THEN
!!!D           CALL ERR_ASR(INL .GT. 0 .AND. INL .LE. NNL)
!!!            CALL MPI_SEND(VPRNO(1,INL), NPRN, MP_TYPE_RE8(),
!!!     &                    I_MASTER,
!!!     &                    I_TAG, I_COMM, I_ERROR)
!!!            IF (ERR_BAD()) GOTO 1999
!!!         ENDIF
!!!
!!!C---        BROADCAST LE CODE D'ERREUR A CAUSE DU WRITE
!!!         CALL MPI_BCAST(IERR, 1, MP_TYPE_INT(), I_MASTER, I_COMM, I_ERROR)
!!!         IF (IERR .EQ. -1) GOTO 9902
!!!D        CALL ERR_ASR(IERR .EQ. 0)
!!!
!!!      ENDDO

C---     FERME LE FICHIER
1999  CONTINUE
      IF (I_RANK .EQ. I_MASTER) CLOSE(M1)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                       FD_ELEM_FNAME(IOB)(1:L)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_MODE_ACCES_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(3A)') 'ERR_ECRITURE_FICHIER',': ',
     &                       FD_ELEM_FNAME(IOB)(1:L)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.elem.write')
      FD_ELEM_ECRIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_ELEM_LIS
C
C Description:
C     La fonction FD_ELEM_LIS est la fonction principale de lecture
C     d'un fichier de type ELEMents.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_LIS(HOBJ,
     &                     HNUMC,
     &                     HNUME,
     &                     NELL,
     &                     NNEL,
     &                     KNG,
     &                     TOLD,
     &                     TNEW,
     &                     MODIF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_LIS
CDEC$ ENDIF
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMC
      INTEGER HNUME
      INTEGER NELL
      INTEGER NNEL
      INTEGER KNG(NNEL, NELL)
      REAL*8  TOLD
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NSCT
      INTEGER L
      INTEGER LTEMP
      INTEGER LFPOS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMC))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUME))
D     CALL ERR_PRE(NELL .GT. 1)
D     CALL ERR_PRE(NNEL .GT. 1)
D     CALL ERR_PRE(TNEW .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     DEMARRE LE CHRONO
      CALL TR_CHRN_START('h2d2.io.elem.read')

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_ELEM_HBASE

C---     RECUPERE LES ATTRIBUTS
      NSCT  = FD_ELEM_NSCT (IOB)
      LTEMP = FD_ELEM_LTEMP(IOB)
      LFPOS = FD_ELEM_LFPOS(IOB)

C---     LIS LA SECTION
      L = SP_STRN_LEN(FD_ELEM_FNAME(IOB))
      IERR = FD_ELEM_LISVAL(HOBJ,
     &                      NELL,
     &                      NNEL,
     &                      KNG,
     &                      TOLD,
     &                      TNEW,
     &                      FD_ELEM_FNAME(IOB)(1:L),
     &                      HNUMC,
     &                      HNUME,
     &                      NSCT,
     &                      VA(SO_ALLC_REQVIND(VA,LTEMP)),
     &                      XA(SO_ALLC_REQXIND(XA,LFPOS)),
     &                      MODIF)

C---     STOPPE LE CHRONO
      CALL TR_CHRN_STOP('h2d2.io.elem.read')

      FD_ELEM_LIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_ELEM_LISVAL
C
C Description:
C     La fonction privée FD_ELEM_LISVAL est la fonction principale
C     de lecture d'un fichier de type ELEMents.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_LISVAL(HOBJ,
     &                        NELTL,
     &                        NNEL,
     &                        KNG,
     &                        TOLD,
     &                        TNEW,
     &                        NOMFIC,
     &                        HNUMC,
     &                        HNUME,
     &                        NSCT,
     &                        VTEMPS,
     &                        KPOS,
     &                        MODIF)

      IMPLICIT NONE

      INTEGER        HOBJ
      INTEGER        NELTL
      INTEGER        NNEL
      INTEGER        KNG(NNEL, NELTL)
      REAL*8         TOLD
      REAL*8         TNEW
      CHARACTER*(*)  NOMFIC
      INTEGER        HNUMC
      INTEGER        HNUME
      INTEGER        NSCT
      REAL*8         VTEMPS(NSCT)
      INTEGER*8      KPOS  (NSCT)
      LOGICAL        MODIF

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER IERR
      INTEGER IS, IS1, IS2
      INTEGER I_RANK, I_ERROR, I_COMM
      LOGICAL SAUT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NELTL.GT. 1)
D     CALL ERR_PRE(NNEL .GT. 1)
D     CALL ERR_PRE(TNEW .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      MODIF = .FALSE.

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

      IF (I_RANK .NE. I_MASTER) GOTO 200

C---     Contrôles primaires
      SAUT = .TRUE.
      IF (TOLD .GE. 0) THEN
         IF (TNEW.EQ.TOLD) GOTO 119
         IF (TOLD.GE.VTEMPS(NSCT) .AND. TNEW.GE.VTEMPS(NSCT)) GOTO 119
         IF (TOLD.LE.VTEMPS(   1) .AND. TNEW.LE.VTEMPS(   1)) GOTO 119
      ENDIF
      SAUT = .FALSE.
119   CONTINUE

C---     Recherche les sections
      IS = 1
      IF (.NOT. SAUT) THEN
         DO IS=1,NSCT
            IF (VTEMPS(IS) .GT. TNEW) GOTO 120
         ENDDO
120      CONTINUE
      ENDIF
      IS2 = IS
      IS1 = IS2 - 1

C---     Broadcast comme securité
200   CONTINUE
      CALL MPI_BCAST(SAUT, 1,MPI_LOGICAL,  I_MASTER,I_COMM,I_ERROR)
      CALL MPI_BCAST(IS1,  1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      CALL MPI_BCAST(IS2,  1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (SAUT) GOTO 9999

C---     TNEW < TMIN
      IF (ERR_GOOD() .AND. IS2 .EQ. 1) THEN
         IERR = FD_ELEM_LISSCT(HOBJ,
     &                         NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &                         KPOS(IS2),
     &                         HNUMC,
     &                         HNUME,
     &                         NELTL,
     &                         NNEL,
     &                         KNG)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TNEW > TMAX
      IF (ERR_GOOD() .AND. IS1 .EQ. NSCT) THEN
         IERR = FD_ELEM_LISSCT(HOBJ,
     &                         NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &                         KPOS(IS1),
     &                         HNUMC,
     &                         HNUME,
     &                         NELTL,
     &                         NNEL,
     &                         KNG)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TNEW = TINF
      IF (ERR_GOOD() .AND. TNEW .EQ. VTEMPS(IS1)) THEN
         IERR = FD_ELEM_LISSCT(HOBJ,
     &                         NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &                         KPOS(IS1),
     &                         HNUMC,
     &                         HNUME,
     &                         NELTL,
     &                         NNEL,
     &                         KNG)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TNEW = TSUP
      IF (ERR_GOOD() .AND. TNEW .EQ. VTEMPS(IS2)) THEN
         IERR = FD_ELEM_LISSCT(HOBJ,
     &                         NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &                         KPOS(IS2),
     &                         HNUMC,
     &                         HNUME,
     &                         NELTL,
     &                         NNEL,
     &                         KNG)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TINF > TNEW > TSUP - Lis la valeur inferieur
      IF (ERR_GOOD()) THEN
         IERR = FD_ELEM_LISSCT(HOBJ,
     &                         NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &                         KPOS(IS1),
     &                         HNUMC,
     &                         HNUME,
     &                         NELTL,
     &                         NNEL,
     &                         KNG)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

8000  CONTINUE

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                       NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_ELEM_LISVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_ELEM_LISSCT
C
C Description:
C     La fonction privée FD_ELEM_LISSCT lis une section d'un fichier
C     de type ELEMents.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_LISSCT(HOBJ,
     &                        FNAME,
     &                        XPOS,
     &                        HNUMC,
     &                        HNUME,
     &                        NELTL,
     &                        NNEL,
     &                        KNG)

      IMPLICIT NONE

      INTEGER,       INTENT(IN) :: HOBJ
      CHARACTER*(*), INTENT(IN) :: FNAME     ! Nom du fichier
      INTEGER*8,     INTENT(IN) :: XPOS      ! Offset dans le fichier
      INTEGER,       INTENT(IN) :: HNUMC
      INTEGER,       INTENT(IN) :: HNUME
      INTEGER,       INTENT(IN) :: NELTL
      INTEGER,       INTENT(IN) :: NNEL
      INTEGER,       INTENT(INOUT) :: KNG(NNEL, NELTL)

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER*8 XFIC       ! handle eXterne sur le fichier
      INTEGER IERR
      INTEGER IB
      INTEGER IE, IEG, IEL, IN
      INTEGER I_RANK, I_SIZE, I_ERROR, I_COMM
      INTEGER NELB, NEL
      INTEGER NELTG, NELTL_SUM
      INTEGER NNEL_L, ITPE_L
      REAL*8  TMPS_L

      INTEGER, PARAMETER :: NBMAX = 512
      INTEGER, ALLOCATABLE :: KTMP(:,:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NELTL .GT. 1)
D     CALL ERR_PRE(NNEL  .GT. 1)
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMC))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUME))
D     CALL ERR_PRE(NNEL  .LE. FD_ELEM_MAXCOL)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)

C---     Ouvre le fichier
      XFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = FD_ELEM_OPEN(HOBJ,
     &                       FNAME(1:SP_STRN_LEN(FNAME)),
     &                       C_FA_LECTURE,
     &                       XPOS,
     &                       XFIC)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     Lis l'entête
      NELTG = -1
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = FD_ELEM_RHDR(HOBJ, XFIC, NELTG, NNEL_L, ITPE_L, TMPS_L)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     Broadcast the data
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(NELTG, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         CALL MPI_BCAST(NNEL_L,1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         CALL MPI_BCAST(ITPE_L,1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         CALL MPI_BCAST(TMPS_L,1,MP_TYPE_RE8(),I_MASTER,I_COMM,I_ERROR)
      ENDIF

C---     Contrôle la cohérence des dimensions locales et globales
      IF (ERR_GOOD()) THEN
         IF (I_SIZE .LE. 1) THEN       ! 1 PROC ==> NELTL = NELTG
            IF (NELTG .NE. NELTL) GOTO 9904
         ELSE                          ! n PROC ==> SUM(NELTL) > NELTG
            CALL MPI_ALLREDUCE(NELTL, NELTL_SUM, 1, MP_TYPE_INT(),
     &                         MPI_SUM, I_COMM, I_ERROR)
            IF (ERR_GOOD() .AND. NELTL_SUM .LE. NELTG) GOTO 9905
         ENDIF
      ENDIF

C---     Alloue le bloc de lecture
      IF (ERR_GOOD()) THEN
         ALLOCATE(KTMP(1:NNEL,1:NBMAX))
      ENDIF

C---     Lis les lignes par blocs
      IEG = 0  ! Element global
      NEL = 0  ! Nombre d'éléments traités
      DO IB=1, NELTG, NBMAX
         NELB = MIN(NELTG-IB+1, NBMAX)
         IF (I_RANK .EQ. I_MASTER) THEN
            IERR = FD_ELEM_BDTA(HOBJ, XFIC, NELB, NNEL, KTMP)
         ENDIF
         IERR = MP_UTIL_SYNCERR()

C---        Broadcast the data
         IF (ERR_GOOD()) THEN    ! devrait être asynch.
            CALL MPI_BCAST(KTMP, NNEL*NELB, MP_TYPE_INT(),
     &                     I_MASTER, I_COMM, I_ERROR)
         ENDIF

C---        Conserve si concerné
         IF (ERR_GOOD()) THEN
            DO IE=1, NELB
               IEG = IEG + 1

               DO IN=1,NNEL
                  KTMP(IN, IE) = NM_NUMR_REQNINT(HNUMC, KTMP(IN, IE))
                  IF (KTMP(IN, IE) .LE. 0) GOTO 199
               ENDDO

               IEL = NM_NUMR_REQNINT(HNUME, IEG)  ! Local numbering
               IF (IEL .LE. 0) GOTO 9906
               KNG(1:NNEL,IEL) = KTMP(1:NNEL,IE)
               NEL = NEL + 1
      
199            CONTINUE
            ENDDO
         ENDIF

      ENDDO

      IF (NEL .NE. NELTL) GOTO 9903

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_NBR_VALEURS_LUES',': ',
     &      IEG, ' / ', NELTL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_NELL_GE_NELT',': ',
     &      NELTL, ' / ', NELTG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_SUM(NELL)_LE_NELT',': ',
     &      NELTL_SUM, ' / ', NELTG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_DONNEES_INVALIDES'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (XFIC .NE. 0) IERR = FD_ELEM_CLOSE(HOBJ, XFIC)
      IF (ALLOCATED(KTMP)) DEALLOCATE(KTMP)
      IERR = MP_UTIL_SYNCERR()
      FD_ELEM_LISSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_ELEM_OPEN
C
C Description:
C     La fonction privée FD_ELEM_OPEN ouvre le fichier à la position
C     XPOS.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     FNAME    Nom du fichier
C     ISTA     Status du fichier
C     XPOS     Offset dans le fichier
C
C Sortie:
C     XFIC     Handle sur le fichier
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_OPEN(HOBJ,
     &                      FNAME,
     &                      ISTA,
     &                      XPOS,
     &                      XFIC)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME     ! Nom du fichier
      INTEGER       ISTA
      INTEGER*8     XPOS      ! Offset dans le fichier
      INTEGER*8     XFIC

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR, IRET
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = 0
      XFIC = 0

C---     Ouvre le fichier
      XFIC = C_FA_OUVRE(FNAME(1:SP_STRN_LEN(FNAME)), ISTA)
      IF (XFIC .EQ. 0) GOTO 9900

C---     Positionne dans le fichier
      IF (XPOS .GT. 0) THEN
         IRET = C_FA_ASGPOS(XFIC, XPOS)
         IF (IRET .NE. 0) GOTO 9901
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_POSITION_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_ELEM_OPEN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_ELEM_CLOSE
C
C Description:
C     La fonction privée FD_ELEM_CLOSE ferme le fichier.
C
C Entrée:
C     XFIC     Handle sur le fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_CLOSE(HOBJ, XFIC)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XFIC

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR, IRET
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = 0

      IRET = C_FA_FERME(XFIC)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FERMETURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_ELEM_CLOSE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis un groupe d'entêtes
C
C Description:
C     La fonction privée FD_ELEM_BHDR lis un maximum de NBMAX entêtes.
C     Elle ne doit être appelée que par le process maître.
C
C Entrée:
C     HOBJ              Handle sur l'objet
C     XFIC              Handle eXterne sur le fichier
C     NNT               Nombre de Noeuds Total
C     NPRN              Nombre de Propriétés Nodales
C     TMAX              Temps max du fichier
C     NBMAX             Nombre max de bloc à lire
C
C Sortie:
C     TMAX              Temps max du fichier
C     IBMAX             Nombre de blocs lus
C     KPOS              Table des positions dans le fichier
C     VTMP              Table des temps
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_BHDR(HOBJ,
     &                      XFIC,
     &                      NELT,
     &                      NNEL,
     &                      ITPE,
     &                      TMAX,
     &                      IBMAX,
     &                      NBMAX,
     &                      KPOS,
     &                      VTMP)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XFIC
      INTEGER   NELT
      INTEGER   NNEL
      INTEGER   ITPE
      REAL*8    TMAX
      INTEGER   IBMAX
      INTEGER   NBMAX
      INTEGER*8 KPOS(NBMAX)
      REAL*8    VTMP(NBMAX)

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      REAL*8    TMPS_L
      INTEGER*8 XPOS
      INTEGER   IB
      INTEGER   IERR, IRET
      INTEGER   LB
      INTEGER   NELT_L, NNEL_L, ITPE_L

      INTEGER, PARAMETER :: LBUF = 1024
      CHARACTER*(LBUF)  BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NNEL .GT. 0)
D     CALL ERR_PRE(NNEL .LE. FD_ELEM_MAXCOL)
D     CALL ERR_PRE(NELT .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Initialise
      BUF  = ' '

C---     Boucle sur les blocs
      DO IB=1,NBMAX
         IRET = C_FA_REQPOS(XFIC, XPOS)
         IRET = C_FA_LISLN (XFIC, BUF)
         IF (IRET .NE. 0) GOTO 199
         LB = SP_STRN_LEN(BUF)
         IF (LB .LE. 0) GOTO 199
         READ(BUF(1:LB), *, ERR=9902, END=9902) 
     &      NELT_L, NNEL_L, ITPE_L, TMPS_L

C---        Contrôles
         IF (NELT_L .NE. NELT) GOTO 9903
         IF (NNEL_L .NE. NNEL) GOTO 9904
         IF (ITPE_L .NE. ITPE) GOTO 9905
         IF (TMPS_L .LE. TMAX) GOTO 9906

C---        Assigne les paramètres de section
         KPOS(IB) = XPOS
         VTMP(IB) = TMPS_L

C---        Saute le bloc de données
         IRET = C_FA_SKIPN(XFIC, NELT)
         IF (IRET .NE. 0) GOTO 9901

         TMAX  = TMPS_L
         IBMAX = IB
      ENDDO
199   CONTINUE

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NELT_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_NELT:', NELT_L, '/', NELT
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NNEL_INVALIDE', ': ', NNEL_L
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_NNEL:', NNEL_L, '/', NNEL
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF, '(2A,I6)') 'ERR_ITPE_INVALIDE', ': ', ITPE_L
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_ITPE:', ITPE_L, '/', ITPE
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF, '(A)') 'ERR_TEMPS_DECROISSANT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2(A,1PE25.17E3))') 'MSG_TEMPS:',TMPS_L,' <=',TMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_ELEM_BHDR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_ELEM_BDTA
C
C Description:
C     La fonction privée FD_ELEM_BDTA lis NELL lignes de données d'un fichier
C     d'éléments.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     XFIC     Handle sur le fichier
C     NELL     Nombre d'ÉLéments à Lire
C     NCEL     Nombre de connectivités par éléments
C
C Sortie:
C     KNGL     Table des valeurs lues
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_BDTA(HOBJ,
     &                      XFIC,
     &                      NELL,
     &                      NCEL,
     &                      KNGL)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XFIC      ! Offset dans le fichier
      INTEGER   NELL
      INTEGER   NCEL
      INTEGER   KNGL(NCEL, NELL)

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR, IRET
      INTEGER IE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NCEL .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      DO IE=1,NELL
         IRET = C_FA_LISINT(XFIC, KNGL(1,IE), NCEL)
         IF (IRET .NE. 0) GOTO 9900
      ENDDO

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_ELEM_BDTA = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis une entête
C
C Description:
C     La fonction privée FD_ELEM_RHDR lis une entête.
C     Elle ne doit être appelée que par le process maître.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     XFIC           Handle sur le fichier
C
C Sortie:
C     NELT
C     NNEL
C     ITPE
C     TMPS
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_RHDR(HOBJ,
     &                      XFIC,
     &                      NELT,
     &                      NNEL,
     &                      ITPE,
     &                      TMPS)

      IMPLICIT NONE

      INTEGER,   INTENT(IN)  :: HOBJ
      INTEGER*8, INTENT(IN)  :: XFIC
      INTEGER,   INTENT(OUT) :: NELT
      INTEGER,   INTENT(OUT) :: NNEL
      INTEGER,   INTENT(OUT) :: ITPE
      REAL*8,    INTENT(OUT) :: TMPS

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR, IRET
      INTEGER LB

      INTEGER, PARAMETER :: LBUF = 1024
      CHARACTER*(LBUF)  BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Initialise
      BUF  = ' '
      NELT = 0
      NNEL = 0
      ITPE = 0

C---     Lis l'entête
      IRET = C_FA_LISLN(XFIC, BUF)
      IF (IRET .NE. 0) GOTO 9901
      LB = SP_STRN_LEN(BUF)
      IF (LB .LE. 0) GOTO 9901
      READ(BUF(1:LB), *, ERR=9902, END=9902) NELT, NNEL, ITPE, TMPS

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_ELEM_RHDR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la structure
C
C Description:
C     La fonction privée FD_ELEM_INISTR initialise la structure interne à
C     partir du fichier dont le nom est passé en argument.
C
C Entrée:
C     HOBJ                    Handle sur l'objet
C     NOMFIC                  Nom du doit
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_INISTR (HOBJ, FNAME)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) FNAME

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER, PARAMETER :: I_MASTER = 0
      INTEGER  I_RANK, I_ERROR, I_COMM

      INTEGER, PARAMETER :: NBMAX = 2

      REAL*8     VTMP(NBMAX)
      REAL*8     TMAX
      INTEGER*8  KPOS(NBMAX)
      INTEGER*8  XFIC       ! handle eXterne sur le fichier
      INTEGER*8  XPOS
      INTEGER    IERR
      INTEGER    IOB
      INTEGER    IB, IBMAX
      INTEGER    ITPE, ITPE_L
      INTEGER    NELT, NELT_L
      INTEGER    NNEL, NNEL_L
      INTEGER    NBTOT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - FD_ELEM_HBASE
      NELT_L = FD_ELEM_NELT(IOB)
      NNEL_L = FD_ELEM_NNEL(IOB)
      ITPE_L = FD_ELEM_ITPE(IOB)

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Lis les dimensions du (premier) fichier
      NELT = 0
      NNEL = 0
      ITPE = 0
      IF (ERR_GOOD()) IERR = FD_ELEM_REQDIM(HOBJ,NELT,NNEL,ITPE,FNAME)
      IF (ERR_GOOD() .AND. NNEL .LE. 0) GOTO 9900
      IF (ERR_GOOD() .AND. NELT .LE. 0) GOTO 9901
      IF (ERR_GOOD() .AND. ITPE .LE. 0) GOTO 9902
D     IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NELT_L .EQ. 0 .OR. NELT_L .EQ. NELT)
D        CALL ERR_ASR(NNEL_L .EQ. 0 .OR. NNEL_L .EQ. NNEL)
D        CALL ERR_ASR(ITPE_L .EQ. 0 .OR. ITPE_L .EQ. ITPE)
D     ENDIF
      IF (NELT_L .EQ. 0) NELT_L = NELT
      IF (NNEL_L .EQ. 0) NNEL_L = NNEL
      IF (ITPE_L .EQ. 0) ITPE_L = ITPE

      TMAX = -1.0D99

C---     Ouvre le fichier
      XFIC = 0
      XPOS = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = FD_ELEM_OPEN(HOBJ,
     &                       FNAME(1:SP_STRN_LEN(FNAME)),
     &                       C_FA_LECTURE,
     &                       XPOS,
     &                       XFIC)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     WHILE (ERR_GOOD())
      NBTOT = 0
      DO WHILE(ERR_GOOD())
         IBMAX = 0
         IF (I_RANK .EQ. I_MASTER) THEN
            IERR = FD_ELEM_BHDR(HOBJ,
     &                          XFIC,
     &                          NELT,
     &                          NNEL,
     &                          ITPE,
     &                          TMAX,
     &                          IBMAX,
     &                          NBMAX,
     &                          KPOS,
     &                          VTMP)
         ENDIF
         IERR = MP_UTIL_SYNCERR()

C---        Broadcast les données
         IF (ERR_GOOD()) THEN
            CALL MPI_BCAST(IBMAX, 1, MP_TYPE_INT(),
     &                     I_MASTER, I_COMM, I_ERROR)
         ENDIF
         IF (ERR_GOOD() .AND. IBMAX .GT. 0) THEN
            CALL MPI_BCAST(KPOS, IBMAX, MP_TYPE_IN8(),
     &                     I_MASTER, I_COMM, I_ERROR)
            CALL MPI_BCAST(VTMP, IBMAX, MP_TYPE_RE8(),
     &                     I_MASTER, I_COMM, I_ERROR)
         ENDIF

C---        Assigne les paramètres de section
         IF (ERR_GOOD()) THEN
D           CALL ERR_ASR(IBMAX .EQ. 1)
            DO IB=1,IBMAX
               IERR = FD_ELEM_ASGSCT(HOBJ, VTMP(IB), KPOS(IB))
            ENDDO
            NBTOT = NBTOT + IBMAX
         ENDIF

         IF (IBMAX .LT. NBMAX) EXIT
      ENDDO

C---     Contrôle les fichiers vides
      IF (ERR_GOOD() .AND. NBTOT .LE. 0) GOTO 9903
      IF (.NOT. ERR_GOOD()) GOTO 9989

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         FD_ELEM_NELT(IOB) = NELT_L
         FD_ELEM_NNEL(IOB) = NNEL_L
         FD_ELEM_ITPE(IOB) = ITPE_L
      ENDIF
      
      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NELT_INVALIDE', ' : ', NELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NNEL_INVALIDE', ' : ', NNEL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_ITPE_INVALIDE', ' : ', ITPE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A)') 'ERR_STRUCT_FICHIER_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9989

9989  CONTINUE
      WRITE(ERR_BUF, '(A)') FNAME(1:SP_STRN_LEN(FNAME))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (XFIC .NE. 0) IERR = FD_ELEM_CLOSE(HOBJ, XFIC)
      FD_ELEM_INISTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une section.
C
C Description:
C     La fonction privée FD_ELEM_ASGSCT ajoute une section à la
C     structure.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     TEMPS       Temps associé à la section
C     XPOS        Offset du début de la section
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_ASGSCT (HOBJ, TEMPS, XPOS)

      IMPLICIT NONE

      INTEGER   HOBJ
      REAL*8    TEMPS
      INTEGER*8 XPOS

      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NSCT
      INTEGER NSMAX
      INTEGER LFPOS
      INTEGER LTEMP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_ELEM_HBASE

C---     RECUPERE LES ATTRIBUTS
      NSMAX = FD_ELEM_NSMAX(IOB)
      NSCT  = FD_ELEM_NSCT (IOB)
      LTEMP = FD_ELEM_LTEMP(IOB)
      LFPOS = FD_ELEM_LFPOS(IOB)

C---     ALLOUE LA MEMOIRE
      IF (NSCT .GE. NSMAX) THEN
         NSMAX = NSMAX + NSMAX
         IF (NSMAX .EQ. 0) NSMAX = 100
         IERR = SO_ALLC_ALLRE8(NSMAX, LTEMP)
         IERR = SO_ALLC_ALLIN8(NSMAX, LFPOS)
      ENDIF

C---     ASSIGNE LES VALEURS
      NSCT = NSCT + 1
      IERR = FD_ELEM_ASGVALSCT(NSCT,
     &                         VA(SO_ALLC_REQVIND(VA,LTEMP)), TEMPS,
     &                         XA(SO_ALLC_REQXIND(XA,LFPOS)), XPOS)

C---     CONSERVE LES ATTRIBUTS
      FD_ELEM_NSMAX(IOB) = NSMAX
      FD_ELEM_NSCT (IOB) = NSCT
      FD_ELEM_LTEMP(IOB) = LTEMP
      FD_ELEM_LFPOS(IOB) = LFPOS

      FD_ELEM_ASGSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne les valeurs d'une section
C
C Description:
C     La fonction privée FD_ELEM_ASGVALSCT assigne les valeurs
C     des paramètres d'une section du doit.
C
C Entrée:
C     NSCT        Indice de la section
C     VTEMPS      Vecteur des temps
C     TEMPS       Temps
C     KPOS        Vecteur des positions
C     XPOS        Position
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_ASGVALSCT(NSCT, VTEMPS, TEMPS, KPOS, XPOS)

      IMPLICIT NONE

      INTEGER   NSCT
      REAL*8    VTEMPS(*)
      REAL*8    TEMPS
      INTEGER*8 KPOS  (*)
      INTEGER*8 XPOS

      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fc'
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      VTEMPS(NSCT) = TEMPS
      KPOS  (NSCT) = XPOS

      FD_ELEM_ASGVALSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les dimensions du fichier.
C
C Description:
C     La fonction privée FD_ELEM_REQDIM lis l'entête d'un fichier
C     et en retourne les dimensions.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     FNAME          Nom du fichier
C
C Sortie:
C     NELT
C     NNEL
C     ITPE
C
C Notes:
C
C************************************************************************
      FUNCTION FD_ELEM_REQDIM(HOBJ, NELT, NNEL, ITPE, FNAME)

      IMPLICIT NONE

      INTEGER, INTENT(IN)  :: HOBJ
      INTEGER, INTENT(OUT) :: NELT
      INTEGER, INTENT(OUT) :: NNEL
      INTEGER, INTENT(OUT) :: ITPE
      CHARACTER*(*), INTENT(IN) ::  FNAME

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER  I_COMM, I_RANK, I_ERROR
      INTEGER, PARAMETER :: I_MASTER = 0

      REAL*8    TEMPS
      REAL*8    TMP(4)
      INTEGER*8 XFIC, XPOS
      INTEGER   IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Lis l'entête de section
      XFIC = 0
      XPOS = 0
      NELT = 0
      NNEL = 0
      ITPE = 0
      TEMPS= 0.0D0
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (ERR_GOOD())
     &      IERR = FD_ELEM_OPEN(HOBJ,
     &                          FNAME(1:SP_STRN_LEN(FNAME)),
     &                          C_FA_LECTURE,
     &                          XPOS,
     &                          XFIC)
         IF (ERR_GOOD())
     &      IERR = FD_ELEM_RHDR(HOBJ,
     &                          XFIC,
     &                          NELT,
     &                          NNEL,
     &                          ITPE,
     &                          TEMPS)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     Broadcast tout
      IF (ERR_GOOD()) THEN
         TMP(1) = NELT
         TMP(2) = NNEL
         TMP(3) = ITPE
         TMP(4) = TEMPS
         CALL MPI_BCAST(TMP, 4, MP_TYPE_RE8(),
     &                  I_MASTER, I_COMM, I_ERROR)
         NELT  = NINT(TMP(1))
         NNEL  = NINT(TMP(2))
         ITPE  = NINT(TMP(3))
         TEMPS = TMP(4)
      ENDIF

C---     Ferme le fichier
      IF (XFIC .NE. 0) THEN
         CALL ERR_PUSH()
         IERR = FD_ELEM_CLOSE(HOBJ, XFIC)
         CALL ERR_POP()
      ENDIF

      FD_ELEM_REQDIM = ERR_TYP()
      RETURN
      END
      
C************************************************************************
C Sommaire: FD_ELEM_REQNELL
C
C Description:
C     La fonction FD_ELEM_REQNELL retourne le nombre d'éléments local.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_REQNELL(HOBJ,
     &                         HNUMC,
     &                         NELTL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_REQNELL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMC
      INTEGER NELTL

      INCLUDE 'fdelem.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER*8 XFIC       ! handle eXterne sur le fichier
      INTEGER   IERR
      INTEGER   IOB
      INTEGER   IE, IEL, IN, ILGN
      INTEGER   I_RANK, I_ERROR, I_COMM
      INTEGER   L
      INTEGER   NELTG, NNEL
D     INTEGER   NELTG_TMP, NNEL_TMP
      INTEGER   KTMP(FD_ELEM_MAXCOL)
      CHARACTER*(1024) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMC))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_ELEM_HBASE

C---     RECUPERE LES ATTRIBUTS
      NELTG = FD_ELEM_NELT(IOB)
      NNEL  = FD_ELEM_NNEL(IOB)

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     INITIALISE
      L = SP_STRN_LEN(FD_ELEM_FNAME(IOB))
      ILGN = 0
      NELTL= 0

C---     OUVRE LE FICHIER
      IERR = 0
      XFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         XFIC = C_FA_OUVRE(FD_ELEM_FNAME(IOB)(1:L), C_FA_LECTURE)
         IF (XFIC .EQ. 0) IERR = -1
      ENDIF
C---     BROADCAST LE CODE D'ERREUR
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9900
D     CALL ERR_ASR(IERR .EQ. 0)

C---     LIS L'ENTETE
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = C_FA_LISLN(XFIC, BUF)
         IF (IERR .NE. 0) GOTO 107
         ILGN = ILGN + 1
D        READ(BUF, *, ERR=108, END=109) NELTG_TMP, NNEL_TMP
D        CALL ERR_ASR(NELTG_TMP .EQ. NELTG)
D        CALL ERR_ASR(NNEL_TMP  .EQ. NNEL)
         IERR = 0
         GOTO 110
107      CONTINUE ! EOF
         IERR = -1
         GOTO 110
108      CONTINUE ! ERR
         IERR = -2
         GOTO 110
109      CONTINUE ! EOR
         IERR = -3
         GOTO 110
110      CONTINUE
      ENDIF
C---     BROADCAST LE CODE D'ERREUR
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9902
      IF (IERR .EQ. -2) GOTO 9901
      IF (IERR .EQ. -3) GOTO 9901
D     CALL ERR_ASR(IERR .EQ. 0)

C---     LIS LES LIGNES
      IEL = 0
      DO IE=1,NELTG
         IF (I_RANK .EQ. I_MASTER) THEN
            IERR = C_FA_LISLN(XFIC, BUF)
            IF (IERR .NE. 0) GOTO 208
            ILGN = ILGN + 1
            READ(BUF,*, ERR=208, END=209) (KTMP(IN),IN=1,NNEL)
            IERR = 0
            GOTO 219
208         CONTINUE ! ERR
            IERR = -1
            GOTO 219
209         CONTINUE ! END
            IERR = -2
            GOTO 219
219         CONTINUE
         ENDIF

C---        BROADCAST THE ERROR CODE
         CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         IF (IERR .EQ. -1) GOTO 9901
         IF (IERR .EQ. -2) GOTO 9902
D        CALL ERR_ASR(IERR .EQ. 0)

C---        BROADCAST THE DATA
         CALL MPI_BCAST(KTMP, NNEL, MP_TYPE_INT(),
     &                  I_MASTER, I_COMM, I_ERROR)

         DO IN=1,NNEL
            IF (KTMP(IN) .LE. 0) GOTO 9903
            KTMP(IN) = NM_NUMR_REQNINT(HNUMC, KTMP(IN))
            IF (KTMP(IN) .LE. 0) GOTO 199
         ENDDO
         IEL = IEL + 1

199      CONTINUE
      ENDDO
      NELTL = IEL

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_OUVERTURE_FICHIER', ': ',
     &                        FD_ELEM_FNAME(IOB)(1:L)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF,'(A)') 'ERR_CONNECTIVITE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I6)') 'MSG_ELEM', ': ', IE
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I6)') 'MSG_VALEUR', ': ', KTMP(IN)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9988

9988  CONTINUE
      IF (SP_STRN_LEN(BUF) .GT. 0) THEN
         CALL SP_STRN_CLP(BUF, 80)
         CALL ERR_AJT(BUF(1:SP_STRN_LEN(BUF)))
      ENDIF
      IF (ILGN .GT. 0) THEN
         WRITE(ERR_BUF, '(2A,I6,A)') FD_ELEM_FNAME(IOB)(1:L),
     &                               '(', ILGN, ')'
         CALL SP_STRN_DLC(ERR_BUF, ' ')
         CALL ERR_AJT(ERR_BUF)
      ENDIF
      GOTO 9999

9999  CONTINUE
      IF (XFIC .NE. 0) IERR = C_FA_FERME(XFIC)
      FD_ELEM_REQNELL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le STATut du fichier
C
C Description:
C     La fonction FD_ELEM_REQISTAT retourne le statut
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_REQISTAT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_REQISTAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'fdelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_ELEM_ITPE (HOBJ-FD_ELEM_HBASE) .GT. 0)
C------------------------------------------------------------------------

      FD_ELEM_REQISTAT = FD_ELEM_ISTAT(HOBJ-FD_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le TyPe d'Eléments
C
C Description:
C     La fonction FD_ELEM_REQITPE retourne le type d'élément
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_REQITPE (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_REQITPE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'fdelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_ELEM_ITPE (HOBJ-FD_ELEM_HBASE) .GT. 0)
C------------------------------------------------------------------------

      FD_ELEM_REQITPE = FD_ELEM_ITPE (HOBJ-FD_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre d'ELéments Total
C
C Description:
C     La fonction FD_ELEM_REQNELT retourne le nombre d'éléments
C     total de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_REQNELT (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_REQNELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'fdelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_ELEM_NELT (HOBJ-FD_ELEM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      FD_ELEM_REQNELT = FD_ELEM_NELT (HOBJ-FD_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre de Noeuds par ELément
C
C Description:
C     La fonction FD_ELEM_REQNNEL retourne le nombre de noeuds par
C     élément de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_REQNNEL (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_REQNNEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'fdelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_ELEM_NNEL (HOBJ-FD_ELEM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      FD_ELEM_REQNNEL = FD_ELEM_NNEL (HOBJ-FD_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre de SeCTions
C
C Description:
C     La fonction FD_ELEM_REQNSCT retourne le nombre de sections
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_REQNSCT (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_REQNSCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'fdelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_ELEM_NSCT (HOBJ-FD_ELEM_HBASE) .NE. 0)
C------------------------------------------------------------------------

      FD_ELEM_REQNSCT  = FD_ELEM_NSCT (HOBJ-FD_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le NOM du Fichier
C
C Description:
C     La fonction FD_ELEM_REQNOMF retourne le nom de fichier de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_ELEM_REQNOMF (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_ELEM_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdelem.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdelem.fc'

      INTEGER LFNAME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LFNAME = SP_STRN_LEN(FD_ELEM_FNAME(HOBJ-FD_ELEM_HBASE))
      LFNAME = MIN(LFNAME, 256)
      IF (LFNAME .EQ. 0) THEN
         FD_ELEM_REQNOMF  = ' '
      ELSE
         FD_ELEM_REQNOMF  = FD_ELEM_FNAME(HOBJ-FD_ELEM_HBASE)(1:LFNAME)
      ENDIF
      RETURN
      END

