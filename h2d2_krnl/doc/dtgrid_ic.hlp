class grid
==========
 
   The class **grid** represents a finite element mesh. A finite element mesh
   is a homogeneous assembly of identical full geometric entities of one, two
   or three dimensions, made of a finite number of nodes placed inside or on
   the contour of the domain, discretizing this domain of calculation.
    
   constructor handle grid(hcoor, helem, hnumc, hnume)
      The constructor **grid** constructs an object, with the given arguments,
      and returns a handle on this object.
         handle   HOBJ        Return value: Handle on the mesh
         handle   hcoor       Handle on the coordinates (nodes) of the mesh
         handle   helem       Handle on the connectivities (elements) of the
                           mesh.
         handle   hnumc       Handle on the node renumbering. By default, the
                           value will be set to 0 (no renumbering).
         handle   hnume       Handle on the elements renumbering. By default,
                           the value will be set to 0 (no renumbering).
    
   getter dim
      Number of dimensions
    
   getter nodes_total
      Total number of nodes
    
   getter nodes_local
      Number of nodes local to the process
    
   getter nodes_per_element
      Number of nodes per element
    
   getter elements_total
      Total number of elements
    
   getter elements_local
      Number of elements local to the process
    
   getter hcoor
      Handle on the coordinates
    
   getter helem
      Handle on the connectivities
    
   getter hnumc
      Handle on the node numbering
    
   getter hnume
      Handle on the element numbering
    
   method handle split()
      The method **split** creates a new grid with the T6 or T6L elements
      splitted into T3.
         handle   HVAL        Handle on the new grid.
    
   method load(rval)
      The method **load** loads the grid from files.
         double   rval        Time
    
   method save(nomcor, nomele)
      The method **save** saves the grid to files. The grid must be loaded,
      either explicitly with a call to the method _load_ or implicitly through
      an algorithm
         string   nomcor      Coordinates file name
         string   nomele      Connectivities file name
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class.
    
