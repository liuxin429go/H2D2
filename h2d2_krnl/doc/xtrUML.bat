@echo off
set MODUL=h2d2_krnl

set XTRDIR=%INRS_DEV%\tools\xtrapi\VP-UML
set UPDONE=%XTRDIR%\update_one.bat
set SRCDIR=%INRS_DEV%\H2D2\%MODUL%\source

call %UPDONE% "h2d2_krnl - dt" "h2d2_krnl - dt.xml" %SRCDIR%\dt*.f*
call %UPDONE% "h2d2_krnl - eg" "h2d2_krnl - eg.xml" %SRCDIR%\eg*.f*
call %UPDONE% "h2d2_krnl - fd" "h2d2_krnl - fd.xml" %SRCDIR%\fd*.f*
call %UPDONE% "h2d2_krnl - gr" "h2d2_krnl - gr.xml" %SRCDIR%\gr*.f*
call %UPDONE% "h2d2_krnl - hs" "h2d2_krnl - hs.xml" %SRCDIR%\hs*.f*
call %UPDONE% "h2d2_krnl - lm" "h2d2_krnl - lm.xml" %SRCDIR%\lm*.f*
call %UPDONE% "h2d2_krnl - nm" "h2d2_krnl - nm.xml" %SRCDIR%\nm*.f*
call %UPDONE% "h2d2_krnl - tg" "h2d2_krnl - tg.xml" %SRCDIR%\tg*.f*

call %UPDONE% "h2d2_krnl - ic" "h2d2_krnl - ic.xml" %SRCDIR%\ic*.f*

pause