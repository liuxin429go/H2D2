//************************************************************************
// H2D2 - External declaration of public symbols
// Module: slvr_mumps
// Entry point: extern "C" void fake_dll_slvr_mumps()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:05.285000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_MR_MMPS
F_PROT(IC_MR_MMPS_XEQCTR, ic_mr_mmps_xeqctr);
F_PROT(IC_MR_MMPS_XEQMTH, ic_mr_mmps_xeqmth);
F_PROT(IC_MR_MMPS_REQCLS, ic_mr_mmps_reqcls);
F_PROT(IC_MR_MMPS_REQHDL, ic_mr_mmps_reqhdl);
 
// ---  class MR_MMPS
F_PROT(MR_MMPS_000, mr_mmps_000);
F_PROT(MR_MMPS_999, mr_mmps_999);
F_PROT(MR_MMPS_CTR, mr_mmps_ctr);
F_PROT(MR_MMPS_DTR, mr_mmps_dtr);
F_PROT(MR_MMPS_INI, mr_mmps_ini);
F_PROT(MR_MMPS_RST, mr_mmps_rst);
F_PROT(MR_MMPS_REQHBASE, mr_mmps_reqhbase);
F_PROT(MR_MMPS_HVALIDE, mr_mmps_hvalide);
F_PROT(MR_MMPS_ESTDIRECTE, mr_mmps_estdirecte);
F_PROT(MR_MMPS_ASMMTX, mr_mmps_asmmtx);
F_PROT(MR_MMPS_FCTMTX, mr_mmps_fctmtx);
F_PROT(MR_MMPS_RESMTX, mr_mmps_resmtx);
F_PROT(MR_MMPS_XEQ, mr_mmps_xeq);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_slvr_mumps()
{
   static char libname[] = "slvr_mumps";
 
   // ---  class IC_MR_MMPS
   F_RGST(IC_MR_MMPS_XEQCTR, ic_mr_mmps_xeqctr, libname);
   F_RGST(IC_MR_MMPS_XEQMTH, ic_mr_mmps_xeqmth, libname);
   F_RGST(IC_MR_MMPS_REQCLS, ic_mr_mmps_reqcls, libname);
   F_RGST(IC_MR_MMPS_REQHDL, ic_mr_mmps_reqhdl, libname);
 
   // ---  class MR_MMPS
   F_RGST(MR_MMPS_000, mr_mmps_000, libname);
   F_RGST(MR_MMPS_999, mr_mmps_999, libname);
   F_RGST(MR_MMPS_CTR, mr_mmps_ctr, libname);
   F_RGST(MR_MMPS_DTR, mr_mmps_dtr, libname);
   F_RGST(MR_MMPS_INI, mr_mmps_ini, libname);
   F_RGST(MR_MMPS_RST, mr_mmps_rst, libname);
   F_RGST(MR_MMPS_REQHBASE, mr_mmps_reqhbase, libname);
   F_RGST(MR_MMPS_HVALIDE, mr_mmps_hvalide, libname);
   F_RGST(MR_MMPS_ESTDIRECTE, mr_mmps_estdirecte, libname);
   F_RGST(MR_MMPS_ASMMTX, mr_mmps_asmmtx, libname);
   F_RGST(MR_MMPS_FCTMTX, mr_mmps_fctmtx, libname);
   F_RGST(MR_MMPS_RESMTX, mr_mmps_resmtx, libname);
   F_RGST(MR_MMPS_XEQ, mr_mmps_xeq, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
