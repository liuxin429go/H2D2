C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MES_HLPPRGL
C
C Description:
C     Aide sur les propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MES_HLPPRGL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MES_HLPPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER I, IPL, IPC
C-----------------------------------------------------------------------

C---     APPEL LE PARENT
      CALL CD2D_BSE_HLPPRGL()

C---     IMPRESSION DU TITRE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_CINETIQUE_LUES:')
      CALL LOG_INCIND()

C---     IMPRESSION DE L'INFO
      I = CD2D_BSE_NPRGLL
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_GRAVITE')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_RHO_EAU')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_VISCO_CINE_EAU')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_MES_COHESIFS_OU_NON')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_W_SED_CONSTANTE')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_SIGMA_CRITIQUE_DEPO')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_SIGMA_CRITIQUE_ERO')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_D_MOYEN_SEDIMENTS')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_RHO_MOYEN_SEDIMENTS')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_CNST_SEDIMENTATION')
      I = I + 1
      IERR=CD2D_BSE_HLP1(I,'MSG_COEF_ERO')

      CALL LOG_DECIND()

      RETURN
      END

