C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************


C************************************************************************
C Sommaire: Assigne
C
C Description:
C     La fonction NS_MACRO3D_ASGCMN copie les valeurs du common externe
C     passés en paramètre sous la forme d'un vecteur sur les valeurs
C     du common local à la DLL.
C
C Entrée:
C     EG_KA       Les valeurs du common de l'élément géométrique
C     EA_KA       Les valeurs du common de l'élément d'approximation
C
C Sortie:
C
C Notes:
C     les common(s) sont privés au DLL, il faut donc propager les
C     changement entre l'externe et l'interne.
C************************************************************************
      FUNCTION NS_MACRO3D_ASGCMN(EG_KA, EA_KA, EA_VA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_ASGCMN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NS_MACRO3D_ASGCMN
      INTEGER EG_KA(*)
      INTEGER EA_KA(*)
      REAL*8  EA_VA(*)

      INCLUDE 'err.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER I
C-----------------------------------------------------------------------

      DO I=1, EG_CMMN_KA_DIM
         EG_CMMN_KA(I) = EG_KA(I)
      ENDDO

      DO I=1, LM_CMMN_KA_DIM
         LM_CMMN_KA(I) = EA_KA(I)
      ENDDO

      DO I=1, LM_CMMN_VA_DIM
         LM_CMMN_VA(I) = EA_VA(I)
      ENDDO

      NS_MACRO3D_ASGCMN = ERR_TYP()
      RETURN
      END
