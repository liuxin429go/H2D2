C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : NS_MACRO3D_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes: Contrôles
C        indice, propriété, test
C        1, Gravité,  != 0 est entre les bornes maximales et minimales
C        2, Latitude, entre -90 et 90
C        3, Viscosité laminaire,  (voir si on conserve) est positive mais
C                                                  inférieure à visoMax
C        4, longueur de mélange, (voir si on conserve) est positive mais
C                                                   inférieure à lmMax
C        6, borne inférieur de la viscosité, (voir si on conserve) est positive
C        7, borne supérieure de la viscosité, (voir si on conserve) est positive
C        6, borne inférieure de la viscosité < 7, borne supérieure de la viscosité
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      REAL*8  GRAMAX, GRAMIN
      PARAMETER (GRAMAX=9.86605398D0)
      PARAMETER (GRAMIN=9.75258282D0)

      REAL*8  VISCOMAX
      PARAMETER (VISCOMAX=1.0D0)
      REAL*8  LMMAX
      PARAMETER (LMMAX=10.0D0)
C-----------------------------------------------------------------------

!      SV2D_GRAVITE         = VPRGL( 1)
!      SV2D_CORIOLIS        = VPRGL(19)
!      SV2D_VISCO_CST       = VPRGL( 3)
!      SV2D_VISCO_LM        = VPRGL( 4)
!      SV2D_VISCO_SMGO      = VPRGL( 5)
!      SV2D_VISCO_BINF      = VPRGL( 6)
!      SV2D_VISCO_BSUP      = VPRGL( 7)
!      SV2D_DECOU_MAN       = VPRGL( 8)
!      SV2D_DECOU_PORO      = VPRGL( 9)
!      SV2D_DECOU_HMIN      = VPRGL(10)
!      SV2D_DECOU_CVCT_CMULT= VPRGL(11)
!      SV2D_DECOU_DIFF_NU   = VPRGL(12)
!      SV2D_DECOU_DARCY_NU  = VPRGL(13)
!      SV2D_STABI_PECLET    = VPRGL(14)
!      SV2D_STABI_DARCY     = VPRGL(15)
!      SV2D_STABI_LAPIDUS   = VPRGL(16)
!      SV2D_CMULT_CON       = VPRGL(17)
!      SV2D_CMULT_GRA       = VPRGL(18)
!      SV2D_CMULT_MAN       = VPRGL(19)
!      SV2D_CMULT_VENT      = VPRGL(20)
!      SV2D_CMULT_INTGCTR   = VPRGL(21)
!      SV2D_PNUMR_PENALITE  = VPRGL(22)
!      SV2D_PNUMR_DELPRT    = VPRGL(23)

C---     CONTROLES
      IF (VPRGL(1). LT. ZERO) THEN
         WRITE(ERR_BUF,'(A,1PE14.6E3)') 'ERR_VALEUR_INVALIDE:',VPRGL(2)
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         CALL ERR_AJT('MSG_GRAVITE')
      ELSEIF (VPRGL(1). LT. GRAMIN .OR. VPRGL(1). GT. GRAMAX) THEN
         WRITE(LOG_BUF,'(A,1PE14.6E3,A,1PE14.6E3)')
     &      'MSG_GRAVITE_NON_TERRESTRE: ', GRAMIN,' <= g <= ',GRAMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     CORRIOLIS
      IF (VPRGL(2) .LT. -90 .OR. VPRGL(2) .GT. 90) THEN
         WRITE(ERR_BUF,'(A,1PE14.6E3)') 'ERR_VALEUR_INVALIDE:',VPRGL(2)
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         CALL ERR_AJT('MSG_LATITUDE')
      ENDIF

C---     VISCOSITÉ LAMINAIRE
      IF (VPRGL(3) .LT. ZERO) THEN
         WRITE(ERR_BUF,'(A,1PE14.6E3)') 'ERR_VALEUR_INVALIDE:',VPRGL(3)
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         CALL ERR_AJT('MSG_VISCO_LAMINAIRE')
      ELSEIF (VPRGL(3) .GT. VISCOMAX) THEN
         WRITE(LOG_BUF,'(2A,1PE14.6E3)')
     &      'MSG_VISCO_NON_PHYSIQUE: ', 'nu <= ',VISCOMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     LONGUEUR DE MÉLANGE
      IF (VPRGL(4) .LT. ZERO) THEN
         WRITE(ERR_BUF,'(A,1PE14.6E3)') 'ERR_VALEUR_INVALIDE:',VPRGL(4)
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         CALL ERR_AJT('MSG_LONGUEUR_DE_MELANGE')
      ELSEIF (VPRGL(4) .GT. LMMAX) THEN
         WRITE(LOG_BUF,'(2A,1PE14.6E3)')
     &      'MSG_LM_PEU_REALISTE: ', 'lm <= ', LMMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

!C---     LONGUEUR DE MÉLANGE - DETJ
!      IF (VPRGL(5) .LT. ZERO) THEN
!         WRITE(ERR_BUF,'(A,1PE14.6E3)') 'ERR_VALEUR_INVALIDE:',VPRGL(5)
!         CALL ERR_ASG(ERR_ERR, ERR_BUF)
!      ELSEIF (VPRGL(5) .GT. LMMAX) THEN
!         WRITE(LOG_BUF,'(2A,1PE14.6E3)')
!     &      'MSG_LM_PEU_REALISTE: ', 'lm <= ', LMMAX
!         CALL LOG_ECRIS(LOG_BUF)
!      ENDIF

C---     BORNE INFERIEURE
      IF (VPRGL(6) .LT. ZERO) THEN
         WRITE(ERR_BUF,'(A,1PE14.6E3)') 'ERR_VALEUR_INVALIDE:',VPRGL(6)
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         CALL ERR_AJT('MSG_VISCO_BORNE_INF')
      ENDIF

C---     BORNE SUPERIEURE
      IF (VPRGL(7) .LT. ZERO) THEN
         WRITE(ERR_BUF,'(A,1PE14.6E3)') 'ERR_VALEUR_INVALIDE:',VPRGL(7)
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         CALL ERR_AJT('MSG_VISCO_BORNE_SUP')
      ENDIF
      IF (VPRGL(7). LT. VPRGL(6)) THEN
         WRITE(ERR_BUF, *) 'ERR_VISCOSITE_INVALIDE'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ENDIF

!C---     VÉRIFIE LA COHÉRENCE DES PROPRIÉTÉS GLOBALES
!C     SI CLM .NE. 0 => CLMDET = 0 ET VISCST = 0
!C     SI CLMDET = 1 => CLM = 0 ET VISCST = 0
!      DO I = 3,16
!         IF (VPRGL(I) .LT. ZERO) THEN
!            WRITE(LOG_BUF, '(A, 1PE14.6E3))')
!               'MSG_VALEUR_NEGATIVE_CORIGEE_A_ZERO, ' : ', VPRGL(3)
!            CALL LOG_ECRIS(LOG_BUF)
!            VPRGL(I) = ZERO
!         ENDIF
!      ENDIF
!      IF (VPRGL( 9) .GT. UN  ) VPRGL( 9) = UN
!      IF (VPRGL(10) .GT. UN  ) VPRGL(10) = UN
!      IF (VPRGL(11) .EQ. ZERO) VPRGL(11) = GRAND
!      IF (VPRGL(13) .LE. ZERO) VPRGL(13) = 1.0D-03
!
!C---     CONTROLES
!      IF (VPRGL( 1). LT. GRAMIN .OR. VPRGL( 1). GT. GRAMAX) THEN
!         WRITE(ERR_BUF,1002)
!     &      'ERR_GRAVITE_INVALIDE: ', GRAMIN,' <= g <= ',GRAMAX
!         CALL ERR_ASG(ERR_ERR, ERR_BUF)
!      ENDIF
!      IF (VPRGL(7). LT. VPRGL(6)) THEN
!         WRITE(ERR_BUF, *) 'ERR_VISCOSITE_INVALIDE'
!         CALL ERR_ASG(ERR_ERR, ERR_BUF)
!      ENDIF


!FTNCHEK : Label defined but not used
C---------------------------------------------------------------
1002  FORMAT(A,1PE14.6E3,A,1PE14.6E3)
C---------------------------------------------------------------
      IERR = ERR_TYP()
      RETURN
      END
