C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER NS_MACRO3D_REQNFN
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction NS_MACRO3D_REQNFN retourne le nom de la fonction
C     associé au code demandé. Le nom combine le nom de la DLL et
C     de la fonction sous la forme "nomFonction@nomDll".
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     KFNC     Nom de la fonction stocké dans une table INTEGER
C
C Notes: fonctions à spécialiser
C************************************************************************
      FUNCTION NS_MACRO3D_REQNFN(KFNC, IFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_REQNFN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NS_MACRO3D_REQNFN
      INTEGER KFNC(*)
      !INTEGER LFNC
      INTEGER IFNC

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'

      INTEGER        I
      INTEGER        JF(16)
      CHARACTER*(64) NF
      EQUIVALENCE (NF, JF)
C-----------------------------------------------------------------------
      IF (IFNC .EQ. EA_FUNC_ASMF) THEN
            NF = 'NS_MACRO3D_ASMF@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCDLIB) THEN
            NF = 'NS_MACRO3D_CLCDLIB@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCDLIB) THEN
           NF = 'NS_MACRO3D_PRCDLIB@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRGL) THEN
           NF = 'NS_MACRO3D_PRCPRGL@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRNO) THEN
           NF = 'NS_MACRO3D_PRCPRNO@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPREV) THEN
           NF = 'NS_MACRO3D_PRCPREV@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRES) THEN
           NF = 'NS_MACRO3D_PRCPRES@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCSOLC) THEN
           NF = 'NS_MACRO3D_PRCSOLC@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCSOLR) THEN
           NF = 'NS_MACRO3D_PRCSOLR@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCCLIM) THEN
           NF = 'NS_MACRO3D_PRCCLIM@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_REQNFN) THEN
           NF = 'NS_MACRO3D_REQNFN@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMK) THEN
           NF = 'NS_MACRO3D_ASMK@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKT) THEN
           NF = 'NS_MACRO3D_ASMKT@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKU) THEN
           NF = 'NS_MACRO3D_ASMKU@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCPRES) THEN
          NF = 'NS_MACRO3D_CLCPRES@ns_macro3d'
          CALL LOG_ECRIS('FONCTION CLCPRES A IMPLANTER POUR NS_MACRO3D')
      ELSEIF (IFNC .EQ. EA_FUNC_CLCPREV) THEN
           NF = 'NS_MACRO3D_CLCPREV@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCPRNO) THEN
           NF = 'NS_MACRO3D_CLCPRNO@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCCLIM) THEN
           NF = 'NS_MACRO3D_CLCCLIM@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNCLIM) THEN
           NF = 'NS_MACRO3D_PRNCLIM@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRGL) THEN
           NF = 'NS_MACRO3D_PRNPRGL@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRNO) THEN
           NF = 'NS_MACRO3D_PRNPRNO@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLCLIM) THEN
           NF = 'NS_MACRO3D_PSLCLIM@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPREV) THEN
           NF = 'NS_MACRO3D_PSLPREV@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPRGL) THEN
           NF = 'NS_MACRO3D_PSLPRGL@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPRNO) THEN
           NF = 'NS_MACRO3D_PSLPRNO@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLSOLC) THEN
           NF = 'NS_MACRO3D_PSLSOLC@ns_macro3d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLSOLR) THEN
           NF = 'NS_MACRO3D_PSLSOLR@ns_macro3d'
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_CODE_FNCT_INVALIDE')
      ENDIF

      IF (ERR_GOOD()) THEN
         DO I=1,16
            KFNC(I) = JF(I)
         ENDDO
      ENDIF

!FTNCHEK LABEL DEFINED BUT NOT USED
9999  CONTINUE
      NS_MACRO3D_REQNFN = ERR_TYP()
      RETURN
      END


