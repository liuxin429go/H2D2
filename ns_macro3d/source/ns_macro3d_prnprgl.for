C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: NS_MACRO3D_PRNPRGL
C
C Description:
C     IMPRESSION DE PROPRIETES GLOBALES
C     EQUATION : NS 3D Hydrostatique
C     ELEMENT  : P12L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_PRNPRGL(NPRGL, VPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PRNPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INTEGER NPRGL
      REAL*8  VPRGL(NPRGL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I
      INTEGER IERR
      INTEGER NS_MACRO3D_PRN1
C-----------------------------------------------------------------------

      I = 0

C---     IMPRIME ENTETE
      CALL LOG_TODO('NS_MACRO3D_PRNPRGL   attention pas modifie')
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ST_VENANT_LUES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     GROUPE DES PROP PHYSIQUES
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PHYSIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_GRAVITE', VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_LATITUDE', VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_VISCOSITE_MOLECULAIRE',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_LONGUEUR_MELANGE',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,
     &                     'MSG_COEF_SMAGORINSKY',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_BORNE_INF_VISCOSITE',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_BORNE_SUP_VISCOSITE',VPRGL(I))

C---     GROUPE DES PROP DU DECOUVREMENT
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_DECOUVREMENT:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_MANNING_DECOUVREMENT',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_POROSITE_DECOUVREMENT',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_PROF_MIN_DECOUVREMENT',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_PROF_COEF_CONVECTION',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_PROF_NU_DIFFUSION',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_PROF_NU_DARCY',VPRGL(I))

C---     GROUPE DES PROP DE STABILISATION
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_STABILISATION:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_PECLET',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_SURFACE_LIBRE_DARCY',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_SURFACE_LIBRE_LAPIDUS',VPRGL(I))

C---     GROUPE DES PROP D'ACTIVATION-DESACTIVATION DES TERMES
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ACTIVATION_TERMES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_CONVECTION',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_GRAVITE',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_FROTTEMENT_FOND',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_FROTTEMENT_VENT',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_INTGRL_CONTOUR',VPRGL(I))

C---     GROUPE DES PROP NUMÉRIQUES
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PARAM_NUMERIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_PENALITE',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_PERTURBATION_KT',VPRGL(I))
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_COEF_RELAXATION_KT',VPRGL(I))

      CALL LOG_DECIND()
      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_PRGL_ST_VENANT_CALCULEES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=NS_MACRO3D_PRN1(I,'MSG_FORCE_CORIOLIS',VPRGL(I))

      RETURN
      END

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire NS_MACRO3D_PRN1 imprime une seule
C     propriété globale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PRN1(IP, TXT, V)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INTEGER       NS_MACRO3D_PRN1
      INTEGER       IP
      CHARACTER*(*) TXT
      REAL*8        V

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,2A,1PE14.6E3)') IP, TXT, '#<38>#=', V
      CALL LOG_ECRIS(LOG_BUF)

      NS_MACRO3D_PRN1 = ERR_TYP()
      RETURN
      END

