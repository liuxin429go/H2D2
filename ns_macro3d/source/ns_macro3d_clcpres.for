C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  NS_MACRO3D_CLCPRES
C
C Description:
C     CALCUL DES PROPRIÉTÉS ÉLÉMENTAIRES DES ELEMENTS DE SURFACE
C
C
C Entrée:   VCORG : Table des coordonnées globales
C           KNGV : Table des connectivitées des éléments de volume
C           KNGS : Table des connectivités des éléments de surface
C           VDJV : Métriques
C           VDJS : Métriques de surface
C           VPRGL : Table des propriétés globales
C           VPRNO : Table des propriétés nodales
C           VPREV : Table des propriétés élémentaires de volume
C           VDLG : Table globale des degrés de liberté
C
C Sortie:   VPRES
C           IERR
C
C Notes: Théoriquement, les PRES devraient servir pour les CL. Or,
C        dans SV2D, je n'ai pas trouvé où elles étaient utilisée
C        cette fonction est donc mise en veille temporairement.
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCPRES(VCORG,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VDLG,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CLCPRES
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV(LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES(LM_CMMN_NPRES, EG_CMMN_NELS)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'

    ! INTEGER IES, IEV
      INTEGER NS1
    ! INTEGER NO1, NO2, NO3, NO4, NO5, NO6
    ! INTEGER IET3_1, IET3_2
    !  REAL*8  VKX, VEX, VKY, VEY
    !  REAL*8  UN_DT3
    !  REAL*8  U1, V1, U2, V2, U3, V3, U4, V4, U5, V5, U6, V6
    !  REAL*8  UX1, UX2, UX3, UY1, UY2, UY3
      REAL*8  VX1, VX2, VX3, VY1, VY2, VY3
    !  REAL*8  VIS1, VIS2, VIS3
    ! REAL*8  TAUXX(3), TAUXY(3), TAUYY(3)
C-----------------------------------------------------------------------

C=============================================================
C      CALL LOG_ECRIS('FONCTIONALITÉ NON CONTROLÉE')
C      CALL ERR_ASR(.FALSE.)
C=============================================================

!C----- BOUCLE SUR LES ELEMENTS
!C      =======================
!      DO 10 IES=1,EG_CMMN_NELS
!
!         NS1 = KNGS(1,IES)
!         IEV = KNGS(4,IES)
!
!C---        CONNECTIVITES DU T6
!         NO1 = KNGV(1,IEV)
!         NO2 = KNGV(2,IEV)
!         NO3 = KNGV(3,IEV)
!         NO4 = KNGV(4,IEV)
!         NO5 = KNGV(5,IEV)
!         NO6 = KNGV(6,IEV)
!
!C---        METRIQUES DES T3 DU T6L
!         VKX    = UN_2*VDJV(1,IEV)
!         VEX    = UN_2*VDJV(2,IEV)
!         VKY    = UN_2*VDJV(3,IEV)
!         VEY    = UN_2*VDJV(4,IEV)
!         UN_DT3 = UN_4/VDJV(5,IEV)
!
!C---        VARIABLES NODALES
!         U1 = VPRNO(7,NO1)
!         V1 = VPRNO(8,NO1)
!         U2 = VPRNO(7,NO2)
!         V2 = VPRNO(8,NO2)
!         U3 = VPRNO(7,NO3)
!         V3 = VPRNO(8,NO3)
!         U4 = VPRNO(7,NO4)
!         V4 = VPRNO(8,NO4)
!         U5 = VPRNO(7,NO5)
!         V5 = VPRNO(8,NO5)
!         U6 = VPRNO(7,NO6)
!         V6 = VPRNO(8,NO6)
!
!C---        DÉRIVÉ EN X DE U SUR LES 3 T3 EXTERNES
!         UX1 = VKX*(U2-U1)+VEX*(U6-U1)
!         UX2 = VKX*(U3-U2)+VEX*(U4-U2)
!         UX3 = VKX*(U4-U6)+VEX*(U5-U6)
!
!C---        DÉRIVÉ EN Y DE U SUR LES 3 T3 EXTERNES
!         UY1 = VKY*(U2-U1)+VEY*(U6-U1)
!         UY2 = VKY*(U3-U2)+VEY*(U4-U2)
!         UY3 = VKY*(U4-U6)+VEY*(U5-U6)
!
!C---        DÉRIVÉ EN X DE V SUR LES 3 T3 EXTERNES
!         VX1 = VKX*(V2-V1)+VEX*(V6-V1)
!         VX2 = VKX*(V3-V2)+VEX*(V4-V2)
!         VX3 = VKX*(V4-V6)+VEX*(V5-V6)
!
!C---        DÉRIVÉ EN Y DE V SUR LES 3 T3 EXTERNES
!         VY1 = VKY*(V2-V1)+VEY*(V6-V1)
!         VY2 = VKY*(V3-V2)+VEY*(V4-V2)
!         VY3 = VKY*(V4-V6)+VEY*(V5-V6)
!
!C---        VISCOSITÉ !bizarre d'utiliser juste 5, 6, et 7
!         VIS1 = VPREV(5,IEV)
!         VIS2 = VPREV(6,IEV)
!         VIS3 = VPREV(7,IEV)
!
!C---        TAUXX
!         TAUXX(1) = UX1*VIS1*DEUX
!         TAUXX(2) = UX2*VIS2*DEUX
!         TAUXX(3) = UX3*VIS3*DEUX
!
!C---        TAUXY
!         TAUXY(1) = (UY1 + VX1)*VIS1
!         TAUXY(2) = (UY2 + VX2)*VIS2
!         TAUXY(3) = (UY3 + VX3)*VIS3
!
!C---        TAUYY
!         TAUYY(1) = VY1*VIS1*DEUX
!         TAUYY(2) = VY2*VIS2*DEUX
!         TAUYY(3) = VY3*VIS3*DEUX
!
!C---        SOUS-ELEMENTS
!         IF (NS1 .EQ. NO1) THEN
!            IET3_1 = 1
!         ELSEIF (NS1 .EQ. NO3) THEN
!            IET3_1 = 2
!         ELSEIF (NS1 .EQ. NO5) THEN
!            IET3_1 = 3
!         ELSE
!            CALL ERR_ASG(ERR_FTL, 'ERR_CONNECTIVITES_INVALIDES')
!         ENDIF
!         IET3_2 = IET3_1 + 1
!         IF (IET3_2 .EQ. 4) IET3_2 = 1
!
!C---        CONTRAINTES SUR LES DEUX SOUS-ELEMENTS
!         VPRES(1,IES) = TAUXX(IET3_1)*UN_DT3
!         VPRES(2,IES) = TAUYY(IET3_1)*UN_DT3
!         VPRES(3,IES) = TAUXY(IET3_1)*UN_DT3
!         VPRES(4,IES) = TAUXX(IET3_2)*UN_DT3
!         VPRES(5,IES) = TAUYY(IET3_2)*UN_DT3
!         VPRES(6,IES) = TAUXY(IET3_2)*UN_DT3
!
!10    CONTINUE

      IERR = ERR_TYP()
      RETURN
      END

