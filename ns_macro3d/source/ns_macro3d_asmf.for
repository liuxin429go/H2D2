C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  NS_MACRO3D_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:LM_CMMN_NDLN doit être égal à NSOLC et NSOLR
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMF (  VCORG,
     &                              KLOCN,
     &                              KNGV,
     &                              KNGS,
     &                              VDJV,
     &                              VDJS,
     &                              VPRGL,
     &                              VPRNO,
     &                              VPREV,
     &                              VPRES, !sortie
     &                              VSOLC,
     &                              VSOLR,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              KDIMP,
     &                              VDIMP,
     &                              KEIMP,
     &                              VDLG,
     &                              VFG   )
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV) ! sortie
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Pré-traitement avant calcul
      CALL NS_MACRO3D_CLC( VCORG,
     &                     KNGV,
     &                     KNGS,
     &                     VDJV,
     &                     VDJS,
     &                     VPRGL,
     &                     VPRNO,
     &                     VPREV,
     &                     VPRES, !sortie
     &                     KCLCND,
     &                     VCLCNV,
     &                     KCLLIM,
     &                     KCLNOD,
     &                     KCLELE,
     &                     KDIMP,
     &                     VDIMP,
     &                     KEIMP,
     &                     VDLG,
     &                     IERR  )

C---  Sollicitations concentrées
      IERR = SP_ELEM_ASMFE(LM_CMMN_NDLL, KLOCN, VSOLC, VFG)

C---  Contribution de volume
      CALL NS_MACRO3D_ASMF_V( VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES, ! pas utilisé
     &                        VSOLC,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        VFG      )

C---     Contribution de surface (DÉJÀ EN COMMENTAIRES DANS Sv2d)
!      CALL NS_MACRO3D_ASMF_S(VCORG,
!     &                     KLOCN,
!     &                     KNGV,
!     &                     KNGS,
!     &                     VDJV,
!     &                     VDJS,
!     &                     VPRGL,
!     &                     VPRNO,
!     &                     VPREV,
!     &                     VPRES,
!     &                     VSOLC,
!     &                     VSOLR,
!     &                     KCLCND,
!     &                     VCLCNV,
!     &                     KCLLIM,
!     &                     KCLNOD,
!     &                     KCLELE,
!     &                     KDIMP,
!     &                     VDIMP,
!     &                     KEIMP,
!     &                     VDLG,
!     &                     VFG)

C---     Contribution des conditions limites ?? OU PLUTOT TRANSFERT DU COMMON
      CALL NS_MACRO3D_ASMF_C( VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES, ! réassignation seulement
     &                        VSOLC,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        VFG)

      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_ASMF_V
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMF_V( VCORG,
     &                              KLOCN,
     &                              KNGV,
     &                              KNGS, !pas utilisé
     &                              VDJV,
     &                              VDJS, ! pas utilisé
     &                              VPRGL,
     &                              VPRNO, ! pas utilisé
     &                              VPREV,
     &                              VPRES, ! pas utilisé
     &                              VSOLC, ! pas utilisé
     &                              VSOLR,
     &                              KCLCND, !pas utilisé
     &                              VCLCNV, !pas utilisé
     &                              KCLLIM, ! pas utilisé
     &                              KCLNOD, ! pas utilisé
     &                              KCLELE, !pas utilisé
     &                              KDIMP,  ! pas utilisé
     &                              VDIMP, ! pas utilisé
     &                              KEIMP,  ! pas utilisé
     &                              VDLG, ! pas utilisé
     &                              VFG   )

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV) ! pas utilisé
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'

      INTEGER IERR
      INTEGER IC, IE, IN, II, NO,IDL
      INTEGER KLOCE(5, 6)

    !  INTEGER NDJV_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      INTEGER NDIM_LCL
      INTEGER NNELV_LCL

    !  PARAMETER (NDJV_LCL = 5)
      PARAMETER (NDLE_LCL = 30)
      PARAMETER (NPRN_LCL = 14)
      PARAMETER (NDIM_LCL = 2)
      PARAMETER (NNELV_LCL = 6)


      INTEGER KNE(6)
 !     REAL*8  VDLE(NDLE_LCL)
      REAL*8  VSOLE(NDLE_LCL)
      REAL*8  VCORE(NDIM_LCL, NNELV_LCL)
      REAL*8  VPRN(NPRN_LCL, 6)
      REAL*8  VFE(5, NNELV_LCL) ! 5 : NDL 6:NDN

C---  VARIABLES ASMF ANCIEN

!      INTEGER IERR
   !   INTEGER IC, IE, IN
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
    !  INTEGER KLOCE(5, 6)
      REAL*8  SUS1, SUS2, SUS3, SUS4, SUS5, SUS6
      REAL*8  SUF1, SUF2, SUF3, SUF4, SUF5, SUF6
      REAL*8  SVF1, SVF2, SVF3, SVF4, SVF5, SVF6
      REAL*8  SVS1, SVS2, SVS3, SVS4, SVS5, SVS6
      REAL*8  SH1,        SH3,        SH5

      REAL*8  SUFE1, SUFE2, SUFE3, SUFE4
      REAL*8  SUSE1, SUSE2, SUSE3, SUSE4
      REAL*8  SVFE1, SVFE2, SVFE3, SVFE4
      REAL*8  SVSE1, SVSE2, SVSE3, SVSE4
      REAL*8  SHE
      REAL*8  SOLR_T6, SOLR_P6
      REAL*8  VFEA(5, 6) ! 5 : NDL 6:NDN
      REAL*8  UN_576
      PARAMETER (UN_576 = 1.7361111111111111111111111111111d-03)
C---  FIN VARIABLES ASMF ANCIEN
C---  VARIABLES ASMFSV2D
      REAL*8  SU1, SU2, SU3, SU4, SU5, SU6
      REAL*8  SV1, SV2, SV3, SV4, SV5, SV6
!      REAL*8  SH1,      SH3,      SH5
      REAL*8  SUE1, SUE2, SUE3, SUE4
      REAL*8  SVE1, SVE2, SVE3, SVE4
!      REAL*8  SHE
      REAL*8  SOLR_T3!, SOLR_T6
      REAL*8  VFESV2D(3, 6)
C---  FIN VARIABLES ASMFSV2D
      real*8 test


C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .EQ. 5)
D     CALL ERR_PRE(EG_CMMN_NCELV.EQ. 6)
C      NDLE
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN, II, NO, NO1, NO2, NO3, NO4, NO5, NO6,
!$omp& SOLR_T3, SOLR_T6, SOLR_P6, SUS1, SUS2, SUS3, SUS4, SUS5, SUS6,
!$omp& SUF1, SUF2, SUF3, SUF4, SUF5, SUF6, SVF1, SVF2, SVF3, SVF4, SVF5,
!$omp& SVF6, SVS1, SVS2, SVS3, SVS4, SVS5, SVS6, SH1, SH3, SH5, SUFE1,
!$omp& SUFE2, SUFE3, SUFE4, SUSE1, SUSE2, SUSE3, SUSE4, SVFE1, SVFE2,
!$omp& SVFE3, SVFE4, SVSE1, SVSE2, SVSE3, SVSE4, SHE, SU1, SU2, SU3,
!$omp& SU4, SU5, SU6, SV1, SV2, SV3, SV4, SV5, SV6, SUE1, SUE2, SUE3,
!$omp& SUE4, SVE1, SVE2, SVE3, SVE4, VFESV2D, VFEA, VSOLE, VCORE, VPRN,
!$omp& KNE)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        TRANSFERT DES CONNECTIVITÉS ÉLÉMENTAIRES
         DO IN=1,EG_CMMN_NNELV
            KNE(IN) = KNGV(IN,IE)
         ENDDO

C---        TRANSFERT DES SOLR ET DES COORDONNÉEs
         II=0
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(EG_CMMN_NDIM,  VCORG(1,NO), 1, VCORE(1,IN), 1)
            DO IDL = 1, 5
               VSOLE(II+IDL) = VSOLR(IDL,NO)
            END DO
            II = II+5
         ENDDO

C---        TRANSFERT DES PRNO (POUR zF)
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(1,IN), 1)
         ENDDO

C---  INITIALISATION DE VFE
      CALL DINIT(5*NNELV_LCL,ZERO,VFE,1)

C---     CALCUL DE LA CONTRIBUTION ÉLÉMENTAIRE À LA MATRICE MASSE
         CALL NS_MACRO3D_ASFE_V (   VFE,
     &                              VCORE,
     &                              VDJV(1,IE),
     &                              VPRGL,
     &                              VPRN,
     &                              VPREV(1,IE),
     &                              VSOLE)


C---     POUR FINS DE COMPARAISON ASMF ANCIEN
C---       CONNECTIVITES
D         NO1 = KNGV(1, IE)
D         NO2 = KNGV(2, IE)
D         NO3 = KNGV(3, IE)
D         NO4 = KNGV(4, IE)
D         NO5 = KNGV(5, IE)
D         NO6 = KNGV(6, IE)
D
C---       CONNECTIVITES (À MODIFIER NS_MACRO3D)
D         SOLR_T6 = UN_24*VDJV(5,IE)!*UN-2 ?? vérifierUN_24*VDJV(5,IE)
D         SOLR_P6 = UN_576*VDJV(5,IE) ! UN_72*detJP6 = UN_72*detJP12/4 = UN_72*UN_4*(detJT6*P/2) = UN_576*P*detJT6 = UN_576*P*VDJE(5)
D                                       !UN_576*P*VDJE(5). Pour le test, on considère P constant et vallant 1 (comme sv2d) même si en réalité
D                                       !P peut varier.
C---       SOLLICITATIONS NODALES
D         SUF1 = SOLR_P6 * VSOLR(1,NO1)
D         SUS1 = SOLR_P6 * VSOLR(2,NO1)
D         SVF1 = SOLR_P6 * VSOLR(3,NO1)
D         SVS1 = SOLR_P6 * VSOLR(4,NO1)
D         SH1 = SOLR_T6  * VSOLR(5,NO1)
D
D         SUF2 = SOLR_P6 * VSOLR(1,NO2)
D         SUS2 = SOLR_P6 * VSOLR(2,NO2)
D         SVF2 = SOLR_P6 * VSOLR(3,NO2)
D         SVS2 = SOLR_P6 * VSOLR(4,NO2)
D
D         SUF3 = SOLR_P6 * VSOLR(1,NO3)
D         SUS3 = SOLR_P6 * VSOLR(2,NO3)
D         SVF3 = SOLR_P6 * VSOLR(3,NO3)
D         SVS3 = SOLR_P6 * VSOLR(4,NO3)
D         SH3 = SOLR_T6  * VSOLR(5,NO3)
D
D         SUF4 = SOLR_P6 * VSOLR(1,NO4)
D         SUS4 = SOLR_P6 * VSOLR(2,NO4)
D         SVF4 = SOLR_P6 * VSOLR(3,NO4)
D         SVS4 = SOLR_P6 * VSOLR(4,NO4)
D
D         SUF5 = SOLR_P6 * VSOLR(1,NO5)
D         SUS5 = SOLR_P6 * VSOLR(2,NO5)
D         SVF5 = SOLR_P6 * VSOLR(3,NO5)
D         SVS5 = SOLR_P6 * VSOLR(4,NO5)
D         SH5  = SOLR_T6 * VSOLR(5,NO5)
D
D         SUF6 = SOLR_P6 * VSOLR(1,NO6)
D         SUS6 = SOLR_P6 * VSOLR(2,NO6)
D         SVF6 = SOLR_P6 * VSOLR(3,NO6)
D         SVS6 = SOLR_P6 * VSOLR(4,NO6)
D
C---       SOMMATIONS POUR CHAQUE SOUS-ELEMENT (vérifier pour nsmacro...)
D         SUFE1 = SUF1 + SUF2 + SUF6
D         SUSE1 = SUS1 + SUS2 + SUS6
D         SVFE1 = SVF1 + SVF2 + SVF6
D         SVSE1 = SVS1 + SVS2 + SVS6
D
D         SUFE2 = SUF2 + SUF3 + SUF4
D         SUSE2 = SUS2 + SUS3 + SUS4
D         SVFE2 = SVF2 + SVF3 + SVF4
D         SVSE2 = SVS2 + SVS3 + SVS4
D
D         SUFE3 = SUF6 + SUF4 + SUF5
D         SUSE3 = SUS6 + SUS4 + SUS5
D         SVFE3 = SVF6 + SVF4 + SVF5
D         SVSE3 = SVS6 + SVS4 + SVS5
D
D         SUFE4 = SUF4 + SUF6 + SUF2
D         SUSE4 = SUS4 + SUS6 + SUS2
D         SVFE4 = SVF4 + SVF6 + SVF2
D         SVSE4 = SVS4 + SVS6 + SVS2
D
D         SHE  = SH1 + SH3 + SH5
D
C---       CONTRIBUTIONS
D         VFEA(1, 1) = DEUX*(SUFE1+SUF1) + SUSE1+SUS1
D         VFEA(2, 1) = DEUX*(SUSE1+SUS1) + SUFE1+SUF1
D         VFEA(3, 1) = DEUX*(SVFE1+SVF1) + SVSE1+SVS1
D         VFEA(4, 1) = DEUX*(SVSE1+SVS1) + SVFE1+SVF1
D         VFEA(5, 1) = SHE +SH1
D
D         VFEA(1, 2) = DEUX*(SUFE1+SUF2) + SUSE1+SUS2 +
D    &               DEUX*(SUFE2+SUF2) + SUSE2+SUS2 +
D    &               DEUX*(SUFE4+SUF2) + SUSE4+SUS2
D         VFEA(2, 2) = SUFE1+SUF2 + DEUX*(SUSE1+SUS2) +
D    &               SUFE2+SUF2 + DEUX*(SUSE2+SUS2) +
D    &               SUFE4+SUF2 + DEUX*(SUSE4+SUS2)
D         VFEA(3, 2) = DEUX*(SVFE1+SVF2) + SVSE1+SVS2 +
D    &               DEUX*(SVFE2+SVF2) + SVSE2+SVS2 +
D    &               DEUX*(SVFE4+SVF2) + SVSE4+SVS2
D         VFEA(4, 2)=  SVFE1+SVF2 + DEUX*(SVSE1+SVS2) +
D    &               SVFE2+SVF2 + DEUX*(SVSE2+SVS2) +
D    &               SVFE4+SVF2 + DEUX*(SVSE4+SVS2)
D         VFEA(5, 2) = ZERO
D
D         VFEA(1, 3) = DEUX*(SUSE2+SUS3) + SUFE2+SUF3
D         VFEA(2, 3) = DEUX*(SUFE2+SUF3) + SUSE2+SUS3
D         VFEA(3, 3) = DEUX*(SVSE2+SVS3) + SVFE2+SVF3
D         VFEA(4, 3) = DEUX*(SVFE2+SVF3) + SVSE2+SVS3
D         VFEA(5, 3) = SHE +SH3
D
D         VFEA(1, 4) = DEUX*(SUFE2+SUF4) + SUSE2 + SUS4 +
D    &               DEUX*(SUFE3+SUF4) + SUSE3 + SUS4 +
D    &               DEUX*(SUFE4+SUF4) + SUSE4 + SUS4
D         VFEA(2, 4) = SUFE2+SUF4 + DEUX*(SUSE2 + SUS4) +
D    &               SUFE3+SUF4 + DEUX*(SUSE3 + SUS4) +
D    &               SUFE4+SUF4 + DEUX*(SUSE4 + SUS4)
D         VFEA(3, 4) = DEUX*(SVFE2+SVF4) + SVSE2 + SVS4 +
D    &               DEUX*(SVFE3+SVF4) + SVSE3 + SVS4 +
D    &               DEUX*(SVFE4+SVF4) + SVSE4 + SVS4
D         VFEA(4, 4) = SVFE2+SVF4 + DEUX*(SVSE2 + SVS4) +
D    &               SVFE3+SVF4 + DEUX*(SVSE3 + SVS4) +
D    &               SVFE4+SVF4 + DEUX*(SVSE4 + SVS4)
D         VFEA(5, 4) = ZERO
D
D         VFEA(1, 5) = DEUX*(SUFE3+SUF5) + SUSE3+SUS5
D         VFEA(2, 5) = DEUX*(SUSE3+SUS5) + SUFE3+SUF5
D         VFEA(3, 5) = DEUX*(SVFE3+SVF5) + SVSE3+SVS5
D         VFEA(4, 5) = DEUX*(SVSE3+SVS5) + SVFE3+SVF5
D         VFEA(5, 5) = SHE +SH5
D
D         VFEA(1, 6) = DEUX*(SUFE3+SUF6) + SUSE3+SUS6 +
D    &               DEUX*(SUFE1+SUF6) + SUSE1+SUS6 +
D    &               DEUX*(SUFE4+SUF6) + SUSE4+SUS6
D         VFEA(2, 6) = SUFE3+SUF6 + DEUX*(SUSE3+SUS6) +
D    &               SUFE1+SUF6 + DEUX*(SUSE1+SUS6) +
D    &               SUFE4+SUF6 + DEUX*(SUSE4+SUS6)
D         VFEA(3, 6) = DEUX*(SVFE3+SVF6) + SVSE3+SVS6 +
D    &               DEUX*(SVFE1+SVF6) + SVSE1+SVS6 +
D    &               DEUX*(SVFE4+SVF6) + SVSE4+SVS6
D         VFEA(4, 6) = SVFE3+SVF6 + DEUX*(SVSE3+SVS6) +
D    &               SVFE1+SVF6 + DEUX*(SVSE1+SVS6) +
D    &               SVFE4+SVF6 + DEUX*(SVSE4+SVS6)
D         VFEA(5, 6) = ZERO
D
D
C---     SV2D POUR TEST POUR TEST POUR TEST
C---       CONNECTIVITES
D         SOLR_T6 = UN_24*VDJV(5,IE)
D         SOLR_T3 = UN_4*SOLR_T6      ! 1/4 POUR LE DJ SUR LE T3
D
C---       SOLLICITATIONS NODALES
D         SU1 = SOLR_T3 * VSOLR(1,NO1)
D         SV1 = SOLR_T3 * VSOLR(3,NO1)
D         SH1 = SOLR_T6 * VSOLR(5,NO1)
D         SU2 = SOLR_T3 * VSOLR(1,NO2)
D         SV2 = SOLR_T3 * VSOLR(3,NO2)
D         SU3 = SOLR_T3 * VSOLR(1,NO3)
D         SV3 = SOLR_T3 * VSOLR(3,NO3)
D         SH3 = SOLR_T6 * VSOLR(5,NO3)
D         SU4 = SOLR_T3 * VSOLR(1,NO4)
D         SV4 = SOLR_T3 * VSOLR(3,NO4)
D         SU5 = SOLR_T3 * VSOLR(1,NO5)
D         SV5 = SOLR_T3 * VSOLR(3,NO5)
D         SH5 = SOLR_T6 * VSOLR(5,NO5)
D         SU6 = SOLR_T3 * VSOLR(1,NO6)
D         SV6 = SOLR_T3 * VSOLR(3,NO6)
D
C---       SOMMATIONS POUR CHAQUE SOUS-ELEMENT
D         SUE1 = SU1 + SU2 + SU6
D         SVE1 = SV1 + SV2 + SV6
D         SUE2 = SU2 + SU3 + SU4
D         SVE2 = SV2 + SV3 + SV4
D         SUE3 = SU6 + SU4 + SU5
D         SVE3 = SV6 + SV4 + SV5
D         SUE4 = SU4 + SU6 + SU2
D         SVE4 = SV4 + SV6 + SV2
D         SHE  = SH1 + SH3 + SH5
D
C---       CONTRIBUTIONS
D         VFESV2D(1, 1) = SUE1+SU1
D         VFESV2D(2, 1) = SVE1+SV1
D         VFESV2D(3, 1) = SHE +SH1
D
D         VFESV2D(1, 2) = SUE1+SU2 + SUE2+SU2 + SUE4+SU2
D         VFESV2D(2, 2) = SVE1+SV2 + SVE2+SV2 + SVE4+SV2
D         VFESV2D(3, 2) = ZERO
D
D         VFESV2D(1, 3) = SUE2+SU3
D        VFESV2D(2, 3) = SVE2+SV3
D        VFESV2D(3, 3) = SHE +SH3
D
D         VFESV2D(1, 4) = SUE2+SU4 + SUE3+SU4 + SUE4+SU4
D         VFESV2D(2, 4) = SVE2+SV4 + SVE3+SV4 + SVE4+SV4
D         VFESV2D(3, 4) = ZERO
D
D         VFESV2D(1, 5) = SUE3+SU5
D         VFESV2D(2, 5) = SVE3+SV5
D         VFESV2D(3, 5) = SHE +SH5

D         VFESV2D(1, 6) = SUE3+SU6 + SUE1+SU6 + SUE4+SU6
D         VFESV2D(2, 6) = SVE3+SV6 + SVE1+SV6 + SVE4+SV6
D         VFESV2D(3, 6) = ZERO
D
D        DO IN=1,6
D           CALL ERR_ASR( ABS(VFESV2D(1,IN) - VFEA(1,IN)- VFEA(1,IN))
d    &                                    .LE. PETIT)
D           CALL ERR_ASR( ABS(VFESV2D(1,IN) - VFEA(2,IN)- VFEA(2,IN))
d    &                                    .LE. PETIT)
D           CALL ERR_ASR( ABS(VFESV2D(2,IN) - VFEA(3,IN)- VFEA(3,IN))
d    &                                    .LE. PETIT)
D           CALL ERR_ASR( ABS(VFESV2D(2,IN) - VFEA(4,IN)- VFEA(4,IN))
d    &                                    .LE. PETIT)
D           CALL ERR_ASR( ABS(VFESV2D(3,IN) - VFEA(5,IN))
d    &                                    .LE. PETIT)
D        END DO
D
D
C---     SV2D POUR TEST POUR TEST POUR TEST


C---        TABLE KLOCE DE L'ÉLÉMENT T6L
         DO IN=1,EG_CMMN_NNELV
            KLOCE(1,IN) = KLOCN(1, KNGV(IN,IE))
            KLOCE(2,IN) = KLOCN(2, KNGV(IN,IE))
            KLOCE(3,IN) = KLOCN(3, KNGV(IN,IE))
            KLOCE(4,IN) = KLOCN(4, KNGV(IN,IE))
            KLOCE(5,IN) = KLOCN(5, KNGV(IN,IE))
         ENDDO
C---        ASSEMBLAGE DU VECTEUR GLOBAL
 !        IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)
!d         TEST = vFE(1,1) - VFEA(1,1)
!d         WRITE(LOG_BUF, '(A,1PE25.17E3,i6)') 'VFE VS VRAI SLR= ', test
!d        CALL LOG_ECRIS(LOG_BUF)
!d         TEST = vFE(1,2) - VFEA(1,2)
!d         WRITE(LOG_BUF, '(A,1PE25.17E3,i6)') 'VFE VS VRAI SLR= ', test
!!d        CALL LOG_ECRIS(LOG_BUF)
!d         TEST = vFE(1,3) - VFEA(1,3)
!d         WRITE(LOG_BUF, '(A,1PE25.17E3,i6)') 'VFE VS VRAI SLR= ', test
!d        CALL LOG_ECRIS(LOG_BUF)
!d         TEST = vFE(1,4) - VFEA(1,4)
!d         WRITE(LOG_BUF, '(A,1PE25.17E3,i6)') 'VFE VS VRAI SLR= ', test
!d        CALL LOG_ECRIS(LOG_BUF)
!d         TEST = vFE(1,5) - VFEA(1,5)
!d         WRITE(LOG_BUF, '(A,1PE25.17E3,i6)') 'VFE VS VRAI SLR= ', test
!!d        CALL LOG_ECRIS(LOG_BUF)
!d         TEST = vFE(1,6) - VFEA(1,6)
!d         WRITE(LOG_BUF, '(A,1PE25.17E3,i6)') 'VFE VS VRAI SLR= ', test
!d        CALL LOG_ECRIS(LOG_BUF)

c         WRITE(LOG_BUF, '(A,1PE25.17E3,i6)')
c     &                     , test
c          CALL LOG_ECRIS(LOG_BUF)
!     &                     , VFE(2,1) - VFEA(2,1)
!     &                     , VFE(3,1) - VFEA(3,1)
!     &                     , VFE(4,1) - VFEA(4,1)
!     &                     , VFE(5,1) - VFEA(5,1)
!     &                     , VFE(1,2) - VFEA(1,2)
!     &                     , VFE(2,2) - VFEA(2,2)
!     &                     , VFE(3,2) - VFEA(3,2)
!     &                     , VFE(4,2) - VFEA(4,2)
!     &                     , VFE(5,2) - VFEA(5,2)
!     &                     , VFE(1,3) - VFEA(1,3)
!     &                     , VFE(2,3) - VFEA(2,3)
!     &                     , VFE(3,3) - VFEA(3,3)
!     &                     , VFE(4,3) - VFEA(4,3)
!     &                     , VFE(5,3) - VFEA(5,3)
!     &                     , VFE(1,4) - VFEA(1,4)
!     &                     , VFE(2,4) - VFEA(2,4)
!     &                     , VFE(3,4) - VFEA(3,4)
!     &                     , VFE(4,4) - VFEA(4,4)
!     &                     , VFE(5,4) - VFEA(5,4)
!     &                     , VFE(1,5) - VFEA(1,5)
!     &                     , VFE(2,5) - VFEA(2,5)
!     &                     , VFE(3,5) - VFEA(3,5)
!     &                     , VFE(4,5) - VFEA(4,5)
!     &                     , VFE(5,5) - VFEA(5,5)


20    CONTINUE
!$omp end do
10    CONTINUE
!$omp end parallel

      RETURN
      END

C-------------------------------------------------------------------------

C-------------------------------------------------------------------------
      SUBROUTINE NS_MACRO3D_ASFE_V (   VFE,
     &                                 VCORE,
     &                                 VDJE,
     &                                 VPRG,
     &                                 VPRN,
     &                                 VPRE,
     &                                 VSOLE  )

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VFE  (5,6) !NDLELV
      REAL*8   VCORE(EG_CMMN_NDIM, EG_CMMN_NNELV)
      REAL*8   VDJE (EG_CMMN_NDJV)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE (2, 4)                         ! 2 POUR CHAQUE P6
      REAL*8   VSOLE(5,6) !(5,6)(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ni_ip2p3.fi'
      INCLUDE 'ns_macro3d_iprno.fi'

      !Entiers pour dimensionnement
!      INTEGER  NDLP6
      INTEGER  NDIMELV              !nombre de dimension du vrai élément (3)
      INTEGER  NFNAPPROXUV          !nombre de fonction d'approximation pour uetv
      INTEGER  NFNAPPROXH           !nombre de fonction d'approximation pour h
!
!      PARAMETER(NDLP6 = 15)         !nombre de dl par sous-élémnet de volume = nombre de noeud par sous-élémnet (3) X nombre de dl par noeud (5)
      PARAMETER(NDIMELV = 3)
      PARAMETER ( NFNAPPROXUV = 6)
      PARAMETER ( NFNAPPROXH = 3)
!
!      !Connectivités et table de localisation
      INTEGER KNET3  (3, 4)         !nombre de noeud par sous-élément , Nombre de sous-élément par élément
!      INTEGER KLOCET3(NDLP6, 4)
!      INTEGER KLOCE  (NDLP6)        !15 dl de l'élément T3 global (dl aux sommets)
!
      !Variables pour l'intégration numérique
      INTEGER  NPG                  !nombre de points de gauss (pour l'intégration numérique)
      INTEGER  IPG                  !itérateur sur les points de gauss
      REAL*8   VCOPG(NDIMELV)       !coordonnées du points de Gauss;
      REAL*8   POIDSPG              !poids du point de gauss
!
      REAL*8   VZCORE(2, EG_CMMN_NNELV) ! donne les zs et zf pour chacun des 6 noeuds.

      !Évaluations au point de gauss des métriques, dl, fonctions d'approximation et
      !leur dérivés
      REAL*8   VNUV(NFNAPPROXUV)          !évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH(NFNAPPROXH)            !évaluation des N de h au pt gauss
      REAL*8   VNUV_XI(NDIMELV,NFNAPPROXUV ) !évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   VNH_XI(NDIMELV,NFNAPPROXH) !évaluation des N,x_i (les N pour les h) aux pt de Gauss
      REAL*8   VJP12L(NDIMELV,NDIMELV)
      REAL*8   VJP6(NDIMELV,NDIMELV)
      REAL*8   DJP12L                     !évaluation du déterminant complet au pt de gauss
      REAL*8   DJP6                       !évaluation du déterminant du sous élément au pt de gauss
      REAL*8   VDLPT(3)                   !évaluation des degrés de liberté au point d'intégration
      REAL*8   VDL_XIPT(3, NDIMELV)       !évalutation des dérivés des dl p/r à x,y et z au point d'intégration
      REAL*8   WPT                        !évaluation de la vitesse verticale au point d'intégration
      REAL*8   VW_XIPT(NDIMELV)           !évalutation des dérivées de la vitesse verticale au point d'intégration

      INTEGER  IP6                        !contient l'indice du P6 correspondant au point de Gauss

      INTEGER  IN !itérateur sur les noeuds de l'élément
      INTEGER  IERR !pas vérifié ni utilisé

      REAL*8 COEFF, COEFFU, COEFFV, COEFFH
      DATA KNET3  / 1,2,6,  2,3,4,  6,4,5,  4,6,2/
!      DATA KLOCET3/  1, 2, 3, 4, 5,   6 ,7 ,8 ,9 ,10,   26,27,28,29,30,   ! NOEUDS 1,2,6,
!     &               6 ,7 ,8 ,9,10,   11,12,13,14,15,   16,17,18,19,20,   ! NOEUDS 2,3,4,
!     &              26,27,28,29,30,   16,17,18,19,20,   21,22,23,24,25,   ! NOEUDS 6,4,5,
!     &              16,17,18,19,20,   26,27,28,29,30,   6 ,7 ,8 ,9 ,10 /  ! NOEUDS 4,6,2
!      DATA KLOCE /  1 ,2 ,3 , 4, 5,   11,12,13,14,15,   21,22,23,24,25 /
!C-----------------------------------------------------------------------
!D     CALL ERR_PRE(LM_CMMN_NPREV .EQ. 2*4)           ! 2 POUR CHAQUE T3
!C-----------------------------------------------------------------------
!
C---  Construction de la table contenant les coordonnées en z
      DO IN = 1, EG_CMMN_NNELV !pour chaque noeud
         VZCORE(1,IN) = VPRN(NS_IPRNO_ZF, IN)
         VZCORE(2,IN) = VZCORE(1, IN) + VPRN(NS_IPRNO_PRFA, IN)
      END DO ! Boucle sur les noeuds

C---  INTÉGRATION NUMÉRIQUE
      NPG = NI_IP2P3_REQNPG()
D     CALL ERR_ASR(NPG .GT. 0)
      DO IPG = 1, NPG

C---     Récupére les coordonnées du points de Gauss et le poids associé
         POIDSPG = ZERO
         IERR = NI_IP2P3_REQCPG(IPG, NDIMELV, VCOPG)
         IERR = NI_IP2P3_REQWPG(IPG, POIDSPG)

C---     Calcul des métriques, des fonctions d'approximation et de leur dérivés,
C        des dl et de leur dérivées ainsi que de w (à traiter) et ses dérivées (à traiter)
C        le tout au point d'intégration
         CALL NS_MACRO3D_EVALUE (   NFNAPPROXUV,  ! entrée : dim
     &                              NFNAPPROXH,   ! entrée : dim
     &                              NDIMELV,      ! entrée : dim
     &                              VCORE,        ! entrée
     &                              VZCORE,       ! entrée
     &                              VDJE,         ! entrée
     &                              VCOPG,        ! entrée
     &                              VSOLE,         ! entrée
     &                              VPRn,         !ENTRÉE
     &                              VNUV,         ! sortie
     &                              VNH,          ! sortie
     &                              VNUV_XI,      ! sortie
     &                              VNH_XI,       ! sortie
     &                              VDLPT,        ! sortie
     &                              VDL_XIPT,     ! sortie
     &                              WPT,          ! sortie à analyser...
     &                              VW_XIPT,      ! sortie à analyser
     &                              DJP12L,       ! sortie
     &                              DJP6,         ! sortie (pas utilisée)
     &                              IP6 )         ! sortie

C---  CALCUL DE VFE = INT {N}(<N>{DL})DETJP12L
C     U : WI*DETJP12*{VNUV}<VDLPT(1)>
C     V : W1*DETJP12*{VNUV}<VDLPT(2)>
C     H : WI*DETjP12*
      COEFF = POIDSPG* djp12l !VDJE(5)/DEUX !tmp devrait être detJP12L P=1 dans l'exemple...
C---  ASSEMBLAGE DANS LES MATRICES
C---  u
      COEFFU = VDLPT(1)*COEFF
      VFE(1,KNET3(1,IP6)) = VFE(1,KNET3(1,IP6)) + VNUV(1)*COEFFU !VFE(1=uf, 1=n1) = n1 <n>{u}
      VFE(1,KNET3(2,IP6)) = VFE(1,KNET3(2,IP6)) + VNUV(2)*COEFFU
      VFE(1,KNET3(3,IP6)) = VFE(1,KNET3(3,IP6)) + VNUV(3)*COEFFU
      VFE(2,KNET3(1,IP6)) = VFE(2,KNET3(1,IP6)) + VNUV(4)*COEFFU
      VFE(2,KNET3(2,IP6)) = VFE(2,KNET3(2,IP6)) + VNUV(5)*COEFFU
      VFE(2,KNET3(3,IP6)) = VFE(2,KNET3(3,IP6)) + VNUV(6)*COEFFU

c--- v
      COEFFV = VDLPT(2)*COEFF
      VFE(3,KNET3(1,IP6)) = VFE(3,KNET3(1,IP6)) + VNUV(1)*COEFFV !VFE(1=uf, 1=n1) = n1 <n>{u}
      VFE(3,KNET3(1,IP6)) = VFE(3,KNET3(2,IP6)) + VNUV(2)*COEFFV
      VFE(3,KNET3(2,IP6)) = VFE(3,KNET3(3,IP6)) + VNUV(3)*COEFFV
      VFE(4,KNET3(2,IP6)) = VFE(4,KNET3(1,IP6)) + VNUV(4)*COEFFV
      VFE(4,KNET3(3,IP6)) = VFE(4,KNET3(2,IP6)) + VNUV(5)*COEFFV
      VFE(4,KNET3(3,IP6)) = VFE(4,KNET3(3,IP6)) + VNUV(6)*COEFFV

c--- H
      COEFFH = VDLPT(3)*POIDSPg*VDJE(5)/DEUX
      VFE(5,1) = VFE(5,1) + VNH(1)*COEFFH !VFE(1=uf, 1=n1) = n1 <n>{u}
      VFE(5,3) = VFE(5,3) + VNH(2)*COEFFH
      VFE(5,5) = VFE(5,5) + VNH(3)*COEFFH

      END DO !BOUCLE SUR PG



      RETURN
      END


C************************************************************************
C Sommaire:  NS_MACRO3D_ASMF_C
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMF_C( VCORG,
     &                              KLOCN,
     &                              KNGV,
     &                              KNGS,
     &                              VDJV,
     &                              VDJS,
     &                              VPRGL,
     &                              VPRNO,
     &                              VPREV,
     &                              VPRES, ! réassignation à Ka
     &                              VSOLC,
     &                              VSOLR,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              KDIMP,
     &                              VDIMP,
     &                              KEIMP,
     &                              VDLG,
     &                              VFG   )

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV) !réassignation à KA
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ns_macro3d.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(22) !VÉRIFIER

      INTEGER  NS_MACRO3D_ASMF_CB
      EXTERNAL NS_MACRO3D_ASMF_CB
C-----------------------------------------------------------------------

      KI( 1) = SO_ALLC_REQVHND(VCORG)
      KI( 2) = SO_ALLC_REQKHND(KLOCN)
      KI( 3) = SO_ALLC_REQKHND(KNGV)
      KI( 4) = SO_ALLC_REQKHND(KNGS)
      KI( 5) = SO_ALLC_REQVHND(VDJV)
      KI( 6) = SO_ALLC_REQVHND(VDJS)
      KI( 7) = SO_ALLC_REQVHND(VPRGL)
      KI( 8) = SO_ALLC_REQVHND(VPRNO)
      KI( 9) = SO_ALLC_REQVHND(VPREV)
      KI(10) = SO_ALLC_REQVHND(VPRES)
      KI(11) = SO_ALLC_REQVHND(VSOLC)
      KI(12) = SO_ALLC_REQVHND(VSOLR)
      KI(13) = SO_ALLC_REQKHND(KCLCND)
      KI(14) = SO_ALLC_REQVHND(VCLCNV)
      KI(15) = SO_ALLC_REQKHND(KCLLIM)
      KI(16) = SO_ALLC_REQKHND(KCLNOD)
      KI(17) = SO_ALLC_REQKHND(KCLELE)
      KI(18) = SO_ALLC_REQKHND(KDIMP)
      KI(19) = SO_ALLC_REQVHND(VDIMP)
      KI(20) = SO_ALLC_REQKHND(KEIMP)
      KI(21) = SO_ALLC_REQVHND(VDLG)
      KI(22) = SO_ALLC_REQVHND(VFG)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         IF (ERR_GOOD()) IERR = NS_MACRO3D_REQFCLIM(IT, 'ASMF', HFNC) ! HFNC sortie
         IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC2(HFNC,
     &                                    NS_MACRO3D_ASMF_CB,
     &                                    SO_ALLC_CST2B(IL),
     &                                    SO_ALLC_CST2B(KI))
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_ASMF_CB
C
C Description:
C     La fonction NS_MACRO3D_ASMF_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.<p>
C     Elle peut être spécialisée par les héritiers.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_ASMF_CB(F, IL, KI)

      IMPLICIT NONE

      INTEGER  NS_MACRO3D_ASMF_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(22)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         SO_ALLC_REQKIND(KA, KI( 1)),
     &         SO_ALLC_REQKIND(KA, KI( 2)),
     &         SO_ALLC_REQKIND(KA, KI( 3)),
     &         SO_ALLC_REQKIND(KA, KI( 4)),
     &         SO_ALLC_REQKIND(KA, KI( 5)),
     &         SO_ALLC_REQKIND(KA, KI( 6)),
     &         SO_ALLC_REQKIND(KA, KI( 7)),
     &         SO_ALLC_REQKIND(KA, KI( 8)),
     &         SO_ALLC_REQKIND(KA, KI( 9)),
     &         SO_ALLC_REQKIND(KA, KI(10)),
     &         SO_ALLC_REQKIND(KA, KI(11)),
     &         SO_ALLC_REQKIND(KA, KI(12)),
     &         SO_ALLC_REQKIND(KA, KI(13)),
     &         SO_ALLC_REQKIND(KA, KI(14)),
     &         SO_ALLC_REQKIND(KA, KI(15)),
     &         SO_ALLC_REQKIND(KA, KI(16)),
     &         SO_ALLC_REQKIND(KA, KI(17)),
     &         SO_ALLC_REQKIND(KA, KI(18)),
     &         SO_ALLC_REQKIND(KA, KI(19)),
     &         SO_ALLC_REQKIND(KA, KI(20)),
     &         SO_ALLC_REQKIND(KA, KI(21)),
     &         SO_ALLC_REQKIND(KA, KI(22)))

      NS_MACRO3D_ASMF_CB = ERR_TYP()
      RETURN
      END
