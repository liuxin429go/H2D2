C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire : NS_MACRO3D_PRCCLIM
C
C Description:
C     Traitement PRé-Calcul des Conditions LIMites
C
C Entrée:
C    int KNGV(EG_CMMN_NCELV=NNELV=6, EG_CMMN_NELV),
C    int KNGS(EG_CMMN_NCELS, EG_CMMN_NELS),
C    real VDJV(EG_CMMN_NDJV,  EG_CMMN_NELV),
C    real VDJS(EG_CMMN_NDJS,  EG_CMMN_NELS),
C    real VPRGL(LM_CMMN_NPRGL),
C    real VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL), n'est pas utilisé
C    real VPREV(LM_CMMN_NPREV, EG_CMMN_NELV),
C    real VPRES(LM_CMMN_NPRES, EG_CMMN_NELS),
C    int KCLCND( 4, LM_CMMN_NCLCND),
C    real VCLCNV(    LM_CMMN_NCLCNV),
C    int KCLLIM( 6, EG_CMMN_NCLLIM),
C    int KCLNOD(    EG_CMMN_NCLNOD),
C    int KCLELE(    EG_CMMN_NCLELE),
C Sortie:
C    int IERR
C    INTEGER KDIMP (LM_CMMN_NDLN = 3,  EG_CMMN_NNL) pour chaque dl, contient les code de CL
C    real VDIMP(LM_CMMN_NDLN = 3,  EG_CMMN_NNL) pour chaque dl, contient la valeur imposée par la cl
C    int KEIMP (EG_CMMN_NELS) pour chaque ELS, contient les codes des CL
C Notes:
C************************************************************************
      SUBROUTINE NS_MACRO3D_PRCCLIM(KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO, ! pas utilisé
     &                            VPREV,
     &                            VPRES,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PRCCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL) !pas utilisé
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d_idl.fi'

      INTEGER NS_MACRO3D_PRCCLIM_KDIMP
      INTEGER NS_MACRO3D_PRCCLIM_VDIMP
      INTEGER NS_MACRO3D_PRCCLIM_EC
      INTEGER IC, IE, ID, IN
      INTEGER IH !indice du degré de liberté H voir ns_macro3d_idl.fi
      INTEGER NEQ, NCLT, NCLTI
C-----------------------------------------------------------------------

C---- INFO
C      WRITE(MP,1000)
C      WRITE(MP,1001)'DESCRIPTION DES CODES DE CONDITIONS AUX LIMITES:'
C      WRITE(MP,1001)'CODE C.L.=1 CONDITION DE DIRICHLET;'
C      WRITE(MP,1001)'CODE C.L.=2 CONDITION DE CAUCHY;'
C      WRITE(MP,1001)'CODE C.L.=3 CONDITION DE FRONTIERE OUVERTE.'
C1000  FORMAT(/)
C1001  FORMAT(15X,A)

C---     INITIALISE
      NEQ  = LM_CMMN_NDLL
      NCLT = 0
      IH = NS_IDL_H

C---     RESET LES C.L.
      CALL IINIT(LM_CMMN_NDLL,    0, KDIMP, 1)
      CALL IINIT(EG_CMMN_NELS,    0, KEIMP, 1)
      CALL DINIT(LM_CMMN_NDLL, ZERO, VDIMP, 1)

C---     FLAG LES DDL DE H SUR LES NOEUDS MILIEUX
      DO IC=1,EG_CMMN_NELCOL
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         IN = KNGV(2,IE)
         KDIMP(IH,IN) = IBSET(KDIMP(IH,IN), EA_TPCL_PHANTOME)
         IN = KNGV(4,IE)
         KDIMP(IH,IN) = IBSET(KDIMP(IH,IN), EA_TPCL_PHANTOME)
         IN = KNGV(6,IE)
         KDIMP(IH,IN) = IBSET(KDIMP(IH,IN), EA_TPCL_PHANTOME)
      ENDDO
      ENDDO

C---     ASSIGNE LES CODES DE CONDITION
      IERR = NS_MACRO3D_PRCCLIM_KDIMP (   KCLCND,
     &                                    VCLCNV,
     &                                    KCLLIM,
     &                                    KCLNOD,
     &                                    KCLELE,
     &                                    KDIMP )

C---     CONTROLE LES C.L. POUR CHAQUE TYPE DE DDL
C---     S'ASSURE QUE CHAQUE DDL A AU MOINS UNE CL MGIA (si NIV > 3)
      DO ID=1,LM_CMMN_NDLN
         NCLTI = 0
         DO IN=1,EG_CMMN_NNL
            IF (KDIMP(ID,IN) .NE. 0) NCLTI = NCLTI + 1
         ENDDO
         IF (NCLTI .EQ. 0 .AND. LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
            WRITE(LOG_BUF,'(2A,I12)') 'MSG_DDL_SANS_CLIM',': ',ID
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
         NCLT = NCLT + NCLTI
      ENDDO

C---     ASSIGNE LES C.L. SUR LES ÉLÉMENTS DE CONTOUR
      IERR = NS_MACRO3D_PRCCLIM_EC(KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRES,
     &                           KDIMP,
     &                           KEIMP)

C---     COMPTE LE NOMBRE DE C.L.
      NCLT = 0
      DO IN = 1,EG_CMMN_NNL
         DO ID=1,LM_CMMN_NDLN
            IF (BTEST(KDIMP(ID,IN), EA_TPCL_DIRICHLET)) THEN
               NCLT = NCLT + 1
            ENDIF
         ENDDO
      ENDDO

C---     MISE A JOUR DE NEQ
      NEQ = NEQ - NCLT

C---     CONTROLE DE NEQ
      IF (NEQ. LE. 0) GOTO 9900

C---     ASSIGNE LES VALEURS DE CONDITION
      IERR = NS_MACRO3D_PRCCLIM_VDIMP  (  KCLCND,
     &                                    VCLCNV,
     &                                    KCLLIM,
     &                                    KCLNOD,
     &                                    KCLELE,
     &                                    KDIMP,
     &                                    VDIMP )

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)') 'ERR_CLIM_NBR_EQUATIONS',': ',NEQ
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_PRCCLIM_KDIMP
C
C Description:
C     La fonction NS_MACRO3D_PRCCLIM_KDIMP fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition.
C     Elle peut être spécialisée par les héritiers.
C
C Entrée: KCLCND
C         VCLCNV
C         KCLLIM
C         KCLNOD
C         KCLELE
C
C
C Sortie: KDIMP: Pour chaque ddl de chaque noeud,
C                               stocke les codes (LM_CMMN_XXX_CL)
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PRCCLIM_KDIMP(KCLCND,
     &                                  VCLCNV,
     &                                  KCLLIM,
     &                                  KCLNOD,
     &                                  KCLELE,
     &                                  KDIMP)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_PRCCLIM_KDIMP
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ns_macro3d.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(6)

      INTEGER  NS_MACRO3D_PRCCLIM_KDIMP_CB
      EXTERNAL NS_MACRO3D_PRCCLIM_KDIMP_CB
C-----------------------------------------------------------------------

      KI(1) = SO_ALLC_REQKHND(KCLCND)
      KI(2) = SO_ALLC_REQVHND(VCLCNV)
      KI(3) = SO_ALLC_REQKHND(KCLLIM)
      KI(4) = SO_ALLC_REQKHND(KCLNOD)
      KI(5) = SO_ALLC_REQKHND(KCLELE)
      KI(6) = SO_ALLC_REQKHND(KDIMP)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         IF (ERR_GOOD()) IERR = NS_MACRO3D_REQFCLIM(IT, 'COD', HFNC)
         IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC2(HFNC,
     &                                    NS_MACRO3D_PRCCLIM_KDIMP_CB,
     &                                    SO_ALLC_CST2B(IL),
     &                                    SO_ALLC_CST2B(KI))
      ENDDO

      NS_MACRO3D_PRCCLIM_KDIMP = ERR_TYP()
      RETURN
      END


C************************************************************************
C Sommaire:  NS_MACRO3D_PRCCLIM_KDIMP_CB
C
C Description:
C     La fonction NS_MACRO3D_PRCCLIM_KDIMP_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.<p>
C     Elle peut être spécialisée par les héritiers.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PRCCLIM_KDIMP_CB(F, IL, KI)

      IMPLICIT NONE

      INTEGER  NS_MACRO3D_PRCCLIM_KDIMP_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(6)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         SO_ALLC_REQKIND(KA, KI( 1)),
     &         SO_ALLC_REQKIND(KA, KI( 2)),
     &         SO_ALLC_REQKIND(KA, KI( 3)),
     &         SO_ALLC_REQKIND(KA, KI( 4)),
     &         SO_ALLC_REQKIND(KA, KI( 5)),
     &         SO_ALLC_REQKIND(KA, KI( 6)))

      NS_MACRO3D_PRCCLIM_KDIMP_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_PRCCLIM_VDIMP
C
C Description:
C     La fonction NS_MACRO3D_PRCCLIM_VDIMP fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.<p>
C     Elle peut être spécialisée par les héritiers.
C
C Entrée:
C
C Sortie: VDIMP
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PRCCLIM_VDIMP(KCLCND,
     &                                VCLCNV,
     &                                KCLLIM,
     &                                KCLNOD,
     &                                KCLELE,
     &                                KDIMP,
     &                                VDIMP)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_PRCCLIM_VDIMP
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ns_macro3d.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(7)

      INTEGER  NS_MACRO3D_PRCCLIM_VDIMP_CB
      EXTERNAL NS_MACRO3D_PRCCLIM_VDIMP_CB
C-----------------------------------------------------------------------

      KI(1) = SO_ALLC_REQKHND(KCLCND)
      KI(2) = SO_ALLC_REQVHND(VCLCNV)
      KI(3) = SO_ALLC_REQKHND(KCLLIM)
      KI(4) = SO_ALLC_REQKHND(KCLNOD)
      KI(5) = SO_ALLC_REQKHND(KCLELE)
      KI(6) = SO_ALLC_REQKHND(KDIMP)
      KI(7) = SO_ALLC_REQVHND(VDIMP)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         IF (ERR_GOOD()) IERR = NS_MACRO3D_REQFCLIM(IT, 'PRC', HFNC)
         IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC2(HFNC,
     &                                    NS_MACRO3D_PRCCLIM_VDIMP_CB,
     &                                    SO_ALLC_CST2B(IL),
     &                                    SO_ALLC_CST2B(KI))
      ENDDO

      NS_MACRO3D_PRCCLIM_VDIMP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_PRCCLIM_VDIMP_CB
C
C Description:
C     La fonction NS_MACRO3D_PRCCLIM_VDIMP_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.<p>
C     Elle peut être spécialisée par les héritiers.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PRCCLIM_VDIMP_CB(F, IL, KI)

      IMPLICIT NONE

      INTEGER  NS_MACRO3D_PRCCLIM_VDIMP_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(7)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         SO_ALLC_REQKIND(KA, KI( 1)),
     &         SO_ALLC_REQKIND(KA, KI( 2)),
     &         SO_ALLC_REQKIND(KA, KI( 3)),
     &         SO_ALLC_REQKIND(KA, KI( 4)),
     &         SO_ALLC_REQKIND(KA, KI( 5)),
     &         SO_ALLC_REQKIND(KA, KI( 6)),
     &         SO_ALLC_REQKIND(KA, KI( 7)))

      NS_MACRO3D_PRCCLIM_VDIMP_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_PRCCLIM_EC
C
C Description:
C     La fonction privée NS_MACRO3D_PRCCLIM_EC assigne les codes
C     des éléments de contour en fonction des C.L. imposées aux
C     noeuds.
C
C Entrée:   KNGV :
C           KNGS : pour chaque ES contient info : n1, n2, n3, parent, côté
C           VDJV :
C           VDJS :
C           VPRES :
C           KDIMP :
C
C Sortie:   KEIMP
C
C Notes:    VDJ (1)TXN  (2)TYN  (3)Le  (4)QN1  (5)QN2
C           *** CONCENTRATION NULLE SI NOEUD DÉCOUVERT
C
C     Les conditions limites imposées doivent être de même type pour tous
C     les degrés de libertés considérés (pour toutes les variables simulées)
C
C************************************************************************
      FUNCTION NS_MACRO3D_PRCCLIM_EC(KNGV,
     &                               KNGS,
     &                               VDJV,
     &                               VDJS,
     &                               VPRES,
     &                               KDIMP,
     &                               KEIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_PRCCLIM_EC
      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS (EG_CMMN_NDJV, EG_CMMN_NELS)
      REAL*8  VPRES(LM_CMMN_NPRES,EG_CMMN_NELS)
      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP(EG_CMMN_NELS)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      CHARACTER*(48) TYPCL
      LOGICAL ESTCAUCHY
      LOGICAL ESTOUVERT
      LOGICAL ESTENTRANT
      LOGICAL ESTSORTANT

      INTEGER IEP, ID, IN, NP1, NP2, IE
      REAL*8  QN1, QN2
C-----------------------------------------------------------------------
      LOGICAL DDL_ESTCAUCHY
      LOGICAL DDL_ESTOUVERT
      DDL_ESTCAUCHY(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_CAUCHY)
      DDL_ESTOUVERT(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     LES LIMITES DE CAUCHY ET ENTRANT SUR "OUVERT"
 !     NELC = 0
      DO IEP=1,EG_CMMN_NELS
         NP1 = KNGS(1,IEP)
         NP2 = KNGS(2,IEP)
         QN1 = VPRES(1,IEP)
         QN2 = VPRES(2,IEP)

C---        LES LIMITES DE CAUCHY ENTRANT
         ESTCAUCHY  = (DDL_ESTCAUCHY(1,NP1) .AND. DDL_ESTCAUCHY(1,NP2))
         ESTENTRANT = (QN1 .LE. ZERO .OR. QN2 .LE. ZERO)
         IF (ESTCAUCHY .AND. ESTENTRANT) THEN
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_CAUCHY)
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_ENTRANT)
         ENDIF

C---        LES LIMITES "OUVERT" ENTRANT
         ESTOUVERT  = (DDL_ESTOUVERT(1,NP1) .AND. DDL_ESTOUVERT(1,NP2))
         ESTENTRANT = (QN1 .LT. ZERO .AND. QN2 .LT. ZERO)
         IF (ESTOUVERT .AND. ESTENTRANT) THEN
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_OUVERT)
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_ENTRANT)
         ENDIF

C---        LES ÉLÉMENTS "OUVERTS" SORTANT
         ESTOUVERT  = (DDL_ESTOUVERT(1,NP1) .OR. DDL_ESTOUVERT(1,NP2))
         ESTSORTANT = (QN1 .GE. ZERO .OR. QN2 .GE. ZERO)
         IF (ESTOUVERT .AND. ESTSORTANT) THEN
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_OUVERT)
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_SORTANT)
         ENDIF

      ENDDO

C---   IMPRESSION DES CONNECTIVITÉS DE LA PEAU DES FLUX DE
C      CAUCHY(ENTRE), OUVERT(NUL+SORT), DIRICHLET+NEUMAN(DÉFAUT)
      IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
         CALL LOG_ECRIS(' ')
         CALL LOG_ECRIS(' ')
         WRITE(LOG_BUF,'(A)') 'MSG_TYPE_CLIM_ELEMENTS_NOEUDS'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
         DO IEP=1,EG_CMMN_NELS

            IF (BTEST(KEIMP(IEP), EA_TPCL_CAUCHY) .AND.
     &          BTEST(KEIMP(IEP), EA_TPCL_SORTANT)) THEN
               IE = KNGS(3,IEP)
               WRITE(LOG_BUF,2050) IEP,NP1,NP2,
     &                          IE,KNGV(1,IE),KNGV(2,IE),KNGV(3,IE)
               CALL LOG_ECRIS(LOG_BUF)
            ENDIF

            IF (BTEST(KEIMP(IEP), EA_TPCL_ENTRANT) .AND.
     &          (BTEST(KEIMP(IEP), EA_TPCL_OUVERT) .OR.
     &           BTEST(KEIMP(IEP), EA_TPCL_CAUCHY))) THEN
               TYPCL = 'MSG_CL_ENTRANT_CAUCHY_OU_OUVERT#<40>#'
            ELSEIF (BTEST(KEIMP(IEP), EA_TPCL_SORTANT) .AND.
     &              BTEST(KEIMP(IEP), EA_TPCL_OUVERT)) THEN
               TYPCL = 'MSG_CL_SORTANT_OUVERT#<40>#'
            ELSE
               TYPCL = 'MSG_CL_DEFAUT#<40>#'
            ENDIF
            WRITE(LOG_BUF,'(A,2X,I9,3X,2(1X,I9))')
     &            TYPCL(1:SP_STRN_LEN(TYPCL)),
     &            IEP, KNGS(1,IEP), KNGS(2,IEP)
            CALL LOG_ECRIS(LOG_BUF)
         ENDDO
         CALL LOG_DECIND()
      ENDIF

C---  IMPRESSION DE L'INFO
C      WRITE(MP,1000)
C      WRITE(MP,1001)'TRAITEMENT DES ELEMENTS DE CONTOUR:'
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DE CAUCHY       =',
C     &               NELC
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. OUVERT          =',
C     &               NELS
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DIRICHLET-NEUMAN=',
C     &               EG_CMMN_NELS-NELC-NELS
C      WRITE(MP,1000)

C---------------------------------------------------------------
1000  FORMAT(/)
1001  FORMAT(15X,A)
1002  FORMAT(15X,A,I12)
2050  FORMAT(2X,'AVERTISSEMENT ---  APPLICATION D''UNE CONDITION DE',/,
     &       2X,'   NEUMAN PAR DEFAUT; LA CONDITION DE CAUCHY-ENTRANT',
     &          '   N''EST PAS APPLICABLE SUR UN FLUX SORTANT',/,
     &       2X,'ELEMENT DE CONTOUR',I12,/,
     &       2X,'NOEUDS DE CONTOUR ',2(1X,I12),/,
     &       2X,'ELEMENT T3 PARENT',I12,/,
     &       2X,'CONNECTIVITE DU T3 PARENT',3(1X,I12),/)
C---------------------------------------------------------------

      NS_MACRO3D_PRCCLIM_EC = ERR_TYP()
      RETURN
      END
