C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: NS_MACRO3D_CLCDLIB
C
C Description:
C     La fonction NS_MACRO3D_CLCDLIB
C        CaLCul sur les Degrées de LIBerté
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCDLIB (  KNGV,
     &                                 KLOCN,
     &                                 KDIMP,
     &                                 VDIMP,
     &                                 VDLG,
     &                                 IERR  )
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CLCDLIB
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KLOCN(LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'ns_macro3d_idl.fi'

      INTEGER IC, IE, ID
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
      LOGICAL EST_PHANTOME
      EST_PHANTOME(ID) = BTEST(ID, EA_TPCL_PHANTOME)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNELV .EQ. 6)
C-----------------------------------------------------------------------


C---     Assigne les noeuds milieux en niveau d'eau
      DO IC=1,EG_CMMN_NELCOL
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         NO2 = KNGV(2,IE)
         IF (EST_PHANTOME(KDIMP(NS_IDL_H,NO2))) THEN
            NO1 = KNGV(1,IE)
            NO3 = KNGV(3,IE)
            VDLG(NS_IDL_H,NO2) =
     &                  UN_2*(VDLG(NS_IDL_H,NO1) + VDLG(NS_IDL_H,NO3))
         ENDIF

         NO2 = KNGV(4,IE)
         IF (EST_PHANTOME(KDIMP(NS_IDL_H,NO2))) THEN
            NO1 = KNGV(3,IE)
            NO3 = KNGV(5,IE)
            VDLG(NS_IDL_H,NO2) =
     &                  UN_2*(VDLG(NS_IDL_H,NO1) + VDLG(NS_IDL_H,NO3))
         ENDIF

         NO2 = KNGV(6,IE)
         IF (EST_PHANTOME(KDIMP(NS_IDL_H,NO2))) THEN
            NO1 = KNGV(5,IE)
            NO3 = KNGV(1,IE)
            VDLG(NS_IDL_H,NO2) =
     &                  UN_2*(VDLG(NS_IDL_H,NO1) + VDLG(NS_IDL_H,NO3))
         ENDIF

      ENDDO
      ENDDO

      IERR = ERR_TYP()
      RETURN
      END
