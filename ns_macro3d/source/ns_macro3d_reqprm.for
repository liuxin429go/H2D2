C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : NS_MACRO3D_REQPRM
C
C Description:
C     PARAMETRES DE L'ELEMENT
C
C Entrée:
C
C Sortie: Ce sont tous des paramètres de sortie
C
C Notes:
C      NDIM n'est pas utilisé. C'est plutôt celui de l'élément géométrique
C           qui est utilisé, toujours pas de test d'équivalence
C      NDJV n'est pas utilisé. C'est plutôt celui de l'élément géométrique
C           qui est utilisé
C      NNELV n'est pas utilisé. C'est plutôt celui de l'élément géométrique
C           qui est utilisé. Pas de test d'équivalence
C      NDELV ne semble pas assigné à LM_CMMN_NDLEV (une fois rendu dans lm_elib_asmdim je découvre qu'il vaut 18 plutôt que 30)
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_REQPRM(TGELV,
     &                             TGELS,
     &                             NNELV,
     &                             NNELS,
     &                             NPRGL,
     &                             NPRGLL,
     &                             NPRNO,
     &                             NPRNOL,
     &                             NPREV,
     &                             NPREVL,
     &                             NPRES,
     &                             NSOLC,
     &                             NSOLCL,
     &                             NSOLR,
     &                             NSOLRL,
     &                             NDLN,
     &                             NDLEV,
     &                             NDLES,
     &                             ASURFACE,
     &                             ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_REQPRM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER TGELV
      INTEGER TGELS
      INTEGER NNELV
      INTEGER NNELS
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES
      LOGICAL ASURFACE
      LOGICAL ESTLIN

      INCLUDE 'eacnst.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'eacmmn.fc'
C-----------------------------------------------------------------------


C--      INITIALISE DES PARAMÈTRES QUI NE DEVRAIENT PAS ÊTRE DIFFÉRENTS

      TGELV  =  EG_TPGEO_P12L         ! TYPE DE GEOMETRIE DE L'ELEMENT DE VOLUME
      TGELS  =  EG_TPGEO_L3L          ! TYPE DE GEOMETRIE DE L'ELEMENT DE SURFACE
      NNELV  =  6                     ! NB DE NOEUDS PAR ÉLÉMENT DE VOLUME
      NNELS  =  3                     ! NB DE NOEUDS PAR ÉLÉMENT DE SURFACE

C---     INITIALISE LES PARAMETRES DE L'HERITIER
      NPRGL  = 30                     ! NB DE PROPRIÉTÉS GLOBALES
      NPRGLL =  29                    ! NB DE PROPRIÉTÉS GLOBALES LUES
      NPRNO  = 14                      ! NB DE PROPRIÉTÉS NODALES; voir vprno.doc pour description
      NPRNOL =  6                     ! NB DE PROPRIÉTÉS NODALES LUES
      NPREV  =  2*4                   ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME
      NPREVL =  0                     ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME LUES
      NPRES  =  6                     ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE SURFACE
      NSOLC  =  5                     ! NB DE SOLLICITATIONS CONCENTRÉES
      NSOLCL =  5                     ! NB DE SOLLICITATIONS CONCENTRÉES LUES
      NSOLR  =  5                     ! NB DE SOLLICITATIONS RÉPARTIES
      NSOLRL =  5                     ! NB DE SOLLICITATIONS RÉPARTIES LUES
      NDLN  = 5                       ! NB DE DDL/NOEUD us, uf, vs, vf, h
      NDLEV = NDLN * NNELV            ! NB DE DDL PAR ELEMENT DE VOLUME
      NDLES = NDLN * NNELS            ! NB DE DDL PAR ELEMENT DE SURFACE

      ASURFACE = .TRUE.
      ESTLIN   = .FALSE.
      RETURN
      END

