C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: NS_MACRO3D_ASMK
C
C Description:
C     La fonction NS_MACRO3D_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMK (  VCORG,
     &                              KLOCN,
     &                              KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES, ! pas utilisé
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_ASMK
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! sortie ou pas vraiment utilisé
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NNELT, NDLNMAX, NDLEMAX
      PARAMETER (NNELT=6)
      PARAMETER (NDLNMAX=5)
      PARAMETER (NDLEMAX=NNELT*NDLNMAX)

      INTEGER   KLOCE(NDLEMAX)
      REAL*8    VKE(NDLEMAX*NDLEMAX)
C----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. NDLNMAX)
C-----------------------------------------------------------------

C---     Pré-traitement avant calcul
      CALL NS_MACRO3D_CLC(VCORG,
     &                  KNGV,
     &                  KNGS,
     &                  VDJV,
     &                  VDJS,
     &                  VPRGL,
     &                  VPRNO,
     &                  VPREV,
     &                  VPRES, ! sortie
     &                  KCLCND,
     &                  VCLCNV,
     &                  KCLLIM,
     &                  KCLNOD,
     &                  KCLELE,
     &                  KDIMP,
     &                  VDIMP,
     &                  KEIMP,
     &                  VDLG,
     &                  IERR)

C---     Contributions de volume
      CALL NS_MACRO3D_ASMK_V(KLOCE,
     &                     VKE,
     &                     VCORG,
     &                     KLOCN,
     &                     KNGV,
     &                     KNGS,
     &                     VDJV,
     &                     VDJS,
     &                     VPRGL,
     &                     VPRNO,
     &                     VPREV,
     &                     VPRES, ! pas utilisé
     &                     VSOLR,
     &                     VDLG,
     &                     HMTX,
     &                     F_ASM)

!C---     Contributions de surface
!      CALL NS_MACRO3D_ASMK_S(KLOCE,
!     &                     VKE,
!     &                     VCORG,
!     &                     KLOCN,
!     &                     KNGV,
!     &                     KNGS,
!     &                     VDJV,
!     &                     VDJS,
!     &                     VPRGL,
!     &                     VPRNO,
!     &                     VPREV,
!     &                     VPRES,  ! pas utilisé
!     &                     VSOLR,
!     &                     VDLG,
!     &                     HMTX,
!     &                     F_ASM)

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_ASMK_V
C
C Description:
C     La fonction NS_MACRO3D_ASMK_V calcul le matrice de rigidité
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMK_V(KLOCE,
     &                           VKE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES, ! pas utilisé
     &                           VSOLR,
     &                           VDLG,
     &                           HMTX,
     &                           F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d.fc'

      INTEGER IERR
      INTEGER IC, IE, IN, II, NO

      INTEGER NDJV_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      INTEGER NDIM_LCL
      INTEGER NNELV_LCL

      PARAMETER (NDJV_LCL = 5)
      PARAMETER (NDLE_LCL = 30)
      PARAMETER (NPRN_LCL = 14)
      PARAMETER (NDIM_LCL = 2)
      PARAMETER (NNELV_LCL = 6)


      INTEGER KNE(6)
      REAL*8  VDLE(NDLE_LCL)
      REAL*8  VPRN(NPRN_LCL, 6)
      REAL*8   VCORE(NDIM_LCL, NNELV_LCL)
      REAL*8  PENA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDJV_LCL .GE. EG_CMMN_NDJV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .EQ. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN, II, NO, PENA, KNE, VKE, KLOCE, VDLE, VCORE,
!$omp& VPRN, IERR)

C---     INITIALISE KLOCE
      CALL IINIT(LM_CMMN_NDLEV,0,KLOCE,1)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        INITIALISE LA MATRICE ELEMENTAIRE
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)

C---        TRANSFERT DES CONNECTIVITÉS ÉLÉMENTAIRES
         DO IN=1,EG_CMMN_NNELV
            KNE(IN) = KNGV(IN,IE)
         ENDDO

C---        TRANSFERT DES DDL
         II=0
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            VDLE(II+1) = VDLG(1,NO)
            VDLE(II+2) = VDLG(2,NO)
            VDLE(II+3) = VDLG(3,NO)
            VDLE(II+4) = VDLG(4,NO)
            VDLE(II+5) = VDLG(5,NO)
            CALL DCOPY(EG_CMMN_NDIM,  VCORG(1,NO), 1, VCORE(1,IN), 1)
            II = II + 5
         ENDDO


C---        TRANSFERT DES PRNO
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(1,IN), 1)
         ENDDO

C---        CALCUL DE LA MATRICE ELEMENTAIRE DE VOLUME
         CALL NS_MACRO3D_ASMKE_V (  VKE,
     &                              VCORE,
     &                              VDJV(1,IE),
     &                              VPRGL,
     &                              VPRN,
     &                              VPREV(1,IE),
     &                              VDLE)


C---        PENALISATION EN hh
         PENA = NS_MACRO3D_PNUMR_PENALITE*VDJV(5,IE)
         IF (VKE( 5, 5) .EQ. ZERO) VKE( 5, 5) = PENA !mettre mieux (idl...)
         IF (VKE( 15, 15) .EQ. ZERO) VKE( 15, 15) = PENA
         IF (VKE(25,25) .EQ. ZERO) VKE(25,25) = PENA

C---        TABLE KLOCE DE L'ÉLÉMENT T6L
         DO IN=1,EG_CMMN_NNELV
            KLOCE(1, IN) = KLOCN(1, KNE(IN))
            KLOCE(2, IN) = KLOCN(2, KNE(IN))
            KLOCE(3, IN) = KLOCN(3, KNE(IN))
            KLOCE(4, IN) = KLOCN(4, KNE(IN))
            KLOCE(5, IN) = KLOCN(5, KNE(IN))

         ENDDO

C---       ASSEMBLAGE DE LA MATRICE
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE
!$omp end parallel

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_ASMK_S
C
C Description:
C     La fonction NS_MACRO3D_ASMK_S calcul la matrice élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
!      SUBROUTINE NS_MACRO3D_ASMK_S(KLOCE,
!     &                           VKE,
!     &                           VCORG,
!     &                           KLOCN,
!     &                           KNGV,
!     &                           KNGS,
!     &                           VDJV,
!     &                           VDJS,
!     &                           VPRGL,
!     &                           VPRNO,
!     &                           VPREV,
!     &                           VPRES, ! pas utilisé
!     &                           VSOLR,
!     &                           VDLG,
!     &                           HMTX,
!     &                           F_ASM)
!
!      IMPLICIT NONE
!
!      INCLUDE 'eacnst.fi'
!      INCLUDE 'eacmmn.fc'
!      INCLUDE 'egcmmn.fc'
!
!      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
!      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
!      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
!      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
!      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
!      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
!      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
!      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
!      REAL*8   VPRGL (LM_CMMN_NPRGL)
!      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
!      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
!      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
!      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
!      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
!      INTEGER  HMTX
!      INTEGER  F_ASM
!      EXTERNAL F_ASM
!
!      INCLUDE 'err.fi'
!      INCLUDE 'log.fi'
!      INCLUDE 'ns_macro3d.fc'
!
!      INTEGER NNEL_LCL
!      INTEGER NDLN_LCL
!      INTEGER NPRN_LCL
!      PARAMETER (NNEL_LCL =  6)
!      PARAMETER (NDLN_LCL =  3)
!      PARAMETER (NPRN_LCL = 14)
!
!      INTEGER IERR
!      INTEGER IN, NO
!      INTEGER IES, IEV, ICT
!      REAL*8  VDLEV(NDLN_LCL, NNEL_LCL)
!      REAL*8  VPRNV(NPRN_LCL, NNEL_LCL)
!C-----------------------------------------------------------------------
!D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
!D     CALL ERR_PRE(NDLN_LCL .GE. LM_CMMN_NDLN)
!D     CALL ERR_PRE(NPRN_LCL .EQ. LM_CMMN_NPRNO)
!C-----------------------------------------------------------------------
!
!C-------  BOUCLE SUR LES ELEMENTS
!C         =======================
!      DO IES=1,EG_CMMN_NELS
!
!C---        ELEMENT PARENT ET COTÉ
!         IEV = KNGS(4,IES)
!         ICT = KNGS(5,IES)
!
!C---        TRANSFERT DES DDL
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN,IEV)
!            VDLEV(1,IN) = VDLG(1,NO)
!            VDLEV(2,IN) = VDLG(2,NO)
!            VDLEV(3,IN) = VDLG(3,NO)
!         ENDDO
!
!C---        TRANSFERT DES PRNO
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN,IEV)
!            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRNV(1,IN), 1)
!         ENDDO
!
!C---        INITIALISE LA MATRICE ELEMENTAIRE
!         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
!
!C---        [K(u)]
!         CALL NS_MACRO3D_ASMKE_S(VKE,
!     &                         VDJV(1,IEV),
!     &                         VDJS(1,IES),
!     &                         VPRGL,
!     &                         VPRNV,
!     &                         VPREV(1,IEV),
!     &                         VPRES(1,IES), !pas utilisé
!     &                         VDLEV,
!     &                         ICT)
!
!C---        TABLE KLOCE DE L'ÉLÉMENT T6L
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN,IEV)
!            KLOCE(1,IN) = KLOCN(1,NO)
!            KLOCE(2,IN) = KLOCN(2,NO)
!            KLOCE(3,IN) = KLOCN(3,NO)
!         ENDDO
!
!C---       ASSEMBLAGE DE LA MATRICE
!         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
!D        IF (ERR_BAD()) THEN
!D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
!D              CALL ERR_RESET()
!D           ELSE
!D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
!D              CALL LOG_ECRIS(LOG_BUF)
!D           ENDIF
!D        ENDIF
!
!      ENDDO
!
!      IF (.NOT. ERR_GOOD() .AND.
!     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
!         CALL ERR_RESET()
!      ENDIF
!
!      RETURN
!      END
