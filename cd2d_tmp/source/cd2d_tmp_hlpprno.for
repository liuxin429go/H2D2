C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_TMP_HLPPRNO
C
C Description:
C     IMPRESSION DES PROPRIETES NODALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_TMP_HLPPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_HLPPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fi'

      INTEGER I
      INTEGER IERR
C-----------------------------------------------------------------------

      CALL CD2D_BSE_HLPPRNO()
      I = 5

C---     IMPRESSION DE L'ENTETE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('PROPRIETES NODALES DE CINETIQUE LUES (SI):')
      CALL LOG_INCIND()

C---     IMPRESSION DE L'INFO
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'FLUX DE RADIATION SOLAIRE EFFECTIVE (W/m2)')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'FRACTION DU COUVERT VEGETAL ([0-1] 0=Découvert)')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'COEFFICIENT DE TURBIDITE DE LA COLONNE D''EAU')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'TEMPERATURE DE L''AIR (degC)')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'EMISSIVITE DE L''ATMOSPHERE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'FONCTION DU VENT (-)')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'HUMIDITE RELATIVE (%)')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'PRECIPITATION DE NEIGE OU PLUIE (m)')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'CONDUCTIVITE DE LA GLACE (W/m2.K)')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,
     &   'FONCTION D''EXTINCTION DU FLUX ATMOSPHERIQUE' //
     &               ' ([0-1] 1=pas d''extinction)')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'CONDUCTIVITE DU FOND (W/m2.K)')

      CALL LOG_DECIND()

      RETURN
      END
