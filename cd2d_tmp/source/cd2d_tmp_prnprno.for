C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_TMP_PRNPRNO
C
C Description:
C     IMPRESSION DES PROPRIETES NODALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_TMP_PRNPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_PRNPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL CD2D_B1L_PRNPRNO()

C---     IMPRESSION DE L'ENTETE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('PROPRIETES NODALES DE CINETIQUE LUES (SI):')

C---     IMPRESSION DE L'INFO
      CALL LOG_INCIND()
      CALL LOG_ECRIS('FLUX DE RADIATION SOLAIRE EFFECTIVE (W/m2)')
      CALL LOG_ECRIS('FRACTION DU COUVERT VEGETAL ([0-1] 0=Découvert)')
      CALL LOG_ECRIS('COEFFICIENT DE TURBIDITE DE LA COLONNE D''EAU')
      CALL LOG_ECRIS('TEMPERATURE DE L''AIR (degC)')
      CALL LOG_ECRIS('EMISSIVITE DE L''ATMOSPHERE')
      CALL LOG_ECRIS('FONCTION DU VENT (-)')
      CALL LOG_ECRIS('HUMIDITE RELATIVE (%)')
      CALL LOG_ECRIS('PRECIPITATION DE NEIGE OU PLUIE (m)')
      CALL LOG_ECRIS('CONDUCTIVITE DE LA GLACE (W/m2.K)')
      CALL LOG_ECRIS('FONCTION D''EXTINCTION DU FLUX ATMOSPHERIQUE' //
     &               ' ([0-1] 1=pas d''extinction)')
      CALL LOG_ECRIS('CONDUCTIVITE DU FOND (W/m2.K)')
      CALL LOG_DECIND()

      RETURN
      END
