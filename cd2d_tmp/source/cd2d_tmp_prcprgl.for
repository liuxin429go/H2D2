C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_TMP_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_TMP_PRCPRGL (VPRGL,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'cd2d_tmp.fc'
      INCLUDE 'cd2d_bse.fc'
C-----------------------------------------------------------------------

C---     Appelle le parent et copie dans la dll
      CALL CD2D_BSE_PRCPRGL(VPRGL, IERR)
      CALL CD2D_BSE_CPYPRGL(CD2D_CBS_VA)

C---     Calcul des termes puits-sources
      CALL DO_PRCPRGL(VPRGL, IERR)

      RETURN
      END

C************************************************************************
C Sommaire : DO_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE DO_PRCPRGL(VPRGL, IERR)

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_tmp.fc'

      INTEGER IP
C-----------------------------------------------------------------------
D     CALL ERR_ASR(CD2D_BSE_NPRGLL+19 .EQ. LM_CMMN_NPRGLL)
C-----------------------------------------------------------------------

C---     AFFECTE LES VALEURS AUX PROPRIÉTÉS GLOBALES
      IP = CD2D_BSE_NPRGLL
      CD2D_TMP_RHOE      = VPRGL(IP+ 1)   ! masse volumique moyenne de l'eau
      CD2D_TMP_CP        = VPRGL(IP+ 2)   ! chaleur specifique moyenne de l'eau
      CD2D_TMP_RHOG      = VPRGL(IP+ 3)   ! masse volumique moyenne de la glace
      CD2D_TMP_CPG       = VPRGL(IP+ 4)   ! chaleur specifique moyenne de la glace
      CD2D_TMP_CLG       = VPRGL(IP+ 5)   ! chaleur latente de fusion de la glace
      CD2D_TMP_PATM      = VPRGL(IP+ 6)   ! pression atmosphérique
      CD2D_TMP_CEVAPO    = VPRGL(IP+ 7)   ! coef. d'evaporation
      CD2D_TMP_CBOWEN    = VPRGL(IP+ 8)   ! coef. du ratio de Bowen
      CD2D_TMP_TFOND     = VPRGL(IP+ 9)   ! température du fond
      CD2D_TMP_A4        = VPRGL(IP+10)   ! pente de la linéarisation de la radiation infrarouge
      CD2D_TMP_B4        = VPRGL(IP+11)   ! ordonnée à l'origine de la linéarisation de la radiation infrarouge
      CD2D_TMP_CMULT_RAD = VPRGL(IP+12)   ! coef. multiplicateur radiation solaire
      CD2D_TMP_CMULT_LWA = VPRGL(IP+13)   ! coef. multiplicateur long wave atm
      CD2D_TMP_CMULT_LWS = VPRGL(IP+14)   ! coef. multiplicateur long wave srf
      CD2D_TMP_CMULT_EVA = VPRGL(IP+15)   ! coef. multiplicateur evaporation
      CD2D_TMP_CMULT_CNV = VPRGL(IP+16)   ! coef. multiplicateur convection
      CD2D_TMP_CMULT_PRC = VPRGL(IP+17)   ! coef. multiplicateur precipitation
      CD2D_TMP_CMULT_GLC = VPRGL(IP+18)   ! coef. multiplicateur glace
      CD2D_TMP_CMULT_LIT = VPRGL(IP+19)   ! coef. multiplicateur lit

      IERR = ERR_TYP()
      RETURN
      END

