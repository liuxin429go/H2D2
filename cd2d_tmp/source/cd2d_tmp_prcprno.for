C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_TMP_PRCPRNO
C
C Description:
C     Pré-traitement au calcul des propriétés nodales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_TMP_PRCPRNO (VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_PRCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     APPELLE LE PARENT
      CALL CD2D_BSEN_PRCPRNO(VCORG,
     &                       KNGV,
     &                       VDJV,
     &                       VPRGL,
     &                       VPRNO,
     &                       IERR)

C---     CALCUL DES TERMES PUITS-SOURCES
      CALL CD2D_TMP_PRCTPS (VPRGL,
     &                      VPRNO)

      RETURN
      END

C************************************************************************
C Sommaire :  CD2D_TMP_PRCTPS
C
C Description:
C     CALCUL DU TERME PUITS-SOURCE DE FLUX DE CHALEUR
C
C Entrée: VPRGL,VDLG,VPRNO
C
C Sortie: VPRNO
C
C Notes:
C     EVALUATION DES COEFFICIENTS TRANSITOIRES A(t) ET B(t) QUI ENTRENT
C     DANS L'EXPRESSION DU FLUX TOTAL S = A(t)*T + B(t).
C
C************************************************************************
      SUBROUTINE CD2D_TMP_PRCTPS(VPRGL, VPRNO)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8 VPRGL(LM_CMMN_NPRGL)
      REAL*8 VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)

      INCLUDE 'cd2d_tmp.fc'

      INTEGER IN
      INTEGER IPA, IPB
      REAL*8  AT, BT
      REAL*8  A(8), B(8)
      REAL*8  RCP, UN_RCP
C-----------------------------------------------------------------------

C---     RHO * CP
      RCP = CD2D_TMP_RHOE * CD2D_TMP_CP
      UN_RCP = UN / RCP

C---     INDICES DANS VPRNO
      IPA = LM_CMMN_NPRNOL + 6
      IPB = LM_CMMN_NPRNOL + 7

      DO IN =1,EG_CMMN_NNL

C---        Calcul les flux
         CALL CD2D_TMP_CLCFLX(VPRNO(1, IN), A, B)

C---        Calcul des coefficients AT et BT (AT*T + BT)
         AT = A(1) + A(2) + A(3) + A(4) + A(5) + A(6) + A(7) + A(8)
         BT = B(1) + B(2) + B(3) + B(4) + B(5) + B(6) + B(7) + B(8)

C---        Chargement de vprno
         VPRNO(IPA,IN) = AT * UN_RCP
         VPRNO(IPB,IN) = BT * UN_RCP

      ENDDO

      RETURN
      END

