C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_TMP_PRNPRGL
C
C Description:
C     IMPRESSION DES PROPRIETES GLOBALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_TMP_PRNPRGL(NPRGL,
     &                            VPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_PRNPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INTEGER NPRGL
      REAL*8  VPRGL(LM_CMMN_NPRGL)

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER I
C-----------------------------------------------------------------------

C---     APPEL LE PARENT
      CALL CD2D_BSE_PRNPRGL(LM_CMMN_NPRGL, VPRGL)

C---     IMPRESSION DE L'ENTETE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_TEMPERATURE_LUES (SI):')

C---     IMPRESSION DE L'INFO
      CALL LOG_INCIND()
      I = CD2D_BSE_NPRGLL
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_RHO_EAU',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CP_EAU',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_RHO_GLACE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CP_GLACE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CHALEUR_FUSION_GLACE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_PRESSION_ATMOSPHERIQUE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_MODELE_EVAPORATION',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_COEFF_BOWEN',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_TEMPERATURE_FOND',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_COEF_LINEARISATION_A',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_COEF_LINEARISATION_B',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_RADIATION_SOLAIRE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_OL_ATMOSPHERE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_OL_SURFACE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_EVAPORATION',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_CONVECTION',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_PRECIPITATION',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_GLACE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_LIT',VPRGL(I))
      CALL LOG_DECIND()

      RETURN
      END

