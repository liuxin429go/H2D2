//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#include "c_netcdf.h"

#include "netcdf.h"

#include <assert.h>
#include <string.h>

// ---  Redéfinition de std::min local à cause de msvc win64
template<typename Tp> inline const Tp& std_min(const Tp& l, const Tp& r)
{
   return (l < r) ? l : r;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_NETCDF_ERRMSG(fint_t* stat,
                                                              F2C_CONF_STRING  str
                                                              F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_NETCDF_ERRMSG(fint_t* stat,
                                                              F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l = len;
#else
   char* sP = str->strP;
   int   l = str->len;
#endif
#ifdef MODE_DEBUG
   assert(l >= 0);
#endif   // MODE_DEBUG

   memset(sP, ' ', l);
   if (*stat != NC_NOERR)
   {
      const char* text = nc_strerror(*stat);
      memcpy(sP, text, std_min(static_cast<size_t>(l), strlen(text)));
   }

   return 0;
}

//************************************************************************
// Sommaire:   Ouvre un fichier NetCDF.
//
// Description:
//    La fonction <code>C_NETCDF_OPEN(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_NETCDF_OPEN(hndl_t*  fHandle,
                                                            F2C_CONF_STRING  fic,
                                                            fint_t*  mode
                                                            F2C_CONF_SUP_INT len)
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* fP = fic;
   int   l = len;
#else
   char* fP = fic->strP;
   int   l = fic->len;
#endif

   enum Mode { MODE_LECTURE = 1, MODE_ECRITURE = 2, MODE_AJOUT = 3 };

   int  stat;        /* return status */
   int  ncid = 0;    /* netCDF id */
               
   if (l < 1024)
   {
      char nom[1024];
      strncpy(nom, fP, l);
      nom[l] = 0x0;

      switch (*mode)
      {
      case MODE_LECTURE:
         stat = nc_open(nom, NC_CLOBBER | NC_64BIT_OFFSET, &ncid);
         break;
      case MODE_ECRITURE:
         stat = nc_create(nom, NC_CLOBBER | NC_64BIT_OFFSET, &ncid);
         break;
      case MODE_AJOUT:
         stat = nc_open(nom, NC_CLOBBER | NC_64BIT_OFFSET, &ncid);
         break;
      }
   }

   return stat;
}
