//************************************************************************
// H2D2 - External declaration of public symbols
// Module: cd2d_mes3
// Entry point: extern "C" void fake_dll_cd2d_mes3()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:58:48.594000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class CD2D_MES3
F_PROT(CD2D_MES3_ASGCMN, cd2d_mes3_asgcmn);
F_PROT(CD2D_MES3_REQNFN, cd2d_mes3_reqnfn);
F_PROT(CD2D_MES3_REQPRM, cd2d_mes3_reqprm);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_cd2d_mes3()
{
   static char libname[] = "cd2d_mes3";
 
   // ---  class CD2D_MES3
   F_RGST(CD2D_MES3_ASGCMN, cd2d_mes3_asgcmn, libname);
   F_RGST(CD2D_MES3_REQNFN, cd2d_mes3_reqnfn, libname);
   F_RGST(CD2D_MES3_REQPRM, cd2d_mes3_reqprm, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
