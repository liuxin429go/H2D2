C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C
C Description :
C     Fournit les routines pour obtenir les point de Gauss
C     et les poids associés pour un élément T6L.
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     Le bloc de données NI_T6L_IP2 initialise les point de Gauss et les
C     poids associés pour l'intégration sur un T6L. C'est intégration à
C     1 point sur chaque sous-triangle.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Dhatt, Touzot, Lefrançois, p 352,367.
C************************************************************************
      BLOCK DATA NI_T6L_IP2

      IMPLICIT NONE

      INCLUDE 'ni_t6l_ip2.fc'

      REAL*8 R_1_6
      REAL*8 R_2_6
      REAL*8 R_4_6
      REAL*8 R_1_8

      PARAMETER (R_1_6 = 1.0D-00 / 6.0D-00,
     &           R_2_6 = 2.0D-00 / 6.0D-00,
     &           R_4_6 = 4.0D-00 / 6.0D-00,
     &           R_1_8 = 0.1250000000000000D-00)
C------------------------------------------------------------------------

      DATA NI_T6L_IP2_CPG
     &     /
     &     R_1_6, R_1_6,
     &     R_4_6, R_1_6,
     &     R_1_6, R_4_6,
     &     R_2_6, R_2_6
     &     /

      DATA NI_T6L_IP2_WPG
     &      /
     &       R_1_8,
     &       R_1_8,
     &       R_1_8,
     &       R_1_8
     &      /

      END

C************************************************************************
C Sommaire: NI_T6L_IP2_REQNPG
C
C Description:
C     La fonction NI_T6L_IP2_REQNPTS retourne le nombre de points de Gauss.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NI_T6L_IP2_REQNPG()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NI_T6L_IP2_REQNPG
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ni_t6l_ip2.fi'
      INCLUDE 'ni_t6l_ip2.fc'
C------------------------------------------------------------------------

      NI_T6L_IP2_REQNPG = NI_T6L_IP2_NPG
      RETURN
      END

C************************************************************************
C Sommaire: NI_T6L_IP2_REQCPG
C
C Description:
C     La fonction NI_T6L_IP2_REQCPG(...) retourne les coordonnées du point
C     d'intégration d'indice "I".
C
C Entrée:
C     I    : indice du point pour lequel on souhaite obtenir la coordonnée
C     NDIM : nombre de dimensions attendues pour la coordonnée
C
C Sortie:
C     VCPG : Coordonnée du point sous la forme (xi, eta, zeta)
C
C
C Notes:
C************************************************************************
      FUNCTION  NI_T6L_IP2_REQCPG(I, NDIM, VCPG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NI_T6L_IP2_REQCPG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I
      INTEGER NDIM
      REAL*8  VCPG(NDIM)

      INCLUDE 'ni_t6l_ip2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ni_t6l_ip2.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GE. 1 .AND. I .LE. NI_T6L_IP2_NPG)
D     CALL ERR_PRE(NDIM .GE. NI_T6L_IP2_NDIM)
C------------------------------------------------------------------------

      VCPG(1) = NI_T6L_IP2_CPG(1,I)
      VCPG(2) = NI_T6L_IP2_CPG(2,I)

      NI_T6L_IP2_REQCPG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NI_T6L_IP2_REQWPG(W,I)
C
C Description:
C     La fonction NI_T6L_IP2_REQWPG retourne le poids du point I
C     pour effectuer l'intégration numérique.
C
C Entrée:
C     I : indice du point d'intégration concerné
C
C Sortie:
C     W : poids du point d'intégration I
C
C Notes:
C************************************************************************
      FUNCTION NI_T6L_IP2_REQWPG(I, W)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NI_T6L_IP2_REQWPG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I
      REAL*8  W

      INCLUDE 'ni_t6l_ip2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ni_t6l_ip2.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GE. 1 .AND. I .LE. NI_T6L_IP2_NPG)
C------------------------------------------------------------------------

      W = NI_T6L_IP2_WPG(I)

      NI_T6L_IP2_REQWPG = ERR_TYP()
      RETURN
      END

