C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SKTR_CMD(IPRM)
CDEC$ATTRIBUTES DLLEXPORT :: IC_SKTR_CMD

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icsktr.fi'
      INCLUDE 'grgrid.fi'
      INCLUDE 'mrskit.fi'
      INCLUDE 'mrprxy.fi'
      INCLUDE 'prprec.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HRES
      INTEGER HGRD
      INTEGER HPRC
      INTEGER IDEB3, IFIN3
C------------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SKTR_AID()
            GOTO 9999
         ENDIF
      ENDIF

C-------  EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CMD_SPARSKIT'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LIS LES PARAM
C     <comment>Handle on the grid</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HGRD)
C     <comment>Handle on the preconditioner</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HPRC)
      IF (IERR .NE. 0) GOTO 9900
      IDEB3 = SP_STRN_TOK(IPRM, ',', 3)
      IFIN3 = SP_STRN_LEN(IPRM)
      IF (IDEB3 .LE. 0) GOTO 9900
      IF (IFIN3 .LT. IDEB3) GOTO 9900

C---     CONTROLE QUE LES PARAM SONT VALIDES
      IF (.NOT. GR_GRID_HVALIDE(HGRD)) GOTO 9901
C      IF (.NOT. PR_PREC_HVALIDE(HPRC)) GOTO 9901

C---     CONSTRUIS ET INITIALISE L'OBJET CONCRET
      HRES = 0
      IF (ERR_GOOD()) IERR = MR_SKIT_CTR(HRES)
      IF (ERR_GOOD()) IERR = MR_SKIT_INI(HRES,
     &                                   HGRD,
     &                                   HPRC,
     &                                   IPRM(IDEB3:IFIN3))

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = MR_PRXY_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = MR_PRXY_INI(HOBJ, HRES)

C-------  IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_PRECOND[25] = ',
C     &                                 NPREC
C         CALL LOG_ECRIS(LOG_BUF)
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_REDEMARAGES[25] = ',
C     &                                 NRDEM
C         CALL LOG_ECRIS(LOG_BUF)
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_ITERATIONS[25] = ',
C     &                                 NITER
C         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C	     <comment>Return value: Handle on the solver</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, *) 'ERR_PARAMETRES_INVALIDES',': ',
     &                  IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SKTR_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_SKTR_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SKTR_REQCMD()
CDEC$ATTRIBUTES DLLEXPORT :: IC_SKTR_REQCMD

      IMPLICIT NONE

      INCLUDE 'icsktr.fi'
C-------------------------------------------------------------------------

C   <comment>
C   The command <b>skit_solver</b> constructs the SPARSKIT iteration
C   solver (external package). (Status: prototype)
C   </comment>
      IC_SKTR_REQCMD = 'skit_solver'

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SKTR_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icsktr.hlp')

      RETURN
      END

