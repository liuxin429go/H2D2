C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Critère d'Arrêt
C Objet:   CRItère d'Arrêt
C Type:    Virtuel
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à CA_CRIA_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA CA_CRIA_DATA

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'cacria.fc'

      DATA CA_CRIA_HBASE  / OB_OBJC_HBSP_TAG /

      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CA_CRIA_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_CRIA_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacria.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CA_CRIA_NOBJMAX,
     &                   CA_CRIA_HBASE,
     &                   'Proxy: Critère d''arrêt')

      CA_CRIA_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_CRIA_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacria.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fc'

      INTEGER  IERR
      EXTERNAL CA_CRIA_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CA_CRIA_NOBJMAX,
     &                   CA_CRIA_HBASE,
     &                   CA_CRIA_DTR)

      CA_CRIA_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_CRIA_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacria.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CA_CRIA_NOBJMAX,
     &                   CA_CRIA_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CA_CRIA_HVALIDE(HOBJ))
         IOB = HOBJ - CA_CRIA_HBASE

         CA_CRIA_CRIA(IOB) = 1.0D99
         CA_CRIA_HOMG(IOB) = 0
         CA_CRIA_HMDL(IOB) = 0
      ENDIF

      CA_CRIA_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_CRIA_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacria.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(CA_CRIA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CA_CRIA_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CA_CRIA_NOBJMAX,
     &                   CA_CRIA_HBASE)

      CA_CRIA_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HOMG        Handle de l'Objet ManaGé
C     NOMMDL      Nom du module
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_CRIA_INI(HOBJ, HOMG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_INI
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HOMG

      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'cacria.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HMDL
      INTEGER  HFNC
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_CRIA_HVALIDE(HOBJ))
D     CALL ERR_PRE(HOMG .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = CA_CRIA_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - CA_CRIA_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HOMG, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      HVALIDE = .FALSE.
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HOMG)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         CA_CRIA_CRIA(IOB) = 1.0D99
         CA_CRIA_HOMG(IOB) = HOMG
         CA_CRIA_HMDL(IOB) = HMDL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HOMG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CA_CRIA_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_CRIA_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_RST
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'cacria.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HFNC
      INTEGER  HMDL
      INTEGER  HOMG
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_CRIA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - CA_CRIA_HBASE
      HOMG = CA_CRIA_HOMG(IOB)
      HMDL = CA_CRIA_HMDL(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      HVALIDE = .FALSE.
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     FAIT L'APPEL POUR DÉTRUIRE L'OBJET ASSOCIÉ
      HFNC = 0
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      CA_CRIA_CRIA(IOB) = 1.0D99
      CA_CRIA_HOMG(IOB) = 0
      CA_CRIA_HMDL(IOB) = 0

      CA_CRIA_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CA_CRIA_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_CRIA_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacria.fi'
      INCLUDE 'cacria.fc'
C------------------------------------------------------------------------

      CA_CRIA_REQHBASE = CA_CRIA_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CA_CRIA_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_CRIA_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacria.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cacria.fc'
C------------------------------------------------------------------------

      CA_CRIA_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CA_CRIA_NOBJMAX,
     &                                  CA_CRIA_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>CA_CRIA_CALCRI(...)</code> retourne .TRUE.
C     si le critère d'arrêt est satisfait.
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_CRIA_CALCRI(HOBJ, NDLN, NNL, VDLG, VDEL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_CALCRI
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)

      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'cacria.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
      REAL*8   CRIA
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_CRIA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - CA_CRIA_HBASE

      HOMG = CA_CRIA_HOMG(IOB)
      HMDL = CA_CRIA_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'CALCRI', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL6(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(NDLN),
     &                        SO_ALLC_CST2B(NNL),
     &                        SO_ALLC_CST2B(VDLG(1,1)),
     &                        SO_ALLC_CST2B(VDEL(1,1)),
     &                        SO_ALLC_CST2B(CRIA))
      ENDIF

C---     CONSERVE LE RESULTAT
      IF (ERR_GOOD()) CA_CRIA_CRIA(IOB) = CRIA

      CA_CRIA_CALCRI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est convergé.
C
C Description:
C     La fonction <code>CA_CRIA_ESTNIVEAU(...)</code> retourne .TRUE.
C     si le critère d'arrêt donné par LVL est satisfait.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_CRIA_ESTNIVEAU(HOBJ, VLVL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_ESTNIVEAU
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VLVL

      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_CRIA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CA_CRIA_ESTNIVEAU = (CA_CRIA_REQVAL(HOBJ) .LT. VLVL)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>CA_CRIA_ESTCONVERGE(...)</code> retourne .TRUE.
C     si le critère d'arrêt est satisfait.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_CRIA_ESTCONVERGE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_ESTCONVERGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_CRIA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CA_CRIA_ESTCONVERGE = CA_CRIA_ESTNIVEAU(HOBJ,CA_CRIA_CONV_NORMALE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>CA_CRIA_REQVAL(...)</code> retourne .TRUE.
C     si le critère d'arrêt est satisfait.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_CRIA_REQVAL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_CRIA_REQVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_CRIA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CA_CRIA_REQVAL = CA_CRIA_CRIA(HOBJ-CA_CRIA_HBASE)
      RETURN
      END
