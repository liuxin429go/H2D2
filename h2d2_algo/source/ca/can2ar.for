C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Critère d'Arret
C Objet:   Norme L2 sur Accroissement Relatif (1 DDL)
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CA_N2AR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_N2AR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_N2AR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'can2ar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'can2ar.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CA_N2AR_NOBJMAX,
     &                   CA_N2AR_HBASE,
     &                   'Stopping Criteria')

      CA_N2AR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_N2AR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_N2AR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'can2ar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'can2ar.fc'

      INTEGER  IERR
      EXTERNAL CA_N2AR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CA_N2AR_NOBJMAX,
     &                   CA_N2AR_HBASE,
     &                   CA_N2AR_DTR)

      CA_N2AR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_N2AR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_N2AR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'can2ar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'can2ar.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CA_N2AR_NOBJMAX,
     &                   CA_N2AR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CA_N2AR_HVALIDE(HOBJ))
         IOB = HOBJ - CA_N2AR_HBASE

         CA_N2AR_EPSA (IOB) = 0.0D0
         CA_N2AR_EPSR (IOB) = 0.0D0
         CA_N2AR_IDDL (IOB) = 0
         CA_N2AR_HNUMR(IOB) = 0
      ENDIF

      CA_N2AR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_N2AR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_N2AR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'can2ar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'can2ar.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_N2AR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CA_N2AR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CA_N2AR_NOBJMAX,
     &                   CA_N2AR_HBASE)

      CA_N2AR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La méthode CA_N2AR_INI initialise et dimensionne l'objet.
C
C Entrée:
C     INTEGER HOBJ      Handle sur l'objet
C     INTEGER HSIM     Handle sur l'élément
C     INTEGER IDDL      Numéro du degré de liberté concerné
C     REAL*8  EPSR      Epsilon relatif
C     REAL*8  EPSA      Epsilon absolu
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_N2AR_INI(HOBJ, HSIM, IDDL, EPSR, EPSA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_N2AR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER IDDL
      REAL*8  EPSR
      REAL*8  EPSA

      INCLUDE 'can2ar.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'can2ar.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER NDLN
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_N2AR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES DONNÉES
      IF (.NOT. LM_HELE_HVALIDE(HSIM)) GOTO 9900
      NDLN = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NDLN)
      IF (IDDL  .LE.    0) GOTO 9901
      IF (IDDL  .GT. NDLN) GOTO 9901
      IF (EPSA  .LT. 0.0D0) GOTO 9902
      IF (EPSR  .LT. 0.0D0) GOTO 9902
      IF (EPSA  .EQ. 0.0D0 .AND. EPSR .EQ. 0.0D0) GOTO 9902

C---     RESET LES DONNEES
      IERR = CA_N2AR_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - CA_N2AR_HBASE
      CA_N2AR_EPSA (IOB) = EPSA
      CA_N2AR_EPSR (IOB) = EPSR
      CA_N2AR_IDDL (IOB) = IDDL
      CA_N2AR_HNUMR(IOB) = LM_HELE_REQHNUMC(HSIM)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HSIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSIM, 'MSG_SIMULATION')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_INDICE_DDL_INVALIDE',':',
     &                           IDDL, ' / ', NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
9902  WRITE(ERR_BUF, '(2A,I6,A,2PE12.5)') 'ERR_EPS_NEGATIF_NULL',':',
     &                           EPSR, EPSA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)

      GOTO 9999

9999  CONTINUE
      CA_N2AR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_N2AR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_N2AR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'can2ar.fi'
      INCLUDE 'err.fi'
      INCLUDE 'can2ar.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_N2AR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CA_N2AR_HBASE
      CA_N2AR_EPSA (IOB) = 0.0D0
      CA_N2AR_EPSR (IOB) = 0.0D0
      CA_N2AR_IDDL (IOB) = 0
      CA_N2AR_HNUMR(IOB) = 0

      CA_N2AR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CA_N2AR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_N2AR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_N2AR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'can2ar.fi'
      INCLUDE 'can2ar.fc'
C------------------------------------------------------------------------

      CA_N2AR_REQHBASE = CA_N2AR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CA_N2AR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_N2AR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_N2AR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'can2ar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'can2ar.fc'
C------------------------------------------------------------------------

      CA_N2AR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CA_N2AR_NOBJMAX,
     &                                  CA_N2AR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>CA_N2GR_CALCRI(...)</code> retourne .TRUE.
C     si le critère d'arrêt est satisfait.
C     Le critère est la norme L2 sur l'accroissement relatif (1 DDL).
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_N2AR_CALCRI(HOBJ, NDLN, NNL, VDLG, VDEL, CRIA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_N2AR_CALCRI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  CRIA

      INCLUDE 'can2ar.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'can2ar.fc'

      INTEGER IERR

      INTEGER IOB
      INTEGER IDDL
      INTEGER HNUMR
      REAL*8  EPSA, EPSR
      REAL*8  DRM
      LOGICAL FINI
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_N2AR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - CA_N2AR_HBASE
      EPSA  = CA_N2AR_EPSA (IOB)
      EPSR  = CA_N2AR_EPSR (IOB)
      IDDL  = CA_N2AR_IDDL (IOB)
      HNUMR = CA_N2AR_HNUMR(IOB)

C---     Calcule la norme
      DRM = NR_UTIL_N2SR(HNUMR, NDLN, NNL, VDEL, IDDL, VDLG, EPSA, EPSR)

C---     Calcule le critère
      FINI = (DRM .LT. 1.0D0)

      WRITE(LOG_BUF, '(A,1PE14.6E3,A,1PE14.6E3,A,L3)')
     &   'MSG_NRM_L2_1DDL#<35>#= ', DRM, ' /', 1.0D0, ', MSG_OK =', FINI
      CALL LOG_ECRIS(LOG_BUF)

      CRIA = DRM
      CA_N2AR_CALCRI = ERR_TYP()
      RETURN
      END
