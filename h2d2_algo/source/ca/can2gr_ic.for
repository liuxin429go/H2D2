C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CAN2GR_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CAN2GR_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'can2gr_ic.fi'
      INCLUDE 'can2gr.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER, PARAMETER :: NVALMAX = 10
      
      REAL*8  VEPSR(NVALMAX)
      REAL*8  VEPSA(NVALMAX)
      INTEGER IERR
      INTEGER I, IV
      INTEGER HOBJ, HELE, HCRA
      INTEGER NTOK, NVAL
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_CAN2GR_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Les dimensions
      NTOK = SP_STRN_NTOK(IPRM, ',')
      NVAL = (NTOK-1) / 2
      IF (NVAL .GT. NVALMAX) GOTO 9902

C---     Lis les paramètres
      HELE = 0
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = 1
C     <comment>Handle on the element</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HELE)
      DO IV = 1,NVAL
C        <comment>
C        List of epsilon, comma separated. For each degree of freedom,
C        2 values for relative and absolute epsilon. The special case
C        with only 2 values will attribute the same values to all
C        degree of freedom.
C        </comment>
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, VEPSR(IV))
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, VEPSA(IV))
         IF (IERR .NE.  0) GOTO 9901
      ENDDO
      
C---     Construis et initialise l'objet concret
      HCRA = 0
      IF (ERR_GOOD()) IERR = CA_N2GR_CTR(HCRA)
      IF (ERR_GOOD()) IERR = CA_N2GR_INI(HCRA, HELE, NVAL, VEPSR, VEPSA)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = CA_CRIA_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = CA_CRIA_INI(HOBJ, HCRA)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = CA_N2GR_PRN(HCRA)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the stopping criterion</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>cria_l2_allrel</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CAN2GR_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_CAN2GR_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CAN2GR_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CAN2GR_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'can2gr_ic.fi'
      INCLUDE 'can2gr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER      IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(CA_N2GR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CA_N2GR_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(CA_N2GR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CA_N2GR_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_CAN2GR_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CAN2GR_AID()

9999  CONTINUE
      IC_CAN2GR_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CAN2GR_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CAN2GR_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'can2gr_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>cria_ l2_allrel</b> represents the stopping criterion in L2 norm,
C  for all degrees of freedom, for a relative increase.
C  The condition on the solution increment is:
C  <pre>
C  || dU / (eps_r*|u| + eps_a) || < 1.0 </pre>
C  On a dof, it can be expressed as:
C  <pre>
C  dU < eps_r*|u| + eps_a </pre>
C</comment>
      IC_CAN2GR_REQCLS = 'cria_l2_allrel'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CAN2GR_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CAN2GR_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'can2gr_ic.fi'
      INCLUDE 'can2gr.fi'
C-------------------------------------------------------------------------

      IC_CAN2GR_REQHDL = CA_N2GR_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_CAN2GR_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_CAN2GR_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('can2gr_ic.hlp')

      RETURN
      END
