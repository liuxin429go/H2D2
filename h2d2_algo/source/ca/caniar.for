C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Critère d'Arret
C Objet:   Norme Infinie sur Accroissement Relatif (1 ddl)
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CA_NIAR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIAR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIAR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'caniar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'caniar.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CA_NIAR_NOBJMAX,
     &                   CA_NIAR_HBASE,
     &                   'Stopping Criteria')

      CA_NIAR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIAR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIAR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'caniar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'caniar.fc'

      INTEGER  IERR
      EXTERNAL CA_NIAR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CA_NIAR_NOBJMAX,
     &                   CA_NIAR_HBASE,
     &                   CA_NIAR_DTR)

      CA_NIAR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NIAR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIAR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'caniar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'caniar.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CA_NIAR_NOBJMAX,
     &                   CA_NIAR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CA_NIAR_HVALIDE(HOBJ))
         IOB = HOBJ - CA_NIAR_HBASE

         CA_NIAR_EPSA (IOB) = 0.0D0
         CA_NIAR_EPSR (IOB) = 0.0D0
         CA_NIAR_IDDL (IOB) = 0
         CA_NIAR_HNUMR(IOB) = 0
      ENDIF

      CA_NIAR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NIAR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIAR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'caniar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'caniar.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NIAR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CA_NIAR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CA_NIAR_NOBJMAX,
     &                   CA_NIAR_HBASE)

      CA_NIAR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La méthode CA_NIAR_INI initialise et dimensionne l'objet.
C
C Entrée:
C     INTEGER HOBJ      Handle sur l'objet
C     INTEGER HSIM     Handle sur l'élément
C     INTEGER IDDL      Numéro du degré de liberté concerné
C     REAL*8  EPSR      Epsilon relatif
C     REAL*8  EPSA      Epsilon absolu
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIAR_INI(HOBJ, HSIM, IDDL, EPSR, EPSA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIAR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER IDDL
      REAL*8  EPSR
      REAL*8  EPSA

      INCLUDE 'caniar.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'caniar.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER NDLN
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NIAR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES DONNÉES
      IF (.NOT. LM_HELE_HVALIDE(HSIM)) GOTO 9900
      NDLN = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NDLN)
      IF (IDDL  .LE.    0) GOTO 9901
      IF (IDDL  .GT. NDLN) GOTO 9901
      IF (EPSA  .LT. 0.0D0) GOTO 9902
      IF (EPSR  .LT. 0.0D0) GOTO 9902
      IF (EPSA  .EQ. 0.0D0 .AND. EPSR .EQ. 0.0D0) GOTO 9902

C---     RESET LES DONNEES
      IERR = CA_NIAR_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - CA_NIAR_HBASE
      CA_NIAR_EPSA (IOB) = EPSA
      CA_NIAR_EPSR (IOB) = EPSR
      CA_NIAR_IDDL (IOB) = IDDL
      CA_NIAR_HNUMR(IOB) = LM_HELE_REQHNUMC(HSIM)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HSIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSIM, 'MSG_SIMULATION')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_INDICE_DDL_INVALIDE',':',
     &                           IDDL, ' / ', NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
9902  WRITE(ERR_BUF, '(2A,I6,A,2PE12.5)') 'ERR_EPS_NEGATIF_NULL',':',
     &                           EPSR, EPSA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)

      GOTO 9999

9999  CONTINUE
      CA_NIAR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NIAR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIAR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'caniar.fi'
      INCLUDE 'err.fi'
      INCLUDE 'caniar.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NIAR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CA_NIAR_HBASE
      CA_NIAR_EPSA (IOB) = 0.0D0
      CA_NIAR_EPSR (IOB) = 0.0D0
      CA_NIAR_IDDL (IOB) = 0
      CA_NIAR_HNUMR(IOB) = 0

      CA_NIAR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CA_NIAR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIAR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIAR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'caniar.fi'
      INCLUDE 'caniar.fc'
C------------------------------------------------------------------------

      CA_NIAR_REQHBASE = CA_NIAR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CA_NIAR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIAR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIAR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'caniar.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'caniar.fc'
C------------------------------------------------------------------------

      CA_NIAR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CA_NIAR_NOBJMAX,
     &                                  CA_NIAR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>CA_N2GR_CALCRI(...)</code> retourne .TRUE.
C     si le critère d'arrêt est satisfait.
C     Le critère est la norme infinie sur l'accroissement relatif (1 ddl).
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIAR_CALCRI(HOBJ, NDLN, NNL, VDLG, VDEL, CRIA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIAR_CALCRI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  CRIA

      INCLUDE 'caniar.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'caniar.fc'

      INTEGER IERR

      INTEGER IOB
      INTEGER IDDL
      INTEGER HNUMR
      REAL*8  EPSA, EPSR
      REAL*8  DRM
      LOGICAL FINI
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NIAR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - CA_NIAR_HBASE
      EPSA  = CA_NIAR_EPSA (IOB)
      EPSR  = CA_NIAR_EPSR (IOB)
      IDDL  = CA_NIAR_IDDL (IOB)
      HNUMR = CA_NIAR_HNUMR(IOB)

C---     Calcule la norme
      DRM = NR_UTIL_NISR(HNUMR, NDLN, NNL, VDEL, IDDL, VDLG, EPSA, EPSR)

C---     Calcule le critère
      FINI = (DRM .LT. 1.0D0)

      WRITE(LOG_BUF, '(A,1PE14.6E3,A,1PE14.6E3,A,L3)')
     &   'MSG_NRM_MAX_1DDL#<35>#= ', DRM,' /', 1.0D0,', MSG_OK =',FINI
      CALL LOG_ECRIS(LOG_BUF)

      CRIA = DRM
      CA_NIAR_CALCRI = ERR_TYP()
      RETURN
      END
