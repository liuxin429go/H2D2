C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Critère d'Arret
C Objet:   Norme Infinie sur accroissement Global Relatif (tous les DDL)
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CA_NIGR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIGR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIGR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'canigr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canigr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CA_NIGR_NOBJMAX,
     &                   CA_NIGR_HBASE,
     &                   'Stopping Criteria')

      CA_NIGR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIGR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIGR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'canigr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canigr.fc'

      INTEGER  IERR
      EXTERNAL CA_NIGR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CA_NIGR_NOBJMAX,
     &                   CA_NIGR_HBASE,
     &                   CA_NIGR_DTR)

      CA_NIGR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NIGR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIGR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canigr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canigr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CA_NIGR_NOBJMAX,
     &                   CA_NIGR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CA_NIGR_HVALIDE(HOBJ))
         IOB = HOBJ - CA_NIGR_HBASE

         CA_NIGR_VEPSA(:,IOB) = 0.0D0
         CA_NIGR_VEPSR(:,IOB) = 0.0D0
         CA_NIGR_NEPS (IOB) = 0
         CA_NIGR_HELE (IOB) = 0
      ENDIF

      CA_NIGR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NIGR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIGR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canigr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canigr.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NIGR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CA_NIGR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CA_NIGR_NOBJMAX,
     &                   CA_NIGR_HBASE)

      CA_NIGR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NIGR_INI(HOBJ, HSIM, NEPS, VEPSR, VEPSA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIGR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NEPS
      REAL*8  VEPSR(NEPS)
      REAL*8  VEPSA(NEPS)

      INCLUDE 'canigr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'canigr.fc'

      INTEGER IOB
      INTEGER IV
      INTEGER IERR
      INTEGER NDLN
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NIGR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (.NOT. LM_ELEM_HVALIDE(HSIM)) GOTO 9900
      NDLN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      IF (NEPS .NE. 1 .AND. NEPS .NE. NDLN) GOTO 9901
      IF (NDLN .GT. CA_NIGR_NEPSMAX) GOTO 9902
      DO IV=1,NEPS
         IF (VEPSR(IV)  .LT. 0.0D0)   GOTO 9903
         IF (VEPSA(IV)  .LT. 1.0D-16) GOTO 9904
      ENDDO

C---     Reset les données
      IERR = CA_NIGR_RST(HOBJ)

C---     Assigne les attributs
      IOB = HOBJ - CA_NIGR_HBASE
      IF (NEPS .EQ. 1) THEN
         CA_NIGR_VEPSA(1:NDLN,IOB) = VEPSA(1)
         CA_NIGR_VEPSR(1:NDLN,IOB) = VEPSR(1)
      ELSE
         CA_NIGR_VEPSA(:,IOB) = VEPSA(:)
         CA_NIGR_VEPSR(:,IOB) = VEPSR(:)
      ENDIF
      CA_NIGR_NEPS (IOB) = NDLN
      CA_NIGR_HELE (IOB) = HSIM

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HSIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSIM, 'MSG_SIMULATION')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A)') 'ERR_NOMBRE_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_OBTIENT', ': ', NEPS
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_ATTEND', ': ', NDLN 
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A)') 'ERR_NOMBRE_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_OBTIENT', ': ', NDLN
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_MAX', ': ', CA_NIGR_NEPSMAX 
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I3,1PE14.6E3)') 'ERR_EPSR_NEGATIF_NULL',':',
     &                           IV, VEPSR(IV)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I3,1PE14.6E3)') 'ERR_EPSA_TROP_PETIT',':',
     &                           IV, VEPSA(IV)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CA_NIGR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NIGR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIGR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canigr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canigr.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NIGR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CA_NIGR_HBASE
      CA_NIGR_VEPSA(:,IOB) = 0.0D0
      CA_NIGR_VEPSR(:,IOB) = 0.0D0
      CA_NIGR_NEPS (IOB) = 0
      CA_NIGR_HELE (IOB) = 0

      CA_NIGR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CA_NIGR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIGR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIGR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'canigr.fi'
      INCLUDE 'canigr.fc'
C------------------------------------------------------------------------

      CA_NIGR_REQHBASE = CA_NIGR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CA_NIGR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIGR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIGR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canigr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'canigr.fc'
C------------------------------------------------------------------------

      CA_NIGR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CA_NIGR_NOBJMAX,
     &                                  CA_NIGR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction CA_NIGR_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NIGR_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canigr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'canigr.fc'

      INTEGER I, IOB
      INTEGER IERR
      INTEGER HSIM
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NIGR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CA_NIGR_HBASE

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_CRITERE_ARRET' //
     &                       '_INFINI_ACCROISSEMENT_RELATIF_TOUS_DDL'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Impression des paramètres du bloc
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)
      IERR = OB_OBJC_REQNOMCMPL(NOM, CA_NIGR_HELE(IOB))
      WRITE (LOG_BUF,'(A,A)') 'MSG_SIMULATION#<35>#= ',
     &                         NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A,2(1PE14.6E3))')
     &   'MSG_PRECISION (rel, abs)#<35>#= ', 
     &   CA_NIGR_VEPSR(1,IOB), 
     &   CA_NIGR_VEPSA(1,IOB)
      CALL LOG_ECRIS(LOG_BUF)
      DO I=2, CA_NIGR_NEPS(IOB)
         WRITE (LOG_BUF,'(A,2(1PE14.6E3))') 
     &      '#<35>#  ',
     &      CA_NIGR_VEPSR(I,IOB), 
     &      CA_NIGR_VEPSA(I,IOB)
         CALL LOG_ECRIS(LOG_BUF)
      ENDDO

      CA_NIGR_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Calcule le critère d'arrêt.
C
C Description:
C     La fonction <code>CA_NIGR_CALCRI(...)</code> calcule le critère d'arrêt.
C     Le critère est la norme infinie sur l'accroissement global relatif
C     (tous les DDL).
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C
C Sortie:
C     CRIA        La valeur du critère
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NIGR_CALCRI(HOBJ, NDLN, NNL, VDLG, VDEL, CRIA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NIGR_CALCRI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN,NNL)
      REAL*8  VDEL(NDLN,NNL)
      REAL*8  CRIA

      INCLUDE 'canigr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'canigr.fc'

      REAL*8  DRM
      INTEGER IERR
      INTEGER IOB
      INTEGER NEPS
      INTEGER HELE
      INTEGER HNUMR
      LOGICAL FINI
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NIGR_HVALIDE(HOBJ))
D     CALL ERR_PRE(NDLN .EQ. CA_NIGR_NEPS(HOBJ-CA_NIGR_HBASE))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - CA_NIGR_HBASE
      NEPS  = CA_NIGR_NEPS(IOB)
      HELE  = CA_NIGR_HELE(IOB)
      HNUMR = LM_ELEM_REQHNUMC(HELE)

C---     Contrôle des données
      IF (NDLN .NE. NEPS) GOTO 9900

C---     Calcule la norme
      DRM = NR_UTIL_NIGR(HNUMR, NDLN, NNL, VDEL, VDLG,
     &                   CA_NIGR_VEPSA(:,IOB),
     &                   CA_NIGR_VEPSR(:,IOB))

C---     Calcule le critère
      FINI = (DRM .LT. 1.0D0)

C---     Log
      WRITE(LOG_BUF, '(A,1PE14.6E3,A,1PE14.6E3,A,L3)')
     &   'MSG_NRM_MAX_GLB#<35>#= ', DRM, ' /', 1.0D0, ', MSG_OK =', FINI
      CALL LOG_ECRIS(LOG_BUF)

      CRIA = DRM
      
      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_NOMBRE_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_OBTIENT', ': ', NEPS
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_ATTEND', ': ', NDLN 
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CA_NIGR_CALCRI = ERR_TYP()
      RETURN
      END
