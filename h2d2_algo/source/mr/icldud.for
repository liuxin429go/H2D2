C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_LDUD_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LDUD_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icldud.fi'
      INCLUDE 'mrldud.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icldud.fc'

      INTEGER IERR
      INTEGER HOBJ, HRES
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_LDUD_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     CONTROLE LES PARAM
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     CONSTRUIS ET INITIALISE L'OBJET
      HRES = 0
      IF (ERR_GOOD()) IERR = MR_LDUD_CTR(HRES)
      IF (ERR_GOOD()) IERR = MR_LDUD_INI(HRES)

C---     Construis et initialise le parent-proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = MR_RESO_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = MR_RESO_INI(HOBJ, HRES)

C---      IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
         IERR = IC_LDUD_PRN(HRES)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
         WRITE(IPRM, '(2A,I12)') 'H', ',', HRES
      ENDIF

C<comment>
C  The constructor <b>ldu_disk</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_LDUD_AID()

9999  CONTINUE
      IC_LDUD_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_LDUD_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LDUD_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mrldud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icldud.fc'

      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_LDU_DISQUE'
      CALL LOG_ECRIS(LOG_BUF)

C---     Impression des paramètres du bloc
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                             NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      IC_LDUD_PRN = ERR_TYP()
      RETURN
      END


C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_LDUD_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LDUD_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'icldud.fi'
      INCLUDE 'mrldud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = MR_LDUD_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = MR_LDUD_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test MR_LDUD_PRN(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_LDUD_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_LDUD_AID()

9999  CONTINUE
      IC_LDUD_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LDUD_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LDUD_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icldud.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>ldu_disk</b> represents the LDU direct resolution method, with
C  storage of the factorized matrix on disk. It is a N^2 method and should
C  only be used for small problems.
C</comment>
      IC_LDUD_REQCLS = 'ldu_disk'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LDUD_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LDUD_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icldud.fi'
      INCLUDE 'mrldud.fi'
C-------------------------------------------------------------------------

      IC_LDUD_REQHDL = MR_LDUD_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_LDUD_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_LDUD_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icldud.hlp')

      RETURN
      END

