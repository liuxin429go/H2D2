C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER MR_LDUD_000
C     INTEGER MR_LDUD_999
C     INTEGER MR_LDUD_CTR
C     INTEGER MR_LDUD_DTR
C     INTEGER MR_LDUD_INI
C     INTEGER MR_LDUD_RST
C     INTEGER MR_LDUD_REQHBASE
C     LOGICAL MR_LDUD_HVALIDE
C     LOGICAL MR_LDUD_ESTDIRECTE
C     INTEGER MR_LDUD_ASMMTX
C     INTEGER MR_LDUD_FCTMTX
C     INTEGER MR_LDUD_RESMTX
C     INTEGER MR_LDUD_XEQ
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MR_LDUD_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUD_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrldud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldud.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_LDUD_NOBJMAX,
     &                   MR_LDUD_HBASE,
     &                   'Matrix Solver LDU-Disk')

      MR_LDUD_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUD_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrldud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldud.fc'

      INTEGER  IERR
      EXTERNAL MR_LDUD_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_LDUD_NOBJMAX,
     &                   MR_LDUD_HBASE,
     &                   MR_LDUD_DTR)

      MR_LDUD_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LDUD_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldud.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_LDUD_NOBJMAX,
     &                   MR_LDUD_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_LDUD_HVALIDE(HOBJ))
         IOB = HOBJ - MR_LDUD_HBASE

         MR_LDUD_HMTX (IOB) = 0
         MR_LDUD_HASM (IOB) = 0
         MR_LDUD_LIASL(IOB) = 0
      ENDIF

      MR_LDUD_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LDUD_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldud.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_LDUD_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_LDUD_NOBJMAX,
     &                   MR_LDUD_HBASE)

      MR_LDUD_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LDUD_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'mrldud.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ILU
      INTEGER HASM
      INTEGER HMTX
      PARAMETER (ILU = 0)
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MR_LDUD_RST(HOBJ)

C---     CONTROLES
      IF (ERR_GOOD()) THEN
         IF (MP_UTIL_ESTMPROC()) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_METHODE_RESO_INVALIDE_EN_MPROC')
         ENDIF
      ENDIF

C---     INITIALISE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_MORS_INI(HMTX, ILU)

C---     INITIALISE L'ASSEMBLEUR
      IF (ERR_GOOD()) IERR = MA_MORS_CTR(HASM)
      IF (ERR_GOOD()) IERR = MA_MORS_INI(HASM, HMTX)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_LDUD_HBASE
         MR_LDUD_HMTX (IOB) = HMTX
         MR_LDUD_HASM (IOB) = HASM
         MR_LDUD_LIASL(IOB) = 0
      ENDIF

      MR_LDUD_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LDUD_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrldud.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HASM
      INTEGER HMTX
      INTEGER LIASL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MR_LDUD_HBASE

      HASM = MR_LDUD_HASM(IOB)
      IF (MA_MORS_HVALIDE(HASM)) IERR = MA_MORS_DTR(HASM)

      HMTX = MR_LDUD_HMTX(IOB)
      IF (MX_MORS_HVALIDE(HMTX)) IERR = MX_MORS_DTR(HMTX)

      LIASL = MR_LDUD_LIASL(IOB)
      IF (LIASL .NE. 0) IERR = SO_ALLC_ALLINT(0, LIASL)

      MR_LDUD_HMTX (IOB) = 0
      MR_LDUD_HASM (IOB) = 0
      MR_LDUD_LIASL(IOB) = 0

      MR_LDUD_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MR_LDUD_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUD_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrldud.fi'
      INCLUDE 'mrldud.fc'
C------------------------------------------------------------------------

      MR_LDUD_REQHBASE = MR_LDUD_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_LDUD_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUD_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrldud.fc'
C------------------------------------------------------------------------

      MR_LDUD_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_LDUD_NOBJMAX,
     &                                  MR_LDUD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la méthode de résolution est directe
C
C Description:
C     La fonction MR_LDUD_ESTDIRECTE retourne .TRUE. si la méthode de
C     résolution est directe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUD_ESTDIRECTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_ESTDIRECTE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldud.fi'
C------------------------------------------------------------------------

      MR_LDUD_ESTDIRECTE = .TRUE.
      RETURN
      END

C************************************************************************
C Sommaire: Assemble le système matriciel.
C
C Description:
C     La fonction MR_LDUD_ASMMTX assemble le système matriciel.
C     La matrice est dimensionnée et assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C     La factorisation frontal détruis les tables de pointeurs!
C     On force le recalcul en initialisant la matrice.
C     On pourrait également stocker les tables
C
C************************************************************************
      FUNCTION MR_LDUD_ASMMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_K,
     &                        F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_ASMMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'mrldud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrldud.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HASM, HMTX
      INTEGER LIASL
      INTEGER NEQL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Demarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.assemblage')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_LDUD_HBASE
      HMTX = MR_LDUD_HMTX (IOB)
      HASM = MR_LDUD_HASM (IOB)
      LIASL= MR_LDUD_LIASL(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))

C---     DIMENSIONNE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_DIMMAT(HMTX, HSIM)

C---     DIMENSIONNE LA TABLE IA SKY-LINE
      IF (ERR_GOOD()) THEN
         NEQL = MX_MORS_REQNEQL(HMTX)
         IERR = SO_ALLC_ALLINT(NEQL+1, LIASL)
         IF (ERR_GOOD()) MR_LDUD_LIASL(IOB) = LIASL
      ENDIF

C---     ASSEMBLE LA MATRICE
      IERR = MA_MORS_ASMMTX(HASM, HSIM, HALG, F_K)

      CALL TR_CHRN_STOP('h2d2.reso.mat.assemblage')
      MR_LDUD_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise le système matriciel
C
C Description:
C     La fonction MR_LDUD_FCTMTX factorise le système matriciel. La matrice
C     doit être assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUD_FCTMTX(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_FCTMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'mrldud.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spldud.fi'
      INCLUDE 'mrldud.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER LIASL, LIAP, LJAP, LKG
      INTEGER NEQL, NKGP
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Demarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.factorisation')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_LDUD_HBASE
      HMTX  = MR_LDUD_HMTX (IOB)
      LIASL = MR_LDUD_LIASL(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(LIASL .NE. 0)

C---     RECUPERE LES POINTEURS
      IF (ERR_GOOD()) THEN
         NEQL = MX_MORS_REQNEQL(HMTX)
         NKGP = MX_MORS_REQNKGP(HMTX)
         LIAP = MX_MORS_REQLIAP(HMTX)
         LJAP = MX_MORS_REQLJAP(HMTX)
         LKG  = MX_MORS_REQLKG (HMTX)
      ENDIF

C---     FACTORISE
      IF (ERR_GOOD()) THEN
         IERR = SP_LDUD_FACT(NEQL,
     &                       NKGP,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       VA(SO_ALLC_REQVIND(VA,LKG)),
     &                       KA(SO_ALLC_REQKIND(KA,LIASL)))
      ENDIF

      CALL TR_CHRN_STOP('h2d2.reso.mat.factorisation')
      MR_LDUD_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Resous le système matriciel
C
C Description:
C     La fonction MR_LDUD_RESMTX résout le système matriciel avec
C     un second membre. Le second membre est assemblé si les handles
C     sur la simulation HSIM et sur l'algorithme HALG sont tous deux non nuls,
C     sinon on résout directement avec VSOL.
C     La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du systeme matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUD_RESMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_F,
     &                        VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_RESMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_F

      INCLUDE 'mrldud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spldud.fi'
      INCLUDE 'mrldud.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HASM, HMTX
      INTEGER LIASL, LKG
      INTEGER NEQL, NKGP
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Demarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.resolution')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_LDUD_HBASE
      HMTX  = MR_LDUD_HMTX(IOB)
      HASM = MR_LDUD_HASM (IOB)
      LIASL= MR_LDUD_LIASL(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))
D     CALL ERR_ASR(LIASL .NE. 0)

C---     RECUPERE LES POINTEURS
      IF (ERR_GOOD()) THEN
         NEQL = MX_MORS_REQNEQL(HMTX)
         NKGP = MX_MORS_REQNKGP(HMTX)
         LKG  = MX_MORS_REQLKG (HMTX)
      ENDIF

C---     ASSEMBLE LE MEMBRE DE DROITE
      IF (ERR_GOOD() .AND. HSIM .NE. 0 .AND. HALG .NE. 0)
     &   IERR = MA_MORS_ASMRHS(HASM, HSIM, HALG, F_F, VSOL)

C---     RESOUS
      IF (ERR_GOOD()) THEN
         IERR = SP_LDUD_SOLV(NEQL,
     &                       NKGP,
     &                       KA(SO_ALLC_REQKIND(KA,LIASL)),
     &                       VA(SO_ALLC_REQVIND(VA,LKG)),
     &                       VSOL)
      ENDIF

      CALL TR_CHRN_STOP('h2d2.reso.mat.resolution')
      MR_LDUD_RESMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel.
C
C Description:
C     La fonction MR_LDUD_XEQ resoud complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du systeme matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUD_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUD_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrldud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldud.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ASSEMBLE LA MATRICE
      IF(ERR_GOOD())IERR=MR_LDUD_ASMMTX(HOBJ,HSIM,HALG,F_K,F_KU)

C---     FACTORISE LA MATRICE
      IF(ERR_GOOD())IERR=MR_LDUD_FCTMTX(HOBJ)

C---     RESOUS
      IF(ERR_GOOD())IERR=MR_LDUD_RESMTX(HOBJ,HSIM,HALG,F_F,VSOL)

      MR_LDUD_XEQ = ERR_TYP()
      RETURN
      END
