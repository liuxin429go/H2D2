C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Functions:
C   Public:
C     INTEGER MR_RESO_000
C     INTEGER MR_RESO_999
C     INTEGER MR_RESO_CTR
C     INTEGER MR_RESO_DTR
C     INTEGER MR_RESO_INI
C     INTEGER MR_RESO_RST
C     LOGICAL MR_RESO_HVALIDE
C     LOGICAL MR_RESO_ESTDIRECTE
C     INTEGER MR_RESO_ASMMTX
C     INTEGER MR_RESO_FCTMTX
C     INTEGER MR_RESO_RESMTX
C     INTEGER MR_RESO_XEQ
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à MR_RESO_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA MR_RESO_DATA

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'mrreso.fc'

      DATA MR_RESO_HBASE  / OB_OBJC_HBSP_TAG /

      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MR_RESO_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_RESO_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrreso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_RESO_NOBJMAX,
     &                   MR_RESO_HBASE,
     &                   'Head: Méthode de resolution')

      MR_RESO_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_RESO_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrreso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fc'

      INTEGER  IERR
      EXTERNAL MR_RESO_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_RESO_NOBJMAX,
     &                   MR_RESO_HBASE,
     &                   MR_RESO_DTR)

      MR_RESO_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_RESO_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrreso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_RESO_NOBJMAX,
     &                   MR_RESO_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_RESO_HVALIDESELF(HOBJ))
         IOB = HOBJ - MR_RESO_HBASE

         MR_RESO_HOMG(IOB) = 0
         MR_RESO_HMDL(IOB) = 0
      ENDIF

      MR_RESO_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_RESO_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrreso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_RESO_HVALIDESELF(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_RESO_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_RESO_NOBJMAX,
     &                   MR_RESO_HBASE)

      MR_RESO_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: MR_RESO_INI
C
C Description:
C     La fonction MR_RESO_INI initialise l'objet PROXY.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HOMG        Handle de l'Objet ManaGé
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_RESO_INI(HOBJ, HOMG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_INI
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HOMG

      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'mrreso.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HMDL, HFNC
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_RESO_HVALIDESELF(HOBJ))
D     CALL ERR_PRE(HOMG .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MR_RESO_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - MR_RESO_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HOMG, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HOMG)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         MR_RESO_HOMG(IOB) = HOMG
         MR_RESO_HMDL(IOB) = HMDL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HOMG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MR_RESO_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_RESO_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_RST
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFNC, HMDL, HOMG
      LOGICAL HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_RESO_HVALIDESELF(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - MR_RESO_HBASE
      HOMG = MR_RESO_HOMG(IOB)
      HMDL = MR_RESO_HMDL(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     FAIT L'APPEL POUR DÉTRUIRE L'OBJET ASSOCIÉ
      HFNC = 0
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      MR_RESO_HOMG(IOB) = 0
      MR_RESO_HMDL(IOB) = 0

      MR_RESO_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_RESO_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_RESO_HVALIDESELF(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrreso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrreso.fc'
C------------------------------------------------------------------------

      MR_RESO_HVALIDESELF = OB_OBJC_HVALIDE(HOBJ,
     &                                      MR_RESO_NOBJMAX,
     &                                      MR_RESO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction MR_RESO_HVALIDE permet de valider un objet de la
C     hiérarchie virtuelle. Elle retourne .TRUE. si le handle qui
C     lui est passé est valide comme handle de l'objet ou d'un
C     héritier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_RESO_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_HVALIDE
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG, HMDL, HFNC
      LOGICAL HVALIDE
C------------------------------------------------------------------------
      
      HVALIDE = MR_RESO_HVALIDESELF(HOBJ)
      IF (HVALIDE) GOTO 9999
         
C---     Cherche si le handle est connu
      HMDL = MR_RESO_REQHMDL(HOBJ)
      HVALIDE = .FALSE.
      IF (HMDL .NE. 0) HVALIDE = SO_MDUL_HVALIDE(HMDL)
      IF (.NOT. HVALIDE) GOTO 9999

C---     Contrôle le handle
      HVALIDE = .FALSE.
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOBJ))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      HVALIDE = (ERR_GOOD() .AND. HVALIDE)

9999  CONTINUE
      MR_RESO_HVALIDE = HVALIDE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode virtuelle MR_RESO_ESTDIRECTE
C
C Entrée:
C     HOBJ     Handle sur une méthode de résolution managée
C
C Sortie:
C
C Notes:
C     On accepte en entrée tout handle sur une méthode de résolution 
C     managée par MR_RESO.
C************************************************************************
      FUNCTION MR_RESO_ESTDIRECTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_ESTDIRECTE
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IERR, IRET
      INTEGER HFNC, HMDL
      LOGICAL ESTDIRECTE
      EQUIVALENCE (IRET, ESTDIRECTE)
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_RESO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Cherche le module
      HMDL = MR_RESO_REQHMDL(HOBJ)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ESTDIRECTE', HFNC)

C---     Fait l'appel: ESTDIRECTE est assigné via l'EQUIVALENCE
      IF (ERR_GOOD()) IRET = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOBJ))

      MR_RESO_ESTDIRECTE = ESTDIRECTE
      RETURN
      END

C************************************************************************
C Sommaire: Assemble le système matriciel.
C
C Description:
C     La méthode virtuelle MR_RESO_ASMMTX assemble le système matriciel.
C     La matrice est dimensionnée et assemblée.
C
C Entrée:
C     HOBJ     Handle sur une méthode de résolution managée
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C     On accepte en entrée tout handle sur une méthode de résolution 
C     managée par MR_RESO.
C************************************************************************
      FUNCTION MR_RESO_ASMMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_K,
     &                        F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_ASMMTX
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IERR
      INTEGER HFNC, HMDL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_RESO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.assemblage')

C---     Cherche le module
      HMDL = MR_RESO_REQHMDL(HOBJ)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ASMMTX', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL5(HFNC,
     &                        SO_ALLC_CST2B(HOBJ),
     &                        SO_ALLC_CST2B(HSIM),
     &                        SO_ALLC_CST2B(HALG),
     &                        SO_ALLC_CST2B(F_K, 0_1),
     &                        SO_ALLC_CST2B(F_KU,0_1))

      CALL TR_CHRN_STOP('h2d2.reso.mat.assemblage')
      MR_RESO_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise le système matriciel
C
C Description:
C     La méthode virtuelle MR_RESO_FCTMTX factorise le système matriciel. La matrice
C     doit être assemblée.
C
C Entrée:
C     HOBJ     Handle sur une méthode de résolution managée
C
C Sortie:
C
C Notes:
C     On accepte en entrée tout handle sur une méthode de résolution 
C     managée par MR_RESO.
C************************************************************************
      FUNCTION MR_RESO_FCTMTX(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_FCTMTX
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IERR
      INTEGER HFNC, HMDL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_RESO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.factorisation')

C---     Cherche le module
      HMDL = MR_RESO_REQHMDL(HOBJ)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'FCTMTX', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOBJ))

      CALL TR_CHRN_STOP('h2d2.reso.mat.factorisation')
      MR_RESO_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel
C
C Description:
C     La méthode virtuelle MR_RESO_RESMTX résout le système matriciel avec
C     le second membre qu'elle assemble. La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     Handle sur une méthode de résolution managée
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C     On accepte en entrée tout handle sur une méthode de résolution 
C     managée par MR_RESO.
C************************************************************************
      FUNCTION MR_RESO_RESMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_F,
     &                        VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_RESMTX
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_F

      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IERR
      INTEGER HFNC, HMDL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_RESO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.resolution')

C---     Cherche le module
      HMDL = MR_RESO_REQHMDL(HOBJ)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'RESMTX', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL5(HFNC,
     &                        SO_ALLC_CST2B(HOBJ),
     &                        SO_ALLC_CST2B(HSIM),
     &                        SO_ALLC_CST2B(HALG),
     &                        SO_ALLC_CST2B(F_F,0_1),
     &                        SO_ALLC_CST2B(VSOL(1)))

      CALL TR_CHRN_STOP('h2d2.reso.mat.resolution')
      MR_RESO_RESMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel.
C
C Description:
C     La méthode virtuelle MR_RESO_XEQ résout complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C
C Entrée:
C     HOBJ     Handle sur une méthode de résolution managée
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C     On accepte en entrée tout handle sur une méthode de résolution 
C     managée par MR_RESO.
C************************************************************************
      FUNCTION MR_RESO_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_RESO_XEQ
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IERR
      INTEGER HFNC, HMDL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_RESO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Cherche le module
      HMDL = MR_RESO_REQHMDL(HOBJ)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'XEQ', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL7(HFNC,
     &                        SO_ALLC_CST2B(HOBJ),
     &                        SO_ALLC_CST2B(HSIM),
     &                        SO_ALLC_CST2B(HALG),
     &                        SO_ALLC_CST2B(F_K, 0_1),
     &                        SO_ALLC_CST2B(F_KU,0_1),
     &                        SO_ALLC_CST2B(F_F, 0_1),
     &                        SO_ALLC_CST2B(VSOL(1)))

      MR_RESO_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée MR_RESO_REQHOMG retourne le handle du module
C     qui correspond à l'objet managé HOBJ.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On accepte en entrée tout handle sur une méthode de résolution 
C     managée par MR_RESO.
C************************************************************************
      FUNCTION MR_RESO_REQHMDL(HOBJ)

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fc'

      INTEGER IOB
      INTEGER HMDL
C------------------------------------------------------------------------

      HMDL = 0
      DO IOB=1,MR_RESO_NOBJMAX
         IF (MR_RESO_HOMG(IOB) .EQ. HOBJ) THEN
            HMDL = MR_RESO_HMDL(IOB)
            EXIT
         ENDIF
      ENDDO

      MR_RESO_REQHMDL = HMDL
      RETURN
      END
