//************************************************************************
// H2D2 - External declaration of public symbols
// Module: h2d2_algo
// Entry point: extern "C" void fake_dll_h2d2_algo()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-04-22 10:43:23.734429
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class AL_EULR
F_PROT(AL_EULR_000, al_eulr_000);
F_PROT(AL_EULR_999, al_eulr_999);
F_PROT(AL_EULR_CTR, al_eulr_ctr);
F_PROT(AL_EULR_DTR, al_eulr_dtr);
F_PROT(AL_EULR_INI, al_eulr_ini);
F_PROT(AL_EULR_RST, al_eulr_rst);
F_PROT(AL_EULR_REQHBASE, al_eulr_reqhbase);
F_PROT(AL_EULR_HVALIDE, al_eulr_hvalide);
F_PROT(AL_EULR_RESOUS, al_eulr_resous);
F_PROT(AL_EULR_RESOUS_E, al_eulr_resous_e);
F_PROT(AL_EULR_REQSTATUS, al_eulr_reqstatus);
F_PROT(AL_EULR_ASGALFA, al_eulr_asgalfa);
F_PROT(AL_EULR_REQALFA, al_eulr_reqalfa);
F_PROT(AL_EULR_ASGDELT, al_eulr_asgdelt);
F_PROT(AL_EULR_REQDELT, al_eulr_reqdelt);
 
// ---  class AL_GMRS
F_PROT(AL_GMRS_000, al_gmrs_000);
F_PROT(AL_GMRS_999, al_gmrs_999);
F_PROT(AL_GMRS_CTR, al_gmrs_ctr);
F_PROT(AL_GMRS_DTR, al_gmrs_dtr);
F_PROT(AL_GMRS_INI, al_gmrs_ini);
F_PROT(AL_GMRS_RST, al_gmrs_rst);
F_PROT(AL_GMRS_REQHBASE, al_gmrs_reqhbase);
F_PROT(AL_GMRS_HVALIDE, al_gmrs_hvalide);
F_PROT(AL_GMRS_RESOUS, al_gmrs_resous);
F_PROT(AL_GMRS_RESOUS_E, al_gmrs_resous_e);
F_PROT(AL_GMRS_REQSTATUS, al_gmrs_reqstatus);
 
// ---  class AL_HMTP
F_PROT(AL_HMTP_000, al_hmtp_000);
F_PROT(AL_HMTP_999, al_hmtp_999);
F_PROT(AL_HMTP_CTR, al_hmtp_ctr);
F_PROT(AL_HMTP_DTR, al_hmtp_dtr);
F_PROT(AL_HMTP_INI, al_hmtp_ini);
F_PROT(AL_HMTP_RST, al_hmtp_rst);
F_PROT(AL_HMTP_REQHBASE, al_hmtp_reqhbase);
F_PROT(AL_HMTP_HVALIDE, al_hmtp_hvalide);
F_PROT(AL_HMTP_PRN, al_hmtp_prn);
F_PROT(AL_HMTP_RESOUS, al_hmtp_resous);
F_PROT(AL_HMTP_RESOUS_E, al_hmtp_resous_e);
F_PROT(AL_HMTP_REQSTATUS, al_hmtp_reqstatus);
F_PROT(AL_HMTP_ASGDELT, al_hmtp_asgdelt);
F_PROT(AL_HMTP_REQDELT, al_hmtp_reqdelt);
 
// ---  class AL_IRK12
F_PROT(AL_IRK12_000, al_irk12_000);
F_PROT(AL_IRK12_999, al_irk12_999);
F_PROT(AL_IRK12_CTR, al_irk12_ctr);
F_PROT(AL_IRK12_DTR, al_irk12_dtr);
F_PROT(AL_IRK12_INI, al_irk12_ini);
F_PROT(AL_IRK12_RST, al_irk12_rst);
F_PROT(AL_IRK12_REQHBASE, al_irk12_reqhbase);
F_PROT(AL_IRK12_HVALIDE, al_irk12_hvalide);
F_PROT(AL_IRK12_RESOUS, al_irk12_resous);
F_PROT(AL_IRK12_RESOUS_E, al_irk12_resous_e);
F_PROT(AL_IRK12_REQSTATUS, al_irk12_reqstatus);
F_PROT(AL_IRK12_ASGDELT, al_irk12_asgdelt);
F_PROT(AL_IRK12_REQDELT, al_irk12_reqdelt);
 
// ---  class AL_IRK23
F_PROT(AL_IRK23_000, al_irk23_000);
F_PROT(AL_IRK23_999, al_irk23_999);
F_PROT(AL_IRK23_CTR, al_irk23_ctr);
F_PROT(AL_IRK23_DTR, al_irk23_dtr);
F_PROT(AL_IRK23_INI, al_irk23_ini);
F_PROT(AL_IRK23_RST, al_irk23_rst);
F_PROT(AL_IRK23_REQHBASE, al_irk23_reqhbase);
F_PROT(AL_IRK23_HVALIDE, al_irk23_hvalide);
F_PROT(AL_IRK23_RESOUS, al_irk23_resous);
F_PROT(AL_IRK23_RESOUS_E, al_irk23_resous_e);
F_PROT(AL_IRK23_REQSTATUS, al_irk23_reqstatus);
F_PROT(AL_IRK23_ASGDELT, al_irk23_asgdelt);
F_PROT(AL_IRK23_REQDELT, al_irk23_reqdelt);
 
// ---  class AL_IRK34
F_PROT(AL_IRK34_000, al_irk34_000);
F_PROT(AL_IRK34_999, al_irk34_999);
F_PROT(AL_IRK34_CTR, al_irk34_ctr);
F_PROT(AL_IRK34_DTR, al_irk34_dtr);
F_PROT(AL_IRK34_INI, al_irk34_ini);
F_PROT(AL_IRK34_RST, al_irk34_rst);
F_PROT(AL_IRK34_REQHBASE, al_irk34_reqhbase);
F_PROT(AL_IRK34_HVALIDE, al_irk34_hvalide);
F_PROT(AL_IRK34_RESOUS, al_irk34_resous);
F_PROT(AL_IRK34_RESOUS_E, al_irk34_resous_e);
F_PROT(AL_IRK34_REQSTATUS, al_irk34_reqstatus);
F_PROT(AL_IRK34_ASGDELT, al_irk34_asgdelt);
F_PROT(AL_IRK34_REQDELT, al_irk34_reqdelt);
 
// ---  class AL_IRKNN
F_PROT(AL_IRKNN_000, al_irknn_000);
F_PROT(AL_IRKNN_999, al_irknn_999);
F_PROT(AL_IRKNN_CTR, al_irknn_ctr);
F_PROT(AL_IRKNN_DTR, al_irknn_dtr);
F_PROT(AL_IRKNN_INI, al_irknn_ini);
F_PROT(AL_IRKNN_RST, al_irknn_rst);
F_PROT(AL_IRKNN_REQHBASE, al_irknn_reqhbase);
F_PROT(AL_IRKNN_HVALIDE, al_irknn_hvalide);
F_PROT(AL_IRKNN_RESOUS, al_irknn_resous);
F_PROT(AL_IRKNN_RESOUS_E, al_irknn_resous_e);
F_PROT(AL_IRKNN_REQSTATUS, al_irknn_reqstatus);
F_PROT(AL_IRKNN_ASGDELT, al_irknn_asgdelt);
F_PROT(AL_IRKNN_REQDELT, al_irknn_reqdelt);
 
// ---  class AL_MRQD
F_PROT(AL_MRQD_000, al_mrqd_000);
F_PROT(AL_MRQD_999, al_mrqd_999);
F_PROT(AL_MRQD_CTR, al_mrqd_ctr);
F_PROT(AL_MRQD_DTR, al_mrqd_dtr);
F_PROT(AL_MRQD_INI, al_mrqd_ini);
F_PROT(AL_MRQD_RST, al_mrqd_rst);
F_PROT(AL_MRQD_REQHBASE, al_mrqd_reqhbase);
F_PROT(AL_MRQD_HVALIDE, al_mrqd_hvalide);
F_PROT(AL_MRQD_ASGNITER, al_mrqd_asgniter);
F_PROT(AL_MRQD_REQNITER, al_mrqd_reqniter);
F_PROT(AL_MRQD_REQHRES, al_mrqd_reqhres);
F_PROT(AL_MRQD_RESOUS, al_mrqd_resous);
F_PROT(AL_MRQD_RESOUS_E, al_mrqd_resous_e);
F_PROT(AL_MRQD_REQSTATUS, al_mrqd_reqstatus);
 
// ---  class AL_NOBI
F_PROT(AL_NOBI_000, al_nobi_000);
F_PROT(AL_NOBI_999, al_nobi_999);
F_PROT(AL_NOBI_CTR, al_nobi_ctr);
F_PROT(AL_NOBI_DTR, al_nobi_dtr);
F_PROT(AL_NOBI_INI, al_nobi_ini);
F_PROT(AL_NOBI_RST, al_nobi_rst);
F_PROT(AL_NOBI_REQHBASE, al_nobi_reqhbase);
F_PROT(AL_NOBI_HVALIDE, al_nobi_hvalide);
F_PROT(AL_NOBI_RESOUS, al_nobi_resous);
F_PROT(AL_NOBI_RESOUS_E, al_nobi_resous_e);
F_PROT(AL_NOBI_REQSTATUS, al_nobi_reqstatus);
 
// ---  class AL_NODO
F_PROT(AL_NODO_000, al_nodo_000);
F_PROT(AL_NODO_999, al_nodo_999);
F_PROT(AL_NODO_CTR, al_nodo_ctr);
F_PROT(AL_NODO_DTR, al_nodo_dtr);
F_PROT(AL_NODO_INI, al_nodo_ini);
F_PROT(AL_NODO_RST, al_nodo_rst);
F_PROT(AL_NODO_REQHBASE, al_nodo_reqhbase);
F_PROT(AL_NODO_HVALIDE, al_nodo_hvalide);
F_PROT(AL_NODO_RESOUS, al_nodo_resous);
F_PROT(AL_NODO_RESOUS_E, al_nodo_resous_e);
F_PROT(AL_NODO_REQSTATUS, al_nodo_reqstatus);
 
// ---  class AL_NOLR
F_PROT(AL_NOLR_000, al_nolr_000);
F_PROT(AL_NOLR_999, al_nolr_999);
F_PROT(AL_NOLR_CTR, al_nolr_ctr);
F_PROT(AL_NOLR_DTR, al_nolr_dtr);
F_PROT(AL_NOLR_INI, al_nolr_ini);
F_PROT(AL_NOLR_RST, al_nolr_rst);
F_PROT(AL_NOLR_REQHBASE, al_nolr_reqhbase);
F_PROT(AL_NOLR_HVALIDE, al_nolr_hvalide);
F_PROT(AL_NOLR_RESOUS, al_nolr_resous);
F_PROT(AL_NOLR_RESOUS_E, al_nolr_resous_e);
F_PROT(AL_NOLR_REQSTATUS, al_nolr_reqstatus);
 
// ---  class AL_NOSP
F_PROT(AL_NOSP_000, al_nosp_000);
F_PROT(AL_NOSP_999, al_nosp_999);
F_PROT(AL_NOSP_CTR, al_nosp_ctr);
F_PROT(AL_NOSP_DTR, al_nosp_dtr);
F_PROT(AL_NOSP_INI, al_nosp_ini);
F_PROT(AL_NOSP_RST, al_nosp_rst);
F_PROT(AL_NOSP_REQHBASE, al_nosp_reqhbase);
F_PROT(AL_NOSP_HVALIDE, al_nosp_hvalide);
F_PROT(AL_NOSP_RESOUS, al_nosp_resous);
F_PROT(AL_NOSP_RESOUS_E, al_nosp_resous_e);
F_PROT(AL_NOSP_REQSTATUS, al_nosp_reqstatus);
 
// ---  class AL_NWTN
F_PROT(AL_NWTN_000, al_nwtn_000);
F_PROT(AL_NWTN_999, al_nwtn_999);
F_PROT(AL_NWTN_CTR, al_nwtn_ctr);
F_PROT(AL_NWTN_DTR, al_nwtn_dtr);
F_PROT(AL_NWTN_INI, al_nwtn_ini);
F_PROT(AL_NWTN_RST, al_nwtn_rst);
F_PROT(AL_NWTN_REQHBASE, al_nwtn_reqhbase);
F_PROT(AL_NWTN_HVALIDE, al_nwtn_hvalide);
F_PROT(AL_NWTN_ASGNITER, al_nwtn_asgniter);
F_PROT(AL_NWTN_REQNITER, al_nwtn_reqniter);
F_PROT(AL_NWTN_REQHRES, al_nwtn_reqhres);
F_PROT(AL_NWTN_RESOUS, al_nwtn_resous);
F_PROT(AL_NWTN_RESOUS_E, al_nwtn_resous_e);
F_PROT(AL_NWTN_REQSTATUS, al_nwtn_reqstatus);
 
// ---  class AL_PCRD
F_PROT(AL_PCRD_000, al_pcrd_000);
F_PROT(AL_PCRD_999, al_pcrd_999);
F_PROT(AL_PCRD_CTR, al_pcrd_ctr);
F_PROT(AL_PCRD_DTR, al_pcrd_dtr);
F_PROT(AL_PCRD_INI, al_pcrd_ini);
F_PROT(AL_PCRD_RST, al_pcrd_rst);
F_PROT(AL_PCRD_REQHBASE, al_pcrd_reqhbase);
F_PROT(AL_PCRD_HVALIDE, al_pcrd_hvalide);
F_PROT(AL_PCRD_ASGNITER, al_pcrd_asgniter);
F_PROT(AL_PCRD_REQNITER, al_pcrd_reqniter);
F_PROT(AL_PCRD_REQHRES, al_pcrd_reqhres);
F_PROT(AL_PCRD_RESOUS, al_pcrd_resous);
F_PROT(AL_PCRD_RESOUS_E, al_pcrd_resous_e);
F_PROT(AL_PCRD_REQSTATUS, al_pcrd_reqstatus);
 
// ---  class AL_READ
F_PROT(AL_READ_000, al_read_000);
F_PROT(AL_READ_999, al_read_999);
F_PROT(AL_READ_CTR, al_read_ctr);
F_PROT(AL_READ_DTR, al_read_dtr);
F_PROT(AL_READ_INI, al_read_ini);
F_PROT(AL_READ_RST, al_read_rst);
F_PROT(AL_READ_REQHBASE, al_read_reqhbase);
F_PROT(AL_READ_HVALIDE, al_read_hvalide);
F_PROT(AL_READ_RESOUS, al_read_resous);
F_PROT(AL_READ_RESOUS_E, al_read_resous_e);
F_PROT(AL_READ_REQSTATUS, al_read_reqstatus);
 
// ---  class AL_RESI
F_PROT(AL_RESI_CTR, al_resi_ctr);
F_PROT(AL_RESI_DTR, al_resi_dtr);
F_PROT(AL_RESI_INI, al_resi_ini);
F_PROT(AL_RESI_000, al_resi_000);
F_PROT(AL_RESI_999, al_resi_999);
F_PROT(AL_RESI_REQHBASE, al_resi_reqhbase);
F_PROT(AL_RESI_HVALIDE, al_resi_hvalide);
F_PROT(AL_RESI_TVALIDE, al_resi_tvalide);
 
// ---  class AL_RK4L
F_PROT(AL_RK4L_000, al_rk4l_000);
F_PROT(AL_RK4L_999, al_rk4l_999);
F_PROT(AL_RK4L_CTR, al_rk4l_ctr);
F_PROT(AL_RK4L_DTR, al_rk4l_dtr);
F_PROT(AL_RK4L_INI, al_rk4l_ini);
F_PROT(AL_RK4L_RST, al_rk4l_rst);
F_PROT(AL_RK4L_REQHBASE, al_rk4l_reqhbase);
F_PROT(AL_RK4L_HVALIDE, al_rk4l_hvalide);
F_PROT(AL_RK4L_RESOUS, al_rk4l_resous);
F_PROT(AL_RK4L_RESOUS_E, al_rk4l_resous_e);
F_PROT(AL_RK4L_REQSTATUS, al_rk4l_reqstatus);
 
// ---  class AL_SLVR
F_PROT(AL_SLVR_000, al_slvr_000);
F_PROT(AL_SLVR_999, al_slvr_999);
F_PROT(AL_SLVR_CTR, al_slvr_ctr);
F_PROT(AL_SLVR_DTR, al_slvr_dtr);
F_PROT(AL_SLVR_INI, al_slvr_ini);
F_PROT(AL_SLVR_RST, al_slvr_rst);
F_PROT(AL_SLVR_REQHBASE, al_slvr_reqhbase);
F_PROT(AL_SLVR_HVALIDE, al_slvr_hvalide);
F_PROT(AL_SLVR_RESOUS, al_slvr_resous);
F_PROT(AL_SLVR_RESOUS_E, al_slvr_resous_e);
F_PROT(AL_SLVR_REQSTATUS, al_slvr_reqstatus);
F_PROT(AL_SLVR_ASGDELT, al_slvr_asgdelt);
F_PROT(AL_SLVR_REQDELT, al_slvr_reqdelt);
 
// ---  class AL_SQIT
F_PROT(AL_SQIT_000, al_sqit_000);
F_PROT(AL_SQIT_999, al_sqit_999);
F_PROT(AL_SQIT_CTR, al_sqit_ctr);
F_PROT(AL_SQIT_DTR, al_sqit_dtr);
F_PROT(AL_SQIT_INIAL, al_sqit_inial);
F_PROT(AL_SQIT_INIPS, al_sqit_inips);
F_PROT(AL_SQIT_RST, al_sqit_rst);
F_PROT(AL_SQIT_REQHBASE, al_sqit_reqhbase);
F_PROT(AL_SQIT_HVALIDE, al_sqit_hvalide);
F_PROT(AL_SQIT_REQHALG, al_sqit_reqhalg);
F_PROT(AL_SQIT_REQHPST, al_sqit_reqhpst);
F_PROT(AL_SQIT_REQHSIM, al_sqit_reqhsim);
F_PROT(AL_SQIT_REQHTRG, al_sqit_reqhtrg);
F_PROT(AL_SQIT_XEQ, al_sqit_xeq);
 
// ---  class AL_SQNC
F_PROT(AL_SQNC_000, al_sqnc_000);
F_PROT(AL_SQNC_999, al_sqnc_999);
F_PROT(AL_SQNC_CTR, al_sqnc_ctr);
F_PROT(AL_SQNC_DTR, al_sqnc_dtr);
F_PROT(AL_SQNC_INI, al_sqnc_ini);
F_PROT(AL_SQNC_RST, al_sqnc_rst);
F_PROT(AL_SQNC_REQHBASE, al_sqnc_reqhbase);
F_PROT(AL_SQNC_HVALIDE, al_sqnc_hvalide);
F_PROT(AL_SQNC_ADDALGO, al_sqnc_addalgo);
F_PROT(AL_SQNC_ADDPOST, al_sqnc_addpost);
F_PROT(AL_SQNC_ADDITEM, al_sqnc_additem);
F_PROT(AL_SQNC_REQDIM, al_sqnc_reqdim);
F_PROT(AL_SQNC_REQHITM, al_sqnc_reqhitm);
F_PROT(AL_SQNC_XEQ, al_sqnc_xeq);
 
// ---  class AL_UTIL
F_PROT(AL_UTIL_LOGSOL, al_util_logsol);
 
// ---  class CA_COMP
F_PROT(CA_COMP_000, ca_comp_000);
F_PROT(CA_COMP_999, ca_comp_999);
F_PROT(CA_COMP_CTR, ca_comp_ctr);
F_PROT(CA_COMP_DTR, ca_comp_dtr);
F_PROT(CA_COMP_INI, ca_comp_ini);
F_PROT(CA_COMP_RST, ca_comp_rst);
F_PROT(CA_COMP_REQHBASE, ca_comp_reqhbase);
F_PROT(CA_COMP_HVALIDE, ca_comp_hvalide);
F_PROT(CA_COMP_CALCRI, ca_comp_calcri);
 
// ---  class CA_CRIA
F_PROT(CA_CRIA_000, ca_cria_000);
F_PROT(CA_CRIA_999, ca_cria_999);
F_PROT(CA_CRIA_CTR, ca_cria_ctr);
F_PROT(CA_CRIA_DTR, ca_cria_dtr);
F_PROT(CA_CRIA_INI, ca_cria_ini);
F_PROT(CA_CRIA_RST, ca_cria_rst);
F_PROT(CA_CRIA_REQHBASE, ca_cria_reqhbase);
F_PROT(CA_CRIA_HVALIDE, ca_cria_hvalide);
F_PROT(CA_CRIA_CALCRI, ca_cria_calcri);
F_PROT(CA_CRIA_ESTNIVEAU, ca_cria_estniveau);
F_PROT(CA_CRIA_ESTCONVERGE, ca_cria_estconverge);
F_PROT(CA_CRIA_REQVAL, ca_cria_reqval);
 
// ---  class CA_N2AR
F_PROT(CA_N2AR_000, ca_n2ar_000);
F_PROT(CA_N2AR_999, ca_n2ar_999);
F_PROT(CA_N2AR_CTR, ca_n2ar_ctr);
F_PROT(CA_N2AR_DTR, ca_n2ar_dtr);
F_PROT(CA_N2AR_INI, ca_n2ar_ini);
F_PROT(CA_N2AR_RST, ca_n2ar_rst);
F_PROT(CA_N2AR_REQHBASE, ca_n2ar_reqhbase);
F_PROT(CA_N2AR_HVALIDE, ca_n2ar_hvalide);
F_PROT(CA_N2AR_CALCRI, ca_n2ar_calcri);
 
// ---  class CA_N2GR
F_PROT(CA_N2GR_000, ca_n2gr_000);
F_PROT(CA_N2GR_999, ca_n2gr_999);
F_PROT(CA_N2GR_CTR, ca_n2gr_ctr);
F_PROT(CA_N2GR_DTR, ca_n2gr_dtr);
F_PROT(CA_N2GR_INI, ca_n2gr_ini);
F_PROT(CA_N2GR_RST, ca_n2gr_rst);
F_PROT(CA_N2GR_REQHBASE, ca_n2gr_reqhbase);
F_PROT(CA_N2GR_HVALIDE, ca_n2gr_hvalide);
F_PROT(CA_N2GR_CALCRI, ca_n2gr_calcri);
 
// ---  class CA_NIAR
F_PROT(CA_NIAR_000, ca_niar_000);
F_PROT(CA_NIAR_999, ca_niar_999);
F_PROT(CA_NIAR_CTR, ca_niar_ctr);
F_PROT(CA_NIAR_DTR, ca_niar_dtr);
F_PROT(CA_NIAR_INI, ca_niar_ini);
F_PROT(CA_NIAR_RST, ca_niar_rst);
F_PROT(CA_NIAR_REQHBASE, ca_niar_reqhbase);
F_PROT(CA_NIAR_HVALIDE, ca_niar_hvalide);
F_PROT(CA_NIAR_CALCRI, ca_niar_calcri);
 
// ---  class CA_NIGR
F_PROT(CA_NIGR_000, ca_nigr_000);
F_PROT(CA_NIGR_999, ca_nigr_999);
F_PROT(CA_NIGR_CTR, ca_nigr_ctr);
F_PROT(CA_NIGR_DTR, ca_nigr_dtr);
F_PROT(CA_NIGR_INI, ca_nigr_ini);
F_PROT(CA_NIGR_RST, ca_nigr_rst);
F_PROT(CA_NIGR_REQHBASE, ca_nigr_reqhbase);
F_PROT(CA_NIGR_HVALIDE, ca_nigr_hvalide);
F_PROT(CA_NIGR_CALCRI, ca_nigr_calcri);
 
// ---  class CA_NOOP
F_PROT(CA_NOOP_000, ca_noop_000);
F_PROT(CA_NOOP_999, ca_noop_999);
F_PROT(CA_NOOP_CTR, ca_noop_ctr);
F_PROT(CA_NOOP_DTR, ca_noop_dtr);
F_PROT(CA_NOOP_INI, ca_noop_ini);
F_PROT(CA_NOOP_RST, ca_noop_rst);
F_PROT(CA_NOOP_REQHBASE, ca_noop_reqhbase);
F_PROT(CA_NOOP_HVALIDE, ca_noop_hvalide);
F_PROT(CA_NOOP_CALCRI, ca_noop_calcri);
 
// ---  class CC_CRIC
F_PROT(CC_CRIC_000, cc_cric_000);
F_PROT(CC_CRIC_999, cc_cric_999);
F_PROT(CC_CRIC_CTR, cc_cric_ctr);
F_PROT(CC_CRIC_DTR, cc_cric_dtr);
F_PROT(CC_CRIC_INI, cc_cric_ini);
F_PROT(CC_CRIC_RST, cc_cric_rst);
F_PROT(CC_CRIC_REQHBASE, cc_cric_reqhbase);
F_PROT(CC_CRIC_HVALIDE, cc_cric_hvalide);
F_PROT(CC_CRIC_CALCRI, cc_cric_calcri);
F_PROT(CC_CRIC_ESTRAISON, cc_cric_estraison);
F_PROT(CC_CRIC_ESTSTATUS, cc_cric_eststatus);
F_PROT(CC_CRIC_REQSTATUS, cc_cric_reqstatus);
F_PROT(CC_CRIC_REQRAISON, cc_cric_reqraison);
 
// ---  class CI_CFL
F_PROT(CI_CFL_000, ci_cfl_000);
F_PROT(CI_CFL_999, ci_cfl_999);
F_PROT(CI_CFL_CTR, ci_cfl_ctr);
F_PROT(CI_CFL_DTR, ci_cfl_dtr);
F_PROT(CI_CFL_INI, ci_cfl_ini);
F_PROT(CI_CFL_RST, ci_cfl_rst);
F_PROT(CI_CFL_REQHBASE, ci_cfl_reqhbase);
F_PROT(CI_CFL_HVALIDE, ci_cfl_hvalide);
F_PROT(CI_CFL_ASGHCFL, ci_cfl_asghcfl);
F_PROT(CI_CFL_ASGDTEF, ci_cfl_asgdtef);
F_PROT(CI_CFL_CLCDEL, ci_cfl_clcdel);
F_PROT(CI_CFL_CLCERR, ci_cfl_clcerr);
F_PROT(CI_CFL_DEB, ci_cfl_deb);
F_PROT(CI_CFL_REQHCFL, ci_cfl_reqhcfl);
 
// ---  class CI_CINC
F_PROT(CI_CINC_000, ci_cinc_000);
F_PROT(CI_CINC_999, ci_cinc_999);
F_PROT(CI_CINC_CTR, ci_cinc_ctr);
F_PROT(CI_CINC_DTR, ci_cinc_dtr);
F_PROT(CI_CINC_INI, ci_cinc_ini);
F_PROT(CI_CINC_RST, ci_cinc_rst);
F_PROT(CI_CINC_REQHBASE, ci_cinc_reqhbase);
F_PROT(CI_CINC_HVALIDE, ci_cinc_hvalide);
F_PROT(CI_CINC_CLCDEL, ci_cinc_clcdel);
F_PROT(CI_CINC_CLCERR, ci_cinc_clcerr);
F_PROT(CI_CINC_DEB, ci_cinc_deb);
F_PROT(CI_CINC_ASGDTEF, ci_cinc_asgdtef);
F_PROT(CI_CINC_REQDELT, ci_cinc_reqdelt);
F_PROT(CI_CINC_ESTSTATUS, ci_cinc_eststatus);
F_PROT(CI_CINC_REQSTATUS, ci_cinc_reqstatus);
 
// ---  class CI_COMP
F_PROT(CI_COMP_000, ci_comp_000);
F_PROT(CI_COMP_999, ci_comp_999);
F_PROT(CI_COMP_CTR, ci_comp_ctr);
F_PROT(CI_COMP_DTR, ci_comp_dtr);
F_PROT(CI_COMP_INI, ci_comp_ini);
F_PROT(CI_COMP_RST, ci_comp_rst);
F_PROT(CI_COMP_REQHBASE, ci_comp_reqhbase);
F_PROT(CI_COMP_HVALIDE, ci_comp_hvalide);
F_PROT(CI_COMP_CLCDEL, ci_comp_clcdel);
F_PROT(CI_COMP_CLCERR, ci_comp_clcerr);
F_PROT(CI_COMP_ASGDTEF, ci_comp_asgdtef);
F_PROT(CI_COMP_DEB, ci_comp_deb);
 
// ---  class CI_LMTR
F_PROT(CI_LMTR_000, ci_lmtr_000);
F_PROT(CI_LMTR_999, ci_lmtr_999);
F_PROT(CI_LMTR_CTR, ci_lmtr_ctr);
F_PROT(CI_LMTR_DTR, ci_lmtr_dtr);
F_PROT(CI_LMTR_INI, ci_lmtr_ini);
F_PROT(CI_LMTR_RST, ci_lmtr_rst);
F_PROT(CI_LMTR_REQHBASE, ci_lmtr_reqhbase);
F_PROT(CI_LMTR_HVALIDE, ci_lmtr_hvalide);
F_PROT(CI_LMTR_ASGDTEF, ci_lmtr_asgdtef);
F_PROT(CI_LMTR_CLCDEL, ci_lmtr_clcdel);
F_PROT(CI_LMTR_CLCERR, ci_lmtr_clcerr);
F_PROT(CI_LMTR_DEB, ci_lmtr_deb);
 
// ---  class CI_NOOP
F_PROT(CI_NOOP_000, ci_noop_000);
F_PROT(CI_NOOP_999, ci_noop_999);
F_PROT(CI_NOOP_CTR, ci_noop_ctr);
F_PROT(CI_NOOP_DTR, ci_noop_dtr);
F_PROT(CI_NOOP_INI, ci_noop_ini);
F_PROT(CI_NOOP_RST, ci_noop_rst);
F_PROT(CI_NOOP_REQHBASE, ci_noop_reqhbase);
F_PROT(CI_NOOP_HVALIDE, ci_noop_hvalide);
F_PROT(CI_NOOP_ASGDTEF, ci_noop_asgdtef);
F_PROT(CI_NOOP_CLCDEL, ci_noop_clcdel);
F_PROT(CI_NOOP_CLCERR, ci_noop_clcerr);
F_PROT(CI_NOOP_DEB, ci_noop_deb);
 
// ---  class CI_PIDC
F_PROT(CI_PIDC_000, ci_pidc_000);
F_PROT(CI_PIDC_999, ci_pidc_999);
F_PROT(CI_PIDC_CTR, ci_pidc_ctr);
F_PROT(CI_PIDC_DTR, ci_pidc_dtr);
F_PROT(CI_PIDC_INI, ci_pidc_ini);
F_PROT(CI_PIDC_RST, ci_pidc_rst);
F_PROT(CI_PIDC_REQHBASE, ci_pidc_reqhbase);
F_PROT(CI_PIDC_HVALIDE, ci_pidc_hvalide);
F_PROT(CI_PIDC_ASGXPO, ci_pidc_asgxpo);
F_PROT(CI_PIDC_ASGDTEF, ci_pidc_asgdtef);
F_PROT(CI_PIDC_CLCDEL, ci_pidc_clcdel);
F_PROT(CI_PIDC_CLCERR, ci_pidc_clcerr);
F_PROT(CI_PIDC_DEB, ci_pidc_deb);
F_PROT(CI_PIDC_REQXPO, ci_pidc_reqxpo);
 
// ---  class CI_PRDC
F_PROT(CI_PRDC_000, ci_prdc_000);
F_PROT(CI_PRDC_999, ci_prdc_999);
F_PROT(CI_PRDC_CTR, ci_prdc_ctr);
F_PROT(CI_PRDC_DTR, ci_prdc_dtr);
F_PROT(CI_PRDC_INI, ci_prdc_ini);
F_PROT(CI_PRDC_RST, ci_prdc_rst);
F_PROT(CI_PRDC_REQHBASE, ci_prdc_reqhbase);
F_PROT(CI_PRDC_HVALIDE, ci_prdc_hvalide);
F_PROT(CI_PRDC_ASGXPO, ci_prdc_asgxpo);
F_PROT(CI_PRDC_ASGDTEF, ci_prdc_asgdtef);
F_PROT(CI_PRDC_CLCDEL, ci_prdc_clcdel);
F_PROT(CI_PRDC_CLCERR, ci_prdc_clcerr);
F_PROT(CI_PRDC_DEB, ci_prdc_deb);
F_PROT(CI_PRDC_REQXPO, ci_prdc_reqxpo);
 
// ---  class CI_SMPL
F_PROT(CI_SMPL_000, ci_smpl_000);
F_PROT(CI_SMPL_999, ci_smpl_999);
F_PROT(CI_SMPL_CTR, ci_smpl_ctr);
F_PROT(CI_SMPL_DTR, ci_smpl_dtr);
F_PROT(CI_SMPL_INI, ci_smpl_ini);
F_PROT(CI_SMPL_RST, ci_smpl_rst);
F_PROT(CI_SMPL_REQHBASE, ci_smpl_reqhbase);
F_PROT(CI_SMPL_HVALIDE, ci_smpl_hvalide);
F_PROT(CI_SMPL_ASGDTEF, ci_smpl_asgdtef);
F_PROT(CI_SMPL_CLCDEL, ci_smpl_clcdel);
F_PROT(CI_SMPL_CLCERR, ci_smpl_clcerr);
F_PROT(CI_SMPL_DEB, ci_smpl_deb);
 
// ---  class GL_ARLX
F_PROT(GL_ARLX_000, gl_arlx_000);
F_PROT(GL_ARLX_999, gl_arlx_999);
F_PROT(GL_ARLX_CTR, gl_arlx_ctr);
F_PROT(GL_ARLX_DTR, gl_arlx_dtr);
F_PROT(GL_ARLX_INI, gl_arlx_ini);
F_PROT(GL_ARLX_RST, gl_arlx_rst);
F_PROT(GL_ARLX_REQHBASE, gl_arlx_reqhbase);
F_PROT(GL_ARLX_HVALIDE, gl_arlx_hvalide);
F_PROT(GL_ARLX_DEB, gl_arlx_deb);
F_PROT(GL_ARLX_XEQ, gl_arlx_xeq);
F_PROT(GL_ARLX_REQTHETA, gl_arlx_reqtheta);
 
// ---  class GL_BKRS
F_PROT(GL_BKRS_000, gl_bkrs_000);
F_PROT(GL_BKRS_999, gl_bkrs_999);
F_PROT(GL_BKRS_CTR, gl_bkrs_ctr);
F_PROT(GL_BKRS_DTR, gl_bkrs_dtr);
F_PROT(GL_BKRS_INI, gl_bkrs_ini);
F_PROT(GL_BKRS_RST, gl_bkrs_rst);
F_PROT(GL_BKRS_REQHBASE, gl_bkrs_reqhbase);
F_PROT(GL_BKRS_HVALIDE, gl_bkrs_hvalide);
F_PROT(GL_BKRS_DEB, gl_bkrs_deb);
F_PROT(GL_BKRS_XEQ, gl_bkrs_xeq);
 
// ---  class GL_BTK2
F_PROT(GL_BTK2_000, gl_btk2_000);
F_PROT(GL_BTK2_999, gl_btk2_999);
F_PROT(GL_BTK2_CTR, gl_btk2_ctr);
F_PROT(GL_BTK2_DTR, gl_btk2_dtr);
F_PROT(GL_BTK2_INI, gl_btk2_ini);
F_PROT(GL_BTK2_RST, gl_btk2_rst);
F_PROT(GL_BTK2_REQHBASE, gl_btk2_reqhbase);
F_PROT(GL_BTK2_HVALIDE, gl_btk2_hvalide);
F_PROT(GL_BTK2_DEB, gl_btk2_deb);
F_PROT(GL_BTK2_XEQ, gl_btk2_xeq);
 
// ---  class GL_BTK3
F_PROT(GL_BTK3_000, gl_btk3_000);
F_PROT(GL_BTK3_999, gl_btk3_999);
F_PROT(GL_BTK3_CTR, gl_btk3_ctr);
F_PROT(GL_BTK3_DTR, gl_btk3_dtr);
F_PROT(GL_BTK3_INI, gl_btk3_ini);
F_PROT(GL_BTK3_RST, gl_btk3_rst);
F_PROT(GL_BTK3_REQHBASE, gl_btk3_reqhbase);
F_PROT(GL_BTK3_HVALIDE, gl_btk3_hvalide);
F_PROT(GL_BTK3_DEB, gl_btk3_deb);
F_PROT(GL_BTK3_XEQ, gl_btk3_xeq);
 
// ---  class GL_BTRK
F_PROT(GL_BTRK_000, gl_btrk_000);
F_PROT(GL_BTRK_999, gl_btrk_999);
F_PROT(GL_BTRK_CTR, gl_btrk_ctr);
F_PROT(GL_BTRK_DTR, gl_btrk_dtr);
F_PROT(GL_BTRK_INI, gl_btrk_ini);
F_PROT(GL_BTRK_RST, gl_btrk_rst);
F_PROT(GL_BTRK_REQHBASE, gl_btrk_reqhbase);
F_PROT(GL_BTRK_HVALIDE, gl_btrk_hvalide);
F_PROT(GL_BTRK_DEB, gl_btrk_deb);
F_PROT(GL_BTRK_XEQ, gl_btrk_xeq);
 
// ---  class GL_COMP
F_PROT(GL_COMP_000, gl_comp_000);
F_PROT(GL_COMP_999, gl_comp_999);
F_PROT(GL_COMP_CTR, gl_comp_ctr);
F_PROT(GL_COMP_DTR, gl_comp_dtr);
F_PROT(GL_COMP_INI, gl_comp_ini);
F_PROT(GL_COMP_RST, gl_comp_rst);
F_PROT(GL_COMP_REQHBASE, gl_comp_reqhbase);
F_PROT(GL_COMP_HVALIDE, gl_comp_hvalide);
F_PROT(GL_COMP_DEB, gl_comp_deb);
F_PROT(GL_COMP_XEQ, gl_comp_xeq);
 
// ---  class GL_CRLX
F_PROT(GL_CRLX_000, gl_crlx_000);
F_PROT(GL_CRLX_999, gl_crlx_999);
F_PROT(GL_CRLX_CTR, gl_crlx_ctr);
F_PROT(GL_CRLX_DTR, gl_crlx_dtr);
F_PROT(GL_CRLX_INI, gl_crlx_ini);
F_PROT(GL_CRLX_RST, gl_crlx_rst);
F_PROT(GL_CRLX_REQHBASE, gl_crlx_reqhbase);
F_PROT(GL_CRLX_HVALIDE, gl_crlx_hvalide);
F_PROT(GL_CRLX_DEB, gl_crlx_deb);
F_PROT(GL_CRLX_XEQ, gl_crlx_xeq);
F_PROT(GL_CRLX_REQTHETA, gl_crlx_reqtheta);
 
// ---  class GL_DMPR
F_PROT(GL_DMPR_000, gl_dmpr_000);
F_PROT(GL_DMPR_999, gl_dmpr_999);
F_PROT(GL_DMPR_CTR, gl_dmpr_ctr);
F_PROT(GL_DMPR_DTR, gl_dmpr_dtr);
F_PROT(GL_DMPR_INI, gl_dmpr_ini);
F_PROT(GL_DMPR_RST, gl_dmpr_rst);
F_PROT(GL_DMPR_REQHBASE, gl_dmpr_reqhbase);
F_PROT(GL_DMPR_HVALIDE, gl_dmpr_hvalide);
F_PROT(GL_DMPR_DEB, gl_dmpr_deb);
F_PROT(GL_DMPR_XEQ, gl_dmpr_xeq);
F_PROT(GL_DMPR_REQTHETA, gl_dmpr_reqtheta);
F_PROT(GL_DMPR_REQLTHT, gl_dmpr_reqltht);
 
// ---  class GL_GLBL
F_PROT(GL_GLBL_000, gl_glbl_000);
F_PROT(GL_GLBL_999, gl_glbl_999);
F_PROT(GL_GLBL_CTR, gl_glbl_ctr);
F_PROT(GL_GLBL_DTR, gl_glbl_dtr);
F_PROT(GL_GLBL_INI, gl_glbl_ini);
F_PROT(GL_GLBL_RST, gl_glbl_rst);
F_PROT(GL_GLBL_REQHBASE, gl_glbl_reqhbase);
 
// ---  class GL_LMTR
F_PROT(GL_LMTR_000, gl_lmtr_000);
F_PROT(GL_LMTR_999, gl_lmtr_999);
F_PROT(GL_LMTR_CTR, gl_lmtr_ctr);
F_PROT(GL_LMTR_DTR, gl_lmtr_dtr);
F_PROT(GL_LMTR_INI, gl_lmtr_ini);
F_PROT(GL_LMTR_RST, gl_lmtr_rst);
F_PROT(GL_LMTR_REQHBASE, gl_lmtr_reqhbase);
F_PROT(GL_LMTR_HVALIDE, gl_lmtr_hvalide);
F_PROT(GL_LMTR_DEB, gl_lmtr_deb);
F_PROT(GL_LMTR_XEQ, gl_lmtr_xeq);
 
// ---  class GL_SCLR
F_PROT(GL_SCLR_000, gl_sclr_000);
F_PROT(GL_SCLR_999, gl_sclr_999);
F_PROT(GL_SCLR_CTR, gl_sclr_ctr);
F_PROT(GL_SCLR_DTR, gl_sclr_dtr);
F_PROT(GL_SCLR_INI, gl_sclr_ini);
F_PROT(GL_SCLR_RST, gl_sclr_rst);
F_PROT(GL_SCLR_REQHBASE, gl_sclr_reqhbase);
F_PROT(GL_SCLR_HVALIDE, gl_sclr_hvalide);
F_PROT(GL_SCLR_DEB, gl_sclr_deb);
F_PROT(GL_SCLR_XEQ, gl_sclr_xeq);
 
// ---  class HM_COMP
F_PROT(HM_COMP_000, hm_comp_000);
F_PROT(HM_COMP_999, hm_comp_999);
F_PROT(HM_COMP_CTR, hm_comp_ctr);
F_PROT(HM_COMP_DTR, hm_comp_dtr);
F_PROT(HM_COMP_INI, hm_comp_ini);
F_PROT(HM_COMP_RST, hm_comp_rst);
F_PROT(HM_COMP_REQHBASE, hm_comp_reqhbase);
F_PROT(HM_COMP_HVALIDE, hm_comp_hvalide);
F_PROT(HM_COMP_XEQ, hm_comp_xeq);
F_PROT(HM_COMP_REQALFA, hm_comp_reqalfa);
 
// ---  class HM_HMTP
F_PROT(HM_HMTP_000, hm_hmtp_000);
F_PROT(HM_HMTP_999, hm_hmtp_999);
F_PROT(HM_HMTP_CTR, hm_hmtp_ctr);
F_PROT(HM_HMTP_DTR, hm_hmtp_dtr);
F_PROT(HM_HMTP_INI, hm_hmtp_ini);
F_PROT(HM_HMTP_RST, hm_hmtp_rst);
F_PROT(HM_HMTP_REQHBASE, hm_hmtp_reqhbase);
F_PROT(HM_HMTP_HVALIDE, hm_hmtp_hvalide);
F_PROT(HM_HMTP_ASGVAL, hm_hmtp_asgval);
F_PROT(HM_HMTP_CLCVAL, hm_hmtp_clcval);
F_PROT(HM_HMTP_REQVAL, hm_hmtp_reqval);
F_PROT(HM_HMTP_XEQ, hm_hmtp_xeq);
F_PROT(HM_HMTP_REQALFA, hm_hmtp_reqalfa);
 
// ---  class HM_PRGL
F_PROT(HM_PRGL_000, hm_prgl_000);
F_PROT(HM_PRGL_999, hm_prgl_999);
F_PROT(HM_PRGL_CTR, hm_prgl_ctr);
F_PROT(HM_PRGL_DTR, hm_prgl_dtr);
F_PROT(HM_PRGL_INI, hm_prgl_ini);
F_PROT(HM_PRGL_RST, hm_prgl_rst);
F_PROT(HM_PRGL_REQHBASE, hm_prgl_reqhbase);
F_PROT(HM_PRGL_HVALIDE, hm_prgl_hvalide);
F_PROT(HM_PRGL_PRN, hm_prgl_prn);
F_PROT(HM_PRGL_XEQ, hm_prgl_xeq);
F_PROT(HM_PRGL_REQALFA, hm_prgl_reqalfa);
 
// ---  class IC_ALALGO
F_PROT(IC_ALALGO_REQMDL, ic_alalgo_reqmdl);
F_PROT(IC_ALALGO_OPBDOT, ic_alalgo_opbdot);
F_PROT(IC_ALALGO_REQNOM, ic_alalgo_reqnom);
F_PROT(IC_ALALGO_REQHDL, ic_alalgo_reqhdl);
 
// ---  class IC_ALEULR
F_PROT(IC_ALEULR_XEQCTR, ic_aleulr_xeqctr);
F_PROT(IC_ALEULR_XEQMTH, ic_aleulr_xeqmth);
F_PROT(IC_ALEULR_REQCLS, ic_aleulr_reqcls);
F_PROT(IC_ALEULR_REQHDL, ic_aleulr_reqhdl);
 
// ---  class IC_ALGMRS
F_PROT(IC_ALGMRS_XEQCTR, ic_algmrs_xeqctr);
F_PROT(IC_ALGMRS_XEQMTH, ic_algmrs_xeqmth);
F_PROT(IC_ALGMRS_REQCLS, ic_algmrs_reqcls);
F_PROT(IC_ALGMRS_REQHDL, ic_algmrs_reqhdl);
 
// ---  class IC_ALHMTP
F_PROT(IC_ALHMTP_XEQCTR, ic_alhmtp_xeqctr);
F_PROT(IC_ALHMTP_XEQMTH, ic_alhmtp_xeqmth);
F_PROT(IC_ALHMTP_REQCLS, ic_alhmtp_reqcls);
F_PROT(IC_ALHMTP_REQHDL, ic_alhmtp_reqhdl);
 
// ---  class IC_ALMRQD
F_PROT(IC_ALMRQD_XEQCTR, ic_almrqd_xeqctr);
F_PROT(IC_ALMRQD_XEQMTH, ic_almrqd_xeqmth);
F_PROT(IC_ALMRQD_REQCLS, ic_almrqd_reqcls);
F_PROT(IC_ALMRQD_REQHDL, ic_almrqd_reqhdl);
 
// ---  class IC_ALNOBI
F_PROT(IC_ALNOBI_XEQCTR, ic_alnobi_xeqctr);
F_PROT(IC_ALNOBI_XEQMTH, ic_alnobi_xeqmth);
F_PROT(IC_ALNOBI_REQCLS, ic_alnobi_reqcls);
F_PROT(IC_ALNOBI_REQHDL, ic_alnobi_reqhdl);
 
// ---  class IC_ALNODO
F_PROT(IC_ALNODO_XEQCTR, ic_alnodo_xeqctr);
F_PROT(IC_ALNODO_XEQMTH, ic_alnodo_xeqmth);
F_PROT(IC_ALNODO_REQCLS, ic_alnodo_reqcls);
F_PROT(IC_ALNODO_REQHDL, ic_alnodo_reqhdl);
 
// ---  class IC_ALNOLR
F_PROT(IC_ALNOLR_XEQCTR, ic_alnolr_xeqctr);
F_PROT(IC_ALNOLR_XEQMTH, ic_alnolr_xeqmth);
F_PROT(IC_ALNOLR_REQCLS, ic_alnolr_reqcls);
F_PROT(IC_ALNOLR_REQHDL, ic_alnolr_reqhdl);
 
// ---  class IC_ALNOSP
F_PROT(IC_ALNOSP_XEQCTR, ic_alnosp_xeqctr);
F_PROT(IC_ALNOSP_XEQMTH, ic_alnosp_xeqmth);
F_PROT(IC_ALNOSP_REQCLS, ic_alnosp_reqcls);
F_PROT(IC_ALNOSP_REQHDL, ic_alnosp_reqhdl);
 
// ---  class IC_ALNWTN
F_PROT(IC_ALNWTN_XEQCTR, ic_alnwtn_xeqctr);
F_PROT(IC_ALNWTN_XEQMTH, ic_alnwtn_xeqmth);
F_PROT(IC_ALNWTN_REQCLS, ic_alnwtn_reqcls);
F_PROT(IC_ALNWTN_REQHDL, ic_alnwtn_reqhdl);
 
// ---  class IC_ALPCRD
F_PROT(IC_ALPCRD_XEQCTR, ic_alpcrd_xeqctr);
F_PROT(IC_ALPCRD_XEQMTH, ic_alpcrd_xeqmth);
F_PROT(IC_ALPCRD_REQCLS, ic_alpcrd_reqcls);
F_PROT(IC_ALPCRD_REQHDL, ic_alpcrd_reqhdl);
 
// ---  class IC_ALRK4L
F_PROT(IC_ALRK4L_XEQCTR, ic_alrk4l_xeqctr);
F_PROT(IC_ALRK4L_XEQMTH, ic_alrk4l_xeqmth);
F_PROT(IC_ALRK4L_REQCLS, ic_alrk4l_reqcls);
F_PROT(IC_ALRK4L_REQHDL, ic_alrk4l_reqhdl);
 
// ---  class IC_ALSLVR
F_PROT(IC_ALSLVR_XEQCTR, ic_alslvr_xeqctr);
F_PROT(IC_ALSLVR_XEQMTH, ic_alslvr_xeqmth);
F_PROT(IC_ALSLVR_REQCLS, ic_alslvr_reqcls);
F_PROT(IC_ALSLVR_REQHDL, ic_alslvr_reqhdl);
 
// ---  class IC_ALSQIP
F_PROT(IC_ALSQIP_CMD, ic_alsqip_cmd);
F_PROT(IC_ALSQIP_REQCMD, ic_alsqip_reqcmd);
 
// ---  class IC_ALSQIT
F_PROT(IC_ALSQIT_XEQCTR, ic_alsqit_xeqctr);
F_PROT(IC_ALSQIT_XEQMTH, ic_alsqit_xeqmth);
F_PROT(IC_ALSQIT_REQCLS, ic_alsqit_reqcls);
F_PROT(IC_ALSQIT_REQHDL, ic_alsqit_reqhdl);
 
// ---  class IC_ALSQNC
F_PROT(IC_ALSQNC_XEQCTR, ic_alsqnc_xeqctr);
F_PROT(IC_ALSQNC_XEQMTH, ic_alsqnc_xeqmth);
F_PROT(IC_ALSQNC_REQCLS, ic_alsqnc_reqcls);
F_PROT(IC_ALSQNC_REQHDL, ic_alsqnc_reqhdl);
 
// ---  class IC_CACOMP
F_PROT(IC_CACOMP_XEQCTR, ic_cacomp_xeqctr);
F_PROT(IC_CACOMP_XEQMTH, ic_cacomp_xeqmth);
F_PROT(IC_CACOMP_REQCLS, ic_cacomp_reqcls);
F_PROT(IC_CACOMP_REQHDL, ic_cacomp_reqhdl);
 
// ---  class IC_CACRIA
F_PROT(IC_CACRIA_XEQCTR, ic_cacria_xeqctr);
F_PROT(IC_CACRIA_XEQMTH, ic_cacria_xeqmth);
F_PROT(IC_CACRIA_REQCLS, ic_cacria_reqcls);
F_PROT(IC_CACRIA_REQHDL, ic_cacria_reqhdl);
 
// ---  class IC_CAN2AR
F_PROT(IC_CAN2AR_XEQCTR, ic_can2ar_xeqctr);
F_PROT(IC_CAN2AR_XEQMTH, ic_can2ar_xeqmth);
F_PROT(IC_CAN2AR_REQCLS, ic_can2ar_reqcls);
F_PROT(IC_CAN2AR_REQHDL, ic_can2ar_reqhdl);
 
// ---  class IC_CAN2GR
F_PROT(IC_CAN2GR_XEQCTR, ic_can2gr_xeqctr);
F_PROT(IC_CAN2GR_XEQMTH, ic_can2gr_xeqmth);
F_PROT(IC_CAN2GR_REQCLS, ic_can2gr_reqcls);
F_PROT(IC_CAN2GR_REQHDL, ic_can2gr_reqhdl);
 
// ---  class IC_CANIAR
F_PROT(IC_CANIAR_XEQCTR, ic_caniar_xeqctr);
F_PROT(IC_CANIAR_XEQMTH, ic_caniar_xeqmth);
F_PROT(IC_CANIAR_REQCLS, ic_caniar_reqcls);
F_PROT(IC_CANIAR_REQHDL, ic_caniar_reqhdl);
 
// ---  class IC_CANIGR
F_PROT(IC_CANIGR_XEQCTR, ic_canigr_xeqctr);
F_PROT(IC_CANIGR_XEQMTH, ic_canigr_xeqmth);
F_PROT(IC_CANIGR_REQCLS, ic_canigr_reqcls);
F_PROT(IC_CANIGR_REQHDL, ic_canigr_reqhdl);
 
// ---  class IC_GLARLX
F_PROT(IC_GLARLX_XEQCTR, ic_glarlx_xeqctr);
F_PROT(IC_GLARLX_XEQMTH, ic_glarlx_xeqmth);
F_PROT(IC_GLARLX_REQCLS, ic_glarlx_reqcls);
F_PROT(IC_GLARLX_REQHDL, ic_glarlx_reqhdl);
 
// ---  class IC_GLBKRS
F_PROT(IC_GLBKRS_XEQCTR, ic_glbkrs_xeqctr);
F_PROT(IC_GLBKRS_XEQMTH, ic_glbkrs_xeqmth);
F_PROT(IC_GLBKRS_REQCLS, ic_glbkrs_reqcls);
F_PROT(IC_GLBKRS_REQHDL, ic_glbkrs_reqhdl);
 
// ---  class IC_GLBTK2
F_PROT(IC_GLBTK2_XEQCTR, ic_glbtk2_xeqctr);
F_PROT(IC_GLBTK2_XEQMTH, ic_glbtk2_xeqmth);
F_PROT(IC_GLBTK2_REQCLS, ic_glbtk2_reqcls);
F_PROT(IC_GLBTK2_REQHDL, ic_glbtk2_reqhdl);
 
// ---  class IC_GLBTK3
F_PROT(IC_GLBTK3_XEQCTR, ic_glbtk3_xeqctr);
F_PROT(IC_GLBTK3_XEQMTH, ic_glbtk3_xeqmth);
F_PROT(IC_GLBTK3_REQCLS, ic_glbtk3_reqcls);
F_PROT(IC_GLBTK3_REQHDL, ic_glbtk3_reqhdl);
 
// ---  class IC_GLBTRK
F_PROT(IC_GLBTRK_XEQCTR, ic_glbtrk_xeqctr);
F_PROT(IC_GLBTRK_XEQMTH, ic_glbtrk_xeqmth);
F_PROT(IC_GLBTRK_REQCLS, ic_glbtrk_reqcls);
F_PROT(IC_GLBTRK_REQHDL, ic_glbtrk_reqhdl);
 
// ---  class IC_GLCOMP
F_PROT(IC_GLCOMP_XEQCTR, ic_glcomp_xeqctr);
F_PROT(IC_GLCOMP_XEQMTH, ic_glcomp_xeqmth);
F_PROT(IC_GLCOMP_REQCLS, ic_glcomp_reqcls);
F_PROT(IC_GLCOMP_REQHDL, ic_glcomp_reqhdl);
 
// ---  class IC_GLCRLX
F_PROT(IC_GLCRLX_XEQCTR, ic_glcrlx_xeqctr);
F_PROT(IC_GLCRLX_XEQMTH, ic_glcrlx_xeqmth);
F_PROT(IC_GLCRLX_REQCLS, ic_glcrlx_reqcls);
F_PROT(IC_GLCRLX_REQHDL, ic_glcrlx_reqhdl);
 
// ---  class IC_GLDMPR
F_PROT(IC_GLDMPR_XEQCTR, ic_gldmpr_xeqctr);
F_PROT(IC_GLDMPR_XEQMTH, ic_gldmpr_xeqmth);
F_PROT(IC_GLDMPR_REQCLS, ic_gldmpr_reqcls);
F_PROT(IC_GLDMPR_REQHDL, ic_gldmpr_reqhdl);
 
// ---  class IC_GLGLBL
F_PROT(IC_GLGLBL_REQMDL, ic_glglbl_reqmdl);
F_PROT(IC_GLGLBL_OPBDOT, ic_glglbl_opbdot);
F_PROT(IC_GLGLBL_REQNOM, ic_glglbl_reqnom);
F_PROT(IC_GLGLBL_REQHDL, ic_glglbl_reqhdl);
 
// ---  class IC_GLLMTR
F_PROT(IC_GLLMTR_XEQCTR, ic_gllmtr_xeqctr);
F_PROT(IC_GLLMTR_XEQMTH, ic_gllmtr_xeqmth);
F_PROT(IC_GLLMTR_REQCLS, ic_gllmtr_reqcls);
F_PROT(IC_GLLMTR_REQHDL, ic_gllmtr_reqhdl);
 
// ---  class IC_GLSCLR
F_PROT(IC_GLSCLR_XEQCTR, ic_glsclr_xeqctr);
F_PROT(IC_GLSCLR_XEQMTH, ic_glsclr_xeqmth);
F_PROT(IC_GLSCLR_REQCLS, ic_glsclr_reqcls);
F_PROT(IC_GLSCLR_REQHDL, ic_glsclr_reqhdl);
 
// ---  class IC_HMCOMP
F_PROT(IC_HMCOMP_XEQCTR, ic_hmcomp_xeqctr);
F_PROT(IC_HMCOMP_XEQMTH, ic_hmcomp_xeqmth);
F_PROT(IC_HMCOMP_REQCLS, ic_hmcomp_reqcls);
F_PROT(IC_HMCOMP_REQHDL, ic_hmcomp_reqhdl);
 
// ---  class IC_HMHMTP
F_PROT(IC_HMHMTP_XEQCTR, ic_hmhmtp_xeqctr);
F_PROT(IC_HMHMTP_XEQMTH, ic_hmhmtp_xeqmth);
F_PROT(IC_HMHMTP_REQCLS, ic_hmhmtp_reqcls);
F_PROT(IC_HMHMTP_REQHDL, ic_hmhmtp_reqhdl);
 
// ---  class IC_HMPRGL
F_PROT(IC_HMPRGL_XEQCTR, ic_hmprgl_xeqctr);
F_PROT(IC_HMPRGL_XEQMTH, ic_hmprgl_xeqmth);
F_PROT(IC_HMPRGL_REQCLS, ic_hmprgl_reqcls);
F_PROT(IC_HMPRGL_REQHDL, ic_hmprgl_reqhdl);
 
// ---  class IC_LDUD
F_PROT(IC_LDUD_XEQCTR, ic_ldud_xeqctr);
F_PROT(IC_LDUD_XEQMTH, ic_ldud_xeqmth);
F_PROT(IC_LDUD_REQCLS, ic_ldud_reqcls);
F_PROT(IC_LDUD_REQHDL, ic_ldud_reqhdl);
 
// ---  class IC_LDUM
F_PROT(IC_LDUM_XEQCTR, ic_ldum_xeqctr);
F_PROT(IC_LDUM_XEQMTH, ic_ldum_xeqmth);
F_PROT(IC_LDUM_REQCLS, ic_ldum_reqcls);
F_PROT(IC_LDUM_REQHDL, ic_ldum_reqhdl);
 
// ---  class IC_PRIDEN
F_PROT(IC_PRIDEN_XEQCTR, ic_priden_xeqctr);
F_PROT(IC_PRIDEN_XEQMTH, ic_priden_xeqmth);
F_PROT(IC_PRIDEN_REQCLS, ic_priden_reqcls);
F_PROT(IC_PRIDEN_REQHDL, ic_priden_reqhdl);
 
// ---  class IC_PRILUN
F_PROT(IC_PRILUN_XEQCTR, ic_prilun_xeqctr);
F_PROT(IC_PRILUN_XEQMTH, ic_prilun_xeqmth);
F_PROT(IC_PRILUN_REQCLS, ic_prilun_reqcls);
F_PROT(IC_PRILUN_REQHDL, ic_prilun_reqhdl);
 
// ---  class IC_PRKTD
F_PROT(IC_PRKTD_CMD, ic_prktd_cmd);
F_PROT(IC_PRKTD_REQCMD, ic_prktd_reqcmd);
 
// ---  class IC_PRMASD
F_PROT(IC_PRMASD_CMD, ic_prmasd_cmd);
F_PROT(IC_PRMASD_REQCMD, ic_prmasd_reqcmd);
 
// ---  class IC_PRPREC
F_PROT(IC_PRPREC_REQMDL, ic_prprec_reqmdl);
F_PROT(IC_PRPREC_OPBDOT, ic_prprec_opbdot);
F_PROT(IC_PRPREC_REQNOM, ic_prprec_reqnom);
F_PROT(IC_PRPREC_REQHDL, ic_prprec_reqhdl);
 
// ---  class IC_PRSLVR
F_PROT(IC_PRSLVR_XEQCTR, ic_prslvr_xeqctr);
F_PROT(IC_PRSLVR_XEQMTH, ic_prslvr_xeqmth);
F_PROT(IC_PRSLVR_REQCLS, ic_prslvr_reqcls);
F_PROT(IC_PRSLVR_REQHDL, ic_prslvr_reqhdl);
 
// ---  class IC_SOLV
F_PROT(IC_SOLV_XEQ, ic_solv_xeq);
 
// ---  class IC_AL_IRK12
F_PROT(IC_AL_IRK12_XEQCTR, ic_al_irk12_xeqctr);
F_PROT(IC_AL_IRK12_XEQMTH, ic_al_irk12_xeqmth);
F_PROT(IC_AL_IRK12_REQCLS, ic_al_irk12_reqcls);
F_PROT(IC_AL_IRK12_REQHDL, ic_al_irk12_reqhdl);
 
// ---  class IC_AL_IRK23
F_PROT(IC_AL_IRK23_XEQCTR, ic_al_irk23_xeqctr);
F_PROT(IC_AL_IRK23_XEQMTH, ic_al_irk23_xeqmth);
F_PROT(IC_AL_IRK23_REQCLS, ic_al_irk23_reqcls);
F_PROT(IC_AL_IRK23_REQHDL, ic_al_irk23_reqhdl);
 
// ---  class IC_AL_IRK34
F_PROT(IC_AL_IRK34_XEQCTR, ic_al_irk34_xeqctr);
F_PROT(IC_AL_IRK34_XEQMTH, ic_al_irk34_xeqmth);
F_PROT(IC_AL_IRK34_REQCLS, ic_al_irk34_reqcls);
F_PROT(IC_AL_IRK34_REQHDL, ic_al_irk34_reqhdl);
 
// ---  class IC_AL_READ
F_PROT(IC_AL_READ_XEQCTR, ic_al_read_xeqctr);
F_PROT(IC_AL_READ_XEQMTH, ic_al_read_xeqmth);
F_PROT(IC_AL_READ_REQCLS, ic_al_read_reqcls);
F_PROT(IC_AL_READ_REQHDL, ic_al_read_reqhdl);
 
// ---  class IC_CC_CRIC
F_PROT(IC_CC_CRIC_XEQCTR, ic_cc_cric_xeqctr);
F_PROT(IC_CC_CRIC_XEQMTH, ic_cc_cric_xeqmth);
F_PROT(IC_CC_CRIC_REQCLS, ic_cc_cric_reqcls);
F_PROT(IC_CC_CRIC_REQHDL, ic_cc_cric_reqhdl);
 
// ---  class IC_CI_CFL
F_PROT(IC_CI_CFL_XEQCTR, ic_ci_cfl_xeqctr);
F_PROT(IC_CI_CFL_XEQMTH, ic_ci_cfl_xeqmth);
F_PROT(IC_CI_CFL_REQCLS, ic_ci_cfl_reqcls);
F_PROT(IC_CI_CFL_REQHDL, ic_ci_cfl_reqhdl);
 
// ---  class IC_CI_CINC
F_PROT(IC_CI_CINC_XEQCTR, ic_ci_cinc_xeqctr);
F_PROT(IC_CI_CINC_XEQMTH, ic_ci_cinc_xeqmth);
F_PROT(IC_CI_CINC_REQCLS, ic_ci_cinc_reqcls);
F_PROT(IC_CI_CINC_REQHDL, ic_ci_cinc_reqhdl);
 
// ---  class IC_CI_COMP
F_PROT(IC_CI_COMP_XEQCTR, ic_ci_comp_xeqctr);
F_PROT(IC_CI_COMP_XEQMTH, ic_ci_comp_xeqmth);
F_PROT(IC_CI_COMP_REQCLS, ic_ci_comp_reqcls);
F_PROT(IC_CI_COMP_REQHDL, ic_ci_comp_reqhdl);
 
// ---  class IC_CI_LMTR
F_PROT(IC_CI_LMTR_XEQCTR, ic_ci_lmtr_xeqctr);
F_PROT(IC_CI_LMTR_XEQMTH, ic_ci_lmtr_xeqmth);
F_PROT(IC_CI_LMTR_REQCLS, ic_ci_lmtr_reqcls);
F_PROT(IC_CI_LMTR_REQHDL, ic_ci_lmtr_reqhdl);
 
// ---  class IC_CI_NOOP
F_PROT(IC_CI_NOOP_XEQCTR, ic_ci_noop_xeqctr);
F_PROT(IC_CI_NOOP_XEQMTH, ic_ci_noop_xeqmth);
F_PROT(IC_CI_NOOP_REQCLS, ic_ci_noop_reqcls);
F_PROT(IC_CI_NOOP_REQHDL, ic_ci_noop_reqhdl);
 
// ---  class IC_CI_PIDC
F_PROT(IC_CI_PIDC_XEQCTR, ic_ci_pidc_xeqctr);
F_PROT(IC_CI_PIDC_XEQMTH, ic_ci_pidc_xeqmth);
F_PROT(IC_CI_PIDC_REQCLS, ic_ci_pidc_reqcls);
F_PROT(IC_CI_PIDC_REQHDL, ic_ci_pidc_reqhdl);
 
// ---  class IC_CI_PRDC
F_PROT(IC_CI_PRDC_XEQCTR, ic_ci_prdc_xeqctr);
F_PROT(IC_CI_PRDC_XEQMTH, ic_ci_prdc_xeqmth);
F_PROT(IC_CI_PRDC_REQCLS, ic_ci_prdc_reqcls);
F_PROT(IC_CI_PRDC_REQHDL, ic_ci_prdc_reqhdl);
 
// ---  class IC_CI_SMPL
F_PROT(IC_CI_SMPL_XEQCTR, ic_ci_smpl_xeqctr);
F_PROT(IC_CI_SMPL_XEQMTH, ic_ci_smpl_xeqmth);
F_PROT(IC_CI_SMPL_REQCLS, ic_ci_smpl_reqcls);
F_PROT(IC_CI_SMPL_REQHDL, ic_ci_smpl_reqhdl);
 
// ---  class IC_PJ_POD
F_PROT(IC_PJ_POD_XEQCTR, ic_pj_pod_xeqctr);
F_PROT(IC_PJ_POD_XEQMTH, ic_pj_pod_xeqmth);
F_PROT(IC_PJ_POD_REQCLS, ic_pj_pod_reqcls);
F_PROT(IC_PJ_POD_REQHDL, ic_pj_pod_reqhdl);
 
// ---  class IC_PJ_POLY
F_PROT(IC_PJ_POLY_XEQCTR, ic_pj_poly_xeqctr);
F_PROT(IC_PJ_POLY_XEQMTH, ic_pj_poly_xeqmth);
F_PROT(IC_PJ_POLY_REQCLS, ic_pj_poly_reqcls);
F_PROT(IC_PJ_POLY_REQHDL, ic_pj_poly_reqhdl);
 
// ---  class IC_PJ_PRJC
F_PROT(IC_PJ_PRJC_XEQCTR, ic_pj_prjc_xeqctr);
F_PROT(IC_PJ_PRJC_XEQMTH, ic_pj_prjc_xeqmth);
F_PROT(IC_PJ_PRJC_REQCLS, ic_pj_prjc_reqcls);
F_PROT(IC_PJ_PRJC_REQHDL, ic_pj_prjc_reqhdl);
 
// ---  class MA_MORS
F_PROT(MA_MORS_000, ma_mors_000);
F_PROT(MA_MORS_999, ma_mors_999);
F_PROT(MA_MORS_CTR, ma_mors_ctr);
F_PROT(MA_MORS_DTR, ma_mors_dtr);
F_PROT(MA_MORS_INI, ma_mors_ini);
F_PROT(MA_MORS_RST, ma_mors_rst);
F_PROT(MA_MORS_REQHBASE, ma_mors_reqhbase);
F_PROT(MA_MORS_HVALIDE, ma_mors_hvalide);
F_PROT(MA_MORS_ASMMTX, ma_mors_asmmtx);
F_PROT(MA_MORS_ASMRHS, ma_mors_asmrhs);
 
// ---  class MR_GMRS
F_PROT(MR_GMRS_000, mr_gmrs_000);
F_PROT(MR_GMRS_999, mr_gmrs_999);
F_PROT(MR_GMRS_CTR, mr_gmrs_ctr);
F_PROT(MR_GMRS_DTR, mr_gmrs_dtr);
F_PROT(MR_GMRS_INI, mr_gmrs_ini);
F_PROT(MR_GMRS_RST, mr_gmrs_rst);
F_PROT(MR_GMRS_HVALIDE, mr_gmrs_hvalide);
F_PROT(MR_GMRS_ESTDIRECTE, mr_gmrs_estdirecte);
F_PROT(MR_GMRS_XEQ, mr_gmrs_xeq);
 
// ---  class MR_LDUD
F_PROT(MR_LDUD_000, mr_ldud_000);
F_PROT(MR_LDUD_999, mr_ldud_999);
F_PROT(MR_LDUD_CTR, mr_ldud_ctr);
F_PROT(MR_LDUD_DTR, mr_ldud_dtr);
F_PROT(MR_LDUD_INI, mr_ldud_ini);
F_PROT(MR_LDUD_RST, mr_ldud_rst);
F_PROT(MR_LDUD_REQHBASE, mr_ldud_reqhbase);
F_PROT(MR_LDUD_HVALIDE, mr_ldud_hvalide);
F_PROT(MR_LDUD_ESTDIRECTE, mr_ldud_estdirecte);
F_PROT(MR_LDUD_ASMMTX, mr_ldud_asmmtx);
F_PROT(MR_LDUD_FCTMTX, mr_ldud_fctmtx);
F_PROT(MR_LDUD_RESMTX, mr_ldud_resmtx);
F_PROT(MR_LDUD_XEQ, mr_ldud_xeq);
 
// ---  class MR_LDUM
F_PROT(MR_LDUM_000, mr_ldum_000);
F_PROT(MR_LDUM_999, mr_ldum_999);
F_PROT(MR_LDUM_CTR, mr_ldum_ctr);
F_PROT(MR_LDUM_DTR, mr_ldum_dtr);
F_PROT(MR_LDUM_INI, mr_ldum_ini);
F_PROT(MR_LDUM_RST, mr_ldum_rst);
F_PROT(MR_LDUM_REQHBASE, mr_ldum_reqhbase);
F_PROT(MR_LDUM_HVALIDE, mr_ldum_hvalide);
F_PROT(MR_LDUM_ESTDIRECTE, mr_ldum_estdirecte);
F_PROT(MR_LDUM_ASMMTX, mr_ldum_asmmtx);
F_PROT(MR_LDUM_FCTMTX, mr_ldum_fctmtx);
F_PROT(MR_LDUM_RESMTX, mr_ldum_resmtx);
F_PROT(MR_LDUM_XEQ, mr_ldum_xeq);
 
// ---  class MR_LMPD
F_PROT(MR_LMPD_000, mr_lmpd_000);
F_PROT(MR_LMPD_999, mr_lmpd_999);
F_PROT(MR_LMPD_CTR, mr_lmpd_ctr);
F_PROT(MR_LMPD_DTR, mr_lmpd_dtr);
F_PROT(MR_LMPD_INI, mr_lmpd_ini);
F_PROT(MR_LMPD_RST, mr_lmpd_rst);
F_PROT(MR_LMPD_REQHBASE, mr_lmpd_reqhbase);
F_PROT(MR_LMPD_HVALIDE, mr_lmpd_hvalide);
F_PROT(MR_LMPD_ESTDIRECTE, mr_lmpd_estdirecte);
F_PROT(MR_LMPD_ASMMTX, mr_lmpd_asmmtx);
F_PROT(MR_LMPD_FCTMTX, mr_lmpd_fctmtx);
F_PROT(MR_LMPD_RESMTX, mr_lmpd_resmtx);
F_PROT(MR_LMPD_XEQ, mr_lmpd_xeq);
 
// ---  class MR_RESO
F_PROT(MR_RESO_000, mr_reso_000);
F_PROT(MR_RESO_999, mr_reso_999);
F_PROT(MR_RESO_CTR, mr_reso_ctr);
F_PROT(MR_RESO_DTR, mr_reso_dtr);
F_PROT(MR_RESO_INI, mr_reso_ini);
F_PROT(MR_RESO_RST, mr_reso_rst);
F_PROT(MR_RESO_HVALIDE, mr_reso_hvalide);
F_PROT(MR_RESO_ESTDIRECTE, mr_reso_estdirecte);
F_PROT(MR_RESO_ASMMTX, mr_reso_asmmtx);
F_PROT(MR_RESO_FCTMTX, mr_reso_fctmtx);
F_PROT(MR_RESO_RESMTX, mr_reso_resmtx);
F_PROT(MR_RESO_XEQ, mr_reso_xeq);
 
// ---  class MX_CPNT
F_PROT(MX_CPNT_000, mx_cpnt_000);
F_PROT(MX_CPNT_999, mx_cpnt_999);
F_PROT(MX_CPNT_CTR, mx_cpnt_ctr);
F_PROT(MX_CPNT_DTR, mx_cpnt_dtr);
F_PROT(MX_CPNT_INI, mx_cpnt_ini);
F_PROT(MX_CPNT_RST, mx_cpnt_rst);
F_PROT(MX_CPNT_REQHBASE, mx_cpnt_reqhbase);
F_PROT(MX_CPNT_HVALIDE, mx_cpnt_hvalide);
F_PROT(MX_CPNT_ASMKE, mx_cpnt_asmke);
F_PROT(MX_CPNT_DIMMAT, mx_cpnt_dimmat);
F_PROT(MX_CPNT_LISMAT, mx_cpnt_lismat);
F_PROT(MX_CPNT_ADDMAT, mx_cpnt_addmat);
F_PROT(MX_CPNT_MULVAL, mx_cpnt_mulval);
F_PROT(MX_CPNT_MULVEC, mx_cpnt_mulvec);
F_PROT(MX_CPNT_REQILU, mx_cpnt_reqilu);
F_PROT(MX_CPNT_REQNEQL, mx_cpnt_reqneql);
F_PROT(MX_CPNT_REQNKGP, mx_cpnt_reqnkgp);
F_PROT(MX_CPNT_REQLIAP, mx_cpnt_reqliap);
F_PROT(MX_CPNT_REQLJAP, mx_cpnt_reqljap);
F_PROT(MX_CPNT_REQLJDP, mx_cpnt_reqljdp);
F_PROT(MX_CPNT_REQLKG, mx_cpnt_reqlkg);
F_PROT(MX_CPNT_DMPMAT, mx_cpnt_dmpmat);
 
// ---  class MX_DIAG
F_PROT(MX_DIAG_000, mx_diag_000);
F_PROT(MX_DIAG_999, mx_diag_999);
F_PROT(MX_DIAG_CTR, mx_diag_ctr);
F_PROT(MX_DIAG_DTR, mx_diag_dtr);
F_PROT(MX_DIAG_INI, mx_diag_ini);
F_PROT(MX_DIAG_RST, mx_diag_rst);
F_PROT(MX_DIAG_REQHBASE, mx_diag_reqhbase);
F_PROT(MX_DIAG_HVALIDE, mx_diag_hvalide);
F_PROT(MX_DIAG_ASMKE, mx_diag_asmke);
F_PROT(MX_DIAG_ASMFIN, mx_diag_asmfin);
F_PROT(MX_DIAG_ASGMTX, mx_diag_asgmtx);
F_PROT(MX_DIAG_DIMMAT, mx_diag_dimmat);
F_PROT(MX_DIAG_ADDMAT, mx_diag_addmat);
F_PROT(MX_DIAG_INIMAT, mx_diag_inimat);
F_PROT(MX_DIAG_MULVAL, mx_diag_mulval);
F_PROT(MX_DIAG_MULVEC, mx_diag_mulvec);
F_PROT(MX_DIAG_MULVECI, mx_diag_mulveci);
F_PROT(MX_DIAG_ASMDIM, mx_diag_asmdim);
F_PROT(MX_DIAG_REQHMORS, mx_diag_reqhmors);
F_PROT(MX_DIAG_REQNEQL, mx_diag_reqneql);
F_PROT(MX_DIAG_REQLKG, mx_diag_reqlkg);
F_PROT(MX_DIAG_DMPMAT, mx_diag_dmpmat);
 
// ---  class MX_DIST
F_PROT(MX_DIST_000, mx_dist_000);
F_PROT(MX_DIST_999, mx_dist_999);
F_PROT(MX_DIST_CTR, mx_dist_ctr);
F_PROT(MX_DIST_DTR, mx_dist_dtr);
F_PROT(MX_DIST_INI, mx_dist_ini);
F_PROT(MX_DIST_RST, mx_dist_rst);
F_PROT(MX_DIST_REQHBASE, mx_dist_reqhbase);
F_PROT(MX_DIST_HVALIDE, mx_dist_hvalide);
F_PROT(MX_DIST_ASMKE, mx_dist_asmke);
F_PROT(MX_DIST_DIMMAT, mx_dist_dimmat);
F_PROT(MX_DIST_ADDMAT, mx_dist_addmat);
F_PROT(MX_DIST_MULVAL, mx_dist_mulval);
F_PROT(MX_DIST_MULVEC, mx_dist_mulvec);
F_PROT(MX_DIST_REQNEQL, mx_dist_reqneql);
F_PROT(MX_DIST_REQNEQP, mx_dist_reqneqp);
F_PROT(MX_DIST_REQNEQT, mx_dist_reqneqt);
F_PROT(MX_DIST_REQHMORS, mx_dist_reqhmors);
F_PROT(MX_DIST_REQLLING, mx_dist_reqlling);
F_PROT(MX_DIST_REQLCOLG, mx_dist_reqlcolg);
 
// ---  class MX_DNSE
F_PROT(MX_DNSE_000, mx_dnse_000);
F_PROT(MX_DNSE_999, mx_dnse_999);
F_PROT(MX_DNSE_CTR, mx_dnse_ctr);
F_PROT(MX_DNSE_DTR, mx_dnse_dtr);
F_PROT(MX_DNSE_INI, mx_dnse_ini);
F_PROT(MX_DNSE_RST, mx_dnse_rst);
F_PROT(MX_DNSE_REQHBASE, mx_dnse_reqhbase);
F_PROT(MX_DNSE_HVALIDE, mx_dnse_hvalide);
F_PROT(MX_DNSE_ASMKE, mx_dnse_asmke);
F_PROT(MX_DNSE_DIMMAT, mx_dnse_dimmat);
F_PROT(MX_DNSE_ADDMAT, mx_dnse_addmat);
F_PROT(MX_DNSE_MULVAL, mx_dnse_mulval);
F_PROT(MX_DNSE_MULVEC, mx_dnse_mulvec);
F_PROT(MX_DNSE_REQNEQL, mx_dnse_reqneql);
F_PROT(MX_DNSE_REQLKG, mx_dnse_reqlkg);
 
// ---  class MX_MORS
F_PROT(MX_MORS_000, mx_mors_000);
F_PROT(MX_MORS_999, mx_mors_999);
F_PROT(MX_MORS_CTR, mx_mors_ctr);
F_PROT(MX_MORS_DTR, mx_mors_dtr);
F_PROT(MX_MORS_RAZ, mx_mors_raz);
F_PROT(MX_MORS_INI, mx_mors_ini);
F_PROT(MX_MORS_RST, mx_mors_rst);
F_PROT(MX_MORS_REQHBASE, mx_mors_reqhbase);
F_PROT(MX_MORS_HVALIDE, mx_mors_hvalide);
F_PROT(MX_MORS_ASMKE, mx_mors_asmke);
F_PROT(MX_MORS_DIMIND, mx_mors_dimind);
F_PROT(MX_MORS_DIMMAT, mx_mors_dimmat);
F_PROT(MX_MORS_LISMAT, mx_mors_lismat);
F_PROT(MX_MORS_DIAGMIN, mx_mors_diagmin);
F_PROT(MX_MORS_ADDMAT, mx_mors_addmat);
F_PROT(MX_MORS_MULVAL, mx_mors_mulval);
F_PROT(MX_MORS_MULVEC, mx_mors_mulvec);
F_PROT(MX_MORS_REQCRC32, mx_mors_reqcrc32);
F_PROT(MX_MORS_REQILU, mx_mors_reqilu);
F_PROT(MX_MORS_REQNEQL, mx_mors_reqneql);
F_PROT(MX_MORS_REQNKGP, mx_mors_reqnkgp);
F_PROT(MX_MORS_REQLIAP, mx_mors_reqliap);
F_PROT(MX_MORS_REQLJAP, mx_mors_reqljap);
F_PROT(MX_MORS_REQLJDP, mx_mors_reqljdp);
F_PROT(MX_MORS_REQLKG, mx_mors_reqlkg);
F_PROT(MX_MORS_DMPMAT, mx_mors_dmpmat);
 
// ---  class MX_MTRX
F_PROT(MX_MTRX_DTR, mx_mtrx_dtr);
F_PROT(MX_MTRX_CTRLH, mx_mtrx_ctrlh);
F_PROT(MX_MTRX_ASMKE, mx_mtrx_asmke);
F_PROT(MX_MTRX_DIMMAT, mx_mtrx_dimmat);
F_PROT(MX_MTRX_ADDMAT, mx_mtrx_addmat);
F_PROT(MX_MTRX_MULVAL, mx_mtrx_mulval);
F_PROT(MX_MTRX_MULVEC, mx_mtrx_mulvec);
F_PROT(MX_MTRX_REQNEQL, mx_mtrx_reqneql);
 
// ---  class MX_PRXY
F_PROT(MX_PRXY_000, mx_prxy_000);
F_PROT(MX_PRXY_999, mx_prxy_999);
F_PROT(MX_PRXY_CTR, mx_prxy_ctr);
F_PROT(MX_PRXY_DTR, mx_prxy_dtr);
F_PROT(MX_PRXY_INI, mx_prxy_ini);
F_PROT(MX_PRXY_RST, mx_prxy_rst);
F_PROT(MX_PRXY_REQHBASE, mx_prxy_reqhbase);
F_PROT(MX_PRXY_HVALIDE, mx_prxy_hvalide);
F_PROT(MX_PRXY_ASMKE, mx_prxy_asmke);
F_PROT(MX_PRXY_DIMMAT, mx_prxy_dimmat);
F_PROT(MX_PRXY_ADDMAT, mx_prxy_addmat);
F_PROT(MX_PRXY_MULVAL, mx_prxy_mulval);
F_PROT(MX_PRXY_MULVEC, mx_prxy_mulvec);
F_PROT(MX_PRXY_REQNEQL, mx_prxy_reqneql);
 
// ---  class MX_SKYL
F_PROT(MX_SKYL_000, mx_skyl_000);
F_PROT(MX_SKYL_999, mx_skyl_999);
F_PROT(MX_SKYL_CTR, mx_skyl_ctr);
F_PROT(MX_SKYL_DTR, mx_skyl_dtr);
F_PROT(MX_SKYL_INI, mx_skyl_ini);
F_PROT(MX_SKYL_RST, mx_skyl_rst);
F_PROT(MX_SKYL_REQHBASE, mx_skyl_reqhbase);
F_PROT(MX_SKYL_HVALIDE, mx_skyl_hvalide);
F_PROT(MX_SKYL_ASMKE, mx_skyl_asmke);
F_PROT(MX_SKYL_DIMMAT, mx_skyl_dimmat);
F_PROT(MX_SKYL_ADDMAT, mx_skyl_addmat);
F_PROT(MX_SKYL_MULVAL, mx_skyl_mulval);
F_PROT(MX_SKYL_MULVEC, mx_skyl_mulvec);
F_PROT(MX_SKYL_DMPMAT, mx_skyl_dmpmat);
F_PROT(MX_SKYL_REQCRC32, mx_skyl_reqcrc32);
F_PROT(MX_SKYL_REQNEQL, mx_skyl_reqneql);
F_PROT(MX_SKYL_REQNKGP, mx_skyl_reqnkgp);
F_PROT(MX_SKYL_REQLIAP, mx_skyl_reqliap);
F_PROT(MX_SKYL_REQLJAP, mx_skyl_reqljap);
F_PROT(MX_SKYL_REQLJDP, mx_skyl_reqljdp);
F_PROT(MX_SKYL_REQLKGS, mx_skyl_reqlkgs);
F_PROT(MX_SKYL_REQLKGD, mx_skyl_reqlkgd);
F_PROT(MX_SKYL_REQLKGI, mx_skyl_reqlkgi);
 
// ---  class PJ_POD
F_PROT(PJ_POD_000, pj_pod_000);
F_PROT(PJ_POD_999, pj_pod_999);
F_PROT(PJ_POD_CTR, pj_pod_ctr);
F_PROT(PJ_POD_DTR, pj_pod_dtr);
F_PROT(PJ_POD_INI, pj_pod_ini);
F_PROT(PJ_POD_RST, pj_pod_rst);
F_PROT(PJ_POD_REQHBASE, pj_pod_reqhbase);
F_PROT(PJ_POD_HVALIDE, pj_pod_hvalide);
F_PROT(PJ_POD_AJT, pj_pod_ajt);
F_PROT(PJ_POD_PRJ, pj_pod_prj);
F_PROT(PJ_POD_XEQ, pj_pod_xeq);
 
// ---  class PJ_POLY
F_PROT(PJ_POLY_000, pj_poly_000);
F_PROT(PJ_POLY_999, pj_poly_999);
F_PROT(PJ_POLY_CTR, pj_poly_ctr);
F_PROT(PJ_POLY_DTR, pj_poly_dtr);
F_PROT(PJ_POLY_INI, pj_poly_ini);
F_PROT(PJ_POLY_RST, pj_poly_rst);
F_PROT(PJ_POLY_REQHBASE, pj_poly_reqhbase);
F_PROT(PJ_POLY_HVALIDE, pj_poly_hvalide);
F_PROT(PJ_POLY_AJT, pj_poly_ajt);
F_PROT(PJ_POLY_PRJ, pj_poly_prj);
F_PROT(PJ_POLY_XEQ, pj_poly_xeq);
 
// ---  class PJ_PRJC
F_PROT(PJ_PRJC_000, pj_prjc_000);
F_PROT(PJ_PRJC_999, pj_prjc_999);
F_PROT(PJ_PRJC_CTR, pj_prjc_ctr);
F_PROT(PJ_PRJC_DTR, pj_prjc_dtr);
F_PROT(PJ_PRJC_INI, pj_prjc_ini);
F_PROT(PJ_PRJC_RST, pj_prjc_rst);
F_PROT(PJ_PRJC_REQHBASE, pj_prjc_reqhbase);
F_PROT(PJ_PRJC_HVALIDE, pj_prjc_hvalide);
F_PROT(PJ_PRJC_AJT, pj_prjc_ajt);
F_PROT(PJ_PRJC_PRJ, pj_prjc_prj);
F_PROT(PJ_PRJC_XEQ, pj_prjc_xeq);
F_PROT(PJ_PRJC_DEB, pj_prjc_deb);
 
// ---  class PR_DIAG
F_PROT(PR_DIAG_000, pr_diag_000);
F_PROT(PR_DIAG_999, pr_diag_999);
F_PROT(PR_DIAG_CTR, pr_diag_ctr);
F_PROT(PR_DIAG_DTR, pr_diag_dtr);
F_PROT(PR_DIAG_INI, pr_diag_ini);
F_PROT(PR_DIAG_RST, pr_diag_rst);
F_PROT(PR_DIAG_REQHBASE, pr_diag_reqhbase);
F_PROT(PR_DIAG_HVALIDE, pr_diag_hvalide);
F_PROT(PR_DIAG_ASM, pr_diag_asm);
F_PROT(PR_DIAG_PRC, pr_diag_prc);
 
// ---  class PR_IDEN
F_PROT(PR_IDEN_000, pr_iden_000);
F_PROT(PR_IDEN_999, pr_iden_999);
F_PROT(PR_IDEN_CTR, pr_iden_ctr);
F_PROT(PR_IDEN_DTR, pr_iden_dtr);
F_PROT(PR_IDEN_INI, pr_iden_ini);
F_PROT(PR_IDEN_RST, pr_iden_rst);
F_PROT(PR_IDEN_REQHBASE, pr_iden_reqhbase);
F_PROT(PR_IDEN_HVALIDE, pr_iden_hvalide);
F_PROT(PR_IDEN_ASM, pr_iden_asm);
F_PROT(PR_IDEN_PRC, pr_iden_prc);
 
// ---  class PR_ILUN
F_PROT(PR_ILUN_000, pr_ilun_000);
F_PROT(PR_ILUN_999, pr_ilun_999);
F_PROT(PR_ILUN_CTR, pr_ilun_ctr);
F_PROT(PR_ILUN_DTR, pr_ilun_dtr);
F_PROT(PR_ILUN_INI, pr_ilun_ini);
F_PROT(PR_ILUN_RST, pr_ilun_rst);
F_PROT(PR_ILUN_REQHBASE, pr_ilun_reqhbase);
F_PROT(PR_ILUN_HVALIDE, pr_ilun_hvalide);
F_PROT(PR_ILUN_ASM, pr_ilun_asm);
F_PROT(PR_ILUN_PRC, pr_ilun_prc);
F_PROT(PR_ILUN_REQILU, pr_ilun_reqilu);
 
// ---  class PR_MASD
F_PROT(PR_MASD_000, pr_masd_000);
F_PROT(PR_MASD_999, pr_masd_999);
F_PROT(PR_MASD_CTR, pr_masd_ctr);
F_PROT(PR_MASD_DTR, pr_masd_dtr);
F_PROT(PR_MASD_INI, pr_masd_ini);
F_PROT(PR_MASD_RST, pr_masd_rst);
F_PROT(PR_MASD_REQHBASE, pr_masd_reqhbase);
F_PROT(PR_MASD_HVALIDE, pr_masd_hvalide);
F_PROT(PR_MASD_ASM, pr_masd_asm);
F_PROT(PR_MASD_PRC, pr_masd_prc);
 
// ---  class PR_PREC
F_PROT(PR_PREC_DTR, pr_prec_dtr);
F_PROT(PR_PREC_CTRLH, pr_prec_ctrlh);
F_PROT(PR_PREC_ASM, pr_prec_asm);
F_PROT(PR_PREC_PRC, pr_prec_prc);
 
// ---  class PR_PRXY
F_PROT(PR_PRXY_000, pr_prxy_000);
F_PROT(PR_PRXY_999, pr_prxy_999);
F_PROT(PR_PRXY_CTR, pr_prxy_ctr);
F_PROT(PR_PRXY_DTR, pr_prxy_dtr);
F_PROT(PR_PRXY_INI, pr_prxy_ini);
F_PROT(PR_PRXY_RST, pr_prxy_rst);
F_PROT(PR_PRXY_HVALIDE, pr_prxy_hvalide);
F_PROT(PR_PRXY_ASM, pr_prxy_asm);
F_PROT(PR_PRXY_PRC, pr_prxy_prc);
 
// ---  class PR_SLVR
F_PROT(PR_SLVR_000, pr_slvr_000);
F_PROT(PR_SLVR_999, pr_slvr_999);
F_PROT(PR_SLVR_CTR, pr_slvr_ctr);
F_PROT(PR_SLVR_DTR, pr_slvr_dtr);
F_PROT(PR_SLVR_INI, pr_slvr_ini);
F_PROT(PR_SLVR_RST, pr_slvr_rst);
F_PROT(PR_SLVR_REQHBASE, pr_slvr_reqhbase);
F_PROT(PR_SLVR_HVALIDE, pr_slvr_hvalide);
F_PROT(PR_SLVR_ASM, pr_slvr_asm);
F_PROT(PR_SLVR_PRC, pr_slvr_prc);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_h2d2_algo()
{
   static char libname[] = "h2d2_algo";
 
   // ---  class AL_EULR
   F_RGST(AL_EULR_000, al_eulr_000, libname);
   F_RGST(AL_EULR_999, al_eulr_999, libname);
   F_RGST(AL_EULR_CTR, al_eulr_ctr, libname);
   F_RGST(AL_EULR_DTR, al_eulr_dtr, libname);
   F_RGST(AL_EULR_INI, al_eulr_ini, libname);
   F_RGST(AL_EULR_RST, al_eulr_rst, libname);
   F_RGST(AL_EULR_REQHBASE, al_eulr_reqhbase, libname);
   F_RGST(AL_EULR_HVALIDE, al_eulr_hvalide, libname);
   F_RGST(AL_EULR_RESOUS, al_eulr_resous, libname);
   F_RGST(AL_EULR_RESOUS_E, al_eulr_resous_e, libname);
   F_RGST(AL_EULR_REQSTATUS, al_eulr_reqstatus, libname);
   F_RGST(AL_EULR_ASGALFA, al_eulr_asgalfa, libname);
   F_RGST(AL_EULR_REQALFA, al_eulr_reqalfa, libname);
   F_RGST(AL_EULR_ASGDELT, al_eulr_asgdelt, libname);
   F_RGST(AL_EULR_REQDELT, al_eulr_reqdelt, libname);
 
   // ---  class AL_GMRS
   F_RGST(AL_GMRS_000, al_gmrs_000, libname);
   F_RGST(AL_GMRS_999, al_gmrs_999, libname);
   F_RGST(AL_GMRS_CTR, al_gmrs_ctr, libname);
   F_RGST(AL_GMRS_DTR, al_gmrs_dtr, libname);
   F_RGST(AL_GMRS_INI, al_gmrs_ini, libname);
   F_RGST(AL_GMRS_RST, al_gmrs_rst, libname);
   F_RGST(AL_GMRS_REQHBASE, al_gmrs_reqhbase, libname);
   F_RGST(AL_GMRS_HVALIDE, al_gmrs_hvalide, libname);
   F_RGST(AL_GMRS_RESOUS, al_gmrs_resous, libname);
   F_RGST(AL_GMRS_RESOUS_E, al_gmrs_resous_e, libname);
   F_RGST(AL_GMRS_REQSTATUS, al_gmrs_reqstatus, libname);
 
   // ---  class AL_HMTP
   F_RGST(AL_HMTP_000, al_hmtp_000, libname);
   F_RGST(AL_HMTP_999, al_hmtp_999, libname);
   F_RGST(AL_HMTP_CTR, al_hmtp_ctr, libname);
   F_RGST(AL_HMTP_DTR, al_hmtp_dtr, libname);
   F_RGST(AL_HMTP_INI, al_hmtp_ini, libname);
   F_RGST(AL_HMTP_RST, al_hmtp_rst, libname);
   F_RGST(AL_HMTP_REQHBASE, al_hmtp_reqhbase, libname);
   F_RGST(AL_HMTP_HVALIDE, al_hmtp_hvalide, libname);
   F_RGST(AL_HMTP_PRN, al_hmtp_prn, libname);
   F_RGST(AL_HMTP_RESOUS, al_hmtp_resous, libname);
   F_RGST(AL_HMTP_RESOUS_E, al_hmtp_resous_e, libname);
   F_RGST(AL_HMTP_REQSTATUS, al_hmtp_reqstatus, libname);
   F_RGST(AL_HMTP_ASGDELT, al_hmtp_asgdelt, libname);
   F_RGST(AL_HMTP_REQDELT, al_hmtp_reqdelt, libname);
 
   // ---  class AL_IRK12
   F_RGST(AL_IRK12_000, al_irk12_000, libname);
   F_RGST(AL_IRK12_999, al_irk12_999, libname);
   F_RGST(AL_IRK12_CTR, al_irk12_ctr, libname);
   F_RGST(AL_IRK12_DTR, al_irk12_dtr, libname);
   F_RGST(AL_IRK12_INI, al_irk12_ini, libname);
   F_RGST(AL_IRK12_RST, al_irk12_rst, libname);
   F_RGST(AL_IRK12_REQHBASE, al_irk12_reqhbase, libname);
   F_RGST(AL_IRK12_HVALIDE, al_irk12_hvalide, libname);
   F_RGST(AL_IRK12_RESOUS, al_irk12_resous, libname);
   F_RGST(AL_IRK12_RESOUS_E, al_irk12_resous_e, libname);
   F_RGST(AL_IRK12_REQSTATUS, al_irk12_reqstatus, libname);
   F_RGST(AL_IRK12_ASGDELT, al_irk12_asgdelt, libname);
   F_RGST(AL_IRK12_REQDELT, al_irk12_reqdelt, libname);
 
   // ---  class AL_IRK23
   F_RGST(AL_IRK23_000, al_irk23_000, libname);
   F_RGST(AL_IRK23_999, al_irk23_999, libname);
   F_RGST(AL_IRK23_CTR, al_irk23_ctr, libname);
   F_RGST(AL_IRK23_DTR, al_irk23_dtr, libname);
   F_RGST(AL_IRK23_INI, al_irk23_ini, libname);
   F_RGST(AL_IRK23_RST, al_irk23_rst, libname);
   F_RGST(AL_IRK23_REQHBASE, al_irk23_reqhbase, libname);
   F_RGST(AL_IRK23_HVALIDE, al_irk23_hvalide, libname);
   F_RGST(AL_IRK23_RESOUS, al_irk23_resous, libname);
   F_RGST(AL_IRK23_RESOUS_E, al_irk23_resous_e, libname);
   F_RGST(AL_IRK23_REQSTATUS, al_irk23_reqstatus, libname);
   F_RGST(AL_IRK23_ASGDELT, al_irk23_asgdelt, libname);
   F_RGST(AL_IRK23_REQDELT, al_irk23_reqdelt, libname);
 
   // ---  class AL_IRK34
   F_RGST(AL_IRK34_000, al_irk34_000, libname);
   F_RGST(AL_IRK34_999, al_irk34_999, libname);
   F_RGST(AL_IRK34_CTR, al_irk34_ctr, libname);
   F_RGST(AL_IRK34_DTR, al_irk34_dtr, libname);
   F_RGST(AL_IRK34_INI, al_irk34_ini, libname);
   F_RGST(AL_IRK34_RST, al_irk34_rst, libname);
   F_RGST(AL_IRK34_REQHBASE, al_irk34_reqhbase, libname);
   F_RGST(AL_IRK34_HVALIDE, al_irk34_hvalide, libname);
   F_RGST(AL_IRK34_RESOUS, al_irk34_resous, libname);
   F_RGST(AL_IRK34_RESOUS_E, al_irk34_resous_e, libname);
   F_RGST(AL_IRK34_REQSTATUS, al_irk34_reqstatus, libname);
   F_RGST(AL_IRK34_ASGDELT, al_irk34_asgdelt, libname);
   F_RGST(AL_IRK34_REQDELT, al_irk34_reqdelt, libname);
 
   // ---  class AL_IRKNN
   F_RGST(AL_IRKNN_000, al_irknn_000, libname);
   F_RGST(AL_IRKNN_999, al_irknn_999, libname);
   F_RGST(AL_IRKNN_CTR, al_irknn_ctr, libname);
   F_RGST(AL_IRKNN_DTR, al_irknn_dtr, libname);
   F_RGST(AL_IRKNN_INI, al_irknn_ini, libname);
   F_RGST(AL_IRKNN_RST, al_irknn_rst, libname);
   F_RGST(AL_IRKNN_REQHBASE, al_irknn_reqhbase, libname);
   F_RGST(AL_IRKNN_HVALIDE, al_irknn_hvalide, libname);
   F_RGST(AL_IRKNN_RESOUS, al_irknn_resous, libname);
   F_RGST(AL_IRKNN_RESOUS_E, al_irknn_resous_e, libname);
   F_RGST(AL_IRKNN_REQSTATUS, al_irknn_reqstatus, libname);
   F_RGST(AL_IRKNN_ASGDELT, al_irknn_asgdelt, libname);
   F_RGST(AL_IRKNN_REQDELT, al_irknn_reqdelt, libname);
 
   // ---  class AL_MRQD
   F_RGST(AL_MRQD_000, al_mrqd_000, libname);
   F_RGST(AL_MRQD_999, al_mrqd_999, libname);
   F_RGST(AL_MRQD_CTR, al_mrqd_ctr, libname);
   F_RGST(AL_MRQD_DTR, al_mrqd_dtr, libname);
   F_RGST(AL_MRQD_INI, al_mrqd_ini, libname);
   F_RGST(AL_MRQD_RST, al_mrqd_rst, libname);
   F_RGST(AL_MRQD_REQHBASE, al_mrqd_reqhbase, libname);
   F_RGST(AL_MRQD_HVALIDE, al_mrqd_hvalide, libname);
   F_RGST(AL_MRQD_ASGNITER, al_mrqd_asgniter, libname);
   F_RGST(AL_MRQD_REQNITER, al_mrqd_reqniter, libname);
   F_RGST(AL_MRQD_REQHRES, al_mrqd_reqhres, libname);
   F_RGST(AL_MRQD_RESOUS, al_mrqd_resous, libname);
   F_RGST(AL_MRQD_RESOUS_E, al_mrqd_resous_e, libname);
   F_RGST(AL_MRQD_REQSTATUS, al_mrqd_reqstatus, libname);
 
   // ---  class AL_NOBI
   F_RGST(AL_NOBI_000, al_nobi_000, libname);
   F_RGST(AL_NOBI_999, al_nobi_999, libname);
   F_RGST(AL_NOBI_CTR, al_nobi_ctr, libname);
   F_RGST(AL_NOBI_DTR, al_nobi_dtr, libname);
   F_RGST(AL_NOBI_INI, al_nobi_ini, libname);
   F_RGST(AL_NOBI_RST, al_nobi_rst, libname);
   F_RGST(AL_NOBI_REQHBASE, al_nobi_reqhbase, libname);
   F_RGST(AL_NOBI_HVALIDE, al_nobi_hvalide, libname);
   F_RGST(AL_NOBI_RESOUS, al_nobi_resous, libname);
   F_RGST(AL_NOBI_RESOUS_E, al_nobi_resous_e, libname);
   F_RGST(AL_NOBI_REQSTATUS, al_nobi_reqstatus, libname);
 
   // ---  class AL_NODO
   F_RGST(AL_NODO_000, al_nodo_000, libname);
   F_RGST(AL_NODO_999, al_nodo_999, libname);
   F_RGST(AL_NODO_CTR, al_nodo_ctr, libname);
   F_RGST(AL_NODO_DTR, al_nodo_dtr, libname);
   F_RGST(AL_NODO_INI, al_nodo_ini, libname);
   F_RGST(AL_NODO_RST, al_nodo_rst, libname);
   F_RGST(AL_NODO_REQHBASE, al_nodo_reqhbase, libname);
   F_RGST(AL_NODO_HVALIDE, al_nodo_hvalide, libname);
   F_RGST(AL_NODO_RESOUS, al_nodo_resous, libname);
   F_RGST(AL_NODO_RESOUS_E, al_nodo_resous_e, libname);
   F_RGST(AL_NODO_REQSTATUS, al_nodo_reqstatus, libname);
 
   // ---  class AL_NOLR
   F_RGST(AL_NOLR_000, al_nolr_000, libname);
   F_RGST(AL_NOLR_999, al_nolr_999, libname);
   F_RGST(AL_NOLR_CTR, al_nolr_ctr, libname);
   F_RGST(AL_NOLR_DTR, al_nolr_dtr, libname);
   F_RGST(AL_NOLR_INI, al_nolr_ini, libname);
   F_RGST(AL_NOLR_RST, al_nolr_rst, libname);
   F_RGST(AL_NOLR_REQHBASE, al_nolr_reqhbase, libname);
   F_RGST(AL_NOLR_HVALIDE, al_nolr_hvalide, libname);
   F_RGST(AL_NOLR_RESOUS, al_nolr_resous, libname);
   F_RGST(AL_NOLR_RESOUS_E, al_nolr_resous_e, libname);
   F_RGST(AL_NOLR_REQSTATUS, al_nolr_reqstatus, libname);
 
   // ---  class AL_NOSP
   F_RGST(AL_NOSP_000, al_nosp_000, libname);
   F_RGST(AL_NOSP_999, al_nosp_999, libname);
   F_RGST(AL_NOSP_CTR, al_nosp_ctr, libname);
   F_RGST(AL_NOSP_DTR, al_nosp_dtr, libname);
   F_RGST(AL_NOSP_INI, al_nosp_ini, libname);
   F_RGST(AL_NOSP_RST, al_nosp_rst, libname);
   F_RGST(AL_NOSP_REQHBASE, al_nosp_reqhbase, libname);
   F_RGST(AL_NOSP_HVALIDE, al_nosp_hvalide, libname);
   F_RGST(AL_NOSP_RESOUS, al_nosp_resous, libname);
   F_RGST(AL_NOSP_RESOUS_E, al_nosp_resous_e, libname);
   F_RGST(AL_NOSP_REQSTATUS, al_nosp_reqstatus, libname);
 
   // ---  class AL_NWTN
   F_RGST(AL_NWTN_000, al_nwtn_000, libname);
   F_RGST(AL_NWTN_999, al_nwtn_999, libname);
   F_RGST(AL_NWTN_CTR, al_nwtn_ctr, libname);
   F_RGST(AL_NWTN_DTR, al_nwtn_dtr, libname);
   F_RGST(AL_NWTN_INI, al_nwtn_ini, libname);
   F_RGST(AL_NWTN_RST, al_nwtn_rst, libname);
   F_RGST(AL_NWTN_REQHBASE, al_nwtn_reqhbase, libname);
   F_RGST(AL_NWTN_HVALIDE, al_nwtn_hvalide, libname);
   F_RGST(AL_NWTN_ASGNITER, al_nwtn_asgniter, libname);
   F_RGST(AL_NWTN_REQNITER, al_nwtn_reqniter, libname);
   F_RGST(AL_NWTN_REQHRES, al_nwtn_reqhres, libname);
   F_RGST(AL_NWTN_RESOUS, al_nwtn_resous, libname);
   F_RGST(AL_NWTN_RESOUS_E, al_nwtn_resous_e, libname);
   F_RGST(AL_NWTN_REQSTATUS, al_nwtn_reqstatus, libname);
 
   // ---  class AL_PCRD
   F_RGST(AL_PCRD_000, al_pcrd_000, libname);
   F_RGST(AL_PCRD_999, al_pcrd_999, libname);
   F_RGST(AL_PCRD_CTR, al_pcrd_ctr, libname);
   F_RGST(AL_PCRD_DTR, al_pcrd_dtr, libname);
   F_RGST(AL_PCRD_INI, al_pcrd_ini, libname);
   F_RGST(AL_PCRD_RST, al_pcrd_rst, libname);
   F_RGST(AL_PCRD_REQHBASE, al_pcrd_reqhbase, libname);
   F_RGST(AL_PCRD_HVALIDE, al_pcrd_hvalide, libname);
   F_RGST(AL_PCRD_ASGNITER, al_pcrd_asgniter, libname);
   F_RGST(AL_PCRD_REQNITER, al_pcrd_reqniter, libname);
   F_RGST(AL_PCRD_REQHRES, al_pcrd_reqhres, libname);
   F_RGST(AL_PCRD_RESOUS, al_pcrd_resous, libname);
   F_RGST(AL_PCRD_RESOUS_E, al_pcrd_resous_e, libname);
   F_RGST(AL_PCRD_REQSTATUS, al_pcrd_reqstatus, libname);
 
   // ---  class AL_READ
   F_RGST(AL_READ_000, al_read_000, libname);
   F_RGST(AL_READ_999, al_read_999, libname);
   F_RGST(AL_READ_CTR, al_read_ctr, libname);
   F_RGST(AL_READ_DTR, al_read_dtr, libname);
   F_RGST(AL_READ_INI, al_read_ini, libname);
   F_RGST(AL_READ_RST, al_read_rst, libname);
   F_RGST(AL_READ_REQHBASE, al_read_reqhbase, libname);
   F_RGST(AL_READ_HVALIDE, al_read_hvalide, libname);
   F_RGST(AL_READ_RESOUS, al_read_resous, libname);
   F_RGST(AL_READ_RESOUS_E, al_read_resous_e, libname);
   F_RGST(AL_READ_REQSTATUS, al_read_reqstatus, libname);
 
   // ---  class AL_RESI
   F_RGST(AL_RESI_CTR, al_resi_ctr, libname);
   F_RGST(AL_RESI_DTR, al_resi_dtr, libname);
   F_RGST(AL_RESI_INI, al_resi_ini, libname);
   F_RGST(AL_RESI_000, al_resi_000, libname);
   F_RGST(AL_RESI_999, al_resi_999, libname);
   F_RGST(AL_RESI_REQHBASE, al_resi_reqhbase, libname);
   F_RGST(AL_RESI_HVALIDE, al_resi_hvalide, libname);
   F_RGST(AL_RESI_TVALIDE, al_resi_tvalide, libname);
 
   // ---  class AL_RK4L
   F_RGST(AL_RK4L_000, al_rk4l_000, libname);
   F_RGST(AL_RK4L_999, al_rk4l_999, libname);
   F_RGST(AL_RK4L_CTR, al_rk4l_ctr, libname);
   F_RGST(AL_RK4L_DTR, al_rk4l_dtr, libname);
   F_RGST(AL_RK4L_INI, al_rk4l_ini, libname);
   F_RGST(AL_RK4L_RST, al_rk4l_rst, libname);
   F_RGST(AL_RK4L_REQHBASE, al_rk4l_reqhbase, libname);
   F_RGST(AL_RK4L_HVALIDE, al_rk4l_hvalide, libname);
   F_RGST(AL_RK4L_RESOUS, al_rk4l_resous, libname);
   F_RGST(AL_RK4L_RESOUS_E, al_rk4l_resous_e, libname);
   F_RGST(AL_RK4L_REQSTATUS, al_rk4l_reqstatus, libname);
 
   // ---  class AL_SLVR
   F_RGST(AL_SLVR_000, al_slvr_000, libname);
   F_RGST(AL_SLVR_999, al_slvr_999, libname);
   F_RGST(AL_SLVR_CTR, al_slvr_ctr, libname);
   F_RGST(AL_SLVR_DTR, al_slvr_dtr, libname);
   F_RGST(AL_SLVR_INI, al_slvr_ini, libname);
   F_RGST(AL_SLVR_RST, al_slvr_rst, libname);
   F_RGST(AL_SLVR_REQHBASE, al_slvr_reqhbase, libname);
   F_RGST(AL_SLVR_HVALIDE, al_slvr_hvalide, libname);
   F_RGST(AL_SLVR_RESOUS, al_slvr_resous, libname);
   F_RGST(AL_SLVR_RESOUS_E, al_slvr_resous_e, libname);
   F_RGST(AL_SLVR_REQSTATUS, al_slvr_reqstatus, libname);
   F_RGST(AL_SLVR_ASGDELT, al_slvr_asgdelt, libname);
   F_RGST(AL_SLVR_REQDELT, al_slvr_reqdelt, libname);
 
   // ---  class AL_SQIT
   F_RGST(AL_SQIT_000, al_sqit_000, libname);
   F_RGST(AL_SQIT_999, al_sqit_999, libname);
   F_RGST(AL_SQIT_CTR, al_sqit_ctr, libname);
   F_RGST(AL_SQIT_DTR, al_sqit_dtr, libname);
   F_RGST(AL_SQIT_INIAL, al_sqit_inial, libname);
   F_RGST(AL_SQIT_INIPS, al_sqit_inips, libname);
   F_RGST(AL_SQIT_RST, al_sqit_rst, libname);
   F_RGST(AL_SQIT_REQHBASE, al_sqit_reqhbase, libname);
   F_RGST(AL_SQIT_HVALIDE, al_sqit_hvalide, libname);
   F_RGST(AL_SQIT_REQHALG, al_sqit_reqhalg, libname);
   F_RGST(AL_SQIT_REQHPST, al_sqit_reqhpst, libname);
   F_RGST(AL_SQIT_REQHSIM, al_sqit_reqhsim, libname);
   F_RGST(AL_SQIT_REQHTRG, al_sqit_reqhtrg, libname);
   F_RGST(AL_SQIT_XEQ, al_sqit_xeq, libname);
 
   // ---  class AL_SQNC
   F_RGST(AL_SQNC_000, al_sqnc_000, libname);
   F_RGST(AL_SQNC_999, al_sqnc_999, libname);
   F_RGST(AL_SQNC_CTR, al_sqnc_ctr, libname);
   F_RGST(AL_SQNC_DTR, al_sqnc_dtr, libname);
   F_RGST(AL_SQNC_INI, al_sqnc_ini, libname);
   F_RGST(AL_SQNC_RST, al_sqnc_rst, libname);
   F_RGST(AL_SQNC_REQHBASE, al_sqnc_reqhbase, libname);
   F_RGST(AL_SQNC_HVALIDE, al_sqnc_hvalide, libname);
   F_RGST(AL_SQNC_ADDALGO, al_sqnc_addalgo, libname);
   F_RGST(AL_SQNC_ADDPOST, al_sqnc_addpost, libname);
   F_RGST(AL_SQNC_ADDITEM, al_sqnc_additem, libname);
   F_RGST(AL_SQNC_REQDIM, al_sqnc_reqdim, libname);
   F_RGST(AL_SQNC_REQHITM, al_sqnc_reqhitm, libname);
   F_RGST(AL_SQNC_XEQ, al_sqnc_xeq, libname);
 
   // ---  class AL_UTIL
   F_RGST(AL_UTIL_LOGSOL, al_util_logsol, libname);
 
   // ---  class CA_COMP
   F_RGST(CA_COMP_000, ca_comp_000, libname);
   F_RGST(CA_COMP_999, ca_comp_999, libname);
   F_RGST(CA_COMP_CTR, ca_comp_ctr, libname);
   F_RGST(CA_COMP_DTR, ca_comp_dtr, libname);
   F_RGST(CA_COMP_INI, ca_comp_ini, libname);
   F_RGST(CA_COMP_RST, ca_comp_rst, libname);
   F_RGST(CA_COMP_REQHBASE, ca_comp_reqhbase, libname);
   F_RGST(CA_COMP_HVALIDE, ca_comp_hvalide, libname);
   F_RGST(CA_COMP_CALCRI, ca_comp_calcri, libname);
 
   // ---  class CA_CRIA
   F_RGST(CA_CRIA_000, ca_cria_000, libname);
   F_RGST(CA_CRIA_999, ca_cria_999, libname);
   F_RGST(CA_CRIA_CTR, ca_cria_ctr, libname);
   F_RGST(CA_CRIA_DTR, ca_cria_dtr, libname);
   F_RGST(CA_CRIA_INI, ca_cria_ini, libname);
   F_RGST(CA_CRIA_RST, ca_cria_rst, libname);
   F_RGST(CA_CRIA_REQHBASE, ca_cria_reqhbase, libname);
   F_RGST(CA_CRIA_HVALIDE, ca_cria_hvalide, libname);
   F_RGST(CA_CRIA_CALCRI, ca_cria_calcri, libname);
   F_RGST(CA_CRIA_ESTNIVEAU, ca_cria_estniveau, libname);
   F_RGST(CA_CRIA_ESTCONVERGE, ca_cria_estconverge, libname);
   F_RGST(CA_CRIA_REQVAL, ca_cria_reqval, libname);
 
   // ---  class CA_N2AR
   F_RGST(CA_N2AR_000, ca_n2ar_000, libname);
   F_RGST(CA_N2AR_999, ca_n2ar_999, libname);
   F_RGST(CA_N2AR_CTR, ca_n2ar_ctr, libname);
   F_RGST(CA_N2AR_DTR, ca_n2ar_dtr, libname);
   F_RGST(CA_N2AR_INI, ca_n2ar_ini, libname);
   F_RGST(CA_N2AR_RST, ca_n2ar_rst, libname);
   F_RGST(CA_N2AR_REQHBASE, ca_n2ar_reqhbase, libname);
   F_RGST(CA_N2AR_HVALIDE, ca_n2ar_hvalide, libname);
   F_RGST(CA_N2AR_CALCRI, ca_n2ar_calcri, libname);
 
   // ---  class CA_N2GR
   F_RGST(CA_N2GR_000, ca_n2gr_000, libname);
   F_RGST(CA_N2GR_999, ca_n2gr_999, libname);
   F_RGST(CA_N2GR_CTR, ca_n2gr_ctr, libname);
   F_RGST(CA_N2GR_DTR, ca_n2gr_dtr, libname);
   F_RGST(CA_N2GR_INI, ca_n2gr_ini, libname);
   F_RGST(CA_N2GR_RST, ca_n2gr_rst, libname);
   F_RGST(CA_N2GR_REQHBASE, ca_n2gr_reqhbase, libname);
   F_RGST(CA_N2GR_HVALIDE, ca_n2gr_hvalide, libname);
   F_RGST(CA_N2GR_CALCRI, ca_n2gr_calcri, libname);
 
   // ---  class CA_NIAR
   F_RGST(CA_NIAR_000, ca_niar_000, libname);
   F_RGST(CA_NIAR_999, ca_niar_999, libname);
   F_RGST(CA_NIAR_CTR, ca_niar_ctr, libname);
   F_RGST(CA_NIAR_DTR, ca_niar_dtr, libname);
   F_RGST(CA_NIAR_INI, ca_niar_ini, libname);
   F_RGST(CA_NIAR_RST, ca_niar_rst, libname);
   F_RGST(CA_NIAR_REQHBASE, ca_niar_reqhbase, libname);
   F_RGST(CA_NIAR_HVALIDE, ca_niar_hvalide, libname);
   F_RGST(CA_NIAR_CALCRI, ca_niar_calcri, libname);
 
   // ---  class CA_NIGR
   F_RGST(CA_NIGR_000, ca_nigr_000, libname);
   F_RGST(CA_NIGR_999, ca_nigr_999, libname);
   F_RGST(CA_NIGR_CTR, ca_nigr_ctr, libname);
   F_RGST(CA_NIGR_DTR, ca_nigr_dtr, libname);
   F_RGST(CA_NIGR_INI, ca_nigr_ini, libname);
   F_RGST(CA_NIGR_RST, ca_nigr_rst, libname);
   F_RGST(CA_NIGR_REQHBASE, ca_nigr_reqhbase, libname);
   F_RGST(CA_NIGR_HVALIDE, ca_nigr_hvalide, libname);
   F_RGST(CA_NIGR_CALCRI, ca_nigr_calcri, libname);
 
   // ---  class CA_NOOP
   F_RGST(CA_NOOP_000, ca_noop_000, libname);
   F_RGST(CA_NOOP_999, ca_noop_999, libname);
   F_RGST(CA_NOOP_CTR, ca_noop_ctr, libname);
   F_RGST(CA_NOOP_DTR, ca_noop_dtr, libname);
   F_RGST(CA_NOOP_INI, ca_noop_ini, libname);
   F_RGST(CA_NOOP_RST, ca_noop_rst, libname);
   F_RGST(CA_NOOP_REQHBASE, ca_noop_reqhbase, libname);
   F_RGST(CA_NOOP_HVALIDE, ca_noop_hvalide, libname);
   F_RGST(CA_NOOP_CALCRI, ca_noop_calcri, libname);
 
   // ---  class CC_CRIC
   F_RGST(CC_CRIC_000, cc_cric_000, libname);
   F_RGST(CC_CRIC_999, cc_cric_999, libname);
   F_RGST(CC_CRIC_CTR, cc_cric_ctr, libname);
   F_RGST(CC_CRIC_DTR, cc_cric_dtr, libname);
   F_RGST(CC_CRIC_INI, cc_cric_ini, libname);
   F_RGST(CC_CRIC_RST, cc_cric_rst, libname);
   F_RGST(CC_CRIC_REQHBASE, cc_cric_reqhbase, libname);
   F_RGST(CC_CRIC_HVALIDE, cc_cric_hvalide, libname);
   F_RGST(CC_CRIC_CALCRI, cc_cric_calcri, libname);
   F_RGST(CC_CRIC_ESTRAISON, cc_cric_estraison, libname);
   F_RGST(CC_CRIC_ESTSTATUS, cc_cric_eststatus, libname);
   F_RGST(CC_CRIC_REQSTATUS, cc_cric_reqstatus, libname);
   F_RGST(CC_CRIC_REQRAISON, cc_cric_reqraison, libname);
 
   // ---  class CI_CFL
   F_RGST(CI_CFL_000, ci_cfl_000, libname);
   F_RGST(CI_CFL_999, ci_cfl_999, libname);
   F_RGST(CI_CFL_CTR, ci_cfl_ctr, libname);
   F_RGST(CI_CFL_DTR, ci_cfl_dtr, libname);
   F_RGST(CI_CFL_INI, ci_cfl_ini, libname);
   F_RGST(CI_CFL_RST, ci_cfl_rst, libname);
   F_RGST(CI_CFL_REQHBASE, ci_cfl_reqhbase, libname);
   F_RGST(CI_CFL_HVALIDE, ci_cfl_hvalide, libname);
   F_RGST(CI_CFL_ASGHCFL, ci_cfl_asghcfl, libname);
   F_RGST(CI_CFL_ASGDTEF, ci_cfl_asgdtef, libname);
   F_RGST(CI_CFL_CLCDEL, ci_cfl_clcdel, libname);
   F_RGST(CI_CFL_CLCERR, ci_cfl_clcerr, libname);
   F_RGST(CI_CFL_DEB, ci_cfl_deb, libname);
   F_RGST(CI_CFL_REQHCFL, ci_cfl_reqhcfl, libname);
 
   // ---  class CI_CINC
   F_RGST(CI_CINC_000, ci_cinc_000, libname);
   F_RGST(CI_CINC_999, ci_cinc_999, libname);
   F_RGST(CI_CINC_CTR, ci_cinc_ctr, libname);
   F_RGST(CI_CINC_DTR, ci_cinc_dtr, libname);
   F_RGST(CI_CINC_INI, ci_cinc_ini, libname);
   F_RGST(CI_CINC_RST, ci_cinc_rst, libname);
   F_RGST(CI_CINC_REQHBASE, ci_cinc_reqhbase, libname);
   F_RGST(CI_CINC_HVALIDE, ci_cinc_hvalide, libname);
   F_RGST(CI_CINC_CLCDEL, ci_cinc_clcdel, libname);
   F_RGST(CI_CINC_CLCERR, ci_cinc_clcerr, libname);
   F_RGST(CI_CINC_DEB, ci_cinc_deb, libname);
   F_RGST(CI_CINC_ASGDTEF, ci_cinc_asgdtef, libname);
   F_RGST(CI_CINC_REQDELT, ci_cinc_reqdelt, libname);
   F_RGST(CI_CINC_ESTSTATUS, ci_cinc_eststatus, libname);
   F_RGST(CI_CINC_REQSTATUS, ci_cinc_reqstatus, libname);
 
   // ---  class CI_COMP
   F_RGST(CI_COMP_000, ci_comp_000, libname);
   F_RGST(CI_COMP_999, ci_comp_999, libname);
   F_RGST(CI_COMP_CTR, ci_comp_ctr, libname);
   F_RGST(CI_COMP_DTR, ci_comp_dtr, libname);
   F_RGST(CI_COMP_INI, ci_comp_ini, libname);
   F_RGST(CI_COMP_RST, ci_comp_rst, libname);
   F_RGST(CI_COMP_REQHBASE, ci_comp_reqhbase, libname);
   F_RGST(CI_COMP_HVALIDE, ci_comp_hvalide, libname);
   F_RGST(CI_COMP_CLCDEL, ci_comp_clcdel, libname);
   F_RGST(CI_COMP_CLCERR, ci_comp_clcerr, libname);
   F_RGST(CI_COMP_ASGDTEF, ci_comp_asgdtef, libname);
   F_RGST(CI_COMP_DEB, ci_comp_deb, libname);
 
   // ---  class CI_LMTR
   F_RGST(CI_LMTR_000, ci_lmtr_000, libname);
   F_RGST(CI_LMTR_999, ci_lmtr_999, libname);
   F_RGST(CI_LMTR_CTR, ci_lmtr_ctr, libname);
   F_RGST(CI_LMTR_DTR, ci_lmtr_dtr, libname);
   F_RGST(CI_LMTR_INI, ci_lmtr_ini, libname);
   F_RGST(CI_LMTR_RST, ci_lmtr_rst, libname);
   F_RGST(CI_LMTR_REQHBASE, ci_lmtr_reqhbase, libname);
   F_RGST(CI_LMTR_HVALIDE, ci_lmtr_hvalide, libname);
   F_RGST(CI_LMTR_ASGDTEF, ci_lmtr_asgdtef, libname);
   F_RGST(CI_LMTR_CLCDEL, ci_lmtr_clcdel, libname);
   F_RGST(CI_LMTR_CLCERR, ci_lmtr_clcerr, libname);
   F_RGST(CI_LMTR_DEB, ci_lmtr_deb, libname);
 
   // ---  class CI_NOOP
   F_RGST(CI_NOOP_000, ci_noop_000, libname);
   F_RGST(CI_NOOP_999, ci_noop_999, libname);
   F_RGST(CI_NOOP_CTR, ci_noop_ctr, libname);
   F_RGST(CI_NOOP_DTR, ci_noop_dtr, libname);
   F_RGST(CI_NOOP_INI, ci_noop_ini, libname);
   F_RGST(CI_NOOP_RST, ci_noop_rst, libname);
   F_RGST(CI_NOOP_REQHBASE, ci_noop_reqhbase, libname);
   F_RGST(CI_NOOP_HVALIDE, ci_noop_hvalide, libname);
   F_RGST(CI_NOOP_ASGDTEF, ci_noop_asgdtef, libname);
   F_RGST(CI_NOOP_CLCDEL, ci_noop_clcdel, libname);
   F_RGST(CI_NOOP_CLCERR, ci_noop_clcerr, libname);
   F_RGST(CI_NOOP_DEB, ci_noop_deb, libname);
 
   // ---  class CI_PIDC
   F_RGST(CI_PIDC_000, ci_pidc_000, libname);
   F_RGST(CI_PIDC_999, ci_pidc_999, libname);
   F_RGST(CI_PIDC_CTR, ci_pidc_ctr, libname);
   F_RGST(CI_PIDC_DTR, ci_pidc_dtr, libname);
   F_RGST(CI_PIDC_INI, ci_pidc_ini, libname);
   F_RGST(CI_PIDC_RST, ci_pidc_rst, libname);
   F_RGST(CI_PIDC_REQHBASE, ci_pidc_reqhbase, libname);
   F_RGST(CI_PIDC_HVALIDE, ci_pidc_hvalide, libname);
   F_RGST(CI_PIDC_ASGXPO, ci_pidc_asgxpo, libname);
   F_RGST(CI_PIDC_ASGDTEF, ci_pidc_asgdtef, libname);
   F_RGST(CI_PIDC_CLCDEL, ci_pidc_clcdel, libname);
   F_RGST(CI_PIDC_CLCERR, ci_pidc_clcerr, libname);
   F_RGST(CI_PIDC_DEB, ci_pidc_deb, libname);
   F_RGST(CI_PIDC_REQXPO, ci_pidc_reqxpo, libname);
 
   // ---  class CI_PRDC
   F_RGST(CI_PRDC_000, ci_prdc_000, libname);
   F_RGST(CI_PRDC_999, ci_prdc_999, libname);
   F_RGST(CI_PRDC_CTR, ci_prdc_ctr, libname);
   F_RGST(CI_PRDC_DTR, ci_prdc_dtr, libname);
   F_RGST(CI_PRDC_INI, ci_prdc_ini, libname);
   F_RGST(CI_PRDC_RST, ci_prdc_rst, libname);
   F_RGST(CI_PRDC_REQHBASE, ci_prdc_reqhbase, libname);
   F_RGST(CI_PRDC_HVALIDE, ci_prdc_hvalide, libname);
   F_RGST(CI_PRDC_ASGXPO, ci_prdc_asgxpo, libname);
   F_RGST(CI_PRDC_ASGDTEF, ci_prdc_asgdtef, libname);
   F_RGST(CI_PRDC_CLCDEL, ci_prdc_clcdel, libname);
   F_RGST(CI_PRDC_CLCERR, ci_prdc_clcerr, libname);
   F_RGST(CI_PRDC_DEB, ci_prdc_deb, libname);
   F_RGST(CI_PRDC_REQXPO, ci_prdc_reqxpo, libname);
 
   // ---  class CI_SMPL
   F_RGST(CI_SMPL_000, ci_smpl_000, libname);
   F_RGST(CI_SMPL_999, ci_smpl_999, libname);
   F_RGST(CI_SMPL_CTR, ci_smpl_ctr, libname);
   F_RGST(CI_SMPL_DTR, ci_smpl_dtr, libname);
   F_RGST(CI_SMPL_INI, ci_smpl_ini, libname);
   F_RGST(CI_SMPL_RST, ci_smpl_rst, libname);
   F_RGST(CI_SMPL_REQHBASE, ci_smpl_reqhbase, libname);
   F_RGST(CI_SMPL_HVALIDE, ci_smpl_hvalide, libname);
   F_RGST(CI_SMPL_ASGDTEF, ci_smpl_asgdtef, libname);
   F_RGST(CI_SMPL_CLCDEL, ci_smpl_clcdel, libname);
   F_RGST(CI_SMPL_CLCERR, ci_smpl_clcerr, libname);
   F_RGST(CI_SMPL_DEB, ci_smpl_deb, libname);
 
   // ---  class GL_ARLX
   F_RGST(GL_ARLX_000, gl_arlx_000, libname);
   F_RGST(GL_ARLX_999, gl_arlx_999, libname);
   F_RGST(GL_ARLX_CTR, gl_arlx_ctr, libname);
   F_RGST(GL_ARLX_DTR, gl_arlx_dtr, libname);
   F_RGST(GL_ARLX_INI, gl_arlx_ini, libname);
   F_RGST(GL_ARLX_RST, gl_arlx_rst, libname);
   F_RGST(GL_ARLX_REQHBASE, gl_arlx_reqhbase, libname);
   F_RGST(GL_ARLX_HVALIDE, gl_arlx_hvalide, libname);
   F_RGST(GL_ARLX_DEB, gl_arlx_deb, libname);
   F_RGST(GL_ARLX_XEQ, gl_arlx_xeq, libname);
   F_RGST(GL_ARLX_REQTHETA, gl_arlx_reqtheta, libname);
 
   // ---  class GL_BKRS
   F_RGST(GL_BKRS_000, gl_bkrs_000, libname);
   F_RGST(GL_BKRS_999, gl_bkrs_999, libname);
   F_RGST(GL_BKRS_CTR, gl_bkrs_ctr, libname);
   F_RGST(GL_BKRS_DTR, gl_bkrs_dtr, libname);
   F_RGST(GL_BKRS_INI, gl_bkrs_ini, libname);
   F_RGST(GL_BKRS_RST, gl_bkrs_rst, libname);
   F_RGST(GL_BKRS_REQHBASE, gl_bkrs_reqhbase, libname);
   F_RGST(GL_BKRS_HVALIDE, gl_bkrs_hvalide, libname);
   F_RGST(GL_BKRS_DEB, gl_bkrs_deb, libname);
   F_RGST(GL_BKRS_XEQ, gl_bkrs_xeq, libname);
 
   // ---  class GL_BTK2
   F_RGST(GL_BTK2_000, gl_btk2_000, libname);
   F_RGST(GL_BTK2_999, gl_btk2_999, libname);
   F_RGST(GL_BTK2_CTR, gl_btk2_ctr, libname);
   F_RGST(GL_BTK2_DTR, gl_btk2_dtr, libname);
   F_RGST(GL_BTK2_INI, gl_btk2_ini, libname);
   F_RGST(GL_BTK2_RST, gl_btk2_rst, libname);
   F_RGST(GL_BTK2_REQHBASE, gl_btk2_reqhbase, libname);
   F_RGST(GL_BTK2_HVALIDE, gl_btk2_hvalide, libname);
   F_RGST(GL_BTK2_DEB, gl_btk2_deb, libname);
   F_RGST(GL_BTK2_XEQ, gl_btk2_xeq, libname);
 
   // ---  class GL_BTK3
   F_RGST(GL_BTK3_000, gl_btk3_000, libname);
   F_RGST(GL_BTK3_999, gl_btk3_999, libname);
   F_RGST(GL_BTK3_CTR, gl_btk3_ctr, libname);
   F_RGST(GL_BTK3_DTR, gl_btk3_dtr, libname);
   F_RGST(GL_BTK3_INI, gl_btk3_ini, libname);
   F_RGST(GL_BTK3_RST, gl_btk3_rst, libname);
   F_RGST(GL_BTK3_REQHBASE, gl_btk3_reqhbase, libname);
   F_RGST(GL_BTK3_HVALIDE, gl_btk3_hvalide, libname);
   F_RGST(GL_BTK3_DEB, gl_btk3_deb, libname);
   F_RGST(GL_BTK3_XEQ, gl_btk3_xeq, libname);
 
   // ---  class GL_BTRK
   F_RGST(GL_BTRK_000, gl_btrk_000, libname);
   F_RGST(GL_BTRK_999, gl_btrk_999, libname);
   F_RGST(GL_BTRK_CTR, gl_btrk_ctr, libname);
   F_RGST(GL_BTRK_DTR, gl_btrk_dtr, libname);
   F_RGST(GL_BTRK_INI, gl_btrk_ini, libname);
   F_RGST(GL_BTRK_RST, gl_btrk_rst, libname);
   F_RGST(GL_BTRK_REQHBASE, gl_btrk_reqhbase, libname);
   F_RGST(GL_BTRK_HVALIDE, gl_btrk_hvalide, libname);
   F_RGST(GL_BTRK_DEB, gl_btrk_deb, libname);
   F_RGST(GL_BTRK_XEQ, gl_btrk_xeq, libname);
 
   // ---  class GL_COMP
   F_RGST(GL_COMP_000, gl_comp_000, libname);
   F_RGST(GL_COMP_999, gl_comp_999, libname);
   F_RGST(GL_COMP_CTR, gl_comp_ctr, libname);
   F_RGST(GL_COMP_DTR, gl_comp_dtr, libname);
   F_RGST(GL_COMP_INI, gl_comp_ini, libname);
   F_RGST(GL_COMP_RST, gl_comp_rst, libname);
   F_RGST(GL_COMP_REQHBASE, gl_comp_reqhbase, libname);
   F_RGST(GL_COMP_HVALIDE, gl_comp_hvalide, libname);
   F_RGST(GL_COMP_DEB, gl_comp_deb, libname);
   F_RGST(GL_COMP_XEQ, gl_comp_xeq, libname);
 
   // ---  class GL_CRLX
   F_RGST(GL_CRLX_000, gl_crlx_000, libname);
   F_RGST(GL_CRLX_999, gl_crlx_999, libname);
   F_RGST(GL_CRLX_CTR, gl_crlx_ctr, libname);
   F_RGST(GL_CRLX_DTR, gl_crlx_dtr, libname);
   F_RGST(GL_CRLX_INI, gl_crlx_ini, libname);
   F_RGST(GL_CRLX_RST, gl_crlx_rst, libname);
   F_RGST(GL_CRLX_REQHBASE, gl_crlx_reqhbase, libname);
   F_RGST(GL_CRLX_HVALIDE, gl_crlx_hvalide, libname);
   F_RGST(GL_CRLX_DEB, gl_crlx_deb, libname);
   F_RGST(GL_CRLX_XEQ, gl_crlx_xeq, libname);
   F_RGST(GL_CRLX_REQTHETA, gl_crlx_reqtheta, libname);
 
   // ---  class GL_DMPR
   F_RGST(GL_DMPR_000, gl_dmpr_000, libname);
   F_RGST(GL_DMPR_999, gl_dmpr_999, libname);
   F_RGST(GL_DMPR_CTR, gl_dmpr_ctr, libname);
   F_RGST(GL_DMPR_DTR, gl_dmpr_dtr, libname);
   F_RGST(GL_DMPR_INI, gl_dmpr_ini, libname);
   F_RGST(GL_DMPR_RST, gl_dmpr_rst, libname);
   F_RGST(GL_DMPR_REQHBASE, gl_dmpr_reqhbase, libname);
   F_RGST(GL_DMPR_HVALIDE, gl_dmpr_hvalide, libname);
   F_RGST(GL_DMPR_DEB, gl_dmpr_deb, libname);
   F_RGST(GL_DMPR_XEQ, gl_dmpr_xeq, libname);
   F_RGST(GL_DMPR_REQTHETA, gl_dmpr_reqtheta, libname);
   F_RGST(GL_DMPR_REQLTHT, gl_dmpr_reqltht, libname);
 
   // ---  class GL_GLBL
   F_RGST(GL_GLBL_000, gl_glbl_000, libname);
   F_RGST(GL_GLBL_999, gl_glbl_999, libname);
   F_RGST(GL_GLBL_CTR, gl_glbl_ctr, libname);
   F_RGST(GL_GLBL_DTR, gl_glbl_dtr, libname);
   F_RGST(GL_GLBL_INI, gl_glbl_ini, libname);
   F_RGST(GL_GLBL_RST, gl_glbl_rst, libname);
   F_RGST(GL_GLBL_REQHBASE, gl_glbl_reqhbase, libname);
 
   // ---  class GL_LMTR
   F_RGST(GL_LMTR_000, gl_lmtr_000, libname);
   F_RGST(GL_LMTR_999, gl_lmtr_999, libname);
   F_RGST(GL_LMTR_CTR, gl_lmtr_ctr, libname);
   F_RGST(GL_LMTR_DTR, gl_lmtr_dtr, libname);
   F_RGST(GL_LMTR_INI, gl_lmtr_ini, libname);
   F_RGST(GL_LMTR_RST, gl_lmtr_rst, libname);
   F_RGST(GL_LMTR_REQHBASE, gl_lmtr_reqhbase, libname);
   F_RGST(GL_LMTR_HVALIDE, gl_lmtr_hvalide, libname);
   F_RGST(GL_LMTR_DEB, gl_lmtr_deb, libname);
   F_RGST(GL_LMTR_XEQ, gl_lmtr_xeq, libname);
 
   // ---  class GL_SCLR
   F_RGST(GL_SCLR_000, gl_sclr_000, libname);
   F_RGST(GL_SCLR_999, gl_sclr_999, libname);
   F_RGST(GL_SCLR_CTR, gl_sclr_ctr, libname);
   F_RGST(GL_SCLR_DTR, gl_sclr_dtr, libname);
   F_RGST(GL_SCLR_INI, gl_sclr_ini, libname);
   F_RGST(GL_SCLR_RST, gl_sclr_rst, libname);
   F_RGST(GL_SCLR_REQHBASE, gl_sclr_reqhbase, libname);
   F_RGST(GL_SCLR_HVALIDE, gl_sclr_hvalide, libname);
   F_RGST(GL_SCLR_DEB, gl_sclr_deb, libname);
   F_RGST(GL_SCLR_XEQ, gl_sclr_xeq, libname);
 
   // ---  class HM_COMP
   F_RGST(HM_COMP_000, hm_comp_000, libname);
   F_RGST(HM_COMP_999, hm_comp_999, libname);
   F_RGST(HM_COMP_CTR, hm_comp_ctr, libname);
   F_RGST(HM_COMP_DTR, hm_comp_dtr, libname);
   F_RGST(HM_COMP_INI, hm_comp_ini, libname);
   F_RGST(HM_COMP_RST, hm_comp_rst, libname);
   F_RGST(HM_COMP_REQHBASE, hm_comp_reqhbase, libname);
   F_RGST(HM_COMP_HVALIDE, hm_comp_hvalide, libname);
   F_RGST(HM_COMP_XEQ, hm_comp_xeq, libname);
   F_RGST(HM_COMP_REQALFA, hm_comp_reqalfa, libname);
 
   // ---  class HM_HMTP
   F_RGST(HM_HMTP_000, hm_hmtp_000, libname);
   F_RGST(HM_HMTP_999, hm_hmtp_999, libname);
   F_RGST(HM_HMTP_CTR, hm_hmtp_ctr, libname);
   F_RGST(HM_HMTP_DTR, hm_hmtp_dtr, libname);
   F_RGST(HM_HMTP_INI, hm_hmtp_ini, libname);
   F_RGST(HM_HMTP_RST, hm_hmtp_rst, libname);
   F_RGST(HM_HMTP_REQHBASE, hm_hmtp_reqhbase, libname);
   F_RGST(HM_HMTP_HVALIDE, hm_hmtp_hvalide, libname);
   F_RGST(HM_HMTP_ASGVAL, hm_hmtp_asgval, libname);
   F_RGST(HM_HMTP_CLCVAL, hm_hmtp_clcval, libname);
   F_RGST(HM_HMTP_REQVAL, hm_hmtp_reqval, libname);
   F_RGST(HM_HMTP_XEQ, hm_hmtp_xeq, libname);
   F_RGST(HM_HMTP_REQALFA, hm_hmtp_reqalfa, libname);
 
   // ---  class HM_PRGL
   F_RGST(HM_PRGL_000, hm_prgl_000, libname);
   F_RGST(HM_PRGL_999, hm_prgl_999, libname);
   F_RGST(HM_PRGL_CTR, hm_prgl_ctr, libname);
   F_RGST(HM_PRGL_DTR, hm_prgl_dtr, libname);
   F_RGST(HM_PRGL_INI, hm_prgl_ini, libname);
   F_RGST(HM_PRGL_RST, hm_prgl_rst, libname);
   F_RGST(HM_PRGL_REQHBASE, hm_prgl_reqhbase, libname);
   F_RGST(HM_PRGL_HVALIDE, hm_prgl_hvalide, libname);
   F_RGST(HM_PRGL_PRN, hm_prgl_prn, libname);
   F_RGST(HM_PRGL_XEQ, hm_prgl_xeq, libname);
   F_RGST(HM_PRGL_REQALFA, hm_prgl_reqalfa, libname);
 
   // ---  class IC_ALALGO
   F_RGST(IC_ALALGO_REQMDL, ic_alalgo_reqmdl, libname);
   F_RGST(IC_ALALGO_OPBDOT, ic_alalgo_opbdot, libname);
   F_RGST(IC_ALALGO_REQNOM, ic_alalgo_reqnom, libname);
   F_RGST(IC_ALALGO_REQHDL, ic_alalgo_reqhdl, libname);
 
   // ---  class IC_ALEULR
   F_RGST(IC_ALEULR_XEQCTR, ic_aleulr_xeqctr, libname);
   F_RGST(IC_ALEULR_XEQMTH, ic_aleulr_xeqmth, libname);
   F_RGST(IC_ALEULR_REQCLS, ic_aleulr_reqcls, libname);
   F_RGST(IC_ALEULR_REQHDL, ic_aleulr_reqhdl, libname);
 
   // ---  class IC_ALGMRS
   F_RGST(IC_ALGMRS_XEQCTR, ic_algmrs_xeqctr, libname);
   F_RGST(IC_ALGMRS_XEQMTH, ic_algmrs_xeqmth, libname);
   F_RGST(IC_ALGMRS_REQCLS, ic_algmrs_reqcls, libname);
   F_RGST(IC_ALGMRS_REQHDL, ic_algmrs_reqhdl, libname);
 
   // ---  class IC_ALHMTP
   F_RGST(IC_ALHMTP_XEQCTR, ic_alhmtp_xeqctr, libname);
   F_RGST(IC_ALHMTP_XEQMTH, ic_alhmtp_xeqmth, libname);
   F_RGST(IC_ALHMTP_REQCLS, ic_alhmtp_reqcls, libname);
   F_RGST(IC_ALHMTP_REQHDL, ic_alhmtp_reqhdl, libname);
 
   // ---  class IC_ALMRQD
   F_RGST(IC_ALMRQD_XEQCTR, ic_almrqd_xeqctr, libname);
   F_RGST(IC_ALMRQD_XEQMTH, ic_almrqd_xeqmth, libname);
   F_RGST(IC_ALMRQD_REQCLS, ic_almrqd_reqcls, libname);
   F_RGST(IC_ALMRQD_REQHDL, ic_almrqd_reqhdl, libname);
 
   // ---  class IC_ALNOBI
   F_RGST(IC_ALNOBI_XEQCTR, ic_alnobi_xeqctr, libname);
   F_RGST(IC_ALNOBI_XEQMTH, ic_alnobi_xeqmth, libname);
   F_RGST(IC_ALNOBI_REQCLS, ic_alnobi_reqcls, libname);
   F_RGST(IC_ALNOBI_REQHDL, ic_alnobi_reqhdl, libname);
 
   // ---  class IC_ALNODO
   F_RGST(IC_ALNODO_XEQCTR, ic_alnodo_xeqctr, libname);
   F_RGST(IC_ALNODO_XEQMTH, ic_alnodo_xeqmth, libname);
   F_RGST(IC_ALNODO_REQCLS, ic_alnodo_reqcls, libname);
   F_RGST(IC_ALNODO_REQHDL, ic_alnodo_reqhdl, libname);
 
   // ---  class IC_ALNOLR
   F_RGST(IC_ALNOLR_XEQCTR, ic_alnolr_xeqctr, libname);
   F_RGST(IC_ALNOLR_XEQMTH, ic_alnolr_xeqmth, libname);
   F_RGST(IC_ALNOLR_REQCLS, ic_alnolr_reqcls, libname);
   F_RGST(IC_ALNOLR_REQHDL, ic_alnolr_reqhdl, libname);
 
   // ---  class IC_ALNOSP
   F_RGST(IC_ALNOSP_XEQCTR, ic_alnosp_xeqctr, libname);
   F_RGST(IC_ALNOSP_XEQMTH, ic_alnosp_xeqmth, libname);
   F_RGST(IC_ALNOSP_REQCLS, ic_alnosp_reqcls, libname);
   F_RGST(IC_ALNOSP_REQHDL, ic_alnosp_reqhdl, libname);
 
   // ---  class IC_ALNWTN
   F_RGST(IC_ALNWTN_XEQCTR, ic_alnwtn_xeqctr, libname);
   F_RGST(IC_ALNWTN_XEQMTH, ic_alnwtn_xeqmth, libname);
   F_RGST(IC_ALNWTN_REQCLS, ic_alnwtn_reqcls, libname);
   F_RGST(IC_ALNWTN_REQHDL, ic_alnwtn_reqhdl, libname);
 
   // ---  class IC_ALPCRD
   F_RGST(IC_ALPCRD_XEQCTR, ic_alpcrd_xeqctr, libname);
   F_RGST(IC_ALPCRD_XEQMTH, ic_alpcrd_xeqmth, libname);
   F_RGST(IC_ALPCRD_REQCLS, ic_alpcrd_reqcls, libname);
   F_RGST(IC_ALPCRD_REQHDL, ic_alpcrd_reqhdl, libname);
 
   // ---  class IC_ALRK4L
   F_RGST(IC_ALRK4L_XEQCTR, ic_alrk4l_xeqctr, libname);
   F_RGST(IC_ALRK4L_XEQMTH, ic_alrk4l_xeqmth, libname);
   F_RGST(IC_ALRK4L_REQCLS, ic_alrk4l_reqcls, libname);
   F_RGST(IC_ALRK4L_REQHDL, ic_alrk4l_reqhdl, libname);
 
   // ---  class IC_ALSLVR
   F_RGST(IC_ALSLVR_XEQCTR, ic_alslvr_xeqctr, libname);
   F_RGST(IC_ALSLVR_XEQMTH, ic_alslvr_xeqmth, libname);
   F_RGST(IC_ALSLVR_REQCLS, ic_alslvr_reqcls, libname);
   F_RGST(IC_ALSLVR_REQHDL, ic_alslvr_reqhdl, libname);
 
   // ---  class IC_ALSQIP
   F_RGST(IC_ALSQIP_CMD, ic_alsqip_cmd, libname);
   F_RGST(IC_ALSQIP_REQCMD, ic_alsqip_reqcmd, libname);
 
   // ---  class IC_ALSQIT
   F_RGST(IC_ALSQIT_XEQCTR, ic_alsqit_xeqctr, libname);
   F_RGST(IC_ALSQIT_XEQMTH, ic_alsqit_xeqmth, libname);
   F_RGST(IC_ALSQIT_REQCLS, ic_alsqit_reqcls, libname);
   F_RGST(IC_ALSQIT_REQHDL, ic_alsqit_reqhdl, libname);
 
   // ---  class IC_ALSQNC
   F_RGST(IC_ALSQNC_XEQCTR, ic_alsqnc_xeqctr, libname);
   F_RGST(IC_ALSQNC_XEQMTH, ic_alsqnc_xeqmth, libname);
   F_RGST(IC_ALSQNC_REQCLS, ic_alsqnc_reqcls, libname);
   F_RGST(IC_ALSQNC_REQHDL, ic_alsqnc_reqhdl, libname);
 
   // ---  class IC_CACOMP
   F_RGST(IC_CACOMP_XEQCTR, ic_cacomp_xeqctr, libname);
   F_RGST(IC_CACOMP_XEQMTH, ic_cacomp_xeqmth, libname);
   F_RGST(IC_CACOMP_REQCLS, ic_cacomp_reqcls, libname);
   F_RGST(IC_CACOMP_REQHDL, ic_cacomp_reqhdl, libname);
 
   // ---  class IC_CACRIA
   F_RGST(IC_CACRIA_XEQCTR, ic_cacria_xeqctr, libname);
   F_RGST(IC_CACRIA_XEQMTH, ic_cacria_xeqmth, libname);
   F_RGST(IC_CACRIA_REQCLS, ic_cacria_reqcls, libname);
   F_RGST(IC_CACRIA_REQHDL, ic_cacria_reqhdl, libname);
 
   // ---  class IC_CAN2AR
   F_RGST(IC_CAN2AR_XEQCTR, ic_can2ar_xeqctr, libname);
   F_RGST(IC_CAN2AR_XEQMTH, ic_can2ar_xeqmth, libname);
   F_RGST(IC_CAN2AR_REQCLS, ic_can2ar_reqcls, libname);
   F_RGST(IC_CAN2AR_REQHDL, ic_can2ar_reqhdl, libname);
 
   // ---  class IC_CAN2GR
   F_RGST(IC_CAN2GR_XEQCTR, ic_can2gr_xeqctr, libname);
   F_RGST(IC_CAN2GR_XEQMTH, ic_can2gr_xeqmth, libname);
   F_RGST(IC_CAN2GR_REQCLS, ic_can2gr_reqcls, libname);
   F_RGST(IC_CAN2GR_REQHDL, ic_can2gr_reqhdl, libname);
 
   // ---  class IC_CANIAR
   F_RGST(IC_CANIAR_XEQCTR, ic_caniar_xeqctr, libname);
   F_RGST(IC_CANIAR_XEQMTH, ic_caniar_xeqmth, libname);
   F_RGST(IC_CANIAR_REQCLS, ic_caniar_reqcls, libname);
   F_RGST(IC_CANIAR_REQHDL, ic_caniar_reqhdl, libname);
 
   // ---  class IC_CANIGR
   F_RGST(IC_CANIGR_XEQCTR, ic_canigr_xeqctr, libname);
   F_RGST(IC_CANIGR_XEQMTH, ic_canigr_xeqmth, libname);
   F_RGST(IC_CANIGR_REQCLS, ic_canigr_reqcls, libname);
   F_RGST(IC_CANIGR_REQHDL, ic_canigr_reqhdl, libname);
 
   // ---  class IC_GLARLX
   F_RGST(IC_GLARLX_XEQCTR, ic_glarlx_xeqctr, libname);
   F_RGST(IC_GLARLX_XEQMTH, ic_glarlx_xeqmth, libname);
   F_RGST(IC_GLARLX_REQCLS, ic_glarlx_reqcls, libname);
   F_RGST(IC_GLARLX_REQHDL, ic_glarlx_reqhdl, libname);
 
   // ---  class IC_GLBKRS
   F_RGST(IC_GLBKRS_XEQCTR, ic_glbkrs_xeqctr, libname);
   F_RGST(IC_GLBKRS_XEQMTH, ic_glbkrs_xeqmth, libname);
   F_RGST(IC_GLBKRS_REQCLS, ic_glbkrs_reqcls, libname);
   F_RGST(IC_GLBKRS_REQHDL, ic_glbkrs_reqhdl, libname);
 
   // ---  class IC_GLBTK2
   F_RGST(IC_GLBTK2_XEQCTR, ic_glbtk2_xeqctr, libname);
   F_RGST(IC_GLBTK2_XEQMTH, ic_glbtk2_xeqmth, libname);
   F_RGST(IC_GLBTK2_REQCLS, ic_glbtk2_reqcls, libname);
   F_RGST(IC_GLBTK2_REQHDL, ic_glbtk2_reqhdl, libname);
 
   // ---  class IC_GLBTK3
   F_RGST(IC_GLBTK3_XEQCTR, ic_glbtk3_xeqctr, libname);
   F_RGST(IC_GLBTK3_XEQMTH, ic_glbtk3_xeqmth, libname);
   F_RGST(IC_GLBTK3_REQCLS, ic_glbtk3_reqcls, libname);
   F_RGST(IC_GLBTK3_REQHDL, ic_glbtk3_reqhdl, libname);
 
   // ---  class IC_GLBTRK
   F_RGST(IC_GLBTRK_XEQCTR, ic_glbtrk_xeqctr, libname);
   F_RGST(IC_GLBTRK_XEQMTH, ic_glbtrk_xeqmth, libname);
   F_RGST(IC_GLBTRK_REQCLS, ic_glbtrk_reqcls, libname);
   F_RGST(IC_GLBTRK_REQHDL, ic_glbtrk_reqhdl, libname);
 
   // ---  class IC_GLCOMP
   F_RGST(IC_GLCOMP_XEQCTR, ic_glcomp_xeqctr, libname);
   F_RGST(IC_GLCOMP_XEQMTH, ic_glcomp_xeqmth, libname);
   F_RGST(IC_GLCOMP_REQCLS, ic_glcomp_reqcls, libname);
   F_RGST(IC_GLCOMP_REQHDL, ic_glcomp_reqhdl, libname);
 
   // ---  class IC_GLCRLX
   F_RGST(IC_GLCRLX_XEQCTR, ic_glcrlx_xeqctr, libname);
   F_RGST(IC_GLCRLX_XEQMTH, ic_glcrlx_xeqmth, libname);
   F_RGST(IC_GLCRLX_REQCLS, ic_glcrlx_reqcls, libname);
   F_RGST(IC_GLCRLX_REQHDL, ic_glcrlx_reqhdl, libname);
 
   // ---  class IC_GLDMPR
   F_RGST(IC_GLDMPR_XEQCTR, ic_gldmpr_xeqctr, libname);
   F_RGST(IC_GLDMPR_XEQMTH, ic_gldmpr_xeqmth, libname);
   F_RGST(IC_GLDMPR_REQCLS, ic_gldmpr_reqcls, libname);
   F_RGST(IC_GLDMPR_REQHDL, ic_gldmpr_reqhdl, libname);
 
   // ---  class IC_GLGLBL
   F_RGST(IC_GLGLBL_REQMDL, ic_glglbl_reqmdl, libname);
   F_RGST(IC_GLGLBL_OPBDOT, ic_glglbl_opbdot, libname);
   F_RGST(IC_GLGLBL_REQNOM, ic_glglbl_reqnom, libname);
   F_RGST(IC_GLGLBL_REQHDL, ic_glglbl_reqhdl, libname);
 
   // ---  class IC_GLLMTR
   F_RGST(IC_GLLMTR_XEQCTR, ic_gllmtr_xeqctr, libname);
   F_RGST(IC_GLLMTR_XEQMTH, ic_gllmtr_xeqmth, libname);
   F_RGST(IC_GLLMTR_REQCLS, ic_gllmtr_reqcls, libname);
   F_RGST(IC_GLLMTR_REQHDL, ic_gllmtr_reqhdl, libname);
 
   // ---  class IC_GLSCLR
   F_RGST(IC_GLSCLR_XEQCTR, ic_glsclr_xeqctr, libname);
   F_RGST(IC_GLSCLR_XEQMTH, ic_glsclr_xeqmth, libname);
   F_RGST(IC_GLSCLR_REQCLS, ic_glsclr_reqcls, libname);
   F_RGST(IC_GLSCLR_REQHDL, ic_glsclr_reqhdl, libname);
 
   // ---  class IC_HMCOMP
   F_RGST(IC_HMCOMP_XEQCTR, ic_hmcomp_xeqctr, libname);
   F_RGST(IC_HMCOMP_XEQMTH, ic_hmcomp_xeqmth, libname);
   F_RGST(IC_HMCOMP_REQCLS, ic_hmcomp_reqcls, libname);
   F_RGST(IC_HMCOMP_REQHDL, ic_hmcomp_reqhdl, libname);
 
   // ---  class IC_HMHMTP
   F_RGST(IC_HMHMTP_XEQCTR, ic_hmhmtp_xeqctr, libname);
   F_RGST(IC_HMHMTP_XEQMTH, ic_hmhmtp_xeqmth, libname);
   F_RGST(IC_HMHMTP_REQCLS, ic_hmhmtp_reqcls, libname);
   F_RGST(IC_HMHMTP_REQHDL, ic_hmhmtp_reqhdl, libname);
 
   // ---  class IC_HMPRGL
   F_RGST(IC_HMPRGL_XEQCTR, ic_hmprgl_xeqctr, libname);
   F_RGST(IC_HMPRGL_XEQMTH, ic_hmprgl_xeqmth, libname);
   F_RGST(IC_HMPRGL_REQCLS, ic_hmprgl_reqcls, libname);
   F_RGST(IC_HMPRGL_REQHDL, ic_hmprgl_reqhdl, libname);
 
   // ---  class IC_LDUD
   F_RGST(IC_LDUD_XEQCTR, ic_ldud_xeqctr, libname);
   F_RGST(IC_LDUD_XEQMTH, ic_ldud_xeqmth, libname);
   F_RGST(IC_LDUD_REQCLS, ic_ldud_reqcls, libname);
   F_RGST(IC_LDUD_REQHDL, ic_ldud_reqhdl, libname);
 
   // ---  class IC_LDUM
   F_RGST(IC_LDUM_XEQCTR, ic_ldum_xeqctr, libname);
   F_RGST(IC_LDUM_XEQMTH, ic_ldum_xeqmth, libname);
   F_RGST(IC_LDUM_REQCLS, ic_ldum_reqcls, libname);
   F_RGST(IC_LDUM_REQHDL, ic_ldum_reqhdl, libname);
 
   // ---  class IC_PRIDEN
   F_RGST(IC_PRIDEN_XEQCTR, ic_priden_xeqctr, libname);
   F_RGST(IC_PRIDEN_XEQMTH, ic_priden_xeqmth, libname);
   F_RGST(IC_PRIDEN_REQCLS, ic_priden_reqcls, libname);
   F_RGST(IC_PRIDEN_REQHDL, ic_priden_reqhdl, libname);
 
   // ---  class IC_PRILUN
   F_RGST(IC_PRILUN_XEQCTR, ic_prilun_xeqctr, libname);
   F_RGST(IC_PRILUN_XEQMTH, ic_prilun_xeqmth, libname);
   F_RGST(IC_PRILUN_REQCLS, ic_prilun_reqcls, libname);
   F_RGST(IC_PRILUN_REQHDL, ic_prilun_reqhdl, libname);
 
   // ---  class IC_PRKTD
   F_RGST(IC_PRKTD_CMD, ic_prktd_cmd, libname);
   F_RGST(IC_PRKTD_REQCMD, ic_prktd_reqcmd, libname);
 
   // ---  class IC_PRMASD
   F_RGST(IC_PRMASD_CMD, ic_prmasd_cmd, libname);
   F_RGST(IC_PRMASD_REQCMD, ic_prmasd_reqcmd, libname);
 
   // ---  class IC_PRPREC
   F_RGST(IC_PRPREC_REQMDL, ic_prprec_reqmdl, libname);
   F_RGST(IC_PRPREC_OPBDOT, ic_prprec_opbdot, libname);
   F_RGST(IC_PRPREC_REQNOM, ic_prprec_reqnom, libname);
   F_RGST(IC_PRPREC_REQHDL, ic_prprec_reqhdl, libname);
 
   // ---  class IC_PRSLVR
   F_RGST(IC_PRSLVR_XEQCTR, ic_prslvr_xeqctr, libname);
   F_RGST(IC_PRSLVR_XEQMTH, ic_prslvr_xeqmth, libname);
   F_RGST(IC_PRSLVR_REQCLS, ic_prslvr_reqcls, libname);
   F_RGST(IC_PRSLVR_REQHDL, ic_prslvr_reqhdl, libname);
 
   // ---  class IC_SOLV
   F_RGST(IC_SOLV_XEQ, ic_solv_xeq, libname);
 
   // ---  class IC_AL_IRK12
   F_RGST(IC_AL_IRK12_XEQCTR, ic_al_irk12_xeqctr, libname);
   F_RGST(IC_AL_IRK12_XEQMTH, ic_al_irk12_xeqmth, libname);
   F_RGST(IC_AL_IRK12_REQCLS, ic_al_irk12_reqcls, libname);
   F_RGST(IC_AL_IRK12_REQHDL, ic_al_irk12_reqhdl, libname);
 
   // ---  class IC_AL_IRK23
   F_RGST(IC_AL_IRK23_XEQCTR, ic_al_irk23_xeqctr, libname);
   F_RGST(IC_AL_IRK23_XEQMTH, ic_al_irk23_xeqmth, libname);
   F_RGST(IC_AL_IRK23_REQCLS, ic_al_irk23_reqcls, libname);
   F_RGST(IC_AL_IRK23_REQHDL, ic_al_irk23_reqhdl, libname);
 
   // ---  class IC_AL_IRK34
   F_RGST(IC_AL_IRK34_XEQCTR, ic_al_irk34_xeqctr, libname);
   F_RGST(IC_AL_IRK34_XEQMTH, ic_al_irk34_xeqmth, libname);
   F_RGST(IC_AL_IRK34_REQCLS, ic_al_irk34_reqcls, libname);
   F_RGST(IC_AL_IRK34_REQHDL, ic_al_irk34_reqhdl, libname);
 
   // ---  class IC_AL_READ
   F_RGST(IC_AL_READ_XEQCTR, ic_al_read_xeqctr, libname);
   F_RGST(IC_AL_READ_XEQMTH, ic_al_read_xeqmth, libname);
   F_RGST(IC_AL_READ_REQCLS, ic_al_read_reqcls, libname);
   F_RGST(IC_AL_READ_REQHDL, ic_al_read_reqhdl, libname);
 
   // ---  class IC_CC_CRIC
   F_RGST(IC_CC_CRIC_XEQCTR, ic_cc_cric_xeqctr, libname);
   F_RGST(IC_CC_CRIC_XEQMTH, ic_cc_cric_xeqmth, libname);
   F_RGST(IC_CC_CRIC_REQCLS, ic_cc_cric_reqcls, libname);
   F_RGST(IC_CC_CRIC_REQHDL, ic_cc_cric_reqhdl, libname);
 
   // ---  class IC_CI_CFL
   F_RGST(IC_CI_CFL_XEQCTR, ic_ci_cfl_xeqctr, libname);
   F_RGST(IC_CI_CFL_XEQMTH, ic_ci_cfl_xeqmth, libname);
   F_RGST(IC_CI_CFL_REQCLS, ic_ci_cfl_reqcls, libname);
   F_RGST(IC_CI_CFL_REQHDL, ic_ci_cfl_reqhdl, libname);
 
   // ---  class IC_CI_CINC
   F_RGST(IC_CI_CINC_XEQCTR, ic_ci_cinc_xeqctr, libname);
   F_RGST(IC_CI_CINC_XEQMTH, ic_ci_cinc_xeqmth, libname);
   F_RGST(IC_CI_CINC_REQCLS, ic_ci_cinc_reqcls, libname);
   F_RGST(IC_CI_CINC_REQHDL, ic_ci_cinc_reqhdl, libname);
 
   // ---  class IC_CI_COMP
   F_RGST(IC_CI_COMP_XEQCTR, ic_ci_comp_xeqctr, libname);
   F_RGST(IC_CI_COMP_XEQMTH, ic_ci_comp_xeqmth, libname);
   F_RGST(IC_CI_COMP_REQCLS, ic_ci_comp_reqcls, libname);
   F_RGST(IC_CI_COMP_REQHDL, ic_ci_comp_reqhdl, libname);
 
   // ---  class IC_CI_LMTR
   F_RGST(IC_CI_LMTR_XEQCTR, ic_ci_lmtr_xeqctr, libname);
   F_RGST(IC_CI_LMTR_XEQMTH, ic_ci_lmtr_xeqmth, libname);
   F_RGST(IC_CI_LMTR_REQCLS, ic_ci_lmtr_reqcls, libname);
   F_RGST(IC_CI_LMTR_REQHDL, ic_ci_lmtr_reqhdl, libname);
 
   // ---  class IC_CI_NOOP
   F_RGST(IC_CI_NOOP_XEQCTR, ic_ci_noop_xeqctr, libname);
   F_RGST(IC_CI_NOOP_XEQMTH, ic_ci_noop_xeqmth, libname);
   F_RGST(IC_CI_NOOP_REQCLS, ic_ci_noop_reqcls, libname);
   F_RGST(IC_CI_NOOP_REQHDL, ic_ci_noop_reqhdl, libname);
 
   // ---  class IC_CI_PIDC
   F_RGST(IC_CI_PIDC_XEQCTR, ic_ci_pidc_xeqctr, libname);
   F_RGST(IC_CI_PIDC_XEQMTH, ic_ci_pidc_xeqmth, libname);
   F_RGST(IC_CI_PIDC_REQCLS, ic_ci_pidc_reqcls, libname);
   F_RGST(IC_CI_PIDC_REQHDL, ic_ci_pidc_reqhdl, libname);
 
   // ---  class IC_CI_PRDC
   F_RGST(IC_CI_PRDC_XEQCTR, ic_ci_prdc_xeqctr, libname);
   F_RGST(IC_CI_PRDC_XEQMTH, ic_ci_prdc_xeqmth, libname);
   F_RGST(IC_CI_PRDC_REQCLS, ic_ci_prdc_reqcls, libname);
   F_RGST(IC_CI_PRDC_REQHDL, ic_ci_prdc_reqhdl, libname);
 
   // ---  class IC_CI_SMPL
   F_RGST(IC_CI_SMPL_XEQCTR, ic_ci_smpl_xeqctr, libname);
   F_RGST(IC_CI_SMPL_XEQMTH, ic_ci_smpl_xeqmth, libname);
   F_RGST(IC_CI_SMPL_REQCLS, ic_ci_smpl_reqcls, libname);
   F_RGST(IC_CI_SMPL_REQHDL, ic_ci_smpl_reqhdl, libname);
 
   // ---  class IC_PJ_POD
   F_RGST(IC_PJ_POD_XEQCTR, ic_pj_pod_xeqctr, libname);
   F_RGST(IC_PJ_POD_XEQMTH, ic_pj_pod_xeqmth, libname);
   F_RGST(IC_PJ_POD_REQCLS, ic_pj_pod_reqcls, libname);
   F_RGST(IC_PJ_POD_REQHDL, ic_pj_pod_reqhdl, libname);
 
   // ---  class IC_PJ_POLY
   F_RGST(IC_PJ_POLY_XEQCTR, ic_pj_poly_xeqctr, libname);
   F_RGST(IC_PJ_POLY_XEQMTH, ic_pj_poly_xeqmth, libname);
   F_RGST(IC_PJ_POLY_REQCLS, ic_pj_poly_reqcls, libname);
   F_RGST(IC_PJ_POLY_REQHDL, ic_pj_poly_reqhdl, libname);
 
   // ---  class IC_PJ_PRJC
   F_RGST(IC_PJ_PRJC_XEQCTR, ic_pj_prjc_xeqctr, libname);
   F_RGST(IC_PJ_PRJC_XEQMTH, ic_pj_prjc_xeqmth, libname);
   F_RGST(IC_PJ_PRJC_REQCLS, ic_pj_prjc_reqcls, libname);
   F_RGST(IC_PJ_PRJC_REQHDL, ic_pj_prjc_reqhdl, libname);
 
   // ---  class MA_MORS
   F_RGST(MA_MORS_000, ma_mors_000, libname);
   F_RGST(MA_MORS_999, ma_mors_999, libname);
   F_RGST(MA_MORS_CTR, ma_mors_ctr, libname);
   F_RGST(MA_MORS_DTR, ma_mors_dtr, libname);
   F_RGST(MA_MORS_INI, ma_mors_ini, libname);
   F_RGST(MA_MORS_RST, ma_mors_rst, libname);
   F_RGST(MA_MORS_REQHBASE, ma_mors_reqhbase, libname);
   F_RGST(MA_MORS_HVALIDE, ma_mors_hvalide, libname);
   F_RGST(MA_MORS_ASMMTX, ma_mors_asmmtx, libname);
   F_RGST(MA_MORS_ASMRHS, ma_mors_asmrhs, libname);
 
   // ---  class MR_GMRS
   F_RGST(MR_GMRS_000, mr_gmrs_000, libname);
   F_RGST(MR_GMRS_999, mr_gmrs_999, libname);
   F_RGST(MR_GMRS_CTR, mr_gmrs_ctr, libname);
   F_RGST(MR_GMRS_DTR, mr_gmrs_dtr, libname);
   F_RGST(MR_GMRS_INI, mr_gmrs_ini, libname);
   F_RGST(MR_GMRS_RST, mr_gmrs_rst, libname);
   F_RGST(MR_GMRS_HVALIDE, mr_gmrs_hvalide, libname);
   F_RGST(MR_GMRS_ESTDIRECTE, mr_gmrs_estdirecte, libname);
   F_RGST(MR_GMRS_XEQ, mr_gmrs_xeq, libname);
 
   // ---  class MR_LDUD
   F_RGST(MR_LDUD_000, mr_ldud_000, libname);
   F_RGST(MR_LDUD_999, mr_ldud_999, libname);
   F_RGST(MR_LDUD_CTR, mr_ldud_ctr, libname);
   F_RGST(MR_LDUD_DTR, mr_ldud_dtr, libname);
   F_RGST(MR_LDUD_INI, mr_ldud_ini, libname);
   F_RGST(MR_LDUD_RST, mr_ldud_rst, libname);
   F_RGST(MR_LDUD_REQHBASE, mr_ldud_reqhbase, libname);
   F_RGST(MR_LDUD_HVALIDE, mr_ldud_hvalide, libname);
   F_RGST(MR_LDUD_ESTDIRECTE, mr_ldud_estdirecte, libname);
   F_RGST(MR_LDUD_ASMMTX, mr_ldud_asmmtx, libname);
   F_RGST(MR_LDUD_FCTMTX, mr_ldud_fctmtx, libname);
   F_RGST(MR_LDUD_RESMTX, mr_ldud_resmtx, libname);
   F_RGST(MR_LDUD_XEQ, mr_ldud_xeq, libname);
 
   // ---  class MR_LDUM
   F_RGST(MR_LDUM_000, mr_ldum_000, libname);
   F_RGST(MR_LDUM_999, mr_ldum_999, libname);
   F_RGST(MR_LDUM_CTR, mr_ldum_ctr, libname);
   F_RGST(MR_LDUM_DTR, mr_ldum_dtr, libname);
   F_RGST(MR_LDUM_INI, mr_ldum_ini, libname);
   F_RGST(MR_LDUM_RST, mr_ldum_rst, libname);
   F_RGST(MR_LDUM_REQHBASE, mr_ldum_reqhbase, libname);
   F_RGST(MR_LDUM_HVALIDE, mr_ldum_hvalide, libname);
   F_RGST(MR_LDUM_ESTDIRECTE, mr_ldum_estdirecte, libname);
   F_RGST(MR_LDUM_ASMMTX, mr_ldum_asmmtx, libname);
   F_RGST(MR_LDUM_FCTMTX, mr_ldum_fctmtx, libname);
   F_RGST(MR_LDUM_RESMTX, mr_ldum_resmtx, libname);
   F_RGST(MR_LDUM_XEQ, mr_ldum_xeq, libname);
 
   // ---  class MR_LMPD
   F_RGST(MR_LMPD_000, mr_lmpd_000, libname);
   F_RGST(MR_LMPD_999, mr_lmpd_999, libname);
   F_RGST(MR_LMPD_CTR, mr_lmpd_ctr, libname);
   F_RGST(MR_LMPD_DTR, mr_lmpd_dtr, libname);
   F_RGST(MR_LMPD_INI, mr_lmpd_ini, libname);
   F_RGST(MR_LMPD_RST, mr_lmpd_rst, libname);
   F_RGST(MR_LMPD_REQHBASE, mr_lmpd_reqhbase, libname);
   F_RGST(MR_LMPD_HVALIDE, mr_lmpd_hvalide, libname);
   F_RGST(MR_LMPD_ESTDIRECTE, mr_lmpd_estdirecte, libname);
   F_RGST(MR_LMPD_ASMMTX, mr_lmpd_asmmtx, libname);
   F_RGST(MR_LMPD_FCTMTX, mr_lmpd_fctmtx, libname);
   F_RGST(MR_LMPD_RESMTX, mr_lmpd_resmtx, libname);
   F_RGST(MR_LMPD_XEQ, mr_lmpd_xeq, libname);
 
   // ---  class MR_RESO
   F_RGST(MR_RESO_000, mr_reso_000, libname);
   F_RGST(MR_RESO_999, mr_reso_999, libname);
   F_RGST(MR_RESO_CTR, mr_reso_ctr, libname);
   F_RGST(MR_RESO_DTR, mr_reso_dtr, libname);
   F_RGST(MR_RESO_INI, mr_reso_ini, libname);
   F_RGST(MR_RESO_RST, mr_reso_rst, libname);
   F_RGST(MR_RESO_HVALIDE, mr_reso_hvalide, libname);
   F_RGST(MR_RESO_ESTDIRECTE, mr_reso_estdirecte, libname);
   F_RGST(MR_RESO_ASMMTX, mr_reso_asmmtx, libname);
   F_RGST(MR_RESO_FCTMTX, mr_reso_fctmtx, libname);
   F_RGST(MR_RESO_RESMTX, mr_reso_resmtx, libname);
   F_RGST(MR_RESO_XEQ, mr_reso_xeq, libname);
 
   // ---  class MX_CPNT
   F_RGST(MX_CPNT_000, mx_cpnt_000, libname);
   F_RGST(MX_CPNT_999, mx_cpnt_999, libname);
   F_RGST(MX_CPNT_CTR, mx_cpnt_ctr, libname);
   F_RGST(MX_CPNT_DTR, mx_cpnt_dtr, libname);
   F_RGST(MX_CPNT_INI, mx_cpnt_ini, libname);
   F_RGST(MX_CPNT_RST, mx_cpnt_rst, libname);
   F_RGST(MX_CPNT_REQHBASE, mx_cpnt_reqhbase, libname);
   F_RGST(MX_CPNT_HVALIDE, mx_cpnt_hvalide, libname);
   F_RGST(MX_CPNT_ASMKE, mx_cpnt_asmke, libname);
   F_RGST(MX_CPNT_DIMMAT, mx_cpnt_dimmat, libname);
   F_RGST(MX_CPNT_LISMAT, mx_cpnt_lismat, libname);
   F_RGST(MX_CPNT_ADDMAT, mx_cpnt_addmat, libname);
   F_RGST(MX_CPNT_MULVAL, mx_cpnt_mulval, libname);
   F_RGST(MX_CPNT_MULVEC, mx_cpnt_mulvec, libname);
   F_RGST(MX_CPNT_REQILU, mx_cpnt_reqilu, libname);
   F_RGST(MX_CPNT_REQNEQL, mx_cpnt_reqneql, libname);
   F_RGST(MX_CPNT_REQNKGP, mx_cpnt_reqnkgp, libname);
   F_RGST(MX_CPNT_REQLIAP, mx_cpnt_reqliap, libname);
   F_RGST(MX_CPNT_REQLJAP, mx_cpnt_reqljap, libname);
   F_RGST(MX_CPNT_REQLJDP, mx_cpnt_reqljdp, libname);
   F_RGST(MX_CPNT_REQLKG, mx_cpnt_reqlkg, libname);
   F_RGST(MX_CPNT_DMPMAT, mx_cpnt_dmpmat, libname);
 
   // ---  class MX_DIAG
   F_RGST(MX_DIAG_000, mx_diag_000, libname);
   F_RGST(MX_DIAG_999, mx_diag_999, libname);
   F_RGST(MX_DIAG_CTR, mx_diag_ctr, libname);
   F_RGST(MX_DIAG_DTR, mx_diag_dtr, libname);
   F_RGST(MX_DIAG_INI, mx_diag_ini, libname);
   F_RGST(MX_DIAG_RST, mx_diag_rst, libname);
   F_RGST(MX_DIAG_REQHBASE, mx_diag_reqhbase, libname);
   F_RGST(MX_DIAG_HVALIDE, mx_diag_hvalide, libname);
   F_RGST(MX_DIAG_ASMKE, mx_diag_asmke, libname);
   F_RGST(MX_DIAG_ASMFIN, mx_diag_asmfin, libname);
   F_RGST(MX_DIAG_ASGMTX, mx_diag_asgmtx, libname);
   F_RGST(MX_DIAG_DIMMAT, mx_diag_dimmat, libname);
   F_RGST(MX_DIAG_ADDMAT, mx_diag_addmat, libname);
   F_RGST(MX_DIAG_INIMAT, mx_diag_inimat, libname);
   F_RGST(MX_DIAG_MULVAL, mx_diag_mulval, libname);
   F_RGST(MX_DIAG_MULVEC, mx_diag_mulvec, libname);
   F_RGST(MX_DIAG_MULVECI, mx_diag_mulveci, libname);
   F_RGST(MX_DIAG_ASMDIM, mx_diag_asmdim, libname);
   F_RGST(MX_DIAG_REQHMORS, mx_diag_reqhmors, libname);
   F_RGST(MX_DIAG_REQNEQL, mx_diag_reqneql, libname);
   F_RGST(MX_DIAG_REQLKG, mx_diag_reqlkg, libname);
   F_RGST(MX_DIAG_DMPMAT, mx_diag_dmpmat, libname);
 
   // ---  class MX_DIST
   F_RGST(MX_DIST_000, mx_dist_000, libname);
   F_RGST(MX_DIST_999, mx_dist_999, libname);
   F_RGST(MX_DIST_CTR, mx_dist_ctr, libname);
   F_RGST(MX_DIST_DTR, mx_dist_dtr, libname);
   F_RGST(MX_DIST_INI, mx_dist_ini, libname);
   F_RGST(MX_DIST_RST, mx_dist_rst, libname);
   F_RGST(MX_DIST_REQHBASE, mx_dist_reqhbase, libname);
   F_RGST(MX_DIST_HVALIDE, mx_dist_hvalide, libname);
   F_RGST(MX_DIST_ASMKE, mx_dist_asmke, libname);
   F_RGST(MX_DIST_DIMMAT, mx_dist_dimmat, libname);
   F_RGST(MX_DIST_ADDMAT, mx_dist_addmat, libname);
   F_RGST(MX_DIST_MULVAL, mx_dist_mulval, libname);
   F_RGST(MX_DIST_MULVEC, mx_dist_mulvec, libname);
   F_RGST(MX_DIST_REQNEQL, mx_dist_reqneql, libname);
   F_RGST(MX_DIST_REQNEQP, mx_dist_reqneqp, libname);
   F_RGST(MX_DIST_REQNEQT, mx_dist_reqneqt, libname);
   F_RGST(MX_DIST_REQHMORS, mx_dist_reqhmors, libname);
   F_RGST(MX_DIST_REQLLING, mx_dist_reqlling, libname);
   F_RGST(MX_DIST_REQLCOLG, mx_dist_reqlcolg, libname);
 
   // ---  class MX_DNSE
   F_RGST(MX_DNSE_000, mx_dnse_000, libname);
   F_RGST(MX_DNSE_999, mx_dnse_999, libname);
   F_RGST(MX_DNSE_CTR, mx_dnse_ctr, libname);
   F_RGST(MX_DNSE_DTR, mx_dnse_dtr, libname);
   F_RGST(MX_DNSE_INI, mx_dnse_ini, libname);
   F_RGST(MX_DNSE_RST, mx_dnse_rst, libname);
   F_RGST(MX_DNSE_REQHBASE, mx_dnse_reqhbase, libname);
   F_RGST(MX_DNSE_HVALIDE, mx_dnse_hvalide, libname);
   F_RGST(MX_DNSE_ASMKE, mx_dnse_asmke, libname);
   F_RGST(MX_DNSE_DIMMAT, mx_dnse_dimmat, libname);
   F_RGST(MX_DNSE_ADDMAT, mx_dnse_addmat, libname);
   F_RGST(MX_DNSE_MULVAL, mx_dnse_mulval, libname);
   F_RGST(MX_DNSE_MULVEC, mx_dnse_mulvec, libname);
   F_RGST(MX_DNSE_REQNEQL, mx_dnse_reqneql, libname);
   F_RGST(MX_DNSE_REQLKG, mx_dnse_reqlkg, libname);
 
   // ---  class MX_MORS
   F_RGST(MX_MORS_000, mx_mors_000, libname);
   F_RGST(MX_MORS_999, mx_mors_999, libname);
   F_RGST(MX_MORS_CTR, mx_mors_ctr, libname);
   F_RGST(MX_MORS_DTR, mx_mors_dtr, libname);
   F_RGST(MX_MORS_RAZ, mx_mors_raz, libname);
   F_RGST(MX_MORS_INI, mx_mors_ini, libname);
   F_RGST(MX_MORS_RST, mx_mors_rst, libname);
   F_RGST(MX_MORS_REQHBASE, mx_mors_reqhbase, libname);
   F_RGST(MX_MORS_HVALIDE, mx_mors_hvalide, libname);
   F_RGST(MX_MORS_ASMKE, mx_mors_asmke, libname);
   F_RGST(MX_MORS_DIMIND, mx_mors_dimind, libname);
   F_RGST(MX_MORS_DIMMAT, mx_mors_dimmat, libname);
   F_RGST(MX_MORS_LISMAT, mx_mors_lismat, libname);
   F_RGST(MX_MORS_DIAGMIN, mx_mors_diagmin, libname);
   F_RGST(MX_MORS_ADDMAT, mx_mors_addmat, libname);
   F_RGST(MX_MORS_MULVAL, mx_mors_mulval, libname);
   F_RGST(MX_MORS_MULVEC, mx_mors_mulvec, libname);
   F_RGST(MX_MORS_REQCRC32, mx_mors_reqcrc32, libname);
   F_RGST(MX_MORS_REQILU, mx_mors_reqilu, libname);
   F_RGST(MX_MORS_REQNEQL, mx_mors_reqneql, libname);
   F_RGST(MX_MORS_REQNKGP, mx_mors_reqnkgp, libname);
   F_RGST(MX_MORS_REQLIAP, mx_mors_reqliap, libname);
   F_RGST(MX_MORS_REQLJAP, mx_mors_reqljap, libname);
   F_RGST(MX_MORS_REQLJDP, mx_mors_reqljdp, libname);
   F_RGST(MX_MORS_REQLKG, mx_mors_reqlkg, libname);
   F_RGST(MX_MORS_DMPMAT, mx_mors_dmpmat, libname);
 
   // ---  class MX_MTRX
   F_RGST(MX_MTRX_DTR, mx_mtrx_dtr, libname);
   F_RGST(MX_MTRX_CTRLH, mx_mtrx_ctrlh, libname);
   F_RGST(MX_MTRX_ASMKE, mx_mtrx_asmke, libname);
   F_RGST(MX_MTRX_DIMMAT, mx_mtrx_dimmat, libname);
   F_RGST(MX_MTRX_ADDMAT, mx_mtrx_addmat, libname);
   F_RGST(MX_MTRX_MULVAL, mx_mtrx_mulval, libname);
   F_RGST(MX_MTRX_MULVEC, mx_mtrx_mulvec, libname);
   F_RGST(MX_MTRX_REQNEQL, mx_mtrx_reqneql, libname);
 
   // ---  class MX_PRXY
   F_RGST(MX_PRXY_000, mx_prxy_000, libname);
   F_RGST(MX_PRXY_999, mx_prxy_999, libname);
   F_RGST(MX_PRXY_CTR, mx_prxy_ctr, libname);
   F_RGST(MX_PRXY_DTR, mx_prxy_dtr, libname);
   F_RGST(MX_PRXY_INI, mx_prxy_ini, libname);
   F_RGST(MX_PRXY_RST, mx_prxy_rst, libname);
   F_RGST(MX_PRXY_REQHBASE, mx_prxy_reqhbase, libname);
   F_RGST(MX_PRXY_HVALIDE, mx_prxy_hvalide, libname);
   F_RGST(MX_PRXY_ASMKE, mx_prxy_asmke, libname);
   F_RGST(MX_PRXY_DIMMAT, mx_prxy_dimmat, libname);
   F_RGST(MX_PRXY_ADDMAT, mx_prxy_addmat, libname);
   F_RGST(MX_PRXY_MULVAL, mx_prxy_mulval, libname);
   F_RGST(MX_PRXY_MULVEC, mx_prxy_mulvec, libname);
   F_RGST(MX_PRXY_REQNEQL, mx_prxy_reqneql, libname);
 
   // ---  class MX_SKYL
   F_RGST(MX_SKYL_000, mx_skyl_000, libname);
   F_RGST(MX_SKYL_999, mx_skyl_999, libname);
   F_RGST(MX_SKYL_CTR, mx_skyl_ctr, libname);
   F_RGST(MX_SKYL_DTR, mx_skyl_dtr, libname);
   F_RGST(MX_SKYL_INI, mx_skyl_ini, libname);
   F_RGST(MX_SKYL_RST, mx_skyl_rst, libname);
   F_RGST(MX_SKYL_REQHBASE, mx_skyl_reqhbase, libname);
   F_RGST(MX_SKYL_HVALIDE, mx_skyl_hvalide, libname);
   F_RGST(MX_SKYL_ASMKE, mx_skyl_asmke, libname);
   F_RGST(MX_SKYL_DIMMAT, mx_skyl_dimmat, libname);
   F_RGST(MX_SKYL_ADDMAT, mx_skyl_addmat, libname);
   F_RGST(MX_SKYL_MULVAL, mx_skyl_mulval, libname);
   F_RGST(MX_SKYL_MULVEC, mx_skyl_mulvec, libname);
   F_RGST(MX_SKYL_DMPMAT, mx_skyl_dmpmat, libname);
   F_RGST(MX_SKYL_REQCRC32, mx_skyl_reqcrc32, libname);
   F_RGST(MX_SKYL_REQNEQL, mx_skyl_reqneql, libname);
   F_RGST(MX_SKYL_REQNKGP, mx_skyl_reqnkgp, libname);
   F_RGST(MX_SKYL_REQLIAP, mx_skyl_reqliap, libname);
   F_RGST(MX_SKYL_REQLJAP, mx_skyl_reqljap, libname);
   F_RGST(MX_SKYL_REQLJDP, mx_skyl_reqljdp, libname);
   F_RGST(MX_SKYL_REQLKGS, mx_skyl_reqlkgs, libname);
   F_RGST(MX_SKYL_REQLKGD, mx_skyl_reqlkgd, libname);
   F_RGST(MX_SKYL_REQLKGI, mx_skyl_reqlkgi, libname);
 
   // ---  class PJ_POD
   F_RGST(PJ_POD_000, pj_pod_000, libname);
   F_RGST(PJ_POD_999, pj_pod_999, libname);
   F_RGST(PJ_POD_CTR, pj_pod_ctr, libname);
   F_RGST(PJ_POD_DTR, pj_pod_dtr, libname);
   F_RGST(PJ_POD_INI, pj_pod_ini, libname);
   F_RGST(PJ_POD_RST, pj_pod_rst, libname);
   F_RGST(PJ_POD_REQHBASE, pj_pod_reqhbase, libname);
   F_RGST(PJ_POD_HVALIDE, pj_pod_hvalide, libname);
   F_RGST(PJ_POD_AJT, pj_pod_ajt, libname);
   F_RGST(PJ_POD_PRJ, pj_pod_prj, libname);
   F_RGST(PJ_POD_XEQ, pj_pod_xeq, libname);
 
   // ---  class PJ_POLY
   F_RGST(PJ_POLY_000, pj_poly_000, libname);
   F_RGST(PJ_POLY_999, pj_poly_999, libname);
   F_RGST(PJ_POLY_CTR, pj_poly_ctr, libname);
   F_RGST(PJ_POLY_DTR, pj_poly_dtr, libname);
   F_RGST(PJ_POLY_INI, pj_poly_ini, libname);
   F_RGST(PJ_POLY_RST, pj_poly_rst, libname);
   F_RGST(PJ_POLY_REQHBASE, pj_poly_reqhbase, libname);
   F_RGST(PJ_POLY_HVALIDE, pj_poly_hvalide, libname);
   F_RGST(PJ_POLY_AJT, pj_poly_ajt, libname);
   F_RGST(PJ_POLY_PRJ, pj_poly_prj, libname);
   F_RGST(PJ_POLY_XEQ, pj_poly_xeq, libname);
 
   // ---  class PJ_PRJC
   F_RGST(PJ_PRJC_000, pj_prjc_000, libname);
   F_RGST(PJ_PRJC_999, pj_prjc_999, libname);
   F_RGST(PJ_PRJC_CTR, pj_prjc_ctr, libname);
   F_RGST(PJ_PRJC_DTR, pj_prjc_dtr, libname);
   F_RGST(PJ_PRJC_INI, pj_prjc_ini, libname);
   F_RGST(PJ_PRJC_RST, pj_prjc_rst, libname);
   F_RGST(PJ_PRJC_REQHBASE, pj_prjc_reqhbase, libname);
   F_RGST(PJ_PRJC_HVALIDE, pj_prjc_hvalide, libname);
   F_RGST(PJ_PRJC_AJT, pj_prjc_ajt, libname);
   F_RGST(PJ_PRJC_PRJ, pj_prjc_prj, libname);
   F_RGST(PJ_PRJC_XEQ, pj_prjc_xeq, libname);
   F_RGST(PJ_PRJC_DEB, pj_prjc_deb, libname);
 
   // ---  class PR_DIAG
   F_RGST(PR_DIAG_000, pr_diag_000, libname);
   F_RGST(PR_DIAG_999, pr_diag_999, libname);
   F_RGST(PR_DIAG_CTR, pr_diag_ctr, libname);
   F_RGST(PR_DIAG_DTR, pr_diag_dtr, libname);
   F_RGST(PR_DIAG_INI, pr_diag_ini, libname);
   F_RGST(PR_DIAG_RST, pr_diag_rst, libname);
   F_RGST(PR_DIAG_REQHBASE, pr_diag_reqhbase, libname);
   F_RGST(PR_DIAG_HVALIDE, pr_diag_hvalide, libname);
   F_RGST(PR_DIAG_ASM, pr_diag_asm, libname);
   F_RGST(PR_DIAG_PRC, pr_diag_prc, libname);
 
   // ---  class PR_IDEN
   F_RGST(PR_IDEN_000, pr_iden_000, libname);
   F_RGST(PR_IDEN_999, pr_iden_999, libname);
   F_RGST(PR_IDEN_CTR, pr_iden_ctr, libname);
   F_RGST(PR_IDEN_DTR, pr_iden_dtr, libname);
   F_RGST(PR_IDEN_INI, pr_iden_ini, libname);
   F_RGST(PR_IDEN_RST, pr_iden_rst, libname);
   F_RGST(PR_IDEN_REQHBASE, pr_iden_reqhbase, libname);
   F_RGST(PR_IDEN_HVALIDE, pr_iden_hvalide, libname);
   F_RGST(PR_IDEN_ASM, pr_iden_asm, libname);
   F_RGST(PR_IDEN_PRC, pr_iden_prc, libname);
 
   // ---  class PR_ILUN
   F_RGST(PR_ILUN_000, pr_ilun_000, libname);
   F_RGST(PR_ILUN_999, pr_ilun_999, libname);
   F_RGST(PR_ILUN_CTR, pr_ilun_ctr, libname);
   F_RGST(PR_ILUN_DTR, pr_ilun_dtr, libname);
   F_RGST(PR_ILUN_INI, pr_ilun_ini, libname);
   F_RGST(PR_ILUN_RST, pr_ilun_rst, libname);
   F_RGST(PR_ILUN_REQHBASE, pr_ilun_reqhbase, libname);
   F_RGST(PR_ILUN_HVALIDE, pr_ilun_hvalide, libname);
   F_RGST(PR_ILUN_ASM, pr_ilun_asm, libname);
   F_RGST(PR_ILUN_PRC, pr_ilun_prc, libname);
   F_RGST(PR_ILUN_REQILU, pr_ilun_reqilu, libname);
 
   // ---  class PR_MASD
   F_RGST(PR_MASD_000, pr_masd_000, libname);
   F_RGST(PR_MASD_999, pr_masd_999, libname);
   F_RGST(PR_MASD_CTR, pr_masd_ctr, libname);
   F_RGST(PR_MASD_DTR, pr_masd_dtr, libname);
   F_RGST(PR_MASD_INI, pr_masd_ini, libname);
   F_RGST(PR_MASD_RST, pr_masd_rst, libname);
   F_RGST(PR_MASD_REQHBASE, pr_masd_reqhbase, libname);
   F_RGST(PR_MASD_HVALIDE, pr_masd_hvalide, libname);
   F_RGST(PR_MASD_ASM, pr_masd_asm, libname);
   F_RGST(PR_MASD_PRC, pr_masd_prc, libname);
 
   // ---  class PR_PREC
   F_RGST(PR_PREC_DTR, pr_prec_dtr, libname);
   F_RGST(PR_PREC_CTRLH, pr_prec_ctrlh, libname);
   F_RGST(PR_PREC_ASM, pr_prec_asm, libname);
   F_RGST(PR_PREC_PRC, pr_prec_prc, libname);
 
   // ---  class PR_PRXY
   F_RGST(PR_PRXY_000, pr_prxy_000, libname);
   F_RGST(PR_PRXY_999, pr_prxy_999, libname);
   F_RGST(PR_PRXY_CTR, pr_prxy_ctr, libname);
   F_RGST(PR_PRXY_DTR, pr_prxy_dtr, libname);
   F_RGST(PR_PRXY_INI, pr_prxy_ini, libname);
   F_RGST(PR_PRXY_RST, pr_prxy_rst, libname);
   F_RGST(PR_PRXY_HVALIDE, pr_prxy_hvalide, libname);
   F_RGST(PR_PRXY_ASM, pr_prxy_asm, libname);
   F_RGST(PR_PRXY_PRC, pr_prxy_prc, libname);
 
   // ---  class PR_SLVR
   F_RGST(PR_SLVR_000, pr_slvr_000, libname);
   F_RGST(PR_SLVR_999, pr_slvr_999, libname);
   F_RGST(PR_SLVR_CTR, pr_slvr_ctr, libname);
   F_RGST(PR_SLVR_DTR, pr_slvr_dtr, libname);
   F_RGST(PR_SLVR_INI, pr_slvr_ini, libname);
   F_RGST(PR_SLVR_RST, pr_slvr_rst, libname);
   F_RGST(PR_SLVR_REQHBASE, pr_slvr_reqhbase, libname);
   F_RGST(PR_SLVR_HVALIDE, pr_slvr_hvalide, libname);
   F_RGST(PR_SLVR_ASM, pr_slvr_asm, libname);
   F_RGST(PR_SLVR_PRC, pr_slvr_prc, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
