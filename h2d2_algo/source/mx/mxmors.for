C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER MX_MORS_000
C     INTEGER MX_MORS_999
C     INTEGER MX_MORS_CTR
C     INTEGER MX_MORS_DTR
C     SUBROUTINE MX_MORS_RAZ
C     INTEGER MX_MORS_INI
C     INTEGER MX_MORS_RST
C     INTEGER MX_MORS_REQHBASE
C     LOGICAL MX_MORS_HVALIDE
C     INTEGER MX_MORS_ASMKE
C     INTEGER MX_MORS_DIMIND
C     INTEGER MX_MORS_DIMMAT
C     INTEGER MX_MORS_LISMAT
C     INTEGER MX_MORS_DIAGMIN
C     INTEGER MX_MORS_ADDMAT
C     INTEGER MX_MORS_MULVAL
C     INTEGER MX_MORS_MULVEC
C     INTEGER MX_MORS_REQCRC32
C     INTEGER MX_MORS_REQILU
C     INTEGER MX_MORS_REQNEQL
C     INTEGER MX_MORS_REQNKGP
C     INTEGER MX_MORS_REQLIAP
C     INTEGER MX_MORS_REQLJAP
C     INTEGER MX_MORS_REQLJDP
C     INTEGER MX_MORS_REQLKG
C     INTEGER MX_MORS_DMPMAT
C   Private:
C     SUBROUTINE MX_MORS_ASMDIM
C     INTEGER MX_MORS_CLCDIM
C     INTEGER MX_MORS_ASMIND
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MX_MORS_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_000
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INCLUDE 'mxmors.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(MX_MORS_HBASE,
     &                   'Matrix sparse')

      MX_MORS_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_999
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INCLUDE 'mxmors.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL MX_MORS_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(MX_MORS_HBASE,
     &                   MX_MORS_DTR)

      MX_MORS_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_CTR
CDEC$ ENDIF

      USE MX_MORS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR, IRET
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   MX_MORS_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = MX_MORS_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. MX_MORS_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      MX_MORS_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_DTR
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = MX_MORS_RST(HOBJ)

C---     Dé-alloue
      SELF => MX_MORS_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, MX_MORS_HBASE)

      MX_MORS_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée MX_MORS_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_RAZ(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_RAZ
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fc'

      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      SELF%ILU  = 0
      SELF%HSIM = 0
      SELF%HHSH = 0
      SELF%NEQL = 0
      SELF%NKGP = 0
      SELF%NKGSP= 0
      SELF%LIAP = SO_ALLC_PTR()
      SELF%LJDP = SO_ALLC_PTR()
      SELF%LJAP = SO_ALLC_PTR()
      SELF%LKG  = SO_ALLC_PTR()

      MX_MORS_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_INI(HOBJ, ILU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_INI
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER ILU

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = MX_MORS_RST(HOBJ)

C---     Assigne ILU
      IF (ERR_GOOD()) THEN
         SELF => MX_MORS_REQSELF(HOBJ)
         SELF%ILU = ILU
      ENDIF

      MX_MORS_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_RST
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      INTEGER LIAP, LJDP, LJAP, LKG
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      LIAP = SELF%LIAP%REQHNDL()
      LJDP = SELF%LJDP%REQHNDL()
      LJAP = SELF%LJAP%REQHNDL()
      LKG  = SELF%LKG %REQHNDL()

C---     Récupère la mémoire
      IF (SO_ALLC_HEXIST(LIAP)) IERR = SO_ALLC_ALLINT(0, LIAP)
      IF (SO_ALLC_HEXIST(LJDP)) IERR = SO_ALLC_ALLINT(0, LJDP)
      IF (SO_ALLC_HEXIST(LJAP)) IERR = SO_ALLC_ALLINT(0, LJAP)
      IF (SO_ALLC_HEXIST(LKG))  IERR = SO_ALLC_ALLRE8(0, LKG)

C---     Reset
      IERR = MX_MORS_RAZ(HOBJ)

      MX_MORS_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MX_MORS_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_REQHBASE
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmors.fc'
C------------------------------------------------------------------------

      MX_MORS_REQHBASE = MX_MORS_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MX_MORS_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_HVALIDE
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      MX_MORS_HVALIDE = OB_OBJN_HVALIDE(HOBJ, MX_MORS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     NDLE     : Nombre de degrés de libertés dans l'élément
C     KLOCE    : Table de localisation élémentaire (lie num éqn au
C                degrés de liberté).
C     VDLE     : Vecteur des dl élémentaires (valeurs imposées)
C     VKE      : Matrice élémentaire (à assembler dans la grande)
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_ASMKE(HOBJ, NDLE, KLOCE, VKE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_ASMKE
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLE
      INTEGER KLOCE(*)
      REAL*8  VKE  (*)

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'mxmors.fc'

      INTEGER, PARAMETER :: IOFF = 0

      INTEGER IERR
      INTEGER NEQL
      INTEGER, POINTER :: KIAP(:)
      INTEGER, POINTER :: KJAP(:)
      REAL*8,  POINTER :: VKG (:)
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      NEQL =  SELF%NEQL
      KIAP => SELF%LIAP%CST2K()
      KJAP => SELF%LJAP%CST2K()
      VKG  => SELF%LKG %CST2V()

C---     Assemblage dans la matrice
      IF (NDLE .GT. 0) THEN
         IERR = SP_MORS_ASMKE(NDLE,
     &                        KLOCE,
     &                        VKE,
     &                        NEQL,
     &                        KIAP(:),
     &                        KJAP(:),
     &                        VKG (:),
     &                        IOFF)
      ELSE
         IERR = SP_MORS_ASMFE(-NDLE,
     &                        KLOCE(1),
     &                        KLOCE(2),
     &                        VKE,
     &                        NEQL,
     &                        KIAP(:),
     &                        KJAP(:),
     &                        VKG (:),
     &                        IOFF)
      ENDIF
     
      MX_MORS_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dimensionne et initialise la matrice.
C
C Description:
C     La méthode MX_MORS_DIMMAT dimensionne les indices.
C     La méthode doit être appelée avant tout calcul sur la matrice afin de
C     l'initialiser.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_DIMIND(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_DIMIND
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      INTEGER NEQL
      INTEGER HHSH

      LOGICAL DODIM
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.algo.matrix.morse'

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)

C---     Paramètres de la simulation
      NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
      HHSH = LM_HELE_REQHASH(HSIM)
      IF (NEQL .LE. 0) GOTO 9900

C---     Assemble les indices
      DODIM = (HHSH .NE. SELF%HHSH)
D     IF (ERR_GOOD() .AND. DODIM) THEN
D        WRITE(LOG_BUF, *) 'Resizing from', SELF%NEQL, ' to ', NEQL
D        CALL LOG_WRN(LOG_ZNE, ' ')
D        CALL LOG_WRN(LOG_ZNE, '-------------------------------')
D        CALL LOG_WRN(LOG_ZNE, 'WARNING: Matrix resizing')
D        CALL LOG_WRN(LOG_ZNE, LOG_BUF)
D        CALL LOG_WRN(LOG_ZNE, '-------------------------------')
D      ENDIF
      IF (ERR_GOOD() .AND. DODIM) IERR = MX_MORS_ASMDIM(HOBJ, HSIM)
      IF (ERR_GOOD() .AND. DODIM) IERR = MX_MORS_ASMIND(HOBJ, HSIM)

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         SELF%HSIM = HSIM
         SELF%HHSH = HHSH
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_MATRICE_DIM_NUL'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MX_MORS_DIMIND = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dimensionne et initialise la matrice.
C
C Description:
C     La méthode MX_MORS_DIMMAT dimensionne la matrice. Elle fait les appels
C     pour assembler les dimensions et les indices. Elle initialise l'espace
C     de la matrice.
C     La méthode doit être appelée avant tout calcul sur la matrice afin de
C     l'initialiser.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_DIMMAT(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_DIMMAT
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      INTEGER LKG
      INTEGER NEQL, NKGP
      INTEGER HHSH

      LOGICAL DODIM, DOINIT
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.algo.matrix.morse'

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)

C---     Paramètres de la simulation
      NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
      HHSH = LM_HELE_REQHASH(HSIM)
      IF (NEQL .LE. 0) GOTO 9900

C---     Assemble les indices
      DODIM = (HHSH .NE. SELF%HHSH)
D     IF (ERR_GOOD() .AND. DODIM) THEN
D        WRITE(LOG_BUF, *) 'Resizing from', SELF%NEQL, ' to ', NEQL
D        CALL LOG_DBG(LOG_ZNE, ' ')
D        CALL LOG_DBG(LOG_ZNE, '-------------------------------')
D        CALL LOG_DBG(LOG_ZNE, 'WARNING: Matrix resizing')
D        CALL LOG_DBG(LOG_ZNE, LOG_BUF)
D        CALL LOG_DBG(LOG_ZNE, '-------------------------------')
D     ENDIF
      IF (ERR_GOOD() .AND. DODIM) IERR = MX_MORS_ASMDIM(HOBJ, HSIM)
      IF (ERR_GOOD() .AND. DODIM) IERR = MX_MORS_ASMIND(HOBJ, HSIM)

C---     Récupère les attributs
      IF (ERR_GOOD()) THEN
         NEQL = SELF%NEQL
         NKGP = SELF%NKGP
         LKG  = SELF%LKG %REQHNDL()
      ENDIF
      DOINIT = SO_ALLC_HEXIST(LKG)

C---     (Re)dimensionne la matrice
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NKGP, LKG)

C---     Initialise la matrice au besoin
      IF (ERR_GOOD() .AND. DOINIT) THEN
         CALL DINIT(NKGP,  0.0D0, VA(SO_ALLC_REQVIND(VA, LKG)), 1)
      ENDIF

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         SELF%HSIM = HSIM
         SELF%HHSH = HHSH
         SELF%LKG  = SO_ALLC_PTR(LKG)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_MATRICE_DIM_NUL'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MX_MORS_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     DIMENSIONNE
C     CALCUL DES POINTEURS DU STOCKAGE MORSE DU TYPE
C     COMPRESSION DE LIGNE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_LISMAT(HOBJ, FNAME)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_LISMAT
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spmors.fi'

      INTEGER IERR
      INTEGER NEQL, NKGP
      INTEGER LIA, LJA, LKG
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)

C---     Lis la matrice
      IF (ERR_GOOD()) THEN
         IERR = SP_MORS_LISMAT(FNAME,
     &                         NEQL,
     &                         NKGP,
     &                         LIA,
     &                         LJA,
     &                         LKG)
      ENDIF

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         SELF%NEQL = NEQL
         SELF%NKGP = NKGP
         SELF%LIAP = SO_ALLC_PTR(LIA)
         SELF%LJAP = SO_ALLC_PTR(LJA)
         SELF%LKG  = SO_ALLC_PTR(LKG)
      ENDIF

      MX_MORS_LISMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Valeur min sur la diagonale
C
C Description:
C     La méthode MX_MORS_DIAGMIN assure un valeur min sur la diagonale.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VMIN        Valeur min
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_DIAGMIN(HOBJ, VMIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_DIAGMIN
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VMIN

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spmors.fi'

      INTEGER IERR
      INTEGER NEQL
      INTEGER, POINTER :: KJDP(:)
      REAL*8,  POINTER :: VKG (:)
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
D     CALL ERR_PRE(VMIN .GE. 0.0D0)
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      NEQL =  SELF%NEQL
      KJDP => SELF%LJDP%CST2K()
      VKG  => SELF%LKG %CST2V()

C---     Valeur min sur la diagonale
      IERR = SP_MORS_DMIN(NEQL,
     &                    KJDP(:),
     &                    VKG (:),
     &                    VMIN)

      MX_MORS_DIAGMIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Addition de matrice
C
C Description:
C     La méthode MX_MORS_ADDMAT additionne à l'objet courant la matrice
C     HMAT.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HMAT        Handle sur la matrice à additionner
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_ADDMAT(HOBJ, HMAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_ADDMAT
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HMAT

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NKGP, NEQL
      INTEGER LJDP
      INTEGER LKGx, LKGy

      REAL*8 UN
      PARAMETER (UN = 1.0D0)
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
D     CALL ERR_PRE(HOBJ .NE. HMAT)
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      NKGP = MX_MORS_REQNKGP(HOBJ)
      NEQL = MX_MORS_REQNEQL(HOBJ)
      LKGy = MX_MORS_REQLKG (HOBJ)

C---     Matrice morse
      IF (MX_MORS_HVALIDE(HMAT)) THEN
D        CALL ERR_ASR(NKGP .EQ. MX_MORS_REQNKGP(HMAT))
D        CALL ERR_ASR(NEQL .EQ. MX_MORS_REQNEQL(HMAT))
         LKGx = MX_MORS_REQLKG (HMAT)
         CALL DAXPY(NKGP, UN,
     &              VA(SO_ALLC_REQVIND(VA,LKGx)), 1,
     &              VA(SO_ALLC_REQVIND(VA,LKGy)), 1)

C---     Matrice diagonale
      ELSEIF (MX_DIAG_HVALIDE(HMAT)) THEN
D        CALL ERR_ASR(NEQL .EQ. MX_DIAG_REQNEQL(HMAT))
         LKGx = MX_DIAG_REQLKG (HMAT)
         LJDP = MX_MORS_REQLJDP(HOBJ)
         IERR = SP_MORS_ADDD(NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LJDP)),
     &                       VA(SO_ALLC_REQVIND(VA,LKGy)),
     &                       VA(SO_ALLC_REQVIND(VA,LKGx)))

D     ELSE
D        CALL ERR_ASR(.FALSE.)
      ENDIF

      MX_MORS_ADDMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Multiplication par un scalaire.
C
C Description:
C     La méthode MX_MORS_MULVAL multiplie l'objet courant par le scalaire
C     A.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     A           Facteur multiplicatif
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_MULVAL(HOBJ, A)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_MULVAL
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  A

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      INTEGER NEQL
      INTEGER, POINTER :: KIAP(:)
      INTEGER, POINTER :: KJAP(:)
      REAL*8,  POINTER :: VKG (:)
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      NEQL =  SELF%NEQL
      KIAP => SELF%LIAP%CST2K()
      KJAP => SELF%LJAP%CST2K()
      VKG  => SELF%LKG %CST2V()

C---     L'opération
      IERR = SP_MORS_SCAL(NEQL,
     &                    KIAP(:),
     &                    KJAP(:),
     &                    VKG (:),
     &                    A)

      MX_MORS_MULVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Multiplication par un vecteur
C
C Description:
C     La méthode MX_MORS_MULVEC fait l'opération {R} = [M]{V}.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     N           Dimension des vecteurs
C     V           Vecteur à multiplier
C
C Sortie:
C     R           Vecteur résultat
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_MULVEC(HOBJ, N, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_MULVEC
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)
      REAL*8  R(*)

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      INTEGER NEQL
      INTEGER, POINTER :: KIAP(:)
      INTEGER, POINTER :: KJAP(:)
      REAL*8,  POINTER :: VKG (:)
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      NEQL =  SELF%NEQL
      KIAP => SELF%LIAP%CST2K()
      KJAP => SELF%LJAP%CST2K()
      VKG  => SELF%LKG %CST2V()

C---     L'opération
      IERR = SP_MORS_MULV(NEQL,
     &                    KIAP(:),
     &                    KJAP(:),
     &                    VKG (:),
     &                    V,
     &                    R)

      MX_MORS_MULVEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble les dimensions de la matrice
C
C Description:
C     La fonction privée MX_MORS_ASMDIM assemble les dimensions de la
C     matrice.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_ASMDIM(HOBJ, HSIM)

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      INTEGER ILU, ICRC
      INTEGER NEQL
      INTEGER NKGP, NKGSP
      INTEGER LIAP
      INTEGER L_KLD
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      ILU  = SELF%ILU
      LIAP = SELF%LIAP%REQHNDL()

C---     Paramètres de la simulation
      NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
      ICRC = LM_HELE_REQHASH(HSIM)
D     CALL ERR_ASR(NEQL .GT. 0)

C---     ALLOCATION D'ESPACE
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL+1, LIAP)
      L_KLD=0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL+1, L_KLD)

C---     ASSEMBLE LES DIMENSIONS
      IF (ERR_GOOD())
     &   IERR=LM_HELE_ASMDIM(HSIM,
     &                       ILU,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP):),
     &                       KA(SO_ALLC_REQKIND(KA,L_KLD):))

C---     AJOUTE POUR ILU(n)
      IF (ERR_GOOD() .AND. ILU .GT. 0)
     &   IERR=SP_MORS_DIMILUN(ILU,
     &                        NEQL,
     &                        KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                        KA(SO_ALLC_REQKIND(KA,L_KLD)))

C---     CUMULE LES TABLES
      IF (ERR_GOOD())
     &   IERR=SP_MORS_CMLTBL(NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,L_KLD)))

C---     CALCULE LES DIMENSIONS GLOBALES
      IF (ERR_GOOD())
     &   IERR=MX_MORS_CLCDIM(ILU,
     &                       NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       NKGSP,
     &                       NKGP)

C---     Récupère l'espace
      IF (L_KLD .NE. 0) IERR = SO_ALLC_ALLINT(0, L_KLD)

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         SELF%HSIM = HSIM
         SELF%HHSH = ICRC
         SELF%NEQL = NEQL
         SELF%NKGP = NKGP
         SELF%NKGSP= NKGSP
         SELF%LIAP = SO_ALLC_PTR(LIAP)
      ENDIF

      MX_MORS_ASMDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée MX_MORS_CLCDIM calcule les dimensions globales
C     de la matrice. Elle permet de résoudre l'appel à IA
C
C Entrée:
C     INTEGER ILU
C     INTEGER NEQ
C     INTEGER IA (*)
C
C Sortie:
C     INTEGER NKGSP
C     INTEGER NKGP
C     INTEGER NCIEL
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_CLCDIM(ILU, NEQ, IA, NKGSP, NKGP)

      IMPLICIT NONE

      INTEGER MX_MORS_CLCDIM
      INTEGER ILU
      INTEGER NEQ
      INTEGER IA (*)
      INTEGER NKGSP
      INTEGER NKGP

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      IF (ILU .EQ. 0) THEN
         NKGP  = IA(NEQ+1) - IA(1)
         NKGSP = (NKGP - NEQ) / 2
      ELSE
         NKGSP = IA(NEQ+1) - IA(1)
         NKGP  = 2*NKGSP + NEQ
      ENDIF
      NKGSP = MAX(1, NKGSP)

      MX_MORS_CLCDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée MX_MORS_ASMIND assemble les indices des pointeurs
C     pour le stockage par compression de ligne.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_ASMIND(HOBJ, HSIM)

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      INTEGER ILU
      INTEGER NEQL
      INTEGER NKGP
      INTEGER NKGSP
      INTEGER LIAP
      INTEGER LJDP
      INTEGER LJAP
      INTEGER L_IAC
      INTEGER L_JAC
      INTEGER L_JR
      LOGICAL DOINIT, ESTCCS
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      ILU  = SELF%ILU
      NEQL = SELF%NEQL
      NKGP = SELF%NKGP
      NKGSP= SELF%NKGSP
      LIAP = SELF%LIAP%REQHNDL()
      LJDP = SELF%LJDP%REQHNDL()
      LJAP = SELF%LJAP%REQHNDL()
D     CALL ERR_ASR(LIAP .NE. 0)
D     CALL ERR_ASR((LJDP .EQ. 0 .AND. LJAP .EQ. 0) .OR.
D    &             (LJDP .NE. 0 .AND. LJAP .NE. 0))

C---     États
      DOINIT = SO_ALLC_HEXIST(LJDP)    ! Tables existent?
      ESTCCS = (ILU .NE. 0)

C---     (Ré)allocation d'espace
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL, LJDP)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NKGP, LJAP)
      L_IAC = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL+1, L_IAC)
      L_JAC = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NKGSP, L_JAC)
      L_JR = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL, L_JR)

C---     Initialise la matrice au besoin
      IF (ERR_GOOD() .AND. DOINIT) THEN
         CALL IINIT(NEQL, 0, KA(SO_ALLC_REQKIND(KA,LJDP)), 1)
      ENDIF
      IF (ERR_GOOD() .AND. DOINIT) THEN
         CALL IINIT(NKGP, 0, KA(SO_ALLC_REQKIND(KA,LJAP)), 1)
      ENDIF

C---     Assemble les indices
      IF (ERR_GOOD())
     &   IERR=LM_HELE_ASMIND(HSIM,
     &                       ILU,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP):),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP):))

C---     Trie les indices de lignes par ordre croissant et remplis
      IF (ERR_GOOD())
     &   IERR=SP_MORS_TRIIND(ESTCCS,
     &                       ILU,
     &                       NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,L_JR)),
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       KA(SO_ALLC_REQKIND(KA,L_IAC)),
     &                       KA(SO_ALLC_REQKIND(KA,L_JAC)))

C---     Rotation du type de stockage creux de colonne à ligne
      IF (ERR_GOOD() .AND. ESTCCS)
     &   IERR=SP_MORS_CCSCRS(NEQL,
     &                       NKGP,
     &                       KA(SO_ALLC_REQKIND(KA,L_JR)),
     &                       KA(SO_ALLC_REQKIND(KA,L_IAC)),
     &                       KA(SO_ALLC_REQKIND(KA,L_JAC)),
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)))

C---     Extrait les indices de la diagonale
      IF (ERR_GOOD())
     &   IERR=SP_MORS_XTRJD (NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJDP)))
      IF (.NOT. ESTCCS) THEN  ! Une matrice CRS peut ne pas être carrée
         IF (ERR_BAD() .AND. ERR_ESTMSG('ERR_TERME_DIAGONAL_MANQUANT'))
     &      CALL ERR_RESET()
      ENDIF

C---     Récupère l'espace
      IF (L_JR  .NE. 0) IERR = SO_ALLC_ALLINT(0, L_JR)
      IF (L_JAC .NE. 0) IERR = SO_ALLC_ALLINT(0, L_JAC)
      IF (L_IAC .NE. 0) IERR = SO_ALLC_ALLINT(0, L_IAC)

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         SELF%LJAP = SO_ALLC_PTR(LJAP)
         SELF%LJDP = SO_ALLC_PTR(LJDP)
      ENDIF

      MX_MORS_ASMIND = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQCRC32(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_REQCRC32
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmors.fc'

      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => MX_MORS_REQSELF(HOBJ)
      MX_MORS_REQCRC32 = SELF%HHSH
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQILU(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_REQILU
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmors.fc'

      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => MX_MORS_REQSELF(HOBJ)
      MX_MORS_REQILU = SELF%ILU
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQNEQL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_REQNEQL
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmors.fc'

      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => MX_MORS_REQSELF(HOBJ)
      MX_MORS_REQNEQL = SELF%NEQL
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQNKGP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_REQNKGP
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmors.fc'

      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => MX_MORS_REQSELF(HOBJ)
      MX_MORS_REQNKGP = SELF%NKGP
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQLIAP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_REQLIAP
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmors.fc'
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => MX_MORS_REQSELF(HOBJ)
      MX_MORS_REQLIAP = SELF%LIAP%REQHNDL()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQLJAP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_REQLJAP
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmors.fc'
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => MX_MORS_REQSELF(HOBJ)
      MX_MORS_REQLJAP = SELF%LJAP%REQHNDL()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQLJDP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_REQLJDP
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmors.fc'
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => MX_MORS_REQSELF(HOBJ)
      MX_MORS_REQLJDP = SELF%LJDP%REQHNDL()
      RETURN
      END

C************************************************************************
C Sommaire: Pointeur à la  table VKG
C
C Description:
C     La méthode MX_MORS_REQLKG retourne le pointeur à la table VKG
C     des valeurs de la matrice.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQLKG(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_REQLKG
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmors.fc'
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => MX_MORS_REQSELF(HOBJ)
      MX_MORS_REQLKG = SELF%LKG%REQHNDL()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MORS_DMPMAT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MORS_DMPMAT
CDEC$ ENDIF

      USE MX_MORS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'mxmors.fc'

      INTEGER IERR
      INTEGER NEQL
      INTEGER, POINTER :: KIAP(:)
      INTEGER, POINTER :: KJAP(:)
      REAL*8,  POINTER :: VKG (:)
      TYPE (MX_MORS_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MX_MORS_REQSELF(HOBJ)
      NEQL =  SELF%NEQL
      KIAP => SELF%LIAP%CST2K()
      KJAP => SELF%LJAP%CST2K()
      VKG  => SELF%LKG %CST2V()

C---     L'opération
      IERR = SP_MORS_DMPMAT(NEQL,
     &                      KIAP(:),
     &                      KJAP(:),
     &                      VKG (:))

      MX_MORS_DMPMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
CC Sommaire: Construit une sous-matrice
CC
CC Description:
CC
CC Entrée:
CC
CC Sortie:
CC
CC Notes:
CC
C************************************************************************
C      FUNCTION MX_MORS_CTRSMX(HOBJ, HSMX)
CC$pragma aux MX_MORS_CTRSMX export
CCMS$ ATTRIBUTES DLLEXPORT :: MX_MORS_CTRSMX
C
C      IMPLICIT NONE
C
C      INTEGER HOBJ
C      INTEGER HSMX
C
C      INCLUDE 'mxmors.fi'
C      INCLUDE 'err.fi'
C      INCLUDE 'spmors.fi'
C      INCLUDE 'soallc.fi'
C      INCLUDE 'mxmors.fc'
C
C      INTEGER IOB
C      INTEGER ISM
C      INTEGER IERR
C      INTEGER LKG
C      INTEGER NKGP
CC------------------------------------------------------------------------
CD     CALL ERR_PRE(MX_MORS_HVALIDE(HOBJ))
CC------------------------------------------------------------------------
C
CC---     CONSTRUIS LA SOUS-MATRICE
C      IERR = MX_MORS_CTR(HSMX)
C      IERR = MX_MORS_INI(HSMX,
C     &                   MX_MORS_ILU (IOB))
C
CC---     RECUPERE LES ATTRIBUTS
C      IOB = HOBJ - MX_MORS_HBASE
C      ISM = HSMX - MX_MORS_HBASE
C
CC---     ASSIGNE LES VALEURS DU PARENT
C      MX_MORS_ILUM (ISM) = MX_MORS_ILUM (IOB)
C      MX_MORS_NDLT (ISM) = MX_MORS_NDLT (IOB)
C      MX_MORS_NKGP (ISM) = MX_MORS_NKGP (IOB)
C      MX_MORS_NKGSP(ISM) = MX_MORS_NKGSP(IOB)
C      MX_MORS_LIAP (ISM) = MX_MORS_LIAP (IOB)
C      MX_MORS_LJDP (ISM) = MX_MORS_LJDP (IOB)
C      MX_MORS_LJAP (ISM) = MX_MORS_LJAP (IOB)
C      MX_MORS_LJTR (ISM) = MX_MORS_LJTR (IOB)
C      MX_MORS_SMTX (ISM) = .TRUE.
C
CC---     ALLOUE LA MEMOIRE
C      NKGP = MX_MORS_NKGP(ISM)
C      LKG  = 0
C      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NKGP, LKG)
C      IF (ERR_GOOD()) MX_MORS_LKG(ISM) = LKG
C
C      MX_MORS_CTRSMX = ERR_TYP()
C      RETURN
C      END
C
C************************************************************************
CC Sommaire: Initialise une sous-matrice
CC
CC Description:
CC
CC Entrée:
CC
CC Sortie:
CC
CC Notes:
CC
C************************************************************************
C      FUNCTION MX_MORS_INISM(HOBJ, HPAR, DLLMIN, DLLMAX)
CC$pragma aux MX_MORS_INISM export
CCMS$ ATTRIBUTES DLLEXPORT :: MX_MORS_INISM
C
C      IMPLICIT NONE
C
C      INTEGER HOBJ
C      INTEGER HPAR
C      INTEGER DLLMIN
C      INTEGER DLLMAX
C
C      INCLUDE 'mxmors.fi'
C      INCLUDE 'err.fi'
C      INCLUDE 'mxmors.fc'
C
CC      INTEGER IERR
CC      INTEGER IOB
CC------------------------------------------------------------------------
CD      CALL ERR_ASR(MX_MORS_HVALIDE(HPAR))
CC------------------------------------------------------------------------
C
C      MX_MORS_INISM = ERR_TYP()
C      RETURN
C      END
