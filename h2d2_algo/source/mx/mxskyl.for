C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Note:
C     Avec MX_MORS_, il faudrait partager la base commune
C
C Functions:
C   Public:
C     INTEGER MX_SKYL_000
C     INTEGER MX_SKYL_999
C     INTEGER MX_SKYL_CTR
C     INTEGER MX_SKYL_DTR
C     INTEGER MX_SKYL_INI
C     INTEGER MX_SKYL_RST
C     INTEGER MX_SKYL_REQHBASE
C     LOGICAL MX_SKYL_HVALIDE
C     INTEGER MX_SKYL_ASMKE
C     INTEGER MX_SKYL_DIMMAT
C     INTEGER MX_SKYL_ADDMAT
C     INTEGER MX_SKYL_MULVAL
C     INTEGER MX_SKYL_MULVEC
C     INTEGER MX_SKYL_REQCRC32
C     INTEGER MX_SKYL_REQNEQL
C     INTEGER MX_SKYL_REQNKGP
C     INTEGER MX_SKYL_REQLIAP
C     INTEGER MX_SKYL_REQLJAP
C     INTEGER MX_SKYL_REQLJDP
C     INTEGER MX_SKYL_REQLKGS
C     INTEGER MX_SKYL_REQLKGD
C     INTEGER MX_SKYL_REQLKGI
C   Private:
C     INTEGER MX_SKYL_ASMDIM
C     INTEGER MX_SKYL_CLCDIM
C     INTEGER MX_SKYL_ASMIND
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MX_SKYL_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxskyl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MX_SKYL_NOBJMAX,
     &                   MX_SKYL_HBASE,
     &                   'Matrix Sky-Line')

      MX_SKYL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxskyl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER  IERR
      EXTERNAL MX_SKYL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MX_SKYL_NOBJMAX,
     &                   MX_SKYL_HBASE,
     &                   MX_SKYL_DTR)

      MX_SKYL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_SKYL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MX_SKYL_NOBJMAX,
     &                   MX_SKYL_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MX_SKYL_HVALIDE(HOBJ))
         IOB = HOBJ - MX_SKYL_HBASE

         MX_SKYL_ILU  (IOB) = 0
         MX_SKYL_HSIM (IOB) = 0
         MX_SKYL_ICRC (IOB) = 0
         MX_SKYL_NEQL (IOB) = 0
         MX_SKYL_NKGP (IOB) = 0
         MX_SKYL_NKGSP(IOB) = 0
         MX_SKYL_NCIEL(IOB) = 0
         MX_SKYL_LIAP (IOB) = 0
         MX_SKYL_LJDP (IOB) = 0
         MX_SKYL_LJAP (IOB) = 0
         MX_SKYL_LKGS (IOB) = 0
         MX_SKYL_LKGD (IOB) = 0
         MX_SKYL_LKGI (IOB) = 0
      ENDIF

      MX_SKYL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_SKYL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MX_SKYL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MX_SKYL_NOBJMAX,
     &                   MX_SKYL_HBASE)

      MX_SKYL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_SKYL_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MX_SKYL_RST(HOBJ)

      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - MX_SKYL_HBASE
         MX_SKYL_ILU (IOB) = -1
      ENDIF

      MX_SKYL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_SKYL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LIAP, LJDP, LJAP, LKGD, LKGS, LKGI
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_SKYL_HBASE

C---     RECUPERE LES ATTRIBUTS
      LIAP = MX_SKYL_LIAP(IOB)
      LJDP = MX_SKYL_LJDP(IOB)
      LJAP = MX_SKYL_LJAP(IOB)
      LKGS = MX_SKYL_LKGS(IOB)
      LKGD = MX_SKYL_LKGD(IOB)
      LKGI = MX_SKYL_LKGI(IOB)

C---     RECUPERE LA MEMOIRE
      IF (LIAP .NE. 0) IERR = SO_ALLC_ALLINT(0, LIAP)
      IF (LJDP .NE. 0) IERR = SO_ALLC_ALLINT(0, LJDP)
      IF (LJAP .NE. 0) IERR = SO_ALLC_ALLINT(0, LJAP)
      IF (LKGS .NE. 0) IERR = SO_ALLC_ALLRE8(0, LKGS)
      IF (LKGD .NE. 0) IERR = SO_ALLC_ALLRE8(0, LKGD)
      IF (LKGI .NE. 0) IERR = SO_ALLC_ALLRE8(0, LKGI)

C---     RESET
      MX_SKYL_ILU  (IOB) = 0
      MX_SKYL_HSIM (IOB) = 0
      MX_SKYL_ICRC (IOB) = 0
      MX_SKYL_NEQL (IOB) = 0
      MX_SKYL_NKGP (IOB) = 0
      MX_SKYL_NKGSP(IOB) = 0
      MX_SKYL_NCIEL(IOB) = 0
      MX_SKYL_LIAP (IOB) = 0
      MX_SKYL_LJDP (IOB) = 0
      MX_SKYL_LJAP (IOB) = 0
      MX_SKYL_LKGS (IOB) = 0
      MX_SKYL_LKGD (IOB) = 0
      MX_SKYL_LKGI (IOB) = 0

      MX_SKYL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MX_SKYL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C------------------------------------------------------------------------

      MX_SKYL_REQHBASE = MX_SKYL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MX_SKYL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mxskyl.fc'
C------------------------------------------------------------------------

      MX_SKYL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MX_SKYL_NOBJMAX,
     &                                  MX_SKYL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     NDLE     : Nombre de degrés de libertés dans l'élément
C     KLOCE    : Table de localisation élémentaire (lie num éqn au
C                degrés de liberté).
C     VKE      : Matrice élémentaire (à assembler dans la grande)
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_SKYL_ASMKE(HOBJ, NDLE, KLOCE, VKE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_ASMKE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLE
      INTEGER KLOCE(*)
      REAL*8  VKE  (*)

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spskyl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LIAP, LKGD, LKGS, LKGI
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_SKYL_HBASE
      LIAP = MX_SKYL_LIAP(IOB)
      LKGS = MX_SKYL_LKGS(IOB)
      LKGD = MX_SKYL_LKGD(IOB)
      LKGI = MX_SKYL_LKGI(IOB)

C---        ASSEMBLE LA MATRICE
      IERR = SP_SKYL_ASMKE(NDLE,
     &                     KLOCE,
     &                     VKE,
     &                     KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                     VA(SO_ALLC_REQVIND(VA,LKGS)),
     &                     VA(SO_ALLC_REQVIND(VA,LKGD)),
     &                     VA(SO_ALLC_REQVIND(VA,LKGI)))

      MX_SKYL_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dimensionne et initialise la matrice.
C
C Description:
C     La méthode MX_SKYL_DIMMAT dimensionne la matrice. Elle fait les appels
C     pour assembler les dimensions et les indices. Elle initialise l'espace
C     de la matrice.
C     La méthode doit être appelée avant tout calcul sur la matrice afin de
C     l'initialiser.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_DIMMAT(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_DIMMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ICRC
      INTEGER LKGD, LKGS, LKGI
      INTEGER NEQL, NKGSP
      LOGICAL DODIM, DOINIT
C----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - MX_SKYL_HBASE

C---     Paramètres de la simulation
      NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
      ICRC = LM_HELE_REQHASH(HSIM)
      IF (NEQL .LE. 0) GOTO 9900

C---     Assemble les indices
      DODIM = (ICRC .NE. MX_SKYL_ICRC(IOB))
      IF (ERR_GOOD() .AND. DODIM) IERR = MX_SKYL_ASMDIM(HOBJ, HSIM)
      IF (ERR_GOOD() .AND. DODIM) IERR = MX_SKYL_ASMIND(HOBJ, HSIM)

C---     Récupère les attributs
      IF (ERR_GOOD()) THEN
         NEQL  = MX_SKYL_NEQL (IOB)
         NKGSP = MX_SKYL_NKGSP(IOB)
         LKGS  = MX_SKYL_LKGS (IOB)
         LKGD  = MX_SKYL_LKGD (IOB)
         LKGI  = MX_SKYL_LKGI (IOB)
      ENDIF
      DOINIT = SO_ALLC_HEXIST(LKGS)

C---     (Re)dimensionne la matrice
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NKGSP, LKGS)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NEQL,  LKGD)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NKGSP, LKGI)

C---     Initialise la matrice au besoin
      IF (ERR_GOOD() .AND. DOINIT) THEN
         CALL DINIT(NKGSP, 0.0D0, VA(SO_ALLC_REQVIND(VA,LKGS)), 1)
         CALL DINIT(NEQL,  0.0D0, VA(SO_ALLC_REQVIND(VA,LKGD)), 1)
         CALL DINIT(NKGSP, 0.0D0, VA(SO_ALLC_REQVIND(VA,LKGI)), 1)
      ENDIF

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         MX_SKYL_HSIM(IOB) = HSIM
         MX_SKYL_ICRC(IOB) = ICRC
         MX_SKYL_LKGS(IOB) = LKGS
         MX_SKYL_LKGD(IOB) = LKGD
         MX_SKYL_LKGI(IOB) = LKGI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_MATRICE_DIM_NUL'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MX_SKYL_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_SKYL_ADDMAT(HOBJ, HRHS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_ADDMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HRHS

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('MX_SKYL_ADDMAT not yet implemented')
      CALL ERR_ASR(.FALSE.)

      MX_SKYL_ADDMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Multiplication par un scalaire.
C
C Description:
C     La méthode MX_SKYL_MULVAL multiplie l'objet courant par le scalaire
C     A.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     A           Facteur multiplicatif
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_SKYL_MULVAL(HOBJ, A)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_MULVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  A

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NEQL
      INTEGER NKGSP
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - MX_SKYL_HBASE
      NEQL  = MX_SKYL_NEQL (IOB)
      NKGSP = MX_SKYL_NKGSP(IOB)

      CALL DSCAL(NKGSP,A,VA(SO_ALLC_REQVIND(VA,MX_SKYL_LKGS(IOB))),1)
      CALL DSCAL(NEQL, A,VA(SO_ALLC_REQVIND(VA,MX_SKYL_LKGD(IOB))),1)
      CALL DSCAL(NKGSP,A,VA(SO_ALLC_REQVIND(VA,MX_SKYL_LKGI(IOB))),1)

      MX_SKYL_MULVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Multiplication par un vecteur
C
C Description:
C     La méthode MX_SKYL_MULVEC fait l'opération {R} = [M]{V}.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     N           Dimension des vecteurs
C     V           Vecteur à multiplier
C
C Sortie:
C     R           Vecteur résultat
C
C Notes:
C
C************************************************************************
      FUNCTION MX_SKYL_MULVEC(HOBJ, N, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_MULVEC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)
      REAL*8  R(*)

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spskyl.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NEQL
      INTEGER LIAP, LJAP, LKGS, LKGD, LKGI
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - MX_SKYL_HBASE
      NEQL = MX_SKYL_NEQL(IOB)
      LKGS = MX_SKYL_LKGS(IOB)
      LKGD = MX_SKYL_LKGD(IOB)
      LKGI = MX_SKYL_LKGI(IOB)

C---     Multiplie
      IERR = SP_SKYL_MULV(NEQL,
     &                    KA(SO_ALLC_REQKIND(KA, LIAP)),
     &                    KA(SO_ALLC_REQKIND(KA, LJAP)),
     &                    VA(SO_ALLC_REQVIND(VA, LKGS)),
     &                    VA(SO_ALLC_REQVIND(VA, LKGD)),
     &                    VA(SO_ALLC_REQVIND(VA, LKGI)),
     &                    V,
     &                    R)

      MX_SKYL_MULVEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_SKYL_DMPMAT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_DMPMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spskyl.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LIAP
      INTEGER LKGS, LKGD, LKGI
      INTEGER NEQL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_SKYL_HBASE
      NEQL = MX_SKYL_NEQL(IOB)
      LIAP = MX_SKYL_LIAP(IOB)
      LKGS = MX_SKYL_LKGS(IOB)
      LKGD = MX_SKYL_LKGD(IOB)
      LKGI = MX_SKYL_LKGI(IOB)

      IERR = SP_SKYL_DMPMAT(NEQL,
     &                      KA(SO_ALLC_REQKIND(KA, LIAP)),
     &                      VA(SO_ALLC_REQVIND(VA, LKGS)),
     &                      VA(SO_ALLC_REQVIND(VA, LKGD)),
     &                      VA(SO_ALLC_REQVIND(VA, LKGI)))

      MX_SKYL_DMPMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble les dimensions de la matrice
C
C Description:
C     La fonction privée MX_SKYL_ASMDIM assemble les dimensions de la
C     matrice.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     A ILU=-1 près, c'est le même code que MX_MORS_ASMDIM !!!
C     Le contrôle pour savoir si la structure de la matrice n'a pas
C     changé est fragile.
C************************************************************************
      FUNCTION MX_SKYL_ASMDIM(HOBJ, HSIM)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ILU, ICRC
      INTEGER NEQL
      INTEGER NKGP
      INTEGER NKGSP
      INTEGER NCIEL
      INTEGER LIAP
      INTEGER L_KLD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - MX_SKYL_HBASE
      ILU  = MX_SKYL_ILU (IOB)
      LIAP = MX_SKYL_LIAP(IOB)

C---     Paramètres de la simulation
      NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
      ICRC = LM_HELE_REQHASH(HSIM)
D     CALL ERR_ASR(NEQL .GT. 0)

C---     ALLOCATION D'ESPACE
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL+1, LIAP)
      L_KLD=0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL+1, L_KLD)

C---     ASSEMBLE LES DIMENSIONS
      IF (ERR_GOOD())
     &   IERR=LM_HELE_ASMDIM(HSIM,
     &                       ILU,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP):),
     &                       KA(SO_ALLC_REQKIND(KA,L_KLD):))

C---     AJOUTE POUR ILU(n)
      IF (ERR_GOOD())
     &   IERR=SP_MORS_DIMILUN(ILU,
     &                        NEQL,
     &                        KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                        KA(SO_ALLC_REQKIND(KA,L_KLD)))

C---     CUMULE LES TABLES
      IF (ERR_GOOD())
     &   IERR=SP_MORS_CMLTBL(NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,L_KLD)))

C---     CALCULE LES DIMENSIONS GLOBALES
      IF (ERR_GOOD())
     &   IERR=MX_SKYL_CLCDIM(NKGSP,
     &                       NKGP,
     &                       NCIEL,
     &                       NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,L_KLD)))

C---     RECUPERE L'ESPACE
      IF (L_KLD .NE. 0) IERR = SO_ALLC_ALLINT(0, L_KLD)

C---     STOKE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         MX_SKYL_HSIM (IOB) = HSIM
         MX_SKYL_ICRC (IOB) = ICRC
         MX_SKYL_ILU  (IOB) = ILU
         MX_SKYL_NEQL (IOB) = NEQL
         MX_SKYL_NKGP (IOB) = MAX(1, NKGP)
         MX_SKYL_NKGSP(IOB) = MAX(1, NKGSP)
         MX_SKYL_NCIEL(IOB) = NCIEL
         MX_SKYL_LIAP (IOB) = LIAP
      ENDIF

      MX_SKYL_ASMDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée MX_SKYL_CLCDIM calcule les dimensions globales
C     de la matrice. Elle permet de résoudre l'appel à IA
C
C Entrée:
C     INTEGER ILU
C     INTEGER NEQ
C     INTEGER IA (*)
C     INTEGER KLD(*)
C
C Sortie:
C     INTEGER NKGSP
C     INTEGER NKGP
C     INTEGER NCIEL
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_CLCDIM(NKGSP, NKGP, NCIEL, NDLT, IA, KLD)

      IMPLICIT NONE

      INTEGER NKGSP
      INTEGER NKGP
      INTEGER NCIEL
      INTEGER NDLT
      INTEGER IA (*)
      INTEGER KLD(*)

      INCLUDE 'err.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------

      NKGSP = IA(NDLT+1) - IA(1)
      NKGP  = 2*NKGSP + NDLT
      NCIEL = 2*(KLD(NDLT+1)-KLD(1)) + NDLT

      MX_SKYL_CLCDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble les indices
C
C Description:
C     La fonction privée MX_SKYL_ASMIND assemble les indices des pointeurs
C     pour le stockage par ligne de ciel
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_ASMIND(HOBJ, HSIM)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spskyl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxskyl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NEQL
      INTEGER LIAP, LJDP, LJAP
      LOGICAL DOINIT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - MX_SKYL_HBASE
      NEQL = MX_SKYL_NEQL (IOB)
      LIAP = MX_SKYL_LIAP (IOB)
      LJDP = MX_SKYL_LJDP (IOB)
      LJAP = MX_SKYL_LJAP (IOB)
D     CALL ERR_ASR(LIAP .NE. 0)
D     CALL ERR_ASR((LJDP .EQ. 0 .AND. LJAP .EQ. 0) .OR.
D    &             (LJDP .NE. 0 .AND. LJAP .NE. 0))

C---     États
      DOINIT = SO_ALLC_HEXIST(LJDP)    ! Tables existent?

C---     (Ré)allocation d'espace
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL, LJDP)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL, LJAP)

C---     Initialise la matrice au besoin
      IF (ERR_GOOD() .AND. DOINIT) THEN
         CALL IINIT(NEQL, 0, KA(SO_ALLC_REQKIND(KA,LJDP)), 1)
      ENDIF
      IF (ERR_GOOD() .AND. DOINIT) THEN
         CALL IINIT(NEQL, 0, KA(SO_ALLC_REQKIND(KA,LJAP)), 1)
      ENDIF

C---     Construis les tables
      IF (ERR_GOOD())
     &   IERR = SP_SKYL_CLCIND(NEQL,
     &                         KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                         KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                         KA(SO_ALLC_REQKIND(KA,LJDP)))

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         MX_SKYL_LJAP (IOB) = LJAP
         MX_SKYL_LJDP (IOB) = LJDP
      ENDIF

      GOTO 9999
C----------------------------------------------------------------------

9999  CONTINUE
      MX_SKYL_ASMIND = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQCRC32(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQCRC32
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_SKYL_REQCRC32 = MX_SKYL_ICRC(HOBJ-MX_SKYL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQNEQL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQNEQL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_SKYL_NEQL(HOBJ-MX_SKYL_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_SKYL_REQNEQL = MX_SKYL_NEQL(HOBJ-MX_SKYL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQNKGP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQNKGP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_SKYL_NKGP(HOBJ-MX_SKYL_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_SKYL_REQNKGP = MX_SKYL_NKGP(HOBJ-MX_SKYL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQLIAP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQLIAP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_SKYL_LIAP(HOBJ-MX_SKYL_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_SKYL_REQLIAP = MX_SKYL_LIAP(HOBJ-MX_SKYL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQLJAP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQLJAP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_SKYL_LJAP(HOBJ-MX_SKYL_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_SKYL_REQLJAP = MX_SKYL_LJAP(HOBJ-MX_SKYL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQLJDP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQLJDP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_SKYL_LJDP(HOBJ-MX_SKYL_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_SKYL_REQLJDP = MX_SKYL_LJDP(HOBJ-MX_SKYL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQLKGS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQLKGS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_SKYL_LKGS(HOBJ-MX_SKYL_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_SKYL_REQLKGS = MX_SKYL_LKGS(HOBJ-MX_SKYL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQLKGD(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQLKGD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_SKYL_LKGD(HOBJ-MX_SKYL_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_SKYL_REQLKGD = MX_SKYL_LKGD(HOBJ-MX_SKYL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_SKYL_REQLKGI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_SKYL_REQLKGI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxskyl.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_SKYL_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_SKYL_LKGI(HOBJ-MX_SKYL_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_SKYL_REQLKGI = MX_SKYL_LKGI(HOBJ-MX_SKYL_HBASE)
      RETURN
      END

