C************************************************************************
C --- Copyright (c) INRS 2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  ALgorithme
C Objet:   UTILitaires
C Type:    Concret
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_UTIL
C         FTN (Sub)Module: AL_RESI_M
C            Public:
C            Private:
C               SUBROUTINE AL_RESI_CTR
C               SUBROUTINE AL_RESI_CTR_PRM
C               SUBROUTINE AL_RESI_DTR
C
C            FTN Type: AL_RESI_T
C               Public:
C                  INTEGER AL_RESI_INI
C                  INTEGER AL_RESI_RST
C                  INTEGER AL_RESI_CLCRES
C                  INTEGER AL_RESI_CLCNRM
C               Private:
C
C************************************************************************

      MODULE AL_RESI_M

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR1_T, SO_ALLC_VPTR1,
     &                     SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      !---------------------------------------------------------------------
      !  Interface de la fonction des ALGO de calcul du résidu
      !---------------------------------------------------------------------
      INTERFACE
         INTEGER FUNCTION FUNC_RES(HOBJ, VRES, VDLG, ADLG)
            INTEGER, INTENT(IN)    :: HOBJ
            REAL*8,  INTENT(INOUT) :: VRES(*)
            REAL*8,  INTENT(IN)    :: VDLG(*)
            LOGICAL, INTENT(IN)    :: ADLG
         END FUNCTION FUNC_RES
      END INTERFACE

      !---------------------------------------------------------------------
      !  Classe de calcul du résidu
      !---------------------------------------------------------------------
      TYPE :: AL_RESI_T
         PRIVATE
         PROCEDURE(FUNC_RES), POINTER, NOPASS :: F_RES => NULL()
         INTEGER :: HALG = 0
         TYPE(SO_ALLC_VPTR2_T) :: VTRV_P
         TYPE(SO_ALLC_VPTR1_T) :: VRES_P
      CONTAINS
         PROCEDURE, PRIVATE :: AL_RESI_CLCRES1
         PROCEDURE, PRIVATE :: AL_RESI_CLCRES2
         PROCEDURE, PRIVATE :: AL_RESI_CLCNRM1
         PROCEDURE, PRIVATE :: AL_RESI_CLCNRM2

         ! INITIAL :: AL_RESI_CTR
         FINAL   :: AL_RESI_DTR
         PROCEDURE, PUBLIC :: INI    => AL_RESI_INI
         PROCEDURE, PUBLIC :: RST    => AL_RESI_RST
         GENERIC,   PUBLIC :: CLCRES => AL_RESI_CLCRES1, AL_RESI_CLCRES2
         GENERIC,   PUBLIC :: CLCNRM => AL_RESI_CLCNRM1, AL_RESI_CLCNRM2
      END TYPE AL_RESI_T

      ! ---  Constructeur
      INTERFACE AL_RESI
          MODULE PROCEDURE AL_RESI_CTR
          MODULE PROCEDURE AL_RESI_CTR_0
          MODULE PROCEDURE AL_RESI_CTR_2
      END INTERFACE
      INTERFACE DEL
          MODULE PROCEDURE AL_RESI_DTR
      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire: Constructeur
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER 
     &FUNCTION AL_RESI_CTR(SELF)

      TYPE(AL_RESI_T), INTENT(INOUT)  :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      NULLIFY(SELF%F_RES)
      SELF%HALG  = 0
      SELF%VTRV_P = SO_ALLC_VPTR2()
      SELF%VRES_P = SO_ALLC_VPTR1()

      AL_RESI_CTR = ERR_TYP()
      RETURN
      END FUNCTION AL_RESI_CTR

C************************************************************************
C Sommaire: Constructeur
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_CTR_0() RESULT(SELF)

      TYPE(AL_RESI_T) :: SELF
C-----------------------------------------------------------------------

      NULLIFY(SELF%F_RES)
      SELF%HALG  = 0
      SELF%VTRV_P = SO_ALLC_VPTR2()
      SELF%VRES_P = SO_ALLC_VPTR1()

      RETURN
      END FUNCTION AL_RESI_CTR_0

C************************************************************************
C Sommaire: Constructeur
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_CTR_2(F_RES, HALG) RESULT(SELF)

      INTEGER  F_RES
      INTEGER  HALG
      EXTERNAL F_RES

      TYPE(AL_RESI_T) :: SELF
C-----------------------------------------------------------------------

      NULLIFY(SELF%F_RES)
      SELF%F_RES => F_RES
      SELF%HALG  = HALG
      SELF%VTRV_P = SO_ALLC_VPTR2()
      SELF%VRES_P = SO_ALLC_VPTR1()

      RETURN
      END FUNCTION AL_RESI_CTR_2

C************************************************************************
C Sommaire: Constructeur
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE AL_RESI_DTR(SELF)

      TYPE(AL_RESI_T), INTENT(INOUT) :: SELF
C-----------------------------------------------------------------------

      NULLIFY(SELF%F_RES)
      SELF%HALG  = 0
      SELF%VTRV_P = SO_ALLC_VPTR2()
      SELF%VRES_P = SO_ALLC_VPTR1()

      RETURN
      END SUBROUTINE AL_RESI_DTR

C************************************************************************
C Sommaire: Constructeur
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Ne vide pas les tables car elles sont allouées/réallouées si les 
C     dimensions changent.
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_INI(SELF, F_RES, HALG)

      CLASS(AL_RESI_T), INTENT(INOUT) :: SELF
      INTEGER  F_RES
      INTEGER  HALG
      EXTERNAL F_RES

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SELF%F_RES => F_RES
      SELF%HALG  = HALG
      ! SELF%VTRV_P = SO_ALLC_VPTR2() cf.note
      ! SELF%VRES_P = SO_ALLC_VPTR1()

      AL_RESI_INI = ERR_TYP()
      RETURN
      END FUNCTION AL_RESI_INI

C************************************************************************
C Sommaire: Constructeur
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_RST(SELF)

      CLASS(AL_RESI_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      NULLIFY(SELF%F_RES)
      SELF%HALG  = 0
      SELF%VTRV_P = SO_ALLC_VPTR2()
      SELF%VRES_P = SO_ALLC_VPTR1()

      AL_RESI_RST = ERR_TYP()
      RETURN
      END FUNCTION AL_RESI_RST

C************************************************************************
C Sommaire: Calcule le résidu.
C
C Description:
C     La fonction statique AL_RESI_CLCRES calcule la norme du résidu pour une
C     valeur de Theta.
C
C Entrée:
C     SELF        Objet courant
C     THETA
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_CLCRES1(SELF, VDLG, VRES)

      CLASS(AL_RESI_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN)    :: VDLG(:,:)
      REAL*8, INTENT(INOUT) :: VRES(:)

      INCLUDE 'err.fi'

      INTEGER IERR
      LOGICAL, PARAMETER :: ADLG = .TRUE.
C-----------------------------------------------------------------------

      IERR = SELF%F_RES(SELF%HALG, VRES(:), VDLG(:,:), ADLG)

      AL_RESI_CLCRES1 = ERR_TYP()
      RETURN
      END FUNCTION AL_RESI_CLCRES1

C************************************************************************
C Sommaire: Calcule le résidu.
C
C Description:
C     La fonction statique AL_RESI_CLCRES calcule la norme du résidu pour une
C     valeur de Theta.
C
C Entrée:
C     SELF        Objet courant
C     THETA
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_CLCRES2(SELF, THETA, VDLG, VDEL, VRES)

      CLASS(AL_RESI_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN)    :: THETA
      REAL*8, INTENT(IN)    :: VDLG(:,:)
      REAL*8, INTENT(IN)    :: VDEL(:,:)
      REAL*8, INTENT(INOUT) :: VRES(:)

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NDLN, NNL
      LOGICAL DODIM, DOSCL
      LOGICAL, PARAMETER :: ADLG = .TRUE.
      REAL*8, POINTER :: VTRV(:,:)
C-----------------------------------------------------------------------

      DOSCL = (THETA .NE. 0.0D0)

      ! ---  Alloue la table de travail
      IF (DOSCL) THEN
         DODIM = .FALSE.
         IF (ASSOCIATED(SELF%VTRV_P%VPTR)) THEN
            DODIM = ANY(SHAPE(SELF%VTRV_P%VPTR) .NE. SHAPE(VDLG))
         ELSE
            DODIM = .TRUE.
         ENDIF
         IF (DODIM) THEN
            NDLN = SIZE(VDLG, 1)
            NNL  = SIZE(VDLG, 2)
            SELF%VTRV_P = SO_ALLC_VPTR2(NDLN, NNL)
         ENDIF
         VTRV => SELF%VTRV_P%VPTR
      ENDIF

      ! ---  Assemble K(U_0 + Theta*dU) dans VRES
      IF (DOSCL) THEN
         VTRV(:,:) = VDLG(:,:) + THETA*VDEL(:,:)
         IERR = SELF%CLCRES(VTRV(:,:), VRES(:))
      ELSE
         IERR = SELF%F_RES(SELF%HALG, VRES(:), VDLG(:,:), ADLG)
      ENDIF

      ! ---  Passe de NEQ à NDLL
!!!            IF (ERR_GOOD()) THEN
!!!               CALL DINIT(NDLL, 0.0D0, VNEW, 1)
!!!               IERR = SP_ELEM_NEQ2NDLT(NEQL,
!!!     &                                 NDLL,
!!!     &                                 KA(SO_ALLC_REQKIND(KA, LLOCN)),
!!!     &                                 VSOL,
!!!     &                                 VNEW)

      AL_RESI_CLCRES2 = ERR_TYP()
      RETURN
      END FUNCTION AL_RESI_CLCRES2

C************************************************************************
C Sommaire: Calcule la norme du résidu.
C
C Description:
C     La fonction statique AL_RESI_CLCNRM calcule la norme du résidu
C     pour une valeur de Theta.
C
C Entrée:
C     SELF        Objet courant
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C
C Sortie:
C     Po          Norme du résidu
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_CLCNRM1(SELF, VDLG, VNRM)

      CLASS(AL_RESI_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN)  :: VDLG(:,:)
      REAL*8, INTENT(OUT) :: VNRM

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_ERROR

      REAL*8  DN
      INTEGER IERR
      INTEGER NDLN, NNL
      LOGICAL DODIM
      REAL*8, POINTER :: VRES(:)
C-----------------------------------------------------------------------

      ! ---  Alloue les tables
      DODIM = .FALSE.
      IF (ASSOCIATED(SELF%VRES_P%VPTR)) THEN
         DODIM = ANY(SHAPE(SELF%VRES_P%VPTR) .NE. PRODUCT(SHAPE(VDLG)))
      ELSE
         DODIM = .TRUE.
      ENDIF
      IF (DODIM) THEN
         NDLN = SIZE(VDLG, 1)
         NNL  = SIZE(VDLG, 2)
         SELF%VRES_P = SO_ALLC_VPTR1(NDLN*NNL)
      ENDIF
      VRES => SELF%VRES_P%VPTR

      ! ---  Assemble dans VRES
      IERR = SELF%CLCRES(VDLG, VRES)

      ! ---  Calcule et réduis la norme
      IF (ERR_GOOD()) THEN
         DN = SUM( VRES(:)*VRES(:) )
         CALL MPI_ALLREDUCE(DN, VNRM, 1, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
         VNRM = SQRT(VNRM)
      ENDIF

      AL_RESI_CLCNRM1 = ERR_TYP()
      RETURN
      END FUNCTION AL_RESI_CLCNRM1

C************************************************************************
C Sommaire: Calcule la norme du résidu.
C
C Description:
C     La fonction statique AL_RESI_CLCNRM calcule la norme du résidu
C     pour une valeur de Theta.
C
C Entrée:
C     SELF        Objet courant
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C
C Sortie:
C     Po          Norme du résidu
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_CLCNRM2(SELF, THETA, VDLG, VDEL, VNRM)

      CLASS(AL_RESI_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN)  :: THETA
      REAL*8, INTENT(IN)  :: VDLG(:,:)
      REAL*8, INTENT(IN)  :: VDEL(:,:)
      REAL*8, INTENT(OUT) :: VNRM

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_ERROR

      REAL*8  DN
      INTEGER IERR
      INTEGER NDLN, NNL
      LOGICAL DODIM
      REAL*8, POINTER :: VRES(:)
C-----------------------------------------------------------------------

      ! ---  Alloue les tables
      DODIM = .FALSE.
      IF (ASSOCIATED(SELF%VRES_P%VPTR)) THEN
         DODIM = ANY(SHAPE(SELF%VRES_P%VPTR) .NE. PRODUCT(SHAPE(VDLG)))
      ELSE
         DODIM = .TRUE.
      ENDIF
      IF (DODIM) THEN
         NDLN = SIZE(VDLG, 1)
         NNL  = SIZE(VDLG, 2)
         SELF%VRES_P = SO_ALLC_VPTR1(NDLN*NNL)
      ENDIF
      VRES => SELF%VRES_P%VPTR

      ! ---  Assemble dans VRES
      IERR = SELF%CLCRES(THETA, VDLG, VDEL, VRES)

      ! ---  Calcule et réduis la norme
      IF (ERR_GOOD()) THEN
         DN = SUM( VRES(:)*VRES(:) )
         CALL MPI_ALLREDUCE(DN, VNRM, 1, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
         VNRM = SQRT(VNRM)
      ENDIF

      AL_RESI_CLCNRM2 = ERR_TYP()
      RETURN
      END FUNCTION AL_RESI_CLCNRM2

      END MODULE AL_RESI_M
