C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  ALgorithme
C Objet:   NeWToN
C Type:    Concret
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_NWTN
C         Public:
C            SUBROUTINE AL_NWTN_000
C            SUBROUTINE AL_NWTN_999
C            SUBROUTINE AL_NWTN_CTR
C            SUBROUTINE AL_NWTN_DTR
C            SUBROUTINE AL_NWTN_INI
C            SUBROUTINE AL_NWTN_RST
C            SUBROUTINE AL_NWTN_REQHBASE
C            SUBROUTINE AL_NWTN_HVALIDE
C            SUBROUTINE AL_NWTN_ASGNITER
C            SUBROUTINE AL_NWTN_REQNITER
C            SUBROUTINE AL_NWTN_REQHRES
C            SUBROUTINE AL_NWTN_RESOUS
C            SUBROUTINE AL_NWTN_RESOUS_E
C            SUBROUTINE AL_NWTN_REQSTATUS
C         Private:
C            SUBROUTINE AL_NWTN_RES1ITER
C            SUBROUTINE AL_NWTN_ASMKT
C            SUBROUTINE AL_NWTN_ASMRES
C            SUBROUTINE AL_NWTN_DUMMY
C
C************************************************************************

      MODULE AL_NWTN_PRIVATE_M

      IMPLICIT NONE
      
      CONTAINS
      
C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode privée AL_NWTN_RES1ITER(...) effectue une itération de l'algo
C     de Newton, soit une itération principale, soit une sous-itération.
C     En sortie:
C        VDLG contient la solution mise à jour,
C        VDEL contient l'incrément (NDLN)
C        VSOL, la solution (NEQL)
C        Le critère de convergence est calculé
C     VDEL et VDLG sont globalisées.
C
C Entrée:
C     INTEGER  HOBJ        Handle sur l'objet courant
C     INTEGER  HALG        Paramètre pour les fonction call-back
C     EXTERNAL F_KT        Fonction de calcul de [KT]
C     EXTERNAL F_KU        Fonction de calcul de [K].{U}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C     INTEGER  ITER        Itération courante
C     INTEGER  NITER       Nombre d'itérations (ou de sous-itérations)
C
C Sortie:
C     VDLG                 La solution mise à jour
C     VDEL                 L'incrément (NDLN)
C     VSOL                 Le résultat de la résolution (NEQL)
C
C Notes:
C     À partir du moment où on globalise une sous-iter,
C     on peut considérer qu'il n'y a plus convergence.
C************************************************************************
      INTEGER
     &FUNCTION AL_NWTN_RES1ITER(HOBJ,
     &                          HALG,
     &                          F_KT,
     &                          F_KU,
     &                          F_RES,
     &                          VSOL,
     &                          VDEL,
     &                          ITER,
     &                          NITER,
     &                          ISTTS)

      USE SO_ALLC_M, ONLY : SO_ALLC_PTR_T, SO_ALLC_PTR

      INTEGER, INTENT(IN)   :: HOBJ
      INTEGER, INTENT(IN)   :: HALG
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_RES
      REAL*8,  INTENT(INOUT):: VSOL(:)
      REAL*8,  INTENT(INOUT):: VDEL(:,:)
      INTEGER, INTENT(IN)   :: ITER
      INTEGER, INTENT(IN)   :: NITER
      INTEGER, INTENT(OUT)  :: ISTTS
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_RES

      INCLUDE 'alnwtn.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'alutil.fi'
      INCLUDE 'alresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'alnwtn.fc'

      REAL*8  ETA_K, VNRM
      INTEGER IERR
      INTEGER IOB
      INTEGER ITER_CC
      INTEGER HSIM, HRES
      INTEGER HGLB, HCCS, HCRA, HLTR, HRDU
      INTEGER HNMR
      INTEGER LDLG, LLOCN
      INTEGER NNL, NDLN, NDLL, NEQL
      LOGICAL ACCS, ACRA, AGLB
      LOGICAL ESTSITR, ESTRES, ESTGLB, ESTGLM, ESTRJT, ESTOSC
      
      TYPE(SO_ALLC_PTR_T) :: VDLG_P
      REAL*8, POINTER :: VDLG(:,:)
C------------------------------------------------------------------------
      INTEGER II
      LOGICAL IS_GL, IS_RS, IS_SS
      IS_GL(II) = BTEST(GL_GLBL_REQSTATUS(HGLB), II)
      IS_RS(II) = (CC_CRIC_REQRAISON(HCCS) .EQ. II)
      IS_SS(II) = (CC_CRIC_REQSTATUS(HCCS) .EQ. II)
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_NWTN_HBASE
      HCRA  = AL_NWTN_HCRA (IOB)
      HCCS  = AL_NWTN_HCCS (IOB)
      HGLB  = AL_NWTN_HGLB (IOB)
      HRDU  = AL_NWTN_HRDU (IOB)
      HSIM  = AL_NWTN_HSIM (IOB)
      HRES  = AL_NWTN_HRES (IOB)
D     CALL ERR_ASR(LM_ELEM_HVALIDE(HSIM))

C---     Récupère les données
      NNL   = LM_ELEM_REQPRM(HSIM, LM_GEOM_PRM_NNL)
      NDLN  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NEQL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)
      LLOCN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)
      HNMR  = LM_ELEM_REQHNUMC(HSIM)
      ESTSITR = ITER .GT. 1

C---     Pointeurs aux tables
      VDLG_P = SO_ALLC_PTR(LDLG)
      VDLG => VDLG_P%CST2V(NDLN, NNL)

C---     Présence/absence des composantes
      ACCS = CC_CRIC_HVALIDE(HCCS)
      ACRA = CA_CRIA_HVALIDE(HCRA)
      AGLB = GL_GLBL_HVALIDE(HGLB)

C---     Pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC(HSIM)

C---     Résous le système
      IF (ERR_GOOD()) THEN
         IF (.NOT. ESTSITR) THEN
            IERR = MR_RESO_XEQ(HRES,
     &                         HSIM,
     &                         HALG,
     &                         F_KT,
     &                         F_KU,
     &                         F_RES,
     &                         VSOL)
         ELSE
            IERR = MR_RESO_RESMTX(HRES,
     &                         HSIM,
     &                         HALG,
     &                         F_RES,
     &                         VSOL)
         ENDIF
      ENDIF

C---     Passe de NEQ à NDLL
      IF (ERR_GOOD()) THEN
         CALL DINIT(NDLL, 0.0D0, VDEL, 1)
         IERR = SP_ELEM_NEQ2NDLT(NEQL,
     &                           NDLL,
     &                           KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                           VSOL,
     &                           VDEL)
      ENDIF

C---     Synchronise les process
      IF (ERR_GOOD() .AND. NM_NUMR_CTRLH(HNMR)) THEN
         IERR = NM_NUMR_DSYNC(HNMR,
     &                        NDLN,
     &                        NNL,
     &                        VDEL)
      ENDIF

C---     Critère d'arrêt, avant globalisation
      ESTRES = .FALSE.
      IF (ERR_GOOD() .AND. ACRA) THEN
         IERR = CA_CRIA_CALCRI(HCRA,
     &                         NDLN,
     &                         NNL,
     &                         VDLG,
     &                         VDEL)
         ESTRES = CA_CRIA_ESTCONVERGE(HCRA)
      ENDIF

C---     Globalisation si non convergence
      ESTGLB = .FALSE.
      IF (ERR_GOOD() .AND. AGLB .AND. .NOT. ESTRES) THEN
         ETA_K = 1.0D-4
         IERR = GL_GLBL_XEQ(HGLB,
     &                      HSIM,
     &                      NDLN,
     &                      NNL,
     &                      VDLG,
     &                      VDEL,
     &                      ETA_K,
     &                      HALG,
     &                      F_RES)
         ESTGLB = IS_GL(GL_STTS_MODIF)
         ESTGLM = IS_GL(GL_STTS_BMIN)
      ENDIF

C---     Critère de convergence, après globalisation
      ESTRJT = .FALSE.
      ESTOSC = .FALSE.
      IF (ERR_GOOD() .AND. .NOT. ESTRES) THEN
         IF (ESTSITR .AND. ESTGLM) THEN   ! c.f. Note
            ESTRJT = .TRUE.
         ENDIF
         IF (ACCS) THEN
            ITER_CC = ITER
            IF (ESTRJT) ITER_CC = -1
            IERR = AL_RESI_CLCNRM2(HRDU, 1.0D0, VDLG, VDEL, VNRM)
            IERR = CC_CRIC_CALCRI(HCCS, VNRM, ITER_CC, NITER)
            ESTRJT = IS_SS(CC_CRIC_STATUS_STEP_REJECTED)
            ESTOSC = IS_RS(CC_CRIC_REASON_OSCILLATION_DETECTED)
         ENDIF
      ENDIF

C---     Amortis les oscillations
      IF (ERR_GOOD() .AND. .NOT. ESTRJT .AND. ESTOSC)
     &   VDEL(:,:) = 0.5D0 * VDEL(:,:)

C---     Mise à jour de la solution dans VDLG
      IF (ERR_GOOD() .AND. .NOT. ESTRJT)
     &   VDLG(:,:) = VDLG(:,:) + VDEL(:,:)

C---     Traitement post-reso sur les DDL
      IF (ERR_GOOD() .AND. .NOT. ESTRJT)
     &   IERR = LM_ELEM_CLCPST(HSIM)

C---        L'état de résolution
      IF (ERR_GOOD()) THEN
         ISTTS = AL_STTS_OK
         IF (.NOT. ESTRES) ISTTS = IBSET(ISTTS, AL_STTS_CRIA_BAD)
         IF (ESTGLB)       ISTTS = IBSET(ISTTS, AL_STTS_GLOB_LIM)
         IF (ESTRJT)       ISTTS = IBSET(ISTTS, AL_STTS_CRIC_RJT)
!!!         IF (ESTOSC)       ISTTS = IBSET(ISTTS, AL_STTS_CRIC_OSC)
      ENDIF

      AL_NWTN_RES1ITER = ERR_TYP()
      RETURN
      END FUNCTION AL_NWTN_RES1ITER

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER
     &FUNCTION AL_NWTN_ASMKT(HOBJ, HMTX, F_ASM)

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'alnwtn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_NWTN_HBASE
      HSIM = AL_NWTN_HSIM(IOB)

C---     ASSEMBLE Kt
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMKT (HSIM, HMTX, F_ASM)

      AL_NWTN_ASMKT = ERR_TYP()
      RETURN
      END FUNCTION AL_NWTN_ASMKT

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER
     &FUNCTION AL_NWTN_ASMRES(HOBJ, VRES, VDLG, ADLG)

      INTEGER  HOBJ
      REAL*8   VRES(*)
      REAL*8   VDLG(*)
      LOGICAL  ADLG

      INCLUDE 'alnwtn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      INTEGER LDLG
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_NWTN_HBASE
      HSIM = AL_NWTN_HSIM(IOB)

C---     ASSIGNE VDLG À LA SIMULATION
      IF (ADLG) THEN
         IERR = LM_ELEM_PUSHLDLG(HSIM, SO_ALLC_REQVHND(VDLG))
      ENDIF

C---     ASSEMBLE (F-KU) DANS VRES
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMFKU(HSIM, VRES)

C---     RÉTABLIS LA SIMULATION
      IF (ADLG) THEN
         CALL ERR_PUSH()
         IERR = LM_ELEM_POPLDLG(HSIM)
         CALL ERR_POP()
      ENDIF

      AL_NWTN_ASMRES = ERR_TYP()
      RETURN
      END FUNCTION AL_NWTN_ASMRES

C************************************************************************
C Sommaire: Fonction vide, tenant lieu d'une autre.
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER
     &FUNCTION AL_NWTN_DUMMY(HOBJ)

      INTEGER  HOBJ

      INCLUDE 'alnwtn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnwtn.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

D     CALL ERR_ASR(.FALSE.)
      CALL ERR_ASG(ERR_FTL, 'ERR_APPEL_FNCT_INVALIDE')

      AL_NWTN_DUMMY = ERR_TYP()
      RETURN
      END FUNCTION AL_NWTN_DUMMY

      END MODULE AL_NWTN_PRIVATE_M


C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_NWTN_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NWTN_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnwtn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_NWTN_NOBJMAX,
     &                   AL_NWTN_HBASE,
     &                   'Algorithm Newton')

      AL_NWTN_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NWTN_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnwtn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER  IERR
      EXTERNAL AL_NWTN_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_NWTN_NOBJMAX,
     &                   AL_NWTN_HBASE,
     &                   AL_NWTN_DTR)

      AL_NWTN_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NWTN_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnwtn.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_NWTN_NOBJMAX,
     &                   AL_NWTN_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_NWTN_HVALIDE(HOBJ))
         IOB = HOBJ - AL_NWTN_HBASE

         AL_NWTN_HCRA (IOB) = 0
         AL_NWTN_HCCG (IOB) = 0
         AL_NWTN_HCCS (IOB) = 0
         AL_NWTN_HGLB (IOB) = 0
         AL_NWTN_HRDU (IOB) = 0
         AL_NWTN_HRES (IOB) = 0
         AL_NWTN_HSIM (IOB) = 0
         AL_NWTN_LDLU (IOB) = 0
         AL_NWTN_LSOL (IOB) = 0
         AL_NWTN_NITER(IOB) = 0
         AL_NWTN_NSITR(IOB) = 0
         AL_NWTN_STTS (IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_NWTN_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NWTN_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnwtn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_NWTN_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_NWTN_NOBJMAX,
     &                   AL_NWTN_HBASE)

      AL_NWTN_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HCRA     Handle sur le critère d'arrêt
C     HCCG     Handle sur le critère de convergence global
C     HGLB     Handle sur l'algo de globalisation
C     HRES     Handle sur l'algo de résolution matricielle
C     NITER    Nombre d'itérations principales
C     NSITR    Nombre de sous-itérations
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NWTN_INI(HOBJ, HCRA, HCCG, HGLB, HRES, NITER, NSITR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCRA
      INTEGER HCCG
      INTEGER HGLB
      INTEGER HRES
      INTEGER NITER
      INTEGER NSITR

      INCLUDE 'alnwtn.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'alresi.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HCCS, HRDU
      INTEGER NSITREFF
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.solver.newton'

C---     Contrôle des paramètres
      IF (HCRA .NE. 0 .AND. .NOT. CA_CRIA_HVALIDE(HCRA)) GOTO 9900
      IF (HCCG .NE. 0 .AND. .NOT. CC_CRIC_HVALIDE(HCCG)) GOTO 9901
      IF (HGLB .NE. 0 .AND. .NOT. GL_GLBL_HVALIDE(HGLB)) GOTO 9902
      IF (.NOT. MR_RESO_HVALIDE(HRES)) GOTO 9903
      IF (NITER .LT. 0) GOTO 9904
      IF (NSITR .LT. 0) GOTO 9905

C---     Nombre de sous-itérations effectif
      NSITREFF = NSITR
      IF (NSITR .GT. 0 .AND. .NOT. MR_RESO_ESTDIRECTE(HRES)) THEN
         WRITE(LOG_BUF,*) 'MSG_NBR_SOUS_ITER_MIS_A_ZERO'
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         NSITREFF = 0
      ENDIF

C---     Construis le critère de convergence des sous-itérations
      HCCS = 0
      IF (NSITR .GT. 0) THEN
         IERR = CC_CRIC_CTR(HCCS)
         IERR = CC_CRIC_INI(HCCS, 1)
      ENDIF

C---     Construis l'algo de calcul de résidu
      HRDU = 0
      IERR = AL_RESI_CTR(HRDU)

C---     Reset les données
      IERR = AL_NWTN_RST(HOBJ)

C---     Enregistre les données
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - AL_NWTN_HBASE
         AL_NWTN_HCRA (IOB) = HCRA
         AL_NWTN_HCCG (IOB) = HCCG
         AL_NWTN_HCCS (IOB) = HCCS
         AL_NWTN_HGLB (IOB) = HGLB
         AL_NWTN_HRDU (IOB) = HRDU
         AL_NWTN_HRES (IOB) = HRES
         AL_NWTN_LDLU (IOB) = 0
         AL_NWTN_LSOL (IOB) = 0
         AL_NWTN_NITER(IOB) = NITER
         AL_NWTN_NSITR(IOB) = NSITREFF
         AL_NWTN_STTS (IOB) = AL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HCRA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HCRA, 'MSG_CRITERE_ARRET')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HCCG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HCCG, 'MSG_CRITERE_CONVERGENCE')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HGLB
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HGLB, 'MSG_GLOBALISATION')
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HRES
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRES, 'MSG_SOLVER')
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_ITERATION_INVALIDE',': ',NITER
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_SOUS_ITER_INVALIDE',': ',NSITR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_NWTN_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NWTN_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnwtn.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'alresi.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HCCS, HRDU
      INTEGER LDLU, LSOL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_NWTN_HBASE
      HCCS = AL_NWTN_HCCS (IOB)
      HRDU = AL_NWTN_HRDU (IOB)
      LDLU = AL_NWTN_LDLU (IOB)
      LSOL = AL_NWTN_LSOL (IOB)

C---     Récupère l'espace
      IF (SO_ALLC_HEXIST(LSOL)) IERR = SO_ALLC_ALLRE8(0, LSOL)
      IF (SO_ALLC_HEXIST(LDLU)) IERR = SO_ALLC_ALLRE8(0, LDLU)

C---     Détruis les attributs
      IF (CC_CRIC_HVALIDE(HCCS)) IERR = CC_CRIC_DTR(HCCS)
      IF (AL_RESI_HVALIDE(HRDU)) IERR = AL_RESI_CTR(HRDU)

C---     Reset les attributs
      AL_NWTN_HCRA (IOB) = 0
      AL_NWTN_HCCG (IOB) = 0
      AL_NWTN_HCCS (IOB) = 0
      AL_NWTN_HGLB (IOB) = 0
      AL_NWTN_HRDU (IOB) = 0
      AL_NWTN_HRES (IOB) = 0
      AL_NWTN_HSIM (IOB) = 0
      AL_NWTN_LDLU (IOB) = 0
      AL_NWTN_LSOL (IOB) = 0
      AL_NWTN_NITER(IOB) = 0
      AL_NWTN_NSITR(IOB) = 0
      AL_NWTN_STTS (IOB) = AL_STTS_INDEFINI

      AL_NWTN_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_NWTN_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NWTN_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnwtn.fi'
      INCLUDE 'alnwtn.fc'
C------------------------------------------------------------------------

      AL_NWTN_REQHBASE = AL_NWTN_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_NWTN_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NWTN_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnwtn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alnwtn.fc'
C------------------------------------------------------------------------

      AL_NWTN_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_NWTN_NOBJMAX,
     &                                  AL_NWTN_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assigne les nombres d'itérations.
C
C Description:
C     La fonction AL_NWTN_ASGNITER permet d'assigner le nombre d'itérations
C     et le nombre de sous-itérations.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NITER
C     NSITR
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NWTN_ASGNITER(HOBJ, NITER, NSITR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_ASGNITER
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NITER
      INTEGER NSITR

      INCLUDE 'alnwtn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER IOB
      INTEGER HRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.algo.solver.newton'

C---     Récupère les attributs
      IOB = HOBJ - AL_NWTN_HBASE
      HRES = AL_NWTN_HRES(IOB)

C---     Contrôle des paramètres
      IF (NITER .LT. 0) GOTO 9901
      IF (NSITR .LT. 0) GOTO 9902
      IF (NSITR .GT. 0 .AND. .NOT. MR_RESO_ESTDIRECTE(HRES)) THEN
         WRITE(LOG_BUF,*) 'MSG_NBR_SOUS_ITER_MIS_A_ZERO'
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         NSITR = 0
      ENDIF

C---     Enregistre les données
      AL_NWTN_NITER(IOB) = NITER
      AL_NWTN_NSITR(IOB) = NSITR

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_ITERATION_INVALIDE',': ',NITER
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_SOUS_ITER_INVALIDE',': ',NSITR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_NWTN_ASGNITER = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les nombres d'itérations.
C
C Description:
C     La fonction AL_NWTN_ASGNITER retourne le nombre d'itérations
C     et le nombre de sous-itérations.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     NITER
C     NSITR
C
C Notes:
C************************************************************************
      FUNCTION AL_NWTN_REQNITER(HOBJ, NITER, NSITR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_REQNITER
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NITER
      INTEGER NSITR

      INCLUDE 'alnwtn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_NWTN_HBASE
      NITER = AL_NWTN_NITER(IOB)
      NSITR = AL_NWTN_NSITR(IOB)

      AL_NWTN_REQNITER = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle sur le solver.
C
C Description:
C     La fonction AL_NWTN_REQHRES retourne le handle sur le solver.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NWTN_REQHRES(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_REQHRES
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnwtn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnwtn.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_NWTN_REQHRES = AL_NWTN_HRES(HOBJ-AL_NWTN_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Résolution par un algorithme de Newton.
C
C Description:
C     La méthode AL_NWTN_RESOUS(...) résout la simulation par un algorithme
C     itératif de linéarisation de Newton.
C     Pour un calcul stationnaire, DELT = 0.
C     En sortie, les DLL sont à TSIM+DELT, mais les prop ne sont pas toutes
C     ajustées à la dernière valeur des DDL. En fait elles sont une
C     itération en retard.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM         Handle sur l'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NWTN_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_RESOUS
CDEC$ ENDIF

      USE AL_NWTN_PRIVATE_M, ONLY : AL_NWTN_ASMKT,
     &                              AL_NWTN_ASMRES,
     &                              AL_NWTN_DUMMY
      IMPLICIT NONE

      INTEGER, INTENT(IN)     :: HOBJ
      REAL*8,  INTENT(INOUT)  :: TSIM
      REAL*8,  INTENT(INOUT)  :: DELT
      INTEGER, INTENT(IN)     :: HSIM

      INCLUDE 'alnwtn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alnwtn.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Conserve les paramètres pour la simulation
      IOB  = HOBJ - AL_NWTN_HBASE
      AL_NWTN_HSIM(IOB) = HSIM

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM+DELT)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)

C---     Résous
      IF (ERR_GOOD())
     &   IERR = AL_NWTN_RESOUS_E(HOBJ,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           HOBJ,
     &                           AL_NWTN_DUMMY, ! K
     &                           AL_NWTN_ASMKT,
     &                           AL_NWTN_DUMMY, ! KU
     &                           AL_NWTN_DUMMY, ! F
     &                           AL_NWTN_ASMRES)! R

C---     Post-calcul des données (TSIM mis à jour)
      IF (ERR_GOOD()) IERR = LM_ELEM_PASFIN(HSIM, TSIM)

      AL_NWTN_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_NWTN_RESOUS_E(...) est la méthode de bas niveau pour
C     résoudre une simulation par un algorithme itératif de linéarisation
C     de Newton.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo.
C     L'algo sort avec la dernière itération non rejetée.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM         Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_K         Fonction de calcul de [K]
C     EXTERNAL F_KT        Fonction de calcul de [KT]
C     EXTERNAL F_KU        Fonction de calcul de [K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C     les propriétés ne sont pas réactualisées sur les itérations
C
C     Hiérarchie entre cria, glob et cric. 2 possibilités
C     1) cria == glob > cric     plus contraignant
C     2) cria > glob > cric      cria prépondérant
C     C'est 2) qui est implanté
C************************************************************************
      FUNCTION AL_NWTN_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          HALG,
     &                          F_K,
     &                          F_KT,
     &                          F_KU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_RESOUS_E
CDEC$ ENDIF

      USE AL_NWTN_PRIVATE_M, ONLY : AL_NWTN_RES1ITER
      USE SO_ALLC_M, ONLY : SO_ALLC_PTR_T, SO_ALLC_PTR
      IMPLICIT NONE

      INTEGER, INTENT(IN)     :: HOBJ
      REAL*8,  INTENT(INOUT)  :: TSIM
      REAL*8,  INTENT(INOUT)  :: DELT
      INTEGER, INTENT(IN)     :: HSIM
      INTEGER, INTENT(IN)     :: HALG
      INTEGER  F_K
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_K
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alnwtn.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'alutil.fi'
      INCLUDE 'alresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'alnwtn.fc'

      REAL*8  CRIAV, VNRM
      INTEGER IERR
      INTEGER IOB
      INTEGER IP, IS
      INTEGER HSVPT
      INTEGER HCRA, HCCG, HCCS, HGLB, HNMR, HRDU
      INTEGER LSOL, LDLU
      INTEGER LDLG, LSPT, LLOCN
      INTEGER NNL, NDLN, NDLL, NEQL
      INTEGER NITER, NSITR
      INTEGER ICNT_NITR, ICNT_NSIT, ISTTS
      LOGICAL ACCG, ACCS, ACRA, AGLB
      LOGICAL ESTOK,  ESTRES
      LOGICAL ESTRJG, ESTRJS, ESTOSC, DOSMO
      
      TYPE(SO_ALLC_PTR_T) :: VSPT_P
      TYPE(SO_ALLC_PTR_T) :: VDLG_P
      TYPE(SO_ALLC_PTR_T) :: VDLU_P
      TYPE(SO_ALLC_PTR_T) :: VSOL_P

      REAL*8, POINTER :: VSPT(:,:)
      REAL*8, POINTER :: VDLG(:,:)
      REAL*8, POINTER :: VDLU(:,:)
      REAL*8, POINTER :: VSOL(:)

      INTEGER, PARAMETER :: NITER_SMOOTH_MAX = 2
C------------------------------------------------------------------------
      INTEGER II
      LOGICAL IS_RG, IS_SG, IS_RS, IS_SS
      IS_RG(II) = (CC_CRIC_REQRAISON(HCCG) .EQ. II)
      IS_SG(II) = (CC_CRIC_REQSTATUS(HCCG) .EQ. II)
      IS_RS(II) = (CC_CRIC_REQRAISON(HCCS) .EQ. II)
      IS_SS(II) = (CC_CRIC_REQSTATUS(HCCS) .EQ. II)
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.algo.solver.newton'

C---     Récupère les attributs
      IOB = HOBJ - AL_NWTN_HBASE
      HCRA  = AL_NWTN_HCRA (IOB)
      HCCG  = AL_NWTN_HCCG (IOB)
      HCCS  = AL_NWTN_HCCS (IOB)
      HGLB  = AL_NWTN_HGLB (IOB)
      HRDU  = AL_NWTN_HRDU (IOB)
      LDLU  = AL_NWTN_LDLU (IOB)
      LSOL  = AL_NWTN_LSOL (IOB)
      NITER = AL_NWTN_NITER(IOB)
      NSITR = AL_NWTN_NSITR(IOB)

C---     Récupère les données
      HNMR = LM_ELEM_REQHNUMC(HSIM)
      NNL  = LM_ELEM_REQPRM(HSIM, LM_GEOM_PRM_NNL)
      NDLN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      LDLG = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)
      LLOCN= LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)

C---     Conserve les paramètres
      AL_NWTN_HSIM(IOB) = HSIM

C---     Présence/absence des composantes
      ACCG = CC_CRIC_HVALIDE(HCCG)
      ACCS = CC_CRIC_HVALIDE(HCCS)
      ACRA = CA_CRIA_HVALIDE(HCRA)
      AGLB = GL_GLBL_HVALIDE(HGLB)

C---     Les stats
      ICNT_NITR = 0     ! Nbr d'itérations
      ICNT_NSIT = 0     ! Nbr de sous-iter

C---     Écris l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_RESOLUTION_NEWTON'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Écris les attributs pertinents
      WRITE(LOG_BUF,'(2A,F25.6)') 'MSG_TEMPS_SIMU#<35>#', '= ',TSIM+DELT
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(2A,I12)') 'MSG_NBR_ITERATIONS#<35>#', '= ',NITER
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(2A,I12)') 'MSG_NBR_SOUS_ITER#<35>#', '= ',NSITR
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(2A,I12)') 'MSG_NBR_INCONNUES#<35>#', '= ',NEQL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     (Re)-alloue l'espace pour la solution
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LDLU)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NEQL, LSOL)
      IF (ERR_GOOD()) AL_NWTN_LDLU(IOB) = LDLU
      IF (ERR_GOOD()) AL_NWTN_LSOL(IOB) = LSOL

C---     Crée un point de sauvegarde
      HSVPT = 0
      LSPT = 0
      IF (ERR_GOOD()) IERR = SO_SVPT_CTR(HSVPT)
      IF (ERR_GOOD()) IERR = SO_SVPT_INI(HSVPT, LDLG)
      IF (ERR_GOOD()) LSPT = SO_SVPT_GET(HSVPT)
      
C---     Pointeurs aux tables
      IF (ERR_GOOD()) THEN
         VSPT_P = SO_ALLC_PTR(LSPT)
         VDLG_P = SO_ALLC_PTR(LDLG)
         VDLU_P = SO_ALLC_PTR(LDLU)
         VSOL_P = SO_ALLC_PTR(LSOL)
         VSPT => VSPT_P%CST2V(NDLN, NNL)
         VDLG => VDLG_P%CST2V(NDLN, NNL)
         VDLU => VDLU_P%CST2V(NDLN, NNL)
         VSOL => VSOL_P%CST2V(NEQL)
      ENDIF

C---     Initialise la globalisation
      IF (ERR_GOOD() .AND. AGLB) IERR = GL_GLBL_DEB(HGLB)

C---     Initialise l'algorithme de calcul du résidu 
      IERR = AL_RESI_INI(HRDU, F_RES, HALG)
      
C---     Initialise les états
      ISTTS = AL_STTS_INDEFINI
      ESTOK  = .FALSE.
      ESTRES = .FALSE.
      ESTRJG = .FALSE.
      ESTRJS = .FALSE.

C---     Boucle sur les itérations
C        =========================
      IF (ERR_BAD()) GOTO 199
      DO IP=1, NITER
         CALL LOG_INFO(LOG_ZNE, ' ')
         WRITE(LOG_BUF, '(2(A,I6))') 'MSG_NEWTON_ITER: ',IP,' /',NITER
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         CALL LOG_INCIND()

C---        Sauve l'état courant
         IF (ERR_GOOD() .AND. HSVPT .NE. 0) THEN
            IERR = SO_SVPT_SAVE(HSVPT)
         ENDIF

C---        Résous
         ICNT_NITR = ICNT_NITR + 1
         CRIAV = 1.0D+99
         IF (ERR_GOOD()) THEN
            IERR = AL_NWTN_RES1ITER(HOBJ,
     &                              HALG,
     &                              F_KT,
     &                              F_KU,
     &                              F_RES,
     &                              VSOL,
     &                              VDLU,
     &                              1, NSITR+1,  ! Iteration 1 pour CC_CRIC
     &                              ISTTS)       ! Status de la réso
         ENDIF

C---        L'état de résolution
         ESTOK  = (ISTTS .EQ. AL_STTS_OK)
         ESTRES = .NOT. BTEST(ISTTS, AL_STTS_CRIA_BAD)
         ESTRJG = BTEST(ISTTS, AL_STTS_CRIC_RJT)

C---        Log l'incrément après globalisation
         IF (ERR_GOOD()) THEN
            IERR = AL_UTIL_LOGSOL(NDLN, NNL, VDLU, HNMR)
         ENDIF

C---        La valeur du critère d'arrêt
         IF (ERR_GOOD() .AND. ACRA)
     &      CRIAV = CA_CRIA_REQVAL(HCRA)

C---        Gère la sortie
         CALL LOG_DECIND()
         IF (ERR_BAD()) GOTO 189
         IF (ESTRES)    GOTO 189
         IF (ESTRJG)    GOTO 189

C---        Boucle sur les sous-itérations
C           ==============================
         DO IS=1, NSITR
            ICNT_NSIT = ICNT_NSIT + 1
            WRITE(LOG_BUF,'(2(A,I6))')'MSG_NEWTON_SITER: ',IS,' /',NSITR
            CALL LOG_INFO(LOG_ZNE, LOG_BUF)
            CALL LOG_INCIND()

            IF (ERR_GOOD()) THEN
               IERR = AL_NWTN_RES1ITER(HOBJ,
     &                                 HALG,
     &                                 F_KT,
     &                                 F_KU,
     &                                 F_RES,
     &                                 VSOL,
     &                                 VDLU,
     &                                 IS+1, NSITR+1,
     &                                 ISTTS)       ! Status de la réso
            ENDIF

C---           L'état de résolution
            ESTOK  = (ISTTS .EQ. AL_STTS_OK)
            ESTRES = .NOT. BTEST(ISTTS, AL_STTS_CRIA_BAD)
            ESTRJS = BTEST(ISTTS, AL_STTS_CRIC_RJT)

C---           Log l'état de convergence
            IF (ERR_GOOD() .AND. ACCS .AND. .NOT. ESTOK) THEN
               IF (IS_RS(CC_CRIC_REASON_DIVERGENCE_DETECTED)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_DIVERGENCE_DETECTEE')
               ENDIF
               IF (IS_RS(CC_CRIC_REASON_DIVERGENCE_ESTABLISHED)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_DIVERGENCE')
               ENDIF
               IF (IS_RS(CC_CRIC_REASON_WEAK_CONVERGENCE)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_WEAK_CONVERGENCE')
               ENDIF
               IF (IS_RS(CC_CRIC_REASON_EXTERNAL_IMPOSED)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_GLOBALISATION_DU_PAS')
               ENDIF
               IF (IS_RS(CC_CRIC_REASON_OSCILLATION_DETECTED)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_OSCILLATION_DETECTEE')
               ENDIF
               IF (IS_SS(CC_CRIC_STATUS_STEP_REJECTED)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_ITER_REJETEE')
               ENDIF
            ENDIF

C---           Log l'incrément après globalisation
            IF (ERR_GOOD() .AND. .NOT. ESTRJS) THEN
               IERR = AL_UTIL_LOGSOL(NDLN, NNL, VDLU, HNMR)
            ENDIF

C---           La valeur du critère d'arrêt
            IF (ERR_GOOD() .AND. ACRA  .AND. .NOT. ESTRJS)
     &         CRIAV = CA_CRIA_REQVAL(HCRA)

C---           Gère la sortie
            CALL LOG_DECIND()
            IF (ERR_BAD()) EXIT
            IF (ESTRES) EXIT
            IF (ESTRJS) EXIT
         ENDDO
189      CONTINUE

C---        Traceur
         IF (ERR_GOOD()) THEN
            IERR = TRA_ASGALGO('nwtn')
            IERR = TRA_ASGITER(IP)
            IERR = TRA_ASGCRIA(CRIAV)
            IERR = TRA_ECRIS()
         ENDIF

C---        Sortie en cas de succès
         IF (ESTRES) EXIT
         ISTTS = IBCLR(ISTTS, AL_STTS_GLOB_LIM)  ! Clear GLOB
         ISTTS = IBCLR(ISTTS, AL_STTS_CRIC_RJT)  ! Clear CRIC
         ISTTS = IBCLR(ISTTS, AL_STTS_CRIC_OSC)  ! Clear CRIC

C---        Delta global dans LDLU
         IF (ERR_GOOD() .AND. ACCG .AND. NSITR .GT. 0) THEN
            ! VDLU(:,:) = VDLG(:,:) - VSPT(:,:)   ! Crée un stack-oveflow
            CALL DCOPY(NDLL, VDLG, 1, VDLU, 1)
            CALL DAXPY(NDLL,-1.0D0, VSPT, 1, VDLU, 1)
            IERR = SP_ELEM_CLIM2VAL(NDLL,
     &                              KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                              0.0D0,
     &                              VDLU)
         ENDIF

C---        Boucle d'amortissement
C           ======================
         ESTRJG = .FALSE.
         IF (ERR_GOOD() .AND. ACCG) THEN
            DO IS=1, NITER_SMOOTH_MAX

               ! ---  Critère de convergence, sur le delta global, après globalisation
               IERR = AL_RESI_CLCNRM2(HRDU, 1.0D0, VSPT, VDLU, VNRM)
               IERR = CC_CRIC_CALCRI(HCCG, VNRM, IP, NITER)

               ! ---  Log l'état de convergence
               ESTRJG = IS_SG(CC_CRIC_STATUS_STEP_REJECTED)
               ESTOSC = IS_RG(CC_CRIC_REASON_OSCILLATION_DETECTED)
               IF (IS_RG(CC_CRIC_REASON_DIVERGENCE_DETECTED)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_DIVERGENCE_DETECTEE')
               ENDIF
               IF (IS_RG(CC_CRIC_REASON_DIVERGENCE_ESTABLISHED)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_DIVERGENCE')
               ENDIF
               IF (IS_RG(CC_CRIC_REASON_WEAK_CONVERGENCE)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_WEAK_CONVERGENCE')
               ENDIF
               IF (IS_RG(CC_CRIC_REASON_EXTERNAL_IMPOSED)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_GLOBALISATION_DU_PAS')
               ENDIF
               IF (IS_RG(CC_CRIC_REASON_OSCILLATION_DETECTED)) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_OSCILLATION_DETECTEE')
               ENDIF

               ! ---  Met à jour l'état
               IF (ESTRJG) ISTTS = IBSET(ISTTS, AL_STTS_CRIC_RJT)
               IF (ESTOSC) ISTTS = IBSET(ISTTS, AL_STTS_CRIC_OSC)

               ! ---  Amortis les oscillations
               DOSMO = IS .NE. NITER_SMOOTH_MAX
               DOSMO = DOSMO .AND. ESTOSC
               DOSMO = DOSMO .AND. .NOT. ESTRJG
               IF (DOSMO) THEN
                  CALL LOG_INFO(LOG_ZNE, 'MSG_APPLIQUE_AMORTISSEMENT')
                  VDLU(:,:) = 0.5D0 * VDLU(:,:)
               ENDIF

               ! ---  Gère la sortie
               IF (ESTRJG) EXIT
               IF (.NOT. ESTOSC) EXIT
            ENDDO
         ENDIF

C---        Met à jour VDLG
         IF (ERR_GOOD()) THEN
            IF (ESTRJG) THEN
               IERR = SO_SVPT_RBCK(HSVPT)
               IERR = LM_ELEM_CLCPST(HSIM)
            ELSEIF (ACCG .AND. NSITR .GT. 0) THEN
               ! VDLG(:,:) = VSPT(:,:) + VDLU(:,:) !Stack-overflow
               CALL DAXPY(NDLL, 1.0D0, VSPT, 1, VDLU, 1)
               CALL DCOPY(NDLL, VDLU, 1, VDLG, 1)
               IERR = LM_ELEM_CLCPST(HSIM)
            ENDIF
         ENDIF

C---        Gère la sortie
         IF (ERR_BAD()) EXIT
         !!! IF (ISTTS .EQ. AL_STTS_OK) EXIT
         IF (ESTRJG) EXIT
      ENDDO
199   CONTINUE

C---     Relâche le point de sauvegarde
      IF (HSVPT .NE. 0) THEN
         CALL ERR_PUSH()
         IERR = SO_SVPT_DTR(HSVPT)
         CALL ERR_POP()
      ENDIF

C---     Ajuste TSIM et DELT à la partie calculée
      IF (ERR_GOOD() .AND. ISTTS .EQ. AL_STTS_OK) THEN
         TSIM = TSIM + DELT
         DELT = 0.0D0
      ENDIF

C---     Conserve l'état
      AL_NWTN_STTS(IOB) = ISTTS

C---     Conserve les statistiques
      AL_NWTN_CNTRS(1,IOB) = ICNT_NITR
      AL_NWTN_CNTRS(2,IOB) = ICNT_NSIT

      CALL LOG_DECIND()
      AL_NWTN_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NWTN_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NWTN_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alnwtn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnwtn.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_NWTN_REQSTATUS = AL_NWTN_STTS(HOBJ - AL_NWTN_HBASE)
      RETURN
      END
