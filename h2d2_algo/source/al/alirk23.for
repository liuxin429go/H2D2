C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  ALgorithme
C Objet:   esdIRK23
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_IRK23_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRK23_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alirk23.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_IRK23_NOBJMAX,
     &                   AL_IRK23_HBASE,
     &                   'Algorithm ESDIRK23')

      AL_IRK23_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRK23_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alirk23.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER  IERR
      EXTERNAL AL_IRK23_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_IRK23_NOBJMAX,
     &                   AL_IRK23_HBASE,
     &                   AL_IRK23_DTR)

      AL_IRK23_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRK23_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirk23.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_IRK23_NOBJMAX,
     &                   AL_IRK23_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_IRK23_HVALIDE(HOBJ))
         IOB = HOBJ - AL_IRK23_HBASE

         AL_IRK23_HPRNT(IOB) = 0
      ENDIF

      AL_IRK23_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRK23_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirk23.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRK23_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_IRK23_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_IRK23_NOBJMAX,
     &                   AL_IRK23_HBASE)

      AL_IRK23_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRK23_INI(HOBJ, HRES, DELT, HCTI)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HRES
      REAL*8  DELT
      INTEGER HCTI

      INCLUDE 'alirk23.fi'
      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'alirk23.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
      INTEGER HF_A, HF_B, HF_C, HF_D, HF_CS
      EXTERNAL AL_IRK23_FA
      EXTERNAL AL_IRK23_FB
      EXTERNAL AL_IRK23_FC
      EXTERNAL AL_IRK23_FD
      EXTERNAL AL_IRK23_CS
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRK23_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Construis les call back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HF_A)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HF_A, AL_IRK23_FA)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HF_B)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HF_B, AL_IRK23_FB)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HF_C)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HF_C, AL_IRK23_FC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HF_D)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HF_D, AL_IRK23_FD)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HF_CS)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HF_CS, AL_IRK23_CS)

C---     Construis le parent
      IF (ERR_GOOD()) IERR = AL_IRKNN_CTR(HPRNT)
      IF (ERR_GOOD()) IERR = AL_IRKNN_INI(HPRNT,
     &                                    HCTI,
     &                                    HRES,
     &                                    DELT,
     &                                    AL_IRK23_NSTG,
     &                                    HF_A,
     &                                    HF_B,
     &                                    HF_C,
     &                                    HF_D,
     &                                    HF_CS)

C---     RESET LES DONNEES
      IF (ERR_GOOD()) IERR = AL_IRK23_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - AL_IRK23_HBASE
         AL_IRK23_HPRNT(IOB) = HPRNT
      ENDIF

      AL_IRK23_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRK23_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirk23.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirknn.fi'
      INCLUDE 'alirk23.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRK23_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - AL_IRK23_HBASE
      HPRNT = AL_IRK23_HPRNT(IOB)

C---     Détruis le parent
      IF (AL_IRKNN_HVALIDE(HPRNT)) IERR = AL_IRKNN_DTR(HPRNT)

C---     RESET
      AL_IRK23_HPRNT(IOB) = 0

      AL_IRK23_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_IRK23_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRK23_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alirk23.fi'
      INCLUDE 'alirk23.fc'
C------------------------------------------------------------------------

      AL_IRK23_REQHBASE = AL_IRK23_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_IRK23_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRK23_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirk23.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alirk23.fc'
C------------------------------------------------------------------------

      AL_IRK23_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_IRK23_NOBJMAX,
     &                                  AL_IRK23_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_IRK23_RESOUS intègre dans le temps par une méthode d'Euler
C     la simulation HSIM. Globalement, elle amène HSIM de TSIM à TSIM+DELT.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM        Handle sur l'élément
C
C Sortie:
C
C Notes:
C     En sortie, les dll sont à t+dt, mais les prop ne sont pas toutes
C     ajustées à la dernière valeur des ddl. En fait elles sont une
C     itération en retard.
C************************************************************************
      FUNCTION AL_IRK23_RESOUS(HOBJ,
     &                         TSIM,
     &                         DELT,
     &                         HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM

      INCLUDE 'alirk23.fi'
      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRK23_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_IRK23_HBASE
      HPRNT = AL_IRK23_HPRNT(IOB)

C---     Appel le parent
      IERR = AL_IRKNN_RESOUS(HPRNT, TSIM, DELT, HSIM)

      AL_IRK23_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_IRK23_RESOUS_E(...) est la méthode de bas niveau pour
C     résoudre une simulation par un algorithme d'Euler.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo. C'est la fonction appelante qui dois gérer le
C     chargement des données.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM        Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_MDTK      Fonction de calcul de [M.dt + a.K]
C     EXTERNAL F_MDTKT     Fonction de calcul de [M.dt + a.KT]
C     EXTERNAL F_MDTKU     Fonction de calcul de [M.dt + a.K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRK23_RESOUS_E(HOBJ,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           HALG,
     &                           F_MDTK,
     &                           F_MDTKT,
     &                           F_MDTKU,
     &                           F_F,
     &                           F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_MDTK
      INTEGER  F_MDTKT
      INTEGER  F_MDTKU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_MDTK
      EXTERNAL F_MDTKT
      EXTERNAL F_MDTKU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alirk23.fi'
      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRK23_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Recupere les attributs
      IOB = HOBJ - AL_IRK23_HBASE
      HPRNT = AL_IRK23_HPRNT(IOB)

C---     RESOUS_E
      IF (ERR_GOOD()) THEN
         IERR = AL_IRKNN_RESOUS_E(HPRNT,
     &                            TSIM,
     &                            DELT,
     &                            HSIM,
     &                            HALG,
     &                            F_MDTK,
     &                            F_MDTKT,
     &                            F_MDTKU,
     &                            F_F,
     &                            F_RES)
      ENDIF

      AL_IRK23_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRK23_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alirk23.fi'
      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRK23_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Appel le parent
      HPRNT = AL_IRK23_HPRNT(HOBJ - AL_IRK23_HBASE)
      AL_IRK23_REQSTATUS = AL_IRKNN_REQSTATUS(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRK23_ASGDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_ASGDELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'alirk23.fi'
      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRK23_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Appel le parent
      HPRNT = AL_IRK23_HPRNT(HOBJ - AL_IRK23_HBASE)
      AL_IRK23_ASGDELT = AL_IRKNN_ASGDELT(HPRNT, DELT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRK23_REQDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRK23_REQDELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'alirk23.fi'
      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRK23_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Appel le parent
      HPRNT = AL_IRK23_HPRNT(HOBJ - AL_IRK23_HBASE)
      AL_IRK23_REQDELT = AL_IRKNN_REQDELT(HPRNT, DELT)
      RETURN
      END

C************************************************************************
C Sommaire: Composante A du tableau de Butcher
C
C Description:
C     La fonction AL_IRK23_FA retourne les coefficients de la matrice A
C     du tableau de Butcher
C
C Entrée:
C     I, J     Indices
C
C Sortie:
C     A        Valeur
C
C Notes:
C     Tableau de Butcher
C           c      |   A
C           ------------
C           u(n+1) |  bT
C           e(n+1) |  dT
C
C************************************************************************
      FUNCTION AL_IRK23_FA(I, J, A)

      IMPLICIT NONE

      INTEGER I
      INTEGER J
      REAL*8  A

      INCLUDE 'alirk23.fc'
      INCLUDE 'err.fi'

      INTEGER II
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GT. 0)
D     CALL ERR_PRE(I .LE. AL_IRK23_NSTG)
D     CALL ERR_PRE(J .LE. I)
C------------------------------------------------------------------------

      II = I*10 + J
      IF (II .EQ. 11) A =  0.00000000000000D+00
      IF (II .EQ. 21) A =  0.29289321881345D+00
      IF (II .EQ. 22) A =  AL_IRK23_GMMA
      IF (II .EQ. 31) A =  0.35355339059327D+00
      IF (II .EQ. 32) A =  0.35355339059327D+00
      IF (II .EQ. 33) A =  AL_IRK23_GMMA

      AL_IRK23_FA = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Composante B du tableau de Butcher
C
C Description:
C     La fonction AL_IRK23_FB retourne les coefficients du vecteur B
C     du tableau de Butcher
C
C Entrée:
C     J        Indices
C
C Sortie:
C     B        Valeur
C
C Notes:
C     Tableau de Butcher
C           c      |   A
C           ------------
C           u(n+1) |  bT
C           e(n+1) |  dT
C************************************************************************
      FUNCTION AL_IRK23_FB(J,B)

      IMPLICIT NONE

      INTEGER J
      REAL*8  B

      INCLUDE 'alirk23.fc'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------
D     CALL ERR_PRE(J .GT. 0)
D     CALL ERR_PRE(J .LE. AL_IRK23_NSTG)
C------------------------------------------------------------------------

      IF (J .EQ. 1) B =  0.35355339059327D+00
      IF (J .EQ. 2) B =  0.35355339059327D+00
      IF (J .EQ. 3) B =  AL_IRK23_GMMA

      AL_IRK23_FB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Composante C du tableau de Butcher
C
C Description:
C     La fonction AL_IRK23_FC retourne les coefficients du vecteur C
C     du tableau de Butcher
C
C Entrée:
C     J        Indices
C
C Sortie:
C     C        Valeur
C
C Notes:
C     Tableau de Butcher
C           c      |   A
C           ------------
C           u(n+1) |  bT
C           e(n+1) |  dT
C************************************************************************
      FUNCTION AL_IRK23_FC(I,C)

      IMPLICIT NONE

      INTEGER I
      REAL*8  C

      INCLUDE 'alirk23.fc'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GT. 0)
D     CALL ERR_PRE(I .LE. AL_IRK23_NSTG)
C------------------------------------------------------------------------

      IF (I .EQ. 1) C = 0.00000000000000D+00
      IF (I .EQ. 2) C = 0.58578643762690D+00
      IF (I .EQ. 3) C = 1.00000000000000D+00

      AL_IRK23_FC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Composante D du tableau de Butcher
C
C Description:
C     La fonction AL_IRK23_FD retourne les coefficients du vecteur D
C     du tableau de Butcher
C
C Entrée:
C     J        Indices
C
C Sortie:
C     D        Valeur
C
C Notes:
C     Tableau de Butcher
C           c      |   A
C           ------------
C           u(n+1) |  bT
C           e(n+1) |  dT
C************************************************************************
      FUNCTION AL_IRK23_FD(J,D)

      IMPLICIT NONE

      INTEGER J
      REAL*8  D

      INCLUDE 'alirk23.fc'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------
D     CALL ERR_PRE(J .GT. 0)
D     CALL ERR_PRE(J .LE. AL_IRK23_NSTG)
C------------------------------------------------------------------------

      IF (J .EQ. 1) D = -0.13807118745770D+00
      IF (J .EQ. 2) D =  0.33333333333333D+00
      IF (J .EQ. 3) D = -0.19526214587563D+00

      AL_IRK23_FD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Composante de la matrice de solution continue
C
C Description:
C     La fonction AL_IRK23_FZ retourne les coefficients de la matrice
C
C Entrée:
C     I,J      Indices
C
C Sortie:
C     Z        Valeur
C
C Notes:
C************************************************************************
      FUNCTION AL_IRK23_FZ(I, J, Z)

      IMPLICIT NONE

      INTEGER I
      INTEGER J
      REAL*8  Z

      INCLUDE 'alirk23.fc'
      INCLUDE 'err.fi'

      INTEGER II
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GT. 0)
D     CALL ERR_PRE(I .LE. AL_IRK23_NSTG)
D     CALL ERR_PRE(J .GT. 0)
D     CALL ERR_PRE(J .LE. 3)
C------------------------------------------------------------------------

      II = I*10 + J
      IF (II .EQ. 11) Z =  0.92716600367945D+00
      IF (II .EQ. 12) Z = -1.64140141649749D+00
      IF (II .EQ. 13) Z =  0.81663481343795D+00
      IF (II .EQ. 21) Z = -0.65894519150133D+00
      IF (II .EQ. 22) Z = -0.66560479362449D+00
      IF (II .EQ. 23) Z =  0.94767153287026D+00
      IF (II .EQ. 31) Z =  0.29591266631342D+00
      IF (II .EQ. 32) Z =  2.30700621012198D+00
      IF (II .EQ. 33) Z = -1.76430634630822D+00
      IF (II .EQ. 41) Z =  0.43586652150846D+00
      IF (II .EQ. 42) Z =  0.00000000000000D+00
      IF (II .EQ. 43) Z =  0.00000000000000D+00

      AL_IRK23_FZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcul la solution continue
C
C Description:
C     La fonction AL_IRK12_CS calcule la solution continue. Pour le pas
C     de temps THETA*DELT elle calcule l'incrément de solution.
C
C Entrée:
C     ISTG     Stage index
C     DELT     Pas de temps
C     THETA    Solution demandée pour THETA*DELT
C     NEQ      Dimension des table VRES et VSOL
C     VRES     Table (NEQ,*) des résidus
C
C Sortie:
C     VSOL     Incrément de solution
C
C Notes:
C************************************************************************
      FUNCTION AL_IRK23_CS (ISTG,
     &                      DELT,
     &                      THETA,
     &                      NEQ,
     &                      VRES,
     &                      VSOL)

      IMPLICIT NONE

      INTEGER  ISTG
      REAL*8   DELT
      REAL*8   THETA
      INTEGER  NEQ
      REAL*8   VRES(NEQ, *)
      REAL*8   VSOL (NEQ)

      INCLUDE 'alirk23.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirk23.fc'

      INTEGER IERR
      INTEGER I
      REAL*8  B1, B2, B3, BT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      CALL DINIT(NEQ, 0.0D0, VSOL, 1)
      DO I=1,ISTG
         IERR = AL_IRK23_FZ(I, 1, B1)
         IERR = AL_IRK23_FZ(I, 2, B2)
         IERR = AL_IRK23_FZ(I, 3, B3)
         BT = THETA*(B1 + THETA*(B2 + THETA*B3))

         CALL DAXPY(NEQ, BT*DELT, VRES(1,I), 1, VSOL, 1)
      ENDDO

      AL_IRK23_CS = ERR_TYP()
      RETURN
      END

