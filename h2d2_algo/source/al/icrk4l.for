C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALRK4L_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALRK4L_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icrk4l.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alrk4l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icrk4l.fc'

      INTEGER IERR
      INTEGER HALG
      INTEGER HOBJ
      INTEGER HCTI
      INTEGER NITR
      REAL*8  DELT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALRK4L_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Number of internal iterations (max)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, NITR)
C     <comment>Nominal time step</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, DELT)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Handle on the increment controller (default 0)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HCTI)
      IF (IERR .NE. 0) HCTI = 0

C---     CONSTRUIS ET INITIALISE L'OBJET
      HALG = 0
      IF (ERR_GOOD()) IERR = AL_RK4L_CTR(HALG)
      IF (ERR_GOOD()) IERR = AL_RK4L_INI(HALG, NITR, DELT, HCTI)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SLVR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SLVR_INI(HOBJ, HALG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_ALRK4L_PRN(HALG)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HALG
      ENDIF

C<comment>
C  The constructor <b>runge_kutta_4l</b> constructs an object, with the
C  given arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALRK4L_AID()

9999  CONTINUE
      IC_ALRK4L_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: fonction PRiNt
C
C Description: Fonction PRN qui imprime tous les paramètres de l'objet
C
C Entrée: HOBJ
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALRK4L_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'alrk4l.fi'
      INCLUDE 'alrk4l.fc'
      INCLUDE 'icrk4l.fc'

      INTEGER IOB
      INTEGER NITR
      REAL*8  DELT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CMD_ALGORITHME_RK4L'
      CALL LOG_ECRIS(LOG_BUF)

C---     IMPRESSION DES PARAMETRES DU BLOC
      IOB = HOBJ - AL_RK4L_HBASE
      NITR = AL_RK4L_NITR(IOB)
      WRITE (LOG_BUF,'(A,I6)') 'MSG_NITER_INTERNES#<35>#= ', NITR
      CALL LOG_ECRIS(LOG_BUF)
      DELT = AL_RK4L_DTAL(IOB)
      WRITE (LOG_BUF,'(A,1PE14.6E3)') 'MSG_PAS_DE_TEMPS#<35>#= ', DELT
      CALL LOG_ECRIS(LOG_BUF)

      IC_ALRK4L_PRN = ERR_TYP()
      RETURN
      END


C************************************************************************
C Sommaire:
C
C Description:
C     On ne résout ici que les méthodes particulières à la classe.
C     Les méthodes virtuelles des algos sont résolues par le proxy.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALRK4L_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALRK4L_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'icrk4l.fi'
      INCLUDE 'alrk4l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icrk4l.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>solve</b> executes the algorithm.</comment>
      IF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQ@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_ALRK4L_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_ALRK4L_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALRK4L_AID()

9999  CONTINUE
      IC_ALRK4L_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALRK4L_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALRK4L_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icrk4l.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>runge_kutta_4l</b> represents the Runge-Kutta
C  time integration algorithm of 4th order, with lumped mass matrix.
C</comment>
      IC_ALRK4L_REQCLS = 'runge_kutta_4l'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALRK4L_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALRK4L_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icrk4l.fi'
      INCLUDE 'alrk4l.fi'
C-------------------------------------------------------------------------

      IC_ALRK4L_REQHDL = AL_RK4L_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALRK4L_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icrk4l.hlp')

      RETURN
      END

