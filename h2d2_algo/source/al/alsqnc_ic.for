C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C      FUNCTION IC_ALSQNC_XEQCTR ()
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQNC_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQNC_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'alsqnc_ic.fi'
      INCLUDE 'alsqnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alsqnc_ic.fc'

      INTEGER IERR
      INTEGER HOBJ
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALSQNC_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SQNC_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SQNC_INI(HOBJ)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_ALSQNC_PRN(HOBJ)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the sequence</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>algo_sequence</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_AUCUN_PARAMETRE_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALSQNC_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_ALSQNC_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALSQNC_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQNC_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'alsqnc_ic.fi'
      INCLUDE 'alsqnc.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alsqnc_ic.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      INTEGER      HVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
      INTEGER      HELE, HALG, HPST, HTRG, HITM
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
!!!         IOB = HOBJ - AL_SQNC_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Number of items in the sequence</comment>
         IF (PROP .EQ. 'n') THEN
            IVAL = AL_SQNC_REQDIM(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
         ENDIF

C     <comment>Get an item with its index</comment>
      ELSEIF (IMTH .EQ. '##opb_[]_get##') THEN
D        CALL ERR_ASR(AL_SQNC_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Index of the item</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
         IF (IERR .NE. 0) GOTO 9901
         IF (ERR_GOOD()) HVAL = AL_SQNC_REQHITM(HOBJ, IVAL)
C        <comment>The sequence item</comment>
         IF (ERR_GOOD()) WRITE(IPRM, '(2A,I12)') 'H', ',', HVAL

C     <comment>The method <b>add_algo</b> adds an algorithm item to the sequence.</comment>
      ELSEIF (IMTH .EQ. 'add_algo') THEN
D        CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))

C---        LIS LES PARAM
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Handle on the element</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HELE)
C        <comment>Handle on the resolution algorithm or strategy</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HALG)
         IF (IERR .NE. 0) GOTO 9901
C        <comment>Handle on the resolution trigger (default 0) (Status: Experimental)</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HTRG)
         IF (IERR .NE. 0) HTRG = 0

         IERR = AL_SQNC_ADDALGO(HOBJ, HELE, HALG, HTRG)

C     <comment>The method <b>add_post</b> adds a post-treatment item to the sequence.</comment>
      ELSEIF (IMTH .EQ. 'add_post') THEN
D        CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))

C---        LIS LES PARAM
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Handle on the element</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HELE)
C        <comment>Handle on the post-treatment</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HPST)
         IF (IERR .NE. 0) GOTO 9901
C        <comment>Handle on the trigger (default 0)</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HTRG)
         IF (IERR .NE. 0) HTRG = 0

         IERR = AL_SQNC_ADDPOST(HOBJ, HELE, HPST, HTRG)

C     <comment>The method <b>add_item</b> adds an item to the sequence.</comment>
      ELSEIF (IMTH .EQ. 'add_item') THEN
D        CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))

C---        LIS LES PARAM
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Handle on the sequence item</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HITM)
         IF (IERR .NE. 0) GOTO 9901

         IERR = AL_SQNC_ADDITEM(HOBJ, HITM)

C     <comment>The method <b>solve</b> solves the sequence.</comment>
      ELSEIF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQS@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_SQNC_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_ALSQNC_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_ALSQNC_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALSQNC_AID()

9999  CONTINUE
      IC_ALSQNC_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQNC_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQNC_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqnc_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>algo_sequence</b> represents a sequence of simulations to be
C  solved one after the other. For a time stepping algorithm, this amount
C  to a time splitting scheme.
C</comment>
      IC_ALSQNC_REQCLS = 'algo_sequence'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQNC_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQNC_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqnc_ic.fi'
      INCLUDE 'alsqnc.fi'
C-------------------------------------------------------------------------

      IC_ALSQNC_REQHDL = AL_SQNC_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALSQNC_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('alsqnc_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQNC_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqnc_ic.fi'
      INCLUDE 'alsqnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alsqnc_ic.fc'

      INTEGER IERR
      INTEGER IVAL
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_SEQUENCE'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IVAL = AL_SQNC_REQDIM(HOBJ)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_ITEM#<35>#', '= ', IVAL
      CALL LOG_ECRIS(LOG_BUF)

!!!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_SQNC_HNUMR(IOB))
!!!      LTXT = SP_STRN_LEN(TXT)
!!!      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_SQNC_HGRID(IOB))
!!!      LTXT = SP_STRN_LEN(TXT)
!!!      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_SQNC_HDLIB(IOB))
!!!      LTXT = SP_STRN_LEN(TXT)
!!!      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_SQNC_HCLIM(IOB))
!!!      LTXT = SP_STRN_LEN(TXT)
!!!      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_SQNC_HSOLC(IOB))
!!!      LTXT = SP_STRN_LEN(TXT)
!!!      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_SQNC_HSOLR(IOB))
!!!      LTXT = SP_STRN_LEN(TXT)
!!!      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_SQNC_HPRGL(IOB))
!!!      LTXT = SP_STRN_LEN(TXT)
!!!      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_SQNC_HPRNO(IOB))
!!!      LTXT = SP_STRN_LEN(TXT)
!!!      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_SQNC_HPREV(IOB))
!!!      LTXT = SP_STRN_LEN(TXT)
!!!      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
!!!      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_ALSQNC_PRN = ERR_TYP()
      RETURN
      END
