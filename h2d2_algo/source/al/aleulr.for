C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  ALgorithme
C Objet:   EULeR
C Type:    Concret
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_EULR
C         SUBROUTINE AL_EULR_000
C         SUBROUTINE AL_EULR_999
C         SUBROUTINE AL_EULR_CTR
C         SUBROUTINE AL_EULR_DTR
C         SUBROUTINE AL_EULR_INI
C         SUBROUTINE AL_EULR_RST
C         SUBROUTINE AL_EULR_REQHBASE
C         SUBROUTINE AL_EULR_HVALIDE
C         SUBROUTINE AL_EULR_RESOUS
C         SUBROUTINE AL_EULR_RESOUS_E
C         SUBROUTINE AL_EULR_UN_PAS
C         SUBROUTINE AL_EULR_REQSTATUS
C         SUBROUTINE AL_EULR_ASMMDTK
C         SUBROUTINE AL_EULR_ASMMDTKT
C         SUBROUTINE AL_EULR_ASMMDTKU
C         SUBROUTINE AL_EULR_ASMF_T0
C         SUBROUTINE AL_EULR_ASMF_T1
C         SUBROUTINE AL_EULR_ASMF
C         SUBROUTINE AL_EULR_ASMRES
C         SUBROUTINE AL_EULR_ASGALFA
C         SUBROUTINE AL_EULR_REQALFA
C         SUBROUTINE AL_EULR_ASGDELT
C         SUBROUTINE AL_EULR_REQDELT
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_EULR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_EULR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'aleulr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_EULR_NOBJMAX,
     &                   AL_EULR_HBASE,
     &                   'Algorithm Euler')

      AL_EULR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_EULR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'aleulr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'aleulr.fc'

      INTEGER  IERR
      EXTERNAL AL_EULR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_EULR_NOBJMAX,
     &                   AL_EULR_HBASE,
     &                   AL_EULR_DTR)

      AL_EULR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'aleulr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_EULR_NOBJMAX,
     &                   AL_EULR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_EULR_HVALIDE(HOBJ))
         IOB = HOBJ - AL_EULR_HBASE

         AL_EULR_ALFA(IOB) = 0.0D0
         AL_EULR_DTAL(IOB) = 0.0D0
         AL_EULR_DTEF(IOB) = 0.0D0
         AL_EULR_HRES(IOB) = 0
         AL_EULR_HSIM(IOB) = 0
         AL_EULR_HCTI(IOB) = 0
         AL_EULR_HPJC(IOB) = 0
         AL_EULR_LFT0(IOB) = 0
         AL_EULR_LTMP(IOB) = 0
         AL_EULR_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_EULR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'aleulr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'aleulr.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_EULR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_EULR_NOBJMAX,
     &                   AL_EULR_HBASE)

      AL_EULR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HRES  Handle sur l'algo de résolution
C     HCTI  Handle sur le contrôleur d'incrément
C     HPJC  Handle sur la projection
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_INI(HOBJ, HRES, DELT, ALFA, HCTI, HPJC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HRES
      REAL*8  DELT
      REAL*8  ALFA
      INTEGER HCTI
      INTEGER HPJC

      INCLUDE 'aleulr.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pjprjc.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (.NOT. (HCTI .EQ. 0 .OR. CI_CINC_HVALIDE(HCTI))) GOTO 9900
      IF (.NOT. (HPJC .EQ. 0 .OR. PJ_PRJC_HVALIDE(HPJC))) GOTO 9901
      IF (.NOT. AL_SLVR_HVALIDE(HRES)) GOTO 9902
      IF (DELT .LE. 0.0D0) GOTO 9903
      IF (ALFA .LT. 0.0D0) GOTO 9904
      IF (ALFA .GT. 1.0D0) GOTO 9904

C---     Reset les données
      IERR = AL_EULR_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - AL_EULR_HBASE
         AL_EULR_ALFA(IOB) = MAX(MIN(ALFA, 1.0D0), 0.0D0)
         AL_EULR_DTAL(IOB) = DELT
         AL_EULR_DTEF(IOB) = 0.0D0
         AL_EULR_HRES(IOB) = HRES
         AL_EULR_HSIM(IOB) = 0
         AL_EULR_HCTI(IOB) = HCTI
         AL_EULR_HPJC(IOB) = HPJC
         AL_EULR_LFT0(IOB) = 0
         AL_EULR_LTMP(IOB) = 0
         AL_EULR_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HPJC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HPJC, 'MSG_PROJECTION_DDL')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HCTI
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HCTI, 'MSG_CTRL_INCREMENT')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HRES
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRES, 'MSG_SOLVER')
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_DELT_INVALIDE',': ',DELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_ALFA_INVALIDE',': ',ALFA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_EULR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'aleulr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LFT0, LTMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_EULR_HBASE
      LFT0 = AL_EULR_LFT0(IOB)
      LTMP = AL_EULR_LTMP(IOB)

C---     Récupère la mémoire
      IF (SO_ALLC_HEXIST(LTMP)) IERR = SO_ALLC_ALLRE8(0, LTMP)
      IF (SO_ALLC_HEXIST(LFT0)) IERR = SO_ALLC_ALLRE8(0, LFT0)

C---     Reset
      AL_EULR_ALFA(IOB) = 0.0D0
      AL_EULR_DTAL(IOB) = 0.0D0
      AL_EULR_DTEF(IOB) = 0.0D0
      AL_EULR_HRES(IOB) = 0
      AL_EULR_HSIM(IOB) = 0
      AL_EULR_HCTI(IOB) = 0
      AL_EULR_HPJC(IOB) = 0
      AL_EULR_LFT0(IOB) = 0
      AL_EULR_LTMP(IOB) = 0
      AL_EULR_STTS(IOB) = AL_STTS_INDEFINI

      AL_EULR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_EULR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_EULR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'aleulr.fi'
      INCLUDE 'aleulr.fc'
C------------------------------------------------------------------------

      AL_EULR_REQHBASE = AL_EULR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_EULR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_EULR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'aleulr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'aleulr.fc'
C------------------------------------------------------------------------

      AL_EULR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_EULR_NOBJMAX,
     &                                  AL_EULR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_EULR_RESOUS intègre dans le temps par une méthode d'Euler
C     la simulation HSIM. Globalement, elle amène HSIM de TSIM à TSIM+DELT.
C     En sortie, TSIM et DELT sont ajustés à la partie calculée.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C    2) En sortie, les DLL sont à t+dt, mais les prop ne sont pas toutes
C       ajustées à la dernière valeur des DDL. En fait elles sont une
C       itération en retard.
C************************************************************************
      FUNCTION AL_EULR_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN)     :: HOBJ
      REAL*8,  INTENT(INOUT)  :: TSIM
      REAL*8,  INTENT(INOUT)  :: DELT
      INTEGER, INTENT(IN)     :: HSIM

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR

      EXTERNAL AL_EULR_ASMMDTK
      EXTERNAL AL_EULR_ASMMDTKT
      EXTERNAL AL_EULR_ASMMDTKU
      EXTERNAL AL_EULR_ASMF
      EXTERNAL AL_EULR_ASMRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)    ! cf. note
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)

C---     Résous
      IF (ERR_GOOD())
     &   IERR = AL_EULR_RESOUS_E(HOBJ,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           HOBJ,
     &                           AL_EULR_ASMMDTK,
     &                           AL_EULR_ASMMDTKT,
     &                           AL_EULR_ASMMDTKU,
     &                           AL_EULR_ASMF,
     &                           AL_EULR_ASMRES)

C---     Post-calcul des données (TSIM mis à jour)
      IF (ERR_GOOD()) IERR = LM_ELEM_PASFIN(HSIM, TSIM)

      AL_EULR_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_EULR_RESOUS_E(...) est la méthode de bas niveau pour
C     résoudre une simulation par un algorithme d'Euler.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo. C'est la fonction appelante qui dois gérer le
C     chargement des données.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM         Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_MDTK      Fonction de calcul de [M.dt + a.K]
C     EXTERNAL F_MDTKT     Fonction de calcul de [M.dt + a.KT]
C     EXTERNAL F_MDTKU     Fonction de calcul de [M.dt + a.K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C     En sortie, les DLL sont à t+dt, mais les prop ne sont pas toutes
C     ajustées à la dernière valeur des DDL. En fait elles sont une
C     itération en retard.
C************************************************************************
      FUNCTION AL_EULR_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          HALG,
     &                          F_MDTK,
     &                          F_MDTKT,
     &                          F_MDTKU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN)     :: HOBJ
      REAL*8,  INTENT(INOUT)  :: TSIM
      REAL*8,  INTENT(INOUT)  :: DELT
      INTEGER, INTENT(IN)     :: HSIM
      INTEGER, INTENT(IN)     :: HALG
      INTEGER  F_MDTK
      INTEGER  F_MDTKT
      INTEGER  F_MDTKU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_MDTK
      EXTERNAL F_MDTKT
      EXTERNAL F_MDTKU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'aleulr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'alutil.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'pjprjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'aleulr.fc'

      REAL*8  ALFA
      REAL*8  DTNM, DTPRV, DTNOW, DTNXT, DTRST, TINI, TFIN
      REAL*8  TS_C, DT_C

      INTEGER IERR
      INTEGER IOB
      INTEGER IPAS, ISTG, ISTC
      INTEGER HCTI
      INTEGER HNMR, HRESI
      INTEGER HSVPT
      INTEGER NEQL, NDLN, NNL, NDLL
      INTEGER NSTG
      INTEGER LFT0, LTMP
      INTEGER LDLG, LSPT, LLOCN
      LOGICAL ACTI, ESTOK, ESTRES, ESTRJT, ESTXIT, PUSHT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.solver.euler'

C---     Récupère les attributs
      IOB = HOBJ - AL_EULR_HBASE
      ALFA = AL_EULR_ALFA(IOB)
      HCTI = AL_EULR_HCTI(IOB)
      LFT0 = AL_EULR_LFT0(IOB)
      LTMP = AL_EULR_LTMP(IOB)
D     CALL ERR_ASR(LFT0 .EQ. 0 .OR. SO_ALLC_HEXIST(LFT0))
D     CALL ERR_ASR(LTMP .EQ. 0 .OR. SO_ALLC_HEXIST(LTMP))

C---     Récupère les paramètres de la simulation
      HNMR = LM_ELEM_REQHNUMC(HSIM)
      LLOCN= LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)
      LDLG = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      NDLN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NNL  = LM_ELEM_REQPRM(HSIM, LM_GEOM_PRM_NNL)

C---     Présence/absence des composantes
      ACTI = CI_CINC_HVALIDE(HCTI)

C---     Calcule les paramètres
      DTNM  = AL_EULR_DTAL(IOB)
      IF (HSIM .EQ. AL_EULR_HSIM(IOB)) THEN
         DTNXT = MIN(AL_EULR_DTEF(IOB), DELT)
      ELSE
         DTNXT = MIN(AL_EULR_DTAL(IOB), DELT)
      ENDIF
      DTNOW = DTNXT
      TINI = TSIM
      TFIN = TSIM + DELT

C---     Écris l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_EULER_SOLVE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TSIM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TFIN#<35>#', '= ', TFIN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_DELT_NOMINAL#<35>#', '= ', DTNM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_ALFA#<35>#', '= ', ALFA
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     (Ré)-alloue les tables de travail
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LFT0) ! Utilisées en
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LTMP) ! NEQ et NDLL

C---     Conserve les paramètres pour la simulation
      AL_EULR_HSIM(IOB) = HSIM
      AL_EULR_LFT0(IOB) = LFT0
      AL_EULR_LTMP(IOB) = LTMP

C---     Crée un point de sauvegarde
      HSVPT = 0
      LSPT = 0
      IF (ERR_GOOD()) IERR = SO_SVPT_CTR(HSVPT)
      IF (ERR_GOOD()) IERR = SO_SVPT_INI(HSVPT, LDLG)
      IF (ERR_GOOD()) LSPT = SO_SVPT_GET(HSVPT)

C---     Initialise le contrôleur d'incrément
      IF (ERR_GOOD() .AND. ACTI)
     &   IERR = CI_CINC_DEB(HCTI, TINI, TFIN, DTNOW)

C---     CB pour la projection
      HRESI = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR(HRESI)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HRESI, HOBJ, F_RES)


C        --------------------------------
C---     Boucle sur les sous-pas de temps
C        --------------------------------
      ISTG = AL_STTS_OK
      IPAS = 0
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         IF (DTNOW .LE. 0.0D0) GOTO 199
         IF (TINI  .GE. TFIN)  GOTO 199

C---        Log
         IPAS = IPAS + 1
         CALL LOG_INFO(LOG_ZNE, ' ')
         WRITE(LOG_BUF, '(A,I6,3(A,F20.6))',ERR=109)
     &      'MSG_PAS_DE_TEMPS:', IPAS,
     &      ', MSG_TINI=', TINI,
     &      ', MSG_TFIN=', TINI+DTNOW,
     &      ', MSG_DELT=', DTNOW
109      CONTINUE
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         CALL LOG_INCIND()

C---        Traceur
         IERR = TRA_ASGTALG(TINI)

C---        Sauve l'état courant
         IF (ERR_GOOD() .AND. HSVPT .NE. 0) THEN
            IERR = SO_SVPT_SAVE(HSVPT)
         ENDIF

C---        Résous
         TS_C = TINI
         DT_C = DTNOW
         IF (ERR_GOOD()) THEN
            IERR = AL_EULR_UN_PAS(HOBJ,
     &                            TS_C,
     &                            DT_C,
     &                            HSIM,
     &                            HALG,
     &                            F_MDTK,
     &                            F_MDTKT,
     &                            F_MDTKU,
     &                            F_F,
     &                            F_RES, HRESI)
         ENDIF
         IF (ERR_BAD()) GOTO 199
         CALL LOG_INFO(LOG_ZNE, ' ')

C---        Delta sur le pas de temps dans LFT0
         CALL DCOPY(NDLL,
     &              VA(SO_ALLC_REQVIND(VA,LDLG)), 1,
     &              VA(SO_ALLC_REQVIND(VA,LFT0)), 1)
         CALL DAXPY(NDLL,
     &              -1.0D0,
     &               VA(SO_ALLC_REQVIND(VA,LSPT)), 1,
     &               VA(SO_ALLC_REQVIND(VA,LFT0)), 1)
         IERR = SP_ELEM_CLIM2VAL(NDLL,
     &                           KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                           0.0D0,
     &                           VA(SO_ALLC_REQVIND(VA, LFT0)))

C---        L'état de résolution
         ISTG = AL_EULR_REQSTATUS(HOBJ)

C---        Adapte le pas de temps
         DTPRV = DTNOW
         IF (ACTI) THEN
D           CALL ERR_ASR(SO_ALLC_HEXIST(LSPT))
            ESTOK = (ISTG .EQ. AL_STTS_OK)
            IERR = CI_CINC_CLCDEL(HCTI,
     &                            NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                            VA(SO_ALLC_REQVIND(VA,LFT0)), ! delU
     &                            ESTOK)
            IERR = CI_CINC_REQDELT(HCTI, ISTC, DTNXT, DTNOW)
            ISTG = AL_UTIL_CLRSTTS(ISTG)
            ISTG = AL_UTIL_IORSTTS(ISTG, ISTC)
         ELSE
            DTRST = TFIN - (TINI+DTPRV)
            DTNOW = MIN(DTPRV, DTRST)
            DTRST = DTRST - DTNOW
            IF (DTRST .GT. 0.0D0 .AND. DTRST < (0.05D0*DTPRV)) THEN
               DTNOW = DTNOW + DTRST
            ENDIF
         ENDIF

C---        L'état de résolution
         ESTOK  = (ISTG .EQ. AL_STTS_OK)
         ESTRES = .NOT. BTEST(ISTG, AL_STTS_CRIA_BAD)
         ESTRJT = .FALSE.
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_COUPE)
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_DTMIN)
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_STOP)
         ESTXIT = .FALSE.
         ESTXIT = ESTXIT .OR. BTEST(ISTG, AL_STTS_CINC_DTMIN)
         ESTXIT = ESTXIT .OR. BTEST(ISTG, AL_STTS_CINC_STOP)

C---        Logique de continuation
         PUSHT = .FALSE.
         IF (ESTRES) THEN
            PUSHT = .TRUE.
D           CALL ERR_ASR(ERR_BAD() .OR. DT_C .EQ. 0.0D0)
         ELSE
            IF (ACTI) THEN                ! !OK, Avec contrôleur d'incrément
               IERR = SO_SVPT_RBCK(HSVPT) !  rollback. On pert possiblement
               PUSHT = .FALSE.            !  une partie déjà convergée
            ELSE
               PUSHT = .TRUE.             ! !OK, Sans contrôleur d'incrément
            ENDIF                         !   continue
         ENDIF
         IF (PUSHT) TINI = TINI + DTPRV

C---        Log l'incrément sur le pas de temps
         IF (ERR_GOOD()) THEN
            IERR = AL_UTIL_LOGSOL(NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,LFT0)),
     &                            HNMR)
         ENDIF

C---        Log l'état
         IF (ERR_GOOD()) THEN
            IF (ACTI) THEN
               IF (BTEST(ISTG, AL_STTS_CINC_DTMIN))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_DTMIN')
               IF (BTEST(ISTG, AL_STTS_CINC_SKIP))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_SKIP')
               IF (BTEST(ISTG, AL_STTS_CINC_STOP))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_STOP')
               IF (.NOT. ESTRES)
     &         CALL LOG_INFO(LOG_ZNE, 'MSG_PAS_REJETE')
            ELSE
               IF (.NOT. ESTRES) THEN
                  CALL LOG_WRN(LOG_ZNE, 'MSG_PAS_NON_CONVERGE')
                  CALL LOG_WRN(LOG_ZNE, 'MSG_CONTINUE')
               ENDIF
            ENDIF
         ENDIF

         CALL LOG_DECIND()
         IF (ESTXIT) GOTO 199
      GOTO 100 ! end while
199   CONTINUE

C---     Ajuste TSIM et DELT à la partie calculée
      IF (ERR_GOOD()) THEN
         DELT = (TSIM+DELT) - TINI
         TSIM = TINI
      ENDIF

C---     Conserve la prévision suite au dernier pas de temps
      IF (ERR_GOOD()) THEN
         AL_EULR_DTEF(IOB) = DTNXT
      ENDIF

C---     Conserve l'état
      AL_EULR_STTS(IOB) = ISTG

C---     Relâche le point de sauvegarde
      IF (HSVPT .NE. 0) THEN
         CALL ERR_PUSH()
         IERR = SO_SVPT_DTR(HSVPT)
         CALL ERR_POP()
      ENDIF

C---     CB pour la projection
      IF (SO_FUNC_HVALIDE(HRESI)) THEN
         CALL ERR_PUSH()
         IERR = SO_FUNC_DTR(HRESI)
         CALL ERR_POP()
      ENDIF

      CALL LOG_DECIND()
      AL_EULR_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_EULR_UN_PAS intègre dans le temps par une méthode d'Euler
C     la simulation HSIM. Globalement, elle amène HSIM de TSIM à TSIM+DELT.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C     INTEGER HCRA         Handle sur le critère d'arrêt
C     INTEGER HLTR         Handle sur le limiteur
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C    2) En sortie, les DLL sont à t+dt, mais les prop ne sont pas toutes
C       ajustées à la dernière valeur des DDL. En fait elles sont une
C       itération en retard.
C************************************************************************
      FUNCTION AL_EULR_UN_PAS(HOBJ,
     &                        TSIM,
     &                        DTEF,
     &                        HSIM,
     &                        HALG,
     &                        F_MDTK,
     &                        F_MDTKT,
     &                        F_MDTKU,
     &                        F_F,
     &                        F_RES,
     &                        HFRES)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DTEF
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_MDTK
      INTEGER  F_MDTKT
      INTEGER  F_MDTKU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_MDTK
      EXTERNAL F_MDTKT
      EXTERNAL F_MDTKU
      EXTERNAL F_F
      EXTERNAL F_RES
      INTEGER  HFRES

      INCLUDE 'aleulr.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'pjprjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HRES, HPJC
      INTEGER LDLG
      INTEGER NDLL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_EULR_HBASE
      HRES = AL_EULR_HRES(IOB)
      HPJC = AL_EULR_HPJC(IOB)

C---     Récupère les paramètres de la simulation
      LDLG = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)

C---     Conserve les paramètres pour la simulation
      AL_EULR_DTEF(IOB) = DTEF

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC   (HSIM)

C---     Partie de F à T
      IF (ERR_GOOD()) IERR = AL_EULR_ASMF_T0(HOBJ)

C---     Ajoute la solution initiale à la projection
      IF (ERR_GOOD() .AND. HPJC .NE. 0) THEN
         IERR = PJ_PRJC_AJT(HPJC,
     &                      HSIM,
     &                      TSIM,
     &                      VA(SO_ALLC_REQVIND(VA,LDLG)))
      ENDIF

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM+DTEF)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC   (HSIM)

C---     Partie de F à T+dt
      IF (ERR_GOOD()) IERR = AL_EULR_ASMF_T1(HOBJ)

C---        Projette
      IF (ERR_GOOD() .AND. HPJC .NE. 0) THEN
         IERR = PJ_PRJC_PRJ(HPJC,
     &                      HSIM,
     &                      HFRES,
     &                      TSIM+DTEF,
     &                      VA(SO_ALLC_REQVIND(VA,LDLG)))
      ENDIF

C---     Résous
      IF (ERR_GOOD()) THEN
         IERR = AL_SLVR_RESOUS_E(HRES,
     &                           TSIM,
     &                           DTEF,
     &                           HSIM,
     &                           HALG,
     &                           F_MDTK,
     &                           F_MDTKT,
     &                           F_MDTKU,
     &                           F_F,
     &                           F_RES)
      ENDIF

C---     Conserve le résultat
      IF (ERR_GOOD()) THEN
      AL_EULR_STTS(IOB) = AL_SLVR_REQSTATUS(HRES)
      ENDIF

      AL_EULR_UN_PAS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'aleulr.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_EULR_REQSTATUS = AL_EULR_STTS(HOBJ - AL_EULR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_ASMMDTK(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'mxmtrx.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      REAL*8  DELT
      REAL*8  ALFA
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_EULR_HBASE
      DELT = AL_EULR_DTEF(IOB)
      ALFA = AL_EULR_ALFA(IOB)
      HSIM = AL_EULR_HSIM(IOB)

C---     ASSEMBLE K
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMK  (HSIM, HMTX, F_ASM)

C---     ALFA*DELT * K
      IF (ERR_GOOD()) IERR = MX_MTRX_MULVAL(HMTX, ALFA*DELT)

C---     ASSEMBLE M
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMM  (HSIM, HMTX, F_ASM)

      AL_EULR_ASMMDTK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_ASMMDTKT(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'mxmtrx.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      REAL*8  DELT
      REAL*8  ALFA
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_EULR_HBASE
      DELT = AL_EULR_DTEF(IOB)
      ALFA = AL_EULR_ALFA(IOB)
      HSIM = AL_EULR_HSIM(IOB)

C---     ASSEMBLE Kt
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMKT (HSIM, HMTX, F_ASM)

C---     ALFA*DELT*Kt
      IF (ERR_GOOD()) IERR = MX_MTRX_MULVAL(HMTX, ALFA*DELT)

C---     ASSEMBLE M
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMM  (HSIM, HMTX, F_ASM)

      AL_EULR_ASMMDTKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_ASMMDTKU(HOBJ, VDLG, VRES)

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VDLG(*)
      REAL*8  VRES(*)

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'aleulr.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_ECRIS('--------------------------------------------')
      CALL LOG_ECRIS('---  Fonction AL_EULR_ASMMDTKU non implantée')
      CALL LOG_ECRIS('--------------------------------------------')
      CALL ERR_ASR(.FALSE.)

      AL_EULR_ASMMDTKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: AL_EULR_ASMF_T0
C
C Description:
C     La méthode privée AL_EULR_ASMF_T0 assemble la partie à T
C     du membre de droite.
C     On assemble en NDLL car NEQ peut être modifié entre T et T+dT (CD2D)
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_ASMF_T0(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM, HDLIB
      INTEGER LFT0, LTMP
      INTEGER NEQL, NDLL, LLOCN
      REAL*8  DELT
      REAL*8  ALFA, UN_ALFA
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_EULR_HBASE
      HSIM = AL_EULR_HSIM(IOB)
      LFT0 = AL_EULR_LFT0(IOB)
      LTMP = AL_EULR_LTMP(IOB)
      DELT = AL_EULR_DTEF(IOB)
      ALFA = AL_EULR_ALFA(IOB)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LFT0))
D     CALL ERR_ASR(SO_ALLC_HEXIST(LTMP))

C---     Variables
      NEQL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      LLOCN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)
      UN_ALFA = 1.0D0 - ALFA

C---     Assemble (F - KU) dans LFT0
      IF (ERR_GOOD()) THEN
         IF (UN_ALFA .LE. 0.0D0) THEN
            CALL DINIT(NEQL, 0.0D0, VA(SO_ALLC_REQVIND(VA,LFT0)), 1)
         ELSE
            IERR = LM_ELEM_ASMFKU(HSIM, VA(SO_ALLC_REQVIND(VA,LFT0)))
         ENDIF
      ENDIF

C---     Assemble MU dans LTMP
      IF (ERR_GOOD())
     &   IERR = LM_ELEM_ASMMU(HSIM, VA(SO_ALLC_REQVIND(VA,LTMP)))

C---     DELT*(1-A)*(F - KU) + MU dans LTMP
      IF (ERR_GOOD())
     &   CALL DAXPY(NEQL,
     &              DELT*UN_ALFA,
     &              VA(SO_ALLC_REQVIND(VA,LFT0)),
     &              1,
     &              VA(SO_ALLC_REQVIND(VA,LTMP)),
     &              1)

C---     Passe en NDLL
      IF (ERR_GOOD()) THEN
         CALL DINIT(NDLL,
     &              0.0D0,
     &              VA(SO_ALLC_REQVIND(VA,LFT0)),
     &              1)
         IERR = SP_ELEM_NEQ2NDLT(NEQL,
     &                           NDLL,
     &                           KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                           VA(SO_ALLC_REQVIND(VA, LTMP)),
     &                           VA(SO_ALLC_REQVIND(VA, LFT0)))
      ENDIF

      AL_EULR_ASMF_T0 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: AL_EULR_ASMF_T1
C
C Description:
C     La méthode privée AL_EULR_ASMF_T1 ajoute à la partie pré-calculée
C     par AL_EULR_ASMF_T0 la partie à T+DT.
C     On repasse en NEQ car on est à T+dT
C
C Entrée:
C
C Sortie:
C
C Notes:
C  L'initialisation de LFT0 est indispensable car en distribué,
C  il peut contenir des trous.
C************************************************************************
      FUNCTION AL_EULR_ASMF_T1(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM, HDLIB
      INTEGER LFT0, LTMP
      INTEGER NEQL, NDLL, LLOCN
      REAL*8  DELT
      REAL*8  ALFA
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_EULR_HBASE
      HSIM = AL_EULR_HSIM(IOB)
      LFT0 = AL_EULR_LFT0(IOB)
      LTMP = AL_EULR_LTMP(IOB)
      DELT = AL_EULR_DTEF(IOB)
      ALFA = AL_EULR_ALFA(IOB)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LFT0))
D     CALL ERR_ASR(SO_ALLC_HEXIST(LTMP))

C---     Récupère les données
      NEQL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      LLOCN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)

C---     Passe LFT0 en NEQL
      IF (ERR_GOOD()) THEN
         CALL DCOPY(NDLL, VA(SO_ALLC_REQVIND(VA,LFT0)), 1,
     &                    VA(SO_ALLC_REQVIND(VA,LTMP)), 1)
         CALL DINIT(NDLL,
     &              0.0D0,
     &              VA(SO_ALLC_REQVIND(VA,LFT0)),
     &              1)
         IERR = SP_ELEM_NDLT2NEQ(NEQL,
     &                           NDLL,
     &                           KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                           VA(SO_ALLC_REQVIND(VA, LTMP)),
     &                           VA(SO_ALLC_REQVIND(VA, LFT0)))
      ENDIF

      AL_EULR_ASMF_T1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_ASMF(HOBJ, VRES, VDLG, ADLG)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VRES(*)
      REAL*8   VDLG(*)
      LOGICAL  ADLG

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IOB
      INTEGER HSIM
      INTEGER LFT0
      INTEGER NEQL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
D     CALL ERR_PRE(.NOT. ADLG)
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_EULR_HBASE
      HSIM = AL_EULR_HSIM(IOB)
      LFT0 = AL_EULR_LFT0(IOB)

C---     RECUPERE LES DONNEES
      NEQL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)

C---     TRANSFERT
      CALL DCOPY(NEQL, VA(SO_ALLC_REQVIND(VA,LFT0)), 1, VRES, 1)

      AL_EULR_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_ASMRES(HOBJ, VRES, VDLG, ADLG)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VRES(*)
      REAL*8   VDLG(*)
      LOGICAL  ADLG

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      INTEGER LTMP
      INTEGER NEQL
      REAL*8  DUMMY(1)
      REAL*8  DELT
      REAL*8  ALFA
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_EULR_HBASE
      HSIM = AL_EULR_HSIM(IOB)
      LTMP = AL_EULR_LTMP(IOB)
      DELT = AL_EULR_DTEF(IOB)
      ALFA = AL_EULR_ALFA(IOB)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LTMP))

C---     Variables
      NEQL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)

C---     Assigne VDLG à la simulation
      IF (ADLG) THEN
         IERR = LM_ELEM_PUSHLDLG(HSIM, SO_ALLC_REQVHND(VDLG))
      ENDIF

C---     Ft = DELT*(1-A)*(F(T) - K.U(T)) + M.U(T)
      IF (ERR_GOOD()) THEN
         IERR = AL_EULR_ASMF(HOBJ, VRES, DUMMY, .FALSE.) ! Pas de VDLG passé
      ENDIF

C---     Assemble M.U(T+dT)
      IF (ERR_GOOD()) THEN
         IERR = LM_ELEM_ASMMU(HSIM, VA(SO_ALLC_REQVIND(VA,LTMP)))
      ENDIF

C---     Ft - M.U(T+dT)
      IF (ERR_GOOD()) THEN
         CALL DAXPY(NEQL,
     &              -1.0D0,
     &              VA(SO_ALLC_REQVIND(VA,LTMP)),
     &              1,
     &              VRES,
     &              1)
      ENDIF

C---     Assemble (F - KU(T+dT)) dans LTMP
      IF (ERR_GOOD()) THEN
         IERR = LM_ELEM_ASMFKU(HSIM, VA(SO_ALLC_REQVIND(VA,LTMP)))
      ENDIF

C---     Ft + DELT*A*KU(T+dT) - M.U(T+dT)
      IF (ERR_GOOD()) THEN
         CALL DAXPY(NEQL,
     &              DELT*ALFA,
     &              VA(SO_ALLC_REQVIND(VA,LTMP)),
     &              1,
     &              VRES,
     &              1)
      ENDIF

C---     Rétablis la simulation
      IF (ADLG) THEN
         CALL ERR_PUSH()
         IERR = LM_ELEM_POPLDLG(HSIM)
         CALL ERR_POP()
      ENDIF

      AL_EULR_ASMRES = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_EULR_ASGALFA(HOBJ, ALFA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_ASGALFA
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   ALFA

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (ALFA .LT. 0.0D0) GOTO 9900
      IF (ALFA .GT. 1.0D0) GOTO 9900

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - AL_EULR_HBASE
      AL_EULR_ALFA(IOB) = MAX(MIN(ALFA, 1.0D0), 0.0D0)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_ALFA_INVALIDE',': ',ALFA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_EULR_ASGALFA = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_EULR_REQALFA(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_REQALFA
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'aleulr.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_EULR_REQALFA = AL_EULR_ALFA(HOBJ-AL_EULR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_EULR_ASGDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_ASGDELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'aleulr.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (DELT .LE. 0.0D0) GOTO 9900

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - AL_EULR_HBASE
      AL_EULR_DTAL(IOB) = DELT

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_DELT_INVALIDE',': ',DELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_EULR_ASGDELT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_EULR_REQDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_EULR_REQDELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'aleulr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'aleulr.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DELT = AL_EULR_DTAL(HOBJ-AL_EULR_HBASE)

      AL_EULR_REQDELT = ERR_TYP()
      RETURN
      END
