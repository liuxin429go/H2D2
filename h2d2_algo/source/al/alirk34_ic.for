C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_AL_IRK34_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_AL_IRK34_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'alirk34_ic.fi'
      INCLUDE 'alirk34.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alirk34_ic.fc'

      INTEGER IERR
      INTEGER HOBJ, HALG
      INTEGER HRES
      INTEGER HCTI
      REAL*8  DELT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_AL_IRK34_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      HRES = 0
      DELT = 0.0D0
      HCTI = 0
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the resolution algorithm</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HRES)
C     <comment>Nominal time step</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, DELT)
C     <comment>Handle on the increment controller (default 0)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HCTI)
      IF (IERR .NE. 0) HCTI = 0

C---     CONSTRUIS ET INITIALISE L'OBJET
      HALG = 0
      IF (ERR_GOOD()) IERR = AL_IRK34_CTR(HALG)
      IF (ERR_GOOD()) IERR = AL_IRK34_INI(HALG, HRES, DELT, HCTI)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SLVR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SLVR_INI(HOBJ, HALG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_AL_IRK34_PRN(HALG)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HALG
      ENDIF

C<comment>
C  The constructor <b>esdirk34</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_AL_IRK34_AID()

9999  CONTINUE
      IC_AL_IRK34_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_AL_IRK34_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'alirk34.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alirk34.fc'
      INCLUDE 'alirk34_ic.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LTXT
      REAL*8  ALFA
      REAL*8  DELT
      CHARACTER*(256) TXT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRK34_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CMD_ALGORITHME_ESDIRK34'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      IOB = HOBJ - AL_IRK34_HBASE

C---     IMPRESSION DES PARAMETRES DE L'OBJET
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)
!      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_IRK34_HRES(IOB))
!      LTXT = SP_STRN_LEN(TXT)
!      WRITE(LOG_BUF,'(3A)') 'MSG_RESO_MATRICIEL#<35>#', '= ', TXT(1:LTXT)
!      CALL LOG_ECRIS(LOG_BUF)
!      ALFA = AL_IRK34_ALFA(IOB)
!      WRITE(LOG_BUF,'(A,F4.1)') 'MSG_EULER_ALFA#<35>#= ', ALFA
!      CALL LOG_ECRIS(LOG_BUF)
!      DELT = AL_IRK34_DTAL(IOB)
!      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_PAS_DE_TEMPS#<35>#= ', DELT
!      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_AL_IRK34_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_AL_IRK34_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_AL_IRK34_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'alirk34_ic.fi'
      INCLUDE 'alirk34.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alirk34_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>solve</b> executes the algorithm.</comment>
      IF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_IRK34_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQA@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_IRK34_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_IRK34_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_IRK34_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_AL_IRK34_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_AL_IRK34_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_AL_IRK34_AID()

9999  CONTINUE
      IC_AL_IRK34_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_AL_IRK34_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_AL_IRK34_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alirk34_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>esdirk34</b> represents the Explicit Singly Diagonally
C  Implicit Runge-Kutta time integration algorithm of order 3-4.
C</comment>
      IC_AL_IRK34_REQCLS = 'esdirk34'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_AL_IRK34_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_AL_IRK34_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alirk34_ic.fi'
      INCLUDE 'alirk34.fi'
C-------------------------------------------------------------------------

      IC_AL_IRK34_REQHDL = AL_IRK34_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_AL_IRK34_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('alirk34_ic.hlp')
      RETURN
      END

