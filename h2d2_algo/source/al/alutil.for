C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  ALgorithme
C Objet:   Util
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Log de la mise à jour de la solution
C
C Description:
C     La fonction AL_UTIL_LOGSOL log les normes max et L2 de l'incrément
C     de solutions VDEL.
C
C Entrée:
C     NDLN     Nombre de Degrés de Libertés par Noeud
C     NNL      Nombre de noeuds locaux
C     VDEL     Table des corrections
C     HNUMR    La renumérotation des noeuds
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_UTIL_LOGSOL(NDLN, NNL, VDEL, HNUMR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_UTIL_LOGSOL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDEL(NDLN, NNL)
      INTEGER HNUMR

      INCLUDE 'alutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'

      REAL*8  DUL2_L, DUL2_G
      REAL*8  DUMX_L, DUMX_G

      INTEGER IERR
      INTEGER IN, ID
      INTEGER I_ERROR, I_OPSIGNEDMAX
      INTEGER IDMX_L, IDMX_G
      INTEGER IUMX_L, IUMX_G
      INTEGER NDLL, NDLP, NDLT
      INTEGER NOMX_L, NOMX_G

      INTEGER IDAMAX
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NDLN .GT. 0)
D     CALL ERR_PRE(NNL  .GT. 0)
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.reso.algo.util'

C---     Assemble
      NDLL  = NDLN * NNL
      NDLP = 0
      DUL2_L = 0.0D0
      DO IN=1,NNL
         IF (NM_NUMR_ESTNOPP(HNUMR, IN)) THEN
            DO ID=1,NDLN
               NDLP = NDLP + 1
               DUL2_L = DUL2_L + VDEL(ID,IN)*VDEL(ID,IN)
            ENDDO
         ENDIF
      ENDDO
      IUMX_L = IDAMAX(NDLL, VDEL, 1)            ! Pas 100% correct
      NOMX_L = (IUMX_L-1)/NDLN + 1
      IDMX_L = MOD(IUMX_L-1,NDLN) + 1
      DUMX_L = VDEL(IDMX_L,NOMX_L)
      NOMX_L = NM_NUMR_REQNEXT(HNUMR, NOMX_L)   ! En num. globale

C---     Log les infos locales
      WRITE(LOG_BUF, '(A,1PE14.6E3,A,I2,A,I7,A)')
     &      'MSG_DELTA_NORME_MAX_DU_PROCESS#<35>#= ', DUMX_L,
     &      ' ; MSG_DLIB: (', IDMX_L, ', ', NOMX_L, ')'
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,1PE14.6E3)')
     &      'MSG_DELTA_NORME_L2_DU_PROCESS#<35>#= ', SQRT(DUL2_L/NDLP)
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Demande l'op de reduction
      I_OPSIGNEDMAX = MP_UTIL_GETOP_SIGNEDMAX()

C---     Synchronise
      DUL2_G = 1.0D99
      DUMX_G = 1.0D99
      IDMX_G = -1
      IUMX_G = -1
      NOMX_G = -1
      NDLT = 1
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(DUL2_L, DUL2_G, 1, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(NDLP, NDLT, 1, MP_TYPE_INT(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(DUMX_L, DUMX_G, 1, MP_TYPE_RE8(),
     &                      I_OPSIGNEDMAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         IF (DUMX_G .EQ. DUMX_L) NOMX_L = NOMX_L + NDLT
         CALL MPI_ALLREDUCE(NOMX_L, NOMX_G, 1, MP_TYPE_INT(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
         NOMX_G = NOMX_G - NDLT
      ENDIF
      IF (ERR_GOOD()) THEN
         IF (DUMX_G .EQ. DUMX_L) IDMX_L = IDMX_L + NDLT
         CALL MPI_ALLREDUCE(IDMX_L, IDMX_G, 1, MP_TYPE_INT(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
         IDMX_G = IDMX_G - NDLT
      ENDIF

C---     Log les infos globales
      DUL2_G = SQRT(DUL2_G / NDLT)
      WRITE(LOG_BUF,'(A,1PE14.6E3,A,I2,A,I7,A)')
     &         'MSG_DELTA_NORME_ABS_MAX#<35>#= ', DUMX_G,
     &         ' ; DDL: (', IDMX_G, ', ', NOMX_G, ')'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_DELTA_NORME_L2#<35>#= ',DUL2_G
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      AL_UTIL_LOGSOL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: AL_UTIL_CLRSTTS
C
C Description:
C     La fonction AL_UTIL_CLRSTTS(...) 
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_UTIL_CLRSTTS(ISTT_AL)

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: ISTT_AL

      INCLUDE 'alutil.fi'
      INCLUDE 'alstts.fi'
      
      INTEGER IRET
C------------------------------------------------------------------------
      
      IRET = ISTT_AL
      IRET = IBCLR(IRET, AL_STTS_CINC_COUPE)
      IRET = IBCLR(IRET, AL_STTS_CINC_DTMIN)
      IRET = IBCLR(IRET, AL_STTS_CINC_SKIP)
      IRET = IBCLR(IRET, AL_STTS_CINC_STOP)
      
      AL_UTIL_CLRSTTS = IRET
      RETURN
      END
      
C************************************************************************
C Sommaire: AL_UTIL_IORSTTS
C
C Description:
C     La fonction AL_UTIL_IORSTTS assigne à ISTT_AL les bits d'états 
C     correspondant au code de contrôleur d'incrément ISTT_CI.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_UTIL_IORSTTS(ISTT_AL, ISTT_CI)

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: ISTT_AL
      INTEGER, INTENT(IN) :: ISTT_CI

      INCLUDE 'alutil.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'cicinc.fi'
      
      INTEGER IRET
C------------------------------------------------------------------------
      INTEGER DO1, IA, IC
      DO1(IA, IC) = MERGE(IBSET(IRET, IA), IRET, ISTT_CI .EQ. IC)
C------------------------------------------------------------------------
      
      IRET = ISTT_AL
      IRET = DO1(AL_STTS_CINC_COUPE, CI_CINC_STATUS_COUPE)
      IRET = DO1(AL_STTS_CINC_DTMIN, CI_CINC_STATUS_DTMIN)
      IRET = DO1(AL_STTS_CINC_SKIP,  CI_CINC_STATUS_SKIP)
      IRET = DO1(AL_STTS_CINC_STOP,  CI_CINC_STATUS_STOP)
      
      AL_UTIL_IORSTTS = IRET
      RETURN
      END