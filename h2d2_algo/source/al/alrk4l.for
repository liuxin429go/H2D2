C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  ALgorithme
C Objet:   Runge-Kutta Ordre 4 Lumped
C Type:    Concret
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_RK4L
C         SUBROUTINE AL_RK4L_000
C         SUBROUTINE AL_RK4L_999
C         SUBROUTINE AL_RK4L_CTR
C         SUBROUTINE AL_RK4L_DTR
C         SUBROUTINE AL_RK4L_INI
C         SUBROUTINE AL_RK4L_RST
C         SUBROUTINE AL_RK4L_REQHBASE
C         SUBROUTINE AL_RK4L_HVALIDE
C         SUBROUTINE AL_RK4L_RESOUS
C         SUBROUTINE AL_RK4L_RESOUS_E
C         SUBROUTINE AL_RK4L_UN_PAS
C         SUBROUTINE AL_RK4L_UN_STG
C         SUBROUTINE AL_RK4L_REQSTATUS
C         SUBROUTINE AL_RK4L_ASMM
C         SUBROUTINE AL_RK4L_ASMRES
C         SUBROUTINE AL_RK4L_DUMMY
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_RK4L_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RK4L_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alrk4l.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_RK4L_NOBJMAX,
     &                   AL_RK4L_HBASE,
     &                   'Algorithm Runge-Kutta Explicit O(4)')

      AL_RK4L_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RK4L_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alrk4l.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER  IERR
      EXTERNAL AL_RK4L_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_RK4L_NOBJMAX,
     &                   AL_RK4L_HBASE,
     &                   AL_RK4L_DTR)

      AL_RK4L_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_RK4L_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alrk4l.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_RK4L_NOBJMAX,
     &                   AL_RK4L_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_RK4L_HVALIDE(HOBJ))
         IOB = HOBJ - AL_RK4L_HBASE

         AL_RK4L_DTAL(IOB) = 0.0D0
         AL_RK4L_DTEF(IOB) = 0.0D0
         AL_RK4L_HCTI(IOB) = 0
         AL_RK4L_HRES(IOB) = 0
         AL_RK4L_HSIM(IOB) = 0
         AL_RK4L_NPAS(IOB) = 0
         AL_RK4L_NDLL(IOB) = 0
         AL_RK4L_NEQL(IOB) = 0
         AL_RK4L_LDLU(IOB) = 0
         AL_RK4L_LTV1(IOB) = 0
         AL_RK4L_LTV2(IOB) = 0
         AL_RK4L_LTV3(IOB) = 0
         AL_RK4L_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_RK4L_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_RK4L_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alrk4l.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_RK4L_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_RK4L_NOBJMAX,
     &                   AL_RK4L_HBASE)

      AL_RK4L_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     NITER    Nombre d'itération de réso lumpée
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_RK4L_INI(HOBJ, NITER, DELT, HCTI)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NITER
      REAL*8  DELT
      INTEGER HCTI

      INCLUDE 'alrk4l.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (.NOT. (HCTI .EQ. 0 .OR. CI_CINC_HVALIDE(HCTI))) GOTO 9900
      IF (NITER .LE. 0) GOTO 9901
      IF (NITER .GT. 5) GOTO 9902
      IF (DELT  .LE. 0.0D0) GOTO 9903

C---     Reset les données
      IERR = AL_RK4L_RST(HOBJ)

C---     CONSTRUIS LA RESO
      IERR = MR_LMPD_CTR(HRES)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - AL_RK4L_HBASE
         AL_RK4L_DTAL(IOB) = DELT
         AL_RK4L_DTEF(IOB) = 0.0D0
         AL_RK4L_HCTI(IOB) = HCTI
         AL_RK4L_HRES(IOB) = HRES
         AL_RK4L_NITR(IOB) = NITER
         AL_RK4L_NPAS(IOB) = 0
         AL_RK4L_NDLL(IOB) = 0
         AL_RK4L_NEQL(IOB) = 0
         AL_RK4L_LDLU(IOB) = 0
         AL_RK4L_LTV1(IOB) = 0
         AL_RK4L_LTV2(IOB) = 0
         AL_RK4L_LTV3(IOB) = 0
         AL_RK4L_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HCTI
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRES, 'MSG_CTRL_INCREMENT')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NITER_NEGATIF_OU_NUL',': ',NITER
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NITER_IRREALISTE',': ',NITER
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_DELT_INVALIDE',': ',DELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_RK4L_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_RK4L_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alrk4l.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HRES
      INTEGER LDLU, LTV1, LTV2, LTV3
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_RK4L_HBASE
      HRES = AL_RK4L_HRES(IOB)
      LDLU = AL_RK4L_LDLU(IOB)
      LTV1 = AL_RK4L_LTV1(IOB)
      LTV2 = AL_RK4L_LTV2(IOB)
      LTV3 = AL_RK4L_LTV3(IOB)

C---     Détruis la reso
      IF (MR_LMPD_HVALIDE(HRES)) IERR = MR_LMPD_CTR(HRES)

C---     Récupère l'espace
      IF (LTV3 .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTV3)
      IF (LTV2 .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTV2)
      IF (LTV1 .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTV1)
      IF (LDLU .NE. 0) IERR = SO_ALLC_ALLRE8(0, LDLU)

C---     Reset
      AL_RK4L_DTAL(IOB) = 0.0D0
      AL_RK4L_DTEF(IOB) = 0.0D0
      AL_RK4L_HCTI(IOB) = 0
      AL_RK4L_HRES(IOB) = 0
      AL_RK4L_HSIM(IOB) = 0
      AL_RK4L_NITR(IOB) = 0
      AL_RK4L_NPAS(IOB) = 0
      AL_RK4L_NDLL(IOB) = 0
      AL_RK4L_NEQL(IOB) = 0
      AL_RK4L_LDLU(IOB) = 0
      AL_RK4L_LTV1(IOB) = 0
      AL_RK4L_LTV2(IOB) = 0
      AL_RK4L_LTV3(IOB) = 0
      AL_RK4L_STTS(IOB) = AL_STTS_INDEFINI

      AL_RK4L_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_RK4L_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RK4L_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alrk4l.fi'
      INCLUDE 'alrk4l.fc'
C------------------------------------------------------------------------

      AL_RK4L_REQHBASE = AL_RK4L_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_RK4L_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RK4L_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alrk4l.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alrk4l.fc'
C------------------------------------------------------------------------

      AL_RK4L_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_RK4L_NOBJMAX,
     &                                  AL_RK4L_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système
C
C Description:
C     La méthode AL_RK4L_RESOUS intègre dans le temps par une méthode de
C     Runge-Kutta d'ordre 4 avec résolution itérative par matrice masse
C     lumpée. Globalement, elle amène la simulation HSIM de TSIM à TSIM+DELT.
C     En sortie, TSIM et DELT sont ajustés à la partie calculée.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C    2) En sortie, les dll sont à t+dt, mais les prop ne sont pas toutes
C       ajustées à la dernière valeur des ddl. En fait elles sont une
C       itération en retard.
C************************************************************************
      FUNCTION AL_RK4L_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM

      INCLUDE 'alrk4l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER IERR

      EXTERNAL AL_RK4L_ASMM
      EXTERNAL AL_RK4L_ASMRES
      EXTERNAL AL_RK4L_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)    ! cf. note
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)

C---     Résous
      IF (ERR_GOOD())
     &   IERR = AL_RK4L_RESOUS_E(HOBJ,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           HOBJ,
     &                           AL_RK4L_ASMM,  ! K
     &                           AL_RK4L_DUMMY, ! Kt
     &                           AL_RK4L_DUMMY, ! KU
     &                           AL_RK4L_DUMMY, ! F
     &                           AL_RK4L_ASMRES)

C---     Post-calcul des données (TSIM mis à jour)
      IF (ERR_GOOD()) IERR = LM_ELEM_PASFIN(HSIM, TSIM)

      AL_RK4L_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_RK4L_RESOUS_E(...) est la méthode de bas niveau pour
C     résoudre une simulation par un algorithme Runge-Kutta d'ordre 4.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo. C'est la fonction appelante qui dois gérer le
C     chargement des données.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM         Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_MDTK      Fonction de calcul de [M.dt + a.K]
C     EXTERNAL F_MDTKT     Fonction de calcul de [M.dt + a.KT]
C     EXTERNAL F_MDTKU     Fonction de calcul de [M.dt + a.K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C************************************************************************
      FUNCTION AL_RK4L_RESOUS_E (HOBJ,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           HALG,
     &                           F_MDTK,
     &                           F_MDTKT,
     &                           F_MDTKU,
     &                           F_F,
     &                           F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_MDTK
      INTEGER  F_MDTKT
      INTEGER  F_MDTKU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_MDTK
      EXTERNAL F_MDTKT
      EXTERNAL F_MDTKU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alrk4l.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'alutil.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'alrk4l.fc'

      REAL*8  DTNM, DTPRV, DTNOW, DTNXT, DTRST, TINI, TFIN
      REAL*8  TS_C, DT_C
      REAL*8  EPS0, EPS1    ! Convergence de la réso lumpée
      REAL*8  VTOL(1)

      INTEGER IERR
      INTEGER IOB
      INTEGER HSVPT
      INTEGER IPAS, ISTG, ISTC
      INTEGER HCTI, HRES
      INTEGER HNUMR
      INTEGER NEQL, NDLN, NNL, NDLL
      INTEGER NITR
      INTEGER NSTG
      INTEGER LDLG
      INTEGER LDLU, LTV1, LTV2, LTV3
      INTEGER LSPT
      LOGICAL ACTI, ESTOK, ESTRES, ESTRJT, ESTXIT, PUSHT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.solver.rk4l'

C---     Récupère les attributs
      IOB = HOBJ - AL_RK4L_HBASE
      HCTI = AL_RK4L_HCTI(IOB)
      HRES = AL_RK4L_HRES(IOB)
      NITR = AL_RK4L_NITR(IOB)
      LDLU = AL_RK4L_LDLU(IOB)
      LTV1 = AL_RK4L_LTV1(IOB)
      LTV2 = AL_RK4L_LTV2(IOB)
      LTV3 = AL_RK4L_LTV3(IOB)
D     CALL ERR_ASR(LDLU .EQ. 0 .OR. SO_ALLC_HEXIST(LDLU))
D     CALL ERR_ASR(LTV1 .EQ. 0 .OR. SO_ALLC_HEXIST(LTV1))
D     CALL ERR_ASR(LTV2 .EQ. 0 .OR. SO_ALLC_HEXIST(LTV2))
D     CALL ERR_ASR(LTV3 .EQ. 0 .OR. SO_ALLC_HEXIST(LTV3))

C---     Récupère les paramètres de la simulation
      HNUMR= LM_ELEM_REQHNUMC(HSIM)
      LDLG = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      NDLN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NNL  = LM_ELEM_REQPRM(HSIM, LM_GEOM_PRM_NNL)

C---     Présence/absence des composantes
      ACTI = CI_CINC_HVALIDE(HCTI)

C---     Calcule les paramètres
      DTNM  = AL_RK4L_DTAL(IOB)
      IF (HSIM .EQ. AL_RK4L_HSIM(IOB)) THEN
         DTNXT = MIN(AL_RK4L_DTEF(IOB), DELT)
      ELSE
         DTNXT = MIN(AL_RK4L_DTAL(IOB), DELT)
      ENDIF
      DTNOW = DTNXT
      TINI = TSIM
      TFIN = TSIM + DELT

C---     Écris l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_RK4L_SOLVE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TSIM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TFIN#<35>#', '= ', TFIN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_DELT#<35>#', '= ', DTNM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     (Ré)-alloue les tables de travail
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LDLU)  ! Tous à NDLL
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LTV1)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LTV2)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LTV3)

C---     Conserve les paramètres pour la simulation
      IF (ERR_GOOD()) THEN
         AL_RK4L_HSIM(IOB) = HSIM
         AL_RK4L_NDLL(IOB) = NDLL
         AL_RK4L_NEQL(IOB) = NEQL
         AL_RK4L_LDLU(IOB) = LDLU
         AL_RK4L_LTV1(IOB) = LTV1
         AL_RK4L_LTV2(IOB) = LTV2
         AL_RK4L_LTV3(IOB) = LTV3
      ENDIF

C---     Charge les données et pre-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC(HSIM)

C---     Méthode de reso lumpée
      EPS0 = 1.0D-6
      EPS1 = 1.0D-2
      IERR = MR_LMPD_INI(HRES,
     &                   SP_MORS_LUMP_SUM,
     &                   NITR,
     &                   EPS0,
     &                   EPS1)
      IERR = MR_LMPD_ASMMTX(HRES,
     &                      HSIM,
     &                      HALG,
     &                      F_MDTK,
     &                      F_MDTKU)
      IERR = MR_LMPD_FCTMTX(HRES)

C---     Crée un point de sauvegarde
      HSVPT = 0
      LSPT = 0
      IF (ERR_GOOD()) IERR = SO_SVPT_CTR(HSVPT)
      IF (ERR_GOOD()) IERR = SO_SVPT_INI(HSVPT, LDLG)
      IF (ERR_GOOD()) LSPT = SO_SVPT_GET(HSVPT)

C---     Initialise le contrôleur d'incrément
      IF (ERR_GOOD() .AND. ACTI)
     &   IERR = CI_CINC_DEB(HCTI, TINI, TFIN, DTNOW)


C        --------------------------------
C---     Boucle sur les sous-pas de temps
C        --------------------------------
      IPAS = 0
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         IF (DTNOW .LE. 0.0D0) GOTO 199
         IF (TINI  .GE. TFIN)  GOTO 199

C---        Log
         IPAS = IPAS + 1
         CALL LOG_INFO(LOG_ZNE, ' ')
         WRITE(LOG_BUF, '(A,I6,3(A,F20.6))',ERR=109)
     &      'MSG_PAS_DE_TEMPS:', IPAS,
     &      ', MSG_TINI=', TINI,
     &      ', MSG_TFIN=', TINI+DTNOW,
     &      ', MSG_DELT=', DTNOW
109      CONTINUE
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         CALL LOG_INCIND()

C---        Traceur
         IERR = TRA_ASGTALG(TINI)

C---        Sauve l'état courant
         IF (ERR_GOOD() .AND. HSVPT .NE. 0) THEN
            IERR = SO_SVPT_SAVE(HSVPT)
         ENDIF

C---        Résous
         TS_C = TINI
         DT_C = DTNOW
         IF (ERR_GOOD()) THEN
            IERR = AL_RK4L_UN_PAS (HOBJ,
     &                             TS_C,
     &                             DT_C,
     &                             HSIM,
     &                             VA(SO_ALLC_REQVIND(VA,LDLG)), !vdlg0
     &                             VA(SO_ALLC_REQVIND(VA,LDLU)),
     &                             HALG,
     &                             F_RES)
         ENDIF
         IF (ERR_BAD()) GOTO 199

C---        L'état de résolution
         ISTG = AL_RK4L_REQSTATUS(HOBJ)

C---        Adapte le pas de temps
         DTPRV = DTNOW
         IF (ACTI) THEN
D           CALL ERR_ASR(SO_ALLC_HEXIST(LSPT))
            ESTOK = (ISTG .EQ. AL_STTS_OK)
            IERR = CI_CINC_CLCDEL(HCTI,
     &                            NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                            VA(SO_ALLC_REQVIND(VA,LDLU)),
     &                            ESTOK)
            IERR = CI_CINC_REQDELT(HCTI, ISTC, DTNXT, DTNOW)
            ISTG = AL_UTIL_CLRSTTS(ISTG)
            ISTG = AL_UTIL_IORSTTS(ISTG, ISTC)
         ELSE
            DTRST = TFIN - (TINI+DTPRV)
            DTNOW = MIN(DTPRV, DTRST)
            DTRST = DTRST - DTNOW
            IF (DTRST .GT. 0.0D0 .AND. DTRST < (0.05D0*DTPRV)) THEN
               DTNOW = DTNOW + DTRST
            ENDIF
         ENDIF

C---        L'état de résolution
         ESTOK  = (ISTG .EQ. AL_STTS_OK)
         ESTRES = .NOT. BTEST(ISTG, AL_STTS_CRIA_BAD)
         ESTRJT = .FALSE.
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_COUPE)
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_DTMIN)
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_STOP)
         ESTXIT = .FALSE.
         ESTXIT = ESTXIT .OR. BTEST(ISTG, AL_STTS_CINC_DTMIN)
         ESTXIT = ESTXIT .OR. BTEST(ISTG, AL_STTS_CINC_STOP)

C---        Logique de continuation
         PUSHT = .FALSE.
         IF (ESTRES) THEN
            PUSHT = .TRUE.
D           CALL ERR_ASR(ERR_BAD() .OR. DT_C .EQ. 0.0D0)
         ELSE
            IF (ACTI) THEN                ! !OK, Avec contrôleur d'incrément
               IERR = SO_SVPT_RBCK(HSVPT) !  rollback. On pert possiblement
               PUSHT = .FALSE.            !  une partie déjà convergée
            ELSE
               PUSHT = .TRUE.             ! !OK, Sans contrôleur d'incrément
            ENDIF                         !   continue
         ENDIF
         IF (PUSHT) TINI = TINI + DTPRV

C---        Log l'incrément sur le pas de temps
         IF (ERR_GOOD()) THEN
            IERR = AL_UTIL_LOGSOL(NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,LDLU)),
     &                            HNUMR)
         ENDIF

C---        Log l'état
         IF (ERR_GOOD()) THEN
            IF (ACTI) THEN
               IF (BTEST(ISTG, AL_STTS_CINC_DTMIN)) 
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_DTMIN')
               IF (BTEST(ISTG, AL_STTS_CINC_SKIP)) 
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_SKIP')
               IF (BTEST(ISTG, AL_STTS_CINC_STOP))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_STOP')
               IF (.NOT. ESTRES) 
     &         CALL LOG_INFO(LOG_ZNE, 'MSG_PAS_REJETE')
            ELSE
               IF (.NOT. ESTRES) THEN
                  CALL LOG_WRN(LOG_ZNE, 'MSG_PAS_NON_CONVERGE')
                  CALL LOG_WRN(LOG_ZNE, 'MSG_CONTINUE')
               ENDIF
            ENDIF
         ENDIF

C---        Mise à jour de la solution
         IF (ERR_GOOD() .AND. ESTOK) THEN
            CALL DAXPY(NDLL,
     &                 1.0D0,
     &                 VA(SO_ALLC_REQVIND(VA,LDLU)), 1,
     &                 VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
            IERR = LM_ELEM_CLCPST(HSIM)
         ENDIF

         CALL LOG_DECIND()
         IF (ESTXIT) GOTO 199
      GOTO 100 ! end while
199   CONTINUE

C---     Ajuste TSIM et DELT à la partie calculée
      IF (ERR_GOOD()) THEN
         DELT = (TSIM+DELT) - TINI
         TSIM = TINI
      ENDIF

C---     Conserve la prévision suite au dernier pas de temps
      IF (ERR_GOOD()) THEN
         AL_RK4L_DTEF(IOB) = DTNXT
      ENDIF

C---     Conserve l'état
      AL_RK4L_STTS(IOB) = ISTG

C---     Relâche le point de sauvegarde
      IF (HSVPT .NE. 0) THEN
         CALL ERR_PUSH()
         IERR = SO_SVPT_DTR(HSVPT)
         CALL ERR_POP()
      ENDIF

      CALL LOG_DECIND()
      AL_RK4L_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée AL_RK4L_UN_PAS intègre un pas de temps. Elle calcule
C     dans VDELU l'incrément du pas.
C
C Entrée:
C
C Sortie:
C     VDELU    Table des incréments
C
C Notes:
C************************************************************************
      FUNCTION AL_RK4L_UN_PAS (HOBJ,
     &                         TSIM,
     &                         DELT,
     &                         HSIM,
     &                         VDLG0,
     &                         VDELU,
     &                         HALG,
     &                         F_RES)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      REAL*8   VDLG0(*)
      REAL*8   VDELU(*)
      INTEGER  HALG
      INTEGER  F_RES
      EXTERNAL F_RES

      INCLUDE 'alrk4l.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER   IERR
      INTEGER   IOB
      INTEGER   LDLG, LPAS, LRES
      INTEGER   NDLL

      REAL*8, PARAMETER :: ZERO = 0.0000000000000000000D-00
      REAL*8, PARAMETER :: UN_2 = 5.0000000000000000000D-01
      REAL*8, PARAMETER :: UN_3 = 3.3333333333333333333D-01
      REAL*8, PARAMETER :: UN_6 = 1.6666666666666666667D-01
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_RK4L_HBASE
      NDLL = AL_RK4L_NDLL(IOB)
      LDLG = AL_RK4L_LTV1(IOB)
      LPAS = AL_RK4L_LTV2(IOB)
      LRES = AL_RK4L_LTV3(IOB)

C---     Initialise l'incrément DU
      CALL DINIT(NDLL, ZERO, VDELU, 1)

C---        PAS 1
      CALL DCOPY(NDLL, VDLG0, 1, VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
      IERR = AL_RK4L_UN_STG(HOBJ,
     &                      TSIM,
     &                      HSIM,
     &                      VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                      VA(SO_ALLC_REQVIND(VA,LPAS)),
     &                      VA(SO_ALLC_REQVIND(VA,LRES)),
     &                      HALG, F_RES)
      CALL DAXPY(NDLL,
     &           UN_6*DELT,
     &           VA(SO_ALLC_REQVIND(VA,LPAS)), 1,
     &           VDELU, 1)

C---        PAS 2
C        CALL DCOPY(NDLL, VA(SO_ALLC_REQVIND(VA,LDLG)), 1, VA(SO_ALLC_REQVIND(VA,LTV1)), 1) ! PAS BESOIN
      CALL DAXPY(NDLL,
     &           UN_2*DELT,
     &           VA(SO_ALLC_REQVIND(VA,LPAS)), 1,
     &           VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
      IERR = AL_RK4L_UN_STG(HOBJ,
     &                      TSIM + UN_2*DELT,
     &                      HSIM,
     &                      VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                      VA(SO_ALLC_REQVIND(VA,LPAS)),
     &                      VA(SO_ALLC_REQVIND(VA,LRES)),
     &                      HALG, F_RES)
      CALL DAXPY(NDLL,
     &           UN_3*DELT,
     &           VA(SO_ALLC_REQVIND(VA,LPAS)), 1,
     &           VDELU, 1)

C---        PAS 3
      CALL DCOPY(NDLL, VDLG0, 1, VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
      CALL DAXPY(NDLL,
     &           UN_2*DELT,
     &           VA(SO_ALLC_REQVIND(VA,LPAS)), 1,
     &           VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
      IERR = AL_RK4L_UN_STG(HOBJ,
     &                      TSIM + UN_2*DELT,
     &                      HSIM,
     &                      VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                      VA(SO_ALLC_REQVIND(VA,LPAS)),
     &                      VA(SO_ALLC_REQVIND(VA,LRES)),
     &                      HALG, F_RES)
      CALL DAXPY(NDLL,
     &           UN_3*DELT,
     &           VA(SO_ALLC_REQVIND(VA,LPAS)), 1,
     &           VDELU, 1)

C---     PAS 4
      CALL DCOPY(NDLL, VDLG0, 1, VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
      CALL DAXPY(NDLL,
     &           DELT,
     &           VA(SO_ALLC_REQVIND(VA,LPAS)), 1,
     &           VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
      IERR = AL_RK4L_UN_STG(HOBJ,
     &                      TSIM + DELT,
     &                      HSIM,
     &                      VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                      VA(SO_ALLC_REQVIND(VA,LPAS)),
     &                      VA(SO_ALLC_REQVIND(VA,LRES)),
     &                      HALG, F_RES)
      CALL DAXPY(NDLL,
     &           UN_6*DELT,
     &           VA(SO_ALLC_REQVIND(VA,LPAS)), 1,
     &           VDELU, 1)

C---     Conserve le résultat
      AL_RK4L_STTS(IOB) = AL_STTS_OK

      AL_RK4L_UN_PAS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée AL_RK4L_UN_STG intègre un "stage". Elle calcule
C     dans VDLU l'incrément du stage.
C
C Entrée:
C     VDLG     Table des DDL
C     VRES     Table de travail
C
C Sortie:
C     VDLU     Table des incréments
C
C Notes:
C************************************************************************
      FUNCTION AL_RK4L_UN_STG (HOBJ,
     &                         TSIM,
     &                         HSIM,
     &                         VDLG,
     &                         VDLU,
     &                         VRES,
     &                         HALG,
     &                         F_RES)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      INTEGER  HSIM
      REAL*8   VDLG(*)
      REAL*8   VDLU(*)
      REAL*8   VRES(*)
      INTEGER  HALG
      INTEGER  F_RES
      EXTERNAL F_RES

      INCLUDE 'alrk4l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HRES, HNUMR, HDDL
      INTEGER NNL, NDLN, NEQL, NDLL
      INTEGER LLOCN
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - AL_RK4L_HBASE
      HRES = AL_RK4L_HRES(IOB)

C---     RECUPERE LES DONNEES
      HNUMR = LM_ELEM_REQHNUMC(HSIM)
      LLOCN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)
      NDLN  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NEQL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      NNL   = LM_ELEM_REQPRM(HSIM, LM_GEOM_PRM_NNL)

C---     CHARGE LES DONNÉES AU TEMPS T
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
!     IF (ERR_GOOD()) IERR = LM_ELEM_CLC(HSIM)    ! Fait par pushdlg

C---     ASSIGNE VDLG À LA SIMULATION
      IF (ERR_GOOD())
     &   IERR = LM_ELEM_PUSHLDLG(HSIM, SO_ALLC_REQVHND(VDLG))

C---     RÉSOUD
      IF (ERR_GOOD())
     &   IERR = MR_LMPD_RESMTX(HRES,
     &                         HSIM,
     &                         HALG,
     &                         F_RES,
     &                         VRES)

C---     RÉTABLIS LA SIMULATION
      IF (ERR_GOOD()) IERR = LM_ELEM_POPLDLG(HSIM)

C---     PASSE DE NEQ A NDLL
      IF (ERR_GOOD()) THEN
         CALL DINIT(NDLL, 0.0D0, VDLU, 1)
         IERR = SP_ELEM_NEQ2NDLT(NEQL,
     &                           NDLL,
     &                           KA(SO_ALLC_REQKIND(KA,LLOCN)),
     &                           VRES,
     &                           VDLU)
      ENDIF

C---     SYNCHRONISE LES PROCESS
      IF (ERR_GOOD() .AND. NM_NUMR_CTRLH(HNUMR)) THEN
         IERR = NM_NUMR_DSYNC(HNUMR,
     &                        NDLN,
     &                        NNL,
     &                        VDLU)
      ENDIF

      AL_RK4L_UN_STG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_RK4L_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RK4L_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alrk4l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alrk4l.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_RK4L_REQSTATUS = AL_RK4L_STTS(HOBJ - AL_RK4L_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assemble la matrice masse.
C
C Description:
C
C Entrée:
C     INTEGER  HOBJ     ! Handle sur l'objet courant
C     INTEGER  HMTX     ! Handle sur la matrice, param de F_ASM
C     INTEGER  F_ASM    ! Fonction d'assemblage
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RK4L_ASMM(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'alrk4l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_RK4L_HBASE
      HSIM = AL_RK4L_HSIM(IOB)

C---     ASSEMBLE M
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMM(HSIM, HMTX, F_ASM)

      AL_RK4L_ASMM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_RK4L_ASMRES(HOBJ, VRES, VDLG, ADLG)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VRES(*)
      REAL*8   VDLG(*)
      LOGICAL  ADLG

      INCLUDE 'alrk4l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alrk4l.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      INTEGER LDLG
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_RK4L_HBASE
      HSIM = AL_RK4L_HSIM(IOB)

C---     ASSIGNE VDLG À LA SIMULATION
      IF (ADLG) THEN
         IERR = LM_ELEM_PUSHLDLG(HSIM, SO_ALLC_REQVHND(VDLG))
      ENDIF

C---     ASSEMBLE KU DANS VRES
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMFKU(HSIM, VRES)

C---     RÉTABLIS LA SIMULATION
      IF (ADLG) THEN
         CALL ERR_PUSH()
         IERR = LM_ELEM_POPLDLG(HSIM)
         CALL ERR_POP()
      ENDIF

      AL_RK4L_ASMRES = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Fonction vide, tenant lieu d'une autre.
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_RK4L_DUMMY(HOBJ)

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alrk4l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alrk4l.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RK4L_HVALIDE(HOBJ))
C------------------------------------------------------------------------

D     CALL ERR_ASR(.FALSE.)
      CALL ERR_ASG(ERR_FTL, 'ERR_APPEL_FNCT_INVALIDE')

      AL_RK4L_DUMMY = ERR_TYP()
      RETURN
      END
