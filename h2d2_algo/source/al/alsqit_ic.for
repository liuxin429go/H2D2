C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_ALSQIT_XEQCTR
C     INTEGER IC_ALSQIT_XEQMTH
C     CHARACTER*(32) IC_ALSQIT_REQCLS
C     INTEGER IC_ALSQIT_REQHDL
C   Private:
C     SUBROUTINE IC_ALSQIT_AID
C     INTEGER IC_ALSQIT_PRN
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQIT_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQIT_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'alsqit_ic.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alsqit_ic.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HELE, HALG, HTRG
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALSQIT_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---        LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the element</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HELE)
C     <comment>Handle on the resolution algorithm or strategy</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HALG)
C     <comment>Handle on the resolution trigger (default 0) (Status: Experimental)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HTRG)
      IF (IERR .NE. 0) HTRG = 0

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SQIT_CTR  (HOBJ)
      IF (ERR_GOOD()) IERR = AL_SQIT_INIAL(HOBJ,
     &                                     HELE,
     &                                     HALG,
     &                                     HTRG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_ALSQIT_PRN(HOBJ)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the sequence</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>algo_sequence_algo</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_AUCUN_PARAMETRE_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALSQIT_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_ALSQIT_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALSQIT_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQIT_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'alsqit_ic.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alsqit_ic.fc'

      INTEGER      IERR
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the element</comment>
         IF (PROP .EQ. 'helem') THEN
            IVAL = AL_SQIT_REQHSIM(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the algorithm or strategy</comment>
         ELSEIF (PROP .EQ. 'halgo') THEN
            IVAL = AL_SQIT_REQHALG(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the post-treatment</comment>
         ELSEIF (PROP .EQ. 'hpost') THEN
            IVAL = AL_SQIT_REQHPST(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the trigger</comment>
         ELSEIF (PROP .EQ. 'htrig') THEN
            IVAL = AL_SQIT_REQHTRG(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_SQIT_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_ALSQIT_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_ALSQIT_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALSQIT_AID()

9999  CONTINUE
      IC_ALSQIT_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQIT_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQIT_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqit_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>algo_sequence_algo</b> represents an item in a sequence of
C  simulations. Items are to be solved one after the other.
C</comment>
      IC_ALSQIT_REQCLS = 'algo_sequence_algo'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQIT_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQIT_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqit_ic.fi'
      INCLUDE 'alsqit.fi'
C-------------------------------------------------------------------------

      IC_ALSQIT_REQHDL = AL_SQIT_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALSQIT_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('alsqit_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQIT_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqit_ic.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alsqit_ic.fc'

      INTEGER IERR
      INTEGER HNDL
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_ITEM_SEQUENCE'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      TXT = "---"
      HNDL = AL_SQIT_REQHSIM(HOBJ)
      IF (HNDL .NE. 0) IERR = OB_OBJC_REQNOMCMPL(TXT, HNDL)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      TXT = "---"
      HNDL = AL_SQIT_REQHALG(HOBJ)
      IF (HNDL .NE. 0) IERR = OB_OBJC_REQNOMCMPL(TXT, HNDL)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      TXT = "---"
      HNDL = AL_SQIT_REQHPST(HOBJ)
      IF (HNDL .NE. 0) IERR = OB_OBJC_REQNOMCMPL(TXT, HNDL)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      TXT = "---"
      HNDL = AL_SQIT_REQHTRG(HOBJ)
      IF (HNDL .NE. 0) IERR = OB_OBJC_REQNOMCMPL(TXT, HNDL)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_ALSQIT_PRN = ERR_TYP()
      RETURN
      END
