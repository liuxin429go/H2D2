C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER AL_SQIT_000
C     INTEGER AL_SQIT_999
C     INTEGER AL_SQIT_CTR
C     INTEGER AL_SQIT_DTR
C     INTEGER AL_SQIT_INIAL
C     INTEGER AL_SQIT_INIPS
C     INTEGER AL_SQIT_RST
C     INTEGER AL_SQIT_REQHBASE
C     LOGICAL AL_SQIT_HVALIDE
C     INTEGER AL_SQIT_REQHALG
C     INTEGER AL_SQIT_REQHPST
C     INTEGER AL_SQIT_REQHSIM
C     INTEGER AL_SQIT_REQHTRG
C     INTEGER AL_SQIT_XEQ
C   Private:
C     INTEGER AL_SQIT_XEQAL
C     INTEGER AL_SQIT_XEQPS
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_SQIT_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQIT_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_SQIT_NOBJMAX,
     &                   AL_SQIT_HBASE,
     &                   'Algorithm Sequence Item')

      AL_SQIT_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQIT_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fc'

      INTEGER  IERR
      EXTERNAL AL_SQIT_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_SQIT_NOBJMAX,
     &                   AL_SQIT_HBASE,
     &                   AL_SQIT_DTR)

      AL_SQIT_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_SQIT_NOBJMAX,
     &                   AL_SQIT_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_SQIT_HVALIDE(HOBJ))
         IOB = HOBJ - AL_SQIT_HBASE

         AL_SQIT_HSIMD(IOB) = 0
         AL_SQIT_HALGO(IOB) = 0
         AL_SQIT_HPOST(IOB) = 0
         AL_SQIT_HTRIG(IOB) = 0
      ENDIF

      AL_SQIT_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_SQIT_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_SQIT_NOBJMAX,
     &                   AL_SQIT_HBASE)

      AL_SQIT_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction AL_SQIT_INIAL initialise et dimensionne l'objet comme
C     item qui contient un algorithme.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_INIAL(HOBJ,
     &                       HSIMD,
     &                       HALGO,
     &                       HTRIG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_INIAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIMD
      INTEGER HALGO
      INTEGER HTRIG

      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'tgtrig.fi'
      INCLUDE 'alsqit.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER N
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - AL_SQIT_HBASE

C---     CONTROLE DES PARAMETRES
      IF (.NOT. LM_HELE_HVALIDE(HSIMD)) GOTO 9900
      IF (.NOT. AL_SLVR_HVALIDE(HALGO)) GOTO 9901
      IF (HTRIG .NE. 0 .AND. .NOT. TG_TRIG_CTRLH(HTRIG)) GOTO 9902

C---     RESET LES DONNEES
      IERR = AL_SQIT_RST(HOBJ)

C---     ASSIGNE LES DONNÉES
      AL_SQIT_HSIMD(IOB) = HSIMD
      AL_SQIT_HALGO(IOB) = HALGO
      AL_SQIT_HTRIG(IOB) = HTRIG

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ', HSIMD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSIMD, 'MSG_SIMULATION')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ', HALGO
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HALGO, 'MSG_ALGORITHME')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ', HTRIG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HTRIG, 'MSG_TRIGGER')
      GOTO 9999

9999  CONTINUE
      AL_SQIT_INIAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction AL_SQIT_INIPS initialise et dimensionne l'objet comme
C     item qui contient un post_traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_INIPS(HOBJ,
     &                       HSIMD,
     &                       HPOST,
     &                       HTRIG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_INIPS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIMD
      INTEGER HPOST
      INTEGER HTRIG

      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'tgtrig.fi'
      INCLUDE 'alsqit.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER N
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - AL_SQIT_HBASE

C---     CONTROLE DES PARAMETRES
      IF (.NOT. LM_HELE_HVALIDE(HSIMD)) GOTO 9900
      IF (.NOT. PS_POST_HVALIDE(HPOST)) GOTO 9901
      IF (HTRIG .NE. 0 .AND. .NOT. TG_TRIG_CTRLH(HTRIG)) GOTO 9902

C---     RESET LES DONNEES
      IERR = AL_SQIT_RST(HOBJ)

C---     FAIT LE LIEN HPOST-HSIMD
      IF (ERR_GOOD()) IERR = PS_POST_ASGHSIM(HPOST, HSIMD)

C---     ASSIGNE LES DONNÉES
      AL_SQIT_HSIMD(IOB) = HSIMD
      AL_SQIT_HPOST(IOB) = HPOST
      AL_SQIT_HTRIG(IOB) = HTRIG

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ', HSIMD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSIMD, 'MSG_SIMULATION')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ', HPOST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HPOST, 'MSG_POST_TRAITEMENT')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ', HTRIG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HTRIG, 'MSG_TRIGGER')
      GOTO 9999

9999  CONTINUE
      AL_SQIT_INIPS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - AL_SQIT_HBASE
      AL_SQIT_HSIMD(IOB) = 0
      AL_SQIT_HALGO(IOB) = 0
      AL_SQIT_HPOST(IOB) = 0
      AL_SQIT_HTRIG(IOB) = 0

      AL_SQIT_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_SQIT_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQIT_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqit.fi'
      INCLUDE 'alsqit.fc'
C------------------------------------------------------------------------

      AL_SQIT_REQHBASE = AL_SQIT_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_SQIT_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQIT_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alsqit.fc'
C------------------------------------------------------------------------

      AL_SQIT_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_SQIT_NOBJMAX,
     &                                  AL_SQIT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_REQHALG(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_REQHALG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_SQIT_REQHALG = AL_SQIT_HALGO(HOBJ - AL_SQIT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_REQHPST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_REQHPST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_SQIT_REQHPST = AL_SQIT_HPOST(HOBJ - AL_SQIT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_REQHSIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_REQHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_SQIT_REQHSIM = AL_SQIT_HSIMD(HOBJ - AL_SQIT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_REQHTRG(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_REQHTRG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_SQIT_REQHTRG = AL_SQIT_HTRIG(HOBJ - AL_SQIT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_SQIT_XEQ(...) exécute l'algorithme lié à l'item de
C     sequence.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Delta T
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_XEQ(HOBJ, TSIM, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQIT_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TSIM
      REAL*8  DELT

      INCLUDE 'alsqit.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'tgtrig.fi'
      INCLUDE 'alsqit.fc'

      INTEGER IERR
      INTEGER HTRG, HALG, HPST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Incrémente le trigger et sort s'il n'est pas déclenché
      HTRG = AL_SQIT_REQHTRG(HOBJ)  ! Trigger
      IF (TG_TRIG_CTRLH(HTRG)) THEN
         IF (.NOT. TG_TRIG_INC(HTRG)) THEN
            TSIM = TSIM + DELT
            DELT = 0.0D0
            GOTO 9999
         ENDIF
      ENDIF

      HALG = AL_SQIT_REQHALG(HOBJ)  ! Algo
      HPST = AL_SQIT_REQHPST(HOBJ)  ! Post-traitement

      IF     (AL_SLVR_HVALIDE(HALG)) THEN
         IERR = AL_SQIT_XEQAL(HOBJ, TSIM, DELT)
      ELSEIF (PS_POST_HVALIDE(HPST)) THEN
         IERR = AL_SQIT_XEQPS(HOBJ, TSIM, DELT)
D     ELSE
D        CALL ERR_ASR(.FALSE.)
      ENDIF

9999  CONTINUE
      AL_SQIT_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_SQIT_XEQAL(...) exécute l'algorithme de l'item de
C     sequence.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Delta T
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQIT_XEQAL(HOBJ, TSIM, DELT)

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TSIM
      REAL*8  DELT

      INCLUDE 'alsqit.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'alsqit.fc'

      INTEGER IERR
      INTEGER ISTTS
      INTEGER HSIM, HCRA, HLTR, HALG
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.solver'

C---     La résolution
      HSIM = AL_SQIT_REQHSIM(HOBJ)  ! Sim data
      HALG = AL_SQIT_REQHALG(HOBJ)  ! Algo
D     CALL ERR_ASR(AL_SLVR_HVALIDE(HALG))
      IF (ERR_GOOD()) IERR  = AL_SLVR_RESOUS(HALG, TSIM, DELT, HSIM)
      IF (ERR_GOOD()) ISTTS = AL_SLVR_REQSTATUS(HALG)
      
C---     Contrôles de retour
      IF (ERR_GOOD() .AND. DELT .GT. 0.0D0) THEN
         WRITE(LOG_BUF, '(A)') 'MSG_PAS_INCOMPLET'
         CALL LOG_WRN(LOG_ZNE, LOG_BUF)
      ENDIF
      IF (ERR_GOOD() .AND. ISTTS .NE. AL_STTS_OK) THEN
         WRITE(LOG_BUF, '(A)') 'MSG_PAS_NON_CONVERGE'
         CALL LOG_WRN(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (ERR_GOOD() .AND. BTEST(ISTTS, AL_STTS_CINC_DTMIN)) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_DELT_MINIMAL_ATTEINT')
      ENDIF
      IF (ERR_GOOD() .AND. BTEST(ISTTS, AL_STTS_CINC_STOP)) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_CINC_STOP')
      ENDIF

      AL_SQIT_XEQAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_SQIT_XEQPS(...) exécute le post-traitement de l'item de
C     sequence.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Delta T
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQIT_XEQPS(HOBJ, TSIM, DELT)

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TSIM
      REAL*8  DELT

      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'alsqit.fc'

      INTEGER IERR
      INTEGER HSIM
      INTEGER HPST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      HSIM = AL_SQIT_REQHSIM(HOBJ)  ! Sim data
      HPST = AL_SQIT_REQHPST(HOBJ)
D     CALL ERR_ASR(LM_HELE_HVALIDE(HSIM))
D     CALL ERR_ASR(PS_POST_HVALIDE(HPST))

C---     Charge les données de la simulation
      IF (.NOT. LM_HELE_ESTPRC(HSIM)) THEN
         IF (ERR_GOOD()) IERR = LM_HELE_PASDEB(HSIM, TSIM)
         IF (ERR_GOOD()) IERR = LM_HELE_CLCPRE(HSIM)
      ENDIF
      IF (ERR_GOOD()) IERR = LM_HELE_CLC(HSIM)

C---     Post-traite
      IF (ERR_GOOD()) IERR = PS_POST_ACC(HPST)

C---     Ajuste TSIM et DELT à la partie calculée
      IF (ERR_GOOD()) THEN
         TSIM = TSIM+DELT
         DELT = 0.0D0
      ENDIF
      
      AL_SQIT_XEQPS = ERR_TYP()
      RETURN
      END
