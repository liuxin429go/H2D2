C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  ALgorithme
C Objet:   NOde DO loop
C Type:    Concret
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_NODO
C         SUBROUTINE AL_NODO_000
C         SUBROUTINE AL_NODO_999
C         SUBROUTINE AL_NODO_CTR
C         SUBROUTINE AL_NODO_DTR
C         SUBROUTINE AL_NODO_INI
C         SUBROUTINE AL_NODO_RST
C         SUBROUTINE AL_NODO_REQHBASE
C         SUBROUTINE AL_NODO_HVALIDE
C         SUBROUTINE AL_NODO_RESOUS
C         SUBROUTINE AL_NODO_RESOUS_E
C         SUBROUTINE AL_NODO_REQSTATUS
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_NODO_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NODO_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnodo.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnodo.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_NODO_NOBJMAX,
     &                   AL_NODO_HBASE,
     &                   'Algorithm Node For Loop')

      AL_NODO_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NODO_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnodo.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnodo.fc'

      INTEGER  IERR
      EXTERNAL AL_NODO_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_NODO_NOBJMAX,
     &                   AL_NODO_HBASE,
     &                   AL_NODO_DTR)

      AL_NODO_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NODO_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnodo.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnodo.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_NODO_NOBJMAX,
     &                   AL_NODO_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_NODO_HVALIDE(HOBJ))
         IOB = HOBJ - AL_NODO_HBASE

         AL_NODO_HALG (IOB) = 0
         AL_NODO_NITER(IOB) = 0
         AL_NODO_HXPR (IOB) = 0
         AL_NODO_HLFT (IOB) = 0
         AL_NODO_HRHT (IOB) = 0
         AL_NODO_LRST (IOB) = .TRUE.
         AL_NODO_STTS (IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_NODO_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NODO_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnodo.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnodo.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_NODO_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_NODO_NOBJMAX,
     &                   AL_NODO_HBASE)

      AL_NODO_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NODO_INI(HOBJ, HALG, NITER, HXPR, HLFT, HRHT, LRST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HALG
      INTEGER NITER
      INTEGER HXPR
      INTEGER HLFT
      INTEGER HRHT
      LOGICAL LRST

      INCLUDE 'alnodo.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alnodo.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = AL_NODO_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - AL_NODO_HBASE

C---     CONTROLE LES DONNÉES
      IF (.NOT. AL_SLVR_HVALIDE(HALG)) GOTO 9900
      IF (.NOT. (HXPR .EQ. 0 .OR. IC_EXPR_HVALIDE(HXPR))) GOTO 9901
      IF (.NOT. (HLFT .EQ. 0 .OR. AL_SLVR_HVALIDE(HLFT))) GOTO 9902
      IF (.NOT. (HRHT .EQ. 0 .OR. AL_SLVR_HVALIDE(HRHT))) GOTO 9903
      IF (NITER .LE. 0 .OR. NITER .GT. 16) GOTO 9904

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         AL_NODO_HALG (IOB) = HALG
         AL_NODO_NITER(IOB) = NITER
         AL_NODO_HXPR (IOB) = HXPR
         AL_NODO_HLFT (IOB) = HLFT
         AL_NODO_HRHT (IOB) = HRHT
         AL_NODO_LRST (IOB) = LRST
         AL_NODO_STTS (IOB) = AL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HALG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HALG, 'MSG_SOLVER')
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HXPR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HXPR, 'MSG_EXPRESSION')
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HLFT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HLFT, 'MSG_NOEUD_GAUCHE')
      GOTO 9999
9903  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HRHT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRHT, 'MSG_NOEUD_DROIT')
      GOTO 9999
9904  WRITE(ERR_BUF,'(2A,I12)')'ERR_NBR_ITERATION_INVALIDE',': ',NITER
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_NODO_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NODO_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnodo.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnodo.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_NODO_HBASE

C---     RESET LES ATTRIBUTS
      AL_NODO_HALG (IOB) = 0
      AL_NODO_NITER(IOB) = 0
      AL_NODO_HXPR (IOB) = 0
      AL_NODO_HLFT (IOB) = 0
      AL_NODO_HRHT (IOB) = 0
      AL_NODO_LRST (IOB) = .TRUE.
      AL_NODO_STTS (IOB) = AL_STTS_INDEFINI

      AL_NODO_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_NODO_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NODO_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnodo.fi'
      INCLUDE 'alnodo.fc'
C------------------------------------------------------------------------

      AL_NODO_REQHBASE = AL_NODO_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_NODO_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NODO_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnodo.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alnodo.fc'
C------------------------------------------------------------------------

      AL_NODO_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_NODO_NOBJMAX,
     &                                  AL_NODO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NODO_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM

      INCLUDE 'alnodo.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'alnodo.fc'

      REAL*8  TS_C, DT_C
      INTEGER IERR
      INTEGER IOB
      INTEGER HSVPT, IT, NITER
      INTEGER HNOD, HALG, HXPR
      INTEGER ISTTS, IMASK
      INTEGER LDLG
      LOGICAL LRES, LSTP, LRST
C------------------------------------------------------------------------
      INTEGER ix, im
      !! INTEGER BITMASK_SET, BITMASK_CLEAR, BITMASK_FLIP
      INTEGER BITMASK_CHECK_ALL, BITMASK_CHECK_ANY
      !! BITMASK_SET      (ix, im) =  IOR (ix, im)
      !! BITMASK_CLEAR    (ix, im) =  IAND(ix, INOT(im))
      !! BITMASK_FLIP     (ix, im) =  IEOR(ix, im)
      !! BITMASK_CHECK_ALL(ix, im) = (IAND(ix, im) .EQ. im)
      BITMASK_CHECK_ANY(ix, im) =  IAND(ix, im)
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------
      IMASK = 0 
      !! IMASK = IBSET(IMASK, AL_STTS_CRIA_BAD)
      !! IMASK = IBSET(IMASK, AL_STTS_GLOB_LIM)
      !! IMASK = IBSET(IMASK, AL_STTS_CRIC_RJT)
      !! IMASK = IBSET(IMASK, AL_STTS_CRIC_OSC)
      IMASK = IBSET(IMASK, AL_STTS_CINC_COUPE)
      IMASK = IBSET(IMASK, AL_STTS_CINC_DTMIN)
      IMASK = IBSET(IMASK, AL_STTS_CINC_SKIP)
      IMASK = IBSET(IMASK, AL_STTS_CINC_STOP)

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - AL_NODO_HBASE
      HALG = AL_NODO_HALG (IOB)
      NITER= AL_NODO_NITER(IOB)
      HXPR = AL_NODO_HXPR (IOB)
      HNOD = AL_NODO_HRHT (IOB)      ! le noeud négatif
      LRST = AL_NODO_LRST (IOB)

C---     Récupère les paramètres de la simulation
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)

C---     Crée un point de sauvegarde
      HSVPT = 0
      IF (LRST .AND. HNOD .NE. 0) THEN
         IERR = SO_SVPT_CTR(HSVPT)
         IERR = SO_SVPT_INI(HSVPT, LDLG)
      ENDIF

C---     Boucle sur les itérations
      TS_C = TSIM
      DT_C = DELT
      LRES = .TRUE.
      ISTTS = AL_STTS_INDEFINI
      DO IT=1,NITER

C---        Exécute l'algorithme
         ISTTS = AL_STTS_INDEFINI
         IF (ERR_GOOD()) THEN
            IERR = AL_SLVR_RESOUS(HALG, TS_C, DT_C, HSIM)
            ISTTS = AL_SLVR_REQSTATUS(HALG)
         ENDIF

C---        Évalue l'expression
         LRES = .FALSE.    ! Est résolu
         LSTP = .FALSE.    ! Coupe, dtmin, stop ...
         IF (ERR_GOOD()) THEN
            IF (IC_EXPR_HVALIDE(HXPR)) THEN
               IERR = IC_EXPR_XEQ_L(HXPR, LRES)
            ELSE
               LRES = (ISTTS .EQ. AL_STTS_OK)
               LSTP = (BITMASK_CHECK_ANY(ISTTS, IMASK) .NE. 0)
            ENDIF
         ENDIF

         IF (LRES) EXIT
         IF (LSTP) EXIT
         IF (ERR_BAD()) EXIT
      ENDDO

C---     Stop?      
      LSTP = .FALSE.    ! Stop
      IF (ISTTS .NE. AL_STTS_INDEFINI) 
     &   LSTP = BTEST(ISTTS, AL_STTS_CINC_STOP)
      
C---     Choisi le fils
      HNOD = 0
      IF (ERR_GOOD()) THEN
         IF (LRES) THEN
            HNOD = AL_NODO_HLFT(IOB)
         ELSE IF (.NOT. LSTP) THEN
            HNOD = AL_NODO_HRHT(IOB)
            IF (HSVPT .NE. 0) THEN
               TS_C = TSIM
               DT_C = DELT
               IERR = SO_SVPT_RBCK(HSVPT)

               CALL LOG_INCIND()
               WRITE(LOG_BUF,'(3A)') 'MSG_ROLLBACK'
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TS_C
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_DELT#<35>#', '= ', DT_C
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               CALL LOG_DECIND()

            ENDIF
         ENDIF
      ENDIF

C---     Libère le point de sauvegarde
      IF (HSVPT .NE. 0) IERR = SO_SVPT_DTR(HSVPT)

C---     Résous le fils
      IF (ERR_GOOD() .AND. HNOD .NE. 0) THEN
         IERR = AL_SLVR_RESOUS(HNOD, TS_C, DT_C, HSIM)
         ISTTS = AL_SLVR_REQSTATUS(HNOD)
      ENDIF

C---     Ajuste TSIM et DELT
      TSIM = TS_C
      DELT = DT_C

C---     Conserve le résultat
      AL_NODO_STTS(IOB) = ISTTS

      AL_NODO_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_NODO_RESOUS_E(...) est la méthode de bas niveau pour
C     la résolution.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM        Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_MDTK      Fonction de calcul de [M.dt + K]
C     EXTERNAL F_MDTKT     Fonction de calcul de [M.dt + KT]
C     EXTERNAL F_MDTKU     Fonction de calcul de [M.dt + K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NODO_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          H_F,
     &                          F_K,
     &                          F_KT,
     &                          F_KU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      INTEGER  H_F
      INTEGER  F_K
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_K
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alnodo.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'alnodo.fc'

      REAL*8  TS_C, DT_C
      INTEGER IERR
      INTEGER IOB
      INTEGER HSVPT, IT, NITER
      INTEGER HNOD, HALG, HXPR
      INTEGER LDLG
      INTEGER ISTTS, IMASK
      LOGICAL LRES, LSTP, LRST
C------------------------------------------------------------------------
      INTEGER ix, im
      !! INTEGER BITMASK_SET, BITMASK_CLEAR, BITMASK_FLIP
      INTEGER BITMASK_CHECK_ALL, BITMASK_CHECK_ANY
      !! BITMASK_SET      (ix, im) =  IOR (ix, im)
      !! BITMASK_CLEAR    (ix, im) =  IAND(ix, INOT(im))
      !! BITMASK_FLIP     (ix, im) =  IEOR(ix, im)
      !! BITMASK_CHECK_ALL(ix, im) = (IAND(ix, im) .EQ. im)
      BITMASK_CHECK_ANY(ix, im) =  IAND(ix, im)
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------
      IMASK = 0 
      !! IMASK = IBSET(IMASK, AL_STTS_CRIA_BAD)
      !! IMASK = IBSET(IMASK, AL_STTS_GLOB_LIM)
      !! IMASK = IBSET(IMASK, AL_STTS_CRIC_RJT)
      !! IMASK = IBSET(IMASK, AL_STTS_CRIC_OSC)
      IMASK = IBSET(IMASK, AL_STTS_CINC_COUPE)
      IMASK = IBSET(IMASK, AL_STTS_CINC_DTMIN)
      IMASK = IBSET(IMASK, AL_STTS_CINC_SKIP)
      IMASK = IBSET(IMASK, AL_STTS_CINC_STOP)

C---     Récupère les attributs
      IOB  = HOBJ - AL_NODO_HBASE
      HALG = AL_NODO_HALG (IOB)
      NITER= AL_NODO_NITER(IOB)
      HXPR = AL_NODO_HXPR (IOB)
      HNOD = AL_NODO_HRHT (IOB)      ! le noeud négatif
      LRST = AL_NODO_LRST (IOB)      ! do reset

C---     Récupère les paramètres de la simulation
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)

C---     Crée un point de sauvegarde
      HSVPT = 0
      IF (LRST .AND. HNOD .NE. 0) THEN
         IERR = SO_SVPT_CTR(HSVPT)
         IERR = SO_SVPT_INI(HSVPT, LDLG)
      ENDIF

C---     Boucle sur les itérations
      TS_C = TSIM
      DT_C = DELT
      LRES = .TRUE.
      ISTTS = AL_STTS_INDEFINI
      DO IT=1,NITER

C---        Exécute l'algorithme
         IF (ERR_GOOD()) THEN
            IERR = AL_SLVR_RESOUS_E(HALG,
     &                              TS_C,
     &                              DT_C,
     &                              HSIM,
     &                              H_F,
     &                              F_K,
     &                              F_KT,
     &                              F_KU,
     &                              F_F,
     &                              F_RES)
            ISTTS = AL_SLVR_REQSTATUS(HALG)
         ENDIF

C---        Évalue l'expression
         LRES = .FALSE.    ! Est résolu
         LSTP = .FALSE.    ! Coupe, dtmin, stop ...
         IF (ERR_GOOD()) THEN
            IF (IC_EXPR_HVALIDE(HXPR)) THEN
               IERR = IC_EXPR_XEQ_L(HXPR, LRES)
            ELSE
               LRES = (ISTTS .EQ. AL_STTS_OK)
               LSTP = (BITMASK_CHECK_ANY(ISTTS, IMASK) .NE. 0)
            ENDIF
         ENDIF

         IF (LRES) EXIT
         IF (LSTP) EXIT
         IF (ERR_BAD()) EXIT
      ENDDO

C---     Stop?      
      LSTP = .FALSE.    ! Stop
      IF (ISTTS .NE. AL_STTS_INDEFINI) 
     &   LSTP = BTEST(ISTTS, AL_STTS_CINC_STOP)
      
C---     Choisi le fils
      HNOD = 0
      IF (ERR_GOOD()) THEN
         IF (LRES) THEN
            HNOD = AL_NODO_HLFT(IOB)
         ELSE IF (.NOT. LSTP) THEN
            HNOD = AL_NODO_HRHT(IOB)
            IF (HSVPT .NE. 0) THEN
               TS_C = TSIM
               DT_C = DELT
               IERR = SO_SVPT_RBCK(HSVPT)

               CALL LOG_INCIND()
               WRITE(LOG_BUF,'(3A)') 'MSG_ROLLBACK'
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TS_C
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_DELT#<35>#', '= ', DT_C
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               CALL LOG_DECIND()

            ENDIF
         ENDIF
      ENDIF

C---     Libère le point de sauvegarde
      IF (HSVPT .NE. 0) IERR = SO_SVPT_DTR(HSVPT)

C---     Appelle un des fils
      IF (ERR_GOOD() .AND. HNOD .NE. 0) THEN
         IERR = AL_SLVR_RESOUS_E(HNOD,
     &                           TS_C,
     &                           DT_C,
     &                           HSIM,
     &                           H_F,
     &                           F_K,
     &                           F_KT,
     &                           F_KU,
     &                           F_F,
     &                           F_RES)
         ISTTS = AL_SLVR_REQSTATUS(HNOD)
      ENDIF

C---     Ajuste TSIM et DELT
      TSIM = TS_C
      DELT = DT_C

C---     Conserve le résultat
      AL_NODO_STTS(IOB) = ISTTS

      AL_NODO_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NODO_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NODO_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alnodo.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnodo.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_NODO_REQSTATUS = AL_NODO_STTS(HOBJ - AL_NODO_HBASE)
      RETURN
      END
