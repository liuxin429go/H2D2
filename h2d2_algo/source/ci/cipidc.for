C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Contrôle d'Incrément
C Objet:   Proportional-Integral-Derivative (PID) Control
C Type:    Concret
C Note: 
C  1) P.M. Burrage, R.Herdiana, K.Burrage.
C     Adaptive stepsize based on control theory for stochastic differential equations
C     Journal of Computational and Applied Mathematics 170 (2004) 317 – 336
C     <br>
C     La formule utilisée est:
C     </pre>
C           FMUL = F_M1 ** XPF1
C    &           * (ETGT / R) ** XPR0
C    &           * (ETGT / R_M1) ** XPR1
C    &           * (ETGT / R_M2) ** XPR2
C     </pre>
C     Les résultats de Burrage & al. proposent d'utiliser H211b ou H312.
C     <br>
C     Tiré de (1): H312
C        kr0 = kI/4
C        kr1 = kI/2
C        kr2 = kI/4
C        kf1 = 1
C        avec:
C           k*kI = 2/9 (paramètres de Burrage, en principe pour k=2+1)
C        donne:
C               k = 2                    k = 3
C           kI  = 0.111111            kI = 0.074074
C           kr0 = 0.027778            k0 = 0.018519
C           kr1 = 0.055556            k1 = 0.037037
C           kr2 = 0.027778            k2 = 0.018519
C     <br>
C     Tiré de (1): H321 
C        kr0 = 3*kI/4 + kP/2
C        kr1 = kI/2
C        kr2 = -(kI/4 + kP/2)
C        kf1 = 1
C        avec:
C           k = 2
C           k*kI = 0.10 (paramètres de Söderlind)
C           k*kP = 0.45
C        donne:
C           kI, kP = (0.050, 0.225)
C           kr0 =  0.150
C           kr1 =  0.025
C           kr2 = -0.125
C     <br>
C     Tiré de (2): H211b 
C        kr0 = 1 / (b*k)
C        kr1 = 1 / (b*k)
C        kr2 = 0.0
C        kf1 = - 1/b
C        avec:
C           b = 4 (paramètres de Söderlind)
C        donne:
C               k = 2
C           kr0 = 0.125
C           kr1 = 0.125
C           kr2 = 0.000
C           kf1 =-0.250
C
C Functions:
C   Public:
C     INTEGER CI_PIDC_000
C     INTEGER CI_PIDC_999
C     INTEGER CI_PIDC_CTR
C     INTEGER CI_PIDC_DTR
C     INTEGER CI_PIDC_INI
C     INTEGER CI_PIDC_RST
C     INTEGER CI_PIDC_REQHBASE
C     LOGICAL CI_PIDC_HVALIDE
C     INTEGER CI_PIDC_ASGXPO
C     INTEGER CI_PIDC_CLCDEL
C     INTEGER CI_PIDC_CLCERR
C     INTEGER CI_PIDC_DEB
C     INTEGER CI_PIDC_REQXPO
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CI_PIDC_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PIDC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cipidc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CI_PIDC_NOBJMAX,
     &                   CI_PIDC_HBASE,
     &                'Increment Controller - PID Adaptative Control')

      CI_PIDC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset la classe.
C
C Description:
C     La fonction <code>CI_PIDC_999(...)</code> désalloue tous les objets
C     non encore désalloués. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets pour nettoyer les tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PIDC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cipidc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

      INTEGER  IERR
      EXTERNAL CI_PIDC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CI_PIDC_NOBJMAX,
     &                   CI_PIDC_HBASE,
     &                   CI_PIDC_DTR)

      CI_PIDC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe.
C
C Description:
C     Le constructeur <code>CI_PIDC_CTR(...)</code> construit un objet
C     et retourne son handle.
C     <p>
C     L'objet est initialisé aux valeurs par défaut. C'est l'appel à
C     <code>CI_PIDC_INI(...)</code> qui l'initialise avec des valeurs
C     utilisateurs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PIDC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cipidc.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CI_PIDC_NOBJMAX,
     &                   CI_PIDC_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CI_PIDC_HVALIDE(HOBJ))
         IOB = HOBJ - CI_PIDC_HBASE

         CI_PIDC_XPR0(IOB) =  0.125D0
         CI_PIDC_XPR1(IOB) =  0.125D0
         CI_PIDC_XPR2(IOB) =  0.000D0
         CI_PIDC_XPF1(IOB) = -0.250D0
         DO ID=1, CI_PIDC_NEPSMAX
            CI_PIDC_EPSA(ID, IOB) = 1.0D-4
            CI_PIDC_EPSR(ID, IOB) = 1.0D-4
         ENDDO
         CI_PIDC_ETGT(IOB) =  1.0D0
         CI_PIDC_EMAX(IOB) = 10.0D0
         CI_PIDC_R_M1(IOB) = -1.0D0
         CI_PIDC_R_M2(IOB) =  0.0D0
         CI_PIDC_F_M1(IOB) =  1.0D0
         CI_PIDC_NEPS(IOB) = 0
         CI_PIDC_INRM(IOB) = CI_CINC_NRM_INDEFINI
      ENDIF

      CI_PIDC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     Le destructeur <code>CI_PICD_DTR(...)</code> détruis l'objet et
C     tous ses attributs. En sortie, le handle n'est plus valide et
C     son utilisation n'est plus permise.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PIDC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cipidc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PIDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_PIDC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CI_PIDC_NOBJMAX,
     &                   CI_PIDC_HBASE)

      CI_PIDC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet.
C
C Description:
C     La méthode <code>CI_PIDC_INI(...)</code> initialise l'objet avec les
C     valeurs passées en arguments.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     DTMIN    DeltaT MINimal
C     HELE     Handle sur l'élément
C     NEPS     Nombre d'epsilon
C     VEPSR    Epsilon relatif
C     VEPSA    Epsilon absolue
C     ETGT     Target error
C     EMAX     Max error over a step
C     INRM     Type of norm
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PIDC_INI(HOBJ,
     &                     DTMIN,
     &                     HELE,
     &                     NEPS,
     &                     VEPSR,
     &                     VEPSA,
     &                     ETGT,
     &                     EMAX,
     &                     INRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTMIN
      INTEGER HELE
      INTEGER NEPS
      REAL*8  VEPSR(NEPS)
      REAL*8  VEPSA(NEPS)
      REAL*8  ETGT
      REAL*8  EMAX
      INTEGER INRM

      INCLUDE 'cipidc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cipidc.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ID, IV
      INTEGER NDLN
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PIDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (DTMIN .LE. 0.0D0) GOTO 9900
      IF (.NOT. LM_HELE_HVALIDE(HELE)) GOTO 9901
      NDLN = LM_HELE_REQPRM(HELE, LM_HELE_PRM_NDLN)
      IF (NEPS .NE. 1 .AND. NEPS .NE. NDLN) GOTO 9902
      DO ID=1,NEPS
         IF (VEPSA(ID) .LE. 1.0D-16) GOTO 9903
         IF (VEPSA(ID) .GT. 1.0D+08) GOTO 9903
      ENDDO
      IF (EMAX .LE. ETGT) GOTO 9904

C---     Reset les données
      IERR = CI_PIDC_RST(HOBJ)

C---     Assigne les attributs
      IOB = HOBJ - CI_PIDC_HBASE
      CI_PIDC_XPR0(IOB) =  0.125D0
      CI_PIDC_XPR1(IOB) =  0.125D0
      CI_PIDC_XPR2(IOB) =  0.000D0
      CI_PIDC_XPF1(IOB) = -0.250D0
      DO ID=1,NDLN
         IV = MIN(ID, NEPS)
         CI_PIDC_EPSA(ID, IOB) = VEPSA(IV)
         CI_PIDC_EPSR(ID, IOB) = VEPSR(IV)
      ENDDO
      CI_PIDC_ETGT(IOB) = ETGT
      CI_PIDC_EMAX(IOB) = EMAX
      CI_PIDC_R_M1(IOB) = -1.0D0
      CI_PIDC_R_M2(IOB) =  0.0D0
      CI_PIDC_F_M1(IOB) =  1.0D0
      CI_PIDC_DTMI(IOB) = DTMIN
      CI_PIDC_HELE(IOB) = HELE
      CI_PIDC_NEPS(IOB) = NDLN
      CI_PIDC_INRM(IOB) = INRM

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMIN
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6)') 'ERR_HANDLE_INVALIDE',':',
     &                           HELE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HELE, 'MSG_SIMULATION')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NOMBRE_VALEUR_INVALIDE',':',
     &                           NEPS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,1PE14.6E3)')
     &   'ERR_VALEUR_INVALIDE',':', VEPSA(ID)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A,2(A,1PE14.6E3))')
     &   'ERR_BORNES_INVALIDES', ':', ETGT, ' > ', EMAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_PIDC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PIDC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cipidc.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PIDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CI_PIDC_HBASE
      CI_PIDC_XPR0(IOB) =  0.125D0
      CI_PIDC_XPR1(IOB) =  0.125D0
      CI_PIDC_XPR2(IOB) =  0.000D0
      CI_PIDC_XPF1(IOB) = -0.250D0
      DO ID=1, CI_PIDC_NEPSMAX
         CI_PIDC_EPSA(ID, IOB) = 1.0D-4
         CI_PIDC_EPSR(ID, IOB) = 1.0D-4
      ENDDO
      CI_PIDC_ETGT(IOB) =  1.0D0
      CI_PIDC_ETGT(IOB) = 10.0D0
      CI_PIDC_R_M1(IOB) = -1.0D0
      CI_PIDC_R_M2(IOB) =  0.0D0
      CI_PIDC_F_M1(IOB) =  1.0D0
      CI_PIDC_DTMI(IOB) = -1.0D0
      CI_PIDC_HELE(IOB) = 0
      CI_PIDC_NEPS(IOB) = 0
      CI_PIDC_INRM(IOB) = CI_CINC_NRM_INDEFINI

      CI_PIDC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CI_PIDC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PIDC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cipidc.fi'
      INCLUDE 'cipidc.fc'
C------------------------------------------------------------------------

      CI_PIDC_REQHBASE = CI_PIDC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CI_PIDC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PIDC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cipidc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cipidc.fc'
C------------------------------------------------------------------------

      CI_PIDC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CI_PIDC_NOBJMAX,
     &                                  CI_PIDC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assigne les exposants
C
C Description:
C     La méthode CI_PIDC_ASGXPO permet d'assigner les valeurs des
C     exposants utilisés dans le calcul du facteur multiplicatif.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PIDC_ASGXPO(HOBJ, XPR0, XPR1, XPR2, XPF1)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_ASGXPO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  XPR0
      REAL*8  XPR1
      REAL*8  XPR2
      REAL*8  XPF1

      INCLUDE 'cipidc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PIDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Assigne les attributs
      IOB = HOBJ - CI_PIDC_HBASE
      CI_PIDC_XPR0(IOB) = XPR0
      CI_PIDC_XPR1(IOB) = XPR1
      CI_PIDC_XPR2(IOB) = XPR2
      CI_PIDC_XPF1(IOB) = XPF1

      CI_PIDC_ASGXPO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne l'incrément effectif.
C
C Description:
C     La fonction <code>CI_PIDC_ASGDTEF(...)</code> permet d'assigner
C     par l'extérieur la pas de temps effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     DTEF        Incrément effectif
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PIDC_ASGDTEF(HOBJ, DTEF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_ASGDTEF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTEF

      INCLUDE 'cipidc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PIDC_HVALIDE(HOBJ))
D     CALL ERR_PRE(DTEF .LE. CI_PIDC_DTEF(HOBJ-CI_PIDC_HBASE))
C------------------------------------------------------------------------

      CI_PIDC_DTEF(HOBJ-CI_PIDC_HBASE) = DTEF

      CI_PIDC_ASGDTEF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_PIDC_CLCDEL(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C     DELT(3)     Status
C
C Notes:
C     1) Un pas tronqué peut générer un !OK. FMUL s'applique sur DTEF
C************************************************************************
      FUNCTION CI_PIDC_CLCDEL(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_CLCDEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VDLG(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VDEL(NDLN, NNL)
      LOGICAL, INTENT(IN) :: ESTOK
      REAL*8,  INTENT(OUT):: DELT(3)

      INCLUDE 'cipidc.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'cipidc.fc'

      INTEGER IERR
      INTEGER IOB

      REAL*8  R, R_D, R_M1, R_M2, F_M1, FMUL
      REAL*8  XPR0, XPR1, XPR2, XPF1
      REAL*8  EPSA, EPSR, ETGT, EMAX
      REAL*8  TACT, TFIN
      REAL*8  DTMI, DTCI, DTEF, DTRST
      INTEGER ID, INRM, ISTTS
      INTEGER HELE, HNUMR
      LOGICAL DOCLC, DOMUL, DOLMT, DOINC
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PIDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.cinc.pid'

C---     Récupère les attributs
      IOB = HOBJ - CI_PIDC_HBASE
      XPR0 = CI_PIDC_XPR0(IOB)
      XPR1 = CI_PIDC_XPR1(IOB)
      XPR2 = CI_PIDC_XPR2(IOB)
      XPF1 = CI_PIDC_XPF1(IOB)
      ETGT = CI_PIDC_ETGT(IOB)
      EMAX = CI_PIDC_EMAX(IOB)
      R_M1 = CI_PIDC_R_M1(IOB)
      R_M2 = CI_PIDC_R_M2(IOB)
      F_M1 = CI_PIDC_F_M1(IOB)
      DTMI = CI_PIDC_DTMI(IOB)
      TACT = CI_PIDC_TACT(IOB)
      TFIN = CI_PIDC_TFIN(IOB)
      DTCI = CI_PIDC_DTCI(IOB)
      DTEF = CI_PIDC_DTEF(IOB)
      HELE = CI_PIDC_HELE(IOB)
      INRM = CI_PIDC_INRM(IOB)
D     CALL ERR_ASR(CI_PIDC_NEPS(IOB) .EQ. NDLN)

C---     Switch sur le type de pas
      IF (.NOT. ESTOK) THEN               ! Pas non convergé
         FMUL =  0.5D0                    !  Coupe dT
         R    = -1.0D0                    !  Réinitialise
         DOCLC = .FALSE.                  !  Calcule la norme
         DOMUL = .TRUE.                   !  Applique FMUL
         DOLMT = .FALSE.                  !  Limite le pas
         DOINC = .FALSE.                  !  Incrémente
         DTCI  = DTEF                     !  cf. note 1
      ELSEIF (TACT+DTEF .LT. TFIN) THEN   ! Pas intermédiaire
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .TRUE.
         DOINC = .TRUE.
      ELSEIF (DTEF .GE. DTCI) THEN        ! Pas final normal
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ELSE                                ! Pas final tronqué
         FMUL = F_M1                      !  Rétablis au pas calculé
         R_M1 = R_M2
         R    = R_M1
         DOCLC = .FALSE.
         DOMUL = .FALSE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ENDIF

C---     Calcule le max des normes relative
      IF (DOCLC) THEN
         HNUMR = LM_HELE_REQHNUMC(HELE)
         R = -1.0D99
         DO ID=1,NDLN
            EPSA = CI_PIDC_EPSA(ID, IOB)
            EPSR = CI_PIDC_EPSR(ID, IOB)
            IF    (INRM .EQ. CI_CINC_NRM_L2) THEN
               R_D = NR_UTIL_N2SR(HNUMR,NDLN,NNL,VDEL,ID,VDLG,EPSA,EPSR)
            ELSEIF (INRM .EQ. CI_CINC_NRM_MAX) THEN
               R_D = NR_UTIL_NISR(HNUMR,NDLN,NNL,VDEL,ID,VDLG,EPSA,EPSR)
D           ELSE
D              CALL ERR_ASR(INRM .NE. CI_CINC_NRM_INDEFINI)
            ENDIF
            R = MAX(R, R_D)
         ENDDO
      ENDIF

C---     Calcule le facteur multiplicatif
      IF (DOCLC) THEN
         IF (R .GE. EMAX) THEN               ! Erreur trop grande
            WRITE(LOG_BUF, '(A,2(A,1PE14.6E3))') 'MSG_EMAX_DEPASSE',
     &               ': ', R, ' / ', EMAX
            CALL LOG_INFO(LOG_ZNE, LOG_BUF)
            FMUL =  0.5D0                    !  Coupe dT
            R    = -1.0D0                    !  Réinitialise
            DOCLC = .FALSE.                  !  Calcule la norme
            DOLMT = .FALSE.                  !  Limite le pas
            DOINC = .FALSE.
         ELSEIF (R_M1 .LE. 0.0D0) THEN
            FMUL = F_M1 ** XPF1
     &           * (ETGT / R) ** XPR0
         ELSEIF (R_M2 .LE. 0.0D0) THEN
            FMUL = F_M1 ** XPF1
     &           * (ETGT / R) ** XPR0
     &           * (ETGT / R_M1) ** XPR1
         ELSE
            FMUL = F_M1 ** XPF1
     &           * (ETGT / R) ** XPR0
     &           * (ETGT / R_M1) ** XPR1
     &           * (ETGT / R_M2) ** XPR2
         ENDIF
      ENDIF

C---     Applique FMUL
      IF (DOMUL) DTCI = DTCI*FMUL

C---     Status
      ISTTS = CI_CINC_STATUS_UNDEFINED
      IF (DOINC)           ISTTS = CI_CINC_STATUS_OK
      IF (.NOT. DOINC)     ISTTS = CI_CINC_STATUS_COUPE
      IF (DTCI .LT. DTMI)  ISTTS = CI_CINC_STATUS_DTMIN

C---     Limite au pas min
      IF (DOMUL) THEN
         DTCI = MAX(DTCI, DTMI)
      ENDIF

C---     Incrémente le pas
      IF (DOINC) THEN
         TACT = TACT + DTEF
      ENDIF

C---     Limite le pas
      IF (DOLMT) THEN
         DTRST = TFIN - (TACT + DTCI)
         IF (DTRST .GT. 0.0D0 .AND. DTRST < (0.05D0*DTCI)) THEN
            DTEF = TFIN-TACT
            FMUL = FMUL*(DTEF/DTCI)
         ELSE
            DTEF = MIN(DTCI, TFIN-TACT)
         ENDIF
      ELSE
         DTEF = DTCI
      ENDIF

C---     Log
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_CRITERE#<35>#', '= ', R
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_CRI_REL#<35>#', '= ', R/ETGT
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_FACMUL#<35>#', '= ', FMUL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_CI#<35>#', '= ', DTCI
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_EFF#<35>#', '= ', DTEF
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Conserve les valeurs
      CI_PIDC_R_M1(IOB) = R
      CI_PIDC_R_M2(IOB) = R_M1
      CI_PIDC_F_M1(IOB) = FMUL
      CI_PIDC_TACT(IOB) = TACT
      CI_PIDC_DTCI(IOB) = DTCI
      CI_PIDC_DTEF(IOB) = DTEF

C---     Valeurs retournées
      DELT(1) = DTCI
      DELT(2) = DTEF
      DELT(3) = ISTTS

      CI_PIDC_CLCDEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_PIDC_CLCERR(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     VERR        Table des ERReurs
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C************************************************************************
      FUNCTION CI_PIDC_CLCERR(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        VERR,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_CLCERR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VDLG(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VDEL(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VERR(NDLN, NNL)
      LOGICAL, INTENT(IN) :: ESTOK
      REAL*8,  INTENT(OUT):: DELT(3)

      INCLUDE 'cipidc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PIDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_PIDC_CLCDEL(HOBJ,
     &                      NDLN,
     &                      NNL,
     &                      VDLG,
     &                      VDEL,
     &                      ESTOK,
     &                      DELT)

      CI_PIDC_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode <code>CI_PIDC_DEB(...)</code> marque le début d'une
C     séquence de pas qui vont être adaptés.
C
C Entrée:
C     TINI     Temps initial de la séquence
C     TFIN     Temps de fin de la séquence
C     DT0      Pas initial
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PIDC_DEB(HOBJ, TINI, TFIN, DT0)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TINI
      REAL*8  TFIN
      REAL*8  DT0

      INCLUDE 'cipidc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

      REAL*8  DTMI
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PIDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Indice de l'objet
      IOB = HOBJ - CI_PIDC_HBASE

C---     Contrôles
      DTMI = CI_PIDC_DTMI(IOB)
      IF (DT0 .LT. DTMI) GOTO 9900

C---     Assigne les valeurs
      CI_PIDC_TACT(IOB) = TINI
      CI_PIDC_TFIN(IOB) = TFIN
      IF (CI_PIDC_DTCI(IOB) .NE. DT0) THEN
         CI_PIDC_R_M1(IOB) = -1.0D0
         CI_PIDC_R_M2(IOB) =  0.0D0
         CI_PIDC_F_M1(IOB) =  1.0D0
      ENDIF
      CI_PIDC_DTCI(IOB) = DT0
      CI_PIDC_DTEF(IOB) = DT0

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_INITIAL', ': ', DT0
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMI
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_PIDC_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les exposants
C
C Description:
C     La méthode CI_PIDC_REQXPO retourne les valeurs des
C     exposants utilisés dans le calcul du facteur multiplicatif.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PIDC_REQXPO(HOBJ, XPR0, XPR1, XPR2)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PIDC_REQXPO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  XPR0
      REAL*8  XPR1
      REAL*8  XPR2

      INCLUDE 'cipidc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cipidc.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PIDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Assigne les attributs
      IOB = HOBJ - CI_PIDC_HBASE
      XPR0 = CI_PIDC_XPR0(IOB)
      XPR1 = CI_PIDC_XPR1(IOB)
      XPR2 = CI_PIDC_XPR2(IOB)

      CI_PIDC_REQXPO = ERR_TYP()
      RETURN
      END
