C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Functions:
C   Public:
C     SUBROUTINE IC_CI_COMP_XEQCTR
C     SUBROUTINE IC_CI_COMP_XEQMTH
C     SUBROUTINE IC_CI_COMP_REQCLS
C     SUBROUTINE IC_CI_COMP_REQHDL
C   Private:
C     SUBROUTINE IC_CI_COMP_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_COMP_XEQCTR(IPRM)
CDEC$ATTRIBUTES DLLEXPORT :: IC_CI_COMP_XEQCTR

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'cicomp_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'cicomp.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HCINC, HOBJ
      INTEGER I
      INTEGER NCINC
      INTEGER NCINCMAX
      PARAMETER (NCINCMAX = 20)
      INTEGER KCRIC(NCINCMAX)
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_CI_COMP_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_CINC_COMPOSE'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Lis les param
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = 0
100   CONTINUE
         IF (I .EQ. NCINCMAX) GOTO 9901
         I = I + 1
C        <comment>List of the increment controller to compose, comma separated</comment>
         IERR = SP_STRN_TKI(IPRM, ',', I, KCRIC(I))
         IF (IERR .EQ. -1) GOTO 199
         IF (IERR .NE.  0) GOTO 9902
      GOTO 100
199   CONTINUE
      NCINC = I-1

C---     Valide
      IF (NCINC .LE. 0) GOTO 9904

C---     Impression des paramètres du bloc
      IF (ERR_GOOD()) THEN
         WRITE (LOG_BUF,'(2A,I6)') 'MSG_NBR_OBJ_COMPOSE#<25>#',
     &                             '= ', NCINC
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Construis et initialise l'objet
      HCINC = 0
      IF (ERR_GOOD()) IERR = CI_COMP_CTR(HCINC)
      IF (ERR_GOOD()) IERR = CI_COMP_INI(HCINC, NCINC, KCRIC)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = CI_CINC_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = CI_CINC_INI(HOBJ, HCINC)

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the increment controller</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>cinc_compose</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF,'(2A,I6)') 'ERR_NBR_CRIC_INVALIDE',': ', NCINC
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CI_COMP_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_CI_COMP_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_COMP_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ATTRIBUTES DLLEXPORT :: IC_CI_COMP_XEQMTH

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'cicomp_ic.fi'
      INCLUDE 'cicomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(CI_COMP_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CI_COMP_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(CI_COMP_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = CI_COMP_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test CI_COMP_PRN(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_CI_COMP_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CI_COMP_AID()

9999  CONTINUE
      IC_CI_COMP_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_COMP_REQCLS()
CDEC$ATTRIBUTES DLLEXPORT :: IC_CI_COMP_REQCLS

      IMPLICIT NONE

      INCLUDE 'cicomp_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>cinc_compose</b> represents the composition of two or more
C  increment controllers (Status: Experimental).
C</comment>
      IC_CI_COMP_REQCLS = 'cinc_compose'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_COMP_REQHDL()
CDEC$ATTRIBUTES DLLEXPORT :: IC_CI_COMP_REQHDL

      IMPLICIT NONE

      INCLUDE 'cicomp_ic.fi'
      INCLUDE 'cicomp.fi'
C-------------------------------------------------------------------------

      IC_CI_COMP_REQHDL = CI_COMP_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_CI_COMP_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_CI_COMP_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('cicomp_ic.hlp')
      RETURN
      END
