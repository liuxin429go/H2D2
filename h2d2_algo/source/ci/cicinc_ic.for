C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Interface Commandes: Contrôle d'Incrément
C Objet:   Parent et proxy
C Type:    ---
C Functions:
C   Public:
C     INTEGER IC_CI_CINC_XEQCTR
C     INTEGER IC_CI_CINC_XEQMTH
C     CHARACTER*(32) IC_CI_CINC_REQCLS
C     INTEGER IC_CI_CINC_REQHDL
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Fonction vide qui ne devrait jamais être appelée.
C
C Description:
C     La fonction IC_CI_CINC_XEQCTR(IPRM) est une fonction vide car il
C     n'est pas prévu de construire un proxy via une commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_CINC_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_CINC_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'cicinc_ic.fi'
      INCLUDE 'err.fi'

C------------------------------------------------------------------------
      CALL ERR_PRE(.FALSE.)
C------------------------------------------------------------------------

      IPRM = ' '

      IC_CI_CINC_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_CI_CINC_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type CI_CINC.
C     Le proxy résout directement les appels aux méthodes virtuelles.
C     Pour toutes les autres méthodes, on retourne le handle de l'objet
C     géré.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_CINC_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_CINC_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'cicinc_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'cicinc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HVAL
      REAL*8  RVAL, RDUM
      LOGICAL LVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

         CALL LOG_TODO('CI_CINC_XEQMTH: activer prop')
         CALL ERR_ASR(.FALSE.)
!C        <comment>Scaling factor</comment>
!         IF     (PROP .EQ. 'dt_nominal') THEN
!            IERR = CI_CINC_REQDELT(HOBJ, RVAL, RDUM)
!            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
!         ELSEIF (PROP .EQ. 'dt_effectif') THEN
!            IERR = CI_CINC_REQDELT(HOBJ, RDUM, RVAL)
!            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
!         ELSE
            GOTO 9902
!         ENDIF

C     <comment>Class destructor</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CI_CINC_DTR(HOBJ)

      ELSE
         IOB = HOBJ - CI_CINC_HBASE
         HVAL = CI_CINC_HOMG(IOB)
         WRITE(IPRM, '(2A,I12)') 'X', ',', HVAL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_CI_CINC_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_CINC_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_CINC_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicinc_ic.fi'
C-------------------------------------------------------------------------

      IC_CI_CINC_REQCLS = '#__dummy_placeholder__#__CI_CINC__'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_CINC_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_CINC_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicinc_ic.fi'
      INCLUDE 'cicinc.fi'
C-------------------------------------------------------------------------

      IC_CI_CINC_REQHDL = CI_CINC_REQHBASE()
      RETURN
      END
