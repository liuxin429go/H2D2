C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Contrôle d'Incrément
C Objet:   SiMPLe
C Type:    Concret
C
C Functions:
C   Public:
C     INTEGER CI_SMPL_000
C     INTEGER CI_SMPL_999
C     INTEGER CI_SMPL_CTR
C     INTEGER CI_SMPL_DTR
C     INTEGER CI_SMPL_INI
C     INTEGER CI_SMPL_RST
C     INTEGER CI_SMPL_REQHBASE
C     LOGICAL CI_SMPL_HVALIDE
C     INTEGER CI_SMPL_CLCDEL
C     INTEGER CI_SMPL_CLCERR
C     INTEGER CI_SMPL_DEB
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CI_SMPL_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_SMPL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cismpl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cismpl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CI_SMPL_NOBJMAX,
     &                   CI_SMPL_HBASE,
     &                   'Increment Controller - Simple')

      CI_SMPL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset la classe.
C
C Description:
C     La fonction <code>CI_SMPL_999(...)</code> désalloue tous les objets
C     non encore désalloués. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets pour nettoyer les tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_SMPL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cismpl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cismpl.fc'

      INTEGER  IERR
      EXTERNAL CI_SMPL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CI_SMPL_NOBJMAX,
     &                   CI_SMPL_HBASE,
     &                   CI_SMPL_DTR)

      CI_SMPL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe.
C
C Description:
C     Le constructeur <code>CI_SMPL_CTR(...)</code> construit un objet
C     et retourne son handle.
C     <p>
C     L'objet est initialisé aux valeurs par défaut. C'est l'appel à
C     <code>CI_SMPL_INI(...)</code> qui l'initialise avec des valeurs
C     utilisateurs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_SMPL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cismpl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cismpl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CI_SMPL_NOBJMAX,
     &                   CI_SMPL_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CI_SMPL_HVALIDE(HOBJ))
         IOB = HOBJ - CI_SMPL_HBASE

         CI_SMPL_TACT(IOB) = 0.0D0
         CI_SMPL_TFIN(IOB) = 0.0D0
         CI_SMPL_DTCI(IOB) =-1.0D0
         CI_SMPL_DTEF(IOB) =-1.0D0
         CI_SMPL_NPOK(IOB) = 0
      ENDIF

      CI_SMPL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     Le destructeur <code>CI_PICD_DTR(...)</code> détruis l'objet et
C     tous ses attributs. En sortie, le handle n'est plus valide et
C     son utilisation n'est plus permise.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_SMPL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cismpl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cismpl.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_SMPL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_SMPL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CI_SMPL_NOBJMAX,
     &                   CI_SMPL_HBASE)

      CI_SMPL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet.
C
C Description:
C     La méthode <code>CI_SMPL_INI(...)</code> initialise l'objet avec les
C     valeurs passées en arguments.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     DTMIN    DeltaT MINimal
C     DTNOM    DeltaT NOMinal
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_SMPL_INI(HOBJ, DTMIN, DTNOM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTMIN
      REAL*8  DTNOM

      INCLUDE 'cismpl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'cismpl.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_SMPL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (DTNOM .LE. 0.0D0) GOTO 9900
      IF (DTMIN .LE. 0.0D0) GOTO 9900
      IF (DTMIN .GT. DTNOM) GOTO 9900

C---     Reset les données
      IERR = CI_SMPL_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - CI_SMPL_HBASE
         CI_SMPL_TACT(IOB) = 0.0D0
         CI_SMPL_TFIN(IOB) = 0.0D0
         CI_SMPL_DTCI(IOB) =-1.0D0
         CI_SMPL_DTEF(IOB) =-1.0D0
         CI_SMPL_DTNO(IOB) = DTNOM
         CI_SMPL_DTMI(IOB) = DTMIN
         CI_SMPL_NPOK(IOB) = -1
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_NOMINAL', ': ', DTNOM
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMIN
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_SMPL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_SMPL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cismpl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cismpl.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_SMPL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CI_SMPL_HBASE
      CI_SMPL_TACT(IOB) = 0.0D0
      CI_SMPL_TFIN(IOB) = 0.0D0
      CI_SMPL_DTCI(IOB) =-1.0D0
      CI_SMPL_DTEF(IOB) =-1.0D0
      CI_SMPL_DTNO(IOB) =-1.0D0
      CI_SMPL_DTMI(IOB) =-1.0D0
      CI_SMPL_NPOK(IOB) = -1

      CI_SMPL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CI_SMPL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_SMPL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cismpl.fi'
      INCLUDE 'cismpl.fc'
C------------------------------------------------------------------------

      CI_SMPL_REQHBASE = CI_SMPL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CI_SMPL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_SMPL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cismpl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cismpl.fc'
C------------------------------------------------------------------------

      CI_SMPL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CI_SMPL_NOBJMAX,
     &                                  CI_SMPL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne l'incrément effectif.
C
C Description:
C     La fonction <code>CI_SMPL_ASGDTEF(...)</code> permet d'assigner
C     par l'extérieur la pas de temps effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     DTEF        Incrément effectif
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_SMPL_ASGDTEF(HOBJ, DTEF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_ASGDTEF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTEF

      INCLUDE 'cismpl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cismpl.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_SMPL_HVALIDE(HOBJ))
D     CALL ERR_PRE(DTEF .LE. CI_SMPL_DTEF(HOBJ-CI_SMPL_HBASE))
C------------------------------------------------------------------------

      CI_SMPL_DTEF(HOBJ-CI_SMPL_HBASE) = DTEF

      CI_SMPL_ASGDTEF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_SMPL_CLCDEL(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C     1) Un pas tronqué peut générer un !OK. FMUL s'applique sur DTEF
C     2) Le test est fragile mais pas dangereux. Si les multiplications
C        mènent à un pas légèrement inférieur, on va faire 2 pas avec
C        un pas presque nominal.
C************************************************************************
      FUNCTION CI_SMPL_CLCDEL(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_CLCDEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'cismpl.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'cismpl.fc'

      REAL*8  FMUL
      REAL*8  TACT, TFIN
      REAL*8  DTNO, DTCI, DTEF, DTRST, DTMI
      INTEGER IERR
      INTEGER IOB
      INTEGER NPOK, ISTTS
      LOGICAL DOCLC, DOMUL, DOLMT, DOINC
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_SMPL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.cinc.simple'

C---     Récupère les attributs
      IOB = HOBJ - CI_SMPL_HBASE
      TACT = CI_SMPL_TACT(IOB)
      TFIN = CI_SMPL_TFIN(IOB)
      DTCI = CI_SMPL_DTCI(IOB)
      DTEF = CI_SMPL_DTEF(IOB)
      DTNO = CI_SMPL_DTNO(IOB)
      DTMI = CI_SMPL_DTMI(IOB)
      NPOK = CI_SMPL_NPOK(IOB)

C---     Switch sur le type de pas
      FMUL = 1.0D0
      IF (.NOT. ESTOK) THEN               ! Pas non convergé
         FMUL = 0.5D0                     !  Coupe dT
         NPOK = 0                         !  Initialise le compteur
         DOCLC = .FALSE.                  !  Calcule la norme
         DOMUL = .TRUE.                   !  Applique FMUL
         DOLMT = .FALSE.                  !  Limite le pas
         DOINC = .FALSE.                  !  Incrémente
         DTCI  = DTEF                     !  cf. note 1
      ELSEIF (TACT+DTEF .LT. TFIN) THEN   ! Pas intermédiaire
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .TRUE.
         DOINC = .TRUE.
      ELSEIF (DTEF .GE. DTCI) THEN        ! Pas final normal
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ELSE                                ! Pas final tronqué
         DOCLC = .FALSE.                  !  Rétablis au pas calculé
         DOMUL = .FALSE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ENDIF

C---     Calcule le facteur multiplicatif
      IF (DOCLC) THEN
         IF (DTCI .LT. DTNO) THEN   ! cf. note 2)
            NPOK = NPOK + 1         ! Nombre de pas OK
            IF (NPOK .EQ. 2) THEN
               FMUL = SQRT(2.0D0)
               NPOK = 0
            ENDIF
         ENDIF
      ENDIF

C---     Applique FMUL
      IF (DOMUL) DTCI = DTCI*FMUL

C---     Status
      ISTTS = CI_CINC_STATUS_UNDEFINED
      IF (DOINC)          ISTTS = CI_CINC_STATUS_OK
      IF (.NOT. DOINC)    ISTTS = CI_CINC_STATUS_COUPE
      IF (DTCI .LT. DTMI) ISTTS = CI_CINC_STATUS_DTMIN

C---     Limite au pas nominal et au pas min
      IF (DOMUL) THEN
         DTCI = MIN(DTCI, DTNO)
         DTCI = MAX(DTCI, DTMI)
      ENDIF

C---     Incrémente le temps
      IF (DOINC) THEN
         TACT = TACT + DTEF
      ENDIF

C---     Limite le pas
      IF (DOLMT) THEN
         DTRST = TFIN - (TACT + DTCI)
         IF (DTRST .GT. 0.0D0 .AND. DTRST < (0.05D0*DTCI)) THEN
            DTEF = TFIN-TACT
            FMUL = FMUL*(DTEF/DTCI)
         ELSE
            DTEF = MIN(DTCI, TFIN-TACT)
         ENDIF
      ELSE
         DTEF = DTCI
      ENDIF

C---     Log
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_FACMUL#<35>#', '= ', FMUL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_CI#<35>#', '= ', DTCI
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_EFF#<35>#', '= ', DTEF
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Conserve les valeurs
      CI_SMPL_NPOK(IOB) = NPOK
      CI_SMPL_TACT(IOB) = TACT
      CI_SMPL_DTCI(IOB) = DTCI
      CI_SMPL_DTEF(IOB) = DTEF

C---     Valeurs retournées
      DELT(1) = DTCI
      DELT(2) = DTEF
      DELT(3) = ISTTS

      CI_SMPL_CLCDEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_SMPL_CLCERR(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     VERR        Table des ERReurs
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C************************************************************************
      FUNCTION CI_SMPL_CLCERR(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        VERR,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_CLCERR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  VERR(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'cismpl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cismpl.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_SMPL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_SMPL_CLCDEL(HOBJ,
     &                      NDLN,
     &                      NNL,
     &                      VDLG,
     &                      VDEL,
     &                      ESTOK,
     &                      DELT)

      CI_SMPL_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode <code>CI_SMPL_DEB(...)</code> marque le début d'une
C     séquence de pas qui vont être adaptés.
C
C Entrée:
C     TINI     Temps initial de la séquence
C     TFIN     Temps de fin de la séquence
C     DT0      Pas initial
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_SMPL_DEB(HOBJ, TINI, TFIN, DT0)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_SMPL_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TINI
      REAL*8  TFIN
      REAL*8  DT0

      INCLUDE 'cismpl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cismpl.fc'

      REAL*8  DTMI
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_SMPL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Indice de l'objet
      IOB = HOBJ - CI_SMPL_HBASE

C---     Contrôles
      DTMI = CI_SMPL_DTMI(IOB)
      IF (DT0 .LT. DTMI) GOTO 9900

C---     Assigne les valeurs
      CI_SMPL_TACT(IOB) = TINI
      CI_SMPL_TFIN(IOB) = TFIN
      CI_SMPL_DTCI(IOB) = DT0
      CI_SMPL_DTEF(IOB) = DT0

C---     Test pour une continuation
      IF (CI_SMPL_DTCI(IOB) .NE. DT0) THEN
         CI_SMPL_NPOK(IOB) = -1
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_INITIAL', ': ', DT0
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMI
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_SMPL_DEB = ERR_TYP()
      RETURN
      END
