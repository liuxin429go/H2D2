C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_CI_LMTR_XEQCTR
C     INTEGER IC_CI_LMTR_XEQMTH
C     CHARACTER*(32) IC_CI_LMTR_REQCLS
C     INTEGER IC_CI_LMTR_REQHDL
C   Private:
C     INTEGER IC_CI_LMTR_PRN
C     SUBROUTINE IC_CI_LMTR_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_LMTR_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_LMTR_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'cilmtr_ic.fi'
      INCLUDE 'cilmtr.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'cilmtr_ic.fc'

      INTEGER NVALMAX
      PARAMETER (NVALMAX = 10)

      INTEGER IERR
      INTEGER I, IV
      INTEGER HOBJ, HALG
      INTEGER HELE
      INTEGER NTOK, NVAL
      REAL*8  DTMIN
      REAL*8  VVAL(NVALMAX)
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_CI_LMTR_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Les dimensions
      NTOK = SP_STRN_NTOK(IPRM, ',')
      NVAL = NTOK - 1
      IF (NVAL .GT. NVALMAX) GOTO 9902

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = 1
C     <comment>Minimal time step</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, DTMIN)
      I = I + 1
C     <comment>Handle on the element</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HELE)
      DO IV = 1,NVAL
C        <comment>
C        List of limiting values, comma separated. For each degree of freedom,
C        1 values for target max increment value.
C        </comment>
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, VVAL(IV))
         IF (IERR .NE.  0) GOTO 9901
      ENDDO

C---     Contrôle
      IF (NVAL .LE. 0) GOTO 9904

C---     CONSTRUIS ET INITIALISE L'OBJET
      HALG = 0
      IF (ERR_GOOD()) IERR = CI_LMTR_CTR(HALG)
      IF (ERR_GOOD()) IERR = CI_LMTR_INI(HALG,
     &                                   DTMIN,
     &                                   HELE,
     &                                   NVAL,
     &                                   VVAL)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = CI_CINC_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = CI_CINC_INI(HOBJ, HALG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_CI_LMTR_PRN(HALG)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>cinc_limiter</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_NOMBRE_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_NORME_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CI_LMTR_AID()

9999  CONTINUE
      IC_CI_LMTR_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_LMTR_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cilmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'cilmtr.fc'
      INCLUDE 'cilmtr_ic.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LTXT
      CHARACTER*(256) TXT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CI_LIMITEUR'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      IOB = HOBJ - CI_LMTR_HBASE

C---     IMPRESSION DES PARAMETRES DE L'OBJET
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_CI_LMTR_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_LMTR_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_LMTR_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'cilmtr_ic.fi'
      INCLUDE 'cilmtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'cilmtr_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CI_LMTR_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_CI_LMTR_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_CI_LMTR_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CI_LMTR_AID()

9999  CONTINUE
      IC_CI_LMTR_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_LMTR_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_LMTR_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cilmtr_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>cinc_limiter</b> represents a increment controller. It will
C  adjust the increment to respect the given target max norm for each degree of
C  freedom (Status: Experimental).
C</comment>
      IC_CI_LMTR_REQCLS = 'cinc_limiter'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_LMTR_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_LMTR_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cilmtr_ic.fi'
      INCLUDE 'cilmtr.fi'
C-------------------------------------------------------------------------

      IC_CI_LMTR_REQHDL = CI_LMTR_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_CI_LMTR_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('cilmtr_ic.hlp')
      RETURN
      END

