C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Contrôle d'Incrément
C Objet:   Proxy
C Type:    Concret
C Functions:
C   Public:
C     INTEGER CI_CINC_000
C     INTEGER CI_CINC_999
C     INTEGER CI_CINC_CTR
C     INTEGER CI_CINC_DTR
C     INTEGER CI_CINC_INI
C     INTEGER CI_CINC_RST
C     INTEGER CI_CINC_REQHBASE
C     LOGICAL CI_CINC_HVALIDE
C     INTEGER CI_CINC_CLCDEL
C     INTEGER CI_CINC_CLCERR
C     INTEGER CI_CINC_DEB
C     INTEGER CI_CINC_ASGDTEF
C     INTEGER CI_CINC_REQDELT
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à CI_CINC_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA CI_CINC_DATA

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'cicinc.fc'

      DATA CI_CINC_HBASE  / OB_OBJC_HBSP_TAG /

      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CI_CINC_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CI_CINC_NOBJMAX,
     &                   CI_CINC_HBASE,
     &                   'Proxy: Increment controller')

      CI_CINC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fc'

      INTEGER  IERR
      EXTERNAL CI_CINC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CI_CINC_NOBJMAX,
     &                   CI_CINC_HBASE,
     &                   CI_CINC_DTR)

      CI_CINC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CINC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CI_CINC_NOBJMAX,
     &                   CI_CINC_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CI_CINC_HVALIDE(HOBJ))
         IOB = HOBJ - CI_CINC_HBASE

         CI_CINC_DTCI(IOB) = -1.0D0
         CI_CINC_DTEF(IOB) = -1.0D0
         CI_CINC_HOMG(IOB) = 0
         CI_CINC_HMDL(IOB) = 0
         CI_CINC_STTS(IOB) = CI_CINC_STATUS_UNDEFINED
      ENDIF

      CI_CINC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CINC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_CINC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CI_CINC_NOBJMAX,
     &                   CI_CINC_HBASE)

      CI_CINC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HOMG        Handle de l'Objet ManaGé
C     NOMMDL      Nom du module
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_INI(HOBJ, HOMG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_INI
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HOMG

      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'cicinc.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HMDL
      INTEGER  HFNC
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
D     CALL ERR_PRE(HOMG .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = CI_CINC_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - CI_CINC_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HOMG, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      HVALIDE = .FALSE.
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HOMG)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         CI_CINC_DTCI(IOB) = -1.0D0
         CI_CINC_DTEF(IOB) = -1.0D0
         CI_CINC_HOMG(IOB) = HOMG
         CI_CINC_HMDL(IOB) = HMDL
         CI_CINC_STTS(IOB) = CI_CINC_STATUS_UNDEFINED
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HOMG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_CINC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CINC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_RST
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'cicinc.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HFNC
      INTEGER  HMDL
      INTEGER  HOMG
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - CI_CINC_HBASE
      HOMG = CI_CINC_HOMG(IOB)
      HMDL = CI_CINC_HMDL(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      HVALIDE = .FALSE.
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     FAIT L'APPEL POUR DÉTRUIRE L'OBJET ASSOCIÉ
      HFNC = 0
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      CI_CINC_DTCI(IOB) = -1.0D0
      CI_CINC_DTEF(IOB) = -1.0D0
      CI_CINC_HOMG(IOB) = 0
      CI_CINC_HMDL(IOB) = 0
      CI_CINC_STTS(IOB) = CI_CINC_STATUS_UNDEFINED

      CI_CINC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CI_CINC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicinc.fi'
      INCLUDE 'cicinc.fc'
C------------------------------------------------------------------------

      CI_CINC_REQHBASE = CI_CINC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CI_CINC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cicinc.fc'
C------------------------------------------------------------------------

      CI_CINC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CI_CINC_NOBJMAX,
     &                                  CI_CINC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Calcule le facteur multiplicatif pour l'incrément.
C
C Description:
C     La fonction <code>CI_CINC_CCLDEL(...)</code> calcule le
C     facteur multiplicatif pour l'incrément (pas de temps ou
C     pas de chargement).
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     ESTOK       État d'entrée (résultat de la résolution)
C
C Sortie:
C     ESTOK       État final
C     FMUL        Facteur multiplicatif de l'incrément
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_CLCDEL(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        ESTOK)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_CLCDEL
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VDLG(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VDEL(NDLN, NNL)
      LOGICAL, INTENT(IN) :: ESTOK

      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'cicinc.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
      REAL*8   DELT(3)
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - CI_CINC_HBASE
      HOMG = CI_CINC_HOMG(IOB)
      HMDL = CI_CINC_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'CLCDEL', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL7(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(NDLN),
     &                        SO_ALLC_CST2B(NNL),
     &                        SO_ALLC_CST2B(VDLG(1,1)),
     &                        SO_ALLC_CST2B(VDEL(1,1)),
     &                        SO_ALLC_CST2B(ESTOK),
     &                        SO_ALLC_CST2B(DELT))
      ENDIF

C---     Conserve les résultats
      IF (ERR_GOOD()) THEN
         CI_CINC_DTCI(IOB) = DELT(1)
         CI_CINC_DTEF(IOB) = DELT(2)
         CI_CINC_STTS(IOB) = NINT(DELT(3))
      ENDIF

      CI_CINC_CLCDEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Calcule le facteur multiplicatif pour l'incrément.
C
C Description:
C     La fonction <code>CI_CINC_CCLERR(...)</code> calcule le
C     facteur multiplicatif pour l'incrément (pas de temps ou
C     pas de chargement).
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     VERR        Table des erreurs
C     ESTOK       État d'entrée (résultat de la résolution)
C
C Sortie:
C     ESTOK       État final
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_CLCERR(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        VERR,
     &                        ESTOK)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_CLCERR
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VDLG(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VDEL(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VERR(NDLN, NNL)
      LOGICAL, INTENT(IN) :: ESTOK

      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'cicinc.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
      REAL*8   DELT(3)
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - CI_CINC_HBASE
      HOMG = CI_CINC_HOMG(IOB)
      HMDL = CI_CINC_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'CLCERR', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL8(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(NDLN),
     &                        SO_ALLC_CST2B(NNL),
     &                        SO_ALLC_CST2B(VDLG(1,1)),
     &                        SO_ALLC_CST2B(VDEL(1,1)),
     &                        SO_ALLC_CST2B(VERR(1,1)),
     &                        SO_ALLC_CST2B(ESTOK),
     &                        SO_ALLC_CST2B(DELT))
      ENDIF

C---     Conserve les résultats
      IF (ERR_GOOD()) THEN
         CI_CINC_DTCI(IOB) = DELT(1)
         CI_CINC_DTEF(IOB) = DELT(2)
         CI_CINC_STTS(IOB) = NINT(DELT(3))
      ENDIF

      CI_CINC_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Marque le début d'une séquence.
C
C Description:
C     La méthode <code>CI_CINC_DEB(...)</code> marque le début d'une
C     séquence d'incréments qui vont être adaptés.
C
C Entrée:
C     HOBJ        L'objet courant
C     TINI        Temps initial de la séquence
C     TFIN        Temps final de la séquence
C     DTNM        Pas de temps nominal
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_DEB(HOBJ, TINI, TFIN, DTNM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_DEB
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TINI
      REAL*8  TFIN
      REAL*8  DTNM

      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'cicinc.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
!!!D     REAL*8, PARAMETER :: EPS3 = 6.66133814775093924D-016  ! from fuzzy.for
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
D     CALL ERR_PRE(TFIN .GT. TINI)
D     CALL ERR_PRE(DTNM .GT. 0.0D0)
!!!D     CALL ERR_PRE(DTNM .LE. (TFIN-TINI+EPS3)) ! trop sensible à la précision numérique
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - CI_CINC_HBASE
      HOMG = CI_CINC_HOMG(IOB)
      HMDL = CI_CINC_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'DEB', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL4(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(TINI),
     &                        SO_ALLC_CST2B(TFIN),
     &                        SO_ALLC_CST2B(DTNM))
      ENDIF

      CI_CINC_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne l'incrément effectif.
C
C Description:
C     La fonction <code>CI_CINC_ASGDTEF(...)</code> permet d'assigner
C     par l'extérieur la pas de temps effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     DTEF        Incrément effectif
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_ASGDTEF(HOBJ, DTEF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_ASGDTEF
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTEF

      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'cicinc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - CI_CINC_HBASE
      HOMG = CI_CINC_HOMG(IOB)
      HMDL = CI_CINC_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'ASGDTEF', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL2(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(DTEF))
      ENDIF

      CI_CINC_ASGDTEF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne les incréments.
C
C Description:
C     La fonction <code>CI_CINC_REQDELT(...)</code> retourne les incréments
C     calculé et effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C     DTCI        Incrément calculé
C     DTEF        Incrément effectif
C
C Notes:
C************************************************************************
      FUNCTION CI_CINC_REQDELT(HOBJ, ISTT, DTCI, DTEF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_REQDELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER ISTT
      REAL*8  DTCI
      REAL*8  DTEF

      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISTT = CI_CINC_STTS(HOBJ - CI_CINC_HBASE)
      DTCI = CI_CINC_DTCI(HOBJ - CI_CINC_HBASE)
      DTEF = CI_CINC_DTEF(HOBJ - CI_CINC_HBASE)

      CI_CINC_REQDELT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>CI_CINC_ESTSTATUS(...)</code> retourne .TRUE.
C     si le critère d'arrêt est satisfait.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CINC_ESTSTATUS(HOBJ, IS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_ESTSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IS

      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CI_CINC_ESTSTATUS = (CI_CINC_STTS(HOBJ-CI_CINC_HBASE) .EQ. IS)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne l'état du critère.
C
C Description:
C     La fonction <code>CI_CINC_REQSTATUS(...)</code> retourne l'état
C     du critère.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CINC_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CINC_REQVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CINC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CI_CINC_REQSTATUS = CI_CINC_STTS(HOBJ-CI_CINC_HBASE)
      RETURN
      END

      