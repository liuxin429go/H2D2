C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_CI_CFL_XEQCTR
C     INTEGER IC_CI_CFL_XEQMTH
C     CHARACTER*(32) IC_CI_CFL_REQCLS
C     INTEGER IC_CI_CFL_REQHDL
C   Private:
C     INTEGER IC_CI_CFL_PRN
C     SUBROUTINE IC_CI_CFL_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_CFL_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_CFL_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'cicfl_ic.fi'
      INCLUDE 'cicfl.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'cicfl_ic.fc'

      INTEGER IERR
      INTEGER HOBJ, HALG
      INTEGER HCFL, ICFL
      REAL*8  CTGT, DTMIN
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_CI_CFL_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Minimal time step</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, DTMIN)
C     <comment>Target CFL</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, CTGT)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Handle on the CFL nodal values</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HCFL)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Index of column of CFL nodal values (default 1)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, ICFL)
      IF (IERR .NE. 0) ICFL = 1

C---     Construis et initialise l'objet
      HALG = 0
      IF (ERR_GOOD()) IERR = CI_CFL_CTR(HALG)
      IF (ERR_GOOD()) IERR = CI_CFL_INI(HALG, DTMIN, HCFL, ICFL, CTGT)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = CI_CINC_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = CI_CINC_INI(HOBJ, HALG)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = IC_CI_CFL_PRN(HALG)
      END IF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the increment controller</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>cinc_cfl</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CI_CFL_AID()

9999  CONTINUE
      IC_CI_CFL_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_CFL_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cicfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'cicfl.fc'
      INCLUDE 'cicfl_ic.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LTXT
      CHARACTER*(256) TXT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CI_CFL'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      IOB = HOBJ - CI_CFL_HBASE

C---     IMPRESSION DES PARAMETRES DE L'OBJET
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_CI_CFL_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_CFL_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_CFL_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'cicfl_ic.fi'
      INCLUDE 'cicfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'cicfl_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CI_CFL_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_CI_CFL_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_CI_CFL_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CI_CFL_AID()

9999  CONTINUE
      IC_CI_CFL_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_CFL_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_CFL_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicfl_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>cinc_cfl</b> represents an increment controller based on a
C  CFL condition.
C  Step size is halved on non convergence and re-increased after a few
C  successful steps (Status: Experimental).
C</comment>
      IC_CI_CFL_REQCLS = 'cinc_cfl'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_CFL_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_CFL_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicfl_ic.fi'
      INCLUDE 'cicfl.fi'
C-------------------------------------------------------------------------

      IC_CI_CFL_REQHDL = CI_CFL_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_CI_CFL_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('cicfl_ic.hlp')
      RETURN
      END

