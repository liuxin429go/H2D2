C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>HM_PRGL_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_PRGL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmprgl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(HM_PRGL_NOBJMAX,
     &                   HM_PRGL_HBASE,
     &                   'Homotopy On Global Properties')

      HM_PRGL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_PRGL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmprgl.fc'

      INTEGER  IERR
      EXTERNAL HM_PRGL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(HM_PRGL_NOBJMAX,
     &                   HM_PRGL_HBASE,
     &                   HM_PRGL_DTR)

      HM_PRGL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_PRGL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   HM_PRGL_NOBJMAX,
     &                   HM_PRGL_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(HM_PRGL_HVALIDE(HOBJ))
         IOB = HOBJ - HM_PRGL_HBASE

         HM_PRGL_ALFA (IOB) =-1.0D0
         HM_PRGL_HPRNT(IOB) = 0
         HM_PRGL_HPRGL(IOB) = 0
         HM_PRGL_IPRGL(IOB) = 0
      ENDIF

      HM_PRGL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_PRGL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmprgl.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = HM_PRGL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   HM_PRGL_NOBJMAX,
     &                   HM_PRGL_HBASE)

      HM_PRGL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_PRGL_INI(HOBJ,
     &                     HPRNT,
     &                     AMIN, VMIN,
     &                     AMAX, VMAX,
     &                     HPRGL, IPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HPRNT
      REAL*8  AMIN, VMIN
      REAL*8  AMAX, VMAX
      INTEGER HPRGL, IPRGL

      INCLUDE 'hmprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprgl.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmprgl.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (.NOT. HM_HMTP_HVALIDE(HPRNT)) GOTO 9901
      IF (.NOT. DT_PRGL_HVALIDE(HPRGL)) GOTO 9902
      IF (IPRGL .LE. 0) GOTO 9903
      IF (IPRGL .GT. DT_PRGL_REQNPRGL(HPRGL)) GOTO 9903

C---     Reset les données
      IERR = HM_PRGL_RST(HOBJ)

C---     Assigne les valeurs au parent
      IF (ERR_GOOD()) THEN
         IERR = HM_HMTP_ASGVAL(HPRNT, AMIN, VMIN, AMAX, VMAX)
      ENDIF

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - HM_PRGL_HBASE
         HM_PRGL_ALFA (IOB) =-1.0D0
         HM_PRGL_HPRNT(IOB) = HPRNT
         HM_PRGL_HPRGL(IOB) = HPRGL
         HM_PRGL_IPRGL(IOB) = IPRGL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HPRNT, 'MSG_HOMOTOPY')
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HPRGL, 'MSG_PRGL')
      GOTO 9999
9903  WRITE(ERR_BUF,'(2A,I12)')'ERR_INDICE_INVALIDE',':',IPRGL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HM_PRGL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_PRGL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmprgl.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset nos attributs
      IOB = HOBJ - HM_PRGL_HBASE
      HM_PRGL_ALFA (IOB) =-1.0D0
      HM_PRGL_HPRNT(IOB) = 0
      HM_PRGL_HPRGL(IOB) = 0
      HM_PRGL_IPRGL(IOB) = 0

      HM_PRGL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction HM_PRGL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_PRGL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmprgl.fi'
      INCLUDE 'hmprgl.fc'
C------------------------------------------------------------------------

      HM_PRGL_REQHBASE = HM_PRGL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction HM_PRGL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_PRGL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmprgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmprgl.fc'
C------------------------------------------------------------------------

      HM_PRGL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  HM_PRGL_NOBJMAX,
     &                                  HM_PRGL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Calcule et assigne la valeur.
C
C Description:
C     La fonction <code>HM_PRGL_PRN(...)</code>
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_PRGL_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmprgl.fi'
      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'hmprgl.fc'

      REAL*8  VVAL
      INTEGER IERR
      INTEGER IOB
      INTEGER HPRGL, IPRGL
      INTEGER LTXT
      CHARACTER*(256) TXT
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.homotopy.prgl'

C---     Récupère les attributs
      IOB = HOBJ - HM_PRGL_HBASE
      HPRGL = HM_PRGL_HPRGL(IOB)
      IPRGL = HM_PRGL_IPRGL(IOB)
D     CALL ERR_ASR(DT_PRGL_HVALIDE(HPRGL))

C---     Calcule la valeur
      IF (ERR_GOOD()) IERR = DT_PRGL_REQPRGL(HPRGL, IPRGL, VVAL)

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_HOMOTOPY_PRGL'
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Impression des attributs
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, HPRGL)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,'(2A,1PE14.6E3)') 'MSG_INDICE#<35>#', '= ', IPRGL
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(2A,1PE14.6E3)') 'MSG_VALEUR#<35>#', '= ', VVAL
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      CALL LOG_DECIND()

      HM_PRGL_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Calcule et assigne la valeur.
C
C Description:
C     La fonction <code>HM_PRGL_XEQ(...)</code>
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     ALFA        Le point de calcul
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_PRGL_XEQ(HOBJ, ALFA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  ALFA

      INCLUDE 'hmprgl.fi'
      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'hmhmtp.fc'
      INCLUDE 'hmprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HPRGL, IPRGL
      REAL*8  VVAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.homotopy.prgl'

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - HM_PRGL_HBASE
      HPRNT = HM_PRGL_HPRNT(IOB)
      HPRGL = HM_PRGL_HPRGL(IOB)
      IPRGL = HM_PRGL_IPRGL(IOB)
D     CALL ERR_ASR(HM_HMTP_HVALIDE(HPRNT))
D     CALL ERR_ASR(DT_PRGL_HVALIDE(HPRGL))

C---     Calcule la valeur
      IF (ERR_GOOD()) IERR = HM_HMTP_CLCVAL (HPRNT, ALFA, VVAL)
      IF (ERR_GOOD()) IERR = DT_PRGL_ASGPRGL(HPRGL, IPRGL, VVAL)

C---     Conserve le alfa
      IF (ERR_GOOD()) HM_PRGL_ALFA(IOB) = ALFA

C---     Log la valeur
      WRITE(LOG_BUF, '(A,I3,A,1PE14.6E3,A,1PE25.17E3)')
     &    'IPRGL = ', IPRGL,
     &    '; MSG_ALFA = ', ALFA,
     &    '; MSG_VAL = ', VVAL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

      HM_PRGL_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne alfa.
C
C Description:
C     La fonction <code>HM_PRGL_REQALFA(...)</code> retourne alfa, le point
C     de calcul actuel.
C
C Entrée:
C
C Sortie:
C     ALFA        Point de calcul
C
C Notes:
C************************************************************************
      FUNCTION HM_PRGL_REQALFA(HOBJ, ALFA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_PRGL_REQALFA
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  ALFA

      INCLUDE 'hmprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmprgl.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_PRGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ALFA = HM_PRGL_ALFA(HOBJ - HM_PRGL_HBASE)
      HM_PRGL_REQALFA = ERR_TYP()
      RETURN
      END
