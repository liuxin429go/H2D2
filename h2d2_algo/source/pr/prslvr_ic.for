C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PRSLVR_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRSLVR_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'prslvr_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prprxy.fi'
      INCLUDE 'prslvr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'prslvr_ic.fc'

      INTEGER IERR
      INTEGER HOBJ, HPRC
      INTEGER HSLVR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PRSLVR_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      HSLVR = 0
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
C        <comment>Handle on the solver</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HSLVR)
         IF (IERR .NE. 0) GOTO 9901
      ENDIF

C---     CONSTRUIS ET INITIALISE L'OBJET
      HPRC = 0
      IF (ERR_GOOD()) IERR = PR_SLVR_CTR(HPRC)
      IF (ERR_GOOD()) IERR = PR_SLVR_INI(HPRC, HSLVR)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PR_PRXY_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PR_PRXY_INI(HOBJ, HPRC)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_PRSLVR_PRN(HPRC)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the preconditioning</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>pr_slvr</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PRSLVR_AID()

9999  CONTINUE
      IC_PRSLVR_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_PRSLVR_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PRSLVR_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'prslvr.fi'
      INCLUDE 'prslvr.fc'
      INCLUDE 'prslvr_ic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSLVR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - PR_SLVR_HBASE

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_PRECOND_SLVR'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Impression du handle
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

C---     Impression des paramètres
      HSLVR= PR_SLVR_HSLVR(IOB)
      IERR = OB_OBJC_REQNOMCMPL(NOM, HSLVR)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SOLVER#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_PRSLVR_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PRSLVR_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRSLVR_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'prslvr_ic.fi'
      INCLUDE 'prslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(PR_SLVR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = PR_SLVR_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(PR_SLVR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = PR_SLVR_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test PR_SLVR_PRN(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PRSLVR_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PRSLVR_AID()

9999  CONTINUE
      IC_PRSLVR_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PRSLVR_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRSLVR_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prslvr_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>pr_slvr</b> represents a preconditioning by an external matrix system solver.
C</comment>
      IC_PRSLVR_REQCLS = 'pr_slvr'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PRSLVR_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRSLVR_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prslvr_ic.fi'
      INCLUDE 'prslvr.fi'
C-------------------------------------------------------------------------

      IC_PRSLVR_REQHDL = PR_SLVR_REQHBASE()
      RETURN
      END


C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_PRSLVR_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PRSLVR_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('prslvr_ic.hlp')

      RETURN
      END

