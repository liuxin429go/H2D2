C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: PR
C      H2D2 Class: PR_SLVR
C         INTEGER PR_SLVR_000
C         INTEGER PR_SLVR_999
C         INTEGER PR_SLVR_CTR
C         INTEGER PR_SLVR_DTR
C         INTEGER PR_SLVR_INI
C         INTEGER PR_SLVR_RST
C         INTEGER PR_SLVR_REQHBASE
C         LOGICAL PR_SLVR_HVALIDE
C         INTEGER PR_SLVR_ASM
C         INTEGER PR_SLVR_PRC
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_SLVR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prslvr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prslvr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PR_SLVR_NOBJMAX,
     &                   PR_SLVR_HBASE,
     &                   'Preconditioner with direct solver')

      PR_SLVR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_SLVR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prslvr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prslvr.fc'

      INTEGER  IERR
      EXTERNAL PR_SLVR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PR_SLVR_NOBJMAX,
     &                   PR_SLVR_HBASE,
     &                   PR_SLVR_DTR)

      PR_SLVR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SLVR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prslvr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prslvr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PR_SLVR_NOBJMAX,
     &                   PR_SLVR_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PR_SLVR_HVALIDE(HOBJ))
         IOB = HOBJ - PR_SLVR_HBASE

         PR_SLVR_HSLVR(IOB) = 0
      ENDIF

      PR_SLVR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SLVR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prslvr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prslvr.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PR_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PR_SLVR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PR_SLVR_NOBJMAX,
     &                   PR_SLVR_HBASE)

      PR_SLVR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SLVR_INI(HOBJ, HSLVR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSLVR

      INCLUDE 'prslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prslvr.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HASM
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PR_SLVR_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - PR_SLVR_HBASE
         PR_SLVR_HSLVR(IOB) = HSLVR
      ENDIF

      PR_SLVR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SLVR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prslvr.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PR_SLVR_HBASE

      PR_SLVR_HSLVR(IOB) = 0

      PR_SLVR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PR_SLVR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_SLVR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prslvr.fi'
      INCLUDE 'prslvr.fc'
C------------------------------------------------------------------------

      PR_SLVR_REQHBASE = PR_SLVR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PR_SLVR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_SLVR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prslvr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'prslvr.fc'
C------------------------------------------------------------------------

      PR_SLVR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PR_SLVR_NOBJMAX,
     &                                  PR_SLVR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assemble et factorise le préconditionnement.
C
C Description:
C     La fonction PR_SLVR_ASM assemble et factorise la matrice de
C     préconditionnement.
C     La matrice est dimensionnée et assemblèe, puis factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle de la simulation
C     HALG     Handle de l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SLVR_ASM(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_ASM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'prslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'prslvr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSLVR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SLVR_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_SLVR_HBASE
      HSLVR = PR_SLVR_HSLVR(IOB)
D     CALL ERR_ASR(MR_RESO_HVALIDE(HSLVR))

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = MR_RESO_ASMMTX(HSLVR,HSIM,HALG,F_K,F_KU)

C---     FACTORISE LA MATRICE
      IF (ERR_GOOD()) IERR = MR_RESO_FCTMTX(HSLVR)

      PR_SLVR_ASM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Resous le système matriciel pour
C
C Description:
C     La fonction PR_SLVR_PRC préconditionne le second membre
C     qui lui est passé. La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     VFG      Vecteur à préconditionner
C
C Sortie:
C     VFG      Vecteur préconditionné
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SLVR_PRC(HOBJ, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_SLVR_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VFG(*)

      INCLUDE 'prslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'prslvr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSLVR
      INTEGER HSIM, HALG
      EXTERNAL ERR_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_SLVR_HBASE
      HSLVR = PR_SLVR_HSLVR(IOB)
D     CALL ERR_ASR(MR_RESO_HVALIDE(HSLVR))

C---     ASSEMBLE LA MATRICE
      HSIM = 0
      HALG = 0
      IF (ERR_GOOD())
     &   IERR = MR_RESO_RESMTX(HSLVR,HSIM,HALG,ERR_DUMMY,VFG)

      PR_SLVR_PRC = ERR_TYP()
      RETURN
      END
