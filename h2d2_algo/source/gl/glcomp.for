C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_COMP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_COMP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glcomp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_COMP_NOBJMAX,
     &                   GL_COMP_HBASE,
     &                   'Globalisation - Compose')

      GL_COMP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_COMP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glcomp.fc'

      INTEGER  IERR
      EXTERNAL GL_COMP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_COMP_NOBJMAX,
     &                   GL_COMP_HBASE,
     &                   GL_COMP_DTR)

      GL_COMP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_COMP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glcomp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_COMP_NOBJMAX,
     &                   GL_COMP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_COMP_HVALIDE(HOBJ))
         IOB = HOBJ - GL_COMP_HBASE

         GL_COMP_NCOMP(IOB) = 0
         GL_COMP_LCOMP(IOB) = 0
      ENDIF

      GL_COMP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_COMP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glcomp.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_COMP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_COMP_NOBJMAX,
     &                   GL_COMP_HBASE)

      GL_COMP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_COMP_INI(HOBJ, NCOMP, KCOMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCOMP
      INTEGER KCOMP(*)

      INCLUDE 'glcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glcomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER I
      INTEGER LCOMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (NCOMP .LT. 0) GOTO 9901
      DO I=1, NCOMP
         IF (.NOT. GL_GLBL_HVALIDE(KCOMP(I))) GOTO 9902
      ENDDO

C---     RESET LES DONNEES
      IERR = GL_COMP_RST(HOBJ)

C---     ALLOUE LA MÉMOIRE POUR LA TABLE
      LCOMP = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NCOMP, LCOMP)

C---     REMPLIS LA TABLE
      IF (ERR_GOOD()) CALL ICOPY (NCOMP,
     &                            KCOMP, 1,
     &                            KA(SO_ALLC_REQKIND(KA,LCOMP)), 1)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - GL_COMP_HBASE
         GL_COMP_NCOMP(IOB) = NCOMP
         GL_COMP_LCOMP(IOB) = LCOMP
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(2A,I3)')'ERR_NBR_HANDLE_INVALIDE',':',NCOMP
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_HANDLE',':',KCOMP(I)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_INDICE',':',I
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_COMP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_COMP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glcomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LCOMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - GL_COMP_HBASE
      LCOMP = GL_COMP_LCOMP(IOB)

      IF (LCOMP .NE. 0) IERR = SO_ALLC_ALLINT(0, LCOMP)

      GL_COMP_NCOMP(IOB) = 0
      GL_COMP_LCOMP(IOB) = 0

      GL_COMP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_COMP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_COMP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glcomp.fi'
      INCLUDE 'glcomp.fc'
C------------------------------------------------------------------------

      GL_COMP_REQHBASE = GL_COMP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_COMP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_COMP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glcomp.fc'
C------------------------------------------------------------------------

      GL_COMP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_COMP_NOBJMAX,
     &                                  GL_COMP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Imprime l'objet dans le log.
C
C Description:
C     La fonction GL_COMP_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_COMP_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glcomp.fc'

      INTEGER I, IOB, IERR
      INTEGER NCOMP, LCOMP, HCOMP
      CHARACTER*256 NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - GL_COMP_HBASE

C-------  EN-TETE DE COMMANDE
      LOG_ZNE = 'h2d2.glob.compose'
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_GLBL_COMPOSE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DU HANDLE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     IMPRESSION DU NOMBRE DE COMPOSANTES
      NCOMP = GL_COMP_NCOMP(IOB)
      WRITE (LOG_BUF,'(2A,I6)') 'MSG_NBR_OBJ_COMPOSE#<35>#',
     &                          '= ', NCOMP
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     IMPRESSION DES COMPOSANTES
      LCOMP = GL_COMP_LCOMP(IOB)
      HCOMP= SO_ALLC_REQIN4(LCOMP, 1)
      IERR = OB_OBJC_REQNOMCMPL(NOM, HCOMP)
      WRITE (LOG_BUF,'(A,A)') 'MSG_COMPOSANTES#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      DO I=2, NCOMP
         HCOMP= SO_ALLC_REQIN4(LCOMP, I)
         IERR = OB_OBJC_REQNOMCMPL(NOM, HCOMP)
         WRITE (LOG_BUF,'(A,A)') '#<35>#  ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDDO

      CALL LOG_DECIND()

      GL_COMP_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C     La fonction <code>GL_COMP_DEB(...)</code>
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_COMP_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'glcomp.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glcomp.fc'

      INTEGER IOB, IERR, IC
      INTEGER NCOMP, LCOMP, HCOMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - GL_COMP_HBASE
      NCOMP = GL_COMP_NCOMP(IOB)
      LCOMP = GL_COMP_LCOMP(IOB)
D     CALL ERR_ASR(NCOMP .GT. 0)
D     CALL ERR_ASR(LCOMP .NE. 0)

C---     BOUCLE SUR LES COMPOSANTES
      DO IC = 1, NCOMP
         HCOMP= SO_ALLC_REQIN4(LCOMP, IC)
         IERR = GL_GLBL_DEB(HCOMP)
         IF (ERR_BAD()) GOTO 199
      ENDDO
199   CONTINUE

      GL_COMP_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C     La fonction <code>GL_COMP_XEQ(...)</code>
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     ETA         Adapting Forcing Term
C     HFRES       Fonction call-back de calcul de résidu
C     HFSLV       Fonction call-back de résolution
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C************************************************************************
      FUNCTION GL_COMP_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL,
     &                     ETA,
     &                     HFRES,
     &                     HFSLV,
     &                     ISTTS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_COMP_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  ETA
      INTEGER HFRES
      INTEGER HFSLV
      INTEGER ISTTS

      INCLUDE 'glcomp.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glcomp.fc'

      INTEGER IOB, IERR, IC
      INTEGER NCOMP, LCOMP, HCOMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - GL_COMP_HBASE
      NCOMP = GL_COMP_NCOMP(IOB)
      LCOMP = GL_COMP_LCOMP(IOB)
D     CALL ERR_ASR(NCOMP .GT. 0)
D     CALL ERR_ASR(LCOMP .NE. 0)

C---     Boucle sur les composantes
      ISTTS = GL_STTS_OK
      DO IC = 1, NCOMP
         HCOMP= SO_ALLC_REQIN4(LCOMP, IC)
         IERR = GL_GLBL_XEQ2(HCOMP,
     &                       HSIM,
     &                       NDLN,
     &                       NNL,
     &                       VDLG,
     &                       VDEL,
     &                       ETA,
     &                       HFRES,
     &                       HFSLV)
         IF (ERR_BAD()) GOTO 199
         ISTTS = IOR(ISTTS, GL_GLBL_REQSTATUS(HCOMP))
      ENDDO
199   CONTINUE

      GL_COMP_XEQ = ERR_TYP()
      RETURN
      END
