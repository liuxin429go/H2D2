C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GLBTK3_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLBTK3_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'glbtk3_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'glbtk3.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HOBJ, HBTK
      REAL*8  THMIN, THMAX
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_GLBTK3_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
C     <comment>Theta min (default 1.0D-3)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, THMIN)
      IF (IERR .NE. 0) THMIN = 1.0D-3
C     <comment>Theta max (default 1.0)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, THMAX)
      IF (IERR .NE. 0) THMAX = 1.0D0

C---     CONSTRUIS ET INITIALISE L'OBJET
      HBTK = 0
      IF (ERR_GOOD()) IERR = GL_BTK3_CTR(HBTK)
      IF (ERR_GOOD()) IERR = GL_BTK3_INI(HBTK, THMIN, THMAX)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = GL_GLBL_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = GL_GLBL_INI(HOBJ, HBTK)

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the globalization
C        algorithm by third order backtracking</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = GL_BTK3_PRN(HBTK)
      ENDIF

C<comment>
C  The constructor <b>backtracking_3</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLBTK3_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_GLBTK3_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GLBTK3_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLBTK3_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'glbtk3_ic.fi'
      INCLUDE 'glbtk3.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall
C     not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(GL_BTK3_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = GL_BTK3_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(GL_BTK3_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = GL_BTK3_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_GLBTK3_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLBTK3_AID()

9999  CONTINUE
      IC_GLBTK3_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLBTK3_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLBTK3_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbtk3_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>backtracking_3</b> represents a globalization algorithm by
C  backtracking. It is a search by third order polynomial approximation
C  for the minimum residual along the descent direction represented by the
C  increment on the solution. We seek <b>thmin</b> < t < <b>thmax</b> such
C  as to minimize the residual R (u + t du).
C</comment>
      IC_GLBTK3_REQCLS = 'backtracking_3'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLBTK3_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLBTK3_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbtk3_ic.fi'
      INCLUDE 'glbtk3.fi'
C-------------------------------------------------------------------------

      IC_GLBTK3_REQHDL = GL_BTK3_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_GLBTK3_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_GLBTK3_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('glbtk3_ic.hlp')

      RETURN
      END
