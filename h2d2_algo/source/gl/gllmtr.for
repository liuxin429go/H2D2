C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER GL_LMTR_000
C     INTEGER GL_LMTR_999
C     INTEGER GL_LMTR_CTR
C     INTEGER GL_LMTR_DTR
C     INTEGER GL_LMTR_INI
C     INTEGER GL_LMTR_RST
C     INTEGER GL_LMTR_REQHBASE
C     LOGICAL GL_LMTR_HVALIDE
C     INTEGER GL_LMTR_DEB
C     INTEGER GL_LMTR_XEQ
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_LMTR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_LMTR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gllmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gllmtr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_LMTR_NOBJMAX,
     &                   GL_LMTR_HBASE,
     &                   'Solution Limiter')

      GL_LMTR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_LMTR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gllmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gllmtr.fc'

      INTEGER  IERR
      EXTERNAL GL_LMTR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_LMTR_NOBJMAX,
     &                   GL_LMTR_HBASE,
     &                   GL_LMTR_DTR)

      GL_LMTR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_LMTR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gllmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gllmtr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_LMTR_NOBJMAX,
     &                   GL_LMTR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_LMTR_HVALIDE(HOBJ))
         IOB = HOBJ - GL_LMTR_HBASE

         GL_LMTR_HSIM(IOB) = 0
         GL_LMTR_NVAL(IOB) = 0
      ENDIF

      GL_LMTR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_LMTR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gllmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gllmtr.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_LMTR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_LMTR_NOBJMAX,
     &                   GL_LMTR_HBASE)

      GL_LMTR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_LMTR_INI(HOBJ, HSIM, NVAL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NVAL
      REAL*8  VVAL(NVAL)

      INCLUDE 'gllmtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gllmtr.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER IV
      INTEGER NDLN
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (.NOT. LM_ELEM_HVALIDE(HSIM)) GOTO 9901
      NDLN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      IF (NVAL .NE. NDLN .AND. NVAL .NE. NDLN*3) GOTO 9902
      IF (NVAL .EQ. NDLN) THEN
         DO IV=1, NVAL
            IF (VVAL(IV) .LE. 0.0D0)    GOTO 9904
         ENDDO
      ELSE
         DO IV=3, NVAL, 3
            IF (VVAL(IV-2) .GE. VVAL(IV-1)) GOTO 9903
            IF (VVAL(IV)   .LE. 0.0D0)      GOTO 9904
         ENDDO
      ENDIF

C---     Reset les données
      IERR = GL_LMTR_RST(HOBJ)

C---     Stocke les attributs
      IOB = HOBJ - GL_LMTR_HBASE
      GL_LMTR_HSIM(IOB) = HSIM
      IF (NVAL .EQ. NDLN) THEN
         GL_LMTR_NVAL(IOB) = NVAL
         DO IV=1, NVAL
            GL_LMTR_VMIN(IV,IOB) =-1.0D99
            GL_LMTR_VMAX(IV,IOB) = 1.0D99
            GL_LMTR_VDEL(IV,IOB) = VVAL(IV)
         ENDDO
      ELSE
         GL_LMTR_NVAL(IOB) = NVAL / 3
         DO IV=3, NVAL, 3
            GL_LMTR_VMIN(IV/3,IOB) = VVAL(IV-2)
            GL_LMTR_VMAX(IV/3,IOB) = VVAL(IV-1)
            GL_LMTR_VDEL(IV/3,IOB) = VVAL(IV)
         ENDDO
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HSIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSIM, 'MSG_SIMULATION')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NBR_VALEURS_INVALIDE',':',
     &                           NVAL, ' / ', NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,1PE14.6E3,A,1PE14.6E3)') 'ERR_VAL_MIN_GT_MAX',
     &                           ':', VVAL(IV-2), ' >= ', VVAL(IV-1)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,1PE14.6E3,A,1PE14.6E3)') 'ERR_DEL_LT_0',
     &                           ':', VVAL(IV), ' <= ', 0.0D0
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_LMTR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_LMTR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gllmtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gllmtr.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - GL_LMTR_HBASE
      GL_LMTR_HSIM(IOB) = 0
      GL_LMTR_NVAL(IOB) = 0

      GL_LMTR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_LMTR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_LMTR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gllmtr.fi'
      INCLUDE 'gllmtr.fc'
C------------------------------------------------------------------------

      GL_LMTR_REQHBASE = GL_LMTR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_LMTR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_LMTR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gllmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gllmtr.fc'
C------------------------------------------------------------------------

      GL_LMTR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_LMTR_NOBJMAX,
     &                                  GL_LMTR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Marque le début d'une séquence d'itérations.
C
C Description:
C     La fonction GL_LMTR_DEB marque le début d'une séquence d'itérations.
C     Elle fait les traitements nécessaire et doit donc être appelée avant
C     GL_LMTR_XEQ.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_LMTR_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'gllmtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gllmtr.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      GL_LMTR_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Limite la solution
C
C Description:
C     La fonction GL_LMTR_XEQ retourne un incrément de solution limité en
C     fonction des paramètres de l'objet. Elle retourne le nombre de valeurs
C     limitées.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     ETA         Adapting Forcing Term
C     HFRES       Fonction call-back de calcul de résidu
C     HFSLV       Fonction call-back de résolution
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C************************************************************************
      FUNCTION GL_LMTR_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL,
     &                     ETA,
     &                     HFRES,
     &                     HFSLV,
     &                     ISTTS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_LMTR_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  ETA
      INTEGER HFRES
      INTEGER HFSLV
      INTEGER ISTTS

      INCLUDE 'gllmtr.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gllmtr.fc'

      INTEGER IERR, I_ERROR
      INTEGER IOB
      INTEGER IN, IV
      INTEGER NVAL, NLIM, NLIM_L, NLIM_G
      REAL*8  VNEW, DV
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.glob'

C---     Récupère les attributs
      IOB = HOBJ - GL_LMTR_HBASE
      NVAL = GL_LMTR_NVAL(IOB)

C---     Contrôles de cohérence
      IF (HSIM .NE. GL_LMTR_HSIM(IOB)) GOTO 9900
      IF (NVAL .NE. NDLN) GOTO 9901

C---     Exécute
      NLIM = 0
      DO IN=1, NNL
         DO IV=1, NDLN
            DV = ABS(VDEL(IV, IN))
            IF (DV .GT. GL_LMTR_VDEL(IV,IOB)) THEN
               VDEL(IV, IN) = SIGN(GL_LMTR_VDEL(IV,IOB), VDEL(IV, IN))
               NLIM = NLIM + 1
            ENDIF

            VNEW = VDLG(IV, IN) + VDEL(IV, IN)
            IF (VNEW .LT. GL_LMTR_VMIN(IV,IOB)) THEN
               VDEL(IV, IN) = GL_LMTR_VMIN(IV,IOB) - VDLG(IV, IN)
            ELSEIF (VNEW .GT. GL_LMTR_VMAX(IV,IOB)) THEN
               VDEL(IV, IN) = GL_LMTR_VMAX(IV,IOB) - VDLG(IV, IN)
            ENDIF
         ENDDO
      ENDDO

C---     Limite
      NLIM_G = 0
      IF (ERR_GOOD()) THEN
         NLIM_L = NLIM
         CALL MPI_ALLREDUCE(NLIM_L, NLIM_G, 1, MP_TYPE_INT(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     L'état
      ISTTS = GL_STTS_OK
      IF (NLIM_G .NE. 0) ISTTS = IBSET(ISTTS, GL_STTS_MODIF)
      IF (NLIM_G .NE. 0) ISTTS = IBSET(ISTTS, GL_STTS_BMIN)

C---     Log
      IF (ERR_GOOD()) THEN
         WRITE(LOG_BUF,'(A,I12)') 'Limiter - nval#<35>#= ', NLIM_G
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF
      
      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_HANDLE_INVALIDE',':',
     &                           NVAL, ' / ', NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSIM, 'MSG_SIMULATION')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NBR_VALEURS_INVALIDE',':',
     &                           NVAL, ' / ', NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_LMTR_XEQ = ERR_TYP()
      RETURN
      END
