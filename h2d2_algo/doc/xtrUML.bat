@echo off
set MODUL=h2d2_algo

set XTRDIR=%INRS_DEV%\tools\xtrapi\VP-UML
set UPDONE=%XTRDIR%\update_one.bat
set SRCDIR=%INRS_DEV%\H2D2\%MODUL%\source

call %UPDONE% "h2d2_algo - al" "h2d2_algo - al.xml" %SRCDIR%\al\al*.f*
call %UPDONE% "h2d2_algo - ca" "h2d2_algo - ca.xml" %SRCDIR%\ca\ca*.f*
call %UPDONE% "h2d2_algo - ca" "h2d2_algo - ca.xml" %SRCDIR%\cc\cc*.f*
call %UPDONE% "h2d2_algo - gl" "h2d2_algo - gl.xml" %SRCDIR%\gl\gl*.f*
call %UPDONE% "h2d2_algo - mr" "h2d2_algo - mr.xml" %SRCDIR%\mr\mr*.f*
call %UPDONE% "h2d2_algo - mx" "h2d2_algo - mx.xml" %SRCDIR%\mx\mx*.f*
call %UPDONE% "h2d2_algo - pr" "h2d2_algo - pr.xml" %SRCDIR%\pr\pr*.f*

call %UPDONE% "h2d2_algo - ic" "h2d2_algo - ic.xml" %SRCDIR%\ic*.f*

pause
