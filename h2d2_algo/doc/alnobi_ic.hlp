class algo_node_bi
==================
 
   *****  
   This class has been DEPRECATED. Please use the increment controler, more
   specifically **cinc_simple** instead.  
   ***** 
   The class **algo_node_bi** represents a node of a resolution strategy
   specialized with bisection of the time step.
    
   constructor handle algo_node_bi(halg, nlvl, hxpr, hlft, hrht, irst)
      The constructor **algo_node_bi** constructs an object, with the given
      arguments, and returns a handle on this object.
         handle   HNOD        Return value: Handle on the node
         handle   halg        Handle on the resolution algorithm
         integer  nlvl        Number of bisection level
         handle   hxpr        Handle on the testing expression
         handle   hlft        Handle on the left node executed if `hxpr`
                           returns TRUE
         handle   hrht        Handle on the right node executed if `hxpr`
                           returns FALSE
         integer  irst        In case `hxpr` returns FALSE, indicated if the
                           solution
                              is reset [0=no rest, 1=do reset] (default 1).
    
   getter h_algo
      Handle on the resolution algorithm
    
   method solve(htmr, hele, hpst, htrg, lprm)
      The method **solve** executes the algorithm.
         handle   htmr        Handle on the timer (can be 0)
         handle   hele        Handle on the element
         handle   hpst        Handle on the post-treatment during resolution
                           (can be 0)
         handle   htrg        Handle on the trigger for the post-treatment
                           (can be 0)
         string   lprm        Arguments passed directly to the sequence.
                              For a stationary simulation:
                                     TINI: initial time
                              And for a transient simulation:
                                     TINI: initial time,
                                     DELT: time step,
                                     NPAS: number of time steps
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class.
    
