class newton_damped
===================
 
   The class **newton_damped** represents the resolution algorithm of a
   non-linear system with a damped version of the linearization iterative
   method of Newton. (Status: Experimental. Damper should be only on qx, qy as
   the damping will force the values to 0)
    
   constructor handle newton_damped(hcra, hcrc, hglb, hdmp, hres, niter,
                                    nsitr)
      The constructor **newton_damped** constructs an object, with the given
      arguments, and returns a handle on this object.
         handle   HALG        Return value: Handle on the algorithm
         handle   hcra        Handle on the stopping criterion
         handle   hcrc        Handle on the convergence criterion
         handle   hglb        Handle on the globalization algorithm
         handle   hdmp        Handle on the damping algorithm
         handle   hres        Handle on the matrix resolution algorithm
         integer  niter       Number of iterations (max)
         integer  nsitr       Number of sub-iterations (default 0)
    
   property hres
      Handle on the matrix resolution algorithm
    
   property niter
      Number of iterations (max)
    
   method solve(htmr, hele, hpst, htrg, lprm)
      The method **solve** executes the algorithm.
         handle   htmr        Handle on the timer (can be 0)
         handle   hele        Handle on the element
         handle   hpst        Handle on the post-treatment during resolution
                           (can be 0)
         handle   htrg        Handle on the trigger for the post-treatment
                           (can be 0)
         string   lprm        Arguments passed directly to the sequence.
                              For a stationary simulation:
                                     TINI: initial time
                              And for a transient simulation:
                                     TINI: initial time,
                                     DELT: time step,
                                     NPAS: number of time steps
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class.
    
