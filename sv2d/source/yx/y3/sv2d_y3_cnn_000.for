C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Block d'initialisation.
C
C Description:
C     Le block data privé <code>SV2D_Y3_CNN_DATA_000()</code> initialise
C     les attributs statiques.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_Y3_CNN_DATA_000

      INCLUDE 'sv2d_y3_cnn.fc'

      DATA SV2D_Y3_CNN_HBASE /0/
      DATA SV2D_Y3_CNN_HVFT  /0/

      END

C************************************************************************
C Sommaire: SV2D_Y3_CNN_000
C
C Description:
C     La fonction SV2D_Y3_CNN_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_Y3_CNN_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y3_CNN_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_y3_cnn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_y3_cnn.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
C     CALL ERR_PRE(.NOT. SO_VTBL_HVALIDE(SV2D_Y3_CNN_HVFT))
C-----------------------------------------------------------------------

C---     Enregistre la classe
      IERR = OB_OBJN_000(SV2D_Y3_CNN_HBASE,
     &                   'Finite Element Saint-Venant 2D - Y3')

C---     Crée la table
      IF (ERR_GOOD()) IERR = SO_VTBL_CTR(SV2D_Y3_CNN_HVFT)
      IF (ERR_GOOD()) IERR = SO_VTBL_INI(SV2D_Y3_CNN_HVFT)

C---     Remplis la table
      IF (ERR_GOOD()) IERR = SV2D_Y3_CNN_INIVTBL(SV2D_Y3_CNN_HVFT)

      SV2D_Y3_CNN_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SV2D_Y3_CNN_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_Y3_CNN_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y3_CNN_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_y3_cnn.fi'
      INCLUDE 'sv2d_y3_cnn.fc'
C------------------------------------------------------------------------

      SV2D_Y3_CNN_REQHBASE = SV2D_Y3_CNN_HBASE
      RETURN
      END

