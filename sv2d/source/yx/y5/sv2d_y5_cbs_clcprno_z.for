C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y5_cbs_clcprno.for,v 1.8 2016/04/01 14:26:17 secretyv Exp $
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_Y5_CBS_CLCPRNO
C     SUBROUTINE SV2D_Y5_CBS_CLCPRNEV
C     SUBROUTINE SV2D_Y5_CBS_CLCPRNES
C   Private:
C     SUBROUTINE SV2D_Y5_CBS_CLCPRN_1N
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_Y5_CBS_CLCPRNEV_Z
C
C Description:
C     Calcul des propriétés nodales d'un élément de volume,
C     dépendantes de VDLE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_CLCPRNEV_Z(VPRGL,
     &                                  VPRNE,
     &                                  VDLE,
     &                                  IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y5_CBS_CLCPRNEV_Z
CDEC$ ENDIF

      USE COMPLEXIFY
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8     VPRGL(LM_CMMN_NPRGL)
      COMPLEX*16 VPRNE(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      COMPLEX*16 VDLE (LM_CMMN_NDLN,  EG_CMMN_NNELV)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IN
      INTEGER IPV, IPH
      COMPLEX*16 BATHY, EPAIGL, PROF, H
      COMPLEX*16 PRFE, PRFA

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPV = SV2D_IPRNO_V
      IPH = SV2D_IPRNO_H

C---     Bloque à H positif
!!      DO IN=1,EG_CMMN_NNELV
!!         H = VPRNE(SV2D_IPRNO_Z,IN) + SV2D_DECOU_HMIN
!!         VDLE(3,IN) = MAX(VDLE(3,IN), H)
!!      ENDDO

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2
      VDLE(3,NO4) = (VDLE(3,NO3)+VDLE(3,NO5))*UN_2
      VDLE(3,NO6) = (VDLE(3,NO5)+VDLE(3,NO1))*UN_2

C---     Profondeurs effective et absolue
      DO IN=1,EG_CMMN_NNELV
         BATHY   = VPRNE(SV2D_IPRNO_Z,IN)
         EPAIGL  = 0.9D0 * VPRNE(SV2D_IPRNO_ICE_E,IN)
         PROF    = VDLE(3,IN) - BATHY
         EPAIGL  = MIN(EPAIGL, PROF)
         EPAIGL  = MAX(EPAIGL, ZERO)
         PRFE    = PROF - EPAIGL                ! Prof effective
         PRFA    = MAX(PRFE, SV2D_DECOU_HMIN)   ! Prof absolue
         VPRNE(IPV,IN) = PRFE                   ! Prof effective temp.
         VPRNE(IPH,IN) = PRFA                   ! Prof absolue
      ENDDO

C---     Impose les profondeurs sur les noeuds milieux
      VPRNE(IPV,NO2) = (VPRNE(IPV,NO1)+VPRNE(IPV,NO3))*UN_2
      VPRNE(IPV,NO4) = (VPRNE(IPV,NO3)+VPRNE(IPV,NO5))*UN_2
      VPRNE(IPV,NO6) = (VPRNE(IPV,NO5)+VPRNE(IPV,NO1))*UN_2

C        Profondeur linéaire
C        Noeud rehaussé si un noeud sommet est découvert
      VPRNE(IPH,NO2) = (VPRNE(IPH,NO1)+VPRNE(IPH,NO3))*UN_2
      VPRNE(IPH,NO4) = (VPRNE(IPH,NO3)+VPRNE(IPH,NO5))*UN_2
      VPRNE(IPH,NO6) = (VPRNE(IPH,NO5)+VPRNE(IPH,NO1))*UN_2

C---     Boucle sur les noeuds
      DO IN=1,EG_CMMN_NNELV
         CALL SV2D_Y5_CBS_CLCPRN_1N_Z(VPRGL,VPRNE(1,IN),VDLE(1,IN),IERR)
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_Y5_CBS_CLCPRNES
C
C Description:
C     Calcul des propriétés nodales d'un élément de surface,
C     dépendantes de VDLE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_CLCPRNES_Z(VPRGL,
     &                                  VPRNE,
     &                                  VDLE,
     &                                  IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y5_CBS_CLCPRNES_Z
CDEC$ ENDIF

      USE COMPLEXIFY
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8     VPRGL(LM_CMMN_NPRGL)
      COMPLEX*16 VPRNE(LM_CMMN_NPRNO, EG_CMMN_NNELS)
      COMPLEX*16 VDLE (LM_CMMN_NDLN,  EG_CMMN_NNELS)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IN
      INTEGER IPV, IPH
      COMPLEX*16 BATHY, EPAIGL, PROF, H
      COMPLEX*16 PRFE, PRFA

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPV = SV2D_IPRNO_V
      IPH = SV2D_IPRNO_H

C---     Bloque à H positif
!!      DO IN=1,EG_CMMN_NNELS
!!         H = VPRNE(SV2D_IPRNO_Z,IN) + SV2D_DECOU_HMIN
!!         VDLE(3,IN) = MAX(VDLE(3,IN), H)
!!      ENDDO

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2

C---     Profondeurs effective et absolue
      DO IN=1,EG_CMMN_NNELS
         BATHY   = VPRNE(SV2D_IPRNO_Z,IN)
         EPAIGL  = 0.9D0 * VPRNE(SV2D_IPRNO_ICE_E,IN)
         PROF    = VDLE(3,IN) - BATHY
         EPAIGL  = MIN(EPAIGL, PROF)
         EPAIGL  = MAX(EPAIGL, ZERO)
         PRFE    = PROF - EPAIGL                ! Prof effective
         PRFA    = MAX(PRFE, SV2D_DECOU_HMIN)   ! Prof absolue
         VPRNE(IPV,IN) = PRFE                   ! Prof effective temp.
         VPRNE(IPH,IN) = PRFA                   ! Prof absolue
      ENDDO

C---     Impose les profondeurs sur les noeuds milieux
      VPRNE(IPV,NO2) = (VPRNE(IPV,NO1)+VPRNE(IPV,NO3))*UN_2

C        Profondeur linéaire
C        Noeud rehaussé si un noeud sommet est découvert
      VPRNE(IPH,NO2) = (VPRNE(IPH,NO1)+VPRNE(IPH,NO3))*UN_2

C---     Boucle sur les noeuds
      DO IN=1,EG_CMMN_NNELS
         CALL SV2D_Y5_CBS_CLCPRN_1N_Z(VPRGL,VPRNE(1,IN),VDLE(1,IN),IERR)
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_Y5_CBS_CLCPRN_1N_Z
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG. Le calcul est
C     fait sur un noeud.
C
C Entrée:
C      REAL*8  VPRGL       Les PRopriétés GLobales
C      REAL*8  VDLN        Le Degré de Liberté Nodaux
C
C Sortie:
C      REAL*8  VPRN        Les PRopriétés Nodales
C      INTEGER IERR
C
C Notes:
C     En PRFA, un noeud milieu va se retrouver rehaussé si un noeud sommet
C     est découvert. Il y a un impact sur le coefficient de frottement
C     qui est diminué.
C     ULIM vise à "freiner" les fortes vitesses, celles au-delà de UMAX.
C     Jusqu'à UMAX, la loi est linéaire, puis exp 1/3
C     Le vent est mis à 0 en présence de glace. Mais si le vent est relatif,
C     il y aura quand même la contribution du courant.
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_CLCPRN_1N_Z(VPRGL,
     &                                   VPRN,
     &                                   VDLN,
     &                                   IERR)

      USE COMPLEXIFY
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8     VPRGL(LM_CMMN_NPRGL)
      COMPLEX*16 VPRN (LM_CMMN_NPRNO)
      COMPLEX*16 VDLN (LM_CMMN_NDLN)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8, PARAMETER :: RHO_AIR = 1.2475D+00
      REAL*8, PARAMETER :: RHO_EAU = 1.0000D+03
      REAL*8, PARAMETER :: RHO_REL = RHO_AIR / RHO_EAU

      REAL*8, PARAMETER :: R_4_3_NEG = -4.0D0/3.0D0
      REAL*8, PARAMETER :: R_1_3     =  1.0D0/3.0D0

      COMPLEX*16 ALFA
      COMPLEX*16 H1, H2
      COMPLEX*16 PRFE, PRFA, UN_PRFA
      COMPLEX*16 VX, VY
      COMPLEX*16 WX, WY, VENT
      COMPLEX*16 VN1, VN2
      REAL*8  PORO, VMAN, VFLN, FCVT, FGRA, VDIF, VDRC, PENA
      REAL*8  NTOT, FROTT
      REAL*8  VMOD, WMOD
C-----------------------------------------------------------------------
      REAL*8 ULIM, UEXP, U
      UEXP(U) = (U-SV2D_DECOU_UMAX+UN)**R_1_3 + SV2D_DECOU_UMAX-UN
      ULIM(U) = MIN(UEXP(MAX(U,SV2D_DECOU_UMAX)), U)
C-----------------------------------------------------------------------

C---     Profondeur
      PRFE = VPRN(SV2D_IPRNO_V)           ! Prof effective nodale
      PRFA = VPRN(SV2D_IPRNO_H)           ! Prof absolue linéaire
      UN_PRFA = UN / PRFA                 ! Inverse prof absolue

C---     ALFA
      H1 = SV2D_DECOU_HMIN
      H2 = SV2D_DECOU_HTRG
      ALFA = (PRFE-H1)/(H2-H1)
      ALFA = MIN(UN, MAX(ZERO, ALFA))
      VN1  = UN - ALFA
      VN2  = ALFA

C---     Module de la vitesse
      VX = VDLN(1) * UN_PRFA
      VY = VDLN(2) * UN_PRFA
      VMOD = SQRT(VX*VX + VY*VY)

C---     Module du vent relatif
      WX = VPRN(SV2D_IPRNO_WND_X) - SV2D_CMULT_VENT_REL*VX
      WY = VPRN(SV2D_IPRNO_WND_Y) - SV2D_CMULT_VENT_REL*VY
      WMOD = SQRT(WX*WX + WY*WY)
      VENT = RHO_REL * SP_HDRO_CW(WMOD, SV2D_FCT_CW_VENT) * WMOD

C---     Manning global
      NTOT = SQRT(VPRN(SV2D_IPRNO_N)**2 + VPRN(SV2D_IPRNO_ICE_N)**2)

C---     Paramètres variables pour le découvrement
      VMOD = VN1*ULIM(VMOD)          + VN2*VMOD
      PORO = VN1*SV2D_DECOU_PORO     + VN2 !*UN
      VFLN = VN1*SV2D_DECOU_AMORT    + VN2*SV2D_STABI_AMORT
      VMAN = VN1*SV2D_DECOU_MAN      + VN2*SV2D_CMULT_MAN*NTOT
      FCVT = VN1*SV2D_DECOU_CON_FACT + VN2*SV2D_CMULT_CON
      FGRA = VN1*SV2D_DECOU_GRA_FACT + VN2*SV2D_CMULT_GRA
      VDIF = VN1*SV2D_DECOU_DIF_NU  !+ VN2*0.0
      VDRC = VN1*SV2D_DECOU_DRC_NU   + VN2*SV2D_STABI_DARCY
      VENT =                         + VN2*SV2D_CMULT_VENT*VENT
      PENA = VN1                    !+ VN2*0.0

C---     Frottement
!      L'utilisation de la masse lumped pour découpler le frottement
!      mène à des résultats très différents d'Hydrosim.
!      FROTT = SV2D_GRAVITE*VMAN*VMAN * UMOD * (UN_PRFA**R_4_3)
      FROTT = VFLN + SV2D_GRAVITE*VMAN*VMAN * VMOD * PRFA**R_4_3_NEG ! Hydrosim

C---     Valeurs nodales
      VPRN(SV2D_IPRNO_U)          = VDLN(1) * UN_PRFA ! U
      VPRN(SV2D_IPRNO_V)          = VDLN(2) * UN_PRFA ! V
      VPRN(SV2D_IPRNO_H)          = PRFA              ! Prof absolue
      VPRN(SV2D_IPRNO_COEFF_FROT) = FROTT             ! g n2 |u| / H**(4/3)
      VPRN(SV2D_IPRNO_COEFF_CNVT) = FCVT              ! Facteur de convection
      VPRN(SV2D_IPRNO_COEFF_GRVT) = FGRA              ! Facteur de gravité
      VPRN(SV2D_IPRNO_COEFF_DIFF) = VDIF              ! Diffusion de decou.
      VPRN(SV2D_IPRNO_COEFF_DRCY) = VDRC              ! Darcy
      VPRN(SV2D_IPRNO_COEFF_VENT) = VENT              ! cw |w| rho_air/rho_eau
      VPRN(SV2D_IPRNO_COEFF_PORO) = PORO
      VPRN(SV2D_IPRNO_DECOU_PENA) = PENA

      IERR = ERR_TYP()
      RETURN
      END
