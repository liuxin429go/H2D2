C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y5_cnn_inivtbl.for,v 1.6 2016/02/12 17:42:42 secretyv Exp $
C
C Functions:
C   Public:
C   Private:
C     SUBROUTINE SV2D_Y5_CNN_INIVTBL
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction SV2D_Y5_CNN_INIVTBL initialise et remplis la table
C     virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne gère pas l'héritage multiple en diamant.
C************************************************************************
      FUNCTION SV2D_Y5_CNN_INIVTBL(H)

      IMPLICIT NONE

      INTEGER H

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_y4_cnn.fc'
      INCLUDE 'sv2d_y5_cnn.fc'

      INTEGER I
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SO_VTBL_HVALIDE(H))
C-----------------------------------------------------------------------

C---     Appelle le parent
      IF (ERR_GOOD()) I = SV2D_Y4_CNN_INIVTBL(H)

C---     Redéfinis les fonctions de l'interface
      IF (ERR_GOOD()) THEN
         I=SV2D_CBS_AJTFSO(H,EA_FUNC_PSLPRGL, 'SV2D_Y5_CBS_PSLPRGL')
         I=SV2D_CBS_AJTFSO(H,EA_FUNC_PRCPRGL, 'SV2D_Y5_CBS_PRCPRGL')
         I=SV2D_CBS_AJTFSO(H,EA_FUNC_PSLPRNO, 'SV2D_Y5_CBS_PSLPRNO')
         I=SV2D_CBS_AJTFSO(H,EA_FUNC_PSLPREV, 'SV2D_Y5_CBS_PSLPREV')
         I=SV2D_CBS_AJTFSO(H,EA_FUNC_CLCPRNO, 'SV2D_Y5_CBS_CLCPRNO')
         I=SV2D_CBS_AJTFSO(H,EA_FUNC_CLCPREV, 'SV2D_Y5_CBS_CLCPREV')
         I=SV2D_CBS_AJTFSO(H,EA_FUNC_ASMKT,   'SV2D_CBS_ASMKT_R') !_Z')
         I=SV2D_CBS_AJTFSO(H,EA_FUNC_REQPRM,  'SV2D_Y5_CBS_REQPRM')

         I=SV2D_CBS_AJTFSO(H,SV2D_VT_CLCPRNEV,'SV2D_Y5_CBS_CLCPRNEV') !_Z')
         I=SV2D_CBS_AJTFSO(H,SV2D_VT_CLCPRNES,'SV2D_Y5_CBS_CLCPRNES') !_Z')
         I=SV2D_CBS_AJTFSO(H,SV2D_VT_CLCPREVE,'SV2D_Y5_CBS_CLCPREVE') !_Z')
         I=SV2D_CBS_AJTFSO(H,SV2D_VT_CLCPRESE,'SV2D_CBS_CLCPRESE')    !_Z')
      ENDIF

      SV2D_Y5_CNN_INIVTBL = ERR_TYP()
      RETURN
      END
