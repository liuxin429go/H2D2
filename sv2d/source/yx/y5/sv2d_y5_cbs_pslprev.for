C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_pslprev.for,v 1.11 2012/01/10 20:20:00 secretyv Exp $
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_Y5_CBS_PSLPREV
C   Private:
C     SUBROUTINE SV2D_Y5_CBS_INI_IPREV
C
C************************************************************************


C************************************************************************
C Sommaire : SV2D_Y5_CBS_PSLPREV
C
C Description:
C     Traitement post-lecture des propriétés élémentaires de volume
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_PSLPREV (VPREV, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y5_CBS_PSLPREV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      INTEGER  IERR

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      CALL SV2D_Y5_CBS_INI_IPREV()

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SV2D_Y5_CBS_INI_IPREV
C
C Description:
C     La fonction privée SV2D_Y5_CBS_INI_IPREV initialise les indices
C     des propriétés élémentaires.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_INI_IPREV()

      IMPLICIT NONE

      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

      SV2D_IPREV_VTRB =  1
      SV2D_IPREV_VTOT =  2
      SV2D_IPREV_STTS =  3

      RETURN
      END
