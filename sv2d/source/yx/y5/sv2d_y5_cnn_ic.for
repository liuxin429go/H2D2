C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y5_cnn_ic.for,v 1.3 2016/04/20 20:39:36 secretyv Exp $
C Groupe:  Saint-Venant 2D
C Objet:   Élément Y5 CNN
C Type:    ---
C************************************************************************

C************************************************************************
C Sommaire: IC_SV2D_Y5_CNN_000
C
C Description:
C     La fonction IC_SV2D_Y5_CNN_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_Y5_CNN_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_Y5_CNN_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_y5_cnn_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icsimd.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
      INTEGER  IC_SV2D_Y5_CNN_XEQCTR_TRV
      EXTERNAL IC_SV2D_Y5_CNN_XEQCTR_TRV
C-----------------------------------------------------------------------

C---     Enregistre 
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC,
     &                                      IC_SV2D_Y5_CNN_XEQCTR_TRV)
      IF (ERR_GOOD()) IERR = IC_SIMD_AJTCTR('SV2D_Conservatif_CDY5_NN',
     &                                       HFNC)
      
      IC_SV2D_Y5_CNN_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_Y5_CNN_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_Y5_CNN_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_y5_cnn_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      
      INTEGER IERR, IRET
      INTEGER HOBJ
      INTEGER HFRML
      INTEGER HGRID
      INTEGER HDLIB
      INTEGER HCLIM
      INTEGER HSOLC
      INTEGER HSOLR
      INTEGER HPRGL
      INTEGER HPRNO
      INTEGER HPREL
      INTEGER KI(9)
      CHARACTER*(256) NOM
      INTEGER IC_SV2D_Y5_CNN_XEQCTR_TRV
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SV2D_Y5_CNN_AID()
            GOTO 9999
         ENDIF
      ENDIF
      
C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_SV2D_Y5_CNN'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the mesh</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HGRID)
C     <comment>Handle on the degrees of freedom (unknowns)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HDLIB)
C     <comment>Handle on the boundary conditions</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HCLIM)
C     <comment>Handle on the concentrated solicitations</comment> 
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, HSOLC)
C     <comment>Handle on the distributed solicitations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 5, HSOLR)
C     <comment>Handle on the global properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 6, HPRGL)
C     <comment>Handle on the nodal properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 7, HPRNO)
C     <comment>Handle on the elemental properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 8, HPREL)
      IF (IERR .NE. 0) GOTO 9901

C---     Construis l'objet
      HFRML = 0
      KI = (/ HFRML,HGRID,HDLIB,HCLIM,HSOLC,HSOLR,HPRGL,HPRNO,HPREL /)
      IERR = IC_SV2D_Y5_CNN_XEQCTR_TRV(HOBJ, KI)
      
C---     Impression des paramètres du bloc
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Retourne la handle sur le parent virtuel
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the element</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>      
C  The constructor <b>sv2d_y5_cnn</b> constructs an object, with the given 
C  arguments, and returns a handle on this object.  
C</comment>     
      
      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SV2D_Y5_CNN_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_SV2D_Y5_CNN_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_Y5_CNN_XEQCTR_TRV(HOBJ, KI)

      IMPLICIT NONE

      INTEGER IC_SV2D_Y5_CNN_XEQCTR_TRV
      INTEGER HOBJ
      INTEGER KI(*)

      INCLUDE 'sv2d_y5_cnn_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'sv2d_lgcy.fi'

      INTEGER IERR
      INTEGER HGRID
      INTEGER HDLIB
      INTEGER HCLIM
      INTEGER HSOLC
      INTEGER HSOLR
      INTEGER HPRGL
      INTEGER HPRNO
      INTEGER HPREL
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les handles
      HGRID = KI(2)
      HDLIB = KI(3)
      HCLIM = KI(4)
      HSOLC = KI(5)
      HSOLR = KI(6)
      HPRGL = KI(7)
      HPRNO = KI(8)
      HPREL = KI(9)

C---     Construis l'objet
      HOBJ = 0
      IF (ERR_GOOD()) IERR = SV2D_LGCY_CTR(HOBJ,
     &                                     'SV2D_Conservatif_CDY5_NN',
     &                                     EG_TPGEO_T6L)
      IF (ERR_GOOD()) IERR = SV2D_LGCY_INI(HOBJ,
     &                                     HGRID,
     &                                     HDLIB,
     &                                     HCLIM,
     &                                     HSOLC,
     &                                     HSOLR,
     &                                     HPRGL,
     &                                     HPRNO,
     &                                     HPREL)

      IC_SV2D_Y5_CNN_XEQCTR_TRV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_SV2D_Y5_CNN_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type SV2D_Y5_CNN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_Y5_CNN_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_Y5_CNN_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_y5_cnn_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmelem_ic.fi'
      INCLUDE 'sv2d_lgcy.fi'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SV2D_LGCY_DTR(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>         
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_SV2D_Y5_CNN_AID()

      ELSE
C        <include>IC_LM_ELEM_XEQMTH@lmelem_ic.for</include>
         IERR = IC_LM_ELEM_XEQMTH(HOBJ, IMTH, IPRM)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SV2D_Y5_CNN_AID()

9999  CONTINUE
      IC_SV2D_Y5_CNN_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_SV2D_Y5_CNN_OPBDOT(...) exécute les méthodes statiques
C     sur la classe SV2D_Y5_CNN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_Y5_CNN_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_Y5_CNN_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_y5_cnn_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(HOBJ .EQ. IC_SV2D_Y5_CNN_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>help</b> displays the help content for the class.</comment>         
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL SV2D_Y5_INI_IPRGL()

         CALL IC_SV2D_Y5_CNN_AID()
         CALL SV2D_CBS_HLPPRGL()
         CALL SV2D_CBS_HLPPRNO()
         CALL SV2D_CBS_HLPCLIM()

C     <comment>The method <b>help_prgl</b> prints the global properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prgl') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL SV2D_Y5_INI_IPRGL()
         CALL SV2D_CBS_HLPPRGL()

C     <comment>The method <b>help_prno</b> prints the nodal properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prno') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL SV2D_CBS_HLPPRNO()

C     <comment>The method <b>help_bc</b> prints the boundary conditions of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_bc') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL SV2D_CBS_HLPCLIM()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SV2D_Y5_CNN_AID()

9999  CONTINUE
      IC_SV2D_Y5_CNN_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_Y5_CNN_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_Y5_CNN_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_y5_cnn_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>sv2d_y5</b> is a finite element for the 2D St-Venant equations
C     (shallow water) written in conservation form. 
C     The drying/wetting formulation is Y5.
C     The degree of freedom are node numbered.  (Status: Prototype)
C</comment>
      IC_SV2D_Y5_CNN_REQCLS = 'sv2d_y5'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_Y5_CNN_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_Y5_CNN_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_y5_cnn_ic.fi'
      INCLUDE 'sv2d_y5_cnn.fi'
C-------------------------------------------------------------------------

      IC_SV2D_Y5_CNN_REQHDL = SV2D_Y5_CNN_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description: 
C     La fonction IC_HMCOMP_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SV2D_Y5_CNN_AID()

      INCLUDE 'log.fi'
     
      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_y5_cnn_ic.hlp')
      RETURN
      END
      