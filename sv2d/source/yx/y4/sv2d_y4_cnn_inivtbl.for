C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction SV2D_Y4_CNN_INIVTBL initialise et remplis la table
C     virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne gère pas l'héritage multiple en diamant.
C************************************************************************
      FUNCTION SV2D_Y4_CNN_INIVTBL(H)

      IMPLICIT NONE

      INTEGER H

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_y3_cnn.fc'
      INCLUDE 'sv2d_y4_cnn.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SO_VTBL_HVALIDE(H))
C-----------------------------------------------------------------------

C---     Appelle le parent
      IF (ERR_GOOD()) IERR = SV2D_Y3_CNN_INIVTBL(H)

C---     Redéfinis les fonctions de l'interface
      IF (ERR_GOOD()) THEN
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_PSLPRGL, 'SV2D_Y4_CBS_PSLPRGL')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_PRCPRGL, 'SV2D_Y4_CBS_PRCPRGL')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_PSLPRNO, 'SV2D_Y4_CBS_PSLPRNO')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_CLCPRNO, 'SV2D_Y4_CBS_CLCPRNO')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_CLCPREV, 'SV2D_Y4_CBS_CLCPREV')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_ASMF,    'SV2D_CBS_ASMF_R')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_ASMK,    'SV2D_CBS_ASMK_R')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_ASMKT,   'SV2D_CBS_ASMKT_R')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_ASMKU,   'SV2D_CBS_ASMKU_R')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_REQPRM,  'SV2D_Y4_CBS_REQPRM')

         IERR=SV2D_CBS_AJTFSO(H,SV2D_VT_CLCPRNEV,'SV2D_Y4_CBS_CLCPRNEV')
         IERR=SV2D_CBS_AJTFSO(H,SV2D_VT_CLCPRNES,'SV2D_Y4_CBS_CLCPRNES')
         IERR=SV2D_CBS_AJTFSO(H,SV2D_VT_CLCPRN1N,'SV2D_Y4_CBS_CLCPRN1N')
         IERR=SV2D_CBS_AJTFSO(H,SV2D_VT_CLCPREVE,'SV2D_Y4_CBS_CLCPREVE')
       ENDIF

      SV2D_Y4_CNN_INIVTBL = ERR_TYP()
      RETURN
      END
