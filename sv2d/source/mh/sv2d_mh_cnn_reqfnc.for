C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction SV2D_MH_CNN_REQFNC retourne le handle de la fonction
C     associée au code demandé.
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     HFNC     Handle sur la fonction
C
C Notes:
C************************************************************************
      FUNCTION SV2D_MH_CNN_REQFNC(IFNC, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_MH_CNN_REQFNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IFNC
      INTEGER HFNC

      INCLUDE 'sv2d_mh_cnn.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_mh_cnn.fc'
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SV2D_MH_CNN_HVFT .NE. 0)
C-----------------------------------------------------------------------

      SV2D_MH_CNN_REQFNC = SV2D_CBS_REQFNC(SV2D_MH_CNN_HVFT, IFNC, HFNC)
      RETURN
      END
