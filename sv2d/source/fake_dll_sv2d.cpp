//************************************************************************
// H2D2 - External declaration of public symbols
// Module: sv2d
// Entry point: extern "C" void fake_dll_sv2d()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-02-03 18:07:49.810342
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_SV2D
F_PROT(IC_SV2D_LGCY_XEQCTR, ic_sv2d_lgcy_xeqctr);
F_PROT(IC_SV2D_LGCY_XEQMTH, ic_sv2d_lgcy_xeqmth);
F_PROT(IC_SV2D_LGCY_OPBDOT, ic_sv2d_lgcy_opbdot);
F_PROT(IC_SV2D_LGCY_REQCLS, ic_sv2d_lgcy_reqcls);
F_PROT(IC_SV2D_LGCY_REQHDL, ic_sv2d_lgcy_reqhdl);
F_PROT(IC_SV2D_Y2_CNN_000, ic_sv2d_y2_cnn_000);
F_PROT(IC_SV2D_Y2_CNN_XEQCTR, ic_sv2d_y2_cnn_xeqctr);
F_PROT(IC_SV2D_Y2_CNN_XEQMTH, ic_sv2d_y2_cnn_xeqmth);
F_PROT(IC_SV2D_Y2_CNN_OPBDOT, ic_sv2d_y2_cnn_opbdot);
F_PROT(IC_SV2D_Y2_CNN_REQCLS, ic_sv2d_y2_cnn_reqcls);
F_PROT(IC_SV2D_Y2_CNN_REQHDL, ic_sv2d_y2_cnn_reqhdl);
F_PROT(IC_SV2D_Y3_CNN_000, ic_sv2d_y3_cnn_000);
F_PROT(IC_SV2D_Y3_CNN_XEQCTR, ic_sv2d_y3_cnn_xeqctr);
F_PROT(IC_SV2D_Y3_CNN_XEQMTH, ic_sv2d_y3_cnn_xeqmth);
F_PROT(IC_SV2D_Y3_CNN_OPBDOT, ic_sv2d_y3_cnn_opbdot);
F_PROT(IC_SV2D_Y3_CNN_REQCLS, ic_sv2d_y3_cnn_reqcls);
F_PROT(IC_SV2D_Y3_CNN_REQHDL, ic_sv2d_y3_cnn_reqhdl);
F_PROT(IC_SV2D_Y4_CNN_000, ic_sv2d_y4_cnn_000);
F_PROT(IC_SV2D_Y4_CNN_XEQCTR, ic_sv2d_y4_cnn_xeqctr);
F_PROT(IC_SV2D_Y4_CNN_XEQMTH, ic_sv2d_y4_cnn_xeqmth);
F_PROT(IC_SV2D_Y4_CNN_OPBDOT, ic_sv2d_y4_cnn_opbdot);
F_PROT(IC_SV2D_Y4_CNN_REQCLS, ic_sv2d_y4_cnn_reqcls);
F_PROT(IC_SV2D_Y4_CNN_REQHDL, ic_sv2d_y4_cnn_reqhdl);
F_PROT(IC_SV2D_Z4_CNN_000, ic_sv2d_z4_cnn_000);
F_PROT(IC_SV2D_Z4_CNN_XEQCTR, ic_sv2d_z4_cnn_xeqctr);
F_PROT(IC_SV2D_Z4_CNN_XEQMTH, ic_sv2d_z4_cnn_xeqmth);
F_PROT(IC_SV2D_Z4_CNN_OPBDOT, ic_sv2d_z4_cnn_opbdot);
F_PROT(IC_SV2D_Z4_CNN_REQCLS, ic_sv2d_z4_cnn_reqcls);
F_PROT(IC_SV2D_Z4_CNN_REQHDL, ic_sv2d_z4_cnn_reqhdl);
F_PROT(IC_SV2D_PST_CD2D_XEQCTR, ic_sv2d_pst_cd2d_xeqctr);
F_PROT(IC_SV2D_PST_CD2D_XEQMTH, ic_sv2d_pst_cd2d_xeqmth);
F_PROT(IC_SV2D_PST_CD2D_REQCLS, ic_sv2d_pst_cd2d_reqcls);
F_PROT(IC_SV2D_PST_CD2D_REQHDL, ic_sv2d_pst_cd2d_reqhdl);
F_PROT(IC_SV2D_PST_CFL_XEQCTR, ic_sv2d_pst_cfl_xeqctr);
F_PROT(IC_SV2D_PST_CFL_XEQMTH, ic_sv2d_pst_cfl_xeqmth);
F_PROT(IC_SV2D_PST_CFL_REQCLS, ic_sv2d_pst_cfl_reqcls);
F_PROT(IC_SV2D_PST_CFL_REQHDL, ic_sv2d_pst_cfl_reqhdl);
F_PROT(IC_SV2D_PST_CHKQ_CMD, ic_sv2d_pst_chkq_cmd);
F_PROT(IC_SV2D_PST_CHKQ_REQCMD, ic_sv2d_pst_chkq_reqcmd);
F_PROT(IC_SV2D_PST_CMP_XEQCTR, ic_sv2d_pst_cmp_xeqctr);
F_PROT(IC_SV2D_PST_CMP_XEQMTH, ic_sv2d_pst_cmp_xeqmth);
F_PROT(IC_SV2D_PST_CMP_REQCLS, ic_sv2d_pst_cmp_reqcls);
F_PROT(IC_SV2D_PST_CMP_REQHDL, ic_sv2d_pst_cmp_reqhdl);
F_PROT(IC_SV2D_PST_Q_CMD, ic_sv2d_pst_q_cmd);
F_PROT(IC_SV2D_PST_Q_REQCMD, ic_sv2d_pst_q_reqcmd);
F_PROT(IC_SV2D_PST_SIM_XEQCTR, ic_sv2d_pst_sim_xeqctr);
F_PROT(IC_SV2D_PST_SIM_XEQMTH, ic_sv2d_pst_sim_xeqmth);
F_PROT(IC_SV2D_PST_SIM_REQCLS, ic_sv2d_pst_sim_reqcls);
F_PROT(IC_SV2D_PST_SIM_REQHDL, ic_sv2d_pst_sim_reqhdl);
 
// ---  class SV2D_CBS
F_PROT(SV2D_CBS_ASGCMN, sv2d_cbs_asgcmn);
F_PROT(SV2D_CBS_DMPPROP, sv2d_cbs_dmpprop);
F_PROT(SV2D_CBS_REQFNC, sv2d_cbs_reqfnc);
F_PROT(SV2D_CBS_REQPRM, sv2d_cbs_reqprm);
F_PROT(SV2D_CBS_ASMM, sv2d_cbs_asmm);
F_PROT(SV2D_CBS_ASMMU, sv2d_cbs_asmmu);
F_PROT(SV2D_CBS_ASMF_K, sv2d_cbs_asmf_k);
F_PROT(SV2D_CBS_ASMKT_K, sv2d_cbs_asmkt_k);
F_PROT(SV2D_CBS_ASMKU_K, sv2d_cbs_asmku_k);
F_PROT(SV2D_CBS_ASMK_K, sv2d_cbs_asmk_k);
F_PROT(SV2D_CBS_ASMF_R, sv2d_cbs_asmf_r);
F_PROT(SV2D_CBS_ASMKT_R, sv2d_cbs_asmkt_r);
F_PROT(SV2D_CBS_ASMKT_R_Z, sv2d_cbs_asmkt_r_z);
F_PROT(SV2D_CBS_ASMKU_R, sv2d_cbs_asmku_r);
F_PROT(SV2D_CBS_ASMK_R, sv2d_cbs_asmk_r);
F_PROT(SV2D_CBS_CLCCLIM, sv2d_cbs_clcclim);
F_PROT(SV2D_CBS_HLPCLIM, sv2d_cbs_hlpclim);
F_PROT(SV2D_CBS_PRCCLIM, sv2d_cbs_prcclim);
F_PROT(SV2D_CBS_PRNCLIM, sv2d_cbs_prnclim);
F_PROT(SV2D_CBS_PSLCLIM, sv2d_cbs_pslclim);
F_PROT(SV2D_CBS_CLCDLIB, sv2d_cbs_clcdlib);
F_PROT(SV2D_CBS_PSLDLIB, sv2d_cbs_psldlib);
F_PROT(SV2D_CBS_CLCPRES, sv2d_cbs_clcpres);
F_PROT(SV2D_CBS_CLCPRESE, sv2d_cbs_clcprese);
F_PROT(SV2D_CBS_CLCPRESE_Z, sv2d_cbs_clcprese_z);
F_PROT(SV2D_CBS_PRCPRES, sv2d_cbs_prcpres);
F_PROT(SV2D_CBS_CLCPREV, sv2d_cbs_clcprev);
F_PROT(SV2D_CBS_CLCPREVE, sv2d_cbs_clcpreve);
F_PROT(SV2D_CBS_CLCPREVE_Z, sv2d_cbs_clcpreve_z);
F_PROT(SV2D_CBS_PRCPREV, sv2d_cbs_prcprev);
F_PROT(SV2D_CBS_PSLPREV, sv2d_cbs_pslprev);
F_PROT(SV2D_CBS_HLPPRGL, sv2d_cbs_hlpprgl);
F_PROT(SV2D_CBS_PRCPRGL, sv2d_cbs_prcprgl);
F_PROT(SV2D_CBS_PRNPRGL, sv2d_cbs_prnprgl);
F_PROT(SV2D_CBS_PSLPRGL, sv2d_cbs_pslprgl);
F_PROT(SV2D_CBS_HLPPRNO, sv2d_cbs_hlpprno);
F_PROT(SV2D_CBS_PRCPRNO, sv2d_cbs_prcprno);
F_PROT(SV2D_CBS_PRNPRNO, sv2d_cbs_prnprno);
F_PROT(SV2D_CBS_PSCPRNO, sv2d_cbs_pscprno);
F_PROT(SV2D_CBS_PSLPRNO, sv2d_cbs_pslprno);
F_PROT(SV2D_CBS_PRCSOLC, sv2d_cbs_prcsolc);
F_PROT(SV2D_CBS_PSLSOLC, sv2d_cbs_pslsolc);
F_PROT(SV2D_CBS_PRCSOLR, sv2d_cbs_prcsolr);
F_PROT(SV2D_CBS_PSLSOLR, sv2d_cbs_pslsolr);
 
// ---  class SV2D_CL
F_PROT(SV2D_CL_INIVTBL2, sv2d_cl_inivtbl2);
F_PROT(SV2D_CL_INIVTBL, sv2d_cl_inivtbl);
F_PROT(SV2D_CL_HLP, sv2d_cl_hlp);
F_PROT(SV2D_CL_COD, sv2d_cl_cod);
F_PROT(SV2D_CL_PRC, sv2d_cl_prc);
F_PROT(SV2D_CL_CLC, sv2d_cl_clc);
F_PROT(SV2D_CL_ASMF, sv2d_cl_asmf);
F_PROT(SV2D_CL_ASMKU, sv2d_cl_asmku);
F_PROT(SV2D_CL_ASMK, sv2d_cl_asmk);
F_PROT(SV2D_CL_ASMKT, sv2d_cl_asmkt);
F_PROT(SV2D_CL_ASMM, sv2d_cl_asmm);
F_PROT(SV2D_CL_ASMMU, sv2d_cl_asmmu);
 
// ---  class SV2D_CL001
F_PROT(SV2D_CL001_000, sv2d_cl001_000);
F_PROT(SV2D_CL001_999, sv2d_cl001_999);
F_PROT(SV2D_CL001_COD, sv2d_cl001_cod);
F_PROT(SV2D_CL001_PRC, sv2d_cl001_prc);
F_PROT(SV2D_CL001_HLP, sv2d_cl001_hlp);
 
// ---  class SV2D_CL002
F_PROT(SV2D_CL002_000, sv2d_cl002_000);
F_PROT(SV2D_CL002_999, sv2d_cl002_999);
F_PROT(SV2D_CL002_COD, sv2d_cl002_cod);
F_PROT(SV2D_CL002_PRC, sv2d_cl002_prc);
F_PROT(SV2D_CL002_HLP, sv2d_cl002_hlp);
 
// ---  class SV2D_CL003
F_PROT(SV2D_CL003_000, sv2d_cl003_000);
F_PROT(SV2D_CL003_999, sv2d_cl003_999);
F_PROT(SV2D_CL003_COD, sv2d_cl003_cod);
F_PROT(SV2D_CL003_PRC, sv2d_cl003_prc);
F_PROT(SV2D_CL003_HLP, sv2d_cl003_hlp);
 
// ---  class SV2D_CL004
F_PROT(SV2D_CL004_000, sv2d_cl004_000);
F_PROT(SV2D_CL004_999, sv2d_cl004_999);
F_PROT(SV2D_CL004_COD, sv2d_cl004_cod);
F_PROT(SV2D_CL004_PRC, sv2d_cl004_prc);
F_PROT(SV2D_CL004_HLP, sv2d_cl004_hlp);
 
// ---  class SV2D_CL010
F_PROT(SV2D_CL010_000, sv2d_cl010_000);
F_PROT(SV2D_CL010_999, sv2d_cl010_999);
F_PROT(SV2D_CL010_COD, sv2d_cl010_cod);
F_PROT(SV2D_CL010_ASMF, sv2d_cl010_asmf);
F_PROT(SV2D_CL010_HLP, sv2d_cl010_hlp);
 
// ---  class SV2D_CL011
F_PROT(SV2D_CL011_000, sv2d_cl011_000);
F_PROT(SV2D_CL011_999, sv2d_cl011_999);
F_PROT(SV2D_CL011_COD, sv2d_cl011_cod);
F_PROT(SV2D_CL011_ASMF, sv2d_cl011_asmf);
F_PROT(SV2D_CL011_HLP, sv2d_cl011_hlp);
 
// ---  class SV2D_CL012
F_PROT(SV2D_CL012_000, sv2d_cl012_000);
F_PROT(SV2D_CL012_999, sv2d_cl012_999);
F_PROT(SV2D_CL012_COD, sv2d_cl012_cod);
F_PROT(SV2D_CL012_ASMF, sv2d_cl012_asmf);
F_PROT(SV2D_CL012_HLP, sv2d_cl012_hlp);
 
// ---  class SV2D_CL100
F_PROT(SV2D_CL100_000, sv2d_cl100_000);
F_PROT(SV2D_CL100_999, sv2d_cl100_999);
F_PROT(SV2D_CL100_COD, sv2d_cl100_cod);
F_PROT(SV2D_CL100_HLP, sv2d_cl100_hlp);
 
// ---  class SV2D_CL101
F_PROT(SV2D_CL101_000, sv2d_cl101_000);
F_PROT(SV2D_CL101_999, sv2d_cl101_999);
F_PROT(SV2D_CL101_COD, sv2d_cl101_cod);
F_PROT(SV2D_CL101_PRC, sv2d_cl101_prc);
F_PROT(SV2D_CL101_HLP, sv2d_cl101_hlp);
 
// ---  class SV2D_CL102
F_PROT(SV2D_CL102_000, sv2d_cl102_000);
F_PROT(SV2D_CL102_999, sv2d_cl102_999);
F_PROT(SV2D_CL102_COD, sv2d_cl102_cod);
F_PROT(SV2D_CL102_PRC, sv2d_cl102_prc);
F_PROT(SV2D_CL102_HLP, sv2d_cl102_hlp);
 
// ---  class SV2D_CL103
F_PROT(SV2D_CL103_000, sv2d_cl103_000);
F_PROT(SV2D_CL103_999, sv2d_cl103_999);
F_PROT(SV2D_CL103_COD, sv2d_cl103_cod);
F_PROT(SV2D_CL103_PRC, sv2d_cl103_prc);
F_PROT(SV2D_CL103_HLP, sv2d_cl103_hlp);
 
// ---  class SV2D_CL104
F_PROT(SV2D_CL104_000, sv2d_cl104_000);
F_PROT(SV2D_CL104_999, sv2d_cl104_999);
F_PROT(SV2D_CL104_COD, sv2d_cl104_cod);
F_PROT(SV2D_CL104_PRC, sv2d_cl104_prc);
F_PROT(SV2D_CL104_HLP, sv2d_cl104_hlp);
 
// ---  class SV2D_CL111
F_PROT(SV2D_CL111_000, sv2d_cl111_000);
F_PROT(SV2D_CL111_999, sv2d_cl111_999);
F_PROT(SV2D_CL111_COD, sv2d_cl111_cod);
F_PROT(SV2D_CL111_PRC, sv2d_cl111_prc);
F_PROT(SV2D_CL111_ASMF, sv2d_cl111_asmf);
F_PROT(SV2D_CL111_ASMKU, sv2d_cl111_asmku);
F_PROT(SV2D_CL111_ASMK, sv2d_cl111_asmk);
F_PROT(SV2D_CL111_HLP, sv2d_cl111_hlp);
 
// ---  class SV2D_CL112
F_PROT(SV2D_CL112_000, sv2d_cl112_000);
F_PROT(SV2D_CL112_999, sv2d_cl112_999);
F_PROT(SV2D_CL112_COD, sv2d_cl112_cod);
F_PROT(SV2D_CL112_PRC, sv2d_cl112_prc);
F_PROT(SV2D_CL112_ASMF, sv2d_cl112_asmf);
F_PROT(SV2D_CL112_ASMKU, sv2d_cl112_asmku);
F_PROT(SV2D_CL112_ASMK, sv2d_cl112_asmk);
F_PROT(SV2D_CL112_HLP, sv2d_cl112_hlp);
 
// ---  class SV2D_CL113
F_PROT(SV2D_CL113_000, sv2d_cl113_000);
F_PROT(SV2D_CL113_999, sv2d_cl113_999);
F_PROT(SV2D_CL113_COD, sv2d_cl113_cod);
F_PROT(SV2D_CL113_PRC, sv2d_cl113_prc);
F_PROT(SV2D_CL113_ASMF, sv2d_cl113_asmf);
F_PROT(SV2D_CL113_ASMKU, sv2d_cl113_asmku);
F_PROT(SV2D_CL113_ASMK, sv2d_cl113_asmk);
F_PROT(SV2D_CL113_HLP, sv2d_cl113_hlp);
 
// ---  class SV2D_CL121
F_PROT(SV2D_CL121_000, sv2d_cl121_000);
F_PROT(SV2D_CL121_999, sv2d_cl121_999);
F_PROT(SV2D_CL121_COD, sv2d_cl121_cod);
F_PROT(SV2D_CL121_ASMF, sv2d_cl121_asmf);
F_PROT(SV2D_CL121_HLP, sv2d_cl121_hlp);
 
// ---  class SV2D_CL122
F_PROT(SV2D_CL122_000, sv2d_cl122_000);
F_PROT(SV2D_CL122_999, sv2d_cl122_999);
F_PROT(SV2D_CL122_COD, sv2d_cl122_cod);
F_PROT(SV2D_CL122_PRC, sv2d_cl122_prc);
F_PROT(SV2D_CL122_ASMF, sv2d_cl122_asmf);
F_PROT(SV2D_CL122_HLP, sv2d_cl122_hlp);
 
// ---  class SV2D_CL123
F_PROT(SV2D_CL123_000, sv2d_cl123_000);
F_PROT(SV2D_CL123_999, sv2d_cl123_999);
F_PROT(SV2D_CL123_COD, sv2d_cl123_cod);
F_PROT(SV2D_CL123_ASMF, sv2d_cl123_asmf);
F_PROT(SV2D_CL123_HLP, sv2d_cl123_hlp);
 
// ---  class SV2D_CL124
F_PROT(SV2D_CL124_000, sv2d_cl124_000);
F_PROT(SV2D_CL124_999, sv2d_cl124_999);
F_PROT(SV2D_CL124_COD, sv2d_cl124_cod);
F_PROT(SV2D_CL124_ASMF, sv2d_cl124_asmf);
F_PROT(SV2D_CL124_HLP, sv2d_cl124_hlp);
 
// ---  class SV2D_CL125
F_PROT(SV2D_CL125_000, sv2d_cl125_000);
F_PROT(SV2D_CL125_999, sv2d_cl125_999);
F_PROT(SV2D_CL125_COD, sv2d_cl125_cod);
F_PROT(SV2D_CL125_CLC, sv2d_cl125_clc);
F_PROT(SV2D_CL125_ASMF, sv2d_cl125_asmf);
F_PROT(SV2D_CL125_ASMKT, sv2d_cl125_asmkt);
F_PROT(SV2D_CL125_HLP, sv2d_cl125_hlp);
 
// ---  class SV2D_CL126
F_PROT(SV2D_CL126_000, sv2d_cl126_000);
F_PROT(SV2D_CL126_999, sv2d_cl126_999);
F_PROT(SV2D_CL126_COD, sv2d_cl126_cod);
F_PROT(SV2D_CL126_CLC, sv2d_cl126_clc);
F_PROT(SV2D_CL126_ASMF, sv2d_cl126_asmf);
F_PROT(SV2D_CL126_ASMKT, sv2d_cl126_asmkt);
F_PROT(SV2D_CL126_HLP, sv2d_cl126_hlp);
 
// ---  class SV2D_CL133
F_PROT(SV2D_CL133_000, sv2d_cl133_000);
F_PROT(SV2D_CL133_999, sv2d_cl133_999);
F_PROT(SV2D_CL133_COD, sv2d_cl133_cod);
F_PROT(SV2D_CL133_CLC, sv2d_cl133_clc);
F_PROT(SV2D_CL133_ASMF, sv2d_cl133_asmf);
F_PROT(SV2D_CL133_ASMKU, sv2d_cl133_asmku);
F_PROT(SV2D_CL133_ASMK, sv2d_cl133_asmk);
F_PROT(SV2D_CL133_ASMKT, sv2d_cl133_asmkt);
F_PROT(SV2D_CL133_HLP, sv2d_cl133_hlp);
 
// ---  class SV2D_CL138
F_PROT(SV2D_CL138_000, sv2d_cl138_000);
F_PROT(SV2D_CL138_999, sv2d_cl138_999);
F_PROT(SV2D_CL138_COD, sv2d_cl138_cod);
F_PROT(SV2D_CL138_ASMF, sv2d_cl138_asmf);
F_PROT(SV2D_CL138_HLP, sv2d_cl138_hlp);
 
// ---  class SV2D_CL141
F_PROT(SV2D_CL141_000, sv2d_cl141_000);
F_PROT(SV2D_CL141_999, sv2d_cl141_999);
F_PROT(SV2D_CL141_COD, sv2d_cl141_cod);
F_PROT(SV2D_CL141_ASMK, sv2d_cl141_asmk);
F_PROT(SV2D_CL141_ASMKU, sv2d_cl141_asmku);
F_PROT(SV2D_CL141_HLP, sv2d_cl141_hlp);
 
// ---  class SV2D_CL142
F_PROT(SV2D_CL142_000, sv2d_cl142_000);
F_PROT(SV2D_CL142_999, sv2d_cl142_999);
F_PROT(SV2D_CL142_COD, sv2d_cl142_cod);
F_PROT(SV2D_CL142_ASMF, sv2d_cl142_asmf);
 
// ---  class SV2D_CL143
F_PROT(SV2D_CL143_000, sv2d_cl143_000);
F_PROT(SV2D_CL143_999, sv2d_cl143_999);
F_PROT(SV2D_CL143_COD, sv2d_cl143_cod);
F_PROT(SV2D_CL143_CLC, sv2d_cl143_clc);
F_PROT(SV2D_CL143_ASMF, sv2d_cl143_asmf);
F_PROT(SV2D_CL143_ASMKU, sv2d_cl143_asmku);
F_PROT(SV2D_CL143_ASMK, sv2d_cl143_asmk);
F_PROT(SV2D_CL143_ASMKT, sv2d_cl143_asmkt);
 
// ---  class SV2D_CL144
F_PROT(SV2D_CL144_000, sv2d_cl144_000);
F_PROT(SV2D_CL144_999, sv2d_cl144_999);
F_PROT(SV2D_CL144_COD, sv2d_cl144_cod);
F_PROT(SV2D_CL144_CLC, sv2d_cl144_clc);
F_PROT(SV2D_CL144_ASMF, sv2d_cl144_asmf);
F_PROT(SV2D_CL144_ASMKT, sv2d_cl144_asmkt);
 
// ---  class SV2D_CL203
F_PROT(SV2D_CL203_000, sv2d_cl203_000);
F_PROT(SV2D_CL203_999, sv2d_cl203_999);
F_PROT(SV2D_CL203_COD, sv2d_cl203_cod);
F_PROT(SV2D_CL203_PRC, sv2d_cl203_prc);
F_PROT(SV2D_CL203_HLP, sv2d_cl203_hlp);
 
// ---  class SV2D_CL204
F_PROT(SV2D_CL204_000, sv2d_cl204_000);
F_PROT(SV2D_CL204_999, sv2d_cl204_999);
F_PROT(SV2D_CL204_COD, sv2d_cl204_cod);
F_PROT(SV2D_CL204_PRC, sv2d_cl204_prc);
F_PROT(SV2D_CL204_HLP, sv2d_cl204_hlp);
 
// ---  class SV2D_CL213
F_PROT(SV2D_CL213_000, sv2d_cl213_000);
F_PROT(SV2D_CL213_999, sv2d_cl213_999);
F_PROT(SV2D_CL213_COD, sv2d_cl213_cod);
F_PROT(SV2D_CL213_PRC, sv2d_cl213_prc);
F_PROT(SV2D_CL213_HLP, sv2d_cl213_hlp);
 
// ---  class SV2D_CL214
F_PROT(SV2D_CL214_000, sv2d_cl214_000);
F_PROT(SV2D_CL214_999, sv2d_cl214_999);
F_PROT(SV2D_CL214_COD, sv2d_cl214_cod);
F_PROT(SV2D_CL214_PRC, sv2d_cl214_prc);
F_PROT(SV2D_CL214_HLP, sv2d_cl214_hlp);
 
// ---  class SV2D_CNB
F_PROT(SV2D_CNB_PRCDLIB, sv2d_cnb_prcdlib);
 
// ---  class SV2D_CNN
F_PROT(SV2D_CNN_PRCDLIB, sv2d_cnn_prcdlib);
 
// ---  class SV2D_LGCY
F_PROT(SV2D_LGCY_000, sv2d_lgcy_000);
F_PROT(SV2D_LGCY_999, sv2d_lgcy_999);
F_PROT(SV2D_LGCY_CTR, sv2d_lgcy_ctr);
F_PROT(SV2D_LGCY_DTR, sv2d_lgcy_dtr);
F_PROT(SV2D_LGCY_INI, sv2d_lgcy_ini);
F_PROT(SV2D_LGCY_RST, sv2d_lgcy_rst);
F_PROT(SV2D_LGCY_REQHBASE, sv2d_lgcy_reqhbase);
F_PROT(SV2D_LGCY_HVALIDE, sv2d_lgcy_hvalide);
F_PROT(SV2D_LGCY_REQHPRN, sv2d_lgcy_reqhprn);
 
// ---  class SV2D_MH
F_PROT(SV2D_MH_CBS_CLCPRNO, sv2d_mh_cbs_clcprno);
F_PROT(SV2D_MH_CBS_CLCPRNEV, sv2d_mh_cbs_clcprnev);
F_PROT(SV2D_MH_CBS_CLCPRNES, sv2d_mh_cbs_clcprnes);
F_PROT(SV2D_MH_CNB_000, sv2d_mh_cnb_000);
F_PROT(SV2D_MH_CNB_ASGCMN, sv2d_mh_cnb_asgcmn);
F_PROT(SV2D_MH_CNB_REQFNC, sv2d_mh_cnb_reqfnc);
F_PROT(SV2D_MH_CNB_REQPRM, sv2d_mh_cnb_reqprm);
F_PROT(SV2D_MH_CNN_000, sv2d_mh_cnn_000);
F_PROT(SV2D_MH_CNN_ASGCMN, sv2d_mh_cnn_asgcmn);
F_PROT(SV2D_MH_CNN_REQFNC, sv2d_mh_cnn_reqfnc);
F_PROT(SV2D_MH_CNN_REQPRM, sv2d_mh_cnn_reqprm);
 
// ---  class SV2D_PST
F_PROT(SV2D_PST_CD2D_000, sv2d_pst_cd2d_000);
F_PROT(SV2D_PST_CD2D_999, sv2d_pst_cd2d_999);
F_PROT(SV2D_PST_CD2D_CTR, sv2d_pst_cd2d_ctr);
F_PROT(SV2D_PST_CD2D_DTR, sv2d_pst_cd2d_dtr);
F_PROT(SV2D_PST_CD2D_INI, sv2d_pst_cd2d_ini);
F_PROT(SV2D_PST_CD2D_RST, sv2d_pst_cd2d_rst);
F_PROT(SV2D_PST_CD2D_REQHBASE, sv2d_pst_cd2d_reqhbase);
F_PROT(SV2D_PST_CD2D_HVALIDE, sv2d_pst_cd2d_hvalide);
F_PROT(SV2D_PST_CD2D_ACC, sv2d_pst_cd2d_acc);
F_PROT(SV2D_PST_CD2D_FIN, sv2d_pst_cd2d_fin);
F_PROT(SV2D_PST_CD2D_XEQ, sv2d_pst_cd2d_xeq);
F_PROT(SV2D_PST_CD2D_ASGHSIM, sv2d_pst_cd2d_asghsim);
F_PROT(SV2D_PST_CD2D_REQHVNO, sv2d_pst_cd2d_reqhvno);
F_PROT(SV2D_PST_CD2D_REQNOMF, sv2d_pst_cd2d_reqnomf);
F_PROT(SV2D_PST_CFL_000, sv2d_pst_cfl_000);
F_PROT(SV2D_PST_CFL_999, sv2d_pst_cfl_999);
F_PROT(SV2D_PST_CFL_CTR, sv2d_pst_cfl_ctr);
F_PROT(SV2D_PST_CFL_DTR, sv2d_pst_cfl_dtr);
F_PROT(SV2D_PST_CFL_INI, sv2d_pst_cfl_ini);
F_PROT(SV2D_PST_CFL_RST, sv2d_pst_cfl_rst);
F_PROT(SV2D_PST_CFL_REQHBASE, sv2d_pst_cfl_reqhbase);
F_PROT(SV2D_PST_CFL_HVALIDE, sv2d_pst_cfl_hvalide);
F_PROT(SV2D_PST_CFL_ACC, sv2d_pst_cfl_acc);
F_PROT(SV2D_PST_CFL_FIN, sv2d_pst_cfl_fin);
F_PROT(SV2D_PST_CFL_XEQ, sv2d_pst_cfl_xeq);
F_PROT(SV2D_PST_CFL_ASGHSIM, sv2d_pst_cfl_asghsim);
F_PROT(SV2D_PST_CFL_REQHVNO, sv2d_pst_cfl_reqhvno);
F_PROT(SV2D_PST_CFL_REQNOMF, sv2d_pst_cfl_reqnomf);
F_PROT(SV2D_PST_CHKQ_000, sv2d_pst_chkq_000);
F_PROT(SV2D_PST_CHKQ_999, sv2d_pst_chkq_999);
F_PROT(SV2D_PST_CHKQ_CTR, sv2d_pst_chkq_ctr);
F_PROT(SV2D_PST_CHKQ_DTR, sv2d_pst_chkq_dtr);
F_PROT(SV2D_PST_CHKQ_INI, sv2d_pst_chkq_ini);
F_PROT(SV2D_PST_CHKQ_RST, sv2d_pst_chkq_rst);
F_PROT(SV2D_PST_CHKQ_REQHBASE, sv2d_pst_chkq_reqhbase);
F_PROT(SV2D_PST_CHKQ_HVALIDE, sv2d_pst_chkq_hvalide);
F_PROT(SV2D_PST_CHKQ_XEQ, sv2d_pst_chkq_xeq);
F_PROT(SV2D_PST_CHKQ_REQHVNO, sv2d_pst_chkq_reqhvno);
F_PROT(SV2D_PST_CHKQ_REQNOMF, sv2d_pst_chkq_reqnomf);
F_PROT(SV2D_PST_CMP_000, sv2d_pst_cmp_000);
F_PROT(SV2D_PST_CMP_999, sv2d_pst_cmp_999);
F_PROT(SV2D_PST_CMP_CTR, sv2d_pst_cmp_ctr);
F_PROT(SV2D_PST_CMP_DTR, sv2d_pst_cmp_dtr);
F_PROT(SV2D_PST_CMP_INI, sv2d_pst_cmp_ini);
F_PROT(SV2D_PST_CMP_RST, sv2d_pst_cmp_rst);
F_PROT(SV2D_PST_CMP_REQHBASE, sv2d_pst_cmp_reqhbase);
F_PROT(SV2D_PST_CMP_HVALIDE, sv2d_pst_cmp_hvalide);
F_PROT(SV2D_PST_CMP_ACC, sv2d_pst_cmp_acc);
F_PROT(SV2D_PST_CMP_FIN, sv2d_pst_cmp_fin);
F_PROT(SV2D_PST_CMP_XEQ, sv2d_pst_cmp_xeq);
F_PROT(SV2D_PST_CMP_ASGHSIM, sv2d_pst_cmp_asghsim);
F_PROT(SV2D_PST_CMP_REQHVNO, sv2d_pst_cmp_reqhvno);
F_PROT(SV2D_PST_CMP_REQNOMF, sv2d_pst_cmp_reqnomf);
F_PROT(SV2D_PST_Q_000, sv2d_pst_q_000);
F_PROT(SV2D_PST_Q_999, sv2d_pst_q_999);
F_PROT(SV2D_PST_Q_CTR, sv2d_pst_q_ctr);
F_PROT(SV2D_PST_Q_DTR, sv2d_pst_q_dtr);
F_PROT(SV2D_PST_Q_INI, sv2d_pst_q_ini);
F_PROT(SV2D_PST_Q_RST, sv2d_pst_q_rst);
F_PROT(SV2D_PST_Q_REQHBASE, sv2d_pst_q_reqhbase);
F_PROT(SV2D_PST_Q_HVALIDE, sv2d_pst_q_hvalide);
F_PROT(SV2D_PST_Q_ACC, sv2d_pst_q_acc);
F_PROT(SV2D_PST_Q_FIN, sv2d_pst_q_fin);
F_PROT(SV2D_PST_Q_XEQ, sv2d_pst_q_xeq);
F_PROT(SV2D_PST_Q_ASGHSIM, sv2d_pst_q_asghsim);
F_PROT(SV2D_PST_Q_REQHVNO, sv2d_pst_q_reqhvno);
F_PROT(SV2D_PST_Q_REQNOMF, sv2d_pst_q_reqnomf);
F_PROT(SV2D_PST_SIM_000, sv2d_pst_sim_000);
F_PROT(SV2D_PST_SIM_999, sv2d_pst_sim_999);
F_PROT(SV2D_PST_SIM_CTR, sv2d_pst_sim_ctr);
F_PROT(SV2D_PST_SIM_DTR, sv2d_pst_sim_dtr);
F_PROT(SV2D_PST_SIM_INI, sv2d_pst_sim_ini);
F_PROT(SV2D_PST_SIM_RST, sv2d_pst_sim_rst);
F_PROT(SV2D_PST_SIM_REQHBASE, sv2d_pst_sim_reqhbase);
F_PROT(SV2D_PST_SIM_HVALIDE, sv2d_pst_sim_hvalide);
F_PROT(SV2D_PST_SIM_ACC, sv2d_pst_sim_acc);
F_PROT(SV2D_PST_SIM_FIN, sv2d_pst_sim_fin);
F_PROT(SV2D_PST_SIM_XEQ, sv2d_pst_sim_xeq);
F_PROT(SV2D_PST_SIM_ASGHSIM, sv2d_pst_sim_asghsim);
F_PROT(SV2D_PST_SIM_REQHVNO, sv2d_pst_sim_reqhvno);
F_PROT(SV2D_PST_SIM_REQNOMF, sv2d_pst_sim_reqnomf);
 
// ---  class SV2D_XBS
F_PROT(SV2D_XBS_000, sv2d_xbs_000);
F_PROT(SV2D_XBS_999, sv2d_xbs_999);
F_PROT(SV2D_XBS_CTR, sv2d_xbs_ctr);
F_PROT(SV2D_XBS_DTR, sv2d_xbs_dtr);
F_PROT(SV2D_XBS_INI, sv2d_xbs_ini);
F_PROT(SV2D_XBS_RST, sv2d_xbs_rst);
F_PROT(SV2D_XBS_REQHBASE, sv2d_xbs_reqhbase);
F_PROT(SV2D_XBS_HVALIDE, sv2d_xbs_hvalide);
F_PROT(SV2D_XBS_HLPPRNO, sv2d_xbs_hlpprno);
F_PROT(SV2D_XBS_PRCPRNO, sv2d_xbs_prcprno);
F_PROT(SV2D_XBS_PRNPRNO, sv2d_xbs_prnprno);
F_PROT(SV2D_XBS_PSCPRNO, sv2d_xbs_pscprno);
F_PROT(SV2D_XBS_PSLPRNO, sv2d_xbs_pslprno);
F_PROT(SV2D_XBS_HLPPRGL, sv2d_xbs_hlpprgl);
F_PROT(SV2D_XBS_PRCPRGL, sv2d_xbs_prcprgl);
F_PROT(SV2D_XBS_PRNPRGL, sv2d_xbs_prnprgl);
F_PROT(SV2D_XBS_PSLPRGL, sv2d_xbs_pslprgl);
F_PROT(SV2D_XBS_CLCPREV, sv2d_xbs_clcprev);
F_PROT(SV2D_XBS_CLCPREVE, sv2d_xbs_clcpreve);
F_PROT(SV2D_XBS_CLCPREVE_Z, sv2d_xbs_clcpreve_z);
F_PROT(SV2D_XBS_PRCPREV, sv2d_xbs_prcprev);
F_PROT(SV2D_XBS_PSLPREV, sv2d_xbs_pslprev);
F_PROT(SV2D_XBS_CLCPRES, sv2d_xbs_clcpres);
F_PROT(SV2D_XBS_CLCPRESE, sv2d_xbs_clcprese);
F_PROT(SV2D_XBS_PRCPRES, sv2d_xbs_prcpres);
F_PROT(SV2D_XBS_PRCSOLC, sv2d_xbs_prcsolc);
F_PROT(SV2D_XBS_PSLSOLC, sv2d_xbs_pslsolc);
F_PROT(SV2D_XBS_PRCSOLR, sv2d_xbs_prcsolr);
F_PROT(SV2D_XBS_PSLSOLR, sv2d_xbs_pslsolr);
F_PROT(SV2D_XBS_CLCCLIM, sv2d_xbs_clcclim);
F_PROT(SV2D_XBS_HLPCLIM, sv2d_xbs_hlpclim);
F_PROT(SV2D_XBS_PRNCLIM, sv2d_xbs_prnclim);
F_PROT(SV2D_XBS_PSLCLIM, sv2d_xbs_pslclim);
F_PROT(SV2D_XBS_CLCDLIB, sv2d_xbs_clcdlib);
F_PROT(SV2D_XBS_PRCDLIB, sv2d_xbs_prcdlib);
F_PROT(SV2D_XBS_PSLDLIB, sv2d_xbs_psldlib);
F_PROT(SV2D_XBS_ASMF_R, sv2d_xbs_asmf_r);
F_PROT(SV2D_XBS_ASMKT_R, sv2d_xbs_asmkt_r);
F_PROT(SV2D_XBS_ASMKT_R_Z, sv2d_xbs_asmkt_r_z);
F_PROT(SV2D_XBS_ASMKU_R, sv2d_xbs_asmku_r);
F_PROT(SV2D_XBS_ASMK_R, sv2d_xbs_asmk_r);
F_PROT(SV2D_XBS_ASMM, sv2d_xbs_asmm);
F_PROT(SV2D_XBS_ASMMU, sv2d_xbs_asmmu);
 
// ---  class SV2D_XCL001
F_PROT(SV2D_XCL001_000, sv2d_xcl001_000);
F_PROT(SV2D_XCL001_999, sv2d_xcl001_999);
 
// ---  class SV2D_XCL002
F_PROT(SV2D_XCL002_000, sv2d_xcl002_000);
F_PROT(SV2D_XCL002_999, sv2d_xcl002_999);
 
// ---  class SV2D_XCL003
F_PROT(SV2D_XCL003_000, sv2d_xcl003_000);
F_PROT(SV2D_XCL003_999, sv2d_xcl003_999);
 
// ---  class SV2D_XCL004
F_PROT(SV2D_XCL004_000, sv2d_xcl004_000);
F_PROT(SV2D_XCL004_999, sv2d_xcl004_999);
 
// ---  class SV2D_XCL010
F_PROT(SV2D_XCL010_000, sv2d_xcl010_000);
F_PROT(SV2D_XCL010_999, sv2d_xcl010_999);
 
// ---  class SV2D_XCL011
F_PROT(SV2D_XCL011_000, sv2d_xcl011_000);
F_PROT(SV2D_XCL011_999, sv2d_xcl011_999);
 
// ---  class SV2D_XCL012
F_PROT(SV2D_XCL012_000, sv2d_xcl012_000);
F_PROT(SV2D_XCL012_999, sv2d_xcl012_999);
 
// ---  class SV2D_Y2
F_PROT(SV2D_Y2_CBS_CLCPRNO, sv2d_y2_cbs_clcprno);
F_PROT(SV2D_Y2_CBS_CLCPRNEV, sv2d_y2_cbs_clcprnev);
F_PROT(SV2D_Y2_CBS_CLCPRNES, sv2d_y2_cbs_clcprnes);
F_PROT(SV2D_Y2_CBS_REQPRM, sv2d_y2_cbs_reqprm);
F_PROT(SV2D_Y2_CNN_000, sv2d_y2_cnn_000);
F_PROT(SV2D_Y2_CNN_REQHBASE, sv2d_y2_cnn_reqhbase);
F_PROT(SV2D_Y2_CNN_ASGCMN, sv2d_y2_cnn_asgcmn);
F_PROT(SV2D_Y2_CNN_REQFNC, sv2d_y2_cnn_reqfnc);
F_PROT(SV2D_Y2_CNN_REQPRM, sv2d_y2_cnn_reqprm);
 
// ---  class SV2D_Y3
F_PROT(SV2D_Y3_CBS_PRCPRGL, sv2d_y3_cbs_prcprgl);
F_PROT(SV2D_Y3_CBS_PSLPRGL, sv2d_y3_cbs_pslprgl);
F_PROT(SV2D_Y3_CBS_REQPRM, sv2d_y3_cbs_reqprm);
F_PROT(SV2D_Y3_CNN_000, sv2d_y3_cnn_000);
F_PROT(SV2D_Y3_CNN_REQHBASE, sv2d_y3_cnn_reqhbase);
F_PROT(SV2D_Y3_CNN_ASGCMN, sv2d_y3_cnn_asgcmn);
F_PROT(SV2D_Y3_CNN_REQFNC, sv2d_y3_cnn_reqfnc);
F_PROT(SV2D_Y3_CNN_REQPRM, sv2d_y3_cnn_reqprm);
 
// ---  class SV2D_Y4
F_PROT(SV2D_Y4_CBS_CLCPREV, sv2d_y4_cbs_clcprev);
F_PROT(SV2D_Y4_CBS_CLCPREVE, sv2d_y4_cbs_clcpreve);
F_PROT(SV2D_Y4_CBS_CLCPRNO, sv2d_y4_cbs_clcprno);
F_PROT(SV2D_Y4_CBS_CLCPRNEV, sv2d_y4_cbs_clcprnev);
F_PROT(SV2D_Y4_CBS_CLCPRNES, sv2d_y4_cbs_clcprnes);
F_PROT(SV2D_Y4_CBS_CLCPRN1N, sv2d_y4_cbs_clcprn1n);
F_PROT(SV2D_Y4_CBS_PRCPRGL, sv2d_y4_cbs_prcprgl);
F_PROT(SV2D_Y4_CBS_PSLPRGL, sv2d_y4_cbs_pslprgl);
F_PROT(SV2D_Y4_CBS_PSLPRNO, sv2d_y4_cbs_pslprno);
F_PROT(SV2D_Y4_CBS_REQPRM, sv2d_y4_cbs_reqprm);
F_PROT(SV2D_Y4_CNN_000, sv2d_y4_cnn_000);
F_PROT(SV2D_Y4_CNN_REQHBASE, sv2d_y4_cnn_reqhbase);
F_PROT(SV2D_Y4_CNN_ASGCMN, sv2d_y4_cnn_asgcmn);
F_PROT(SV2D_Y4_CNN_REQFNC, sv2d_y4_cnn_reqfnc);
F_PROT(SV2D_Y4_CNN_REQPRM, sv2d_y4_cnn_reqprm);
 
// ---  class SV2D_YS
F_PROT(SV2D_YS_CBS_CLCPRNO, sv2d_ys_cbs_clcprno);
F_PROT(SV2D_YS_CBS_CLCPRNEV, sv2d_ys_cbs_clcprnev);
F_PROT(SV2D_YS_CBS_CLCPRNES, sv2d_ys_cbs_clcprnes);
F_PROT(SV2D_YS_CNN_000, sv2d_ys_cnn_000);
F_PROT(SV2D_YS_CNN_ASGCMN, sv2d_ys_cnn_asgcmn);
F_PROT(SV2D_YS_CNN_REQFNC, sv2d_ys_cnn_reqfnc);
F_PROT(SV2D_YS_CNN_REQPRM, sv2d_ys_cnn_reqprm);
F_PROT(SV2D_YS_CNB_000, sv2d_ys_cnb_000);
F_PROT(SV2D_YS_CNB_ASGCMN, sv2d_ys_cnb_asgcmn);
F_PROT(SV2D_YS_CNB_REQFNC, sv2d_ys_cnb_reqfnc);
F_PROT(SV2D_YS_CNB_REQPRM, sv2d_ys_cnb_reqprm);
 
// ---  class SV2D_Z4
F_PROT(SV2D_Z4_CNN_000, sv2d_z4_cnn_000);
F_PROT(SV2D_Z4_CNN_REQHBASE, sv2d_z4_cnn_reqhbase);
F_PROT(SV2D_Z4_CNN_ASGCMN, sv2d_z4_cnn_asgcmn);
F_PROT(SV2D_Z4_CNN_REQFNC, sv2d_z4_cnn_reqfnc);
F_PROT(SV2D_Z4_CBS_CLCPRNEV, sv2d_z4_cbs_clcprnev);
F_PROT(SV2D_Z4_CBS_CLCPRNES, sv2d_z4_cbs_clcprnes);
F_PROT(SV2D_Z4_CBS_CLCPRN1N, sv2d_z4_cbs_clcprn1n);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_sv2d()
{
   static char libname[] = "sv2d";
 
   // ---  class IC_SV2D
   F_RGST(IC_SV2D_LGCY_XEQCTR, ic_sv2d_lgcy_xeqctr, libname);
   F_RGST(IC_SV2D_LGCY_XEQMTH, ic_sv2d_lgcy_xeqmth, libname);
   F_RGST(IC_SV2D_LGCY_OPBDOT, ic_sv2d_lgcy_opbdot, libname);
   F_RGST(IC_SV2D_LGCY_REQCLS, ic_sv2d_lgcy_reqcls, libname);
   F_RGST(IC_SV2D_LGCY_REQHDL, ic_sv2d_lgcy_reqhdl, libname);
   F_RGST(IC_SV2D_Y2_CNN_000, ic_sv2d_y2_cnn_000, libname);
   F_RGST(IC_SV2D_Y2_CNN_XEQCTR, ic_sv2d_y2_cnn_xeqctr, libname);
   F_RGST(IC_SV2D_Y2_CNN_XEQMTH, ic_sv2d_y2_cnn_xeqmth, libname);
   F_RGST(IC_SV2D_Y2_CNN_OPBDOT, ic_sv2d_y2_cnn_opbdot, libname);
   F_RGST(IC_SV2D_Y2_CNN_REQCLS, ic_sv2d_y2_cnn_reqcls, libname);
   F_RGST(IC_SV2D_Y2_CNN_REQHDL, ic_sv2d_y2_cnn_reqhdl, libname);
   F_RGST(IC_SV2D_Y3_CNN_000, ic_sv2d_y3_cnn_000, libname);
   F_RGST(IC_SV2D_Y3_CNN_XEQCTR, ic_sv2d_y3_cnn_xeqctr, libname);
   F_RGST(IC_SV2D_Y3_CNN_XEQMTH, ic_sv2d_y3_cnn_xeqmth, libname);
   F_RGST(IC_SV2D_Y3_CNN_OPBDOT, ic_sv2d_y3_cnn_opbdot, libname);
   F_RGST(IC_SV2D_Y3_CNN_REQCLS, ic_sv2d_y3_cnn_reqcls, libname);
   F_RGST(IC_SV2D_Y3_CNN_REQHDL, ic_sv2d_y3_cnn_reqhdl, libname);
   F_RGST(IC_SV2D_Y4_CNN_000, ic_sv2d_y4_cnn_000, libname);
   F_RGST(IC_SV2D_Y4_CNN_XEQCTR, ic_sv2d_y4_cnn_xeqctr, libname);
   F_RGST(IC_SV2D_Y4_CNN_XEQMTH, ic_sv2d_y4_cnn_xeqmth, libname);
   F_RGST(IC_SV2D_Y4_CNN_OPBDOT, ic_sv2d_y4_cnn_opbdot, libname);
   F_RGST(IC_SV2D_Y4_CNN_REQCLS, ic_sv2d_y4_cnn_reqcls, libname);
   F_RGST(IC_SV2D_Y4_CNN_REQHDL, ic_sv2d_y4_cnn_reqhdl, libname);
   F_RGST(IC_SV2D_Z4_CNN_000, ic_sv2d_z4_cnn_000, libname);
   F_RGST(IC_SV2D_Z4_CNN_XEQCTR, ic_sv2d_z4_cnn_xeqctr, libname);
   F_RGST(IC_SV2D_Z4_CNN_XEQMTH, ic_sv2d_z4_cnn_xeqmth, libname);
   F_RGST(IC_SV2D_Z4_CNN_OPBDOT, ic_sv2d_z4_cnn_opbdot, libname);
   F_RGST(IC_SV2D_Z4_CNN_REQCLS, ic_sv2d_z4_cnn_reqcls, libname);
   F_RGST(IC_SV2D_Z4_CNN_REQHDL, ic_sv2d_z4_cnn_reqhdl, libname);
   F_RGST(IC_SV2D_PST_CD2D_XEQCTR, ic_sv2d_pst_cd2d_xeqctr, libname);
   F_RGST(IC_SV2D_PST_CD2D_XEQMTH, ic_sv2d_pst_cd2d_xeqmth, libname);
   F_RGST(IC_SV2D_PST_CD2D_REQCLS, ic_sv2d_pst_cd2d_reqcls, libname);
   F_RGST(IC_SV2D_PST_CD2D_REQHDL, ic_sv2d_pst_cd2d_reqhdl, libname);
   F_RGST(IC_SV2D_PST_CFL_XEQCTR, ic_sv2d_pst_cfl_xeqctr, libname);
   F_RGST(IC_SV2D_PST_CFL_XEQMTH, ic_sv2d_pst_cfl_xeqmth, libname);
   F_RGST(IC_SV2D_PST_CFL_REQCLS, ic_sv2d_pst_cfl_reqcls, libname);
   F_RGST(IC_SV2D_PST_CFL_REQHDL, ic_sv2d_pst_cfl_reqhdl, libname);
   F_RGST(IC_SV2D_PST_CHKQ_CMD, ic_sv2d_pst_chkq_cmd, libname);
   F_RGST(IC_SV2D_PST_CHKQ_REQCMD, ic_sv2d_pst_chkq_reqcmd, libname);
   F_RGST(IC_SV2D_PST_CMP_XEQCTR, ic_sv2d_pst_cmp_xeqctr, libname);
   F_RGST(IC_SV2D_PST_CMP_XEQMTH, ic_sv2d_pst_cmp_xeqmth, libname);
   F_RGST(IC_SV2D_PST_CMP_REQCLS, ic_sv2d_pst_cmp_reqcls, libname);
   F_RGST(IC_SV2D_PST_CMP_REQHDL, ic_sv2d_pst_cmp_reqhdl, libname);
   F_RGST(IC_SV2D_PST_Q_CMD, ic_sv2d_pst_q_cmd, libname);
   F_RGST(IC_SV2D_PST_Q_REQCMD, ic_sv2d_pst_q_reqcmd, libname);
   F_RGST(IC_SV2D_PST_SIM_XEQCTR, ic_sv2d_pst_sim_xeqctr, libname);
   F_RGST(IC_SV2D_PST_SIM_XEQMTH, ic_sv2d_pst_sim_xeqmth, libname);
   F_RGST(IC_SV2D_PST_SIM_REQCLS, ic_sv2d_pst_sim_reqcls, libname);
   F_RGST(IC_SV2D_PST_SIM_REQHDL, ic_sv2d_pst_sim_reqhdl, libname);
 
   // ---  class SV2D_CBS
   F_RGST(SV2D_CBS_ASGCMN, sv2d_cbs_asgcmn, libname);
   F_RGST(SV2D_CBS_DMPPROP, sv2d_cbs_dmpprop, libname);
   F_RGST(SV2D_CBS_REQFNC, sv2d_cbs_reqfnc, libname);
   F_RGST(SV2D_CBS_REQPRM, sv2d_cbs_reqprm, libname);
   F_RGST(SV2D_CBS_ASMM, sv2d_cbs_asmm, libname);
   F_RGST(SV2D_CBS_ASMMU, sv2d_cbs_asmmu, libname);
   F_RGST(SV2D_CBS_ASMF_K, sv2d_cbs_asmf_k, libname);
   F_RGST(SV2D_CBS_ASMKT_K, sv2d_cbs_asmkt_k, libname);
   F_RGST(SV2D_CBS_ASMKU_K, sv2d_cbs_asmku_k, libname);
   F_RGST(SV2D_CBS_ASMK_K, sv2d_cbs_asmk_k, libname);
   F_RGST(SV2D_CBS_ASMF_R, sv2d_cbs_asmf_r, libname);
   F_RGST(SV2D_CBS_ASMKT_R, sv2d_cbs_asmkt_r, libname);
   F_RGST(SV2D_CBS_ASMKT_R_Z, sv2d_cbs_asmkt_r_z, libname);
   F_RGST(SV2D_CBS_ASMKU_R, sv2d_cbs_asmku_r, libname);
   F_RGST(SV2D_CBS_ASMK_R, sv2d_cbs_asmk_r, libname);
   F_RGST(SV2D_CBS_CLCCLIM, sv2d_cbs_clcclim, libname);
   F_RGST(SV2D_CBS_HLPCLIM, sv2d_cbs_hlpclim, libname);
   F_RGST(SV2D_CBS_PRCCLIM, sv2d_cbs_prcclim, libname);
   F_RGST(SV2D_CBS_PRNCLIM, sv2d_cbs_prnclim, libname);
   F_RGST(SV2D_CBS_PSLCLIM, sv2d_cbs_pslclim, libname);
   F_RGST(SV2D_CBS_CLCDLIB, sv2d_cbs_clcdlib, libname);
   F_RGST(SV2D_CBS_PSLDLIB, sv2d_cbs_psldlib, libname);
   F_RGST(SV2D_CBS_CLCPRES, sv2d_cbs_clcpres, libname);
   F_RGST(SV2D_CBS_CLCPRESE, sv2d_cbs_clcprese, libname);
   F_RGST(SV2D_CBS_CLCPRESE_Z, sv2d_cbs_clcprese_z, libname);
   F_RGST(SV2D_CBS_PRCPRES, sv2d_cbs_prcpres, libname);
   F_RGST(SV2D_CBS_CLCPREV, sv2d_cbs_clcprev, libname);
   F_RGST(SV2D_CBS_CLCPREVE, sv2d_cbs_clcpreve, libname);
   F_RGST(SV2D_CBS_CLCPREVE_Z, sv2d_cbs_clcpreve_z, libname);
   F_RGST(SV2D_CBS_PRCPREV, sv2d_cbs_prcprev, libname);
   F_RGST(SV2D_CBS_PSLPREV, sv2d_cbs_pslprev, libname);
   F_RGST(SV2D_CBS_HLPPRGL, sv2d_cbs_hlpprgl, libname);
   F_RGST(SV2D_CBS_PRCPRGL, sv2d_cbs_prcprgl, libname);
   F_RGST(SV2D_CBS_PRNPRGL, sv2d_cbs_prnprgl, libname);
   F_RGST(SV2D_CBS_PSLPRGL, sv2d_cbs_pslprgl, libname);
   F_RGST(SV2D_CBS_HLPPRNO, sv2d_cbs_hlpprno, libname);
   F_RGST(SV2D_CBS_PRCPRNO, sv2d_cbs_prcprno, libname);
   F_RGST(SV2D_CBS_PRNPRNO, sv2d_cbs_prnprno, libname);
   F_RGST(SV2D_CBS_PSCPRNO, sv2d_cbs_pscprno, libname);
   F_RGST(SV2D_CBS_PSLPRNO, sv2d_cbs_pslprno, libname);
   F_RGST(SV2D_CBS_PRCSOLC, sv2d_cbs_prcsolc, libname);
   F_RGST(SV2D_CBS_PSLSOLC, sv2d_cbs_pslsolc, libname);
   F_RGST(SV2D_CBS_PRCSOLR, sv2d_cbs_prcsolr, libname);
   F_RGST(SV2D_CBS_PSLSOLR, sv2d_cbs_pslsolr, libname);
 
   // ---  class SV2D_CL
   F_RGST(SV2D_CL_INIVTBL2, sv2d_cl_inivtbl2, libname);
   F_RGST(SV2D_CL_INIVTBL, sv2d_cl_inivtbl, libname);
   F_RGST(SV2D_CL_HLP, sv2d_cl_hlp, libname);
   F_RGST(SV2D_CL_COD, sv2d_cl_cod, libname);
   F_RGST(SV2D_CL_PRC, sv2d_cl_prc, libname);
   F_RGST(SV2D_CL_CLC, sv2d_cl_clc, libname);
   F_RGST(SV2D_CL_ASMF, sv2d_cl_asmf, libname);
   F_RGST(SV2D_CL_ASMKU, sv2d_cl_asmku, libname);
   F_RGST(SV2D_CL_ASMK, sv2d_cl_asmk, libname);
   F_RGST(SV2D_CL_ASMKT, sv2d_cl_asmkt, libname);
   F_RGST(SV2D_CL_ASMM, sv2d_cl_asmm, libname);
   F_RGST(SV2D_CL_ASMMU, sv2d_cl_asmmu, libname);
 
   // ---  class SV2D_CL001
   F_RGST(SV2D_CL001_000, sv2d_cl001_000, libname);
   F_RGST(SV2D_CL001_999, sv2d_cl001_999, libname);
   F_RGST(SV2D_CL001_COD, sv2d_cl001_cod, libname);
   F_RGST(SV2D_CL001_PRC, sv2d_cl001_prc, libname);
   F_RGST(SV2D_CL001_HLP, sv2d_cl001_hlp, libname);
 
   // ---  class SV2D_CL002
   F_RGST(SV2D_CL002_000, sv2d_cl002_000, libname);
   F_RGST(SV2D_CL002_999, sv2d_cl002_999, libname);
   F_RGST(SV2D_CL002_COD, sv2d_cl002_cod, libname);
   F_RGST(SV2D_CL002_PRC, sv2d_cl002_prc, libname);
   F_RGST(SV2D_CL002_HLP, sv2d_cl002_hlp, libname);
 
   // ---  class SV2D_CL003
   F_RGST(SV2D_CL003_000, sv2d_cl003_000, libname);
   F_RGST(SV2D_CL003_999, sv2d_cl003_999, libname);
   F_RGST(SV2D_CL003_COD, sv2d_cl003_cod, libname);
   F_RGST(SV2D_CL003_PRC, sv2d_cl003_prc, libname);
   F_RGST(SV2D_CL003_HLP, sv2d_cl003_hlp, libname);
 
   // ---  class SV2D_CL004
   F_RGST(SV2D_CL004_000, sv2d_cl004_000, libname);
   F_RGST(SV2D_CL004_999, sv2d_cl004_999, libname);
   F_RGST(SV2D_CL004_COD, sv2d_cl004_cod, libname);
   F_RGST(SV2D_CL004_PRC, sv2d_cl004_prc, libname);
   F_RGST(SV2D_CL004_HLP, sv2d_cl004_hlp, libname);
 
   // ---  class SV2D_CL010
   F_RGST(SV2D_CL010_000, sv2d_cl010_000, libname);
   F_RGST(SV2D_CL010_999, sv2d_cl010_999, libname);
   F_RGST(SV2D_CL010_COD, sv2d_cl010_cod, libname);
   F_RGST(SV2D_CL010_ASMF, sv2d_cl010_asmf, libname);
   F_RGST(SV2D_CL010_HLP, sv2d_cl010_hlp, libname);
 
   // ---  class SV2D_CL011
   F_RGST(SV2D_CL011_000, sv2d_cl011_000, libname);
   F_RGST(SV2D_CL011_999, sv2d_cl011_999, libname);
   F_RGST(SV2D_CL011_COD, sv2d_cl011_cod, libname);
   F_RGST(SV2D_CL011_ASMF, sv2d_cl011_asmf, libname);
   F_RGST(SV2D_CL011_HLP, sv2d_cl011_hlp, libname);
 
   // ---  class SV2D_CL012
   F_RGST(SV2D_CL012_000, sv2d_cl012_000, libname);
   F_RGST(SV2D_CL012_999, sv2d_cl012_999, libname);
   F_RGST(SV2D_CL012_COD, sv2d_cl012_cod, libname);
   F_RGST(SV2D_CL012_ASMF, sv2d_cl012_asmf, libname);
   F_RGST(SV2D_CL012_HLP, sv2d_cl012_hlp, libname);
 
   // ---  class SV2D_CL100
   F_RGST(SV2D_CL100_000, sv2d_cl100_000, libname);
   F_RGST(SV2D_CL100_999, sv2d_cl100_999, libname);
   F_RGST(SV2D_CL100_COD, sv2d_cl100_cod, libname);
   F_RGST(SV2D_CL100_HLP, sv2d_cl100_hlp, libname);
 
   // ---  class SV2D_CL101
   F_RGST(SV2D_CL101_000, sv2d_cl101_000, libname);
   F_RGST(SV2D_CL101_999, sv2d_cl101_999, libname);
   F_RGST(SV2D_CL101_COD, sv2d_cl101_cod, libname);
   F_RGST(SV2D_CL101_PRC, sv2d_cl101_prc, libname);
   F_RGST(SV2D_CL101_HLP, sv2d_cl101_hlp, libname);
 
   // ---  class SV2D_CL102
   F_RGST(SV2D_CL102_000, sv2d_cl102_000, libname);
   F_RGST(SV2D_CL102_999, sv2d_cl102_999, libname);
   F_RGST(SV2D_CL102_COD, sv2d_cl102_cod, libname);
   F_RGST(SV2D_CL102_PRC, sv2d_cl102_prc, libname);
   F_RGST(SV2D_CL102_HLP, sv2d_cl102_hlp, libname);
 
   // ---  class SV2D_CL103
   F_RGST(SV2D_CL103_000, sv2d_cl103_000, libname);
   F_RGST(SV2D_CL103_999, sv2d_cl103_999, libname);
   F_RGST(SV2D_CL103_COD, sv2d_cl103_cod, libname);
   F_RGST(SV2D_CL103_PRC, sv2d_cl103_prc, libname);
   F_RGST(SV2D_CL103_HLP, sv2d_cl103_hlp, libname);
 
   // ---  class SV2D_CL104
   F_RGST(SV2D_CL104_000, sv2d_cl104_000, libname);
   F_RGST(SV2D_CL104_999, sv2d_cl104_999, libname);
   F_RGST(SV2D_CL104_COD, sv2d_cl104_cod, libname);
   F_RGST(SV2D_CL104_PRC, sv2d_cl104_prc, libname);
   F_RGST(SV2D_CL104_HLP, sv2d_cl104_hlp, libname);
 
   // ---  class SV2D_CL111
   F_RGST(SV2D_CL111_000, sv2d_cl111_000, libname);
   F_RGST(SV2D_CL111_999, sv2d_cl111_999, libname);
   F_RGST(SV2D_CL111_COD, sv2d_cl111_cod, libname);
   F_RGST(SV2D_CL111_PRC, sv2d_cl111_prc, libname);
   F_RGST(SV2D_CL111_ASMF, sv2d_cl111_asmf, libname);
   F_RGST(SV2D_CL111_ASMKU, sv2d_cl111_asmku, libname);
   F_RGST(SV2D_CL111_ASMK, sv2d_cl111_asmk, libname);
   F_RGST(SV2D_CL111_HLP, sv2d_cl111_hlp, libname);
 
   // ---  class SV2D_CL112
   F_RGST(SV2D_CL112_000, sv2d_cl112_000, libname);
   F_RGST(SV2D_CL112_999, sv2d_cl112_999, libname);
   F_RGST(SV2D_CL112_COD, sv2d_cl112_cod, libname);
   F_RGST(SV2D_CL112_PRC, sv2d_cl112_prc, libname);
   F_RGST(SV2D_CL112_ASMF, sv2d_cl112_asmf, libname);
   F_RGST(SV2D_CL112_ASMKU, sv2d_cl112_asmku, libname);
   F_RGST(SV2D_CL112_ASMK, sv2d_cl112_asmk, libname);
   F_RGST(SV2D_CL112_HLP, sv2d_cl112_hlp, libname);
 
   // ---  class SV2D_CL113
   F_RGST(SV2D_CL113_000, sv2d_cl113_000, libname);
   F_RGST(SV2D_CL113_999, sv2d_cl113_999, libname);
   F_RGST(SV2D_CL113_COD, sv2d_cl113_cod, libname);
   F_RGST(SV2D_CL113_PRC, sv2d_cl113_prc, libname);
   F_RGST(SV2D_CL113_ASMF, sv2d_cl113_asmf, libname);
   F_RGST(SV2D_CL113_ASMKU, sv2d_cl113_asmku, libname);
   F_RGST(SV2D_CL113_ASMK, sv2d_cl113_asmk, libname);
   F_RGST(SV2D_CL113_HLP, sv2d_cl113_hlp, libname);
 
   // ---  class SV2D_CL121
   F_RGST(SV2D_CL121_000, sv2d_cl121_000, libname);
   F_RGST(SV2D_CL121_999, sv2d_cl121_999, libname);
   F_RGST(SV2D_CL121_COD, sv2d_cl121_cod, libname);
   F_RGST(SV2D_CL121_ASMF, sv2d_cl121_asmf, libname);
   F_RGST(SV2D_CL121_HLP, sv2d_cl121_hlp, libname);
 
   // ---  class SV2D_CL122
   F_RGST(SV2D_CL122_000, sv2d_cl122_000, libname);
   F_RGST(SV2D_CL122_999, sv2d_cl122_999, libname);
   F_RGST(SV2D_CL122_COD, sv2d_cl122_cod, libname);
   F_RGST(SV2D_CL122_PRC, sv2d_cl122_prc, libname);
   F_RGST(SV2D_CL122_ASMF, sv2d_cl122_asmf, libname);
   F_RGST(SV2D_CL122_HLP, sv2d_cl122_hlp, libname);
 
   // ---  class SV2D_CL123
   F_RGST(SV2D_CL123_000, sv2d_cl123_000, libname);
   F_RGST(SV2D_CL123_999, sv2d_cl123_999, libname);
   F_RGST(SV2D_CL123_COD, sv2d_cl123_cod, libname);
   F_RGST(SV2D_CL123_ASMF, sv2d_cl123_asmf, libname);
   F_RGST(SV2D_CL123_HLP, sv2d_cl123_hlp, libname);
 
   // ---  class SV2D_CL124
   F_RGST(SV2D_CL124_000, sv2d_cl124_000, libname);
   F_RGST(SV2D_CL124_999, sv2d_cl124_999, libname);
   F_RGST(SV2D_CL124_COD, sv2d_cl124_cod, libname);
   F_RGST(SV2D_CL124_ASMF, sv2d_cl124_asmf, libname);
   F_RGST(SV2D_CL124_HLP, sv2d_cl124_hlp, libname);
 
   // ---  class SV2D_CL125
   F_RGST(SV2D_CL125_000, sv2d_cl125_000, libname);
   F_RGST(SV2D_CL125_999, sv2d_cl125_999, libname);
   F_RGST(SV2D_CL125_COD, sv2d_cl125_cod, libname);
   F_RGST(SV2D_CL125_CLC, sv2d_cl125_clc, libname);
   F_RGST(SV2D_CL125_ASMF, sv2d_cl125_asmf, libname);
   F_RGST(SV2D_CL125_ASMKT, sv2d_cl125_asmkt, libname);
   F_RGST(SV2D_CL125_HLP, sv2d_cl125_hlp, libname);
 
   // ---  class SV2D_CL126
   F_RGST(SV2D_CL126_000, sv2d_cl126_000, libname);
   F_RGST(SV2D_CL126_999, sv2d_cl126_999, libname);
   F_RGST(SV2D_CL126_COD, sv2d_cl126_cod, libname);
   F_RGST(SV2D_CL126_CLC, sv2d_cl126_clc, libname);
   F_RGST(SV2D_CL126_ASMF, sv2d_cl126_asmf, libname);
   F_RGST(SV2D_CL126_ASMKT, sv2d_cl126_asmkt, libname);
   F_RGST(SV2D_CL126_HLP, sv2d_cl126_hlp, libname);
 
   // ---  class SV2D_CL133
   F_RGST(SV2D_CL133_000, sv2d_cl133_000, libname);
   F_RGST(SV2D_CL133_999, sv2d_cl133_999, libname);
   F_RGST(SV2D_CL133_COD, sv2d_cl133_cod, libname);
   F_RGST(SV2D_CL133_CLC, sv2d_cl133_clc, libname);
   F_RGST(SV2D_CL133_ASMF, sv2d_cl133_asmf, libname);
   F_RGST(SV2D_CL133_ASMKU, sv2d_cl133_asmku, libname);
   F_RGST(SV2D_CL133_ASMK, sv2d_cl133_asmk, libname);
   F_RGST(SV2D_CL133_ASMKT, sv2d_cl133_asmkt, libname);
   F_RGST(SV2D_CL133_HLP, sv2d_cl133_hlp, libname);
 
   // ---  class SV2D_CL138
   F_RGST(SV2D_CL138_000, sv2d_cl138_000, libname);
   F_RGST(SV2D_CL138_999, sv2d_cl138_999, libname);
   F_RGST(SV2D_CL138_COD, sv2d_cl138_cod, libname);
   F_RGST(SV2D_CL138_ASMF, sv2d_cl138_asmf, libname);
   F_RGST(SV2D_CL138_HLP, sv2d_cl138_hlp, libname);
 
   // ---  class SV2D_CL141
   F_RGST(SV2D_CL141_000, sv2d_cl141_000, libname);
   F_RGST(SV2D_CL141_999, sv2d_cl141_999, libname);
   F_RGST(SV2D_CL141_COD, sv2d_cl141_cod, libname);
   F_RGST(SV2D_CL141_ASMK, sv2d_cl141_asmk, libname);
   F_RGST(SV2D_CL141_ASMKU, sv2d_cl141_asmku, libname);
   F_RGST(SV2D_CL141_HLP, sv2d_cl141_hlp, libname);
 
   // ---  class SV2D_CL142
   F_RGST(SV2D_CL142_000, sv2d_cl142_000, libname);
   F_RGST(SV2D_CL142_999, sv2d_cl142_999, libname);
   F_RGST(SV2D_CL142_COD, sv2d_cl142_cod, libname);
   F_RGST(SV2D_CL142_ASMF, sv2d_cl142_asmf, libname);
 
   // ---  class SV2D_CL143
   F_RGST(SV2D_CL143_000, sv2d_cl143_000, libname);
   F_RGST(SV2D_CL143_999, sv2d_cl143_999, libname);
   F_RGST(SV2D_CL143_COD, sv2d_cl143_cod, libname);
   F_RGST(SV2D_CL143_CLC, sv2d_cl143_clc, libname);
   F_RGST(SV2D_CL143_ASMF, sv2d_cl143_asmf, libname);
   F_RGST(SV2D_CL143_ASMKU, sv2d_cl143_asmku, libname);
   F_RGST(SV2D_CL143_ASMK, sv2d_cl143_asmk, libname);
   F_RGST(SV2D_CL143_ASMKT, sv2d_cl143_asmkt, libname);
 
   // ---  class SV2D_CL144
   F_RGST(SV2D_CL144_000, sv2d_cl144_000, libname);
   F_RGST(SV2D_CL144_999, sv2d_cl144_999, libname);
   F_RGST(SV2D_CL144_COD, sv2d_cl144_cod, libname);
   F_RGST(SV2D_CL144_CLC, sv2d_cl144_clc, libname);
   F_RGST(SV2D_CL144_ASMF, sv2d_cl144_asmf, libname);
   F_RGST(SV2D_CL144_ASMKT, sv2d_cl144_asmkt, libname);
 
   // ---  class SV2D_CL203
   F_RGST(SV2D_CL203_000, sv2d_cl203_000, libname);
   F_RGST(SV2D_CL203_999, sv2d_cl203_999, libname);
   F_RGST(SV2D_CL203_COD, sv2d_cl203_cod, libname);
   F_RGST(SV2D_CL203_PRC, sv2d_cl203_prc, libname);
   F_RGST(SV2D_CL203_HLP, sv2d_cl203_hlp, libname);
 
   // ---  class SV2D_CL204
   F_RGST(SV2D_CL204_000, sv2d_cl204_000, libname);
   F_RGST(SV2D_CL204_999, sv2d_cl204_999, libname);
   F_RGST(SV2D_CL204_COD, sv2d_cl204_cod, libname);
   F_RGST(SV2D_CL204_PRC, sv2d_cl204_prc, libname);
   F_RGST(SV2D_CL204_HLP, sv2d_cl204_hlp, libname);
 
   // ---  class SV2D_CL213
   F_RGST(SV2D_CL213_000, sv2d_cl213_000, libname);
   F_RGST(SV2D_CL213_999, sv2d_cl213_999, libname);
   F_RGST(SV2D_CL213_COD, sv2d_cl213_cod, libname);
   F_RGST(SV2D_CL213_PRC, sv2d_cl213_prc, libname);
   F_RGST(SV2D_CL213_HLP, sv2d_cl213_hlp, libname);
 
   // ---  class SV2D_CL214
   F_RGST(SV2D_CL214_000, sv2d_cl214_000, libname);
   F_RGST(SV2D_CL214_999, sv2d_cl214_999, libname);
   F_RGST(SV2D_CL214_COD, sv2d_cl214_cod, libname);
   F_RGST(SV2D_CL214_PRC, sv2d_cl214_prc, libname);
   F_RGST(SV2D_CL214_HLP, sv2d_cl214_hlp, libname);
 
   // ---  class SV2D_CNB
   F_RGST(SV2D_CNB_PRCDLIB, sv2d_cnb_prcdlib, libname);
 
   // ---  class SV2D_CNN
   F_RGST(SV2D_CNN_PRCDLIB, sv2d_cnn_prcdlib, libname);
 
   // ---  class SV2D_LGCY
   F_RGST(SV2D_LGCY_000, sv2d_lgcy_000, libname);
   F_RGST(SV2D_LGCY_999, sv2d_lgcy_999, libname);
   F_RGST(SV2D_LGCY_CTR, sv2d_lgcy_ctr, libname);
   F_RGST(SV2D_LGCY_DTR, sv2d_lgcy_dtr, libname);
   F_RGST(SV2D_LGCY_INI, sv2d_lgcy_ini, libname);
   F_RGST(SV2D_LGCY_RST, sv2d_lgcy_rst, libname);
   F_RGST(SV2D_LGCY_REQHBASE, sv2d_lgcy_reqhbase, libname);
   F_RGST(SV2D_LGCY_HVALIDE, sv2d_lgcy_hvalide, libname);
   F_RGST(SV2D_LGCY_REQHPRN, sv2d_lgcy_reqhprn, libname);
 
   // ---  class SV2D_MH
   F_RGST(SV2D_MH_CBS_CLCPRNO, sv2d_mh_cbs_clcprno, libname);
   F_RGST(SV2D_MH_CBS_CLCPRNEV, sv2d_mh_cbs_clcprnev, libname);
   F_RGST(SV2D_MH_CBS_CLCPRNES, sv2d_mh_cbs_clcprnes, libname);
   F_RGST(SV2D_MH_CNB_000, sv2d_mh_cnb_000, libname);
   F_RGST(SV2D_MH_CNB_ASGCMN, sv2d_mh_cnb_asgcmn, libname);
   F_RGST(SV2D_MH_CNB_REQFNC, sv2d_mh_cnb_reqfnc, libname);
   F_RGST(SV2D_MH_CNB_REQPRM, sv2d_mh_cnb_reqprm, libname);
   F_RGST(SV2D_MH_CNN_000, sv2d_mh_cnn_000, libname);
   F_RGST(SV2D_MH_CNN_ASGCMN, sv2d_mh_cnn_asgcmn, libname);
   F_RGST(SV2D_MH_CNN_REQFNC, sv2d_mh_cnn_reqfnc, libname);
   F_RGST(SV2D_MH_CNN_REQPRM, sv2d_mh_cnn_reqprm, libname);
 
   // ---  class SV2D_PST
   F_RGST(SV2D_PST_CD2D_000, sv2d_pst_cd2d_000, libname);
   F_RGST(SV2D_PST_CD2D_999, sv2d_pst_cd2d_999, libname);
   F_RGST(SV2D_PST_CD2D_CTR, sv2d_pst_cd2d_ctr, libname);
   F_RGST(SV2D_PST_CD2D_DTR, sv2d_pst_cd2d_dtr, libname);
   F_RGST(SV2D_PST_CD2D_INI, sv2d_pst_cd2d_ini, libname);
   F_RGST(SV2D_PST_CD2D_RST, sv2d_pst_cd2d_rst, libname);
   F_RGST(SV2D_PST_CD2D_REQHBASE, sv2d_pst_cd2d_reqhbase, libname);
   F_RGST(SV2D_PST_CD2D_HVALIDE, sv2d_pst_cd2d_hvalide, libname);
   F_RGST(SV2D_PST_CD2D_ACC, sv2d_pst_cd2d_acc, libname);
   F_RGST(SV2D_PST_CD2D_FIN, sv2d_pst_cd2d_fin, libname);
   F_RGST(SV2D_PST_CD2D_XEQ, sv2d_pst_cd2d_xeq, libname);
   F_RGST(SV2D_PST_CD2D_ASGHSIM, sv2d_pst_cd2d_asghsim, libname);
   F_RGST(SV2D_PST_CD2D_REQHVNO, sv2d_pst_cd2d_reqhvno, libname);
   F_RGST(SV2D_PST_CD2D_REQNOMF, sv2d_pst_cd2d_reqnomf, libname);
   F_RGST(SV2D_PST_CFL_000, sv2d_pst_cfl_000, libname);
   F_RGST(SV2D_PST_CFL_999, sv2d_pst_cfl_999, libname);
   F_RGST(SV2D_PST_CFL_CTR, sv2d_pst_cfl_ctr, libname);
   F_RGST(SV2D_PST_CFL_DTR, sv2d_pst_cfl_dtr, libname);
   F_RGST(SV2D_PST_CFL_INI, sv2d_pst_cfl_ini, libname);
   F_RGST(SV2D_PST_CFL_RST, sv2d_pst_cfl_rst, libname);
   F_RGST(SV2D_PST_CFL_REQHBASE, sv2d_pst_cfl_reqhbase, libname);
   F_RGST(SV2D_PST_CFL_HVALIDE, sv2d_pst_cfl_hvalide, libname);
   F_RGST(SV2D_PST_CFL_ACC, sv2d_pst_cfl_acc, libname);
   F_RGST(SV2D_PST_CFL_FIN, sv2d_pst_cfl_fin, libname);
   F_RGST(SV2D_PST_CFL_XEQ, sv2d_pst_cfl_xeq, libname);
   F_RGST(SV2D_PST_CFL_ASGHSIM, sv2d_pst_cfl_asghsim, libname);
   F_RGST(SV2D_PST_CFL_REQHVNO, sv2d_pst_cfl_reqhvno, libname);
   F_RGST(SV2D_PST_CFL_REQNOMF, sv2d_pst_cfl_reqnomf, libname);
   F_RGST(SV2D_PST_CHKQ_000, sv2d_pst_chkq_000, libname);
   F_RGST(SV2D_PST_CHKQ_999, sv2d_pst_chkq_999, libname);
   F_RGST(SV2D_PST_CHKQ_CTR, sv2d_pst_chkq_ctr, libname);
   F_RGST(SV2D_PST_CHKQ_DTR, sv2d_pst_chkq_dtr, libname);
   F_RGST(SV2D_PST_CHKQ_INI, sv2d_pst_chkq_ini, libname);
   F_RGST(SV2D_PST_CHKQ_RST, sv2d_pst_chkq_rst, libname);
   F_RGST(SV2D_PST_CHKQ_REQHBASE, sv2d_pst_chkq_reqhbase, libname);
   F_RGST(SV2D_PST_CHKQ_HVALIDE, sv2d_pst_chkq_hvalide, libname);
   F_RGST(SV2D_PST_CHKQ_XEQ, sv2d_pst_chkq_xeq, libname);
   F_RGST(SV2D_PST_CHKQ_REQHVNO, sv2d_pst_chkq_reqhvno, libname);
   F_RGST(SV2D_PST_CHKQ_REQNOMF, sv2d_pst_chkq_reqnomf, libname);
   F_RGST(SV2D_PST_CMP_000, sv2d_pst_cmp_000, libname);
   F_RGST(SV2D_PST_CMP_999, sv2d_pst_cmp_999, libname);
   F_RGST(SV2D_PST_CMP_CTR, sv2d_pst_cmp_ctr, libname);
   F_RGST(SV2D_PST_CMP_DTR, sv2d_pst_cmp_dtr, libname);
   F_RGST(SV2D_PST_CMP_INI, sv2d_pst_cmp_ini, libname);
   F_RGST(SV2D_PST_CMP_RST, sv2d_pst_cmp_rst, libname);
   F_RGST(SV2D_PST_CMP_REQHBASE, sv2d_pst_cmp_reqhbase, libname);
   F_RGST(SV2D_PST_CMP_HVALIDE, sv2d_pst_cmp_hvalide, libname);
   F_RGST(SV2D_PST_CMP_ACC, sv2d_pst_cmp_acc, libname);
   F_RGST(SV2D_PST_CMP_FIN, sv2d_pst_cmp_fin, libname);
   F_RGST(SV2D_PST_CMP_XEQ, sv2d_pst_cmp_xeq, libname);
   F_RGST(SV2D_PST_CMP_ASGHSIM, sv2d_pst_cmp_asghsim, libname);
   F_RGST(SV2D_PST_CMP_REQHVNO, sv2d_pst_cmp_reqhvno, libname);
   F_RGST(SV2D_PST_CMP_REQNOMF, sv2d_pst_cmp_reqnomf, libname);
   F_RGST(SV2D_PST_Q_000, sv2d_pst_q_000, libname);
   F_RGST(SV2D_PST_Q_999, sv2d_pst_q_999, libname);
   F_RGST(SV2D_PST_Q_CTR, sv2d_pst_q_ctr, libname);
   F_RGST(SV2D_PST_Q_DTR, sv2d_pst_q_dtr, libname);
   F_RGST(SV2D_PST_Q_INI, sv2d_pst_q_ini, libname);
   F_RGST(SV2D_PST_Q_RST, sv2d_pst_q_rst, libname);
   F_RGST(SV2D_PST_Q_REQHBASE, sv2d_pst_q_reqhbase, libname);
   F_RGST(SV2D_PST_Q_HVALIDE, sv2d_pst_q_hvalide, libname);
   F_RGST(SV2D_PST_Q_ACC, sv2d_pst_q_acc, libname);
   F_RGST(SV2D_PST_Q_FIN, sv2d_pst_q_fin, libname);
   F_RGST(SV2D_PST_Q_XEQ, sv2d_pst_q_xeq, libname);
   F_RGST(SV2D_PST_Q_ASGHSIM, sv2d_pst_q_asghsim, libname);
   F_RGST(SV2D_PST_Q_REQHVNO, sv2d_pst_q_reqhvno, libname);
   F_RGST(SV2D_PST_Q_REQNOMF, sv2d_pst_q_reqnomf, libname);
   F_RGST(SV2D_PST_SIM_000, sv2d_pst_sim_000, libname);
   F_RGST(SV2D_PST_SIM_999, sv2d_pst_sim_999, libname);
   F_RGST(SV2D_PST_SIM_CTR, sv2d_pst_sim_ctr, libname);
   F_RGST(SV2D_PST_SIM_DTR, sv2d_pst_sim_dtr, libname);
   F_RGST(SV2D_PST_SIM_INI, sv2d_pst_sim_ini, libname);
   F_RGST(SV2D_PST_SIM_RST, sv2d_pst_sim_rst, libname);
   F_RGST(SV2D_PST_SIM_REQHBASE, sv2d_pst_sim_reqhbase, libname);
   F_RGST(SV2D_PST_SIM_HVALIDE, sv2d_pst_sim_hvalide, libname);
   F_RGST(SV2D_PST_SIM_ACC, sv2d_pst_sim_acc, libname);
   F_RGST(SV2D_PST_SIM_FIN, sv2d_pst_sim_fin, libname);
   F_RGST(SV2D_PST_SIM_XEQ, sv2d_pst_sim_xeq, libname);
   F_RGST(SV2D_PST_SIM_ASGHSIM, sv2d_pst_sim_asghsim, libname);
   F_RGST(SV2D_PST_SIM_REQHVNO, sv2d_pst_sim_reqhvno, libname);
   F_RGST(SV2D_PST_SIM_REQNOMF, sv2d_pst_sim_reqnomf, libname);
 
   // ---  class SV2D_XBS
   F_RGST(SV2D_XBS_000, sv2d_xbs_000, libname);
   F_RGST(SV2D_XBS_999, sv2d_xbs_999, libname);
   F_RGST(SV2D_XBS_CTR, sv2d_xbs_ctr, libname);
   F_RGST(SV2D_XBS_DTR, sv2d_xbs_dtr, libname);
   F_RGST(SV2D_XBS_INI, sv2d_xbs_ini, libname);
   F_RGST(SV2D_XBS_RST, sv2d_xbs_rst, libname);
   F_RGST(SV2D_XBS_REQHBASE, sv2d_xbs_reqhbase, libname);
   F_RGST(SV2D_XBS_HVALIDE, sv2d_xbs_hvalide, libname);
   F_RGST(SV2D_XBS_HLPPRNO, sv2d_xbs_hlpprno, libname);
   F_RGST(SV2D_XBS_PRCPRNO, sv2d_xbs_prcprno, libname);
   F_RGST(SV2D_XBS_PRNPRNO, sv2d_xbs_prnprno, libname);
   F_RGST(SV2D_XBS_PSCPRNO, sv2d_xbs_pscprno, libname);
   F_RGST(SV2D_XBS_PSLPRNO, sv2d_xbs_pslprno, libname);
   F_RGST(SV2D_XBS_HLPPRGL, sv2d_xbs_hlpprgl, libname);
   F_RGST(SV2D_XBS_PRCPRGL, sv2d_xbs_prcprgl, libname);
   F_RGST(SV2D_XBS_PRNPRGL, sv2d_xbs_prnprgl, libname);
   F_RGST(SV2D_XBS_PSLPRGL, sv2d_xbs_pslprgl, libname);
   F_RGST(SV2D_XBS_CLCPREV, sv2d_xbs_clcprev, libname);
   F_RGST(SV2D_XBS_CLCPREVE, sv2d_xbs_clcpreve, libname);
   F_RGST(SV2D_XBS_CLCPREVE_Z, sv2d_xbs_clcpreve_z, libname);
   F_RGST(SV2D_XBS_PRCPREV, sv2d_xbs_prcprev, libname);
   F_RGST(SV2D_XBS_PSLPREV, sv2d_xbs_pslprev, libname);
   F_RGST(SV2D_XBS_CLCPRES, sv2d_xbs_clcpres, libname);
   F_RGST(SV2D_XBS_CLCPRESE, sv2d_xbs_clcprese, libname);
   F_RGST(SV2D_XBS_PRCPRES, sv2d_xbs_prcpres, libname);
   F_RGST(SV2D_XBS_PRCSOLC, sv2d_xbs_prcsolc, libname);
   F_RGST(SV2D_XBS_PSLSOLC, sv2d_xbs_pslsolc, libname);
   F_RGST(SV2D_XBS_PRCSOLR, sv2d_xbs_prcsolr, libname);
   F_RGST(SV2D_XBS_PSLSOLR, sv2d_xbs_pslsolr, libname);
   F_RGST(SV2D_XBS_CLCCLIM, sv2d_xbs_clcclim, libname);
   F_RGST(SV2D_XBS_HLPCLIM, sv2d_xbs_hlpclim, libname);
   F_RGST(SV2D_XBS_PRNCLIM, sv2d_xbs_prnclim, libname);
   F_RGST(SV2D_XBS_PSLCLIM, sv2d_xbs_pslclim, libname);
   F_RGST(SV2D_XBS_CLCDLIB, sv2d_xbs_clcdlib, libname);
   F_RGST(SV2D_XBS_PRCDLIB, sv2d_xbs_prcdlib, libname);
   F_RGST(SV2D_XBS_PSLDLIB, sv2d_xbs_psldlib, libname);
   F_RGST(SV2D_XBS_ASMF_R, sv2d_xbs_asmf_r, libname);
   F_RGST(SV2D_XBS_ASMKT_R, sv2d_xbs_asmkt_r, libname);
   F_RGST(SV2D_XBS_ASMKT_R_Z, sv2d_xbs_asmkt_r_z, libname);
   F_RGST(SV2D_XBS_ASMKU_R, sv2d_xbs_asmku_r, libname);
   F_RGST(SV2D_XBS_ASMK_R, sv2d_xbs_asmk_r, libname);
   F_RGST(SV2D_XBS_ASMM, sv2d_xbs_asmm, libname);
   F_RGST(SV2D_XBS_ASMMU, sv2d_xbs_asmmu, libname);
 
   // ---  class SV2D_XCL001
   F_RGST(SV2D_XCL001_000, sv2d_xcl001_000, libname);
   F_RGST(SV2D_XCL001_999, sv2d_xcl001_999, libname);
 
   // ---  class SV2D_XCL002
   F_RGST(SV2D_XCL002_000, sv2d_xcl002_000, libname);
   F_RGST(SV2D_XCL002_999, sv2d_xcl002_999, libname);
 
   // ---  class SV2D_XCL003
   F_RGST(SV2D_XCL003_000, sv2d_xcl003_000, libname);
   F_RGST(SV2D_XCL003_999, sv2d_xcl003_999, libname);
 
   // ---  class SV2D_XCL004
   F_RGST(SV2D_XCL004_000, sv2d_xcl004_000, libname);
   F_RGST(SV2D_XCL004_999, sv2d_xcl004_999, libname);
 
   // ---  class SV2D_XCL010
   F_RGST(SV2D_XCL010_000, sv2d_xcl010_000, libname);
   F_RGST(SV2D_XCL010_999, sv2d_xcl010_999, libname);
 
   // ---  class SV2D_XCL011
   F_RGST(SV2D_XCL011_000, sv2d_xcl011_000, libname);
   F_RGST(SV2D_XCL011_999, sv2d_xcl011_999, libname);
 
   // ---  class SV2D_XCL012
   F_RGST(SV2D_XCL012_000, sv2d_xcl012_000, libname);
   F_RGST(SV2D_XCL012_999, sv2d_xcl012_999, libname);
 
   // ---  class SV2D_Y2
   F_RGST(SV2D_Y2_CBS_CLCPRNO, sv2d_y2_cbs_clcprno, libname);
   F_RGST(SV2D_Y2_CBS_CLCPRNEV, sv2d_y2_cbs_clcprnev, libname);
   F_RGST(SV2D_Y2_CBS_CLCPRNES, sv2d_y2_cbs_clcprnes, libname);
   F_RGST(SV2D_Y2_CBS_REQPRM, sv2d_y2_cbs_reqprm, libname);
   F_RGST(SV2D_Y2_CNN_000, sv2d_y2_cnn_000, libname);
   F_RGST(SV2D_Y2_CNN_REQHBASE, sv2d_y2_cnn_reqhbase, libname);
   F_RGST(SV2D_Y2_CNN_ASGCMN, sv2d_y2_cnn_asgcmn, libname);
   F_RGST(SV2D_Y2_CNN_REQFNC, sv2d_y2_cnn_reqfnc, libname);
   F_RGST(SV2D_Y2_CNN_REQPRM, sv2d_y2_cnn_reqprm, libname);
 
   // ---  class SV2D_Y3
   F_RGST(SV2D_Y3_CBS_PRCPRGL, sv2d_y3_cbs_prcprgl, libname);
   F_RGST(SV2D_Y3_CBS_PSLPRGL, sv2d_y3_cbs_pslprgl, libname);
   F_RGST(SV2D_Y3_CBS_REQPRM, sv2d_y3_cbs_reqprm, libname);
   F_RGST(SV2D_Y3_CNN_000, sv2d_y3_cnn_000, libname);
   F_RGST(SV2D_Y3_CNN_REQHBASE, sv2d_y3_cnn_reqhbase, libname);
   F_RGST(SV2D_Y3_CNN_ASGCMN, sv2d_y3_cnn_asgcmn, libname);
   F_RGST(SV2D_Y3_CNN_REQFNC, sv2d_y3_cnn_reqfnc, libname);
   F_RGST(SV2D_Y3_CNN_REQPRM, sv2d_y3_cnn_reqprm, libname);
 
   // ---  class SV2D_Y4
   F_RGST(SV2D_Y4_CBS_CLCPREV, sv2d_y4_cbs_clcprev, libname);
   F_RGST(SV2D_Y4_CBS_CLCPREVE, sv2d_y4_cbs_clcpreve, libname);
   F_RGST(SV2D_Y4_CBS_CLCPRNO, sv2d_y4_cbs_clcprno, libname);
   F_RGST(SV2D_Y4_CBS_CLCPRNEV, sv2d_y4_cbs_clcprnev, libname);
   F_RGST(SV2D_Y4_CBS_CLCPRNES, sv2d_y4_cbs_clcprnes, libname);
   F_RGST(SV2D_Y4_CBS_CLCPRN1N, sv2d_y4_cbs_clcprn1n, libname);
   F_RGST(SV2D_Y4_CBS_PRCPRGL, sv2d_y4_cbs_prcprgl, libname);
   F_RGST(SV2D_Y4_CBS_PSLPRGL, sv2d_y4_cbs_pslprgl, libname);
   F_RGST(SV2D_Y4_CBS_PSLPRNO, sv2d_y4_cbs_pslprno, libname);
   F_RGST(SV2D_Y4_CBS_REQPRM, sv2d_y4_cbs_reqprm, libname);
   F_RGST(SV2D_Y4_CNN_000, sv2d_y4_cnn_000, libname);
   F_RGST(SV2D_Y4_CNN_REQHBASE, sv2d_y4_cnn_reqhbase, libname);
   F_RGST(SV2D_Y4_CNN_ASGCMN, sv2d_y4_cnn_asgcmn, libname);
   F_RGST(SV2D_Y4_CNN_REQFNC, sv2d_y4_cnn_reqfnc, libname);
   F_RGST(SV2D_Y4_CNN_REQPRM, sv2d_y4_cnn_reqprm, libname);
 
   // ---  class SV2D_YS
   F_RGST(SV2D_YS_CBS_CLCPRNO, sv2d_ys_cbs_clcprno, libname);
   F_RGST(SV2D_YS_CBS_CLCPRNEV, sv2d_ys_cbs_clcprnev, libname);
   F_RGST(SV2D_YS_CBS_CLCPRNES, sv2d_ys_cbs_clcprnes, libname);
   F_RGST(SV2D_YS_CNN_000, sv2d_ys_cnn_000, libname);
   F_RGST(SV2D_YS_CNN_ASGCMN, sv2d_ys_cnn_asgcmn, libname);
   F_RGST(SV2D_YS_CNN_REQFNC, sv2d_ys_cnn_reqfnc, libname);
   F_RGST(SV2D_YS_CNN_REQPRM, sv2d_ys_cnn_reqprm, libname);
   F_RGST(SV2D_YS_CNB_000, sv2d_ys_cnb_000, libname);
   F_RGST(SV2D_YS_CNB_ASGCMN, sv2d_ys_cnb_asgcmn, libname);
   F_RGST(SV2D_YS_CNB_REQFNC, sv2d_ys_cnb_reqfnc, libname);
   F_RGST(SV2D_YS_CNB_REQPRM, sv2d_ys_cnb_reqprm, libname);
 
   // ---  class SV2D_Z4
   F_RGST(SV2D_Z4_CNN_000, sv2d_z4_cnn_000, libname);
   F_RGST(SV2D_Z4_CNN_REQHBASE, sv2d_z4_cnn_reqhbase, libname);
   F_RGST(SV2D_Z4_CNN_ASGCMN, sv2d_z4_cnn_asgcmn, libname);
   F_RGST(SV2D_Z4_CNN_REQFNC, sv2d_z4_cnn_reqfnc, libname);
   F_RGST(SV2D_Z4_CBS_CLCPRNEV, sv2d_z4_cbs_clcprnev, libname);
   F_RGST(SV2D_Z4_CBS_CLCPRNES, sv2d_z4_cbs_clcprnes, libname);
   F_RGST(SV2D_Z4_CBS_CLCPRN1N, sv2d_z4_cbs_clcprn1n, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
