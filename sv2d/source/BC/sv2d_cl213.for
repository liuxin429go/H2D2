C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Condition harmonique (Dirichlet) sur le niveau d'eau h.
C
C Functions:
C   Public:
C     INTEGER SV2D_CL213_000
C     INTEGER SV2D_CL213_999
C     INTEGER SV2D_CL213_COD
C     INTEGER SV2D_CL213_PRC
C     INTEGER SV2D_CL213_HLP
C   Private:
C     INTEGER SV2D_CL213_INIVTBL
C     CHARACTER*(16) SV2D_CL213_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL213_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl213.fc'

      DATA SV2D_CL213_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL213_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL213_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL213_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl213.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl213.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL213_INIVTBL()

      SV2D_CL213_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL213_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL213_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl213.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL213_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL213_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL213_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl213.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl213.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL213_HLP
      EXTERNAL SV2D_CL213_COD
      EXTERNAL SV2D_CL213_PRC
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL213_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL213_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL213_TPN(1:LTPN), SV2D_CL213_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL213_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL213_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL213_TYP,
     &                     SV2D_CL213_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD, SV2D_CL213_TYP,
     &                     SV2D_CL213_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_PRC, SV2D_CL213_TYP,
     &                     SV2D_CL213_PRC)

      SV2D_CL213_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL213_COD
C
C Description:
C     La fonction SV2D_CL213_COD assigne les codes de conditions limites.
C     Sur la limite <code>IL</code>, elle impose le degré de niveau d'eau
C     comme condition de Dirichlet.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL213_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL213_COD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl213.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl213.fc'

      INTEGER I, IC, IN, INDEB, INFIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL213_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
      IF (KCLLIM(4,IL)-KCLLIM(3,IL)+1 .LT. 1) GOTO 9900
      IF (KCLCND(4,IC)-KCLCND(3,IC)+1 .NE. 1) GOTO 9902

!      Contrôles
!         type géométrique de la limite comme segment simple
!         n*2 valeurs
!         n multiple de 3
!         n >= 3

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_DIRICHLET)
         ENDIF
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_NOD_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL213_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL213_PRC
C
C Description:
C     La fonction SV2D_CL213_PRC impose le degré de niveau d'eau
C     comme condition de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C     Conceptuellement
C     (
C        (o,a,p),
C        (o,a,p), ...
C     )
C************************************************************************
      FUNCTION SV2D_CL213_PRC(IL,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL213_PRC
CDEC$ ENDIF

      USE TRIGD_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)

      INCLUDE 'sv2d_cl213.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sptide.fi'
      INCLUDE 'sv2d_cl213.fc'

      INTEGER I, IC, IN, IV
      INTEGER INDEB, INFIN, IVDEB, IVFIN
      REAL*8  H, O, A, D
      REAL*8  T, T0
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL213_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. 3)
      IVDEB = KCLCND(3, IC)
      IVFIN = KCLCND(4, IC)

      T = LM_CMMN_TSIM     ! Temps de la simu.

      H = 0.0D0
      DO IV=IVDEB,IVFIN,3
         O = VCLCNV(IV+0)     ! oméga
         A = VCLCNV(IV+1)     ! amplitude
         D = VCLCNV(IV+2)     ! phi
         H = H + A * COSD(O*T + D)
      ENDDO

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I=INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) VDIMP(3,IN) = H
      ENDDO

      SV2D_CL213_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL213_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL213_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL213_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>213</b>: <br>
C  Harmonic Dirichlet condition on the water level.
C  The water level is applied on the whole boundary.
C  <p>
C  Conceptually the condition is described by:
C  <pre>
C         (
C            (o,a,p),
C            (o,a,p), ...
C         )</pre>
C  where o is the angular frequency, a the amplitude and p the phase shift.
C  The water level H is then:
C  <pre>
C     H = sum(a * cos(o*t + p))</pre>
C  Angular frequency, amplitude and phase shift are not interpolated.
C  <p>
C  <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 213</li>
C     <li>Values: o  a  p  o  a  p  ... </li>
C     <li>Units: o/s  m  o</li>
C     <li>Example:  213   0.0314  2.0  0.0</li>
C  </ul>
C</comment>

      SV2D_CL213_REQHLP = 'bc_type_213'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL213_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL213_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL213_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl213.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl213.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl213.hlp')

      SV2D_CL213_HLP = ERR_TYP()
      RETURN
      END
