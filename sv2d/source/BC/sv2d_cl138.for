C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Impose le débit global d'une autre limite qui est redistribué
C     en fonction du niveau d'eau actuel.
C     Formulation Manning variable
C
C Functions:
C   Public:
C     INTEGER SV2D_CL138_000
C     INTEGER SV2D_CL138_999
C     INTEGER SV2D_CL138_COD
C     INTEGER SV2D_CL138_ASMF
C     INTEGER SV2D_CL138_HLP
C   Private:
C     INTEGER SV2D_CL138_INIVTBL
C     CHARACTER*(16) SV2D_CL138_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL138_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl138.fc'

      DATA SV2D_CL138_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL138_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL138_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL138_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl138.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl138.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL138_INIVTBL()

      SV2D_CL138_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL138_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL138_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl138.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL138_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL138_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL138_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl138.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl138.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL138_HLP
      EXTERNAL SV2D_CL138_COD
      EXTERNAL SV2D_CL138_ASMF
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL138_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL138_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL138_TPN(1:LTPN), SV2D_CL138_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL138_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL138_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL138_TYP,
     &                     SV2D_CL138_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD, SV2D_CL138_TYP,
     &                    SV2D_CL138_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMF,SV2D_CL138_TYP,
     &                    SV2D_CL138_ASMF)

      SV2D_CL138_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL138_COD
C
C Description:
C     La fonction SV2D_CL138_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose les valeurs de Q d'une
C     autre limite.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C     Valeurs lues: limite source 
C     On ne peut pas contrôler le nombre de valeurs de la source. Par
C     contre, elle doit être EG_TPLMT_1SGMT
C************************************************************************
      FUNCTION SV2D_CL138_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL138_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl138.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl138.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_DST, ID_DST
      INTEGER IL_SRC, ID_SRC
      INTEGER, PARAMETER :: KVLD(1) = (/1/)           ! Dest.
      INTEGER, PARAMETER :: KVLS(0) = (/INTEGER::/)   ! Source
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(1) = (/EG_TPLMT_1SGMT/)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL138_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVLD, KRNG, KKND)

C---     Les indices
      IC = KCLLIM(2, IL)
      IV = KCLCND(3, IC)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         IL_DST = IL
         ID_DST = 3        ! Sollicitation sur h
         IL_SRC = NINT( VCLCNV(IV) )
         ID_SRC = 1        ! Devrait être 1&2
         IERR = SV2D_CL_COD2L(IL_DST, ID_DST, SV2D_CL_DEBIT,
     &                        IL_SRC, ID_SRC, SV2D_CL_COPIE,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
         IF (ERR_GOOD()) VCLCNV(IV) = IL_SRC
      ENDIF
      
C---     Contrôle la limite source
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_CHKDIM(ID_SRC, KCLCND, KCLLIM,
     &                         KVLS, KRNG, KKND)
      ENDIF
        
9999  CONTINUE
      SV2D_CL138_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL138_ASMF
C
C Description:
C     La fonction SV2D_CL138_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C     D'après la loi de Manning:
C        u = H**(2/3) / n * pente
C     en posant la pente constante (==1), le débit spécifique est
C        q = H**(5/3) / n
C     qui est intégré.
C************************************************************************
      FUNCTION SV2D_CL138_ASMF(IL,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL138_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      REAL*8  VSOLC (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl138.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl138.fc'

      REAL*8  VNX, VNY, DJL3, DJL2
      REAL*8  QX1, QX2, QX3
      REAL*8  QY1, QY2, QY3
      REAL*8  QXS, QYS
      REAL*8  QTOT, QSPEC
      REAL*8  F1, F3, P1, P3, Q1, Q3, QS
      REAL*8  C
      REAL*8  VL(2), VG(2)
      INTEGER NO1, NO2, NO3
      INTEGER IERR, I_ERROR
      INTEGER I, IE, IC, IV
      INTEGER IL_DST, IL_SRC
      INTEGER IEDEB_DST, IEFIN_DST
      INTEGER IEDEB_SRC, IEFIN_SRC
      INTEGER IN_DST, IN_SRC

      REAL*8  CINQ_TIER
      PARAMETER (CINQ_TIER = 5.0D0/3.0D0)
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL138_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 1)
      IV = KCLCND(3,IC)
      IL_SRC = NINT( VCLCNV(IV) )

      IL_DST = IL
      IEDEB_DST = KCLLIM(5, IL_DST)
      IEFIN_DST = KCLLIM(6, IL_DST)
      IEDEB_SRC = KCLLIM(5, IL_SRC)
      IEFIN_SRC = KCLLIM(6, IL_SRC)

C---        Intègre le débit sur la limite source
      QTOT = 0.0D0
      DO I = IEDEB_SRC, IEFIN_SRC
         IE = KCLELE(I)

C---        Connectivités du L3
         NO1 = KNGS(1,IE)
         NO2 = KNGS(2,IE)
         NO3 = KNGS(3,IE)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 199

         VNX =  VDJS(2,IE)    ! VNX =  VTY
         VNY = -VDJS(1,IE)    ! VNY = -VTX
         DJL3=  VDJS(3,IE)
         DJL2=  UN_2*DJL3

C---        Valeurs nodales
         QX1 = VDLG (1,NO1)   ! Qx
         QY1 = VDLG (2,NO1)   ! Qy
         QX2 = VDLG (1,NO2)
         QY2 = VDLG (2,NO2)
         QX3 = VDLG (1,NO3)
         QY3 = VDLG (2,NO3)

C---        Débits normaux
         QXS = ((QX2+QX1) + (QX3+QX2))*VNX
         QYS = ((QY2+QY1) + (QY3+QY2))*VNY

C---        Intègre
         QTOT = QTOT + (QXS + QYS)*DJL2
199      CONTINUE
      ENDDO

C---        Intègre la surface sur la limite de sortie
      QS = 0.0D0
      DO I = IEDEB_DST, IEFIN_DST
         IE = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IE)
         NO3 = KNGS(3,IE)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 299

C---        Valeurs nodales
         F1 = MAX(VPRNO(SV2D_IPRNO_N, NO1), PETIT)    ! Manning
         P1 =     VPRNO(SV2D_IPRNO_H, NO1)            ! Profondeur
         F3 = MAX(VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q3 = (P3**CINQ_TIER) / F3

C---        Intègre
         QS = QS + VDJS(3,IE)*(Q1+Q3)
299      CONTINUE
      ENDDO

C---    Sync et contrôle
      VL(1) = QTOT
      VL(2) = QS
      CALL MPI_ALLREDUCE(VL, VG, 2, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      QTOT = VG(1)
      QS   = VG(2)
      IF (ABS(QTOT) .GT. PETIT .AND. QS .LT. PETIT) GOTO 9900
      QS = MAX(QS, PETIT)

C---     Débit spécifique
      QTOT = - QTOT     ! Transforme sortant en entrant, et vice-versa
      QSPEC = QTOT / QS

C---     Assemble
      DO I = IEDEB_DST, IEFIN_DST
         IE = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IE)
         NO3 = KNGS(3,IE)

C---        Coefficient
         C = -UN_3*QSPEC*VDJS(3,IE)

C---        Valeurs nodales
         F1 = MAX(VPRNO(SV2D_IPRNO_N, NO1), PETIT)    ! Manning
         P1 =     VPRNO(SV2D_IPRNO_H, NO1)            ! Profondeur
         F3 = MAX(VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q3 = (P3**CINQ_TIER) / F3

C---        ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO1), C*(Q1+Q1 + Q3), VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO3), C*(Q3+Q3 + Q1), VFG)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_CL138_TYP
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_DEBIT: ', QTOT
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_SURFACE: ', QS
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL138_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL138_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL138_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL138_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>138</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The discharge Q taken from a source boundary is distributed on the wetted part of the boundary,
C  taking into account the current water level h.
C  The velocity distribution is based on a Manning formulation with varying coefficient (i.e. depth and Manning dependent).
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 138</li>
C     <li>Values: Name</li>
C     <li>Units: [-]</li>
C     <li>Example:  138  $__Amont__$</li>
C  </ul>
C</comment>

      SV2D_CL138_REQHLP = 'bc_type_138'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL138_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL138_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL138_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl138.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl138.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl138.hlp')

      SV2D_CL138_HLP = ERR_TYP()
      RETURN
      END
