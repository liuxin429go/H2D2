C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Condition de Dirichlet sur le débit spécifique qx
C
C Functions:
C   Public:
C     INTEGER SV2D_CL101_000
C     INTEGER SV2D_CL101_999
C     INTEGER SV2D_CL101_COD
C     INTEGER SV2D_CL101_PRC
C     INTEGER SV2D_CL101_HLP
C   Private:
C     INTEGER SV2D_CL101_INIVTBL
C     CHARACTER*(16) SV2D_CL101_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL101_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl101.fc'

      DATA SV2D_CL101_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL101_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL101_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL101_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl101.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl101.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL101_INIVTBL()

      SV2D_CL101_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL101_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL101_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl101.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL101_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL101_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL101_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl101.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl101.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL101_HLP
      EXTERNAL SV2D_CL101_COD
      EXTERNAL SV2D_CL101_PRC
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL101_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL101_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL101_TPN(1:LTPN), SV2D_CL101_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL101_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL101_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL101_TYP,
     &                     SV2D_CL101_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD, SV2D_CL101_TYP,
     &                     SV2D_CL101_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_PRC, SV2D_CL101_TYP,
     &                     SV2D_CL101_PRC)

      SV2D_CL101_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL101_COD
C
C Description:
C     La fonction SV2D_CL101_COD assigne les codes de conditions limites.
C     Sur la limite <code>IL</code>, elle impose le degré de débit spécifique qx
C     comme condition de Dirichlet.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL101_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL101_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl101.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl101.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      INTEGER, PARAMETER :: KVAL(1) = (/1/)
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(0) = (/INTEGER::/) ! Zero-sized array
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL101_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVAL, KRNG, KKND)

C---     Les indices
      IC = KCLLIM(2, IL)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         INDEB = KCLLIM(3, IL)
         INFIN = KCLLIM(4, IL)
         DO I = INDEB, INFIN
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               KDIMP(1,IN) = IBSET(KDIMP(1,IN), EA_TPCL_DIRICHLET)
            ENDIF
         ENDDO
      ENDIF

      SV2D_CL101_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL101_PRC
C
C Description:
C     La fonction SV2D_CL101_PRC impose le degré de débit spécifique qx
C     comme condition de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL101_PRC(IL,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL101_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)

      INCLUDE 'sv2d_cl101.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl101.fc'

      INTEGER I, IC, IN, INDEB, INFIN, IV
      REAL*8  V
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL101_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 1)
      IV = KCLCND(3, IC)
      V  = VCLCNV(IV)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            VDIMP(1,IN) = V
         ENDIF
      ENDDO

      SV2D_CL101_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL101_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL101_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL101_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>101</b>: <br>
C  Dirichlet condition on the specific discharge in x.
C     <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 101</li>
C     <li>Values: qx</li>
C     <li>Units: m^2/s</li>
C     <li>Example:  101  10.0</li>
C     </ul>
C</comment>

      SV2D_CL101_REQHLP = 'bc_type_101'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL101_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL101_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL101_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl101.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl101.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl101.hlp')

      SV2D_CL101_HLP = ERR_TYP()
      RETURN
      END
