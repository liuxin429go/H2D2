C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Sollicitation de débit via la forme faible de la continuité.
C
C Functions:
C   Public:
C     INTEGER SV2D_CL121_000
C     INTEGER SV2D_CL121_999
C     INTEGER SV2D_CL121_COD
C     INTEGER SV2D_CL121_ASMF
C     INTEGER SV2D_CL121_HLP
C   Private:
C     INTEGER SV2D_CL121_INIVTBL
C     CHARACTER*(16) SV2D_CL121_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL121_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl121.fc'

      DATA SV2D_CL121_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL121_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL121_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL121_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl121.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl121.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL121_INIVTBL()

      SV2D_CL121_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL121_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL121_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl121.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL121_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL121_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL121_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl121.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl121.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL121_HLP
      EXTERNAL SV2D_CL121_COD
      EXTERNAL SV2D_CL121_ASMF
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL121_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL121_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL121_TPN(1:LTPN), SV2D_CL121_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL121_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL121_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL121_TYP,
     &                     SV2D_CL121_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD, SV2D_CL121_TYP,
     &                    SV2D_CL121_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMF,SV2D_CL121_TYP,
     &                    SV2D_CL121_ASMF)

      SV2D_CL121_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL121_COD
C
C Description:
C     La fonction SV2D_CL121_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le degré de niveau d'eau
C     comme sollicitation de débit.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C     Est-ce qu'on veut vraiment avoir plus qu'un segment
C************************************************************************
      FUNCTION SV2D_CL121_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL121_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl121.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl121.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      INTEGER NNOD
      INTEGER :: KVAL(2)
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(2) = (/EG_TPLMT_1SGMT,
     &                                  EG_TPLMT_nSGMT/) ! c.f. note
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL121_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      NNOD = KCLLIM(4,IL) - KCLLIM(3,IL) + 1
      KVAL(:) = (/ 1, NNOD /)
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVAL, KRNG, KKND)

C---     Les indices
      IC = KCLLIM(2, IL)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         INDEB = KCLLIM(3,IL)
         INFIN = KCLLIM(4,IL)
         DO I = INDEB, INFIN
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               KDIMP(3,IN) = IBSET(KDIMP(3,IN), SV2D_CL_SLC_DEBIT)
            ENDIF
         ENDDO
      ENDIF

      SV2D_CL121_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL121_ASMF
C
C Description:
C     La fonction SV2D_CL121_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le degré de niveau d'eau
C     comme sollicitation de débit.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL121_ASMF(IL,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL121_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl121.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cl121.fc'

      INTEGER IERR
      INTEGER I, IC, IN, IV
      INTEGER INDEB, INFIN, IVDEB, IVFIN
D     INTEGER NNOD, NVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL121_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
      INDEB = KCLLIM(3,IL)
      INFIN = KCLLIM(4,IL)
      IVDEB = KCLCND(3,IC)
      IVFIN = KCLCND(4,IC)
D     NNOD = INFIN - INDEB + 1
D     NVAL = IVFIN - IVDEB + 1
D     CALL ERR_ASR(NVAL .EQ. 1 .OR. NVAL .EQ. NNOD)

      IV = IVDEB - 1
      DO I = INDEB, INFIN
         IF (IV .LT. IVFIN) IV = IV + 1
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            IERR = SP_ELEM_ASMFE(1, KLOCN(3,IN), VCLCNV(IV), VFG)
         ENDIF
      ENDDO

      SV2D_CL121_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL121_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL121_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL121_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>121</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The discharge sollicitation is imposed node by node.
C  Conceptually the condition is described by:
C  <pre>    (q0, q1, ..., qn)</pre>
C  where qi is the specific discharge at node i.
C  As a special case, if only 1 value is specified, it will be used
C  for all nodes.
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 121</li>
C     <li>Values: q0  q1  q2 ... </li>
C     <li>Units: m^2/s</li>
C     <li>Example:  121   1.0 1.0 1.0</li>
C  </ul>
C</comment>

      SV2D_CL121_REQHLP = 'bc_type_121'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL121_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL121_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL121_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl121.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl121.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl121.hlp')

      SV2D_CL121_HLP = ERR_TYP()
      RETURN
      END
