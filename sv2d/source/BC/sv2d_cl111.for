C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Condition de Weak Dirichlet sur le débit spécifique qx
C
C Functions:
C   Public:
C     INTEGER SV2D_CL111_000
C     INTEGER SV2D_CL111_999
C     INTEGER SV2D_CL111_COD
C     INTEGER SV2D_CL111_PRC
C     INTEGER SV2D_CL111_ASMF
C     INTEGER SV2D_CL111_ASMKU
C     INTEGER SV2D_CL111_ASMK
C     INTEGER SV2D_CL111_HLP
C   Private:
C     INTEGER SV2D_CL111_INIVTBL
C     CHARACTER*(16) SV2D_CL111_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL111_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl111.fc'

      DATA SV2D_CL111_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL111_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL111_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl111.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl111.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL111_INIVTBL()

      SV2D_CL111_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL111_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl111.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL111_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL111_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl111.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl111.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL111_HLP
      EXTERNAL SV2D_CL111_COD
      EXTERNAL SV2D_CL111_PRC
      EXTERNAL SV2D_CL111_ASMF
      EXTERNAL SV2D_CL111_ASMKU
      EXTERNAL SV2D_CL111_ASMK
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL111_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL111_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL111_TPN(1:LTPN), SV2D_CL111_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL111_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL111_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL111_TYP,
     &                     SV2D_CL111_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD,  SV2D_CL111_TYP,
     &                    SV2D_CL111_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_PRC,  SV2D_CL111_TYP,
     &                    SV2D_CL111_PRC)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMF, SV2D_CL111_TYP,
     &                    SV2D_CL111_ASMF)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMKU,SV2D_CL111_TYP,
     &                    SV2D_CL111_ASMKU)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMK, SV2D_CL111_TYP,
     &                    SV2D_CL111_ASMK)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMKT,SV2D_CL111_TYP,
     &                    SV2D_CL111_ASMK)

      SV2D_CL111_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL111_COD
C
C Description:
C     La fonction SV2D_CL111_COD assigne les codes de conditions limites.
C     Sur la limite <code>IL</code>, elle impose les valeurs de qx
C     comme condition de Weak Dirichlet.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL111_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl111.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl111.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      INTEGER NNOD, NELE, NVAL
      INTEGER, PARAMETER :: KVAL(2) = (/1, 2/)
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(0) = (/INTEGER::/) ! Zero-sized array
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL111_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVAL, KRNG, KKND)

C---     Les indices
      IC = KCLLIM(2, IL)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         INDEB = KCLLIM(3, IL)
         INFIN = KCLLIM(4, IL)
         DO I = INDEB, INFIN
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               KDIMP(1,IN) = IBSET(KDIMP(1,IN), EA_TPCL_WEAKDIR)
            ENDIF
         ENDDO
      ENDIF

      SV2D_CL111_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL111_PRC
C
C Description:
C     La fonction SV2D_CL111_PRC impose le degré de débit spécifique qx
C     comme condition de Weak Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_PRC(IL,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL111_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)

      INCLUDE 'sv2d_cl111.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl111.fc'

      INTEGER I, IC, IN, INDEB, INFIN, IV
      REAL*8  V
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL111_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. 1)
      IV = KCLCND(3, IC)
      V  = VCLCNV(IV)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            VDIMP(1,IN) = V
         ENDIF
      ENDDO

      SV2D_CL111_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL111_ASMF
C
C Description:
C     La fonction SV2D_CL111_ASMF assemble la membre de droite de
C     la condition.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_ASMF(IL,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL111_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8  VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl111.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cl111.fc'

      INTEGER IERR
      INTEGER IC, IV, IE, IES
      INTEGER IEDEB, IEFIN
      INTEGER NVAL
      INTEGER NO1, NO2, NO3
      REAL*8  W, C, V1, V2, V3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL111_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
      NVAL = KCLCND(4, IC) - KCLCND(3, IC) + 1
D     CALL ERR_ASR(NVAL .EQ. 1 .OR. NVAL .EQ. 2)
      IV = KCLCND(3, IC)
      W  = 1.0D0
      IF (NVAL .EQ. 2) W  = VCLCNV(IV+1)

C---     Indices des éléments
      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Assemble
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)

C---        Valeurs nodales
         V1 = VDIMP(1,NO1)
         V2 = VDIMP(1,NO2)
         V3 = VDIMP(1,NO3)

C---        Assemblage du vecteur global lumped
         C = UN_2*W*VDJS(3,IES)
         IERR = SP_ELEM_ASMFE(1, KLOCN(1,NO1), C*(V1),    VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(1,NO2), C*(V2+V2), VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(1,NO3), C*(V3),    VFG)
      ENDDO

      SV2D_CL111_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL111_ASMKU
C
C Description:
C     La fonction SV2D_CL111_ASMKU est vide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_ASMKU(IL,
     &                          KLOCE,
     &                          VKE,
     &                          VFE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL111_ASMKU
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl111.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cl111.fc'

      INTEGER IERR
      INTEGER IC, IV, IE, IES
      INTEGER IEDEB, IEFIN
      INTEGER NVAL
      INTEGER NO1, NO2, NO3
      REAL*8  W, C, V1, V2, V3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL111_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
      NVAL = KCLCND(4, IC) - KCLCND(3, IC) + 1
D     CALL ERR_ASR(NVAL .EQ. 1 .OR. NVAL .EQ. 2)
      IV = KCLCND(3, IC)
      W  = 1.0D0
      IF (NVAL .EQ. 2) W  = VCLCNV(IV+1)

C---     Indices des éléments
      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Assemble
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)

C---        Valeurs nodales
         V1 = VDLG(1,NO1)
         V2 = VDLG(1,NO2)
         V3 = VDLG(1,NO3)

C---        Assemblage du vecteur global lumped
         C = UN_2*W*VDJS(3,IES)
         IERR = SP_ELEM_ASMFE(1, KLOCN(1,NO1), C*(V1),    VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(1,NO2), C*(V2+V2), VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(1,NO3), C*(V3),    VFG)
      ENDDO

      SV2D_CL111_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL111_ASMK
C
C Description:
C     La fonction SV2D_CL111_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_ASMK(IL,
     &                         KLOCE,
     &                         VKE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         HMTX,
     &                         F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL111_ASMK
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER, PARAMETER :: NDLE_LCL = 3

      INTEGER  IL
      INTEGER  KLOCE (NDLE_LCL)
      REAL*8   VKE   (NDLE_LCL, LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl111.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl111.fc'

      INTEGER IERR
      INTEGER IEDEB, IEFIN
      INTEGER IC, IV, IE, IES
      INTEGER NVAL
      INTEGER NO1, NO2, NO3
      REAL*8  W, ML
C----------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL111_TYP)
D     CALL ERR_PRE(LM_CMMN_NDLEV .GE. NDLE_LCL)
C-----------------------------------------------------------------

      IC = KCLLIM(2, IL)
      NVAL = KCLCND(4, IC) - KCLCND(3, IC) + 1
D     CALL ERR_ASR(NVAL .EQ. 1 .OR. NVAL .EQ. 2)
      IV = KCLCND(3, IC)
      W  = 1.0D0
      IF (NVAL .EQ. 2) W  = VCLCNV(IV+1)

C---     Initialise la matrice élémentaire
      CALL DINIT(NDLE_LCL*LM_CMMN_NDLEV, ZERO, VKE, 1)

C---     Indices des éléments
      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Boucle sur les éléments de la limite
C        ====================================
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)

C---        Matrice masse lumpded
         ML = UN_2*W*VDJS(3,IES)
         VKE(1,1) = ML
         VKE(2,2) = ML+ML
         VKE(3,3) = ML

C---        Table KLOCE
         KLOCE(1) = KLOCN(1,NO1)
         KLOCE(2) = KLOCN(1,NO2)
         KLOCE(3) = KLOCN(1,NO3)

C---       Assemblage de la matrice
         IERR = F_ASM(HMTX, NDLE_LCL, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      SV2D_CL111_ASMK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL111_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL111_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>111</b>: <br>
C  Weak Dirichlet condition on the specific discharge in x.<br>
C  A term <i>w*(qx-qx0)</i> is added on the boundary, where 
C  <i>w</i> is the weight and 
C  <i>qx0</i> the target specific discharge.
C     <ul>
C     <li>Kind: Weak Dirichlet</li>
C     <li>Code: 111</li>
C     <li>Values: qx0  [w (default 1.0)]</li>
C     <li>Units: m^2/s  [-]</li>
C     <li>Example:  111  10.0</li>
C     </ul>
C</comment>

      SV2D_CL111_REQHLP = 'bc_type_111'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL111_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL111_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL111_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl111.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl111.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl111.hlp')

      SV2D_CL111_HLP = ERR_TYP()
      RETURN
      END
