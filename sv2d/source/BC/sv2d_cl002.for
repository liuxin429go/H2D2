C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SV2D_CL002_000
C     INTEGER SV2D_CL002_999
C     INTEGER SV2D_CL002_COD
C     INTEGER SV2D_CL002_PRC
C     INTEGER SV2D_CL002_HLP
C   Private:
C     INTEGER SV2D_CL002_INIVTBL
C     CHARACTER*(16) SV2D_CL002_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL002_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL002_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL002_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl002.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl002.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL002_INIVTBL()

      SV2D_CL002_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL002_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL002_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl002.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL002_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL002_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL002_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl002.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl002.fc'

      INTEGER IERR
      INTEGER HVFT
      EXTERNAL SV2D_CL002_HLP
      EXTERNAL SV2D_CL002_COD
      EXTERNAL SV2D_CL002_PRC
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL002_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL002_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_HLP, SV2D_CL002_TYP,
     &                    SV2D_CL002_HLP)
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_COD, SV2D_CL002_TYP,
     &                    SV2D_CL002_COD)
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_PRC, SV2D_CL002_TYP,
     &                    SV2D_CL002_PRC)

      SV2D_CL002_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL002_COD
C
C Description:
C     La sous-routine privée SV2D_CL002_COD impose le degré de
C     niveau d'eau comme condition de Dirichlet
C     sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL002_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL002_COD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl002.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl002.fc'

      INTEGER I, IN, INDEB, INFIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL002_TYP)
C-----------------------------------------------------------------------

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
D           CALL ERR_ASR(IN .LE. EG_CMMN_NNL)
            KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_DIRICHLET)
         ENDIF
      ENDDO

      SV2D_CL002_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL002_PRC
C
C Description:
C     La sous-routine privée SV2D_CL002_PRC impose le degré de
C     niveau d'eau comme condition de Dirichlet
C     sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL002_PRC(IL,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL002_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)

      INCLUDE 'sv2d_cl002.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl002.fc'

      INTEGER I, IC, IN, INDEB, INFIN, IV
      REAL*8  V, Z
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL002_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. 1)
      IV = KCLCND(3, IC)
      V  = VCLCNV(IV)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            !Z = VPRNO(SV2D_IPRNO_Z,IN) + SV2D_DECOU_HMIN
            !VDIMP(3,IN) = MAX(V, Z)
            VDIMP(3,IN) = V
         ENDIF
      ENDDO

      SV2D_CL002_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL002_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL002_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL002_REQHLP
C------------------------------------------------------------------------

C<comment>
C *****<br>
C This boundary condition has been DEPRECATED. Please use condition <b>103</b> instead.<br>
C *****
C <p>
C  Boundary condition of type <b>002</b>: <br>
C  Dirichlet condition on the water level.
C     <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 2</li>
C     <li>Values: h</li>
C     <li>Units: m</li>
C     <li>Example:  2  10.0</li>
C     </ul>
C</comment>

      SV2D_CL002_REQHLP = 'bc_type_002'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL002_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL002_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL002_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl002.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl002.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl002.hlp')

      SV2D_CL002_HLP = ERR_TYP()
      RETURN
      END
