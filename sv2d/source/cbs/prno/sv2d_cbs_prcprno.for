C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire : SV2D_CBS_PRCPRNO
C
C Description:
C     Pré-traitement des propriétés nodales. On force la topo linéaire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_PRCPRNO(VCORG,
     &                            KNGV,
     &                            VDJV,
     &                            VPRGL,
     &                            VPRNO,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PRCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IC, IE, IN
      INTEGER IPZ
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6

      REAL*8 EMIN
      PARAMETER (EMIN = 1.0D-15)
C-----------------------------------------------------------------------

      IPZ = SV2D_IPRNO_Z

!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(IC, IE, IN)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)

C---     Force la topo linéaire
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         VPRNO(IPZ,NO2) = (VPRNO(IPZ,NO1)+VPRNO(IPZ,NO3))*UN_2
         VPRNO(IPZ,NO4) = (VPRNO(IPZ,NO3)+VPRNO(IPZ,NO5))*UN_2
         VPRNO(IPZ,NO6) = (VPRNO(IPZ,NO5)+VPRNO(IPZ,NO1))*UN_2

      ENDDO
!$omp end do
      ENDDO

C---     Force le vent à 0 en présence de glace
!$omp  do
      DO IN=1,EG_CMMN_NNL

         IF (VPRNO(SV2D_IPRNO_ICE_E,IN) .GT. EMIN) THEN
            VPRNO(SV2D_IPRNO_WND_X,IN) = ZERO
            VPRNO(SV2D_IPRNO_WND_Y,IN) = ZERO
         ENDIF

      ENDDO
!$omp end do

      IERR = ERR_OMP_RDC()
!$omp end parallel

      IERR = ERR_TYP()
      RETURN
      END

