C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire : SV2D_CBS_PSLPRNO
C
C Description:
C     Traitement post-lecture des propriétés nodales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_PSLPRNO (VPRNO,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PSLPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      CALL SV2D_CBS_INI_IPRNO()

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SV2D_CBS_INI_IPRNO
C
C Description:
C     La fonction privée SV2D_CBS_INI_IPRNO initialise les indices
C     des propriétés nodales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_INI_IPRNO()

      IMPLICIT NONE

      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

      SV2D_IPRNO_Z          =  1
      SV2D_IPRNO_N          =  2
      SV2D_IPRNO_ICE_E      =  3
      SV2D_IPRNO_ICE_N      =  4
      SV2D_IPRNO_WND_X      =  5
      SV2D_IPRNO_WND_Y      =  6
      SV2D_IPRNO_U          =  7
      SV2D_IPRNO_V          =  8
      SV2D_IPRNO_H          =  9
      SV2D_IPRNO_COEFF_CNVT = 10
      SV2D_IPRNO_COEFF_GRVT = 11
      SV2D_IPRNO_COEFF_FROT = 12
      SV2D_IPRNO_COEFF_DIFF = 13
      SV2D_IPRNO_COEFF_PE   = -1
      SV2D_IPRNO_COEFF_DMPG = -1
      SV2D_IPRNO_COEFF_VENT = -1
      SV2D_IPRNO_COEFF_DRCY = 14
      SV2D_IPRNO_COEFF_PORO = 15
      SV2D_IPRNO_DECOU_PENA = -1

      RETURN
      END
