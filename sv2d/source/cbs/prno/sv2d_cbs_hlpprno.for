C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_HLPPRNO
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_HLPPRNO
C
C Description:
C     Aide sur les propriétés nodales
C                    CST    1)  Z fond
C                    CST    2)  MANNING NODAL
C                    CST    3)  EPAISSEUR DE LA GLACE
C                    CST    4)  MANNING GLACE
C                    CST    5)  COMPOSANTE X DU VENT
C                    CST    6)  COMPOSANTE Y DU VENT
C
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_HLPPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_HLPPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER I
      INTEGER SV2D_CBS_HLP1
C-----------------------------------------------------------------------

C---     Imprime l'entête
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRNO_ST_VENANT:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     PRNO lues
      I = SV2D_CBS_HLP1(SV2D_IPRNO_Z,    'MSG_ZF')
      I = SV2D_CBS_HLP1(SV2D_IPRNO_N,    'MSG_COEF_MANNING')
      I = SV2D_CBS_HLP1(SV2D_IPRNO_ICE_E,'MSG_GLACE_EPAISSEUR')
      I = SV2D_CBS_HLP1(SV2D_IPRNO_ICE_N,'MSG_GLACE_COEF_MANNING')
      I = SV2D_CBS_HLP1(SV2D_IPRNO_WND_X,'MSG_VENT_X')
      I = SV2D_CBS_HLP1(SV2D_IPRNO_WND_Y,'MSG_VENT_Y')

      CALL LOG_DECIND()

      RETURN
      END
