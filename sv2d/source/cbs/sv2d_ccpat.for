C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Notes:
C************************************************************************
C$NOREFERENCE

      MODULE SV2D_COMPAT_M
      
      USE SV2D_IPRG_M
      USE SV2D_IPRN_M
      USE SV2D_XPRG_M
      
      IMPLICIT NONE
      
      TYPE(SV2D_IPRG_T), STATIC :: SV2D_IPRG
      TYPE(SV2D_XPRG_T), STATIC :: SV2D_XPRG
      TYPE(SV2D_IPRN_T), STATIC :: SV2D_IPRN
      
      END MODULE SV2D_COMPAT_M
