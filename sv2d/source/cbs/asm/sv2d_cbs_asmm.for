C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_ASMM
C   Private:
C     SUBROUTINE SV2D_CBS_ASMM_V
C     SUBROUTINE SV2D_CBS_ASMME_V
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_ASMM
C
C Description:
C     ASSEMBLAGE DE LA MATRICE MASSE
C
C Entrée: VCORG,VDJ,VPRGL,VPRNO,VPREG,KNG,VDLG,JR,IAP,JAP
C
C Sortie: VKG
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMM (VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES, ! pas utilisé
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_ASMM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! sortie ou pas vraiment utilisé
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'

      INTEGER NNELT, NDLNMAX, NDLEMAX
      PARAMETER (NNELT=6)
      PARAMETER (NDLNMAX=3)
      PARAMETER (NDLEMAX=NNELT*NDLNMAX)

      REAL*8    VKE(NDLEMAX*NDLEMAX)
      INTEGER   KLOCE(NDLEMAX)
      INTEGER   IERR
C----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN  .LE. NDLNMAX)
D     CALL ERR_PRE(LM_CMMN_NDLEV .LE. NDLEMAX)
C-----------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(KLOCE, VKE)
         CALL SV2D_CBS_ASMM_V(KLOCE,
     &                        VKE,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VDLG,
     &                        HMTX,
     &                        F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF
      
C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_ASMM (KLOCE,
     &                        VKE,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        HMTX,
     &                        F_ASM)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMM_V
C
C Description:
C     ASSEMBLE LA MATRICE MASSE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMM_V(KLOCE,
     &                           VKE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES, ! pas utilisé
     &                           VDLG,
     &                           HMTX,
     &                           F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IC, IE, IN, II, NO
      REAL*8, POINTER :: VDLE(:, :)
      REAL*8, POINTER :: VPRN(:, :)
      INTEGER,POINTER :: KNE (:)

C---     Tables locales
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VDLE_LCL(NDLN_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VPRN_LCL(NPRN_LCL * NNEL_LCL)
      INTEGER, TARGET :: KNE_LCL (NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLN_LCL .GE. LM_CMMN_NDLN)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C---     Reshape the arrays
      VDLE(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VDLE_LCL
      VPRN(1:LM_CMMN_NPRNO,1:EG_CMMN_NNELV) => VPRN_LCL
      KNE (1:EG_CMMN_NNELV) => KNE_LCL

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
         KNE(:) = KNGV(:,IE)

C---        Transfert des valeurs nodales T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         VDLE (:,:) = VDLG (:,KNE(:))
!dir$ IVDEP, LOOP COUNT(18*6)
         VPRN (:,:) = VPRNO(:,KNE(:))

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO
         
C---        Calcul de la matrice élémentaire de volume
         CALL SV2D_CBS_ASMME_V(VKE,
     &                         VDJV(1,IE),
     &                         VPRGL,
     &                         VPRN,
     &                         VPREV(1,IE))

C---        Table KLOCE de l'élément T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         KLOCE(:,:) = KLOCN(:,KNE(:))

C---        Assemblage de la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_M_ELEM', ': ', IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMME_V
C
C Description:
C     La fonction SV2D_CBS_ASMME_V calcul la matrice masse élémentaire.
C     Les tables passées en paramètre sont des tables élémentaires.
C
C Entrée:
C     VDJE        Table du Jacobien Élémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales
C     VPRE        Table de PRopriétés Élémentaires
C
C Sortie:
C     VKE         Matrice élémentaire
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMME_V(VKE,
     &                            VDJE,
     &                            VPRG,
     &                            VPRN,
     &                            VPRE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
      REAL*8   VDJE (EG_CMMN_NDJV)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE (LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER KLOCET3(9, 4)
      INTEGER IT3
      INTEGER IKU1, IKV1, IKH1
      INTEGER IKU2, IKV2
      INTEGER IKU3, IKV3, IKH2
      INTEGER IKH3

      REAL*8  COEF, COEF2
      REAL*8  PORO1, PORO2, PORO3, POROS

      DATA KLOCET3/ 1, 2, 3,   4, 5, 6,  16,17,18,   ! NOEUDS 1,2,6,
     &              4, 5, 6,   7, 8, 9,  10,11,12,   ! NOEUDS 2,3,4,
     &             16,17,18,  10,11,12,  13,14,15,   ! NOEUDS 6,4,5,
     &             10,11,12,  16,17,18,   4, 5, 6/   ! NOEUDS 4,6,2
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPREV_D1 .GE. 2)     ! Au moins 2 POUR CHAQUE T3
D     CALL ERR_PRE(LM_CMMN_NPREV_D2 .EQ. 4)     ! 4 T3
C-----------------------------------------------------------------------

C---     Coefficient pour les sous-éléments
      COEF  = UN_24*UN_4*VDJE(5)
      COEF2 = COEF+COEF

C---        BOUCLE SUR LES SOUS-ÉLÉMENTS
C           ============================
      DO IT3=1,4

C---        Indice de DDL
         IKU1 = KLOCET3(1,IT3)
         IKV1 = KLOCET3(2,IT3)
         IKU2 = KLOCET3(4,IT3)
         IKV2 = KLOCET3(5,IT3)
         IKU3 = KLOCET3(7,IT3)
         IKV3 = KLOCET3(8,IT3)

C---        Assemblage de la sous-matrice U.U
         VKE(IKU1,IKU1) = VKE(IKU1,IKU1) + COEF2
         VKE(IKU2,IKU1) = VKE(IKU2,IKU1) + COEF
         VKE(IKU3,IKU1) = VKE(IKU3,IKU1) + COEF
         VKE(IKU1,IKU2) = VKE(IKU1,IKU2) + COEF
         VKE(IKU2,IKU2) = VKE(IKU2,IKU2) + COEF2
         VKE(IKU3,IKU2) = VKE(IKU3,IKU2) + COEF
         VKE(IKU1,IKU3) = VKE(IKU1,IKU3) + COEF
         VKE(IKU2,IKU3) = VKE(IKU2,IKU3) + COEF
         VKE(IKU3,IKU3) = VKE(IKU3,IKU3) + COEF2

C---        Assemblage de la sous-matrice V.V
         VKE(IKV1,IKV1) = VKE(IKV1,IKV1) + COEF2
         VKE(IKV2,IKV1) = VKE(IKV2,IKV1) + COEF
         VKE(IKV3,IKV1) = VKE(IKV3,IKV1) + COEF
         VKE(IKV1,IKV2) = VKE(IKV1,IKV2) + COEF
         VKE(IKV2,IKV2) = VKE(IKV2,IKV2) + COEF2
         VKE(IKV3,IKV2) = VKE(IKV3,IKV2) + COEF
         VKE(IKV1,IKV3) = VKE(IKV1,IKV3) + COEF
         VKE(IKV2,IKV3) = VKE(IKV2,IKV3) + COEF
         VKE(IKV3,IKV3) = VKE(IKV3,IKV3) + COEF2

      ENDDO

C---     Coefficients
!!      COEF = UN_24*VDJE(5)
!!      COEF2= COEF+COEF
      COEF = UN_120*VDJE(5)

C---     Porosité
      PORO1 = COEF*VPRN(SV2D_IPRNO_COEFF_PORO,1)
      PORO2 = COEF*VPRN(SV2D_IPRNO_COEFF_PORO,3)
      PORO3 = COEF*VPRN(SV2D_IPRNO_COEFF_PORO,5)
      POROS = PORO1 + PORO2 + PORO3

C---     Indice de DDL H
      IKH1 =  3   ! h sur les noeuds sommets
      IKH2 =  9
      IKH3 = 15

C---     Assemblage de la sous-matrice H.H
      VKE(IKH1,IKH1) = VKE(IKH1,IKH1) + DEUX*(POROS+PORO1+PORO1)
      VKE(IKH2,IKH1) = VKE(IKH2,IKH1) +      (POROS+PORO2+PORO1)
      VKE(IKH3,IKH1) = VKE(IKH3,IKH1) +      (POROS+PORO3+PORO1)
      VKE(IKH1,IKH2) = VKE(IKH1,IKH2) +      (POROS+PORO1+PORO2)
      VKE(IKH2,IKH2) = VKE(IKH2,IKH2) + DEUX*(POROS+PORO2+PORO2)
      VKE(IKH3,IKH2) = VKE(IKH3,IKH2) +      (POROS+PORO3+PORO2)
      VKE(IKH1,IKH3) = VKE(IKH1,IKH3) +      (POROS+PORO1+PORO3)
      VKE(IKH2,IKH3) = VKE(IKH2,IKH3) +      (POROS+PORO2+PORO3)
      VKE(IKH3,IKH3) = VKE(IKH3,IKH3) + DEUX*(POROS+PORO3+PORO3)
!      VKE(IKH1,IKH1) = VKE(IKH1,IKH1) + COEF2
!      VKE(IKH2,IKH1) = VKE(IKH2,IKH1) + COEF
!      VKE(IKH3,IKH1) = VKE(IKH3,IKH1) + COEF
!      VKE(IKH1,IKH2) = VKE(IKH1,IKH2) + COEF
!      VKE(IKH2,IKH2) = VKE(IKH2,IKH2) + COEF2
!      VKE(IKH3,IKH2) = VKE(IKH3,IKH2) + COEF
!      VKE(IKH1,IKH3) = VKE(IKH1,IKH3) + COEF
!      VKE(IKH2,IKH3) = VKE(IKH2,IKH3) + COEF
!      VKE(IKH3,IKH3) = VKE(IKH3,IKH3) + COEF2

      RETURN
      END
