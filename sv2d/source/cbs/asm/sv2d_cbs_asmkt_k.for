C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_ASMKT_K
C   Private:
C     SUBROUTINE SV2D_CBS_ASMKT_KV
C     SUBROUTINE SV2D_CBS_ASMKT_KS
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_ASMKT_K
C
C Description: ASSEMBLAGE DE LA MATRICE TANGENTE
C     La fonction SV2D_CBS_ASMKT_R calcule le matrice tangente
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C     Le calcul est fait via la matrice élémentaire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKT_K(VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_ASMKT_K
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'

      INTEGER, PARAMETER :: NNEL_LCL = 6
      INTEGER, PARAMETER :: NDLN_LCL = 3
      INTEGER, PARAMETER :: NDLE_LCL = NNEL_LCL*NDLN_LCL

      REAL*8   VKT1 (NDLE_LCL*NDLE_LCL)
      REAL*8   VKT2 (NDLE_LCL*NDLE_LCL)
      INTEGER  KLOCE(NDLE_LCL)
      INTEGER  IERR
C----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN  .LE. NDLN_LCL)
D     CALL ERR_PRE(LM_CMMN_NDLEV .LE. NDLE_LCL)
C-----------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKT1, VKT2, KLOCE)
         CALL SV2D_CBS_ASMKT_KV(KLOCE,
     &                          VKT1,
     &                          VKT2,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         CALL SV2D_CBS_ASMKT_KS(KLOCE,
     &                          VKT1,
     &                          VKT2,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
      ENDIF

C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_ASMKT(KLOCE,
     &                        VKT1,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        HMTX,
     &                        F_ASM)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMKT_KV
C
C Description:
C     La fonction SV2D_CBS_ASMKT_KV calcule la matrice tangente
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée: HMTX
C
C Sortie: KLOCE : Table de localisation élémentaire
C         VKE : Matrice élémentaire
C
C Notes:
C     1) La matrice tangente est donnée par (Gouri Dhatt p. 342):
C        Kt_ij = K_ij + Somme_sur_l ( (dK_il / du_j) * u_l )
C     avec:
C        dK_il/du_j ~= (K(u+delu_j) - K(u) ) / delu_j
C
C     2) La version d'Hydrosim est plus efficace. Elle calcule la partie
C     tangente comme:
C        dK_il/du_j ~= ([K(u+delu_j)]{u+delu_j} - [K(u)]{u} ) / delu_j
C
C     3) La perturbation d'un DDL en h sur un noeud sommet entraîne une
C     modification du DDL pour le noeud milieu dans le calcul des prop. nodales.
C     La valeur originale du noeud sommet est restaurée à la fin de la boucle
C     de perturbation. Le noeud milieu lui n'est restauré qu'au prochain
C     calcul de prop. nodales. Comme le dernier DDL perturbé est en v
C     (noeud 6), il y a implicitement restauration.
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKT_KV(KLOCE,
     &                             VKT,
     &                             VKT_TR,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLR,
     &                             VDLG,
     &                             HMTX,
     &                             F_ASM)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKT   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VKT_TR(LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
      INTEGER IC, IE, IN, ID, IDG
      INTEGER HVFT, HPRNE, HPREVE
      REAL*8  PENA
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      REAL*8, POINTER :: VCORE (:, :)
      REAL*8, POINTER :: VDLE  (:, :)
      REAL*8, POINTER :: VPRN  (:, :)
      REAL*8, POINTER :: VDJE  (:)
      REAL*8, POINTER :: VPRE  (:)
      REAL*8, POINTER :: VRESE (:)
      REAL*8, POINTER :: VRESEP(:)
      INTEGER,POINTER :: KNE   (:)

C---     Tables locales
      INTEGER, PARAMETER :: NDIM_LCL =  2
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL =  NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NDJE_LCL =  5
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NPRE_LCL =  8
      REAL*8, TARGET  :: VCORE_LCL (NDIM_LCL * NNEL_LCL)  ! rank one
      REAL*8, TARGET  :: VDLE_LCL  (NDLN_LCL * NNEL_LCL)
      REAL*8, TARGET  :: VPRN_LCL  (NPRN_LCL * NNEL_LCL)
      REAL*8, TARGET  :: VDJE_LCL  (NDJE_LCL)
      REAL*8, TARGET  :: VPRE_LCL  (NPRE_LCL)
      REAL*8, TARGET  :: VRESE_LCL (NDLE_LCL)
      REAL*8, TARGET  :: VRESEP_LCL(NDLE_LCL)
      INTEGER, TARGET :: KNE_LCL   (NNEL_LCL)

      BYTE, POINTER :: KNE_B  (:)
      BYTE, POINTER :: VCORE_B(:)
      BYTE, POINTER :: VDJE_B(:)
      BYTE, POINTER :: VPRG_B(:)
      BYTE, POINTER :: VPRN_B(:)
      BYTE, POINTER :: VDLE_B(:)
      BYTE, POINTER :: VPRE_B(:)
      BYTE, POINTER :: IERR_B (:)

      INTEGER, PARAMETER :: KNE_P(NNEL_LCL) = (/ 1, 2, 3, 4, 5, 6/)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDIM_LCL .EQ. EG_CMMN_NDIM)
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDJE_LCL .GE. EG_CMMN_NDJV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
D     CALL ERR_PRE(NPRE_LCL .GE. LM_CMMN_NPREV)
C-----------------------------------------------------------------------

C---     Récupère les données
      HVFT = 0    ! vtable par défaut
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPRNEV, HPRNE)
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPREVE, HPREVE)

C---     Reshape the arrays
      VCORE (1:EG_CMMN_NDIM, 1:EG_CMMN_NNELV) => VCORE_LCL
      VDLE  (1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VDLE_LCL
      VPRN  (1:LM_CMMN_NPRNO,1:EG_CMMN_NNELV) => VPRN_LCL
      VDJE  (1:EG_CMMN_NDJV)  => VDJE_LCL
      VPRE  (1:LM_CMMN_NPREV) => VPRE_LCL
      VRESE (1:LM_CMMN_NDLEV) => VRESE_LCL
      VRESEP(1:LM_CMMN_NDLEV) => VRESEP_LCL
      KNE   (1:EG_CMMN_NNELV) => KNE_LCL

C---     Pointeurs pour le calcul des propriétés perturbées
      KNE_B   => SO_ALLC_CST2B(KNE_P(:))
      VCORE_B => SO_ALLC_CST2B(VCORE(:,1))
      VDJE_B  => SO_ALLC_CST2B(VDJE (:))
      VDLE_B  => SO_ALLC_CST2B(VDLE (:,1))
      VPRG_B  => SO_ALLC_CST2B(VPRGL(:))
      VPRN_B  => SO_ALLC_CST2B(VPRN (:,1))
      VPRE_B  => SO_ALLC_CST2B(VPRE (:))
      IERR_B  => SO_ALLC_CST2B(IERR)
      
C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp  do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
!dir$ IVDEP, LOOP COUNT(6)
         KNE(:) = KNGV(:,IE)

C---        Table KLOCE de l'élément T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         KLOCE(:,:) = KLOCN(:,KNE(:))

C---        Transfert des valeurs nodales T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         VCORE(:,:) = VCORG(:,KNE(:))
!dir$ IVDEP, LOOP COUNT(3*6)
         VDLE (:,:) = VDLG (:,KNE(:))
!dir$ IVDEP, LOOP COUNT(18*6)
         VPRN (:,:) = VPRNO(:,KNE(:))

C---        Transfert des valeurs élémentaires
         VDJE(:) = VDJV (:,IE)
         VPRE(:) = VPREV(:,IE)

C---        Initialise la matrice élémentaire
         VKT_TR(:,:) = ZERO

C---        [K(u)]
         CALL SV2D_CBS_KE_V(VKT_TR,
     &                      VDJE,
     &                      VPRGL,
     &                      VPRN,
     &                      VPRE,
     &                      VDLE)

C---        [K(u)]{u}
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKT_TR,            ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLE,              ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VRESE,             ! y
     &              1)                 ! incy

C---        Initialise la matrice des dérivées
         VKT(:,:) = ZERO

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         IDG = 0
         DO IN=1,LM_CMMN_NNELV
         DO ID=1,LM_CMMN_NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL
            VDL_ORI = VDLE(ID,IN)
            VDL_DEL = SV2D_PNUMR_DELPRT * VDLE(ID,IN)
     &              + SIGN(SV2D_PNUMR_DELMIN, VDLE(ID,IN))
            VDL_INV = SV2D_PNUMR_OMEGAKT / VDL_DEL
            VDLE(ID,IN) = VDLE(ID,IN) + VDL_DEL

C---           Calcule les propriétés nodales perturbées
            IF (SV2D_PNUMR_PRTPRNO) THEN
               IERR = SO_FUNC_CALL4(HPRNE,
     &                              VPRG_B,
     &                              VPRN_B,
     &                              VDLE_B,
     &                              IERR_B)
            ENDIF

C---           Calcule les propriétés élémentaires perturbées
            IF (SV2D_PNUMR_PRTPREL) THEN
               IERR = SO_FUNC_CALL7(HPREVE,
     &                              VCORE_B,
     &                              VDJE_B,
     &                              VPRG_B,
     &                              VPRN_B,
     &                              VPRE_B,
     &                              VDLE_B,
     &                              IERR_B)
            ENDIF

C---           Initialise la matrice de travail
            VKT_TR(:,:) = ZERO

C---           [K(u + du_id)]
            CALL SV2D_CBS_KE_V(VKT_TR,
     &                         VDJE,
     &                         VPRGL,
     &                         VPRN,
     &                         VPRE,
     &                         VDLE)

C---           [K(u + du_id)]{u+du_id}
            CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &                 LM_CMMN_NDLEV,     ! m rows
     &                 LM_CMMN_NDLEV,     ! n columns
     &                 UN,                ! alpha
     &                 VKT_TR,            ! a
     &                 LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &                 VDLE,              ! x
     &                 1,                 ! incx
     &                 ZERO,              ! beta
     &                 VRESEP,            ! y
     &                 1)                 ! incy

C---           Restaure la valeur originale
            VDLE(ID,IN) = VDL_ORI

C---           ([K(u + du_id)]{u+du_id} - [K(u)]{u})/du
!dir$ IVDEP, LOOP COUNT(3*6)
            VKT(:,IDG) = (VRESEP(:) - VRESE(:)) * VDL_INV

199         CONTINUE
         ENDDO
         ENDDO

C---        Pénalisation en hh
         PENA = SV2D_PNUMR_PENALITE*VDJV(5,IE)
         VKT( 3, 3) = MAX(VKT( 3, 3), PENA)
         VKT( 9, 9) = MAX(VKT( 9, 9), PENA)
         VKT(15,15) = MAX(VKT(15,15), PENA)

C---        Assemble la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKT)
D        IF (ERR_BAD()) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMKT_KS
C
C Description:
C     La fonction SV2D_CBS_ASMKT_KS calcul la matrice tangente élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     c.f. SV2D_CBS_ASMKT_V
C
C     Le noeud opposé à l'élément de surface est perturbé en trop.
C     La matrice Ke peut être nulle.
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKT_KS(KLOCE,
     &                             VKT,
     &                             VKT_TR,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES, ! pas utilisé
     &                             VSOLR,
     &                             VDLG,
     &                             HMTX,
     &                             F_ASM)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKT   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VKT_TR(LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
      INTEGER IN, ID, IDG
      INTEGER IES, IEV, ICT
      INTEGER HVFT, HPRNE, HPREVE
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      REAL*8, POINTER :: VCORE (:, :)
      REAL*8, POINTER :: VDLEV (:, :)
      REAL*8, POINTER :: VPRNV (:, :)
      REAL*8, POINTER :: VDJEV (:)
      REAL*8, POINTER :: VPRE  (:)
      REAL*8, POINTER :: VRESE (:)
      REAL*8, POINTER :: VRESEP(:)
      INTEGER,POINTER :: KNEV  (:)

C---     Tables locales
      INTEGER, PARAMETER :: NDIM_LCL =  2
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDJE_LCL =  5
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL =  NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NPRE_LCL =  8
      REAL*8, TARGET  :: VCORE_LCL (NDIM_LCL * NNEL_LCL)  ! rank one
      REAL*8, TARGET  :: VDLEV_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8, TARGET  :: VPRNV_LCL (NPRN_LCL * NNEL_LCL)
      REAL*8, TARGET  :: VDJEV_LCL (NDJE_LCL)
      REAL*8, TARGET  :: VPRE_LCL  (NPRE_LCL)
      REAL*8, TARGET  :: VRESE_LCL (NDLE_LCL)
      REAL*8, TARGET  :: VRESEP_LCL(NDLE_LCL)
      INTEGER, TARGET :: KNEV_LCL  (NNEL_LCL)

      BYTE, POINTER :: KNE_B  (:)
      BYTE, POINTER :: VCORE_B(:)
      BYTE, POINTER :: VDJEV_B(:)
      BYTE, POINTER :: VPRGL_B(:)
      BYTE, POINTER :: VPRNV_B(:)
      BYTE, POINTER :: VDLEV_B(:)
      BYTE, POINTER :: VPRE_B(:)
      BYTE, POINTER :: IERR_B (:)

      INTEGER, PARAMETER :: KNE_P(NNEL_LCL) = ( / 1, 2, 3, 4, 5, 6/ )
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDIM_LCL .EQ. EG_CMMN_NDIM)
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
D     CALL ERR_PRE(NPRE_LCL .GE. LM_CMMN_NPREV)
C-----------------------------------------------------------------------

      HVFT = 0    ! vtable par défaut
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPRNEV, HPRNE)
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPREVE, HPREVE)

C---     Reshape the arrays
      VCORE (1:EG_CMMN_NDIM, 1:EG_CMMN_NNELV) => VCORE_LCL
      VDLEV (1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VDLEV_LCL
      VPRNV (1:LM_CMMN_NPRNO,1:EG_CMMN_NNELV) => VPRNV_LCL
      VDJEV (1:EG_CMMN_NDJV)  => VDJEV_LCL
      VPRE  (1:LM_CMMN_NPREV) => VPRE_LCL
      VRESE (1:LM_CMMN_NDLEV) => VRESE_LCL
      VRESEP(1:LM_CMMN_NDLEV) => VRESEP_LCL
      KNEV  (1:EG_CMMN_NNELV) => KNEV_LCL

C---     Byte pointeurs pour le calcul des propriétés perturbées
      KNE_B   => SO_ALLC_CST2B(KNE_P(:))
      VCORE_B => SO_ALLC_CST2B(VCORE(:,1))
      VDJEV_B => SO_ALLC_CST2B(VDJEV(:))
      VDLEV_B => SO_ALLC_CST2B(VDLEV(:,1))
      VPRGL_B => SO_ALLC_CST2B(VPRGL(:))
      VPRNV_B => SO_ALLC_CST2B(VPRNV(:,1))
      VPRE_B  => SO_ALLC_CST2B(VPRE (:))
      IERR_B  => SO_ALLC_CST2B(IERR)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO IES=1,EG_CMMN_NELS

C---        Élément parent et coté
         IEV = KNGS(4,IES)
         ICT = KNGS(5,IES)

C---        Transfert des connectivités de l'élément T6L
         KNEV(:) = KNGV(:,IEV)

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = KLOCN(:, KNEV(:))

C---        Transfert des valeurs nodales T6L
         VCORE(:,:) = VCORG(:,KNEV(:))
         VDLEV(:,:) = VDLG (:,KNEV(:))
         VPRNV(:,:) = VPRNO(:,KNEV(:))

C---        Transfert des valeurs élémentaires
         VDJEV(:) = VDJV (:,IEV)
         VPRE (:) = VPREV(:,IEV)

C---        Initialise la matrice élémentaire
         VKT_TR(:,:) = ZERO

C---        [K(u)]
         CALL SV2D_CBS_KE_S(VKT_TR,
     &                      VDJV(1,IEV),
     &                      VDJS(1,IES),
     &                      VPRGL,
     &                      VPRNV,
     &                      VPRE,
     &                      VPRES(1,IES),
     &                      VDLEV,
     &                      ICT)

C---        [K(u)]{u}
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKT_TR,            ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLEV,             ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VRESE,             ! y
     &              1)                 ! incy

C---        Initialise la matrice des dérivées
         VKT(:,:) = ZERO

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         IDG = 0
         DO IN=1,LM_CMMN_NNELV
         DO ID=1,LM_CMMN_NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL du T6L
            VDL_ORI = VDLEV(ID,IN)
            VDL_DEL = SV2D_PNUMR_DELPRT * VDLEV(ID,IN)
     &              + SIGN(SV2D_PNUMR_DELMIN, VDLEV(ID,IN))
            VDL_INV = SV2D_PNUMR_OMEGAKT / VDL_DEL
            VDLEV(ID,IN) = VDLEV(ID,IN) + VDL_DEL

C---           Calcule les propriétés nodales perturbées
            IF (SV2D_PNUMR_PRTPRNO) THEN
               IERR = SO_FUNC_CALL4(HPRNE,
     &                              VPRGL_B,
     &                              VPRNV_B,
     &                              VDLEV_B,
     &                              IERR_B)
            ENDIF

C---           Calcule les propriétés élémentaires perturbées
            IF (SV2D_PNUMR_PRTPREL) THEN
               IERR = SO_FUNC_CALL7(HPREVE,
     &                              VCORE_B,
     &                              VDJEV_B,
     &                              VPRGL_B,
     &                              VPRNV_B,
     &                              VPRE_B,
     &                              VDLEV_B,
     &                              IERR_B)
            ENDIF

C---           Initialise la matrice de travail
            VKT_TR(:,:) = ZERO

C---           [K(u + du_id)]
            CALL SV2D_CBS_KE_S(VKT_TR,
     &                         VDJV(1,IEV),
     &                         VDJS(1,IES),
     &                         VPRGL,
     &                         VPRNV,
     &                         VPRE,
     &                         VPRES(1,IES),
     &                         VDLEV,
     &                         ICT)

C---           [K(u + du_id)]{u+du_id}
            CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &                 LM_CMMN_NDLEV,     ! m rows
     &                 LM_CMMN_NDLEV,     ! n columns
     &                 UN,                ! alpha
     &                 VKT_TR,            ! a
     &                 LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &                 VDLEV,             ! x
     &                 1,                 ! incx
     &                 ZERO,              ! beta
     &                 VRESEP,            ! y
     &                 1)                 ! incy

C---           Restaure la valeur originale
            VDLEV(ID,IN) = VDL_ORI

C---           ([K(u + du_id)]{u+du_id} - [K(u)]{u})/du
            VKT(:,IDG) = (VRESEP(:) - VRESE(:)) * VDL_INV

199         CONTINUE
         ENDDO
         ENDDO

C---        Assemble la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKT)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      RETURN
      END

