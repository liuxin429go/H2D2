C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: SV2D
C      H2D2 Class: SV2D_CBS
C         FTN (Sub)Module: SV2D_CBS_RE_M
C            Public:
C            Private:
C               LOGICAL SV2D_CBS_RE_IPVALID
C               SUBROUTINE SV2D_CBS_FE_V
C               SUBROUTINE SV2D_CBS_RE_K
C               SUBROUTINE SV2D_CBS_RE_V
C               SUBROUTINE SV2D_CBS_RE_S
C
C************************************************************************

      MODULE SV2D_CBS_RE_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: SV2D_CBS_RE_IPVALID
C
C Description:
C     La fonction SV2D_CBS_RE_IPVALID contrôle la compatibilité entre
C     les indices de propriétés et le calcul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      LOGICAL FUNCTION SV2D_CBS_RE_IPVALID() RESULT(R)

      INCLUDE 'sv2d_cbs.fc'

      LOGICAL, SAVE      :: DONE = .FALSE.
      INTEGER, PARAMETER :: NOTUSED = -1
C-----------------------------------------------------------------------

      R = .TRUE.
      IF (DONE) RETURN

      R = (R .AND. (SV2D_IPRGL_GRAVITE        .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_LATITUDE       .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_FCT_CW_VENT    .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_CST      .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_LM       .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_SMGO     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_BINF     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_BSUP     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_HTRG     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_HMIN     .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_DECOU_PENA_H   .EQ. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_DECOU_PENA_Q   .EQ. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_MAN      .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_UMAX     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_PORO     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_AMORT    .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_CON_FACT .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_GRA_FACT .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_DIF_NU   .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_DIF_PE   .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_DRC_NU   .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_STABI_PECLET   .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_STABI_AMORT    .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_STABI_DARCY    .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_STABI_LAPIDUS  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_CON      .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_GRA      .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_CMULT_PDYN     .EQ. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_MAN      .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_VENT     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_INTGCTR  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_PENALITE .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_DELPRT   .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_DELMIN   .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_OMEGAKT  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_PRTPREL  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_PRTPRNO  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CORIOLIS       .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_VENT_REL .NE. NOTUSED))

      R = (R .AND. (SV2D_IPRNO_Z              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_N              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_ICE_E          .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_ICE_N          .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_WND_X          .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_WND_Y          .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_U              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_V              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_H              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_FROT     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_CNVT     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_GRVT     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_PE       .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_DMPG     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_DIFF     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_DRCY     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_VENT     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_PORO     .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRNO_DECOU_PENA     .EQ. NOTUSED))

      DONE = .TRUE.
      RETURN
      END FUNCTION SV2D_CBS_RE_IPVALID

C************************************************************************
C Sommaire: SV2D_CBS_FE_V
C
C Description:
C     La fonction SV2D_CBS_FE_V calcule le résidu élémentaire dû à un
C     élément de volume.
C     Les tables passées en paramètre sont des tables élémentaires.
C
C Entrée:
C     VDJE        Table du Jacobien Élémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales
C     VPRE        Table de PRopriétés Élémentaires
C     VDLE        Table de Degrés de Libertés Élémentaires
C     VRHS        Table du Membre de Droite
C
C Sortie:
C     VRES        Résidu élémentaire
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_FE_V(VRES,
     &                         VDJE,
     &                         VPRG,
     &                         VPRN,
     &                         VPRE,
     &                         VDLE,
     &                         VSLR)

      USE SV2D_CBS_CMP_RE_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN)    :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN)    :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VPRE(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8, INTENT(IN)    :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VSLR(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

C---        Sollicitations
      CALL SV2D_CMP_FE_V_SLR(VRES, VDJE, VPRG, VPRN, VPRE, VDLE, VSLR)

      RETURN
      END SUBROUTINE SV2D_CBS_FE_V

C************************************************************************
C Sommaire: SV2D_CBS_RE_K
C
C Description:
C     La fonction SV2D_CBS_RE_K calcule le résidu élémentaire dû à un
C     élément de volume.
C     Les tables passées en paramètre sont des tables élémentaires.
C
C Entrée:
C     VDJE        Table du Jacobien Élémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales
C     VPRE        Table de PRopriétés Élémentaires
C     VDLE        Table de Degrés de Libertés Élémentaires
C     VRHS        Table du Membre de Droite
C
C Sortie:
C     VRES        Résidu élémentaire
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_RE_K(VRES,
     &                         VDJE,
     &                         VPRG,
     &                         VPRN,
     &                         VPRE,
     &                         VDLE,
     &                         VRHS)
     
      USE SV2D_CBS_CMP_RE_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN)    :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN)    :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VPRE(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8, INTENT(IN)    :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  VDJT3 (5, 4)
      INTEGER IT3, ID
      LOGICAL APENA

      INTEGER, PARAMETER, DIMENSION(3, 4) :: KNET3 =
     &                 RESHAPE((/ 1, 2, 6,
     &                            2, 3, 4,
     &                            6, 4, 5,
     &                            4, 6, 2 /), (/3, 4/))
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPREV_D1 .GE. 2)     ! Au moins 2 POUR CHAQUE T3
D     CALL ERR_PRE(LM_CMMN_NPREV_D2 .EQ. 4)     ! 4 T3
C-----------------------------------------------------------------------

C---     Métriques des sous-élément
      DO IT3=1,4
         VDJT3(1,IT3) = UN_2*VDJE(1)
         VDJT3(2,IT3) = UN_2*VDJE(2)
         VDJT3(3,IT3) = UN_2*VDJE(3)
         VDJT3(4,IT3) = UN_2*VDJE(4)
         VDJT3(5,IT3) = UN_4*VDJE(5)
      ENDDO
      VDJT3(1:4,4) = -VDJT3(1:4,4)

C---     Status
      APENA = (SV2D_IPRNO_DECOU_PENA .GT. 0)
      APENA = .FALSE.

C---        Diffusion
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_DIF(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Manning
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_MAN_T6_T6(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
!!!            CALL SV2D_CMP_RE_V_MAN_T6_T6_T6(VRES,
!!!     &                             KNET3(:,IT3), VDJT3(:,IT3),
!!!     &                             VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Damping
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_DMP_T3(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Convection
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_CNV(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Vent
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_VNT(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Coriolis
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_COR(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Manning
!!!      CALL SV2D_CMP_RE_V_MAN_T6_T3(VRES,VDJE,VPRG,VPRN,VPRE,VDLE,VRHS)

C---        Gravité
      CALL SV2D_CMP_RE_V_GRV(VRES,VDJE,VPRG,VPRN,VPRE,VDLE,VRHS)

C---        Continuité
      CALL SV2D_CMP_RE_V_CNT(VRES,VDJE,VPRG,VPRN,VPRE,VDLE,VRHS)

C---        Damping
      CALL SV2D_CMP_RE_V_DMP_T6(VRES,VDJE,VPRG, VPRN,VPRE,VDLE,VRHS)

C---        Pénalisation
      IF (APENA)
     &   CALL SV2D_CMP_RE_K_PNA(VRES,VDJE,VPRG,VPRN,VPRE,VDLE,VRHS)

      RETURN
      END SUBROUTINE SV2D_CBS_RE_K

C************************************************************************
C Sommaire: SV2D_CBS_RE_V
C
C Description:
C     La fonction SV2D_CBS_ASMR_V calcule le résidu élémentaire dû à un
C     élément de volume.
C     Les tables passées en paramètre sont des tables élémentaires.
C
C Entrée:
C     VDJE        Table du Jacobien Élémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales
C     VPRE        Table de PRopriétés Élémentaires
C     VDLE        Table de Degrés de Libertés Élémentaires
C     VRHS        Table du Membre de Droite
C
C Sortie:
C     VRES        Résidu élémentaire
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_RE_V(VRES,
     &                         VDJE,
     &                         VPRG,
     &                         VPRN,
     &                         VPRE,
     &                         VDLE,
     &                         VRHS)

      USE SV2D_CBS_CMP_RE_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN)    :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN)    :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VPRE(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8, INTENT(IN)    :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  VDJT3 (5, 4)
      INTEGER IT3, ID
      LOGICAL APENA

      INTEGER, PARAMETER, DIMENSION(3, 4) :: KNET3 =
     &                 RESHAPE((/ 1, 2, 6,
     &                            2, 3, 4,
     &                            6, 4, 5,
     &                            4, 6, 2 /), (/3, 4/))
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPREV_D1 .GE. 2)     ! Au moins 2 POUR CHAQUE T3
D     CALL ERR_PRE(LM_CMMN_NPREV_D2 .EQ. 4)     ! 4 T3
C-----------------------------------------------------------------------

C---     Métriques des sous-élément
      DO IT3=1,4
         VDJT3(1,IT3) = UN_2*VDJE(1)
         VDJT3(2,IT3) = UN_2*VDJE(2)
         VDJT3(3,IT3) = UN_2*VDJE(3)
         VDJT3(4,IT3) = UN_2*VDJE(4)
         VDJT3(5,IT3) = UN_4*VDJE(5)
      ENDDO
      VDJT3(1:4,4) = -VDJT3(1:4,4)

C---     Status
      APENA = (SV2D_IPRNO_DECOU_PENA .GT. 0)
      APENA = .FALSE.

C---        Diffusion
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_DIF(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Manning
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_MAN_T6_T6(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
!!!            CALL SV2D_CMP_RE_V_MAN_T6_T6_T6(VRES,
!!!     &                             KNET3(:,IT3), VDJT3(:,IT3),
!!!     &                             VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Damping
      DO IT3=1,4
            CALL SV2D_CMP_RE_V_DMP_T3(VRES,
     &                             KNET3(:,IT3), VDJT3(:,IT3),
     &                             VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Convection
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_CNV(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Vent
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_VNT(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Coriolis
      DO IT3=1,4
         CALL SV2D_CMP_RE_V_COR(VRES,
     &                          KNET3(:,IT3), VDJT3(:,IT3),
     &                          VPRG, VPRN, VPRE(:,IT3), VDLE, VRHS)
      ENDDO

C---        Manning
!!!      CALL SV2D_CMP_RE_V_MAN_T6_T3(VRES,VDJE,VPRG,VPRN,VPRE,VDLE,VRHS)

C---        Gravité
      CALL SV2D_CMP_RE_V_GRV(VRES,VDJE,VPRG,VPRN,VPRE,VDLE,VRHS)

C---        Continuité
      CALL SV2D_CMP_RE_V_CNT(VRES,VDJE,VPRG,VPRN,VPRE,VDLE,VRHS)

C---        Damping
      CALL SV2D_CMP_RE_V_DMP_T6(VRES,VDJE,VPRG, VPRN,VPRE,VDLE,VRHS)

C---        Pénalisation
      IF (APENA)
     &   CALL SV2D_CMP_RE_V_PNA(VRES,VDJE,VPRG,VPRN,VPRE,VDLE,VRHS)

      RETURN
      END SUBROUTINE SV2D_CBS_RE_V

C************************************************************************
C Sommaire: SV2D_CBS_RE_S
C
C Description:
C     La fonction SV2D_CBS_ASMR_S calcule le matrice de rigidité
C     élémentaire due à un élément de surface.
C     Les tables passées en paramètre sont des tables élémentaires,
C     à part la table VDLG.
C
C Entrée:
C     VDJV        Table du Jacobien Élémentaire de l'élément de volume
C     VDJS        Table du Jacobien Élémentaire de l'élément de surface
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales de Volume
C     VPREV       Table de PRopriétés Élémentaires de Volume
C     VPRES       Table de PRopriétés Élémentaires de Surface
C     VDLE        Table de Degrés de Libertés Élémentaires de Volume
C     ICOTE       L'élément de surface forme le ième CÔTÉ du triangle
C
C Sortie:
C     VRES        Résidu élémentaire
C
C Notes:
C     On permute les métriques du T6L parent pour que le côté à traiter
C     soit le premier côté (noeuds 1-2-3).
C
C************************************************************************
      SUBROUTINE SV2D_CBS_RE_S(VRES,
     &                         VDJV,
     &                         VDJS,
     &                         VPRG,
     &                         VPRN,
     &                         VPREV,
     &                         VPRES,
     &                         VDLE,
     &                         VRHS,
     &                         ICOTE)

      USE SV2D_CBS_CMP_RE_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VDJV (EG_CMMN_NDJV)
      REAL*8, INTENT(IN)    :: VDJS (EG_CMMN_NDJS)
      REAL*8, INTENT(IN)    :: VPRG (LM_CMMN_NPRGL)
      REAL*8, INTENT(IN)    :: VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VPREV(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8, INTENT(IN)    :: VPRES(LM_CMMN_NPRES_D1, LM_CMMN_NPRES_D2)
      REAL*8, INTENT(IN)    :: VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN)    :: VRHS (LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER ICOTE

      INCLUDE 'err.fi'

      INTEGER IL2, IT3

      REAL*8  VDJX(4), VDJY(4)
      REAL*8  VDJT6(5), VDJT3(5)
      REAL*8  VDJL2(3)

      INTEGER, PARAMETER :: KNET6(3, 3) =
     &               RESHAPE((/ 1,3,5,         ! 3 noeuds sommets, 3 cotés
     &                          3,5,1,
     &                          5,1,3 /), (/3, 3/))
      INTEGER, PARAMETER :: KNET3(3, 2, 3) =
     &               RESHAPE((/ 1,2,6,  2,3,4, ! 3 noeuds, 2 sous-elem T3, 3 cotés
     &                          3,4,2,  4,5,6,
     &                          5,6,4,  6,1,2 /), (/3, 2, 3/))
      INTEGER, PARAMETER :: KT3  (2, 3) =
     &               RESHAPE((/ 1,2,           ! 2 sous-elem T3, 3 cotés
     &                          2,3,
     &                          3,1 /), (/2, 3/))
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPREV_D1 .GE. 2)     ! Au moins 2 POUR CHAQUE T3
D     CALL ERR_PRE(LM_CMMN_NPREV_D2 .EQ. 4)     ! 4 T3
D     CALL ERR_PRE(LM_CMMN_NPRES_D1 .GE. 3)     ! Au moins 3 POUR CHAQUE L2
D     CALL ERR_PRE(LM_CMMN_NPRES_D2 .EQ. 2)     ! 2 L2
D     CALL ERR_PRE(ICOTE .GE. 1 .AND. ICOTE .LE. 3)
C-----------------------------------------------------------------------

C---     Monte les tables aux. pour permuter les métriques
      VDJX(1) = VDJV(1)
      VDJX(2) = VDJV(2)
      VDJX(3) = -(VDJX(1)+VDJX(2))
      VDJX(4) = VDJX(1)
      VDJY(1) = VDJV(3)
      VDJY(2) = VDJV(4)
      VDJY(3) = -(VDJY(1)+VDJY(2))
      VDJY(4) = VDJY(1)

C---     Métriques permutées
      VDJT6(1) = VDJX(ICOTE)
      VDJT6(2) = VDJX(ICOTE+1)
      VDJT6(3) = VDJY(ICOTE)
      VDJT6(4) = VDJY(ICOTE+1)
      VDJT6(5) = VDJV(5)

C---     Métriques du sous-élément T3
      VDJT3(1) = UN_2*VDJT6(1)
      VDJT3(2) = UN_2*VDJT6(2)
      VDJT3(3) = UN_2*VDJT6(3)
      VDJT3(4) = UN_2*VDJT6(4)
      VDJT3(5) = UN_4*VDJT6(5)

C---     Métriques du sous-élément L2
      VDJL2(1) = VDJS(1)
      VDJL2(2) = VDJS(2)
      VDJL2(3) = UN_2*VDJS(3)

C---        BOUCLE SUR LES SOUS-ÉLÉMENTS
C           ============================
      DO IL2=1,2
         IT3 = KT3(IL2, ICOTE)

C---        Diffusion
         CALL SV2D_CMP_RE_S_DIF(VRES,
     &                          KNET3(:, IL2, ICOTE),
     &                          VDJT3,
     &                          VDJL2,
     &                          VPRG,
     &                          VPRN,
     &                          VPREV(:,IT3),
     &                          VPRES(:,IL2),
     &                          VDLE,
     &                          VRHS)

      ENDDO

C---        Continuité
      CALL SV2D_CMP_RE_S_CNT(VRES,
     &                       KNET6  (:, ICOTE),
     &                       VDJT6,
     &                       VDJS,
     &                       VPRG,
     &                       VPRN,
     &                       VPREV,
     &                       VPRES,
     &                       VDLE,
     &                       VRHS)

      RETURN
      END SUBROUTINE SV2D_CBS_RE_S

      END MODULE SV2D_CBS_RE_M
