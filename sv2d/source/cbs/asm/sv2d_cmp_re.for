C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: SV2D
C      H2D2 Class: SV2D_CBS
C         FTN (Sub)Module: SV2D_CBS_CMP_RE_M
C            Public:
C            Private:
C               SUBROUTINE SV2D_CMP_RE_K_PNA
C               SUBROUTINE SV2D_CMP_RE_V_PNA
C               SUBROUTINE SV2D_CMP_RE_V_CNT
C               SUBROUTINE SV2D_CMP_RE_V_GRV
C               SUBROUTINE SV2D_CMP_RE_V_CNV
C               SUBROUTINE SV2D_CMP_RE_V_COR
C               SUBROUTINE SV2D_CMP_RE_V_MAN_T6_T3
C               SUBROUTINE SV2D_CMP_RE_V_MAN_T6_T6
C               SUBROUTINE SV2D_CMP_RE_V_MAN_T6_T6_T6
C               SUBROUTINE SV2D_CMP_RE_V_DMP
C               SUBROUTINE SV2D_CMP_RE_V_VNT
C               SUBROUTINE SV2D_CMP_RE_V_DIF
C               SUBROUTINE SV2D_CMP_FE_V_SLR
C               SUBROUTINE SV2D_CMP_RE_S_DIF
C               SUBROUTINE SV2D_CMP_RE_S_CNT
C
C************************************************************************

      MODULE SV2D_CBS_CMP_RE_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: SV2D_CMP_RE_K_PNA
C
C Description:
C     La subroutine privée SV2D_CMP_RE_K_PNA assemble dans le
C     résidu Re la contribution de volume des termes de pénalisation.
C
C Entrée:
C     VDJE        Métrique de l'élément
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Version pour le calcul de K, qui ne prend en compte que la partie
C     des DDL
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_K_PNA(VRES,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO,EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  DJT6, DJT3
      REAL*8  U1, U2, U3, U4, U5, U6
      REAL*8  V1, V2, V3, V4, V5, V6
      REAL*8  H1, H3, H5
      REAL*8  W1, W2, W3, W4, W5, W6
      REAL*8  CU, CH

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Métriques du T6
      DJT6 = VDJE(5)
      DJT3 = UN_4 * DJT6

C---     Variables
      U1 = VRHS(1,NO1)
      V1 = VRHS(2,NO1)
      H1 = VRHS(3,NO1)
      U2 = VRHS(1,NO2)
      V2 = VRHS(2,NO2)
      U3 = VRHS(1,NO3)
      V3 = VRHS(2,NO3)
      H3 = VRHS(3,NO3)
      U4 = VRHS(1,NO4)
      V4 = VRHS(2,NO4)
      U5 = VRHS(1,NO5)
      V5 = VRHS(2,NO5)
      H5 = VRHS(3,NO5)
      U6 = VRHS(1,NO6)
      V6 = VRHS(2,NO6)

C---     Coefficients de pénalisation (Weak-Dirichlet lumped)
      CU = UN_6 * DJT3 * SV2D_DECOU_PENA_Q
      CH = UN_6 * DJT6 * SV2D_DECOU_PENA_H
      W1 = VPRN(SV2D_IPRNO_DECOU_PENA,NO1)
      W2 = VPRN(SV2D_IPRNO_DECOU_PENA,NO2) * 3
      W3 = VPRN(SV2D_IPRNO_DECOU_PENA,NO3)
      W4 = VPRN(SV2D_IPRNO_DECOU_PENA,NO4) * 3
      W5 = VPRN(SV2D_IPRNO_DECOU_PENA,NO5)
      W6 = VPRN(SV2D_IPRNO_DECOU_PENA,NO6) * 3

C---     Résidu
      VRES(1,NO1) = VRES(1,NO1) + CU * W1 * U1
      VRES(2,NO1) = VRES(2,NO1) + CU * W1 * V1
      VRES(3,NO1) = VRES(3,NO1) + CH * W1 * H1

      VRES(1,NO2) = VRES(1,NO2) + CU * W2 * U2
      VRES(2,NO2) = VRES(2,NO2) + CU * W2 * V2

      VRES(1,NO3) = VRES(1,NO3) + CU * W3 * U3
      VRES(2,NO3) = VRES(2,NO3) + CU * W3 * V3
      VRES(3,NO3) = VRES(3,NO3) + CH * W3 * H3

      VRES(1,NO4) = VRES(1,NO4) + CU * W4 * U4
      VRES(2,NO4) = VRES(2,NO4) + CU * W4 * V4

      VRES(1,NO5) = VRES(1,NO5) + CU * W5 * U5
      VRES(2,NO5) = VRES(2,NO5) + CU * W5 * V5
      VRES(3,NO5) = VRES(3,NO5) + CH * W5 * H5

      VRES(1,NO6) = VRES(1,NO6) + CU * W6 * U6
      VRES(2,NO6) = VRES(2,NO6) + CU * W6 * V6

      RETURN
      END SUBROUTINE SV2D_CMP_RE_K_PNA

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_PNA
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_PNA assemble dans le
C     résidu Re la contribution de volume des termes de pénalisation.
C
C Entrée:
C     VDJE        Métrique de l'élément
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Version pour le calcul du résidu
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_PNA(VRES,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO,EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  DJT6, DJT3
      REAL*8  D, DL, E, R
      REAL*8  P1, P2, P3, P4, P5, P6
      REAL*8  DU1, DU2, DU3, DU4, DU5, DU6
      REAL*8  DV1, DV2, DV3, DV4, DV5, DV6
      REAL*8  DH1, DH3, DH5
      REAL*8  W1, W2, W3, W4, W5, W6
      REAL*8  CU, CH

      REAL*8, PARAMETER :: SV2D_PNA_EXP = 2.0D0
      REAL*8, PARAMETER :: SV2D_PNA_LIN = 0.8D0

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------
C     CALL ERR_PRE(VDLE(1,1) .EQ. VRHS(1,1))
C-----------------------------------------------------------------------

C---     Métriques du T6
      DJT6 = VDJE(5)
      DJT3 = UN_4 * DJT6
      CU = UN_6 * DJT3 * SV2D_DECOU_PENA_Q
      CH = UN_6 * DJT6 * SV2D_DECOU_PENA_H

C---     Profondeur (h-zf)
      P1 = VDLE(3,NO1) - VPRN(SV2D_IPRNO_Z,NO1)
      P2 = VDLE(3,NO2) - VPRN(SV2D_IPRNO_Z,NO2)
      P3 = VDLE(3,NO3) - VPRN(SV2D_IPRNO_Z,NO3)
      P4 = VDLE(3,NO4) - VPRN(SV2D_IPRNO_Z,NO4)
      P5 = VDLE(3,NO5) - VPRN(SV2D_IPRNO_Z,NO5)
      P6 = VDLE(3,NO6) - VPRN(SV2D_IPRNO_Z,NO6)

C---     La limite de linéarité pour la rigidité
      DL = SV2D_PNA_LIN*(SV2D_DECOU_HTRG-SV2D_DECOU_HMIN) ! 80% de (Htrg-Hmin)

C---     Écarts
      D = SV2D_DECOU_HTRG - MIN(P1, SV2D_DECOU_HTRG)  ! Écart (positif)
      E = D / DL                                      ! Écart relatif vs la limite de linéarité
      R = MAX(E, E**SV2D_PNA_EXP)                     ! Rigidité relative
      D = P1 - SV2D_DECOU_HMIN
      DH1 = CH * R * D
      DU1 = CU * R * VDLE(1,NO1)
      DV1 = CU * R * VDLE(2,NO1)

      D = SV2D_DECOU_HTRG - MIN(P2, SV2D_DECOU_HTRG)  ! Écart (positif)
      E = D / DL                                      ! Écart relatif vs la limite de linéarité
      R = MAX(E, E**SV2D_PNA_EXP)                     ! Rigidité relative
      DU2 = CU * R * VDLE(1,NO2)
      DV2 = CU * R * VDLE(2,NO2)

      D = SV2D_DECOU_HTRG - MIN(P3, SV2D_DECOU_HTRG)  ! Écart (positif)
      E = D / DL                                      ! Écart relatif vs la limite de linéarité
      R = MAX(E, E**SV2D_PNA_EXP)                     ! Rigidité relative
      D = P3 - SV2D_DECOU_HMIN
      DH3 = CH * R * D
      DU3 = CU * R * VDLE(1,NO3)
      DV3 = CU * R * VDLE(2,NO3)

      D = SV2D_DECOU_HTRG - MIN(P4, SV2D_DECOU_HTRG)  ! Écart (positif)
      E = D / DL                                      ! Écart relatif vs la limite de linéarité
      R = MAX(E, E**SV2D_PNA_EXP)                     ! Rigidité relative
      DU4 = CU * R * VDLE(1,NO4)
      DV4 = CU * R * VDLE(2,NO4)

      D = SV2D_DECOU_HTRG - MIN(P5, SV2D_DECOU_HTRG)  ! Écart (positif)
      E = D / DL                                      ! Écart relatif vs la limite de linéarité
      R = MAX(E, E**SV2D_PNA_EXP)                     ! Rigidité relative
      D = P5 - SV2D_DECOU_HMIN
      DH5 = CH * R * D
      DU5 = CU * R * VDLE(1,NO5)
      DV5 = CU * R * VDLE(2,NO5)

      D = SV2D_DECOU_HTRG - MIN(P6, SV2D_DECOU_HTRG)  ! Écart (positif)
      E = D / DL                                      ! Écart relatif vs la limite de linéarité
      R = MAX(E, E**SV2D_PNA_EXP)                     ! Rigidité relative
      DU6 = CU * R * VDLE(1,NO6)
      DV6 = CU * R * VDLE(2,NO6)

C---     Coefficients de pénalisation (Weak-Dirichlet lumped)
      W1 = VPRN(SV2D_IPRNO_DECOU_PENA,NO1)
      W2 = VPRN(SV2D_IPRNO_DECOU_PENA,NO2) * 3
      W3 = VPRN(SV2D_IPRNO_DECOU_PENA,NO3)
      W4 = VPRN(SV2D_IPRNO_DECOU_PENA,NO4) * 3
      W5 = VPRN(SV2D_IPRNO_DECOU_PENA,NO5)
      W6 = VPRN(SV2D_IPRNO_DECOU_PENA,NO6) * 3

C---     Résidu
      VRES(1,NO1) = VRES(1,NO1) + W1 * DU1
      VRES(2,NO1) = VRES(2,NO1) + W1 * DV1
      VRES(3,NO1) = VRES(3,NO1) + W1 * DH1

      VRES(1,NO2) = VRES(1,NO2) + W2 * DU2
      VRES(2,NO2) = VRES(2,NO2) + W2 * DV2

      VRES(1,NO3) = VRES(1,NO3) + W3 * DU3
      VRES(2,NO3) = VRES(2,NO3) + W3 * DV3
      VRES(3,NO3) = VRES(3,NO3) + W3 * DH3

      VRES(1,NO4) = VRES(1,NO4) + W4 * DU4
      VRES(2,NO4) = VRES(2,NO4) + W4 * DV4

      VRES(1,NO5) = VRES(1,NO5) + W5 * DU5
      VRES(2,NO5) = VRES(2,NO5) + W5 * DV5
      VRES(3,NO5) = VRES(3,NO5) + W5 * DH5

      VRES(1,NO6) = VRES(1,NO6) + W6 * DU6
      VRES(2,NO6) = VRES(2,NO6) + W6 * DV6

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_PNA

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_CNT
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_CNT assemble dans le
C     résidu Re la contribution de volume de l'équation de
C     continuité. La divergence du débit est intégrée par partie.
C
C Entrée:
C     VDJE        Métrique de l'élément
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Les coefficients sont pour les métriques du T6
C        - <NT6,x> {NT3}      ! continuité
C        + <NT6,x> {NT6,x}    ! stabilisation Lapidus
C
C     Le calcul de la sous-matrice H-H (lissage) mène à des résultats 4 fois plus
C     grands que le calcul effectué par Hydrosim. Dans le calcul de la variable
C     CLISSEH dans Hydrosim, on multiplie le module calculé par un facteur DEMI
C     alors qu'on devrait retrouver un facteur DEUX. Cette erreur dans Hydrosim
C     explique l'écart observé entre les résultats de H2D2 et Hydrosim.
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_CNT(VRES,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO,EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  QX1, QX2, QX3, QX4, QX5, QX6
      REAL*8  QY1, QY2, QY3, QY4, QY5, QY6
      REAL*8  H1, H3, H5
      REAL*8  DHDX, DHDY, QXS, QYS, DHNRM
      REAL*8  C1, C3, C5, C, CM
      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  DETJ

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Métriques du T6
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)
      DETJ = VDJE(5)

C---     Degrés de liberté
      H1 = VDLE(3,NO1)
      H3 = VDLE(3,NO3)
      H5 = VDLE(3,NO5)

C---     Coefficients de Lapidus
      DHDX = VKX*(H3-H1) + VEX*(H5-H1)
      DHDY = VKY*(H3-H1) + VEY*(H5-H1)
      DHNRM = MAX(HYPOT(DHDX, DHDY), 1.0D-12)
      C = SV2D_STABI_LAPIDUS * DHNRM

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      H1  = VRHS(3,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)
      H3  = VRHS(3,NO3)
      QX4 = VRHS(1,NO4)
      QY4 = VRHS(2,NO4)
      QX5 = VRHS(1,NO5)
      QY5 = VRHS(2,NO5)
      H5  = VRHS(3,NO5)
      QX6 = VRHS(1,NO6)
      QY6 = VRHS(2,NO6)

C---     Dérivées
      DHDX = VKX*(H3-H1) + VEX*(H5-H1)
      DHDY = VKY*(H3-H1) + VEY*(H5-H1)

C---     Valeurs élémentaires
      QXS = (QX1+QX2+QX6)+(QX2+QX3+QX4)+(QX6+QX4+QX5)+(QX4+QX6+QX2)
      QYS = (QY1+QY2+QY6)+(QY2+QY3+QY4)+(QY6+QY4+QY5)+(QY4+QY6+QY2)

C---     Coefficients de Darcy
      C1 = VPRN(SV2D_IPRNO_COEFF_DRCY,NO1)
      C3 = VPRN(SV2D_IPRNO_COEFF_DRCY,NO3)
      C5 = VPRN(SV2D_IPRNO_COEFF_DRCY,NO5)
      CM = UN_3*(C1 + C3 + C5)
      C = (C + CM) * UN_2/DETJ

C---     Résidu
      VRES(3,NO1) = VRES(3,NO1) - UN_24*(VSX*QXS + VSY*QYS)
     &                          + C*(VSX*DHDX + VSY*DHDY)
      VRES(3,NO3) = VRES(3,NO3) - UN_24*(VKX*QXS + VKY*QYS)
     &                          + C*(VKX*DHDX + VKY*DHDY)
      VRES(3,NO5) = VRES(3,NO5) - UN_24*(VEX*QXS + VEY*QYS)
     &                          + C*(VEX*DHDX + VEY*DHDY)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_CNT

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_GRV
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_GRV assemble dans le
C     résidu Re la contribution de volume des termes de gravité
C     des équations de mouvement pour un élément T6L.
C
C Entrée:
C     VDJE        Métrique de l'élément T6
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Le résidu est :
C        {N} < N1 ; N2 ; N3 > {H} < N1,x ; N2,x ; N3,x > {h}
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_GRV(VRES,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  H1, H3, H5, DHDX, DHDY
      REAL*8  P1, P2, P3, P4, P5, P6
      REAL*8  PE1, PE2, PE3, PE4
      REAL*8  VKX, VEX
      REAL*8  VKY, VEY
      REAL*8  CQH

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Métriques du T6L
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      CQH = SV2D_GRAVITE * UN_96

C---     Inconnues
      H1 = VRHS(3,NO1)
      H3 = VRHS(3,NO3)
      H5 = VRHS(3,NO5)

C---     Dérivées
      DHDX = CQH * (VKX*(H3-H1) + VEX*(H5-H1))
      DHDY = CQH * (VKY*(H3-H1) + VEY*(H5-H1))

C---     Profondeur nodales
      P1 = VPRN(SV2D_IPRNO_H,NO1) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO1)   ! Prof absolue * lmtr de gravité
      P2 = VPRN(SV2D_IPRNO_H,NO2) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO2)
      P3 = VPRN(SV2D_IPRNO_H,NO3) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO3)
      P4 = VPRN(SV2D_IPRNO_H,NO4) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO4)
      P5 = VPRN(SV2D_IPRNO_H,NO5) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO5)
      P6 = VPRN(SV2D_IPRNO_H,NO6) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO6)

C---     Profondeurs élémentaires
      PE1 = P1 + P2 + P6
      PE2 = P2 + P3 + P4
      PE3 = P6 + P4 + P5
      PE4 = P4 + P6 + P2

C!!!!!!!!!!!!! Ajouter la pression dynamique (SV2D_CMULT_PDYN) !!!!!!!!!!!!!!!!1

C---     Assemblage
      VRES(1,NO1) = VRES(1,NO1) + (PE1+P1)*DHDX
      VRES(2,NO1) = VRES(2,NO1) + (PE1+P1)*DHDY
      VRES(1,NO2) = VRES(1,NO2) + (PE1+P2 + PE2+P2 + PE4+P2)*DHDX
      VRES(2,NO2) = VRES(2,NO2) + (PE1+P2 + PE2+P2 + PE4+P2)*DHDY
      VRES(1,NO3) = VRES(1,NO3) + (PE2+P3)*DHDX
      VRES(2,NO3) = VRES(2,NO3) + (PE2+P3)*DHDY
      VRES(1,NO4) = VRES(1,NO4) + (PE2+P4 + PE3+P4 + PE4+P4)*DHDX
      VRES(2,NO4) = VRES(2,NO4) + (PE2+P4 + PE3+P4 + PE4+P4)*DHDY
      VRES(1,NO5) = VRES(1,NO5) + (PE3+P5)*DHDX
      VRES(2,NO5) = VRES(2,NO5) + (PE3+P5)*DHDY
      VRES(1,NO6) = VRES(1,NO6) + (PE3+P6 + PE4+P6 + PE1+P6)*DHDX
      VRES(2,NO6) = VRES(2,NO6) + (PE3+P6 + PE4+P6 + PE1+P6)*DHDY

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_GRV

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_CNV
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_CNV assemble dans le
C     résidu Re la contribution de volume des termes de convection
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     VDJE        Métrique de l'élément T3
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Le résidu :
C        {N} < N1,x ; N2,x ; N3,x > {U Qx}
C     développé donne
C        {N} ( N1,x*U1*Qx1 + N2,x*U2*Qx2 + N3,x*u3*Qx3)
C     qui est écrit ensuite sous la forme
C        {N} < U1*N1,x ; U2*N2,x ; U3*N3,x > {Qx}
C
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_CNV(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER,INTENT(IN) :: KNE (3)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  U1, U2, U3, V1, V2, V3
      REAL*8  DUQXDX, DVQXDY
      REAL*8  DUQYDX, DVQYDY
      REAL*8  CNVX, CNVY
      REAL*8  VKX, VEX
      REAL*8  VKY, VEY
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Métriques
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Propriétés nodales
      U1 = VPRN(SV2D_IPRNO_U,NO1) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO1)   ! u * limiteur de convection
      V1 = VPRN(SV2D_IPRNO_V,NO1) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO1)   !
      U2 = VPRN(SV2D_IPRNO_U,NO2) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO2)
      V2 = VPRN(SV2D_IPRNO_V,NO2) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO2)
      U3 = VPRN(SV2D_IPRNO_U,NO3) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO3)
      V3 = VPRN(SV2D_IPRNO_V,NO3) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO3)

C---     d/dx (u.qx), d/dy (v.qx)
      DUQXDX = VKX*(U2*QX2-U1*QX1) + VEX*(U3*QX3-U1*QX1)
      DVQXDY = VKY*(V2*QX2-V1*QX1) + VEY*(V3*QX3-V1*QX1)
      DUQYDX = VKX*(U2*QY2-U1*QY1) + VEX*(U3*QY3-U1*QY1)
      DVQYDY = VKY*(V2*QY2-V1*QY1) + VEY*(V3*QY3-V1*QY1)

C---     Termes de convection
      CNVX = UN_6*(DUQXDX + DVQXDY)
      CNVY = UN_6*(DUQYDX + DVQYDY)

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + CNVX
      VRES(2,NO1) = VRES(2,NO1) + CNVY

      VRES(1,NO2) = VRES(1,NO2) + CNVX
      VRES(2,NO2) = VRES(2,NO2) + CNVY

      VRES(1,NO3) = VRES(1,NO3) + CNVX
      VRES(2,NO3) = VRES(2,NO3) + CNVY

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_CNV

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_COR
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_COR assemble dans le
C     résidu Re la contribution de volume des termes de Coriolis
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     VDJE        Métrique de l'élément T3
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     La matrice lumped est trop dissipative
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_COR(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER,INTENT(IN) :: KNE (3)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  CX, CY
      REAL*8  DETJ
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Métriques
      DETJ = VDJE(5)

C---     Coefficients de Coriolis
      CY = UN_24*SV2D_CORIOLIS*DETJ
      CX = -CY

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + CX*(QY1+QY1 + QY2 + QY3)
      VRES(2,NO1) = VRES(2,NO1) + CY*(QX1+QX1 + QX2 + QX3)

      VRES(1,NO2) = VRES(1,NO2) + CX*(QY1 + QY2+QY2 + QY3)
      VRES(2,NO2) = VRES(2,NO2) + CY*(QX1 + QX2+QX2 + QX3)

      VRES(1,NO3) = VRES(1,NO3) + CX*(QY1 + QY2 + QY3+QY3)
      VRES(2,NO3) = VRES(2,NO3) + CY*(QX1 + QX2 + QX3+QX3)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_COR

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_MAN_T6_T3
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_MAN_T6_T3 assemble dans le
C     résidu Re la contribution de volume des termes de frottement de Manning
C     des équations de mouvement pour un sous-élément T3.
C     Produit factorisé de la contrainte au complet, qui est approximée P1.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     VDJE        Métrique de l'élément T3
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_MAN_T6_T3(
     &                             VRES,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO,EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  VFROT1, VFROT3, VFROT5
      REAL*8  QX1, QX3, QX5, QY1, QY3, QY5
      REAL*8  FX1, FX3, FX5, FY1, FY3, FY5
      REAL*8  DETJ, COEF_HY

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Déterminant
      DETJ = VDJE(5)

C---     Coefficient
      COEF_HY = UN_48*DETJ

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)
      QX5 = VRHS(1,NO5)
      QY5 = VRHS(2,NO5)

C---     Coefficients de frottements
      VFROT1 = COEF_HY * VPRN(SV2D_IPRNO_COEFF_FROT, NO1)
      VFROT3 = COEF_HY * VPRN(SV2D_IPRNO_COEFF_FROT, NO3)
      VFROT5 = COEF_HY * VPRN(SV2D_IPRNO_COEFF_FROT, NO5)

C---     Termes de frottement
      FX1 = VFROT1*QX1
      FX3 = VFROT3*QX3
      FX5 = VFROT5*QX5
      FY1 = VFROT1*QY1
      FY3 = VFROT3*QY3
      FY5 = VFROT5*QY5

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + ( 6*FX1 +    FX3 +    FX5)
      VRES(2,NO1) = VRES(2,NO1) + ( 6*FY1 +    FY3 +    FY5)

      VRES(1,NO2) = VRES(1,NO2) + (10*FX1 + 10*FX3 +  4*FX5)
      VRES(2,NO2) = VRES(2,NO2) + (10*FY1 + 10*FY3 +  4*FY5)

      VRES(1,NO3) = VRES(1,NO3) + (   FX1 +  6*FX3 +    FX5)
      VRES(2,NO3) = VRES(2,NO3) + (   FY1 +  6*FY3 +    FY5)

      VRES(1,NO4) = VRES(1,NO4) + ( 4*FX1 + 10*FX3 + 10*FX5)
      VRES(2,NO4) = VRES(2,NO4) + ( 4*FY1 + 10*FY3 + 10*FY5)

      VRES(1,NO5) = VRES(1,NO5) + (   FX1 +    FX3 +  6*FX5)
      VRES(2,NO5) = VRES(2,NO5) + (   FY1 +    FY3 +  6*FY5)

      VRES(1,NO6) = VRES(1,NO6) + (10*FX1 +  4*FX3 + 10*FX5)
      VRES(2,NO6) = VRES(2,NO6) + (10*FY1 +  4*FY3 + 10*FY5)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_MAN_T6_T3

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_MAN_T6_T6
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_MAN_T6_T6 assemble dans le
C     résidu Re la contribution de volume des termes de frottement de Manning
C     des équations de mouvement pour un sous-élément T3.
C     Produit factorisé de la contrainte au complet, qui est approximée P1-isoP2.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     VDJE        Métrique de l'élément T3
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_MAN_T6_T6(
     &                             VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER,INTENT(IN) :: KNE (3)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  VFROT1, VFROT2, VFROT3
      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  FX1, FX2, FX3, FY1, FY2, FY3
      REAL*8  DETJ, COEF_HY

      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Déterminant
      DETJ = VDJE(5)

C---     Coefficient
      COEF_HY = UN_24*DETJ

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Coefficients de frottements
      VFROT1 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO1)
      VFROT2 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO2)
      VFROT3 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO3)

C---     Termes de frottement
      FX1 = VFROT1*QX1
      FX2 = VFROT2*QX2
      FX3 = VFROT3*QX3
      FY1 = VFROT1*QY1
      FY2 = VFROT2*QY2
      FY3 = VFROT3*QY3

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + (FX1+FX1 + FX2 + FX3)
      VRES(2,NO1) = VRES(2,NO1) + (FY1+FY1 + FY2 + FY3)

      VRES(1,NO2) = VRES(1,NO2) + (FX1 + FX2+FX2 + FX3)
      VRES(2,NO2) = VRES(2,NO2) + (FY1 + FY2+FY2 + FY3)

      VRES(1,NO3) = VRES(1,NO3) + (FX1 + FX2 + FX3+FX3)
      VRES(2,NO3) = VRES(2,NO3) + (FY1 + FY2 + FY3+FY3)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_MAN_T6_T6

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_MAN_T6_T6_T6
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_MAN_T6_T6_T6 assemble dans le
C     résidu Re la contribution de volume des termes de frottement de Manning
C     des équations de mouvement pour un sous-élément T3.
C     Version avec:
C        1) une interpolation P1-isoP2 pour la partie calculée
C           dans les PRNO ( n^2 / (H^(4/3) * |u|) ,
C        2) et une interpolation P1-isoP2 pour le débit
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Fichier maxima/Manning/T6_T3.wxmx
C     /* Produits {N_T6L}<N_T6L><n><N_T6L>{u} - en numérotation locale au sous-élément*/
C     Ici 'n' représente la partie qui provient des PRNO.
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_MAN_T6_T6_T6(
     &                                VRES,
     &                                KNE,
     &                                VDJE,
     &                                VPRG,
     &                                VPRN,
     &                                VPRE,
     &                                VDLE,
     &                                VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER,INTENT(IN) :: KNE (3)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  F1, F2, F3, FS2
      REAL*8  C11, C12, C13, C22, C23, C33
      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  DETJ, COEF_HY

      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Déterminant
      DETJ = VDJE(5)

C---     Coefficient
      COEF_HY = UN_120*DETJ

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Coefficients de frottements
      F1 = COEF_HY * VPRN(SV2D_IPRNO_COEFF_FROT, NO1)
      F2 = COEF_HY * VPRN(SV2D_IPRNO_COEFF_FROT, NO2)
      F3 = COEF_HY * VPRN(SV2D_IPRNO_COEFF_FROT, NO3)
      FS2= 2*(F1 + F2 + F3)

C---     Termes de frottement
      C11 = FS2 + 4*F1
      C22 = FS2 + 4*F2
      C33 = FS2 + 4*F3
      C12 = FS2 - F3
      C13 = FS2 - F2
      C23 = FS2 - F1

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + (C11*QX1 + C12*QX2 + C13*QX3)
      VRES(2,NO1) = VRES(2,NO1) + (C11*QY1 + C12*QY2 + C13*QY3)
      VRES(1,NO2) = VRES(1,NO2) + (C12*QX1 + C22*QX2 + C23*QX3)
      VRES(2,NO2) = VRES(2,NO2) + (C12*QY1 + C22*QY2 + C23*QY3)
      VRES(1,NO3) = VRES(1,NO3) + (C13*QX1 + C23*QX2 + C33*QX3)
      VRES(2,NO3) = VRES(2,NO3) + (C13*QY1 + C23*QY2 + C33*QY3)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_MAN_T6_T6_T6

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_DMP_T3
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_DMP_T3 assemble dans le
C     résidu Re la contribution de volume des termes de frottement linéaire
C     (amortissement).
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     VDJE        Métrique de l'élément T3
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_DMP_T3(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER,INTENT(IN) :: KNE (3)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8 C1, C2, C3
      REAL*8 QX1, QX2, QX3, QXM
      REAL*8 QY1, QY2, QY3, QYM
      REAL*8 COEF

      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Coefficient matrice lumped
      COEF = UN_6*VDJE(5)

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)
      QXM = UN_3*(QX1 + QX2 + QX3)
      QYM = UN_3*(QY1 + QY2 + QY3)

C---     Coefficients de damping
      C1 = COEF * VPRN(SV2D_IPRNO_COEFF_DMPG, NO1)
      C2 = COEF * VPRN(SV2D_IPRNO_COEFF_DMPG, NO2)
      C3 = COEF * VPRN(SV2D_IPRNO_COEFF_DMPG, NO3)

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + C1*(QX1 - QXM)
      VRES(2,NO1) = VRES(2,NO1) + C1*(QY1 - QYM)

      VRES(1,NO2) = VRES(1,NO2) + C2*(QX2 - QXM)
      VRES(2,NO2) = VRES(2,NO2) + C2*(QY2 - QYM)

      VRES(1,NO3) = VRES(1,NO3) + C3*(QX3 - QXM)
      VRES(2,NO3) = VRES(2,NO3) + C3*(QY3 - QYM)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_DMP_T3

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_DMP_T6
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_DMP_T6 assemble dans le
C     résidu Re la contribution de volume des termes de frottement linéaire
C     (amortissement).
C
C Entrée:
C     VDJE        Métrique de l'élément T6L
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6L
C     VPRE        Propriétés élémentaires du T6L
C     VDLE        Degrés de liberté du T6L
C
C Sortie:
C     VRES
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_DMP_T6(VRES,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8, DIMENSION(:) :: C(6)
      REAL*8  QXM, QYM, UM, VM
      REAL*8  COEF
      INTEGER IPH, IPU, IPV
      LOGICAL ISDRY1, ISDRY3, ISDRY5
      
C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------
      LOGICAL IS_DRY
      REAL*8 P
      IS_DRY(P) = (ABS(P - SV2D_DECOU_HMIN) .LE. 1.0D-12)
C-----------------------------------------------------------------------

!!! C---     Coefficient matrice lumped
!!!       COEF = UN_6*VDJE(5)
!!! 
!!! C---     Inconnues
!!!       QXM = UN_6*SUM(VRHS(1,:))
!!!       QYM = UN_3*SUM(VRHS(2,:))
!!! 
!!! C---     Coefficients de damping
!!!       C(:) = COEF * VPRN(SV2D_IPRNO_COEFF_DMPG,:)
!!! 
!!! C---     Assemblage de VRES
!!!       VRES(1,:) = VRES(1,:) + C(:)*(VRHS(1,:) - QXM)
!!!       VRES(2,:) = VRES(2,:) + C(:)*(VRHS(2,:) - QYM)

C==============================================================================      
C==============================================================================      
C---     PATCH d'amortissement des noeuds milieux
C        La vitesse des noeuds milieux est "amortie" vers la moyenne de l'arête

      IPH = SV2D_IPRNO_H
      IPU = SV2D_IPRNO_U
      IPV = SV2D_IPRNO_V

C---     Coefficient
      COEF = SV2D_DECOU_AMORT * UN_6 * VDJE(5)    ! Squat CQH

C---     État des noeuds sommets
      ISDRY1 = IS_DRY( VPRN(IPH, NO1) )
      ISDRY3 = IS_DRY( VPRN(IPH, NO3) )
      ISDRY5 = IS_DRY( VPRN(IPH, NO5) )

C---     Assemblage      
      IF (ISDRY1 .OR. ISDRY3) THEN
         UM = UN_2 * (VPRN(IPU,NO1) + VPRN(IPU,NO3))
         VM = UN_2 * (VPRN(IPV,NO1) + VPRN(IPV,NO3))
         VRES(1,NO2) = VRES(1,NO2) + COEF*(VPRN(IPU,NO2) - UM)
         VRES(2,NO2) = VRES(2,NO2) + COEF*(VPRN(IPV,NO2) - VM)
      ENDIF
      IF (ISDRY3 .OR. ISDRY5) THEN
         UM = UN_2 * (VPRN(IPU,NO3) + VPRN(IPU,NO5))
         VM = UN_2 * (VPRN(IPV,NO3) + VPRN(IPV,NO5))
         VRES(1,NO4) = VRES(1,NO4) + COEF*(VPRN(IPU,NO4) - UM)
         VRES(2,NO4) = VRES(2,NO4) + COEF*(VPRN(IPV,NO4) - VM)
      ENDIF
      IF (ISDRY5 .OR. ISDRY1) THEN
         UM = UN_2 * (VPRN(IPU,NO5) + VPRN(IPU,NO1))
         VM = UN_2 * (VPRN(IPV,NO5) + VPRN(IPV,NO1))
         VRES(1,NO6) = VRES(1,NO6) + COEF*(VPRN(IPU,NO6) - UM)
         VRES(2,NO6) = VRES(2,NO6) + COEF*(VPRN(IPV,NO6) - VM)
      ENDIF
C==============================================================================      
C==============================================================================      
      
      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_DMP_T6

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_VNT
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_VNT assemble dans le
C     résidu Re la contribution de volume des termes de vent
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     VDJE        Métrique de l'élément T3
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Cf. Manning
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_VNT(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER,INTENT(IN) :: KNE (3)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  VVENT1, VVENT2, VVENT3
      REAL*8  WX1, WX2, WX3, WY1, WY2, WY3
      REAL*8  VX1, VX2, VX3, VY1, VY2, VY3
      REAL*8  DETJ, COEF, CREL

      INTEGER IPH, IPX, IPY
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

      IPH = SV2D_IPRNO_H
      IPX = SV2D_IPRNO_WND_X
      IPY = SV2D_IPRNO_WND_Y

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Déterminant
      DETJ = VDJE(5)

C---     Coefficient
      COEF = UN_24*DETJ
      CREL = SV2D_CMULT_VENT_REL

C---     Coefficients de vent
      VVENT1 = COEF*VPRN(SV2D_IPRNO_COEFF_VENT,NO1)            ! cw |w| rho_air/rho_eau
      VVENT2 = COEF*VPRN(SV2D_IPRNO_COEFF_VENT,NO2)
      VVENT3 = COEF*VPRN(SV2D_IPRNO_COEFF_VENT,NO3)

C---     Inconnues (vent relatif)
      WX1 = VPRN(IPX,NO1) - CREL*VRHS(1,NO1)/VPRN(IPH,NO1)     ! wx - qx/h
      WY1 = VPRN(IPY,NO1) - CREL*VRHS(2,NO1)/VPRN(IPH,NO1)     ! Recalculé car issu de VRHS
      WX2 = VPRN(IPX,NO2) - CREL*VRHS(1,NO2)/VPRN(IPH,NO2)
      WY2 = VPRN(IPY,NO2) - CREL*VRHS(2,NO2)/VPRN(IPH,NO2)
      WX3 = VPRN(IPX,NO3) - CREL*VRHS(1,NO3)/VPRN(IPH,NO3)
      WY3 = VPRN(IPY,NO3) - CREL*VRHS(2,NO3)/VPRN(IPH,NO3)

C---     Termes de vent
      VX1 = VVENT1*WX1
      VX2 = VVENT2*WX2
      VX3 = VVENT3*WX3
      VY1 = VVENT1*WY1
      VY2 = VVENT2*WY2
      VY3 = VVENT3*WY3

C---     Assemblage
      VRES(1,NO1) = VRES(1,NO1) - (VX1+VX1 + VX2 + VX3)
      VRES(2,NO1) = VRES(2,NO1) - (VY1+VY1 + VY2 + VY3)

      VRES(1,NO2) = VRES(1,NO2) - (VX1 + VX2+VX2 + VX3)
      VRES(2,NO2) = VRES(2,NO2) - (VY1 + VY2+VY2 + VY3)

      VRES(1,NO3) = VRES(1,NO3) - (VX1 + VX2 + VX3+VX3)
      VRES(2,NO3) = VRES(2,NO3) - (VY1 + VY2 + VY3+VY3)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_VNT

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_DIF
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_DIF assemble dans le
C     résidu Re la contribution de volume des termes de frottement de
C     diffusion des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     VDJE        Métrique de l'élément T3
C     VPRG        Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T3
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     {N,x} H_nu TXX
C     Le produit H_nu est la moyenne sur l'élément de volume.
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_DIF(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER,INTENT(IN) :: KNE (3)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VRHS(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  COEF
      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  U1, U2, U3, V1, V2, V3
      REAL*8  C1, C2, C3
      REAL*8  H1, H2, H3, H_NU
      REAL*8  DUDX, DUDY, DVDX, DVDY
      REAL*8  TXX, TXY, TYY
      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  DETJ
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Métriques du T3
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)

C---     Déterminant
      DETJ = VDJE(5)

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Valeurs nodales
      H1 = VPRN(SV2D_IPRNO_H,NO1)          ! Profondeur
      C1 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO1) ! Visco pour le découvrement
      H2 = VPRN(SV2D_IPRNO_H,NO2)
      C2 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO2)
      H3 = VPRN(SV2D_IPRNO_H,NO3)
      C3 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO3)

C---     Visco totale
      C1 = VPRE(2) + C1       ! Visco totale + découvrement
      C2 = VPRE(2) + C2
      C3 = VPRE(2) + C3

C---     qx / h
      U1 = QX1/H1    ! Recalculé car issu de VRHS
      V1 = QY1/H1
      U2 = QX2/H2
      V2 = QY2/H2
      U3 = QX3/H3
      V3 = QY3/H3

C---     (H*nu) moyen
      H_NU = UN_3*(C1*H1 + C2*H2 + C3*H3)

C---     Coefficient
      COEF = UN_2*H_NU/DETJ

C---     Contraintes
      DUDX = VKX*(U2-U1) + VEX*(U3-U1)
      DUDY = VKY*(U2-U1) + VEY*(U3-U1)
      DVDX = VKX*(V2-V1) + VEX*(V3-V1)
      DVDY = VKY*(V2-V1) + VEY*(V3-V1)
      TXX = COEF*(DUDX + DUDX)
      TXY = COEF*(DUDY + DVDX)
      TYY = COEF*(DVDY + DVDY)

C---     Assemblage
      VRES(1,NO1) = VRES(1,NO1) + (VSX*TXX + VSY*TXY)
      VRES(2,NO1) = VRES(2,NO1) + (VSX*TXY + VSY*TYY)

      VRES(1,NO2) = VRES(1,NO2) + (VKX*TXX + VKY*TXY)
      VRES(2,NO2) = VRES(2,NO2) + (VKX*TXY + VKY*TYY)

      VRES(1,NO3) = VRES(1,NO3) + (VEX*TXX + VEY*TXY)
      VRES(2,NO3) = VRES(2,NO3) + (VEX*TXY + VEY*TYY)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_V_DIF

C************************************************************************
C Sommaire:  SV2D_CMP_FE_V_SLR
C
C Description:
C     La subroutine privée SV2D_CMP_FE_V_SLR assemble dans le
C     résidu Re la contribution de volume des sollicitations réparties.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CMP_FE_V_SLR(VRES,
     &                             VDJE,
     &                             VPRG,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VSLR)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8, INTENT(INOUT) :: VRES(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VDJE(EG_CMMN_NDJV)
      REAL*8, INTENT(IN) :: VPRG(LM_CMMN_NPRGL)
      REAL*8, INTENT(IN) :: VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8, INTENT(IN) :: VPRE(LM_CMMN_NPREV_D1)
      REAL*8, INTENT(IN) :: VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV) ! Pas utilisé
      REAL*8, INTENT(IN) :: VSLR(LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  SU1, SU2, SU3, SU4, SU5, SU6
      REAL*8  SV1, SV2, SV3, SV4, SV5, SV6
      REAL*8  SH1,      SH3,      SH5
      REAL*8  SUE1, SUE2, SUE3, SUE4
      REAL*8  SVE1, SVE2, SVE3, SVE4
      REAL*8  SHE
      REAL*8  DETJ_T6, DETJ_T3
      INTEGER IERR
      INTEGER IC, IE, IN

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Métriques
      DETJ_T6 = UN_24*VDJE(5)
      DETJ_T3 = UN_4*DETJ_T6      ! 1/4 POUR LE DJ SUR LE T3

C---     Sollicitations nodales
      SU1 = DETJ_T3 * VSLR(1,NO1)
      SV1 = DETJ_T3 * VSLR(2,NO1)
      SH1 = DETJ_T6 * VSLR(3,NO1)
      SU2 = DETJ_T3 * VSLR(1,NO2)
      SV2 = DETJ_T3 * VSLR(2,NO2)
      SU3 = DETJ_T3 * VSLR(1,NO3)
      SV3 = DETJ_T3 * VSLR(2,NO3)
      SH3 = DETJ_T6 * VSLR(3,NO3)
      SU4 = DETJ_T3 * VSLR(1,NO4)
      SV4 = DETJ_T3 * VSLR(2,NO4)
      SU5 = DETJ_T3 * VSLR(1,NO5)
      SV5 = DETJ_T3 * VSLR(2,NO5)
      SH5 = DETJ_T6 * VSLR(3,NO5)
      SU6 = DETJ_T3 * VSLR(1,NO6)
      SV6 = DETJ_T3 * VSLR(2,NO6)

C---     Sommations pour chaque sous-element
      SUE1 = SU1 + SU2 + SU6
      SVE1 = SV1 + SV2 + SV6
      SUE2 = SU2 + SU3 + SU4
      SVE2 = SV2 + SV3 + SV4
      SUE3 = SU6 + SU4 + SU5
      SVE3 = SV6 + SV4 + SV5
      SUE4 = SU4 + SU6 + SU2
      SVE4 = SV4 + SV6 + SV2
      SHE  = SH1 + SH3 + SH5

C---     Contributions
      VRES(1, NO1) = VRES(1, NO1) + SUE1+SU1
      VRES(2, NO1) = VRES(2, NO1) + SVE1+SV1
      VRES(3, NO1) = VRES(3, NO1) + SHE +SH1
      VRES(1, NO2) = VRES(1, NO2) + SUE1+SU2 + SUE2+SU2 + SUE4+SU2
      VRES(2, NO2) = VRES(2, NO2) + SVE1+SV2 + SVE2+SV2 + SVE4+SV2
!     VRES(3, NO2) = VRES(3, NO2) + ZERO
      VRES(1, NO3) = VRES(1, NO3) + SUE2+SU3
      VRES(2, NO3) = VRES(2, NO3) + SVE2+SV3
      VRES(3, NO3) = VRES(3, NO3) + SHE +SH3
      VRES(1, NO4) = VRES(1, NO4) + SUE2+SU4 + SUE3+SU4 + SUE4+SU4
      VRES(2, NO4) = VRES(2, NO4) + SVE2+SV4 + SVE3+SV4 + SVE4+SV4
!     VRES(3, NO4) = VRES(3, NO4) + ZERO
      VRES(1, NO5) = VRES(1, NO5) + SUE3+SU5
      VRES(2, NO5) = VRES(2, NO5) + SVE3+SV5
      VRES(3, NO5) = VRES(3, NO5) + SHE +SH5
      VRES(1, NO6) = VRES(1, NO6) + SUE3+SU6 + SUE1+SU6 + SUE4+SU6
      VRES(2, NO6) = VRES(2, NO6) + SVE3+SV6 + SVE1+SV6 + SVE4+SV6
!     VRES(3, NO6) = VRES(3, NO6) + ZERO

      RETURN
      END SUBROUTINE SV2D_CMP_FE_V_SLR

C************************************************************************
C Sommaire: SV2D_CMP_RE_S_DIF
C
C Description:
C     La fonction SV2D_CMP_RE_S_DIF calcule la partie de diffusion du
C     résidu Re pour un élément de surface.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) On prend pour acquis que KNE est pour un T3 et que
C        l'élément de contour L2 à calculer forme le premier côté (noeuds 1-2).
C
C     2) {NL2} H_nu < NT3,x > { q/H }
C        Le produit H.nu est la moyenne sur l'élément de volume.
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_S_DIF(VRES,
     &                             KNE,
     &                             VDJV,
     &                             VDJS,
     &                             VPRG,
     &                             VPRN,
     &                             VPREV,
     &                             VPRES,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VRES (LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER  KNE  (3)
      REAL*8   VDJV (EG_CMMN_NDJV)
      REAL*8   VDJS (EG_CMMN_NDJS)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPREV(LM_CMMN_NPREV_D1)
      REAL*8   VPRES(LM_CMMN_NPRES_D1)
      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VRHS (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  H1, H2, H3, H_NU
      REAL*8  C1, C2, C3
      REAL*8  U1, U2, U3, V1, V2, V3
      REAL*8  DUDX, DUDY, DVDX, DVDY, TXX, TXY, TYY
      REAL*8  COEFX, COEFY
      REAL*8  DJL2, DJT3
      REAL*8  VNX, VNY
      REAL*8  VSX, VKX, VEX
      REAL*8  VSY, VKY, VEY
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Connectivités du T3
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Normales et métriques de l'élément L2
      VNX =  VDJS(2)     ! VNX =  VTY
      VNY = -VDJS(1)     ! VNY = -VTX
      DJL2=  VDJS(3)

C---     Métriques du T3
      VKX = VDJV(1)
      VEX = VDJV(2)
      VKY = VDJV(3)
      VEY = VDJV(4)
      DJT3= VDJV(5)

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Valeurs nodales
      H1 = VPRN(SV2D_IPRNO_H,NO1)            ! Profondeur nodales
      C1 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO1)   ! Visco pour le découvrement
      H2 = VPRN(SV2D_IPRNO_H,NO2)
      C2 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO2)
      H3 = VPRN(SV2D_IPRNO_H,NO3)
      C3 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO3)

C---     Visco totale
      C1 = VPREV(2) !!! + C1      ! Visco totale + découvrement
      C2 = VPREV(2) !!! + C2
      C3 = VPREV(2) !!! + C3

C---     qx / h
      U1 = QX1/H1    ! Recalculé car issu de VRHS
      V1 = QY1/H1
      U2 = QX2/H2
      V2 = QY2/H2
      U3 = QX3/H3
      V3 = QY3/H3

C---     (H*nu) moyen
      H_NU = UN_3*(C1*H1 + C2*H2 + C3*H3)

C---     Coefficient
      COEFX = SV2D_CMULT_INTGCTRQX * H_NU * (DJL2/DJT3)
      COEFY = SV2D_CMULT_INTGCTRQY * H_NU * (DJL2/DJT3)

C---     Contraintes
      DUDX = VKX*(U2-U1) + VEX*(U3-U1)
      DUDY = VKY*(U2-U1) + VEY*(U3-U1)
      DVDX = VKX*(V2-V1) + VEX*(V3-V1)
      DVDY = VKY*(V2-V1) + VEY*(V3-V1)
      TXX = DUDX + DUDX
      TXY = DUDY + DVDX
      TYY = DVDY + DVDY

C---     Assemble
      VRES(1,NO1) = VRES(1,NO1) - COEFX*(TXX*VNX + TXY*VNY)
      VRES(1,NO2) = VRES(1,NO2) - COEFX*(TXX*VNX + TXY*VNY)

      VRES(2,NO1) = VRES(2,NO1) - COEFY*(TXY*VNX + TYY*VNY)
      VRES(2,NO2) = VRES(2,NO2) - COEFY*(TXY*VNX + TYY*VNY)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_S_DIF

C************************************************************************
C Sommaire: SV2D_CMP_RE_S_CNT
C
C Description:
C     La fonction SV2D_CBS_RE_S_CNT calcule la partie de Darcy/Lapidus
C     de la continuité du résidu Re.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     {NL2} <NT3_1,x ; NT3_2,x ; NT3_3,x>
C
C     L'intégration est sur l'élément L2/T3 externe
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_S_CNT(VRES,
     &                             KNE,
     &                             VDJV,
     &                             VDJS,
     &                             VPRG,
     &                             VPRN,
     &                             VPREV,
     &                             VPRES,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VRES (LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER  KNE  (3)
      REAL*8   VDJV (EG_CMMN_NDJV)
      REAL*8   VDJS (EG_CMMN_NDJS)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPREV(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8   VPRES(LM_CMMN_NPRES_D1, LM_CMMN_NPRES_D2)
      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VRHS (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'sv2d_cbs.fc'

      REAL*8  C1, C3, C5, CM, C
      REAL*8  H1, H3, H5
      REAL*8  DHDX, DHDY, DHNRM
      REAL*8  VNX, VNY
      REAL*8  VKX, VEX
      REAL*8  VKY, VEY
      REAL*8  DJL2, DJT3
      INTEGER NO1, NO3, NO5
      INTEGER IKH1, IKH2, IKH3
C-----------------------------------------------------------------------

C---     Connectivités des sommets du T6L
      NO1 = KNE(1)
      NO3 = KNE(2)
      NO5 = KNE(3)

C---     Normales et métriques de l'élément L2
      VNX =  VDJS(2)     ! VNX =  VTY
      VNY = -VDJS(1)     ! VNY = -VTX
      DJL2=  VDJS(3)

C---     Métriques du T3
      VKX = VDJV(1)
      VEX = VDJV(2)
      VKY = VDJV(3)
      VEY = VDJV(4)
      DJT3= VDJV(5)

C---     Degrés de liberté
      H1 = VDLE(3,NO1)
      H3 = VDLE(3,NO3)
      H5 = VDLE(3,NO5)

C---     Coefficients de Lapidus
      DHDX = VKX*(H3-H1) + VEX*(H5-H1)
      DHDY = VKY*(H3-H1) + VEY*(H5-H1)
      DHNRM = MAX(HYPOT(DHDX, DHDY), 1.0D-12)
      C = SV2D_STABI_LAPIDUS * DHNRM

C---     Inconnues
      H1 = VRHS(3,NO1)
      H3 = VRHS(3,NO3)
      H5 = VRHS(3,NO5)

C---     Gradients
      DHDX = VKX*(H3-H1) + VEX*(H5-H1)
      DHDY = VKY*(H3-H1) + VEY*(H5-H1)

C---     Coefficients de Darcy
      C1 = VPRN(SV2D_IPRNO_COEFF_DRCY, NO1)
      C3 = VPRN(SV2D_IPRNO_COEFF_DRCY, NO3)
      C5 = VPRN(SV2D_IPRNO_COEFF_DRCY, NO5)
      CM = UN_3*(C1 + C3 + C5)
      C  = SV2D_CMULT_INTGCTRH * (C+CM) * (DJL2/DJT3)

C---     Sous-matrice H.H
      VRES(3, NO1) = VRES(3, NO1) - C*(VNX*DHDX + VNY*DHDY)
      VRES(3, NO3) = VRES(3, NO3) - C*(VNX*DHDX + VNY*DHDY)

      RETURN
      END SUBROUTINE SV2D_CMP_RE_S_CNT

      END MODULE SV2D_CBS_CMP_RE_M
