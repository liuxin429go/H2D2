C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKU_K(VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES, ! sortie seulement...
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_ASMKU_K
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! sortie seulement
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'

      INTEGER, PARAMETER :: NNELT   = 6
      INTEGER, PARAMETER :: NDLNMAX = 3
      INTEGER, PARAMETER :: NDLEMAX = NNELT*NDLNMAX

      REAL*8  VKE  (NDLEMAX*NDLEMAX)
      REAL*8  VFE  (NDLEMAX)
      INTEGER KLOCE(NDLEMAX)
      INTEGER IERR
      
      LOGICAL SV2D_CBS_KE_IPVALID
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. NDLNMAX)
C-----------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôle les indices
      CALL ERR_ASR(SV2D_CBS_KE_IPVALID())
      
C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKE, VFE, KLOCE)
         CALL SV2D_CBS_ASMKU_KV(KLOCE,
     &                          VKE,
     &                          VFE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          VDLG,
     &                          VFG)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         CALL SV2D_CBS_ASMKU_KS(KLOCE,
     &                          VKE,
     &                          VFE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          VDLG,
     &                          VFG)
      ENDIF

C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_ASMKU(KLOCE,
     &                        VKE,
     &                        VFE,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        VFG)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire:  CNF2KU
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : VDLG=C
C
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKU_KV(KLOCE,
     &                             VKE,
     &                             VFE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES, ! pas utilisé
     &                             VSOLR,
     &                             VDLG,
     &                             VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IC, IE, IN

      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NPRN_LCL = 18

      REAL*8, POINTER :: VDLE  (:, :)
      REAL*8, POINTER :: VPRN  (:, :)
      REAL*8, TARGET  :: VDLE_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8, TARGET  :: VPRN_LCL (NPRN_LCL * NNEL_LCL)
      INTEGER KNE (NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLN_LCL .EQ. LM_CMMN_NDLN)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C---     Reshape the arrays
      VDLE(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VDLE_LCL
      VPRN(1:LM_CMMN_NPRNO,1:EG_CMMN_NNELV) => VPRN_LCL

C---      BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL

!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
         KNE(:) = KNGV(:,IE)

C---        Transfert des valeurs nodales T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         VDLE(:,:) = VDLG (:,KNE(:))
!dir$ IVDEP, LOOP COUNT(18*6)
         VPRN(:,:) = VPRNO (:,KNE(:))

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO

C---        Calcul de la matrice élémentaire de volume
         CALL SV2D_CBS_KE_V(VKE,
     &                      VDJV(1,IE),
     &                      VPRGL,
     &                      VPRN,
     &                      VPREV(1,IE),
     &                      VDLE)

C---        Produit matrice-vecteur
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKE,               ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLE,              ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VFE,               ! y
     &              1)                 ! incy

C---        Table KLOCE de l'élément T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         KLOCE(:,:) = KLOCN(:, KNE(:))

C---       Assemblage du vecteur global
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On transfert les propriétés du T6L et du L3L par simplicité de
C     gestion. Le L3L n'aurait besoin que de deux sous-triangles T3.
C     On assemble une matrice complète 18x18, car la matrice Ke doit
C     être carrée pour l'assemblage.
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKU_KS(KLOCE,
     &                             VKE,
     &                             VFE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES, ! pas utilisé
     &                             VSOLR,
     &                             VDLG,
     &                             VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IES, IEV, ICT

      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NPRN_LCL = 18

      REAL*8, POINTER :: VDLEV (:, :)
      REAL*8, POINTER :: VPRNV (:, :)
      REAL*8, TARGET  :: VDLEV_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8, TARGET  :: VPRNV_LCL (NPRN_LCL * NNEL_LCL)
      INTEGER KNEV(NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLN_LCL .GE. LM_CMMN_NDLN)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C---     Reshape the arrays
      VDLEV(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VDLEV_LCL
      VPRNV(1:LM_CMMN_NPRNO,1:EG_CMMN_NNELV) => VPRNV_LCL

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO IES=1,EG_CMMN_NELS

C---        Élément parent et coté
         IEV = KNGS(4,IES)
         ICT = KNGS(5,IES)

C---        Transfert des connectivités élémentaires
         KNEV(:) = KNGV(:,IEV)

C---        Transfert des valeurs nodales T6L
         VDLEV(:,:) = VDLG (:,KNEV(:))
         VPRNV(:,:) = VPRNO(:,KNEV(:))

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO

C---        Calcul de la matrice élémentaire de surface
         CALL SV2D_CBS_KE_S(VKE,
     &                      VDJV(1,IEV),
     &                      VDJS(1,IES),
     &                      VPRGL,
     &                      VPRNV,
     &                      VPREV(1,IEV),
     &                      VPRES(1,IES), ! pas utilisé
     &                      VDLEV,
     &                      ICT)

C---        Produit matrice-vecteur
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKE,               ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLEV,             ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VFE,               ! y
     &              1)                 ! incy

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = KLOCN(:, KNEV(:))

C---        Assemblage du vecteur global
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

      ENDDO

      RETURN
      END
