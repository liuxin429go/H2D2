C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_ASMK_R
C
C Description:
C     La fonction SV2D_CBS_ASMK_R calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMK_R (VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_ASMK_R
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'

      INTEGER, PARAMETER :: NDLN =  3
      INTEGER, PARAMETER :: NNEL =  6
      INTEGER, PARAMETER :: NDLE = NNEL*NDLN

      REAL*8   VKE(NDLE, NDLE)
      INTEGER  KLOCE(NDLE)
      INTEGER  IERR
C----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN  .LE. NDLN)
D     CALL ERR_PRE(LM_CMMN_NDLEV .LE. NDLE)
C-----------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKE, KLOCE)
         CALL SV2D_CBS_ASMK_RV (KLOCE,
     &                          VKE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         CALL SV2D_CBS_ASMK_RS (KLOCE,
     &                          VKE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
      ENDIF

C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_ASMK (KLOCE,
     &                        VKE,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        HMTX,
     &                        F_ASM)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMK_RV
C
C Description:
C     La fonction SV2D_CBS_ASMK_RV calcule la matrice tangente
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée: HMTX
C
C Sortie: KLOCE : Table de localisation élémentaire
C         VKE : Matrice élémentaire
C
C Notes:
C     3) La perturbation d'un ddl en h sur un noeud sommet entraîne une
C     modif du ddl pour le noeud milieu dans le calcul des prop. nodales.
C     La valeur originale du noeud sommet est restaurée à la fin de la boucle
C     de perturbation. Le noeud milieu lui n'est restauré qu'au prochain
C     calcul de prop. nodales. Comme le dernier ddl perturbé est en v
C     (noeud 6), il y a implicitement restauration.
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMK_RV (KLOCE,
     &                             VKE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLR,
     &                             VDLG,
     &                             HMTX,
     &                             F_ASM)

      USE SV2D_CBS_RE_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  PENA
      INTEGER IERR
      INTEGER IC, IE, IN, ID, IDG
      REAL*8, POINTER :: VDLE(:, :)
      REAL*8, POINTER :: VPRN(:, :)
      REAL*8, POINTER :: VRHS(:, :)
      INTEGER,POINTER :: KNE (:)

C---     Tables locales
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VDLE_LCL(NDLN_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VPRN_LCL(NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VRHS_LCL(NDLN_LCL * NNEL_LCL)
      INTEGER, TARGET :: KNE_LCL (NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDLN_LCL .GE. LM_CMMN_NDLN)
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C---     Reshape the arrays
      VDLE(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VDLE_LCL
      VPRN(1:LM_CMMN_NPRNO,1:EG_CMMN_NNELV) => VPRN_LCL
      VRHS(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VRHS_LCL
      KNE (1:EG_CMMN_NNELV) => KNE_LCL

C---     Initialise le RHS
      VRHS(:,:) = ZERO

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp  do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
!dir$ IVDEP, LOOP COUNT(6)
         KNE(:) = KNGV(:,IE)

C---        Table KLOCE de l'élément T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         KLOCE(:,:) = KLOCN(:, KNE(:))

C---        Transfert des valeurs nodales T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         VDLE(:,:) = VDLG (:,KNE(:))
!dir$ IVDEP, LOOP COUNT(18*6)
         VPRN(:,:) = VPRNO(:,KNE(:))

C---        Initialise la matrice élémentaire
!dir$ IVDEP, LOOP COUNT(18*18)
         VKE(:,:) = ZERO

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         IDG = 0
         DO IN=1,LM_CMMN_NNELV
         DO ID=1,LM_CMMN_NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL
            VRHS(ID,IN) = UN

C---           R(u). i
            CALL SV2D_CBS_RE_K(VKE(1,IDG),
     &                         VDJV(1,IE),
     &                         VPRGL,
     &                         VPRN,
     &                         VPREV(1,IE),
     &                         VDLE,
     &                         VRHS)

C---           Restaure la valeur originale
            VRHS(ID,IN) = ZERO

199         CONTINUE
         ENDDO
         ENDDO

C---        Pénalisation en hh
         PENA = SV2D_PNUMR_PENALITE*VDJV(5,IE)
         VKE( 3, 3) = MAX(VKE( 3, 3), PENA)
         VKE( 9, 9) = MAX(VKE( 9, 9), PENA)
         VKE(15,15) = MAX(VKE(15,15), PENA)

C---       Assemble la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMK_RS
C
C Description:
C     La fonction SV2D_CBS_ASMK_RS calcule la matrice élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     c.f. SV2D_CBS_ASMK_V
C
C     Le noeud opposé à l'élément de surface est perturbé en trop.
C     La matrice Ke peut être nulle.
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMK_RS (KLOCE,
     &                             VKE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLR,
     &                             VDLG,
     &                             HMTX,
     &                             F_ASM)

      USE SV2D_CBS_RE_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
      INTEGER IN, ID, IDG
      INTEGER IES, IEV, ICT
      REAL*8, POINTER :: VDLEV(:, :)
      REAL*8, POINTER :: VPRNV(:, :)
      REAL*8, POINTER :: VRHS (:, :)
      INTEGER,POINTER :: KNE  (:)

C---     Tables locales
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VDLEV_LCL(NDLN_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VPRNV_LCL(NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VRHS_LCL (NDLN_LCL * NNEL_LCL)
      INTEGER, TARGET :: KNE_LCL  (NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDLN_LCL .GE. LM_CMMN_NDLN)
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C---     Reshape the arrays
      VDLEV(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VDLEV_LCL
      VPRNV(1:LM_CMMN_NPRNO,1:EG_CMMN_NNELV) => VPRNV_LCL
      VRHS (1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VRHS_LCL
      KNE  (1:EG_CMMN_NNELV) => KNE_LCL

C---     Initialise le RHS
      VRHS(:,:) = ZERO

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO IES=1,EG_CMMN_NELS

C---        Élément parent et coté
         IEV = KNGS(4,IES)
         ICT = KNGS(5,IES)

C---        Transfert des connectivités de l'élément T6L
!dir$ IVDEP, LOOP COUNT(6)
         KNE(:) = KNGV(:,IEV)

C---        Table KLOCE de l'élément T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         KLOCE(:,:) = KLOCN(:, KNE(:))

C---        Transfert des valeurs nodales T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         VDLEV(:,:) = VDLG (:,KNE(:))
!dir$ IVDEP, LOOP COUNT(18*6)
         VPRNV(:,:) = VPRNO(:,KNE(:))

C---        Initialise la matrice élémentaire
!dir$ IVDEP, LOOP COUNT(18*18)
         VKE(:,:) = ZERO

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         IDG = 0
         DO IN=1,LM_CMMN_NNELV
         DO ID=1,LM_CMMN_NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL du T6L
            VRHS(ID,IN) = UN

C---           R(u)
            CALL SV2D_CBS_RE_S(VKE(1,IDG),
     &                         VDJV(1,IEV),
     &                         VDJS(1,IES),
     &                         VPRGL,
     &                         VPRNV,
     &                         VPREV(1,IEV),
     &                         VPRES(1,IES),
     &                         VDLEV,
     &                         VRHS,
     &                         ICT)

C---           Restaure la valeur originale
            VRHS(ID,IN) = ZERO

199         CONTINUE
         ENDDO
         ENDDO

C---       Assemble la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      RETURN
      END
