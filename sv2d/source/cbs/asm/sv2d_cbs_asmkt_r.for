C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_ASMKT_R
C   Private:
C     SUBROUTINE SV2D_CBS_ASMKT_RV
C     SUBROUTINE SV2D_CBS_ASMKT_RS
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_ASMKT_R
C
C Description: ASSEMBLAGE DE LA MATRICE TANGENTE
C     La fonction SV2D_CBS_ASMKT_R calcule le matrice tangente
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C     Le calcul est fait via le résidu élémentaire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKT_R(VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_ASMKT_R
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'

      INTEGER, PARAMETER :: NNEL_LCL = 6
      INTEGER, PARAMETER :: NDLN_LCL = 3
      INTEGER, PARAMETER :: NDLE_LCL = NNEL_LCL*NDLN_LCL

      REAL*8   VKE  (NDLE_LCL, NDLE_LCL)
      INTEGER  KLOCE(NDLE_LCL)
      INTEGER  IERR
C----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN  .LE. NDLN_LCL)
D     CALL ERR_PRE(LM_CMMN_NDLEV .LE. NDLE_LCL)
C-----------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKE, KLOCE)
         CALL SV2D_CBS_ASMKT_RV(KLOCE,
     &                          VKE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         CALL SV2D_CBS_ASMKT_RS(KLOCE,
     &                          VKE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
      ENDIF

C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_ASMKT(KLOCE,
     &                        VKE,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        HMTX,
     &                        F_ASM)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMKT_RV
C
C Description:
C     La fonction SV2D_CBS_ASMKT_RV calcule la matrice tangente
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée: HMTX
C
C Sortie: KLOCE : Table de localisation élémentaire
C         VKE : Matrice élémentaire
C
C Notes:
C     1) La matrice tangente est donnée par (Gouri Dhatt p. 342):
C        Kt_ij = K_ij + Somme_sur_l ( (dK_il / du_j) * u_l )
C     avec:
C        dK_il/du_j ~= (K(u+delu_j) - K(u) ) / delu_j
C
C     2) La version d'Hydrosim est plus efficace. Elle calcule la partie
C     tangente comme:
C        dK_il/du_j ~= ([K(u+delu_j)]{u+delu_j} - [K(u)]{u} ) / delu_j
C     Elle correspond à la partie K de:
C        dK_il/du_j ~= -(R(u+delu_j) - R(u)) / delu_j
C
C     3) La perturbation d'un DDL en h sur un noeud sommet entraîne une
C     modification du DDL pour le noeud milieu dans le calcul des prop. nodales.
C     La valeur originale du noeud sommet est restaurée à la fin de la boucle
C     de perturbation. Le noeud milieu lui n'est restauré qu'au prochain
C     calcul de prop. nodales. Comme le dernier DDL perturbé est en v
C     (noeud 6), il y a implicitement restauration.
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKT_RV(KLOCE,
     &                             VKE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLR,
     &                             VDLG,
     &                             HMTX,
     &                             F_ASM)

      USE SV2D_CBS_RE_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
      INTEGER IC, IE, IN, ID, IDG
      INTEGER HVFT, HPRNE, HPREVE
      REAL*8  PENA
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      REAL*8, POINTER :: VCOR_P(:, :)
      REAL*8, POINTER :: VDJE_P(:)
      REAL*8, POINTER :: VDLE_P(:, :)
      REAL*8, POINTER :: VPRN_P(:, :)
      REAL*8, POINTER :: VPRE_P(:)
      REAL*8, POINTER :: VRESE_P(:)
      REAL*8, POINTER :: VRESP_P(:)
      INTEGER,POINTER :: KNE_P (:)

C---     Tables locales
      INTEGER, PARAMETER :: NDIM_LCL =  2
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL = NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NDJE_LCL =  5
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NPRE_LCL =  8
      REAL*8,  TARGET :: VCOR_LCL (NDIM_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VDJE_LCL (NDJE_LCL)
      REAL*8,  TARGET :: VDLE_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRN_LCL (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRE_LCL (NPRE_LCL)
      REAL*8,  TARGET :: VRESE_LCL(NDLE_LCL)
      REAL*8,  TARGET :: VRESP_LCL(NDLE_LCL)
      INTEGER, TARGET :: KNE_LCL  (NNEL_LCL)

      BYTE, POINTER :: KNE_B  (:)
      BYTE, POINTER :: VCOR_B(:)
      BYTE, POINTER :: VDJE_B(:)
      BYTE, POINTER :: VPRG_B(:)
      BYTE, POINTER :: VPRN_B(:)
      BYTE, POINTER :: VDLE_B(:)
      BYTE, POINTER :: VPRE_B(:)
      BYTE, POINTER :: IERR_B (:)

      INTEGER, PARAMETER :: KNE_E(NNEL_LCL) = (/ 1, 2, 3, 4, 5, 6/)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDIM_LCL .EQ. EG_CMMN_NDIM)
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDJE_LCL .GE. EG_CMMN_NDJV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
D     CALL ERR_PRE(NPRE_LCL .GE. LM_CMMN_NPREV)
C-----------------------------------------------------------------------

C---     Récupère les données
      HVFT = 0    ! vtable par défaut
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPRNEV, HPRNE)
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPREVE, HPREVE)

C---     Reshape the arrays
      VCOR_P (1:EG_CMMN_NDIM, 1:EG_CMMN_NNELV) => VCOR_LCL
      VDJE_P (1:EG_CMMN_NDJV)  => VDJE_LCL
      VDLE_P (1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VDLE_LCL
      VPRN_P (1:LM_CMMN_NPRNO,1:EG_CMMN_NNELV) => VPRN_LCL
      VPRE_P (1:LM_CMMN_NPREV) => VPRE_LCL
      VRESE_P(1:LM_CMMN_NDLEV) => VRESE_LCL
      VRESP_P(1:LM_CMMN_NDLEV) => VRESP_LCL
      KNE_P  (1:EG_CMMN_NNELV) => KNE_LCL

C---     Pointeurs pour le calcul des propriétés perturbées
      KNE_B  => SO_ALLC_CST2B(KNE_E(:))
      VCOR_B => SO_ALLC_CST2B(VCOR_P(:,1))
      VDJE_B => SO_ALLC_CST2B(VDJE_P(:))
      VDLE_B => SO_ALLC_CST2B(VDLE_P(:,1))
      VPRG_B => SO_ALLC_CST2B(VPRGL(:))
      VPRN_B => SO_ALLC_CST2B(VPRN_P(:,1))
      VPRE_B => SO_ALLC_CST2B(VPRE_P(:))
      IERR_B => SO_ALLC_CST2B(IERR)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp  do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
         KNE_P(:) = KNGV(:,IE)

C---        Table KLOCE de l'élément T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         KLOCE(:,:) = KLOCN(:, KNE_P(:))

C---        Transfert des valeurs nodales T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         VCOR_P(:,:) = VCORG(:,KNE_P(:))
!dir$ IVDEP, LOOP COUNT(3*6)
         VDLE_P(:,:) = VDLG (:,KNE_P(:))
!dir$ IVDEP, LOOP COUNT(18*6)
         VPRN_P(:,:) = VPRNO(:,KNE_P(:))

C---        Transfert des valeurs élémentaires
!dir$ IVDEP, LOOP COUNT(5)
         VDJE_P(:) = VDJV (:,IE)
!dir$ IVDEP, LOOP COUNT(2*4)
         VPRE_P(:) = VPREV(:,IE)

C---        Initialise la matrice élémentaire
!dir$ IVDEP, LOOP COUNT(18*18)
         VKE(:,:) = ZERO

C---        - R(u)  (juste la partie k.u)
!dir$ IVDEP, LOOP COUNT(18)
         VRESE_P(:) = ZERO
         CALL SV2D_CBS_RE_V(VRESE_P,
     &                      VDJE_P,
     &                      VPRGL,
     &                      VPRN_P,
     &                      VPRE_P,
     &                      VDLE_P,
     &                      VDLE_P)  ! VRHS

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         IDG = 0
         DO IN=1,LM_CMMN_NNELV
         DO ID=1,LM_CMMN_NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL
            VDL_ORI = VDLE_P(ID,IN)
            VDL_DEL = SV2D_PNUMR_DELPRT * VDLE_P(ID,IN)
     &              + SIGN(SV2D_PNUMR_DELMIN, VDLE_P(ID,IN))
            VDL_INV = SV2D_PNUMR_OMEGAKT / VDL_DEL
            VDLE_P(ID,IN) = VDLE_P(ID,IN) + VDL_DEL

C---           Calcule les propriétés nodales perturbées
            IF (SV2D_PNUMR_PRTPRNO) THEN
               IERR = SO_FUNC_CALL4(HPRNE,
     &                              VPRG_B,
     &                              VPRN_B,
     &                              VDLE_B,
     &                              IERR_B)
            ENDIF

C---           Calcule les propriétés élémentaires perturbées
            IF (SV2D_PNUMR_PRTPREL) THEN
               IERR = SO_FUNC_CALL7(HPREVE,
     &                              VCOR_B,
     &                              VDJE_B,
     &                              VPRG_B,
     &                              VPRN_B,
     &                              VPRE_B,
     &                              VDLE_B,
     &                              IERR_B)
            ENDIF

C---           - R(u + du_id) (juste la partie k.u)
!dir$ IVDEP, LOOP COUNT(18)
            VRESP_P(:) = ZERO
            CALL SV2D_CBS_RE_V(VRESP_P,
     &                         VDJE_P,
     &                         VPRGL,
     &                         VPRN_P,
     &                         VPRE_P,
     &                         VDLE_P,
     &                         VDLE_P)  ! VRHS

C---           Restaure la valeur originale
            VDLE_P(ID,IN) = VDL_ORI

C---          - (R(u + du_id) - R(u))/du
!dir$ IVDEP, LOOP COUNT(18)
            VKE(:,IDG) = (VRESP_P(:) - VRESE_P(:)) * VDL_INV

199         CONTINUE
         ENDDO
         ENDDO

C---        Pénalisation en hh
         PENA = SV2D_PNUMR_PENALITE*VDJV(5,IE)
         VKE( 3, 3) = MAX(VKE( 3, 3), PENA)
         VKE( 9, 9) = MAX(VKE( 9, 9), PENA)
         VKE(15,15) = MAX(VKE(15,15), PENA)

C---        Assemble la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMKT_RS
C
C Description:
C     La fonction SV2D_CBS_ASMKT_RS calcul la matrice tangente élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     c.f. SV2D_CBS_ASMKT_RV
C
C     Le noeud opposé à l'élément de surface est perturbé en trop.
C     La matrice Ke peut être nulle.
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKT_RS(KLOCE,
     &                             VKE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLR,
     &                             VDLG,
     &                             HMTX,
     &                             F_ASM)

      USE SV2D_CBS_RE_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
      INTEGER IN, ID, IDG
      INTEGER IES, IEV, ICT
      INTEGER HVFT, HPRNE, HPREVE
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      REAL*8, POINTER :: VCORE_P(:, :)
      REAL*8, POINTER :: VDJEV_P(:)
      REAL*8, POINTER :: VDLEV_P(:, :)
      REAL*8, POINTER :: VPRNV_P(:, :)
      REAL*8, POINTER :: VPREV_P(:)
      REAL*8, POINTER :: VRESE_P(:)
      REAL*8, POINTER :: VRESP_P(:)
      INTEGER,POINTER :: KNEV_P (:)

C---     Tables locales
      INTEGER, PARAMETER :: NDIM_LCL =  2
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDJE_LCL =  5
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL = NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NPRE_LCL =  8
      REAL*8,  TARGET :: VCORE_LCL (NDIM_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VDJEV_LCL (NDJE_LCL)
      REAL*8,  TARGET :: VDLEV_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRNV_LCL (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPREV_LCL (NPRE_LCL)
      REAL*8,  TARGET :: VRESE_LCL (NDLE_LCL)
      REAL*8,  TARGET :: VRESP_LCL (NDLE_LCL)
      INTEGER, TARGET :: KNEV_LCL  (NNEL_LCL)

      BYTE, POINTER :: KNE_B  (:)
      BYTE, POINTER :: VCORE_B(:)
      BYTE, POINTER :: VDJEV_B(:)
      BYTE, POINTER :: VPRGL_B(:)
      BYTE, POINTER :: VPRNV_B(:)
      BYTE, POINTER :: VDLEV_B(:)
      BYTE, POINTER :: VPREV_B(:)
      BYTE, POINTER :: IERR_B (:)

      INTEGER, PARAMETER :: KNE_E(NNEL_LCL) = (/ 1, 2, 3, 4, 5, 6/)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDIM_LCL .EQ. EG_CMMN_NDIM)
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDJE_LCL .GE. EG_CMMN_NDJV)
D     CALL ERR_PRE(NDJE_LCL .GE. EG_CMMN_NDJS)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
D     CALL ERR_PRE(NPRE_LCL .GE. LM_CMMN_NPREV)
C-----------------------------------------------------------------------

      HVFT = 0    ! vtable par défaut
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPRNEV, HPRNE)
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPREVE, HPREVE)

C---     Reshape the arrays
      VCORE_P(1:EG_CMMN_NDIM, 1:EG_CMMN_NNELV) => VCORE_LCL
      VDJEV_P(1:EG_CMMN_NDJV)  => VDJEV_LCL
      VDLEV_P(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELV) => VDLEV_LCL
      VPRNV_P(1:LM_CMMN_NPRNO,1:EG_CMMN_NNELV) => VPRNV_LCL
      VPREV_P(1:LM_CMMN_NPREV) => VPREV_LCL
      VRESE_P(1:LM_CMMN_NDLEV) => VRESE_LCL
      VRESP_P(1:LM_CMMN_NDLEV) => VRESP_LCL
      KNEV_P (1:EG_CMMN_NNELV) => KNEV_LCL

C---     Byte pointeurs pour le calcul des propriétés perturbées
      KNE_B   => SO_ALLC_CST2B(KNE_E(:))
      VCORE_B => SO_ALLC_CST2B(VCORE_P(:,1))
      VDJEV_B => SO_ALLC_CST2B(VDJEV_P(:))
      VDLEV_B => SO_ALLC_CST2B(VDLEV_P(:,1))
      VPRGL_B => SO_ALLC_CST2B(VPRGL(:))
      VPRNV_B => SO_ALLC_CST2B(VPRNV_P(:,1))
      VPREV_B => SO_ALLC_CST2B(VPREV_P(:))
      IERR_B  => SO_ALLC_CST2B(IERR)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO IES=1,EG_CMMN_NELS

C---        Élément parent et coté
         IEV = KNGS(4,IES)
         ICT = KNGS(5,IES)

C---        Transfert des connectivités de l'élément T6L
!dir$ IVDEP, LOOP COUNT(6)
         KNEV_P(:) = KNGV(:,IEV)

C---        Table KLOCE de l'élément T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         KLOCE(:,:) = KLOCN(:, KNEV_P(:))

C---        Transfert des valeurs nodales T6L
!dir$ IVDEP, LOOP COUNT(3*6)
         VCORE_P(:,:) = VCORG(:,KNEV_P(:))
!dir$ IVDEP, LOOP COUNT(3*6)
         VDLEV_P(:,:) = VDLG (:,KNEV_P(:))
!dir$ IVDEP, LOOP COUNT(18*6)
         VPRNV_P(:,:) = VPRNO(:,KNEV_P(:))

C---        Transfert des valeurs élémentaires
!dir$ IVDEP, LOOP COUNT(5)
         VDJEV_P(:) = VDJV (:,IEV)
!dir$ IVDEP, LOOP COUNT(2*4)
         VPREV_P(:) = VPREV(:,IEV)

C---        Initialise la matrice élémentaire
!dir$ IVDEP, LOOP COUNT(18*18)
         VKE(:,:) = ZERO

C---        R(u)
!dir$ IVDEP, LOOP COUNT(18)
         VRESE_P(:) = ZERO
         CALL SV2D_CBS_RE_S(VRESE_P,
     &                      VDJEV_P,
     &                      VDJS(1,IES),
     &                      VPRGL,
     &                      VPRNV_P,
     &                      VPREV_P,
     &                      VPRES(1,IES),
     &                      VDLEV_P,
     &                      VDLEV_P,        ! VRHS
     &                      ICT)


C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         IDG = 0
         DO IN=1,LM_CMMN_NNELV
         DO ID=1,LM_CMMN_NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL du T6L
            VDL_ORI = VDLEV_P(ID,IN)
            VDL_DEL = SV2D_PNUMR_DELPRT * VDLEV_P(ID,IN)
     &              + SIGN(SV2D_PNUMR_DELMIN, VDLEV_P(ID,IN))
            VDL_INV = SV2D_PNUMR_OMEGAKT / VDL_DEL
            VDLEV_P(ID,IN) = VDLEV_P(ID,IN) + VDL_DEL

C---           Calcule les propriétés nodales perturbées
            IF (SV2D_PNUMR_PRTPRNO) THEN
               IERR = SO_FUNC_CALL4(HPRNE,
     &                              VPRGL_B,
     &                              VPRNV_B,
     &                              VDLEV_B,
     &                              IERR_B)
            ENDIF

C---           Calcule les propriétés élémentaires perturbées
            IF (SV2D_PNUMR_PRTPREL) THEN
               IERR = SO_FUNC_CALL7(HPREVE,
     &                              VCORE_B,
     &                              VDJEV_B,
     &                              VPRGL_B,
     &                              VPRNV_B,
     &                              VPREV_B,
     &                              VDLEV_B,
     &                              IERR_B)
            ENDIF

C---           R(u)
!dir$ IVDEP, LOOP COUNT(18)
            VRESP_P(:) = ZERO
            CALL SV2D_CBS_RE_S(VRESP_P,
     &                         VDJEV_P,
     &                         VDJS(1,IES),
     &                         VPRGL,
     &                         VPRNV_P,
     &                         VPREV_P,
     &                         VPRES(1,IES),
     &                         VDLEV_P,
     &                         VDLEV_P,        ! VRHS
     &                         ICT)

C---           Restaure la valeur originale
            VDLEV_P(ID,IN) = VDL_ORI

C---           (R(u + du_id) - R(u))/du
!dir$ IVDEP, LOOP COUNT(18)
            VKE(:,IDG) = (VRESP_P(:) - VRESE_P(:)) * VDL_INV

199         CONTINUE
         ENDDO
         ENDDO

C---        Assemble la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      RETURN
      END
