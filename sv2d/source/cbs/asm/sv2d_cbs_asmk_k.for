C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_ASMK_K
C
C Description:
C     La fonction SV2D_CBS_ASMK_K calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMK_K(VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VSOLC,
     &                           VSOLR,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           VCLDST,
     &                           KDIMP,
     &                           VDIMP,
     &                           KEIMP,
     &                           VDLG,
     &                           HMTX,
     &                           F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_ASMK_K
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'

      INTEGER NNELT, NDLNMAX, NDLEMAX
      PARAMETER (NNELT=6)
      PARAMETER (NDLNMAX=3)
      PARAMETER (NDLEMAX=NNELT*NDLNMAX)

      REAL*8    VKE(NDLEMAX*NDLEMAX)
      INTEGER   KLOCE(NDLEMAX)
      INTEGER   IERR
C----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN  .LE. NDLNMAX)
D     CALL ERR_PRE(LM_CMMN_NDLEV .LE. NDLEMAX)
C-----------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKE, KLOCE)
         CALL SV2D_CBS_ASMK_KV(KLOCE,
     &                         VKE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLR,
     &                         VDLG,
     &                         HMTX,
     &                         F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         CALL SV2D_CBS_ASMK_KS(KLOCE,
     &                         VKE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLR,
     &                         VDLG,
     &                         HMTX,
     &                         F_ASM)
      ENDIF

C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_ASMK(KLOCE,
     &                       VKE,
     &                       VCORG,
     &                       KLOCN,
     &                       KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       VPRES,
     &                       VSOLR,
     &                       KCLCND,
     &                       VCLCNV,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       VCLDST,
     &                       KDIMP,
     &                       VDIMP,
     &                       KEIMP,
     &                       VDLG,
     &                       HMTX,
     &                       F_ASM)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMK_KV
C
C Description:
C     La fonction SV2D_CBS_ASMK_KV calcul le matrice de rigidité
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMK_KV(KLOCE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
      INTEGER IC, IE, IN, II, NO

      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      PARAMETER (NNEL_LCL =  6)
      PARAMETER (NDLE_LCL = 18)
      PARAMETER (NPRN_LCL = 18)

      INTEGER KNE (NNEL_LCL)
      REAL*8  VDLE(NDLE_LCL)
      REAL*8  VPRN(NPRN_LCL * NNEL_LCL)
      REAL*8  PENA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        TRANSFERT DES CONNECTIVITÉS ÉLÉMENTAIRES
         DO IN=1,EG_CMMN_NNELV
            KNE(IN) = KNGV(IN,IE)
         ENDDO

C---        TRANSFERT DES DDL
         II=0
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            VDLE(II+1) = VDLG(1,NO)
            VDLE(II+2) = VDLG(2,NO)
            VDLE(II+3) = VDLG(3,NO)
            II=II+3
         ENDDO

C---        TRANSFERT DES PRNO
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        INITIALISE LA MATRICE ELEMENTAIRE
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)

C---        CALCUL DE LA MATRICE ELEMENTAIRE DE VOLUME
         CALL SV2D_CBS_KE_V(VKE,
     &                      VDJV(1,IE),
     &                      VPRGL,
     &                      VPRN,
     &                      VPREV(1,IE),
     &                      VDLE)

C---        Pénalisation en hh
         PENA = SV2D_PNUMR_PENALITE*VDJV(5,IE)
         VKE( 3, 3) = MAX(VKE( 3, 3), PENA)
         VKE( 9, 9) = MAX(VKE( 9, 9), PENA)
         VKE(15,15) = MAX(VKE(15,15), PENA)

C---        TABLE KLOCE DE L'ÉLÉMENT T6L
         DO IN=1,EG_CMMN_NNELV
            KLOCE(1, IN) = KLOCN(1, KNE(IN))
            KLOCE(2, IN) = KLOCN(2, KNE(IN))
            KLOCE(3, IN) = KLOCN(3, KNE(IN))
         ENDDO

C---        ASSEMBLAGE DE LA MATRICE
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM', ': ', IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_ASMK_KS
C
C Description:
C     La fonction SV2D_CBS_ASMK_KS calcul la matrice élémentaire due aux
C     éléments de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMK_KS(KLOCE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER NNEL_LCL
      INTEGER NDLN_LCL
      INTEGER NPRN_LCL
      PARAMETER (NNEL_LCL =  6)
      PARAMETER (NDLN_LCL =  3)
      PARAMETER (NPRN_LCL = 18)

      INTEGER IERR
      INTEGER IN, II, NO
      INTEGER IES, IEV, ICT
      REAL*8  VDLEV(NDLN_LCL, NNEL_LCL)
      REAL*8  VPRNV(NPRN_LCL * NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLN_LCL .EQ. LM_CMMN_NDLN)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO IES=1,EG_CMMN_NELS

C---        ELEMENT PARENT ET COTÉ
         IEV = KNGS(4,IES)
         ICT = KNGS(5,IES)

C---        TRANSFERT DES DDL
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN,IEV)
            VDLEV(1,IN) = VDLG(1,NO)
            VDLEV(2,IN) = VDLG(2,NO)
            VDLEV(3,IN) = VDLG(3,NO)
         ENDDO

C---        TRANSFERT DES PRNO
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN,IEV)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRNV(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        INITIALISE LA MATRICE ELEMENTAIRE
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)

C---        [K(u)]
         CALL SV2D_CBS_KE_S(VKE,
     &                      VDJV(1,IEV),
     &                      VDJS(1,IES),
     &                      VPRGL,
     &                      VPRNV,
     &                      VPREV(1,IEV),
     &                      VPRES(1,IES), !pas utilisé
     &                      VDLEV,
     &                      ICT)

C---        TABLE KLOCE DE L'ÉLÉMENT T6L
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN,IEV)
            KLOCE(1,IN) = KLOCN(1,NO)
            KLOCE(2,IN) = KLOCN(2,NO)
            KLOCE(3,IN) = KLOCN(3,NO)
         ENDDO

C---       ASSEMBLAGE DE LA MATRICE
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      RETURN
      END
