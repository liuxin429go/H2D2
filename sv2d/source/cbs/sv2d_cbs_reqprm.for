C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_REQPRM
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_REQPRM
C
C Description:
C     PARAMETRES DE L'ELEMENT
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les PRE sont encodée suivant leurs deux dimensions:
C     2 PRE pour chacun des 4 sous-éléments: 2*1000 + 4
C************************************************************************
      SUBROUTINE SV2D_CBS_REQPRM(TGELV,
     &                           NPRGL,
     &                           NPRGLL,
     &                           NPRNO,
     &                           NPRNOL,
     &                           NPREV,
     &                           NPREVL,
     &                           NPRES,
     &                           NSOLC,
     &                           NSOLCL,
     &                           NSOLR,
     &                           NSOLRL,
     &                           NDLN,
     &                           NDLEV,
     &                           NDLES,
     &                           ASURFACE,
     &                           ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_REQPRM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER TGELV

      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES

      LOGICAL ASURFACE
      LOGICAL ESTLIN

      INCLUDE 'eacnst.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'eacmmn.fc'
C-----------------------------------------------------------------------

C---     Initialise les paramètres invariant de l'élément parent virtuel
      TGELV  =  EG_TPGEO_T6L          ! TYPE DE GEOMETRIE DE L'ELEMENT DE VOLUME

C---     Initialise les paramètres variables sujets à changement chez l'élément héritier
      NPRGLL = 31                     ! NB DE PROPRIÉTÉS GLOBALES LUES
      NPRGL  = NPRGLL + 1             ! NB DE PROPRIÉTÉS GLOBALES
      NPRNO  = 15                     ! NB DE PROPRIÉTÉS NODALES
      NPRNOL =  6                     ! NB DE PROPRIÉTÉS NODALES LUES
      NPREV  =  2004                  ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME
      NPREVL =  0                     ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME LUES
      NPRES  =  3002                  ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE SURFACE
      NSOLC  =  3                     ! NB DE SOLLICITATIONS CONCENTRÉES
      NSOLCL =  3                     ! NB DE SOLLICITATIONS CONCENTRÉES LUES
      NSOLR  =  3                     ! NB DE SOLLICITATIONS RÉPARTIES
      NSOLRL =  3                     ! NB DE SOLLICITATIONS RÉPARTIES LUES
      NDLN   =  3                     ! NB DE DDL PAR NOEUD
      NDLEV  = 18                     ! NB DE DDL PAR ELEMENT DE VOLUME
      NDLES  =  9                     ! NB DE DDL PAR ELEMENT DE SURFACE

      ASURFACE = .TRUE.
      ESTLIN   = .FALSE.

      RETURN
      END

