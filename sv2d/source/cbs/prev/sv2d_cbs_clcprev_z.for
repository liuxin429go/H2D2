C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_CLCPREVE_Z
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:  SV2D_CBS_CLCPREVE_Z
C
C Description:
C     Calcul des propriétés élémentaires des éléments de volume pour
C     un élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_CLCPREVE_Z(VCORG,
     &                               KNE,
     &                               VDJE,
     &                               VPRGL,
     &                               VPRNO,
     &                               VPRE,
     &                               VDLG,
     &                               IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_CLCPREVE_Z
CDEC$ ENDIF

      USE COMPLEXIFY
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8     VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER    KNE  (EG_CMMN_NCELV)
      REAL*8     VDJE (EG_CMMN_NDJV)
      REAL*8     VPRGL(LM_CMMN_NPRGL)
      COMPLEX*16 VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      COMPLEX*16 VPRE (LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      COMPLEX*16 VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sphdro_z.fi'

      INTEGER    NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8     VKX, VEX, VSX, VKY, VEY, VSY, DETJT3, UN_DT3
      COMPLEX*16 P1, P2, P3, P4, P5, P6
      COMPLEX*16 U1, U2, U3, U4, U5, U6
      COMPLEX*16 V1, V2, V3, V4, V5, V6
      COMPLEX*16 PM1, PM2, PM3, PM4
      COMPLEX*16 UM1, UM2, UM3, UM4
      COMPLEX*16 VM1, VM2, VM3, VM4
      COMPLEX*16 UX1, UX2, UX3, UX4
      COMPLEX*16 UY1, UY2, UY3, UY4
      COMPLEX*16 VX1, VX2, VX3, VX4
      COMPLEX*16 VY1, VY2, VY3, VY4
      COMPLEX*16 VT1, VT2, VT3, VT4
      COMPLEX*16 VIS1, VIS2, VIS3, VIS4
      COMPLEX*16 VNM1, VNM2, VNM3, VNM4
      COMPLEX*16 CLMFIN, CSMFIN
C-----------------------------------------------------------------------

C---     Connectivités du T6
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)
      NO4 = KNE(4)
      NO5 = KNE(5)
      NO6 = KNE(6)

C---     Métriques des T3
      VKX = VDJE(1)*UN_2
      VEX = VDJE(2)*UN_2
      VKY = VDJE(3)*UN_2
      VEY = VDJE(4)*UN_2
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)

C---     Déterminant du T3
      DETJT3 = UN_4*VDJE(5)
      UN_DT3 = UN / DETJT3

C---     Valeurs nodales
      U1 = VPRNO(SV2D_IPRNO_U,NO1)     ! NOEUD 1
      V1 = VPRNO(SV2D_IPRNO_V,NO1)
      P1 = VPRNO(SV2D_IPRNO_H,NO1)
      U2 = VPRNO(SV2D_IPRNO_U,NO2)     ! NOEUD 2
      V2 = VPRNO(SV2D_IPRNO_V,NO2)
      P2 = VPRNO(SV2D_IPRNO_H,NO2)
      U3 = VPRNO(SV2D_IPRNO_U,NO3)     ! NOEUD 3
      V3 = VPRNO(SV2D_IPRNO_V,NO3)
      P3 = VPRNO(SV2D_IPRNO_H,NO3)
      U4 = VPRNO(SV2D_IPRNO_U,NO4)     ! NOEUD 4
      V4 = VPRNO(SV2D_IPRNO_V,NO4)
      P4 = VPRNO(SV2D_IPRNO_H,NO4)
      U5 = VPRNO(SV2D_IPRNO_U,NO5)     ! NOEUD 5
      V5 = VPRNO(SV2D_IPRNO_V,NO5)
      P5 = VPRNO(SV2D_IPRNO_H,NO5)
      U6 = VPRNO(SV2D_IPRNO_U,NO6)     ! NOEUD 6
      V6 = VPRNO(SV2D_IPRNO_V,NO6)
      P6 = VPRNO(SV2D_IPRNO_H,NO6)

C---     Vitesses moyenne en X
      UM1 = (U1+U2+U6)*UN_3
      UM2 = (U2+U3+U4)*UN_3
      UM3 = (U6+U4+U5)*UN_3
      UM4 = (U4+U6+U2)*UN_3

C---     Vitesses moyenne en Y
      VM1 = (V1+V2+V6)*UN_3
      VM2 = (V2+V3+V4)*UN_3
      VM3 = (V6+V4+V5)*UN_3
      VM4 = (V4+V6+V2)*UN_3

C---     Profondeur moyenne
      PM1 = (P1+P2+P6)*UN_3
      PM2 = (P2+P3+P4)*UN_3
      PM3 = (P6+P4+P5)*UN_3
      PM4 = (P4+P6+P2)*UN_3

C---     Dérivé en X de U sur les 4 T3
      UX1 = VKX*(U2-U1)+VEX*(U6-U1)
      UX2 = VKX*(U3-U2)+VEX*(U4-U2)
      UX3 = VKX*(U4-U6)+VEX*(U5-U6)
      UX4 = -(VKX*(U6-U4)+VEX*(U2-U4))

C---     Dérivé en Y de U sur les 4 T3
      UY1 = VKY*(U2-U1)+VEY*(U6-U1)
      UY2 = VKY*(U3-U2)+VEY*(U4-U2)
      UY3 = VKY*(U4-U6)+VEY*(U5-U6)
      UY4 = - (VKY*(U6-U4)+VEY*(U2-U4))

C---     Dérivé en X de V sur les 4 T3
      VX1 = VKX*(V2-V1)+VEX*(V6-V1)
      VX2 = VKX*(V3-V2)+VEX*(V4-V2)
      VX3 = VKX*(V4-V6)+VEX*(V5-V6)
      VX4 = -(VKX*(V6-V4)+VEX*(V2-V4))

C---     Dérivé en Y de V sur les 4 T3
      VY1 = VKY*(V2-V1)+VEY*(V6-V1)
      VY2 = VKY*(V3-V2)+VEY*(V4-V2)
      VY3 = VKY*(V4-V6)+VEY*(V5-V6)
      VY4 = -(VKY*(V6-V4)+VEY*(V2-V4))

C---     Coefficients finaux (longueur de mélange & Smagorinsky)
      CLMFIN = SV2D_VISCO_LM
      CSMFIN = SV2D_VISCO_SMGO*SQRT(DETJT3)

C---     Viscosité turbulente finale sur les T3
      VT1 = CLMFIN*PM1 + CSMFIN
      VT2 = CLMFIN*PM2 + CSMFIN
      VT3 = CLMFIN*PM3 + CSMFIN
      VT4 = CLMFIN*PM4 + CSMFIN

C---     Calcul de la viscosité physique
      VIS1 = SV2D_VISCO_CST
     &     + UN_DT3*VT1*VT1*
     &          SQRT(DEUX*UX1*UX1 + DEUX*VY1*VY1 + (UY1+VX1)*(UY1+VX1))
      VIS2 = SV2D_VISCO_CST
     &     + UN_DT3*VT2*VT2*
     &          SQRT(DEUX*UX2*UX2 + DEUX*VY2*VY2 + (UY2+VX2)*(UY2+VX2))
      VIS3 = SV2D_VISCO_CST
     &     + UN_DT3*VT3*VT3*
     &          SQRT(DEUX*UX3*UX3 + DEUX*VY3*VY3 + (UY3+VX3)*(UY3+VX3))
      VIS4 = SV2D_VISCO_CST
     &     + UN_DT3*VT4*VT4*
     &          SQRT(DEUX*UX4*UX4 + DEUX*VY4*VY4 + (UY4+VX4)*(UY4+VX4))

C---     Limiteurs sur la viscosité
      VIS1 = MAX(MIN(VIS1, SV2D_VISCO_BSUP), SV2D_VISCO_BINF)
      VIS2 = MAX(MIN(VIS2, SV2D_VISCO_BSUP), SV2D_VISCO_BINF)
      VIS3 = MAX(MIN(VIS3, SV2D_VISCO_BSUP), SV2D_VISCO_BINF)
      VIS4 = MAX(MIN(VIS4, SV2D_VISCO_BSUP), SV2D_VISCO_BINF)

C---     Calcul de la viscosité numérique
      VNM1 = SP_HDRO_PECLET_Z(UN, UM1, VM1, PM1, VEY, -VEX, -VKY, VKX)
     &     / SV2D_STABI_PECLET
      VNM2 = SP_HDRO_PECLET_Z(UN, UM2, VM2, PM2, VEY, -VEX, -VKY, VKX)
     &     / SV2D_STABI_PECLET
      VNM3 = SP_HDRO_PECLET_Z(UN, UM3, VM3, PM3, VEY, -VEX, -VKY, VKX)
     &     / SV2D_STABI_PECLET
      VNM4 = SP_HDRO_PECLET_Z(UN, UM4, VM4, PM4, VEY, -VEX, -VKY, VKX)
     &     / SV2D_STABI_PECLET

C---     Propriété élémentaire
      VPRE(1,1) = VIS1                   ! Visco physique
      VPRE(2,1) = VPRE(1,1) + VNM1       ! Visco totale
      VPRE(1,2) = VIS2
      VPRE(2,2) = VPRE(1,2) + VNM2
      VPRE(1,3) = VIS3
      VPRE(2,3) = VPRE(1,3) + VNM3
      VPRE(1,4) = VIS4
      VPRE(2,4) = VPRE(1,4) + VNM4

      IERR = ERR_TYP()
      RETURN
      END
