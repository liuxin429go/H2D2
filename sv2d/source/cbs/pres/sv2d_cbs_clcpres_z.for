C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_CLCPRESE_Z
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:  SV2D_CBS_CLCPRESE
C
C Description:
C     Calcul des propriétés élémentaires de surface sur un élément
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On calcule les contraintes sur les 3 sous-éléments externes du T6L
C     avant d'assembler uniquement les deux de l'arête.
C************************************************************************
      SUBROUTINE SV2D_CBS_CLCPRESE_Z(ICT,
!     &                               VCORG,
     &                               KNGV,
     &                               VDJV,
!     &                               VDJS,
!     &                               VPRGL,
     &                               VPRNO,
     &                               VPREV,
     &                               VPRES, !sortie
!     &                               VDLG,
     &                               IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_CLCPRESE_Z
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER    ICT
!      REAL*8     VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER    KNGV (EG_CMMN_NCELV)
      REAL*8     VDJV (EG_CMMN_NDJV)
!      REAL*8     VDJS (EG_CMMN_NDJS)
!      REAL*8     VPRGL(LM_CMMN_NPRGL)
      COMPLEX*16 VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      COMPLEX*16 VPREV(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      COMPLEX*16 VPRES(LM_CMMN_NPRES_D1, LM_CMMN_NPRES_D2) !sortie
!      COMPLEX*16 VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      INTEGER IET3_1, IET3_2
      INTEGER KT3(2, 3)       ! 2 sous-elem T3, 3 cotés

      REAL*8     VKX, VEX, VKY, VEY
      REAL*8     UN_DT3
      COMPLEX*16 U1, V1, U2, V2, U3, V3, U4, V4, U5, V5, U6, V6
      COMPLEX*16 UX1, UX2, UX3, UY1, UY2, UY3
      COMPLEX*16 VX1, VX2, VX3, VY1, VY2, VY3
      COMPLEX*16 VIS1, VIS2, VIS3
      COMPLEX*16 TAUXX(3), TAUXY(3), TAUYY(3)

      DATA KT3 / 1,2, 2,3, 3,1/
C-----------------------------------------------------------------------

C---     Connectivités du T6
      NO1 = KNGV(1)
      NO2 = KNGV(2)
      NO3 = KNGV(3)
      NO4 = KNGV(4)
      NO5 = KNGV(5)
      NO6 = KNGV(6)

C---     Métriques des T3 et T6L
      VKX    = UN_2*VDJV(1)
      VEX    = UN_2*VDJV(2)
      VKY    = UN_2*VDJV(3)
      VEY    = UN_2*VDJV(4)
      UN_DT3 = UN_4/VDJV(5)

C---     Variables nodales
      U1 = VPRNO(SV2D_IPRNO_U,NO1)
      V1 = VPRNO(SV2D_IPRNO_V,NO1)
      U2 = VPRNO(SV2D_IPRNO_U,NO2)
      V2 = VPRNO(SV2D_IPRNO_V,NO2)
      U3 = VPRNO(SV2D_IPRNO_U,NO3)
      V3 = VPRNO(SV2D_IPRNO_V,NO3)
      U4 = VPRNO(SV2D_IPRNO_U,NO4)
      V4 = VPRNO(SV2D_IPRNO_V,NO4)
      U5 = VPRNO(SV2D_IPRNO_U,NO5)
      V5 = VPRNO(SV2D_IPRNO_V,NO5)
      U6 = VPRNO(SV2D_IPRNO_U,NO6)
      V6 = VPRNO(SV2D_IPRNO_V,NO6)

C---     Dérivée en X de U sur les 3 T3 externes
      UX1 = VKX*(U2-U1)+VEX*(U6-U1)
      UX2 = VKX*(U3-U2)+VEX*(U4-U2)
      UX3 = VKX*(U4-U6)+VEX*(U5-U6)

C---     Dérivée en Y de U sur les 3 T3 externes
      UY1 = VKY*(U2-U1)+VEY*(U6-U1)
      UY2 = VKY*(U3-U2)+VEY*(U4-U2)
      UY3 = VKY*(U4-U6)+VEY*(U5-U6)

C---     Dérivée en X de V sur les 3 T3 externes
      VX1 = VKX*(V2-V1)+VEX*(V6-V1)
      VX2 = VKX*(V3-V2)+VEX*(V4-V2)
      VX3 = VKX*(V4-V6)+VEX*(V5-V6)

C---     Dérivée en Y de V sur les 3 T3 externes
      VY1 = VKY*(V2-V1)+VEY*(V6-V1)
      VY2 = VKY*(V3-V2)+VEY*(V4-V2)
      VY3 = VKY*(V4-V6)+VEY*(V5-V6)

C---        Viscosité
      VIS1 = VPREV(1,1)  ! visco physique = 1
      VIS2 = VPREV(1,2)  ! visco totale   = 2
      VIS3 = VPREV(1,3)

C---     TAUXX
      TAUXX(1) = UX1*VIS1*DEUX
      TAUXX(2) = UX2*VIS2*DEUX
      TAUXX(3) = UX3*VIS3*DEUX

C---     TAUXY
      TAUXY(1) = (UY1 + VX1)*VIS1
      TAUXY(2) = (UY2 + VX2)*VIS2
      TAUXY(3) = (UY3 + VX3)*VIS3

C---     TAUYY
      TAUYY(1) = VY1*VIS1*DEUX
      TAUYY(2) = VY2*VIS2*DEUX
      TAUYY(3) = VY3*VIS3*DEUX

C---     Sous-éléments
      IET3_1 = KT3(1, ICT)
      IET3_2 = KT3(2, ICT)

C---     Contraintes sur les deux sous-éléments
      VPRES(1,1) = TAUXX(IET3_1)*UN_DT3
      VPRES(2,1) = TAUYY(IET3_1)*UN_DT3
      VPRES(3,1) = TAUXY(IET3_1)*UN_DT3
      VPRES(1,2) = TAUXX(IET3_2)*UN_DT3
      VPRES(2,2) = TAUYY(IET3_2)*UN_DT3
      VPRES(3,2) = TAUXY(IET3_2)*UN_DT3

      IERR = ERR_TYP()
      RETURN
      END

