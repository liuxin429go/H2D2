C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction SV2D_CBS_REQFNC retourne le handle de la fonction
C     associée au code demandé.
C
C Entrée:
C     HVFT     Handle sur la table virtuelle
C     IFNC     Code de la fonction
C
C Sortie:
C     HFNC     Handle sur la fonction
C
C Notes:
C     On garde le handle sur la table virtuelle pour pouvoir répondre
C     aux demandes de ASMKT, par exemple. On table sur le fait qu'un
C     appel à REQFNC d'un héritier vient toujours avant un appel
C     direct.
C************************************************************************
      FUNCTION SV2D_CBS_REQFNC(HVFT, IFNC, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_REQFNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HVFT
      INTEGER IFNC
      INTEGER HFNC

      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(HVFT .EQ. 0 .OR. SO_VTBL_HVALIDE(HVFT))
C-----------------------------------------------------------------------

      IF (HVFT .NE. 0) SV2D_VT_HVFT_ACTU = HVFT
D     CALL ERR_ASR(SO_VTBL_HVALIDE(SV2D_VT_HVFT_ACTU))
      IERR = SO_VTBL_REQFNC(SV2D_VT_HVFT_ACTU, IFNC, HFNC)

      SV2D_CBS_REQFNC = ERR_TYP()
      RETURN
      END
