C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_CLCDLIB
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_CLCDLIB
C
C Description:
C     La fonction SV2D_CBS_CLCDLIB
C        CaLCul sur les Degrés de LIBerté
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_CLCDLIB(KNGV,
     &                            KLOCN,
     &                            KDIMP,
     &                            VDIMP,
     &                            VDLG,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_CLCDLIB
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KLOCN(LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'eacnst.fi'

      INTEGER IC, IE, ID
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
      LOGICAL EST_PHANTOME
      EST_PHANTOME(ID) = BTEST(ID, EA_TPCL_PHANTOME)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(NO1, NO2, NO3)

C---     Assigne les noeuds milieux en niveau d'eau
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         NO2 = KNGV(2,IE)
         IF (EST_PHANTOME(KDIMP(3,NO2))) THEN
            NO1 = KNGV(1,IE)
            NO3 = KNGV(3,IE)
            VDLG(3,NO2) = UN_2*(VDLG(3,NO1) + VDLG(3,NO3))
         ENDIF

         NO2 = KNGV(4,IE)
         IF (EST_PHANTOME(KDIMP(3,NO2))) THEN
            NO1 = KNGV(3,IE)
            NO3 = KNGV(5,IE)
            VDLG(3,NO2) = UN_2*(VDLG(3,NO1) + VDLG(3,NO3))
         ENDIF

         NO2 = KNGV(6,IE)
         IF (EST_PHANTOME(KDIMP(3,NO2))) THEN
            NO1 = KNGV(5,IE)
            NO3 = KNGV(1,IE)
            VDLG(3,NO2) = UN_2*(VDLG(3,NO1) + VDLG(3,NO3))
         ENDIF

      ENDDO
!$omp end do
      ENDDO

!$omp end parallel

      IERR = ERR_TYP()
      RETURN
      END
