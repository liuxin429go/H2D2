C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Ajoute une fonction à la table virtuelle
C
C Description:
C     La fonction SV2D_CBS_AJTFSO ajoute la fonction d'identifiant ID
C     et de nom FN à la table virtuelle de handle H.
C
C Entrée:
C     H     Handle sur la table
C     ID    Identifiant de la fonction
C     FN    Nom de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CBS_AJTFSO(H, ID, FN)

      IMPLICIT NONE

      INTEGER H
      INTEGER ID
      CHARACTER*(*) FN

      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
      CHARACTER*(4) DLL
      DATA DLL /'sv2d'/
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(H))
C-----------------------------------------------------------------------

      IF (ERR_GOOD())
     &   IERR = SO_VTBL_AJTFSO(H, ID, DLL, FN(1:SP_STRN_LEN(FN)))

      SV2D_CBS_AJTFSO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction protégée SV2D_CBS_INIVTBL remplis la table virtuelle
C     (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CBS_INIVTBL(H)

      IMPLICIT NONE

      INTEGER H

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Les fonctions de l'interface
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_ASMF,    'SV2D_CBS_ASMF_K')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_ASMK,    'SV2D_CBS_ASMK_K')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_ASMKT,   'SV2D_CBS_ASMKT_K')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_ASMKU,   'SV2D_CBS_ASMKU_K')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_ASMM,    'SV2D_CBS_ASMM')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_ASMMU,   'SV2D_CBS_ASMMU')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_CLCCLIM, 'SV2D_CBS_CLCCLIM')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_CLCDLIB, 'SV2D_CBS_CLCDLIB')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_CLCPRES, 'SV2D_CBS_CLCPRES')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_CLCPREV, 'SV2D_CBS_CLCPREV')
C     IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_CLCPRNO, 'ERR_FNCT_A_SPECIALISER')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRCCLIM, 'SV2D_CBS_PRCCLIM')
C     IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRCDLIB, 'ERR_FNCT_A_SPECIALISER')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRCPRES, 'SV2D_CBS_PRCPRES')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRCPREV, 'SV2D_CBS_PRCPREV')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRCPRGL, 'SV2D_CBS_PRCPRGL')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRCPRNO, 'SV2D_CBS_PRCPRNO')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRCSOLC, 'SV2D_CBS_PRCSOLC')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRCSOLR, 'SV2D_CBS_PRCSOLR')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_HLPCLIM, 'SV2D_CBS_HLPCLIM')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_HLPPRGL, 'SV2D_CBS_HLPPRGL')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_HLPPRNO, 'SV2D_CBS_HLPPRNO')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRNCLIM, 'SV2D_CBS_PRNCLIM')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRNPRGL, 'SV2D_CBS_PRNPRGL')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PRNPRNO, 'SV2D_CBS_PRNPRNO')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PSCPRNO, 'SV2D_CBS_PSCPRNO')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PSLCLIM, 'SV2D_CBS_PSLCLIM')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PSLDLIB, 'SV2D_CBS_PSLDLIB')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PSLPREV, 'SV2D_CBS_PSLPREV')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PSLPRGL, 'SV2D_CBS_PSLPRGL')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PSLPRNO, 'SV2D_CBS_PSLPRNO')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PSLSOLC, 'SV2D_CBS_PSLSOLC')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_PSLSOLR, 'SV2D_CBS_PSLSOLR')
!      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_REQPRM,  'ERR_FNCT_A_SPECIALISER')
      IERR = SV2D_CBS_AJTFSO(H, EA_FUNC_DMPPROP, 'SV2D_CBS_DMPPROP')

C---     Les fonctions spécifiques à l'élément
C     IERR = SV2D_CBS_AJTFSO(H, SV2D_VT_CLCPRNEV,'ERR_FNCT_A_SPECIALISER')
C     IERR = SV2D_CBS_AJTFSO(H, SV2D_VT_CLCPRNES,'ERR_FNCT_A_SPECIALISER')
      IERR = SV2D_CBS_AJTFSO(H, SV2D_VT_CLCPREVE,'SV2D_CBS_CLCPREVE')
      IERR = SV2D_CBS_AJTFSO(H, SV2D_VT_CLCPRESE,'SV2D_CBS_CLCPRESE')

      SV2D_CBS_INIVTBL = ERR_TYP()
      RETURN
      END
