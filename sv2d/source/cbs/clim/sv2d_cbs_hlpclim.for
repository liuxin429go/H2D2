C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_HLPCLIM
C   Private:
C     INTEGER SV2D_CBS_HLP1
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_HLPCLIM
C
C Description:
C     Aide sur les conditions limites
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_HLPCLIM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_HLPCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Imprime entête
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_CLIM_ST_VENANT:')
      CALL LOG_INCIND()

      IERR = SV2D_CL_HLP()

      CALL LOG_DECIND()

      RETURN
      END
