C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  SV2D_CBS_CLCCLIM
C
C Description:
C     CALCUL DES CONDITIONS AUX LIMITES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_CLCCLIM(VCORG,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_CLCCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER  IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'

      INTEGER SV2D_CBS_CLCCLIM_S
C-----------------------------------------------------------------------

C---     Contributions de éléments de surface
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CBS_CLCCLIM_S(VCORG,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             KCLCND,
     &                             VCLCNV,
     &                             KCLLIM,
     &                             KCLNOD,
     &                             KCLELE,
     &                             VCLDST,
     &                             KDIMP,
     &                             VDIMP,
     &                             KEIMP,
     &                             VDLG)
      ENDIF

C---     Contributions des conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_CLC(VCORG,
     &                      KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VPRES,
     &                      KCLCND,
     &                      VCLCNV,
     &                      KCLLIM,
     &                      KCLNOD,
     &                      KCLELE,
     &                      VCLDST,
     &                      KDIMP,
     &                      VDIMP,
     &                      KEIMP,
     &                      VDLG)
      ENDIF

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_CLCCLIM
C
C Description:
C     CALCUL DES CONDITIONS AUX LIMITES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CBS_CLCCLIM_S(VCORG,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER SV2D_CBS_CLCCLIM_S
      INTEGER IERR

      REAL*8 VNX, VNY
      REAL*8 QX1, QY1, QX2, QY2
      REAL*8 QN1, QN2
      INTEGER IEP, ID, IN, NP1, NP2, IE
      LOGICAL ESTCAUCHY
      LOGICAL ESTOUVERT
      LOGICAL ESTENTRANT
      CHARACTER*(48) TYPCL
C-----------------------------------------------------------------------
      LOGICAL DDL_ESTCAUCHY
      LOGICAL DDL_ESTOUVERT
      DDL_ESTCAUCHY(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_CAUCHY)
      DDL_ESTOUVERT(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      CALL IINIT(EG_CMMN_NELS, 0, KEIMP, 1)

C---     Boucle sur les éléments de surface
C        ==================================
      DO IEP=1,EG_CMMN_NELS
         NP1 = KNGS(1,IEP)
         NP2 = KNGS(2,IEP)

C---        Métriques
         VNX =  VDJS(2,IEP)
         VNY = -VDJS(2,IEP)

C---        Débits normaux
         QX1 = VDLG(1,NP1)
         QY1 = VDLG(2,NP1)
         QX2 = VDLG(1,NP2)
         QY2 = VDLG(2,NP2)
         QN1 = QX1*VNX + QY1*VNY
         QN2 = QX2*VNX + QY2*VNY

C---        Les limites de Cauchy
         ESTCAUCHY  = (DDL_ESTCAUCHY(3,NP1) .AND. DDL_ESTCAUCHY(3,NP2))
         IF (ESTCAUCHY) THEN
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_CAUCHY)
            ESTENTRANT = (QN1 .LE. ZERO .OR. QN2 .LE. ZERO)
            IF (ESTENTRANT) THEN
               KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_ENTRANT)
            ELSE
               KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_SORTANT)
            ENDIF
         ENDIF

C---        Les limites "OUVERT"
         ESTOUVERT  = (DDL_ESTOUVERT(3,NP1) .AND. DDL_ESTOUVERT(3,NP2))
         IF (ESTOUVERT) THEN
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_OUVERT)
            ESTENTRANT = (QN1 .LT. ZERO .AND. QN2 .LT. ZERO)
            IF (ESTENTRANT) THEN
               KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_ENTRANT)
            ELSE
               KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_SORTANT)
            ENDIF
         ENDIF

      ENDDO

C---   IMPRESSION DES CONNECTIVITÉS DE LA PEAU DES FLUX DE
C      CAUCHY(ENTRE), OUVERT(NUL+SORT), DIRICHLET+NEUMAN(DÉFAUT)
      IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
         CALL LOG_ECRIS(' ')
         CALL LOG_ECRIS(' ')
         WRITE(LOG_BUF,'(A)') 'MSG_TYPE_CLIM_ELEMENTS_NOEUDS'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
         DO IEP=1,EG_CMMN_NELS

            IF     (BTEST(KEIMP(IEP), EA_TPCL_CAUCHY) .AND.
     &              BTEST(KEIMP(IEP), EA_TPCL_SORTANT)) THEN
               IE = KNGS(3,IEP)
               WRITE(LOG_BUF,2050) IEP,NP1,NP2,
     &                          IE,KNGV(1,IE),KNGV(2,IE),KNGV(3,IE)
               CALL LOG_ECRIS(LOG_BUF)
            ELSEIF (BTEST(KEIMP(IEP), EA_TPCL_CAUCHY) .AND.
     &              BTEST(KEIMP(IEP), EA_TPCL_ENTRANT)) THEN
               TYPCL = 'MSG_CL_CAUCHY_ENTRANT#<40>#'
            ELSEIF (BTEST(KEIMP(IEP), EA_TPCL_OUVERT) .AND.
     &              BTEST(KEIMP(IEP), EA_TPCL_ENTRANT)) THEN
               TYPCL = 'MSG_CL_OUVERT_ENTRANT#<40>#'
            ELSEIF (BTEST(KEIMP(IEP), EA_TPCL_OUVERT) .AND.
     &              BTEST(KEIMP(IEP), EA_TPCL_SORTANT)) THEN
               TYPCL = 'MSG_CL_OUVERT_SORTANT#<40>#'
            ELSE
               TYPCL = 'MSG_CL_DEFAUT#<40>#'
            ENDIF
            WRITE(LOG_BUF,'(A,2X,I9,3X,3(1X,I9))')
     &            TYPCL(1:SP_STRN_LEN(TYPCL)),
     &            IEP, KNGS(1,IEP), KNGS(2,IEP), KNGS(3,IEP)
            CALL LOG_ECRIS(LOG_BUF)
         ENDDO
         CALL LOG_DECIND()
      ENDIF

C---  IMPRESSION DE L'INFO
C      WRITE(MP,1000)
C      WRITE(MP,1001)'TRAITEMENT DES ELEMENTS DE CONTOUR:'
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DE CAUCHY       =',
C     &               NELC
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. OUVERT          =',
C     &               NELS
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DIRICHLET-NEUMAN=',
C     &               EG_CMMN_NELS-NELC-NELS
C      WRITE(MP,1000)

C---------------------------------------------------------------
1000  FORMAT(/)
1001  FORMAT(15X,A)
1002  FORMAT(15X,A,I12)
2050  FORMAT(2X,'AVERTISSEMENT ---  APPLICATION D''UNE CONDITION DE',/,
     &       2X,'   NEUMAN PAR DEFAUT; LA CONDITION DE CAUCHY-ENTRANT',
     &          '   N''EST PAS APPLICABLE SUR UN FLUX SORTANT',/,
     &       2X,'ELEMENT DE CONTOUR',I12,/,
     &       2X,'NOEUDS DE CONTOUR ',2(1X,I12),/,
     &       2X,'ELEMENT PARENT',I12,/,
     &       2X,'CONNECTIVITE DU PARENT',3(1X,I12),/)
C---------------------------------------------------------------

      SV2D_CBS_CLCCLIM_S = ERR_TYP()
      RETURN
      END
