C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER SV2D_CBS_ASGCMN
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Assemble
C
C Description:
C     La fonction SV2D_CBS_ASGCMN copie les valeurs du common externe
C     passés en paramètre sous la forme d'un vecteur sur les valeurs
C     du common local à la DLL.
C
C Entrée:
C     KA       Les valeurs du common
C
C Sortie:
C
C Notes:
C     Les common(s) sont privés au DLL, il faut donc propager les
C     changement entre l'externe et l'interne.
C************************************************************************
      FUNCTION SV2D_CBS_ASGCMN(EG_KA, EA_KA, EA_VA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_ASGCMN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER SV2D_CBS_ASGCMN
      INTEGER EG_KA(*)
      INTEGER EA_KA(*)
      REAL*8  EA_VA(*) 

      INCLUDE 'err.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'
      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER I
      INTEGER IERR
C-----------------------------------------------------------------------

      DO I=1, EG_CMMN_KA_DIM
         EG_CMMN_KA(I) = EG_KA(I)
      ENDDO

      DO I=1, LM_CMMN_KA_DIM
         LM_CMMN_KA(I) = EA_KA(I)
      ENDDO

      DO I=1, LM_CMMN_VA_DIM
         LM_CMMN_VA(I) = EA_VA(I)
      ENDDO

      IERR = SV2D_LGCY_CMN2CLS() 
      
      SV2D_CBS_ASGCMN = ERR_TYP()
      RETURN
      END
