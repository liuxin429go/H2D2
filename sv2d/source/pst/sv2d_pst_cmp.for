C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_PST_CMP_000
C     SUBROUTINE SV2D_PST_CMP_999
C     SUBROUTINE SV2D_PST_CMP_CTR
C     SUBROUTINE SV2D_PST_CMP_DTR
C     SUBROUTINE SV2D_PST_CMP_INI
C     SUBROUTINE SV2D_PST_CMP_RST
C     SUBROUTINE SV2D_PST_CMP_REQHBASE
C     SUBROUTINE SV2D_PST_CMP_HVALIDE
C     SUBROUTINE SV2D_PST_CMP_ACC
C     SUBROUTINE SV2D_PST_CMP_FIN
C     SUBROUTINE SV2D_PST_CMP_XEQ
C     SUBROUTINE SV2D_PST_CMP_ASGHSIM
C     SUBROUTINE SV2D_PST_CMP_REQHVNO
C     SUBROUTINE SV2D_PST_CMP_REQNOMF
C   Private:
C     SUBROUTINE SV2D_PST_CMP_CLC
C     SUBROUTINE SV2D_PST_CMP_CLC2
C     SUBROUTINE SV2D_PST_CMP_LOG
C
C************************************************************************

      MODULE SV2D_PST_CMP_M

      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:  SV2D_PST_CMP_CLC2
C
C Description:
C     La fonction privée SV2D_PST_CMP_CLC2 fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER 
     &FUNCTION SV2D_PST_CMP_CLC2_O(HOBJ,
     &                             GDTA,
     &                             EDTA,
     &                             IPRG,
     &                             IPRN,
     &                             NPST,
     &                             NNL,
     &                             VPOST,
     &                             VTRV,
     &                             VFE)

      USE SV2D_XBS_M
      USE SV2D_CBS_RE_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      TYPE (LM_GDTA_DATA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_DATA_T), INTENT(IN) :: EDTA
      TYPE (SV2D_IPRG_T),    INTENT(IN) :: IPRG
      TYPE (SV2D_IPRN_T),    INTENT(IN) :: IPRN
      INTEGER, INTENT(IN) :: NPST
      INTEGER, INTENT(IN) :: NNL
      REAL*8, INTENT(INOUT) :: VPOST (NPST, NNL)
      REAL*8, INTENT(INOUT) :: VTRV  (NNL)
      REAL*8, INTENT(INOUT) :: VFE   (:,:)

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER, PARAMETER :: NRID = 19
      INTEGER :: KRID(NRID)
      REAL*8, PARAMETER :: U = 1.0D0
      REAL*8, PARAMETER :: Z = 0.0D0
      REAL*8, PARAMETER :: G = 1.0D99
      REAL*8 :: VRID(NRID, SV2D_PST_CMP_NRES) = reshape (
C           VISCO     |       DRY/WET        | STAB |     CMULT     |
C      CST,LM,DJ,BI,BM|n,DMP,CNV,GRA,nu,Pe,Dc|Pe,DMP|CNV,GRA,MAN,VNT|CORIOLIS
     & (/
     &  Z, Z, Z, Z, G,   Z, Z, U, Z, Z, G, Z,   G, Z,   U, Z, Z, Z,   Z,  ! convection
     &  Z, Z, Z, Z, G,   Z, Z, Z, U, Z, G, Z,   G, Z,   Z, U, Z, Z,   Z,  ! gravité
     &  Z, Z, Z, Z, G,   U, Z, Z, Z, Z, G, Z,   G, Z,   Z, Z, U, Z,   Z,  ! manning
     &  U, U, U, U, U,   Z, Z, Z, Z, Z, G, Z,   G, Z,   Z, Z, Z, Z,   Z,  ! vlam+lm
     &  Z, Z, Z, Z, G,   Z, U, Z, Z, U, U, U,   U, U,   Z, Z, Z, Z,   Z,  ! vnum
     &  Z, Z, Z, Z, G,   Z, Z, Z, Z, Z, G, Z,   G, Z,   Z, Z, Z, U,   Z,  ! vent
     &  Z, Z, Z, Z, G,   Z, Z, Z, Z, Z, G, Z,   G, Z,   Z, Z, Z, Z,   U   ! coriolis
     &  /),
     &  (/ NRID, SV2D_PST_CMP_NRES /) )

      INTEGER, PARAMETER :: NDIM_LCL =  2
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NPRG_LCL = 40
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NPRE_LCL = 12

      REAL*8,  TARGET :: VCORE_LCL (NDIM_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VDLE_LCL  (NDLN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRG_LCL  (NPRG_LCL)
      REAL*8,  TARGET :: VPRN_LCL  (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRE_LCL  (NPRE_LCL)
      INTEGER, TARGET :: KNE_LCL   (NNEL_LCL)

      REAL*8, POINTER :: VCORE (:,:)
      REAL*8, POINTER :: VDLE  (:,:)
      REAL*8, POINTER :: VPRN  (:,:)
      REAL*8, POINTER :: VPRE  (:,:)
      REAL*8, POINTER :: VPRG  (:)
      REAL*8  DETJT3
      INTEGER,POINTER :: KNE   (:)

      INTEGER HVFT, HPRGL, HPRNE, HPREVE
      INTEGER IERR
      INTEGER IC, IE, IN, II, IR, ID, NO

      INTEGER, PARAMETER :: KNE_P(NNEL_LCL) = ( / 1, 2, 3, 4, 5, 6/ )
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .GE. SV2D_PST_CMP_NPOST)
D     CALL ERR_PRE(NNL  .EQ.  GDTA%NNL)
D     CALL ERR_PRE(NDIM_LCL .GE.  GDTA%NDIM)
D     CALL ERR_PRE(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_PRE(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_PRE(NPRN_LCL .GE. EDTA%NPRNO)
D     CALL ERR_PRE(NPRE_LCL .GE. EDTA%NPREV)
C-----------------------------------------------------------------------

      HVFT = 0    ! vtable par défaut
      IERR = SV2D_CBS_REQFNC(HVFT, EA_FUNC_PRCPRGL,  HPRGL)
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPRNEV, HPRNE)
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPREVE, HPREVE)

C---     Reshape the arrays
      VCORE (1:GDTA%NDIM, 1:GDTA%NNELV) => VCORE_LCL
      VDLE  (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_LCL
      VPRG  (1:EDTA%NPRGL)              => VPRG_LCL
      VPRN  (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_LCL
      VPRE  (1:EDTA%NPREV_D1, 1:EDTA%NPREV_D2) => VPRE_LCL
      KNE   (1:GDTA%NNELV) => KNE_LCL

C---     Table des indices dans VPRG
      KRID =  (/
     &   IPRG%VISCO_CST,      ! les var ne sont pas des
     &   IPRG%VISCO_LM,       ! constantes, et donc KRID
     &   IPRG%VISCO_SMGO,     ! ne peut être initialisé à
     &   IPRG%VISCO_BINF,     ! la déclaration
     &   IPRG%VISCO_BSUP,
     &   IPRG%DECOU_MAN,
     &   IPRG%DECOU_AMORT,
     &   IPRG%DECOU_CON_FACT,
     &   IPRG%DECOU_GRA_FACT,
     &   IPRG%DECOU_DIF_NU,
     &   IPRG%DECOU_DIF_PE,
     &   IPRG%DECOU_DRC_NU,
     &   IPRG%STABI_PECLET,
     &   IPRG%STABI_AMORT,
     &   IPRG%CMULT_CON,
     &   IPRG%CMULT_GRA,
     &   IPRG%CMULT_MAN,
     &   IPRG%CMULT_VENT,
     &   IPRG%LATITUDE
     &   /)

C---     Initialise les tables
      CALL DINIT(NPST*NNL, ZERO, VPOST, 1)
      CALL DINIT(NNL,      ZERO, VTRV,  1)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,GDTA%NELCOL
!!!$omp do
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Transfert des GDTA%VCORG
         VCORE(:,:) = GDTA%VCORG(:, GDTA%KNGV(:,IE))

C---        Transfert des DDL
         VDLE(:,:) = EDTA%VDLG(:, GDTA%KNGV(:,IE))

C---        Calcul les composantes du résidu
         DO IR=1,SV2D_PST_CMP_NRES

C---           Copie et perturbe VPRG
            VPRG(:) = EDTA%VPRGL(:)
            VPRG(KRID(:)) = VPRG(KRID(:)) * VRID(:,IR)

C---           Transfert des PRNO
            VPRN(:,:) = EDTA%VPRNO(:, GDTA%KNGV(:,IE))

C---           Transfert des PREV
            VPRE(:,:) = EDTA%VPREV(:,:,IE)

C---           Pre-calcule les propriétés globales
            IERR = SO_FUNC_CALL2(HPRGL,
     &                           SO_ALLC_CST2B(VPRG(1)),
     &                           SO_ALLC_CST2B(IERR))

C---           Calcule les propriétés nodales perturbées
            IERR = SO_FUNC_CALL4(HPRNE,
     &                           SO_ALLC_CST2B(VPRG(:)),
     &                           SO_ALLC_CST2B(VPRN(:,1)),
     &                           SO_ALLC_CST2B(VDLE(:,1)),
     &                           SO_ALLC_CST2B(IERR))

C---           Calcule les propriétés élémentaires perturbées
            IERR = SO_FUNC_CALL7(HPREVE,
     &                         SO_ALLC_CST2B(VCORE(:,1)),
     &                         SO_ALLC_CST2B(GDTA%VDJV(:,IE)),
     &                         SO_ALLC_CST2B(VPRG(:)),
     &                         SO_ALLC_CST2B(VPRN(:,1)),
     &                         SO_ALLC_CST2B(VPRE(:,1)),
     &                         SO_ALLC_CST2B(VDLE(:,1)),
     &                         SO_ALLC_CST2B(IERR))

C---           Calcul du résidu
            VFE(:,:) = ZERO
            CALL SV2D_CBS_RE_V(VFE,
     &                         GDTA%VDJV(:,IE),
     &                         VPRG,
     &                         VPRN,
     &                         VPRE,
     &                         VDLE,
     &                         VDLE)

C---           Assemblage du post global
            II = 2*(IR-1)
            DO IN=1,GDTA%NNELV
               NO = GDTA%KNGV(IN,IE)
               VPOST(II+1, NO) = VPOST(II+1, NO) + VFE(1,IN)
               VPOST(II+2, NO) = VPOST(II+2, NO) + VFE(2,IN)
            ENDDO
         ENDDO

C---        Accumulation des DETJT3
         DETJT3 = GDTA%VDJV(5,IE)*UN_4
         VTRV(GDTA%KNGV(:,IE)) = VTRV(GDTA%KNGV(:,IE)) + DETJT3

20    CONTINUE
!!!$omp end do
10    CONTINUE

C---     Divise par le surface nodale
      DO IN=1,NNL
         VPOST(:,IN) = VPOST(:,IN) / VTRV(IN)
      ENDDO

      SV2D_PST_CMP_CLC2_O = ERR_TYP()
      RETURN
      END FUNCTION SV2D_PST_CMP_CLC2_O

C************************************************************************
C Sommaire:  SV2D_PST_CMP_CLC2
C
C Description:
C     La fonction privée SV2D_PST_CMP_CLC2 fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER 
     &FUNCTION SV2D_PST_CMP_CLC2_N(HOBJ,
     &                             HELM,
     &                             NPST,
     &                             NNL,
     &                             VPOST,
     &                             VTRV,
     &                             VFE)

      USE SV2D_XBS_M
      USE SV2D_CBS_RE_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HELM
      INTEGER, INTENT(IN) :: NPST
      INTEGER, INTENT(IN) :: NNL
      REAL*8, INTENT(INOUT) :: VPOST (NPST, NNL)
      REAL*8, INTENT(INOUT) :: VTRV  (NNL)
      REAL*8, INTENT(INOUT) :: VFE   (:,:)

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_fnct.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER, PARAMETER :: NRID = 19
      INTEGER :: KRID(NRID)
      REAL*8, PARAMETER :: U = 1.0D0
      REAL*8, PARAMETER :: Z = 0.0D0
      REAL*8, PARAMETER :: G = 1.0D99
      REAL*8 :: VRID(NRID, SV2D_PST_CMP_NRES) = reshape (
C           VISCO     |       DRY/WET        | STAB |     CMULT     |
C      CST,LM,DJ,BI,BM|n,DMP,CNV,GRA,nu,Pe,Dc|Pe,DMP|CNV,GRA,MAN,VNT|CORIOLIS
     & (/
     &  Z, Z, Z, Z, G,   Z, Z, U, Z, Z, G, Z,   G, Z,   U, Z, Z, Z,   Z,  ! convection
     &  Z, Z, Z, Z, G,   Z, Z, Z, U, Z, G, Z,   G, Z,   Z, U, Z, Z,   Z,  ! gravité
     &  Z, Z, Z, Z, G,   U, Z, Z, Z, Z, G, Z,   G, Z,   Z, Z, U, Z,   Z,  ! manning
     &  U, U, U, U, U,   Z, Z, Z, Z, Z, G, Z,   G, Z,   Z, Z, Z, Z,   Z,  ! vlam+lm
     &  Z, Z, Z, Z, G,   Z, U, Z, Z, U, U, U,   U, U,   Z, Z, Z, Z,   Z,  ! vnum
     &  Z, Z, Z, Z, G,   Z, Z, Z, Z, Z, G, Z,   G, Z,   Z, Z, Z, U,   Z,  ! vent
     &  Z, Z, Z, Z, G,   Z, Z, Z, Z, Z, G, Z,   G, Z,   Z, Z, Z, Z,   U   ! coriolis
     &  /),
     &  (/ NRID, SV2D_PST_CMP_NRES /) )

      INTEGER, PARAMETER :: NDIM_LCL =  2
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDJE_LCL =  5
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NPRG_LCL = 40
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NPRE_LCL = 12

      REAL*8,  TARGET :: VCORE_LCL (NDIM_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VDJE_LCL  (NDJE_LCL)
      REAL*8,  TARGET :: VDLE_LCL  (NDLN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRG_LCL  (NPRG_LCL)
      REAL*8,  TARGET :: VPRN_LCL  (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRE_LCL  (NPRE_LCL)
      INTEGER, TARGET :: KNE_LCL   (NNEL_LCL)

      REAL*8, POINTER :: VCORE (:,:)
      REAL*8, POINTER :: VDJE  (:)
      REAL*8, POINTER :: VDLE  (:,:)
      REAL*8, POINTER :: VPRN  (:,:)
      REAL*8, POINTER :: VPRE  (:,:)
      REAL*8, POINTER :: VPRG  (:)
      REAL*8  DETJT3
      INTEGER,POINTER :: KNE   (:)

      INTEGER HVFT, HPRGL, HPRNE, HPREVE
      INTEGER IERR
      INTEGER IC, IE, IN, II, IR, ID, NO
      TYPE (SV2D_XBS_SELF_T),POINTER :: SELF
      TYPE (LM_GDTA_DATA_T), POINTER :: GDTA
      TYPE (LM_EDTA_DATA_T), POINTER :: EDTA
      TYPE (SV2D_IPRG_T),    POINTER :: IPRG
      TYPE (SV2D_IPRN_T),    POINTER :: IPRN

      INTEGER, PARAMETER :: KNE_P(NNEL_LCL) = ( / 1, 2, 3, 4, 5, 6/ )
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .GE. SV2D_PST_CMP_NPOST)
D     CALL ERR_PRE(NNL  .EQ.  GDTA%NNL)
D     CALL ERR_PRE(NDIM_LCL .GE.  GDTA%NDIM)
D     CALL ERR_PRE(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_PRE(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_PRE(NPRN_LCL .GE. EDTA%NPRNO)
D     CALL ERR_PRE(NPRE_LCL .GE. EDTA%NPREV)
C-----------------------------------------------------------------------

C---     Récupère les données
      SELF => SV2D_XBS_REQSELF(HELM)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IERR = OB_VTBL_REQMTH(SELF%HVTBL, EA_FUNC_PRCPRGL,  HPRGL)
      IERR = OB_VTBL_REQMTH(SELF%HVTBL, SV2D_VT_CLCPRNEV, HPRNE)
      IERR = OB_VTBL_REQMTH(SELF%HVTBL, SV2D_VT_CLCPREVE, HPREVE)

C---     Reshape the arrays
      VCORE (1:GDTA%NDIM, 1:GDTA%NNELV) => VCORE_LCL
      VDJE  (1:GDTA%NDJV) => VDJE_LCL
      VDLE  (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_LCL
      VPRG  (1:EDTA%NPRGL)              => VPRG_LCL
      VPRN  (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_LCL
      VPRE  (1:EDTA%NPREV_D1, 1:EDTA%NPREV_D2) => VPRE_LCL
      KNE   (1:GDTA%NNELV) => KNE_LCL

C---     Table des indices dans VPRG
      KRID =  (/
     &   IPRG%VISCO_CST,      ! les var ne sont pas des
     &   IPRG%VISCO_LM,       ! constantes, et donc KRID
     &   IPRG%VISCO_SMGO,     ! ne peut être initialisé à
     &   IPRG%VISCO_BINF,     ! la déclaration
     &   IPRG%VISCO_BSUP,
     &   IPRG%DECOU_MAN,
     &   IPRG%DECOU_AMORT,
     &   IPRG%DECOU_CON_FACT,
     &   IPRG%DECOU_GRA_FACT,
     &   IPRG%DECOU_DIF_NU,
     &   IPRG%DECOU_DIF_PE,
     &   IPRG%DECOU_DRC_NU,
     &   IPRG%STABI_PECLET,
     &   IPRG%STABI_AMORT,
     &   IPRG%CMULT_CON,
     &   IPRG%CMULT_GRA,
     &   IPRG%CMULT_MAN,
     &   IPRG%CMULT_VENT,
     &   IPRG%LATITUDE
     &   /)

C---     Initialise les tables
      VPOST(:,:) = ZERO
      VTRV (:)   = ZERO

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,GDTA%NELCOL
!!!$omp do
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Transfert des données élémentaires
         KNE(:) = GDTA%KNGV(:,IE)
         VCORE(:,:) = GDTA%VCORG(:, KNE(:))
         VDLE (:,:) = EDTA%VDLG (:, KNE(:))
         VDJE (:)   = GDTA%VDJV (:, IE)

C---        Calcul les composantes du résidu
         DO IR=1,SV2D_PST_CMP_NRES

C---           Copie et perturbe VPRG
            VPRG(:) = EDTA%VPRGL(:)
            VPRG(KRID(:)) = VPRG(KRID(:)) * VRID(:,IR)

C---           Transfert des PRNO
            VPRN(:,:) = EDTA%VPRNO(:, GDTA%KNGV(:,IE))

C---           Transfert des PREV
            VPRE(:,:) = EDTA%VPREV(:,:,IE)

C---           Pre-calcule les propriétés globales
            IERR = SO_FUNC_CALL2(HPRGL,
     &                           SO_ALLC_CST2B(VPRG(:)),
     &                           SO_ALLC_CST2B(IERR))

C---           Calcule les propriétés nodales perturbées
            IERR = SO_FUNC_CALL2(HPRNE,
     &                           SO_ALLC_CST2B(VDLE(:,1)),
     &                           SO_ALLC_CST2B(VPRN(:,1)))

C---           Calcule les propriétés élémentaires perturbées
            IERR = SO_FUNC_CALL4(HPREVE,
     &                           SO_ALLC_CST2B(VDJE(:)),
     &                           SO_ALLC_CST2B(VPRN(:,1)),
     &                           SO_ALLC_CST2B(VDLE(:,1)),
     &                           SO_ALLC_CST2B(VPRE(:,1)))
            
C---           Calcul du résidu
            VFE(:,:) = ZERO
            CALL SV2D_CBS_RE_V(VFE,
     &                         VDJE,
     &                         VPRG,
     &                         VPRN,
     &                         VPRE,
     &                         VDLE,
     &                         VDLE)

C---           Assemblage du post global
            II = 2*(IR-1)
            DO IN=1,GDTA%NNELV
               NO = GDTA%KNGV(IN,IE)
               VPOST(II+1, NO) = VPOST(II+1, NO) + VFE(1,IN)
               VPOST(II+2, NO) = VPOST(II+2, NO) + VFE(2,IN)
            ENDDO
         ENDDO

C---        Accumulation des DETJT3
         DETJT3 = GDTA%VDJV(5,IE)*UN_4
         VTRV(GDTA%KNGV(:,IE)) = VTRV(GDTA%KNGV(:,IE)) + DETJT3

20    CONTINUE
!!!$omp end do
10    CONTINUE

C---     Divise par le surface nodale
      DO IN=1,NNL
         VPOST(:,IN) = VPOST(:,IN) / VTRV(IN)
      ENDDO

      SV2D_PST_CMP_CLC2_N = ERR_TYP()
      RETURN
      END FUNCTION SV2D_PST_CMP_CLC2_N

      END MODULE SV2D_PST_CMP_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CMP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SV2D_PST_CMP_NOBJMAX,
     &                   SV2D_PST_CMP_HBASE,
     &                   'SV2D - Post-traitement Composantes')

      SV2D_PST_CMP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CMP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER  IERR
      EXTERNAL SV2D_PST_CMP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SV2D_PST_CMP_NOBJMAX,
     &                   SV2D_PST_CMP_HBASE,
     &                   SV2D_PST_CMP_DTR)

      SV2D_PST_CMP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur par défaut de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CMP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SV2D_PST_CMP_NOBJMAX,
     &                   SV2D_PST_CMP_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SV2D_PST_CMP_HVALIDE(HOBJ))
         IOB = HOBJ - SV2D_PST_CMP_HBASE

         SV2D_PST_CMP_HPRNT(IOB) = 0
      ENDIF

      SV2D_PST_CMP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CMP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SV2D_PST_CMP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SV2D_PST_CMP_NOBJMAX,
     &                   SV2D_PST_CMP_HBASE)

      SV2D_PST_CMP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CMP_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL SV2D_PST_CMP_CLC, SV2D_PST_CMP_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SV2D_PST_CMP_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,HOBJ,SV2D_PST_CMP_CLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFLOG,HOBJ,SV2D_PST_CMP_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR=PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_SIMU_INI(HPRNT,
     &                                HFCLC, HFLOG,
     &                                NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - SV2D_PST_CMP_HBASE
         SV2D_PST_CMP_HPRNT(IOB) = HPRNT
      ENDIF

      SV2D_PST_CMP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CMP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - SV2D_PST_CMP_HBASE
      HPRNT = SV2D_PST_CMP_HPRNT(IOB)

C---     Détruis les attributs
      IF (PS_SIMU_HVALIDE(HPRNT)) IERR = PS_SIMU_DTR(HPRNT)

C---     Reset
      SV2D_PST_CMP_HPRNT(IOB) = 0

      SV2D_PST_CMP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SV2D_PST_CMP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CMP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'sv2d_pst_cmp.fc'
C------------------------------------------------------------------------

      SV2D_PST_CMP_REQHBASE = SV2D_PST_CMP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SV2D_PST_CMP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CMP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sv2d_pst_cmp.fc'
C------------------------------------------------------------------------

      SV2D_PST_CMP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       SV2D_PST_CMP_NOBJMAX,
     &                                       SV2D_PST_CMP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CMP_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CMP_HPRNT(HOBJ - SV2D_PST_CMP_HBASE)
      SV2D_PST_CMP_ACC = PS_SIMU_ACC(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CMP_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CMP_HPRNT(HOBJ - SV2D_PST_CMP_HBASE)
      SV2D_PST_CMP_FIN = PS_SIMU_FIN(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CMP_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_CMP_HPRNT(HOBJ - SV2D_PST_CMP_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, SV2D_PST_CMP_NPOST)

      SV2D_PST_CMP_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le contrôle de cohérence est faible. En fait, il faudrait contrôler
C     si HSIM et la formulation sont bien les bonnes.
C************************************************************************
      FUNCTION SV2D_PST_CMP_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_CMP_HPRNT(HOBJ - SV2D_PST_CMP_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, SV2D_PST_CMP_NPOST)

      SV2D_PST_CMP_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_CMP_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CMP_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CMP_HPRNT(HOBJ-SV2D_PST_CMP_HBASE)
      SV2D_PST_CMP_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_CMP_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CMP_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CMP_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CMP_HPRNT(HOBJ-SV2D_PST_CMP_HBASE)
      SV2D_PST_CMP_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée SV2D_PST_CMP_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'appel à SV2D_LGCY_HVALIDE n'est pas virtuel est va donc détecter
C     si on a un vrai SV2D_LGCY. Il doit donc être fait en premier.
C************************************************************************
      FUNCTION SV2D_PST_CMP_CLC(HOBJ,
     &                          HELM,
     &                          NPST,
     &                          NNL,
     &                          VPOST)

      USE SV2D_XBS_M
      USE SV2D_LGCY_M
      USE SV2D_PST_CMP_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELM
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_xbs.fi'
      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER, PARAMETER :: NDLN_LCL = 3
      INTEGER, PARAMETER :: NNEL_LCL = 6
      INTEGER, PARAMETER :: NDLE_LCL = NDLN_LCL*NNEL_LCL

      REAL*8, TARGET :: VFE_LCL(NDLE_LCL)
      INTEGER L_TRV
      INTEGER IERR
      REAL*8, POINTER :: VFE(:,:)
      TYPE (SV2D_XBS_SELF_T),POINTER :: SELF
      TYPE (LM_GDTA_DATA_T), POINTER :: GDTA
      TYPE (LM_EDTA_DATA_T), POINTER :: EDTA
      TYPE (SV2D_IPRN_T),    POINTER :: IPRN
      TYPE (SV2D_IPRG_T),    POINTER :: IPRG
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Alloue la table temporaire
      L_TRV = 0
      IF (ERR_GOOD())
     &   IERR = SO_ALLC_ALLRE8(NNL, L_TRV)

      IF (SV2D_LGCY_HVALIDE(HELM)) THEN
C---        Récupère les données
         GDTA => SV2D_LGCY_REQGDTA(HELM)
         EDTA => SV2D_LGCY_REQEDTA(HELM)
         IPRN => SV2D_LGCY_REQIPRN(HELM)
         IPRG => SV2D_LGCY_REQIPRG(HELM)
C---        Reshape the arrays
         VFE(1:EDTA%NDLN, 1:GDTA%NNELV) => VFE_LCL
C---        Fait le calcul
         IERR = SV2D_PST_CMP_CLC2_O(HOBJ,
     &                            GDTA,
     &                            EDTA,
     &                            IPRG,
     &                            IPRN,
     &                            NPST,
     &                            NNL,
     &                            VPOST,
     &                            VA(SO_ALLC_REQVIND(VA, L_TRV)),
     &                            VFE)

      ELSEIF (SV2D_XBS_HVALIDE(HELM)) THEN
C---        Récupère les données
         SELF => SV2D_XBS_REQSELF(HELM)
C---        Reshape the arrays
         VFE(1:SELF%EDTA%NDLN, 1:SELF%GDTA%NNELV) => VFE_LCL
C---        Fait le calcul
         IERR = SV2D_PST_CMP_CLC2_N(HOBJ,
     &                            HELM,
     &                            NPST, NNL, VPOST,
     &                            VA(SO_ALLC_REQVIND(VA, L_TRV)),
     &                            VFE)

      ELSE
         CALL ERR_ASG(ERR_FTL, 'Unsupported type')
      ENDIF

C---     Désalloue la table temporaire
      IF (SO_ALLC_HEXIST(L_TRV))
     &   IERR = SO_ALLC_ALLRE8(0, L_TRV)

      SV2D_PST_CMP_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_PST_CMP_LOG
C
C Description:
C     La fonction privée SV2D_PST_CMP_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CMP_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'sv2d_pst_cmp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'sv2d_pst_cmp.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CMP_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .EQ. SV2D_PST_CMP_NPOST)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SV2D_POST_COMPOSANTES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<25>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)')    'MSG_VALEUR#<25>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR=PS_PSTD_LOGVAL(HNUMR, 1, NPST, NNL, VPOST, 'Convection: x')
      IERR=PS_PSTD_LOGVAL(HNUMR, 2, NPST, NNL, VPOST, '          : y')
      IERR=PS_PSTD_LOGVAL(HNUMR, 3, NPST, NNL, VPOST, 'Gravity   : x')
      IERR=PS_PSTD_LOGVAL(HNUMR, 4, NPST, NNL, VPOST, '          : y')
      IERR=PS_PSTD_LOGVAL(HNUMR, 5, NPST, NNL, VPOST, 'Manning   : x')
      IERR=PS_PSTD_LOGVAL(HNUMR, 6, NPST, NNL, VPOST, '          : y')
      IERR=PS_PSTD_LOGVAL(HNUMR, 7, NPST, NNL, VPOST, 'nu phys.  : x')
      IERR=PS_PSTD_LOGVAL(HNUMR, 8, NPST, NNL, VPOST, '          : y')
      IERR=PS_PSTD_LOGVAL(HNUMR, 9, NPST, NNL, VPOST, 'nu num.   : x')
      IERR=PS_PSTD_LOGVAL(HNUMR,10, NPST, NNL, VPOST, '          : y')
      IERR=PS_PSTD_LOGVAL(HNUMR,11, NPST, NNL, VPOST, 'Wind      : x')
      IERR=PS_PSTD_LOGVAL(HNUMR,12, NPST, NNL, VPOST, '          : y')
      IERR=PS_PSTD_LOGVAL(HNUMR,13, NPST, NNL, VPOST, 'Coriolis  : x')
      IERR=PS_PSTD_LOGVAL(HNUMR,14, NPST, NNL, VPOST, '          : y')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      SV2D_PST_CMP_LOG = ERR_TYP()
      RETURN
      END
