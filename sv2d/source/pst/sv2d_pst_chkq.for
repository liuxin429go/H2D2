C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER SV2D_PST_CHKQ_000
C     INTEGER SV2D_PST_CHKQ_999
C     INTEGER SV2D_PST_CHKQ_CTR
C     INTEGER SV2D_PST_CHKQ_DTR
C     INTEGER SV2D_PST_CHKQ_INI
C     INTEGER SV2D_PST_CHKQ_RST
C     INTEGER SV2D_PST_CHKQ_REQHBASE
C     LOGICAL SV2D_PST_CHKQ_HVALIDE
C     INTEGER SV2D_PST_CHKQ_XEQ
C     INTEGER SV2D_PST_CHKQ_REQHVNO
C     CHARACTER*256 SV2D_PST_CHKQ_REQNOMF
C   Private:
C     INTEGER SV2D_PST_CHKQ_CLC
C     INTEGER SV2D_PST_CHKQ_CLC2
C     REAL*8 SV2D_PST_CHKQ_LIM
C     REAL*8 SV2D_PST_CHKQ_Q
C
C************************************************************************

      MODULE SV2D_PST_CHKQ_M

      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:  SV2D_PST_CHKQ_LIM
C
C Description:
C     La fonction privée SV2D_PST_CHKQ_LIM calcule le débit normal sur
C     une limite.
C
C Entrée:
C     IE       Élément de surface
C     KNGS     Table des connectivités de surface
C     VDJS
C     VPRNO
C     VDLG
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8
     &FUNCTION SV2D_PST_CHKQ_LIM (IL,
     &                            KCLLIM,
     &                            KCLELE,
     &                            KNGS,
     &                            VDJS,
     &                            VPRNO,
     &                            KDIMP,
     &                            VDLG)

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      INTEGER, INTENT(IN) :: KCLLIM(:,:)
      INTEGER, INTENT(IN) :: KCLELE(:)
      INTEGER, INTENT(IN) :: KNGS  (:,:)
      REAL*8,  INTENT(IN) :: VDJS  (:,:)
      REAL*8,  INTENT(IN) :: VPRNO (:,:)
      INTEGER, INTENT(IN) :: KDIMP (:,:)
      REAL*8,  INTENT(IN) :: VDLG  (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER I, IE
      INTEGER IEDEB, IEFIN
      REAL*8  QS
C------------------------------------------------------------------------

      IEDEB = KCLLIM(5, IL)
      IEFIN = KCLLIM(6, IL)

      QS = ZERO
      DO I = IEDEB, IEFIN
         IE = KCLELE(I)
         QS = QS + SV2D_PST_CHKQ_Q(IE,
     &                             KNGS,
     &                             VDJS,
     &                             VPRNO,
     &                             KDIMP,
     &                             VDLG)
      ENDDO

      SV2D_PST_CHKQ_LIM = QS
      RETURN
      END FUNCTION SV2D_PST_CHKQ_LIM

C************************************************************************
C Sommaire:  SV2D_PST_CHKQ_Q
C
C Description:
C     La fonction privée SV2D_PST_CHKQ_Q calcule le débit normal à un élément
C     de surface.
C
C Entrée:
C     IE       Élément de surface
C     KNGS     Table des connectivités de surface
C     VDJS
C     VPRGL    Table de propriétés globales
C     VPRNO    Table de propriétés nodales
C     KDIMP    Code des conditions aux limites
C     VDLG     Table des degrés de liberté
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8
     &FUNCTION SV2D_PST_CHKQ_Q(IE,
     &                         KNGS,
     &                         VDJS,
     &                         VPRNO,
     &                         KDIMP,
     &                         VDLG)

      IMPLICIT NONE

      INTEGER IE
      INTEGER KNGS  (:,:)
      REAL*8  VDJS  (:,:)
      REAL*8  VPRNO (:,:)
      INTEGER KDIMP (:,:)
      REAL*8  VDLG  (:,:)

      INCLUDE 'eacdcl.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER NO1, NO2, NO3
      REAL*8  VNX, VNY, DJL3, DJL2
      REAL*8  QX1, QX2, QX3
      REAL*8  QY1, QY2, QY3
      REAL*8  QXS, QYS, QS
C------------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------

      QS = 0.0D0

C---     Connectivités du L3
      NO1 = KNGS(1,IE)
      NO2 = KNGS(2,IE)
      NO3 = KNGS(3,IE)
      IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 9999
      IF (EST_PARALLEL(KDIMP(3,NO2))) GOTO 9999
      IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 9999

C---     Métriques
      VNX =  VDJS(2,IE)    ! VNX =  VTY
      VNY = -VDJS(1,IE)    ! VNY = -VTX
      DJL3=  VDJS(3,IE)
      DJL2=  UN_2*DJL3

C---     Valeurs nodales
      QX1 = VDLG (1,NO1)   ! Qx
      QY1 = VDLG (2,NO1)   ! Qy
      QX2 = VDLG (1,NO2)
      QY2 = VDLG (2,NO2)
      QX3 = VDLG (1,NO3)
      QY3 = VDLG (2,NO3)

C---     Débits normaux
      QXS = ((QX2+QX1) + (QX3+QX2))*VNX
      QYS = ((QY2+QY1) + (QY3+QY2))*VNY

C---     Intègre
      QS = (QXS + QYS)*DJL2

9999  CONTINUE
      SV2D_PST_CHKQ_Q = QS
      RETURN
      END FUNCTION SV2D_PST_CHKQ_Q

      END MODULE SV2D_PST_CHKQ_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SV2D_PST_CHKQ_NOBJMAX,
     &                   SV2D_PST_CHKQ_HBASE,
     &                   'SV2D - Post-traitement Discharge check')

      SV2D_PST_CHKQ_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER  IERR
      EXTERNAL SV2D_PST_CHKQ_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SV2D_PST_CHKQ_NOBJMAX,
     &                   SV2D_PST_CHKQ_HBASE,
     &                   SV2D_PST_CHKQ_DTR)

      SV2D_PST_CHKQ_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SV2D_PST_CHKQ_NOBJMAX,
     &                   SV2D_PST_CHKQ_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SV2D_PST_CHKQ_HVALIDE(HOBJ))
         IOB = HOBJ - SV2D_PST_CHKQ_HBASE
      ENDIF

      SV2D_PST_CHKQ_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CHKQ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SV2D_PST_CHKQ_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SV2D_PST_CHKQ_NOBJMAX,
     &                   SV2D_PST_CHKQ_HBASE)

      SV2D_PST_CHKQ_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CHKQ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('SV2D_PST_CHKQ: post-traitement avec accumulation '
     &           // 'non implante avec handle sur le trigger')

C---     RESET LES DONNEES
      IERR = SV2D_PST_CHKQ_RST(HOBJ)

C---     ASSIGNE LES VALEURS
!      IF (ERR_GOOD()) THEN
!         IOB  = HOBJ - SV2D_PST_CHKQ_HBASE
!      ENDIF

      SV2D_PST_CHKQ_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CHKQ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SV2D_PST_CHKQ_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SV2D_PST_CHKQ_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'sv2d_pst_chkq.fc'
C------------------------------------------------------------------------

      SV2D_PST_CHKQ_REQHBASE = SV2D_PST_CHKQ_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SV2D_PST_CHKQ_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sv2d_pst_chkq.fc'
C------------------------------------------------------------------------

      SV2D_PST_CHKQ_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                     SV2D_PST_CHKQ_NOBJMAX,
     &                                     SV2D_PST_CHKQ_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Soit la simulation est déjà chargé, alors pas besoin de la recharger.
C     Soit le temps n'est pas valide
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_XEQ
CDEC$ ENDIF

      USE LM_HELE_CALLCB_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER IERR
      INTEGER HFCLC
      REAL*8  TSIM
      EXTERNAL SV2D_PST_CHKQ_CLC
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CHKQ_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère le temps
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_HELE_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLC   (HSIM)

C---     Construit et initialise le call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,
     &                                      HOBJ, SV2D_PST_CHKQ_CLC)

C---     Fait le calcul
      IF (ERR_GOOD()) IERR = LM_HELE_CALLCB(HSIM, HFCLC)

C---     Détruit le call-back
      IF (SO_FUNC_HVALIDE(HFCLC)) IERR = SO_FUNC_DTR(HFCLC)

      SV2D_PST_CHKQ_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_CHKQ_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_chkq.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CHKQ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_REQ_INVALIDE')
      SV2D_PST_CHKQ_REQHVNO = 0
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_CHKQ_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CHKQ_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_chkq.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CHKQ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_REQ_INVALIDE')
      SV2D_PST_CHKQ_REQNOMF = 'invalid file name'
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée SV2D_PST_CFL_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'appel à SV2D_LGCY_HVALIDE n'est pas virtuel est va donc détecter
C     si on a un vrai SV2D_LGCY. Il doit donc être fait en premier.
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_CLC(HOBJ,
     &                           HELM)

      USE SV2D_LGCY_M
      USE SV2D_XBS_M, ONLY: SV2D_XBS_T
      USE LM_ELEM_M,  ONLY: LM_ELEM_T
      USE LM_HELE_M,  ONLY: LM_HELE_REQOMNG
      USE LM_GDTA_M,  ONLY: LM_GDTA_T
      USE LM_EDTA_M,  ONLY: LM_EDTA_T
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELM

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER IERR
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN
      TYPE (SV2D_XPRG_T),POINTER :: XPRG
      CLASS(LM_ELEM_T),  POINTER :: OELE
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CHKQ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les données
      IF (SV2D_LGCY_HVALIDE(HELM)) THEN   ! cf note
         GDTA => SV2D_LGCY_REQGDTA(HELM)
         EDTA => SV2D_LGCY_REQEDTA(HELM)
         IPRN => SV2D_LGCY_REQIPRN(HELM)
         XPRG => SV2D_LGCY_REQXPRG(HELM)
      ELSEIF (LM_HELE_HVALIDE(HELM)) THEN
         OELE => LM_HELE_REQOMNG(HELM)
         SELECT TYPE(OELE)
            CLASS IS (SV2D_XBS_T)
               GDTA => OELE%GDTA
               EDTA => OELE%EDTA
               IPRN => OELE%IPRN
               XPRG => OELE%XPRG
            CLASS DEFAULT
               CALL ERR_ASG(ERR_FTL, 'Unsupported type')
         END SELECT
      ELSE
         CALL ERR_ASG(ERR_FTL, 'Unsupported type')
      ENDIF

C---     Fait le calcul
      IF (ERR_GOOD())
     &   IERR = SV2D_PST_CHKQ_CLC2(HOBJ,
     &                             GDTA,
     &                             EDTA,
     &                             XPRG,
     &                             IPRN)

      SV2D_PST_CHKQ_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_CHKQ_CLC
C
C Description:
C     La fonction privée SV2D_PST_CHKQ_CLC est le call-back qui fait le
C     calcul débit.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CHKQ_CLC2(HOBJ,
     &                            GDTA,
     &                            EDTA,
     &                            XPRG,
     &                            IPRN)

      USE SV2D_XBS_M
      USE SV2D_PST_CHKQ_M
      USE LM_GDTA_M
      USE LM_EDTA_M
      IMPLICIT NONE

      INTEGER HOBJ
      TYPE (LM_GDTA_T),   INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T),   INTENT(IN) :: EDTA
      TYPE (SV2D_XPRG_T), INTENT(IN) :: XPRG
      TYPE (SV2D_IPRN_T), INTENT(IN) :: IPRN

      INCLUDE 'sv2d_pst_chkq.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'sv2d_pst_chkq.fc'

      INTEGER I_RANK, I_COMM, I_ERROR
      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER   NLIM, NLIM_MAX
      PARAMETER (NLIM_MAX = 512)

      INTEGER  IERR
      INTEGER  IC, IL, NN, NE
      REAL*8   QS, QG, QNEG, QPOS
      REAL*8   QBUF(2, NLIM_MAX), QDUM
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CHKQ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK
      IF (GDTA%NCLLIM .GT. NLIM_MAX) GOTO 9901

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Calcule les débits
      CALL DINIT(2*NLIM_MAX, 0.0D0, QBUF, 1)
      NLIM = 0
      DO IL=1, GDTA%NCLLIM
         IC = GDTA%KCLLIM(2, IL)
         IF (IC .LE. 0) GOTO 199
         IF (EDTA%KCLCND(2,IC) .LE. 0) GOTO 199

         QS = SV2D_PST_CHKQ_LIM(IL,
     &                          GDTA%KCLLIM,
     &                          GDTA%KCLELE,
     &                          GDTA%KNGS,
     &                          GDTA%VDJS,
     &                          EDTA%VPRNO,
     &                          EDTA%KDIMP,
     &                          EDTA%VDLG)
         QBUF(1,IL) = QS
         QBUF(2,IL) = GDTA%KCLLIM(6,IL) - GDTA%KCLLIM(5,IL) + 1 !Nbr Ele

199      CONTINUE
      ENDDO

C---     Réduis
      IF (I_RANK .EQ. I_MASTER) THEN
         CALL MPI_REDUCE(MPI_IN_PLACE, QBUF, 2*GDTA%NCLLIM,
     &                   MP_TYPE_RE8(),
     &                   MPI_SUM, I_MASTER, I_COMM, I_ERROR)
      ELSE
         CALL MPI_REDUCE(QBUF, QDUM, 2*GDTA%NCLLIM,
     &                   MP_TYPE_RE8(),
     &                   MPI_SUM, I_MASTER, I_COMM, I_ERROR)
      ENDIF

      IF (ERR_BAD()) GOTO 9999
      IF (I_RANK .NE. I_MASTER) GOTO 9999

C---     Cumule les débits
      QNEG = 0.0D0
      QPOS = 0.0D0
      DO IL=1, GDTA%NCLLIM
         QS = QBUF(1,IL)
         IF (QS .LE. 0.0D0) THEN
            QNEG = QNEG + QS
         ELSE
            QPOS = QPOS + QS
         ENDIF
      ENDDO

C---     L'entête
      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SV2D_POST_CHECK_Q (+ MSG_Q_SORTANT):'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Affiche chaque limite
      DO IL=1, GDTA%NCLLIM
         QS = QBUF(1,IL)
         NE = NINT(QBUF(2,IL))
         NN = GDTA%KCLLIM(4,IL) - GDTA%KCLLIM(3,IL) + 1
         IF (NN .LE. 0) GOTO 299

         WRITE(LOG_BUF, '(A,I3)') 'MSG_LIMITE: ', IL
         CALL LOG_ECRIS(LOG_BUF)

         CALL LOG_INCIND()
         WRITE(LOG_BUF, '(A,I6)') 'MSG_NBR_NOEUDS#<15>#= ', NN
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF, '(A,I6)') 'MSG_NBR_ELEMENTS#<15>#= ', NE
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF, '(A,1PE14.6E3)') 'Q#<15>#= ', QS
         CALL LOG_ECRIS(LOG_BUF)
         IF (QPOS .GT. 1.0D-8) THEN
            WRITE(LOG_BUF, '(A,1PE14.6E3,A,F7.2,A)')
     &               'Q/Q+#<15>#= ', QS/QPOS, ' (', 10.0D0*QS/QPOS, '%)'
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
         CALL LOG_DECIND()

299      CONTINUE
      ENDDO

      WRITE(LOG_BUF, '(A)') 'MSG_SOMMAIRE: '
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'Q+#<15>#= ', QPOS
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'Q-#<15>#= ', QNEG
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'dQ#<15>#= ', QPOS+QNEG
      CALL LOG_ECRIS(LOG_BUF)
      IF (QPOS .GT. 1.0D-8) THEN
         WRITE(LOG_BUF, '(A,1PE14.6E3,A,F7.2,A)')
     &               'dQ/Q+#<15>#= ', (QPOS+QNEG)/QPOS,
     &               ' (', 10.0D0*(QPOS+QNEG)/QPOS, '%)'
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF
      CALL LOG_DECIND()

      CALL LOG_DECIND()

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_PST_CHKQ_CLC2 = ERR_TYP()
      RETURN
      END

