C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SV2D_PST_SIM_000
C     INTEGER SV2D_PST_SIM_999
C     INTEGER SV2D_PST_SIM_CTR
C     INTEGER SV2D_PST_SIM_DTR
C     INTEGER SV2D_PST_SIM_INI
C     INTEGER SV2D_PST_SIM_RST
C     INTEGER SV2D_PST_SIM_REQHBASE
C     LOGICAL SV2D_PST_SIM_HVALIDE
C     INTEGER SV2D_PST_SIM_ACC
C     INTEGER SV2D_PST_SIM_FIN
C     INTEGER SV2D_PST_SIM_XEQ
C     INTEGER SV2D_PST_SIM_ASGHSIM
C     INTEGER SV2D_PST_SIM_REQHVNO
C     CHARACTER*256 SV2D_PST_SIM_REQNOMF
C   Private:
C     INTEGER SV2D_PST_SIM_CLC
C     INTEGER SV2D_PST_SIM_CLC2
C     INTEGER SV2D_PST_SIM_LOG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_SIM_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SV2D_PST_SIM_NOBJMAX,
     &                   SV2D_PST_SIM_HBASE,
     &                   'SV2D - Post-traitement Simulation')

      SV2D_PST_SIM_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_SIM_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER  IERR
      EXTERNAL SV2D_PST_SIM_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SV2D_PST_SIM_NOBJMAX,
     &                   SV2D_PST_SIM_HBASE,
     &                   SV2D_PST_SIM_DTR)

      SV2D_PST_SIM_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur par défaut de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_SIM_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SV2D_PST_SIM_NOBJMAX,
     &                   SV2D_PST_SIM_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SV2D_PST_SIM_HVALIDE(HOBJ))
         IOB = HOBJ - SV2D_PST_SIM_HBASE

         SV2D_PST_SIM_HPRNT(IOB) = 0
      ENDIF

      SV2D_PST_SIM_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_SIM_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SV2D_PST_SIM_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SV2D_PST_SIM_NOBJMAX,
     &                   SV2D_PST_SIM_HBASE)

      SV2D_PST_SIM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_SIM_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL SV2D_PST_SIM_CLC, SV2D_PST_SIM_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SV2D_PST_SIM_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,HOBJ,SV2D_PST_SIM_CLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFLOG,HOBJ,SV2D_PST_SIM_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR=PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_SIMU_INI(HPRNT,
     &                                HFCLC, HFLOG,
     &                                NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - SV2D_PST_SIM_HBASE
         SV2D_PST_SIM_HPRNT(IOB) = HPRNT
      ENDIF

      SV2D_PST_SIM_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_SIM_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - SV2D_PST_SIM_HBASE
      HPRNT = SV2D_PST_SIM_HPRNT(IOB)

C---     Détruis les attributs
      IF (PS_SIMU_HVALIDE(HPRNT)) IERR = PS_SIMU_DTR(HPRNT)

C---     Reset
      SV2D_PST_SIM_HPRNT(IOB) = 0

      SV2D_PST_SIM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SV2D_PST_SIM_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_SIM_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'sv2d_pst_sim.fc'
C------------------------------------------------------------------------

      SV2D_PST_SIM_REQHBASE = SV2D_PST_SIM_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SV2D_PST_SIM_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_SIM_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sv2d_pst_sim.fc'
C------------------------------------------------------------------------

      SV2D_PST_SIM_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       SV2D_PST_SIM_NOBJMAX,
     &                                       SV2D_PST_SIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_SIM_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_SIM_HPRNT(HOBJ - SV2D_PST_SIM_HBASE)
      SV2D_PST_SIM_ACC = PS_SIMU_ACC(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_SIM_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_SIM_HPRNT(HOBJ - SV2D_PST_SIM_HBASE)
      SV2D_PST_SIM_FIN = PS_SIMU_FIN(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_SIM_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_SIM_HPRNT(HOBJ - SV2D_PST_SIM_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, SV2D_PST_SIM_NPOST)

      SV2D_PST_SIM_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le contrôle de cohérence est faible. En fait, il faudrait contrôler
C     si HSIM et la formulation sont bien les bonnes.
C************************************************************************
      FUNCTION SV2D_PST_SIM_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_SIM_HPRNT(HOBJ - SV2D_PST_SIM_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, SV2D_PST_SIM_NPOST)

      SV2D_PST_SIM_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_SIM_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_SIM_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_SIM_HPRNT(HOBJ-SV2D_PST_SIM_HBASE)
      SV2D_PST_SIM_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_SIM_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_SIM_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_SIM_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_SIM_HPRNT(HOBJ-SV2D_PST_SIM_HBASE)
      SV2D_PST_SIM_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée SV2D_PST_SIM_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'appel à SV2D_LGCY_HVALIDE n'est pas virtuel est va donc détecter
C     si on a un vrai SV2D_LGCY. Il doit donc être fait en premier.
C************************************************************************
      FUNCTION SV2D_PST_SIM_CLC(HOBJ,
     &                          HELM,
     &                          NPST,
     &                          NNL,
     &                          VPOST)

      USE SV2D_LGCY_M
      USE SV2D_XBS_M, ONLY: SV2D_XBS_T
      USE LM_ELEM_M,  ONLY: LM_ELEM_T
      USE LM_HELE_M,  ONLY: LM_HELE_REQOMNG
      USE LM_GDTA_M,  ONLY: LM_GDTA_T
      USE LM_EDTA_M,  ONLY: LM_EDTA_T
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELM
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IERR
      INTEGER IC, IE, IN
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  DETJT3
      REAL*8  VIS1, VIS2, VIS3, VIS4
      REAL*8  VNM1, VNM2, VNM3, VNM4
      REAL*8  QX, QY, H
      REAL*8  ZF, UX, UY, P, PA, V, Q, VP, VN
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN
      TYPE (SV2D_XPRG_T),POINTER :: XPRG
      CLASS(LM_ELEM_T),  POINTER :: OELE
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .GE. 10)
C------------------------------------------------------------------------

C---     Récupère les données
      IF (SV2D_LGCY_HVALIDE(HELM)) THEN   ! cf note
         GDTA => SV2D_LGCY_REQGDTA(HELM)
         EDTA => SV2D_LGCY_REQEDTA(HELM)
         IPRN => SV2D_LGCY_REQIPRN(HELM)
         XPRG => SV2D_LGCY_REQXPRG(HELM)
      ELSEIF (LM_HELE_HVALIDE(HELM)) THEN
         OELE => LM_HELE_REQOMNG(HELM)
         SELECT TYPE(OELE)
            CLASS IS (SV2D_XBS_T)
               GDTA => OELE%GDTA
               EDTA => OELE%EDTA
               IPRN => OELE%IPRN
               XPRG => OELE%XPRG
            CLASS DEFAULT
               CALL ERR_ASG(ERR_FTL, 'Unsupported type')
         END SELECT
      ELSE
         CALL ERR_ASG(ERR_FTL, 'Unsupported type')
      ENDIF

C---     Contrôles      
D     CALL ERR_ASR(NNL .EQ. GDTA%NNL)

C---     Initialisation de VPOST
      CALL DINIT(NPST*NNL, ZERO, VPOST, 1)

C---     Boucle sur les éléments
      DO 10 IC=1,GDTA%NELCOL
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Connectivités du T6
         NO1  = GDTA%KNGV(1,IE)
         NO2  = GDTA%KNGV(2,IE)
         NO3  = GDTA%KNGV(3,IE)
         NO4  = GDTA%KNGV(4,IE)
         NO5  = GDTA%KNGV(5,IE)
         NO6  = GDTA%KNGV(6,IE)

C---        Métriques du T3
         DETJT3 = GDTA%VDJV(5,IE)*UN_4

C---        Viscosité physique
         VIS1 = EDTA%VPREV(1,1,IE)
         VIS2 = EDTA%VPREV(1,2,IE)
         VIS3 = EDTA%VPREV(1,3,IE)
         VIS4 = EDTA%VPREV(1,4,IE)

C---        Viscosité numérique
         VNM1 = EDTA%VPREV(2,1,IE) - VIS1
         VNM2 = EDTA%VPREV(2,2,IE) - VIS2
         VNM3 = EDTA%VPREV(2,3,IE) - VIS3
         VNM4 = EDTA%VPREV(2,4,IE) - VIS4

C---        Assemblage aux noeuds pour le lissage
         VPOST(8,NO1) = VPOST(8,NO1) + VIS1*DETJT3
         VPOST(8,NO2) = VPOST(8,NO2) + (VIS1+VIS2+VIS4)*DETJT3*UN_3
         VPOST(8,NO3) = VPOST(8,NO3) + VIS2*DETJT3
         VPOST(8,NO4) = VPOST(8,NO4) + (VIS2+VIS3+VIS4)*DETJT3*UN_3
         VPOST(8,NO5) = VPOST(8,NO5) + VIS3*DETJT3
         VPOST(8,NO6) = VPOST(8,NO6) + (VIS1+VIS3+VIS4)*DETJT3*UN_3

C---        Propriété élémentaire
         VPOST(9,NO1) = VPOST(9,NO1) + VNM1*DETJT3
         VPOST(9,NO2) = VPOST(9,NO2) + (VNM1+VNM2+VNM4)*DETJT3*UN_3
         VPOST(9,NO3) = VPOST(9,NO3) + VNM2*DETJT3
         VPOST(9,NO4) = VPOST(9,NO4) + (VNM2+VNM3+VNM4)*DETJT3*UN_3
         VPOST(9,NO5) = VPOST(9,NO5) + VNM3*DETJT3
         VPOST(9,NO6) = VPOST(9,NO6) + (VNM1+VNM3+VNM4)*DETJT3*UN_3

C---        Accumulation des DETJT3
         VPOST(10,NO1) = VPOST(10,NO1) + DETJT3
         VPOST(10,NO2) = VPOST(10,NO2) + DETJT3
         VPOST(10,NO3) = VPOST(10,NO3) + DETJT3
         VPOST(10,NO4) = VPOST(10,NO4) + DETJT3
         VPOST(10,NO5) = VPOST(10,NO5) + DETJT3
         VPOST(10,NO6) = VPOST(10,NO6) + DETJT3

20    CONTINUE
10    CONTINUE

C---     Transfert des valeurs aux noeuds
      DO IN=1,GDTA%NNL
         QX = EDTA%VDLG(1,IN)
         QY = EDTA%VDLG(2,IN)
         H  = EDTA%VDLG(3,IN)

         ZF = EDTA%VPRNO(IPRN%Z,IN)
         UX = EDTA%VPRNO(IPRN%U,IN)
         UY = EDTA%VPRNO(IPRN%V,IN)
         PA = EDTA%VPRNO(IPRN%H,IN)
         P  = H - ZF
         V  = HYPOT(UX, UY)
         Q  = HYPOT(QX, QY)

         VP = VPOST(8,IN) / VPOST(10,IN)  ! Viscosité physique
         VN = VPOST(9,IN) / VPOST(10,IN)  ! Viscosité numérique

C---        Chargement de VPOST
         VPOST( 1,IN) = UX
         VPOST( 2,IN) = UY
         VPOST( 3,IN) = H
         VPOST( 4,IN) = V
         VPOST( 5,IN) = P
         VPOST( 6,IN) = Q

         VPOST( 7,IN) = V / SQRT(XPRG%GRAVITE*PA)

         VPOST( 8,IN) = SQRT(Q*EDTA%VPRNO(IPRN%COEFF_FROT,IN))
         VPOST( 9,IN) = VP
         VPOST(10,IN) = VN
      ENDDO

      SV2D_PST_SIM_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_PST_SIM_LOG
C
C Description:
C     La fonction privée SV2D_PST_SIM_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_SIM_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'sv2d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'sv2d_pst_sim.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .EQ. 10)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SV2D_POST_SIMULATION:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<25>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)')    'MSG_VALEUR#<25>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR,  1, NPST, NNL, VPOST, 'u')
      IERR = PS_PSTD_LOGVAL(HNUMR,  2, NPST, NNL, VPOST, 'v')
      IERR = PS_PSTD_LOGVAL(HNUMR,  3, NPST, NNL, VPOST, 'h')
      IERR = PS_PSTD_LOGVAL(HNUMR,  4, NPST, NNL, VPOST, '|u|')
      IERR = PS_PSTD_LOGVAL(HNUMR,  5, NPST, NNL, VPOST, 'H')
      IERR = PS_PSTD_LOGVAL(HNUMR,  6, NPST, NNL, VPOST, '|q|')
      IERR = PS_PSTD_LOGVAL(HNUMR,  7, NPST, NNL, VPOST, 'Fr')
      IERR = PS_PSTD_LOGVAL(HNUMR,  8, NPST, NNL, VPOST, 'u*')
      IERR = PS_PSTD_LOGVAL(HNUMR,  9, NPST, NNL, VPOST, 'nu physical')
      IERR = PS_PSTD_LOGVAL(HNUMR, 10, NPST, NNL, VPOST, 'nu numerical')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      SV2D_PST_SIM_LOG = ERR_TYP()
      RETURN
      END
