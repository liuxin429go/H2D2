C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_prcpres.for,v 1.12 2012/01/10 20:20:00 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PRCPRES
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PRCPRES_M
            
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XBS_PRCPRES
C
C Description:
C     Calcul des propriétés élémentaires indépendantes de VDLG
C     pour les éléments de surface
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PRCPRES(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PRCPRES
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XBS_PRCPRES = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PRCPRES

      END SUBMODULE SV2D_XBS_PRCPRES_M

