C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_clcdlib.for,v 1.14 2015/12/05 14:32:49 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_CLCDLIB
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_CLCDLIB_M
            
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: SV2D_XBS_CLCDLIB
C
C Description:
C     La fonction SV2D_XBS_CLCDLIB
C        CaLCul sur les Degrés de LIBerté
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_CLCDLIB(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_CLCDLIB
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'eacdcl.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE, ID
      INTEGER NO1, NO2, NO3
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      INTEGER, POINTER :: KNGV (:,:)
      INTEGER, POINTER :: KDIMP(:,:)
      REAL*8,  POINTER :: VDLG (:,:)
C-----------------------------------------------------------------------
      LOGICAL EST_PHANTOME
      EST_PHANTOME(ID) = BTEST(ID, EA_TPCL_PHANTOME)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(IC, IE)
!$omp& private(GDTA, EDTA)
!$omp& private(KNGV, KDIMP, VDLG)
!$omp& private(NO1, NO2, NO3)

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      KNGV => GDTA%KNGV
      KDIMP=> EDTA%KDIMP
      VDLG => EDTA%VDLG

C---     Assigne les noeuds milieux en niveau d'eau
      DO IC=1,GDTA%NELCOL
!$omp  do
      DO IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

         NO2 = KNGV(2,IE)
         IF (EST_PHANTOME(KDIMP(3,NO2))) THEN
            NO1 = KNGV(1,IE)
            NO3 = KNGV(3,IE)
            VDLG(3,NO2) = UN_2*(VDLG(3,NO1) + VDLG(3,NO3))
         ENDIF

         NO2 = KNGV(4,IE)
         IF (EST_PHANTOME(KDIMP(3,NO2))) THEN
            NO1 = KNGV(3,IE)
            NO3 = KNGV(5,IE)
            VDLG(3,NO2) = UN_2*(VDLG(3,NO1) + VDLG(3,NO3))
         ENDIF

         NO2 = KNGV(6,IE)
         IF (EST_PHANTOME(KDIMP(3,NO2))) THEN
            NO1 = KNGV(5,IE)
            NO3 = KNGV(1,IE)
            VDLG(3,NO2) = UN_2*(VDLG(3,NO1) + VDLG(3,NO3))
         ENDIF

      ENDDO
!$omp end do
      ENDDO

      IERR = ERR_OMP_RDC()
!$omp end parallel

      SV2D_XBS_CLCDLIB = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_CLCDLIB

      END SUBMODULE SV2D_XBS_CLCDLIB_M
