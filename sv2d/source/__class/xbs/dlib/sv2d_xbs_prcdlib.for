C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cnn_prcdlib.for,v 1.7 2015/11/16 20:52:45 secretyv Exp $
C
C Description:   St-Venant 2D Conservatif - Numérotation par Noeud
C
C Notes:
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PRCDLIB
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PRCDLIB_M
            
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: SV2D_XBS_PRCDLIB
C
C Description:
C     La fonction SV2D_XBS_PRCDLIB monte la table de localisation
C     des DDL des noeuds pour une numérotation par noeud.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PRCDLIB(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PRCDLIB
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'

      INTEGER ID, IL, IN, IK
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
      LOGICAL EST_RETIRE
      LOGICAL EST_DIRICHLET
      LOGICAL EST_PHANTOME
      LOGICAL EST_PARALLEL
      EST_DIRICHLET(ID) = BTEST(ID, EA_TPCL_DIRICHLET)
      EST_PHANTOME (ID) = BTEST(ID, EA_TPCL_PHANTOME)
      EST_PARALLEL (ID) = BTEST(ID, EA_TPCL_PARALLEL)
      EST_RETIRE(ID) = EST_DIRICHLET(ID) .OR.
     &                 EST_PHANTOME (ID)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Initialise KLOCN
      EDTA%KLOCN(:,:) = 0

C---     Assigne les numéros d'équations
      IL = 0
      DO IN=1, GDTA%NNL
         DO ID=1,EDTA%NDLN
            IK = EDTA%KDIMP(ID,IN)
            IF (.NOT. EST_RETIRE(IK)) THEN
               IL = IL + 1
               EDTA%KLOCN(ID,IN) = IL
               IF (.NOT. EST_PARALLEL(IK)) THEN
                  EDTA%KLOCN(ID,IN) = IL
               ELSE
                  EDTA%KLOCN(ID,IN) = -IL
               ENDIF
            ENDIF
         ENDDO
      ENDDO
      EDTA%NEQL = IL

C---     Impose les C.L. de Dirichlet dans VDLG
      DO IN=1, GDTA%NNL
         DO ID=1,EDTA%NDLN
            IK = EDTA%KDIMP(ID,IN)
            IF (EST_DIRICHLET(IK)) THEN
               EDTA%VDLG(ID, IN) = EDTA%VDIMP(ID, IN)
            ENDIF
         ENDDO
      ENDDO

      SV2D_XBS_PRCDLIB = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PRCDLIB

      END SUBMODULE SV2D_XBS_PRCDLIB_M
      