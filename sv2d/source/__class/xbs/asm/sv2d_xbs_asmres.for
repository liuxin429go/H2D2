C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_asmres.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_XBS_ASMRES
C   Private:
C     INTEGER SV2D_XBS_ASMRES_RV
C     INTEGER SV2D_$_CLC_PRE
C
C************************************************************************

      MODULE SV2D_XBS_ASMRES_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XBS_ASMRES_V(IT,
     &                           SELF,
     &                           PDTA,
     &                           VRES)

      USE SV2D_XBS_M
      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER SV2D_XBS_ASMRES_RV
      INTEGER IT
      TYPE (SV2D_XBS_T),INTENT(IN) :: SELF
      TYPE (GP_PAGE_DATA_T), INTENT(IN) :: PDTA
      REAL*8 , INTENT(INOUT) :: VRES(:,:)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IERR
      INTEGER IE
      REAL*8  VFE(6*3)
      REAL*8, DIMENSION(:,:), POINTER :: VPRN(EDTA%NPRNOL, GDTA%NNELV)
      REAL*8, DIMENSION(:)   :: VPRE(EDTA%NPREV)
      INTEGER SV2D_$_CLC_PRE
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C-------  Boucle sur les éléments de la tâche
C         ===================================
      DO IE=1, PDTA%NELE
         VPRN(:,:) => PDTA%VPRN(:,:,IT,IE)
         VPRE(:) = ZERO

C---        Pré-traitement avant calcul
         IERR = SV2D_$_CLC_PRE(EDTA%VPRGL,
     &                         VPRN,
     &                         VPRE,
     &                         PDTA%VDLG(:, :, IT, IE))

     !!    CALL SV2D_XBS_FE_V(VRES,
     !!&                      GDTA%VDJV(:,IE),
     !!&                      EDTA%VPRGL,
     !!&                      VPRN,
     !!&                      VPRE,
     !!&                      PDTA%VDLG(1, 1, IT, IE),
     !!&                      EDTA%VSOLR(:,IE))  !! (1, 1, IT, IE))
     !!
     !!    CALL SV2D_XBS_RE_V(VRES,
     !!&                      GDTA%VDJV(:,IE),
     !!&                      EDTA%VPRGL,
     !!&                      VPRN,
     !!&                      VPRE,
     !!&                      PDTA%VDLG(1, 1, IT, IE),
     !!&                      PDTA%VDLG(1, 1, IT, IE))

      ENDDO

      SV2D_XBS_ASMRES_RV = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMRES_V

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_$_CLC_PRE(VPRG, VPRN, VPRE, VDLE)

      IMPLICIT NONE

      INTEGER SV2D_$_CLC_PRE
      REAL*8, INTENT(IN)    :: VPRG(:)
      REAL*8, INTENT(INOUT) :: VPRN(:,:)
      REAL*8, INTENT(INOUT) :: VPRE(:)
      REAL*8, INTENT(IN)    :: VDLE(:,:)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xy4.fc'

      INTEGER IERR
      INTEGER IE, IT
      REAL*8  VFE(6*3)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Calcule les propriétés nodales
       IERR = SV2D_XY4_CLCPRNEV(HOBJ, VDLE, VPRN)

C---     CALCULE LES PROP. ELEMENTAIRES (VOLUME ET SURFACE)
!      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPREV(HOBJ)
!      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRES(HOBJ)

C---     CALCULE LES COND. LIMITES
!      IF (ERR_GOOD()) IERR = LM_ELEM_CLCCLIM(HOBJ)

      SV2D_$_CLC_PRE = ERR_TYP()
      RETURN
      END FUNCTION SV2D_$_CLC_PRE


      END MODULE SV2D_XBS_ASMRES_M
      
C************************************************************************
C Sommaire:
C
C Description: ASSEMBLAGE DU RESIDU:   "{F} - [K].{U}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XBS_ASMRES(SELF, HPGE, VFG_F)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_ASMKU
CDEC$ ENDIF

      USE SV2D_XBS_M
      USE GP_PAGE_M
      USE SV2D_XCL_M
      USE SV2D_XBS_ASMRES_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HPGE
      REAL*8, TARGET :: VRES_F(*)

      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SV2D_XBS_T), POINTER :: SELF
      TYPE (GP_PAGE_DATA_T),  POINTER :: PDTA
      REAL*8, POINTER :: VRES  (:)
!C-----------------------------------------------------------------------
!D     CALL ERR_PRE(EDTA%NDLN .LE. NDLNMAX)
C-----------------------------------------------------------------

      CALL LOG_TODO('Code en chantier!!!')
      CALL ERR_ASR(.FALSE.)

      IERR = ERR_OK

C---     Récupère les données
      SELF => SV2D_XBS_REQSELF(HOBJ)
      PDTA => GP_PAGE_REQPDTA(HPGE)

C---     Reshape the arrays
      VRES(1:SELF%EDTA%NDLL) => VRES_F(1:SELF%EDTA%NDLL)
      
C---     Boucle sur les tâches OMP
!$omp do !! sur les ntask gpu
!$omp  parallel
!$omp& private(IERR)
!$omp& default(shared)
      DO IT=1,PDTA%NTSK

C---        Contribution de volume
         IF (ERR_GOOD()) IERR = SV2D_XBS_ASMRES_V(IT, SELF, PDTA, VRES)

C---        Contribution de surface
!      IF (ERR_GOOD()) IERR = SV2D_XBS_ASMRES_S(IT, SELF, PDTA, VRES)

C---        Contribution des conditions limites
!      IF (ERR_GOOD()) IERR = SV2D_XBS_ASMRES_CL(IT, SELF ,PDTA, VRES)

      ENDDO

      IERR = ERR_OMP_RDC()
!$omp end do

      SV2D_XBS_ASMRES = ERR_TYP()
      RETURN
      END
