C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: SV2D
C      H2D2 Class: SV2D_XBS
C         SUBROUTINE SV2D_XBS_ASMKU_R
C         FTN (Sub)Module: SV2D_XBS_ASMKU_R_M
C            Public:
C            Private:
C               INTEGER SV2D_XBS_ASMKU_RV
C               INTEGER SV2D_XBS_ASMKU_RS
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_ASMKU_R_M

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XBS_ASMKU_RV
C
C Description:
C     Assemblage du résidu "[K].{U}" pour les éléments de volume
C
C Entrée:
C     SELF
C
C Sortie:
C     VFG
C
C Notes
C
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMKU_RV (SELF, VFG)

      USE SV2D_XBS_RE_M, ONLY: SV2D_XBS_RE_V

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IERR
      INTEGER IC, IE, IN, II, NO
      REAL*8, POINTER :: VDLE (:,:)
      REAL*8, POINTER :: VPRN (:,:)
      REAL*8, POINTER :: VFE  (:,:)
      INTEGER,POINTER :: KLOCE(:,:)
      INTEGER,POINTER :: KNE  (:)
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA

C---     Tables locales
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VDLE_LCL (NDLN_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VPRN_LCL (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VFE_LCL  (NDLN_LCL * NNEL_LCL)
      INTEGER, TARGET :: KLOCE_LCL(NDLN_LCL * NNEL_LCL)
      INTEGER, TARGET :: KNE_LCL  (NNEL_LCL)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)

C---     Reshape the arrays
      VDLE (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_LCL
      VPRN (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_LCL
      VFE  (1:EDTA%NDLN, 1:GDTA%NNELV) => VFE_LCL
      KLOCE(1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL
      KNE  (1:GDTA%NCELV) => KNE_LCL

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,GDTA%NELCOL
!$omp do
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Transfert des valeurs élémentaires
         KNE(:) = GDTA%KNGV(:,IE)
         VDLE(:,:) = EDTA%VDLG (:,KNE(:))
         VPRN(:,:) = EDTA%VPRNO(:,KNE(:))

C---        R(u)
         VFE(:,:) = ZERO
         IERR = SV2D_XBS_RE_V(VFE,
     &                        GDTA%VDJV(:,IE),
     &                        VPRN,
     &                        EDTA%VPREV(:,:,IE),
     &                        VDLE,
     &                        VDLE,
     &                        SELF%IPRN,
     &                        SELF%XPRG)

C---       Assemblage du vecteur global
         KLOCE(:,:) = EDTA%KLOCN(:,KNE(:))
         IERR = SP_ELEM_ASMFE(EDTA%NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE

      SV2D_XBS_ASMKU_RV = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMKU_RV

C************************************************************************
C Sommaire:  SV2D_XBS_ASMKU_RS
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On transfert les propriétés du T6L et du L3L par simplicité de
C     gestion. Le L3L n'aurait besoin que de deux sous-triangles T3.
C     On assemble une matrice complète 18x18, car la matrice Ke doit
C     être carrée pour l'assemblage.
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMKU_RS(SELF, VFG)

      USE SV2D_XBS_RE_M, ONLY: SV2D_XBS_RE_S

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IERR
      INTEGER IN, II, NO
      INTEGER IES, IEV, ICT
      REAL*8, POINTER :: VDLEV(:, :)
      REAL*8, POINTER :: VPRNV(:, :)
      REAL*8, POINTER :: VFE  (:,:)
      INTEGER,POINTER :: KNEV (:)
      INTEGER,POINTER :: KLOCE(:,:)
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA

C---     Tables locales
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VDLEV_LCL(NDLN_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VPRNV_LCL(NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VFE_LCL  (NDLN_LCL * NNEL_LCL)
      INTEGER, TARGET :: KLOCE_LCL(NDLN_LCL * NNEL_LCL)
      INTEGER, TARGET :: KNE_LCL  (NNEL_LCL)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)

C---     Reshape the arrays
      VDLEV(1:EDTA%NDLN, 1:GDTA%NNELV) => VDLEV_LCL
      VPRNV(1:EDTA%NPRNO,1:GDTA%NNELV) => VPRNV_LCL
      VFE  (1:EDTA%NDLN, 1:GDTA%NNELV) => VFE_LCL
      KNEV (1:GDTA%NNELV) => KNE_LCL
      KLOCE(1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL

C-------  Boucle sur les elements
C         =======================
      DO IES=1,GDTA%NELLS

C---        Element parent et côté
         IEV = GDTA%KNGS(4,IES)
         ICT = GDTA%KNGS(5,IES)

C---        Transfert des valeurs élémentaires
         KNEV(:) = GDTA%KNGV(:,IEV)
         VDLEV(:,:) = EDTA%VDLG (:,KNEV(:))
         VPRNV(:,:) = EDTA%VPRNO(:,KNEV(:))

C---        R(u)
         VFE(:,:) = ZERO
         IERR = SV2D_XBS_RE_S(VFE,
     &                        GDTA%VDJV(:,IEV),
     &                        GDTA%VDJS(:,IES),
     &                        VPRNV,
     &                        EDTA%VPREV(:,:,IEV),
     &                        EDTA%VPRES(:,:,IES),
     &                        VDLEV,
     &                        VDLEV,
     &                        SELF%IPRN,
     &                        SELF%XPRG,
     &                        ICT)

C---        Assemblage du vecteur global
         KLOCE(:,:) = EDTA%KLOCN(:,KNEV(:))
         IERR = SP_ELEM_ASMFE(EDTA%NDLEV, KLOCE, VFE, VFG)

      ENDDO

      SV2D_XBS_ASMKU_RS = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMKU_RS

C************************************************************************
C Sommaire:
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_ASMKU_R(SELF, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_ASMKU_R
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VRES(:)

      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
         IERR = SV2D_XBS_ASMKU_RV (SELF, VRES)

         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         IERR = SV2D_XBS_ASMKU_RS(SELF, VRES)
      ENDIF

C---     Contributions des conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SELF%CLIM%ASMKU(SELF, VRES)
      ENDIF

      SV2D_XBS_ASMKU_R = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMKU_R
      
      END SUBMODULE SV2D_XBS_ASMKU_R_M
