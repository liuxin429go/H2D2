C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_ASMKT_R_Z
C   Private:
C     INTEGER SV2D_XBS_ASMKT_RV
C     INTEGER SV2D_XBS_ASMKT_RS
C
C************************************************************************

      MODULE SV2D_XBS_ASMKT_R_Z_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: SV2D_XBS_ASMKT_RV
C
C Description:
C     La fonction SV2D_XBS_ASMKT_RV calcule la matrice tangente
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée: HMTX
C
C Sortie: KLOCE : Table de localisation élémentaire
C         VKE : Matrice élémentaire
C
C Notes:
C     1) La matrice tangente est donnée par (Gouri Dhatt p. 342):
C        Kt_ij = K_ij + Somme_sur_l ( (dK_il / du_j) * u_l )
C     avec:
C        dK_il/du_j ~= (K(u+delu_j) - K(u) ) / delu_j
C
C     2) La version d'Hydrosim est plus efficace. Elle calcule la partie
C     tangente comme:
C        dK_il/du_j ~= ([K(u+delu_j)]{u+delu_j} - [K(u)]{u} ) / delu_j
C
C     3) La perturbation d'un DDL en h sur un noeud sommet entraîne une
C     modification du DDL pour le noeud milieu dans le calcul des prop. nodales.
C     La valeur originale du noeud sommet est restaurée à la fin de la boucle
C     de perturbation. Le noeud milieu lui n'est restauré qu'au prochain
C     calcul de prop. nodales. Comme le dernier DDL perturbé est en v
C     (noeud 6), il y a implicitement restauration.
C************************************************************************
      FUNCTION SV2D_XBS_ASMKT_RV(SELF, HMTX, F_ASM)

      USE COMPLEXIFY
      USE SV2D_XBS_M
      USE SV2D_XBS_RE_Z_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER SV2D_XBS_ASMKT_RV
      TYPE (SV2D_XBS_T), TARGET, INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_fnct.fi'

      INTEGER IERR
      INTEGER IC, IE, IN, ID, IDG
      INTEGER HVFT, HPRNE, HPREVE
      REAL*8  PENA
      REAL*8  VDL_DEL, VDL_INV
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_XPRG_T),     POINTER :: XPRG
      COMPLEX*16, POINTER :: VDLE (:, :)
      COMPLEX*16, POINTER :: VPRN (:, :)
      COMPLEX*16, POINTER :: VPRE (:, :)
      COMPLEX*16, POINTER :: VRES (:, :)
      REAL*8,     POINTER :: VDJE (:)
      REAL*8,     POINTER :: VKE  (:,:)
      INTEGER,    POINTER :: KLOCE(:,:)
      INTEGER,    POINTER :: KNE  (:)

C---     Tables locales
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL = NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NDJE_LCL =  5
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NPRE_LCL =  8
      COMPLEX*16, TARGET :: VDLE_LCL (NDLE_LCL)
      COMPLEX*16, TARGET :: VPRN_LCL (NPRN_LCL * NNEL_LCL)
      COMPLEX*16, TARGET :: VPRE_LCL (NPRE_LCL)
      COMPLEX*16, TARGET :: VRES_LCL (NDLE_LCL)
      REAL*8,     TARGET :: VDJE_LCL (NDJE_LCL)
      REAL*8,     TARGET :: VKE_LCL  (NDLE_LCL * NDLE_LCL)
      INTEGER,    TARGET :: KNE_LCL  (NNEL_LCL)
      INTEGER,    TARGET :: KLOCE_LCL(NDLE_LCL)

      BYTE, POINTER :: VDJE_B(:)
      BYTE, POINTER :: VPRN_B(:)
      BYTE, POINTER :: VDLE_B(:)
      BYTE, POINTER :: VPRE_B(:)
C-----------------------------------------------------------------------
      REAL*8 CR, CI
      COMPLEX*16 CMPLX16
      CMPLX16(CR, CI) = CMPLX(CR, CI, KIND(CR))
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      XPRG => SELF%XPRG
      !PDTA => GP_PAGE_REQPDTA(HPGE)
      IERR = OB_VTBL_REQMTH(SELF%HVTBL, SV2D_VT_CLCPRNEV, HPRNE)
      IERR = OB_VTBL_REQMTH(SELF%HVTBL, SV2D_VT_CLCPREVE, HPREVE)

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_ASR(NDLE_LCL .GE. EDTA%NDLEV)
D     CALL ERR_ASR(NDJE_LCL .GE. GDTA%NDJV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)
D     CALL ERR_ASR(NPRE_LCL .GE. EDTA%NPREV)

C---     Reshape the arrays
      VDLE  (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_LCL
      VPRN  (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_LCL
      VDJE  (1:GDTA%NDJV)  => VDJE_LCL
      VPRE  (1:EDTA%NPREV_D1, 1:EDTA%NPREV_D2) => VPRE_LCL
      VKE   (1:EDTA%NDLEV,1:EDTA%NDLEV) => VKE_LCL
      VRES  (1:EDTA%NDLN, 1:GDTA%NNELV) => VRES_LCL
      KNE   (1:GDTA%NNELV) => KNE_LCL
      KLOCE (1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL

C---     Pointeurs pour le calcul des propriétés perturbées
      VDJE_B  => SO_ALLC_CST2B(VDJE(:))
      VDLE_B  => SO_ALLC_CST2B(VDLE(:,1))
      VPRN_B  => SO_ALLC_CST2B(VPRN(:,1))
      VPRE_B  => SO_ALLC_CST2B(VPRE(:,1))

C-------  BOUCLE SUR LES ELEMENTS DE VOLUME
C         =================================
      DO 10 IC=1,GDTA%NELCOL
!$omp  do
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
         KNE(:) = GDTA%KNGV(:,IE)

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = EDTA%KLOCN(:, KNE(:))

C---        Transfert des valeurs nodales T6L
         VDLE (:,:) = EDTA%VDLG (:,KNE(:))
         VPRN (:,:) = EDTA%VPRNO(:,KNE(:))

C---        Transfert des valeurs élémentaires
         VDJE(:) = GDTA%VDJV (:,IE)
         VPRE(:,:) = EDTA%VPREV(:,:,IE)

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         IDG = 0
         DO IN=1,GDTA%NNELV
         DO ID=1,EDTA%NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL
            VDL_DEL = XPRG%PNUMR_DELPRT * VDLE(ID,IN)
     &              + SIGN(XPRG%PNUMR_DELMIN, VDLE(ID,IN))
            VDL_INV = XPRG%PNUMR_OMEGAKT / VDL_DEL
            VDLE(ID,IN) = CMPLX16(REAL(VDLE(ID,IN)), VDL_DEL)
            !! VDLE(ID,IN)%IM = VDL_DEL

C---           Propriétés nodales perturbées
            IF (XPRG%PNUMR_PRTPRNO) THEN
               IERR = SO_FUNC_CALL2(HPRNE,
     &                              VDLE_B,
     &                              VPRN_B)
            ENDIF

C---           Propriétés élémentaires perturbées
            IF (XPRG%PNUMR_PRTPREL) THEN
               IERR = SO_FUNC_CALL4(HPREVE,
     &                              VDJE_B,
     &                              VPRN_B,
     &                              VDLE_B,
     &                              VPRE_B)
            ENDIF

C---           R(u + du_id)
            VRES(:,:) = ZERO
            IERR = SV2D_XBS_RE_V(VRES,
     &                           VDJE,
     &                           VPRN,
     &                           VPRE,
     &                           VDLE,
     &                           VDLE,  ! VRHS
     &                           SELF%IPRN,
     &                           SELF%XPRG)

C---           Restaure la valeur originale
            VDLE(ID,IN) = CMPLX16(REAL(VDLE(ID, IN)), ZERO)

C---           (R(u + du_id) - R(u))/du
            VKE(:,IDG) = AIMAG(VRES_LCL(:) * VDL_INV)

199         CONTINUE
         ENDDO
         ENDDO

C---        Pénalisation en hh
         PENA = XPRG%PNUMR_PENALITE*VDJE(5)
         VKE( 3, 3) = MAX(VKE( 3, 3), PENA)
         VKE( 9, 9) = MAX(VKE( 9, 9), PENA)
         VKE(15,15) = MAX(VKE(15,15), PENA)

C---       Assemble la matrice
         IERR = F_ASM(HMTX, EDTA%NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      SV2D_XBS_ASMKT_RV = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMKT_RV

C************************************************************************
C Sommaire: SV2D_XBS_ASMKT_RS
C
C Description:
C     La fonction SV2D_XBS_ASMKT_RS calcule la matrice tangente élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) c.f. SV2D_XBS_ASMKT_RV
C
C     2) Le noeud opposé à l'élément de surface est perturbé en trop.
C     La matrice Ke peut être nulle.
C
C     3) Il faut perturber tous les dll de l'élément de volume car les
C     VPREV dépendent des VPRN. Les VPRES ne sont pas utilisées.
C************************************************************************
      FUNCTION SV2D_XBS_ASMKT_RS(SELF, HMTX, F_ASM)

      USE SV2D_XBS_M
      USE SV2D_XBS_RE_Z_M
      USE COMPLEXIFY
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER SV2D_XBS_ASMKT_RS
      TYPE (SV2D_XBS_T), TARGET, INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_fnct.fi'
      INCLUDE 'sv2d_xbs.fc'

      INTEGER IERR
      INTEGER IN, ID, IDG
      INTEGER IES, IEV, ICT
      INTEGER HVFT, HPRNEV, HPREVE, HPRESE
      REAL*8  VDL_DEL, VDL_INV
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_XPRG_T),     POINTER :: XPRG
      COMPLEX*16, POINTER :: VDLEV (:, :)
      COMPLEX*16, POINTER :: VPRNV (:, :)
      COMPLEX*16, POINTER :: VPREV (:, :)
      COMPLEX*16, POINTER :: VPRES (:, :)
      COMPLEX*16, POINTER :: VRESE (:, :)
      REAL*8,     POINTER :: VDJEV (:)
      REAL*8,     POINTER :: VKE   (:,:)
      INTEGER,    POINTER :: KNEV  (:)
      INTEGER,    POINTER :: KLOCE (:, :)

C---     Tables locales
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL = NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NDJE_LCL =  5
      INTEGER, PARAMETER :: NPRE_LCL =  8
      COMPLEX*16, TARGET :: VDLEV_LCL (NDLN_LCL * NNEL_LCL)
      COMPLEX*16, TARGET :: VPRNV_LCL (NPRN_LCL * NNEL_LCL)
      COMPLEX*16, TARGET :: VPREV_LCL (NPRE_LCL)
      COMPLEX*16, TARGET :: VPRES_LCL (NPRE_LCL)
      COMPLEX*16, TARGET :: VRESE_LCL (NDLE_LCL)
      REAL*8,     TARGET :: VDJEV_LCL (NDJE_LCL)
      REAL*8,     TARGET :: VKE_LCL   (NDLE_LCL * NDLE_LCL)
      INTEGER,    TARGET :: KNEV_LCL  (NNEL_LCL)
      INTEGER,    TARGET :: KLOCE_LCL (NDLE_LCL)
      
      BYTE, POINTER :: VDJEV_B(:)
      BYTE, POINTER :: VPRNV_B(:)
      BYTE, POINTER :: VDLEV_B(:)
      BYTE, POINTER :: VPREV_B(:)
C-----------------------------------------------------------------------
      REAL*8 CR, CI
      COMPLEX*16 CMPLX16
      CMPLX16(CR, CI) = CMPLX(CR, CI, KIND(CR))
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      XPRG => SELF%XPRG
      !PDTA => GP_PAGE_REQPDTA(HPGE)
      IERR = OB_VTBL_REQMTH(SELF%HVTBL, SV2D_VT_CLCPRNEV, HPRNEV)
      IERR = OB_VTBL_REQMTH(SELF%HVTBL, SV2D_VT_CLCPREVE, HPREVE)
      IERR = OB_VTBL_REQMTH(SELF%HVTBL, SV2D_VT_CLCPRESE, HPRESE)

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_ASR(NDLE_LCL .GE. EDTA%NDLEV)
D     CALL ERR_ASR(NDJE_LCL .GE. GDTA%NDJV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)
D     CALL ERR_ASR(NPRE_LCL .GE. EDTA%NPREV)

C---     Reshape the arrays
      VDLEV (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLEV_LCL
      VPRNV (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRNV_LCL
      VDJEV (1:GDTA%NDJV)  => VDJEV_LCL
      VPREV (1:EDTA%NPREV_D1, 1:EDTA%NPREV_D2) => VPREV_LCL
      VPRES (1:EDTA%NPRES_D1, 1:EDTA%NPRES_D2) => VPRES_LCL
      VKE   (1:EDTA%NDLEV,1:EDTA%NDLEV) => VKE_LCL
      VRESE (1:EDTA%NDLN, 1:GDTA%NNELV) => VRESE_LCL
      KNEV  (1:GDTA%NNELV) => KNEV_LCL
      KLOCE (1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL

C---     Pointeurs pour le calcul des propriétés perturbées
      VDJEV_B  => SO_ALLC_CST2B(VDJEV(:))
      VDLEV_B  => SO_ALLC_CST2B(VDLEV(:,1))
      VPRNV_B  => SO_ALLC_CST2B(VPRNV(:,1))
      VPREV_B  => SO_ALLC_CST2B(VPREV(:,1))

!!!C---     Pointeurs dans les tables pour les propriétés élémentaires de surface perturbées
!!!      XPRESE(1) = SO_ALLC_CSTK2B(BA, ICT)
!!!      XPRESE(2) = SO_ALLC_CSTV2B(BA, VDJEV(1))
!!!      XPRESE(3) = SO_ALLC_CSTV2B(BA, VPRNV(1,1))
!!!      XPRESE(4) = SO_ALLC_CSTV2B(BA, VPREV(1,1))
!!!      XPRESE(5) = SO_ALLC_CSTV2B(BA, VDLEV(1,1))
!!!      XPRESE(6) = SO_ALLC_CSTV2B(BA, VPRES(1,1))

C-------  Boucle sur les elements de surface
C         ==================================
      DO IES=1,GDTA%NELLS

C---        Élément parent et coté
         IEV = GDTA%KNGS(4,IES)
         ICT = GDTA%KNGS(5,IES)

C---        Transfert des connectivités de l'élément T6L
         KNEV(:) = GDTA%KNGV(:,IEV)

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = EDTA%KLOCN(:, KNEV(:))

C---        Transfert des valeurs nodales T6L
         VDLEV(:,:) = EDTA%VDLG (:,KNEV(:))
         VPRNV(:,:) = EDTA%VPRNO(:,KNEV(:))

C---        Transfert des valeurs élémentaires
         VDJEV(:)   = GDTA%VDJV (:,IEV)
         VPREV(:,:) = EDTA%VPREV(:,:,IEV)
         VPRES(:,:) = EDTA%VPRES(:,:,IES)

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO

C---        Boucle de perturbation sur tous les ddl
C           =======================================
         IDG = 0
         DO IN=1,GDTA%NNELV
         DO ID=1,EDTA%NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL du T6L
            VDL_DEL = XPRG%PNUMR_DELPRT * VDLEV(ID,IN)
     &              + SIGN(XPRG%PNUMR_DELMIN, VDLEV(ID,IN))
            VDL_INV = XPRG%PNUMR_OMEGAKT / VDL_DEL
            VDLEV(ID,IN) = CMPLX16(REAL(VDLEV(ID,IN)), VDL_DEL)
            !! VDLEV_P(ID,IN)%IM = VDL_DEL

C---           Propriétés nodales perturbées
            IF (XPRG%PNUMR_PRTPRNO) THEN
               IERR = SO_FUNC_CALL2(HPRNEV,
     &                              VDLEV_B,
     &                              VPRNV_B)
            ENDIF

C---           Propriétés élémentaires perturbées
            IF (XPRG%PNUMR_PRTPREL) THEN
               IERR = SO_FUNC_CALL4(HPREVE,
     &                              VDJEV_B,
     &                              VPRNV_B,
     &                              VDLEV_B,
     &                              VPREV_B)
            ENDIF

!!!C---           Propriétés élémentaires de surface perturbées (pas utilisées)
!!!            IF (XPRG%PNUMR_PRTPREL) THEN
!!!               IERR = SO_FUNC_CALL6(HPRESE,
!!!     &                              BA(XPRESE(1)),
!!!     &                              BA(XPRESE(2)),
!!!     &                              BA(XPRESE(3)),
!!!     &                              BA(XPRESE(4)),
!!!     &                              BA(XPRESE(5)),
!!!     &                              BA(XPRESE(6)))
!!!            ENDIF

C---           R(u)
            VRESE(:,:) = ZERO
            IERR = SV2D_XBS_RE_S(VRESE,
     &                           GDTA%VDJV(:,IEV),
     &                           GDTA%VDJS(:,IES),
     &                           VPRNV,
     &                           VPREV,
     &                           VPRES,         ! Pas utilisé
     &                           VDLEV,
     &                           VDLEV,         ! VRHS
     &                           SELF%IPRN,
     &                           SELF%XPRG,
     &                           ICT)

C---           Restaure la valeur originale
            VDLEV(ID,IN) = CMPLX16(REAL(VDLEV(ID, IN)), ZERO)
            !! VDLEV(ID,IN)%IM = ZERO

C---           (R(u + du_id) - R(u))/du
            VKE(:,IDG) = AIMAG(VRESE_LCL(:) * VDL_INV)

199         CONTINUE
         ENDDO
         ENDDO

C---       Assemble la matrice
         IERR = F_ASM(HMTX, EDTA%NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      SV2D_XBS_ASMKT_RS = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMKT_RS

      END MODULE SV2D_XBS_ASMKT_R_Z_M

C************************************************************************
C Sommaire: SV2D_CBS_ASMKT_R_Z
C
C Description: ASSEMBLAGE DE LA MATRICE TANGENTE
C     La fonction SV2D_CBS_ASMKT_R_Z calcule le matrice tangente
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C     Version avec dérivation complexe.
C     "Complex Step Derivative Approximation"
C     JOAQUIM R. R. A. MARTINS - University of Toronto Institute for Aerospace Studies
C     PETER STURDZA and JUAN J. ALONSO - Stanford University
C     ACM Transactions on Mathematical Software, Vol. 29, No. 3, September 2003, Pages 245–262
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées.
C************************************************************************
      FUNCTION SV2D_XBS_ASMKT_R_Z(SELF, HMTX, F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_ASMKT_R_Z
CDEC$ ENDIF

      USE SV2D_XBS_M
      USE SV2D_XBS_ASMKT_R_Z_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'

      INTEGER  IERR
      TYPE (SV2D_XBS_T), POINTER :: SELF
C----------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(SELF)
         SELF => SV2D_XBS_REQSELF(HOBJ)
         IERR = SV2D_XBS_ASMKT_RV(SELF, HMTX, F_ASM)

         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         SELF => SV2D_XBS_REQSELF(HOBJ)
         IERR = SV2D_XBS_ASMKT_RS(SELF, HMTX, F_ASM)
      ENDIF

C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         SELF => SV2D_XBS_REQSELF(HOBJ)
         IERR = SELF%CLIM%ASMKT(SELF, HMTX, F_ASM)
      ENDIF

      SV2D_XBS_ASMKT_R_Z = ERR_TYP()
      RETURN
      END
