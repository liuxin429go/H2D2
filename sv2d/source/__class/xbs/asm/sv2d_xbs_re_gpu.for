C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_re.for,v 1.8 2015/04/30 13:22:39 secretyv Exp $
C
C Functions:
C   Public:
C   Private:
C     INTEGER SV2D_XBS_FE_V
C     INTEGER SV2D_XBS_RE_K
C     INTEGER SV2D_XBS_RE_V
C     INTEGER SV2D_XBS_RE_S
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_XBS_FE_V
C
C Description:
C     La fonction SV2D_XBS_FE_V calcule le résidu élémentaire dû à un
C     élément de volume.
C     Les tables passées en paramètre sont des tables élémentaires.
C
C Entrée:
C     VDJE        Table du Jacobien Élémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales
C     VPRE        Table de PRopriétés Élémentaires
C     VDLE        Table de Degrés de Libertés Élémentaires
C     VRHS        Table du Membre de Droite
C
C Sortie:
C     VRES        Résidu élémentaire
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_XBS_FE_V(VRES,
     &                       VDJE,
     &                       VPRG,
     &                       VPRN,
     &                       VPRE,
     &                       VDLE,
     &                       VSLR)

      IMPLICIT NONE

      REAL*8, INTENT(INOUT) :: VRES(:,:)
      REAL*8, INTENT(IN)    :: VDJE(:)
      REAL*8, INTENT(IN)    :: VPRG(:)
      REAL*8, INTENT(IN)    :: VPRN(:,:)
      REAL*8, INTENT(IN)    :: VPRE(:,:)
      REAL*8, INTENT(IN)    :: VDLE(:,:)
      REAL*8, INTENT(IN)    :: VSLR(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xbs.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---        Sollicitations
      CALL SV2D_CMP_FE_V_SLR(VRES, VDJE, VPRG, VPRN, VPRE, VDLE, VSLR)

      SV2D_XBS_FE_V = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XBS_RE_V
C
C Description:
C     La fonction SV2D_XBS_ASMR_V calcule le résidu élémentaire dû à un
C     élément de volume.
C     Les tables passées en paramètre sont des tables élémentaires.
C
C Entrée:
C     VDJE        Table du Jacobien Élémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales
C     VPRE        Table de PRopriétés Élémentaires
C     VDLE        Table de Degrés de Libertés Élémentaires
C     VRHS        Table du Membre de Droite
C
C Sortie:
C     VRES        Résidu élémentaire
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_XBS_RE_V(VRES,
     &                       VDJE,
     &                       VPRG,
     &                       VPRN,
     &                       VPRE,
     &                       VDLE,
     &                       VRHS)

      IMPLICIT NONE

      REAL*8, INTENT(INOUT) :: VRES(:,:)
      REAL*8, INTENT(IN)    :: VDJE(:)
      REAL*8, INTENT(IN)    :: VPRG(:)
      REAL*8, INTENT(IN)    :: VPRN(:,:)
      REAL*8, INTENT(IN)    :: VPRE(:,:)
      REAL*8, INTENT(IN)    :: VDLE(:,:)
      REAL*8, INTENT(IN)    :: VRHS(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs.fc'

      INTEGER IT3, ID
      REAL*8  VDJT3(5)

      INTEGER, PARAMETER :: KNET3(3,4) = (/ 1,2,6,
     &                                      2,3,4,
     &                                      6,4,5,
     &                                      4,6,2 /)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Métriques du sous-élément
      VDJT3(1) = UN_2*VDJE(1)
      VDJT3(2) = UN_2*VDJE(2)
      VDJT3(3) = UN_2*VDJE(3)
      VDJT3(4) = UN_2*VDJE(4)
      VDJT3(5) = UN_4*VDJE(5)

C---     BOUCLE SUR LES SOUS-ÉLÉMENTS
C        ============================
      DO IT3=1,4

C---        Le T3 interne est inversé
         IF (IT3 .EQ. 4) THEN
            DO ID=1,4
               VDJT3(ID) = -VDJT3(ID)
            ENDDO
         ENDIF

C---        Convection
         CALL SV2D_CMP_RE_V_CNV(VRES,
     &                          KNET3(:,IT3),
     &                          VDJT3, VPRG, VPRN, VPRE(:,IT3),
     &                          VDLE, VRHS)

C---        Diffusion
         CALL SV2D_CMP_RE_V_DIF(VRES,
     &                          KNET3(:,IT3),
     &                          VDJT3, VPRG, VPRN, VPRE(:,IT3),
     &                          VDLE, VRHS)

C---        Manning
         CALL SV2D_CMP_RE_V_MAN(VRES,
     &                          KNET3(:,IT3),
     &                          VDJT3, VPRG, VPRN, VPRE(:,IT3),
     &                          VDLE, VRHS)

C---        Vent
         CALL SV2D_CMP_RE_V_VNT(VRES,
     &                          KNET3(:,IT3),
     &                          VDJT3, VPRG, VPRN, VPRE(:,IT3),
     &                          VDLE, VRHS)

C---        Coriolis
         CALL SV2D_CMP_RE_V_COR(VRES,
     &                          KNET3(:,IT3),
     &                          VDJT3, VPRG, VPRN, VPRE(:,IT3),
     &                          VDLE, VRHS)
      ENDDO

C---        Gravité
      CALL SV2D_CMP_RE_V_GRV(VRES, VDJE, VPRG, VPRN, VPRE, VDLE, VRHS)

C---        Continuité
      CALL SV2D_CMP_RE_V_CNT(VRES, VDJE, VPRG, VPRN, VPRE, VDLE, VRHS)

C---        Pénalisation
      CALL SV2D_CMP_RE_V_PNA(VRES, VDJE, VPRG, VPRN, VPRE, VDLE, VRHS)

      SV2D_XBS_RE_V = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XBS_RE_S
C
C Description:
C     La fonction SV2D_XBS_ASMR_S calcule le matrice de rigidité
C     élémentaire due à un élément de surface.
C     Les tables passées en paramètre sont des tables élémentaires,
C     à part la table VDLG.
C
C Entrée:
C     VDJV        Table du Jacobien Élémentaire de l'élément de volume
C     VDJS        Table du Jacobien Élémentaire de l'élément de surface
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales de Volume
C     VPREV       Table de PRopriétés Élémentaires de Volume
C     VPRES       Table de PRopriétés Élémentaires de Surface
C     VDLE        Table de Degrés de Libertés Élémentaires de Volume
C     ICOTE       L'élément de surface forme le ième CÔTÉ du triangle
C
C Sortie:
C     VRES        Résidu élémentaire
C
C Notes:
C     On permute les métriques du T6L parent pour que le côté à traiter
C     soit le premier côté (noeuds 1-2-3).
C
C************************************************************************
      FUNCTION SV2D_XBS_RE_S(VRES,
     &                       VDJV,
     &                       VDJS,
     &                       VPRG,
     &                       VPRN,
     &                       VPREV,
     &                       VPRES,
     &                       VDLE,
     &                       VRHS,
     &                       ICOTE)

      IMPLICIT NONE

      REAL*8, INTENT(INOUT) :: VRES (:,:)
      REAL*8, INTENT(IN)    :: VDJV (:)
      REAL*8, INTENT(IN)    :: VDJS (:)
      REAL*8, INTENT(IN)    :: VPRG (:)
      REAL*8, INTENT(IN)    :: VPRN (:,:)
      REAL*8, INTENT(IN)    :: VPREV(:,:)
      REAL*8, INTENT(IN)    :: VPRES(:,:)
      REAL*8, INTENT(IN)    :: VDLE (:,:)
      REAL*8, INTENT(IN)    :: VRHS (:,:)
      INTEGER  ICOTE

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_subt3.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs.fc'

      INTEGER IL2, IT3
      REAL*8  VDJX(4), VDJY(4)
      REAL*8  VDJL2(3)
      REAL*8  VDJT3(5)

      INTEGER KNET6  (3, 3)                  ! 3 noeuds sommets, 3 cotés
      DATA KNET6  / 1,3,5,  3,5,1,  5,1,3/   ! Les sommets du T6L
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ICOTE .GE. 1 .AND. ICOTE .LE. 3)
C-----------------------------------------------------------------------

C---     MONTE LES TABLES AUX. POUR PERMUTER LES MÉTRIQUES
      VDJX(1) = VDJV(1)
      VDJX(2) = VDJV(2)
      VDJX(3) = -(VDJX(1)+VDJX(2))
      VDJX(4) = VDJX(1)
      VDJY(1) = VDJV(3)
      VDJY(2) = VDJV(4)
      VDJY(3) = -(VDJY(1)+VDJY(2))
      VDJY(4) = VDJY(1)

C---     Métriques permutées du sous-élément T3
      VDJT3(1) = UN_2*VDJX(ICOTE)
      VDJT3(2) = UN_2*VDJX(ICOTE+1)
      VDJT3(3) = UN_2*VDJY(ICOTE)
      VDJT3(4) = UN_2*VDJY(ICOTE+1)
      VDJT3(5) = UN_4*VDJV(5)

C---     Métriques du sous-élément L2
      VDJL2(1) = VDJS(1)
      VDJL2(2) = VDJS(2)
      VDJL2(3) = UN_2*VDJS(3)

C---        BOUCLE SUR LES SOUS-ÉLÉMENTS
C           ============================
      DO IL2=1,2
         IT3 = KT3(IL2, ICOTE)

C---        DIFFUSION
         CALL SV2D_CMP_RE_S_DIF(VRES,
     &                          KNET3(:, IL2, ICOTE),
     &                          VDJT3,
     &                          VDJL2,
     &                          VPRG,
     &                          VPRN,
     &                          VPREV(:,IT3),
     &                          VPRES(:,IL2),
     &                          VDLE,
     &                          VRHS)

      ENDDO

C---        CONTINUITE
      CALL SV2D_CMP_RE_S_CNT(VRES,
     &                       KNET6  (:, ICOTE),
     &                       VDJT3,
     &                       VDJL2,
     &                       VPRG,
     &                       VPRN,
     &                       VPREV,
     &                       VPRES,
     &                       VDLE,
     &                       VRHS)

      SV2D_XBS_RE_S = ERR_TYP()
      RETURN
      END
