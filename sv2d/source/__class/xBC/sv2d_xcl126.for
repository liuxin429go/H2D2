C************************************************************************
C --- Copyright (c) INRS 2012-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Description:
C     Sollicitation de débit via la forme faible de la continuité.
C     Débit global redistribué en fonction du niveau d'eau actuel.
C     Formulation HYDROSIM.
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL126_000
C     INTEGER SV2D_XCL126_999
C     INTEGER SV2D_XCL126_COD
C     INTEGER SV2D_XCL126_CLC
C     INTEGER SV2D_XCL126_ASMF
C     INTEGER SV2D_XCL126_ASMK
C   Private:
C     INTEGER SV2D_XCL126_INIVTBL
C     INTEGER SV2D_XCL126_ASMRE
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_XCL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_XCL126_DATA_000

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl126.fc'

      DATA SV2D_XCL126_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL126_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL126_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl126.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL126_INIVTBL()

      SV2D_XCL126_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL126_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl126.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL126_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL126_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_INIVTBL()

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl126.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl126.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_XCL126_HLP
      EXTERNAL SV2D_XCL126_COD
      EXTERNAL SV2D_XCL126_CLC
      EXTERNAL SV2D_XCL126_ASMF
      EXTERNAL SV2D_XCL126_ASMK
      EXTERNAL SV2D_XCL126_ASMKT
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL126_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_XCL126_TPN)
!!!      IRET = C_ST_CRC32(SV2D_XCL126_TPN(1:LTPN), SV2D_XCL126_TPC)
!!!      IERR = SV2D_XCL_INIVTBL2(HVFT, SV2D_XCL126_TPN)
      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL126_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL126_TYP,
     &                     SV2D_XCL126_HLP)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_COD,  SV2D_XCL126_TYP,
     &                    SV2D_XCL126_COD)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_CLC,  SV2D_XCL126_TYP,
     &                    SV2D_XCL126_CLC)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMF, SV2D_XCL126_TYP,
     &                    SV2D_XCL126_ASMF)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMK, SV2D_XCL126_TYP,
     &                    SV2D_XCL126_ASMK)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMKT,SV2D_XCL126_TYP,
     &                    SV2D_XCL126_ASMKT)

      SV2D_XCL126_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL126_COD
C
C Description:
C     La fonction SV2D_XCL126_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_COD(IL, BELF)

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl126.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl126.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 2,-1,
     &                            1,-1,
     &                            1,-1,
     &                            1, 1/), (/2, 4/))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Contrôles
D     IC   = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL126_TYP)
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, KCLDIM)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         INDEB = GDTA%KCLLIM(3, IL)
         INFIN = GDTA%KCLLIM(4, IL)
         DO I = INDEB, INFIN
            IN = GDTA%KCLNOD(I)
            IF (IN .GT. 0)
     &         EDTA%KDIMP(3,IN) = IBSET(EDTA%KDIMP(3,IN),
     &                                  SV2D_XCL_DEBIT)
         ENDDO
      ENDIF

      SV2D_XCL126_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL126_CLC
C
C Description:
C     La fonction SV2D_XCL126_CLC calcule les valeurs de condition
C     limite en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau fixe spécifié.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des DDL imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_CLC(IL, BELF)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl126.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xcl126.fc'

      INTEGER IERR, I_ERROR
      INTEGER IC, IV
      INTEGER I, IN, IE, IES
      INTEGER IEDEB, IEFIN
      INTEGER INDEB, INFIN
      INTEGER NO1, NO3
      REAL*8  P1, P3, P
      REAL*8  QTOT, QSPEC
      REAL*8  SS
      REAL*8  RS(1), RG(1)
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRN_T),    POINTER :: IPRN
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL126_TYP)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 1)
      IV = EDTA%KCLCND(3,IC)
      QTOT = EDTA%VCLCNV(IV)

C---     Limites d'itération
      INDEB = GDTA%KCLLIM(3,IL)
      INFIN = GDTA%KCLLIM(4,IL)
      IEDEB = GDTA%KCLLIM(5,IL)
      IEFIN = GDTA%KCLLIM(6,IL)

C---     Intègre la surface mouillée
      SS = 0.0D0
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Valeurs nodales
         P1 = MAX(EDTA%VDLG(3,NO1)-EDTA%VPRNO(IPRN%Z,NO1), ZERO)    ! Profondeur
         P3 = MAX(EDTA%VDLG(3,NO3)-EDTA%VPRNO(IPRN%Z,NO3), ZERO)    ! Profondeur

C---        Intègre
         SS = SS + (P1+P3)*GDTA%VDJS(3,IES)
199      CONTINUE
      ENDDO

C---    Sync et contrôle
      RS(1) = SS
      CALL MPI_ALLREDUCE(RS, RG, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      SS = RG(1)
      IF (SS .LT. PETIT) GOTO 9900

C---     Débit spécifique
      QSPEC = QTOT / SS

C---     Impose les valeurs
      DO I = INDEB, INFIN
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0) THEN
            P = EDTA%VDLG(3,IN)-EDTA%VPRNO(IPRN%Z,IN)
            IF (P .GT. ZERO) THEN
               EDTA%VDIMP(3,IN) = QSPEC
            ELSE
               EDTA%VDIMP(3,IN) = ZERO
            ENDIF
         ENDIF
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_XCL126_TYP
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL126_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL126_ASMRE
C
C Description:
C     La fonction SV2D_XCL126_ASMRE assemble le résidu élémentaire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les profondeurs négatives sont traitées via VDIMP (cf CLC)
C************************************************************************
      FUNCTION SV2D_XCL126_ASMRE(VFE,
     &                           VDJE,
     &                           VPRN,
     &                           VDIMP,
     &                           VDLE)

      USE SV2D_XBS_M
      IMPLICIT NONE


      REAL*8  VFE  (*)
      REAL*8  VDJE (*)
      REAL*8  VPRN (*)
      REAL*8  VDIMP(*)
      REAL*8  VDLE (*)

      INCLUDE 'sv2d_xcl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xcl126.fc'

      REAL*8  H1, H2, Q1, Q2, C1, C2
      REAL*8  C

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
C-----------------------------------------------------------------------

C---     Coefficient
      C = -UN_3*VDJE(3)

C---        Valeurs nodales
      H1 = VDLE(NO1) - VPRN(NO1)   ! Profondeur
      H2 = VDLE(NO2) - VPRN(NO2)
      Q1 = VDIMP(NO1)              ! Débit spec.
      Q2 = VDIMP(NO2)
      C1 = C*Q1*H1
      C2 = C*Q2*H2

C---     Assemblage du vecteur global
      VFE(1) = (C1+C1 + C2)
      VFE(2) = (C2+C2 + C1)

      SV2D_XCL126_ASMRE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL126_ASMF
C
C Description:
C     La fonction SV2D_XCL126_ASMF assemble {F}. En fait on assemble tout
C     le résidu.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_ASMF(IL, BELF, VFG)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl126.fc'

      INTEGER, PARAMETER :: NNEL_LCL =  2
      INTEGER, PARAMETER :: NDLE_LCL =  2

      REAL*8, POINTER :: VPRN (:)

      REAL*8  VDLE (NDLE_LCL)
      REAL*8  VCLE (NDLE_LCL)
      REAL*8, TARGET :: VPRN_LCL (NNEL_LCL)
      REAL*8  VFE  (NDLE_LCL), VFEP(NDLE_LCL)
      INTEGER KLOCE(NDLE_LCL)

      INTEGER IERR
      INTEGER IC
      INTEGER IE, IES
      INTEGER IEDEB, IEFIN
      INTEGER II
      INTEGER KNE(NNEL_LCL)
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRN_T),    POINTER :: IPRN
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA
      IPRN => SELF%IPRN

C---     Reshape the arrays
      VPRN(1:GDTA%NNELV) => VPRN_LCL

C---     Indices
      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL126_TYP)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 1)
      IEDEB = GDTA%KCLLIM(5, IL)
      IEFIN = GDTA%KCLLIM(6, IL)

C---     Assemble
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         KNE(:) = (/ GDTA%KNGS(1,IES), GDTA%KNGS(3,IES) /)

C---        Transfert les données
         VDLE(:) = EDTA%VDLG (3,KNE(:))
         VCLE(:) = EDTA%VDIMP(3,KNE(:))
         VPRN(:) = EDTA%VPRNO(IPRN%Z,KNE(:))

C---        {F} - [K]{U}
         IERR = SV2D_XCL126_ASMRE(VFE,
     &                            GDTA%VDJS(:,IES),
     &                            VPRN,
     &                            VCLE,
     &                            VDLE)

C---        Assemblage du vecteur global
         KLOCE(:) = EDTA%KLOCN(3,KNE(:))
         IERR = SP_ELEM_ASMFE(2, KLOCE, VFE, VFG)
      ENDDO

      SV2D_XCL126_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL126_ASMK
C
C Description:
C     La fonction SV2D_XCL126_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_ASMK(IL,
     &                          BELF,
     &                          HMTX,
     &                          F_ASM)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xcl126.fc'

      INTEGER, PARAMETER :: NNEL_LCL =  2
      INTEGER, PARAMETER :: NDLE_LCL =  2

      REAL*8  VKE(NDLE_LCL, NDLE_LCL)
      REAL*8  Q1, Q3, C1, C3
      REAL*8  C
      INTEGER IERR
      INTEGER IC
      INTEGER IE, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      INTEGER KLOCE(NNEL_LCL)
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA

      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL126_TYP)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 1)
      IEDEB = GDTA%KCLLIM(5,IL)
      IEFIN = GDTA%KCLLIM(6,IL)

C---     Assemble
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)

C---        Coefficient
         C = UN_3*GDTA%VDJS(3,IES)

C---        Valeurs nodales
         Q1 = EDTA%VDIMP(3,NO1)          ! Débit spec.
         Q3 = EDTA%VDIMP(3,NO3)
         C1 = C*Q1
         C3 = C*Q3

C---        Matrice élémentaire
         VKE(1,1) = C1+C1
         VKE(2,1) = C1
         VKE(1,2) = C3
         VKE(2,2) = C3+C3

C---        Table KLOCE
         KLOCE(1) = EDTA%KLOCN(3,NO1)
         KLOCE(2) = EDTA%KLOCN(3,NO3)

C---       Assemblage de la matrice
         IERR = F_ASM(HMTX, NDLE_LCL, KLOCE, VKE)
      ENDDO

      IF (ERR_BAD()) THEN
         IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE'))
     &      CALL ERR_RESET()
      ENDIF

      SV2D_XCL126_ASMK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL126_ASMKT
C
C Description:
C     La fonction SV2D_XCL126_ASMKT calcule le matrice tangente
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_ASMKT(IL, BELF, HMTX, F_ASM)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      !INCLUDE 'sv2d_xcl126.fi'
      INTEGER SV2D_XCL126_ASMKT
      INTERFACE
         FUNCTION SV2D_XCL126_COD(IL,SELF)
            USE SV2D_XBS_M
            INTEGER, INTENT(IN) :: IL
            TYPE (SV2D_XBS_T) ,TARGET, INTENT(IN) :: SELF
            INTEGER :: SV2D_XCL126_COD
         END FUNCTION SV2D_XCL126_COD
      END INTERFACE
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_fnct.fi'
      INCLUDE 'sv2d_xcl126.fc'

      INTEGER, PARAMETER :: NNEL_LCL =  2
      INTEGER, PARAMETER :: NDLE_LCL =  2

      REAL*8, POINTER :: VPRN(:)

      REAL*8  VDLE (NDLE_LCL)
      REAL*8  VCLE (NDLE_LCL)
      REAL*8, TARGET :: VPRN_LCL (NNEL_LCL)
      REAL*8  VFE  (NDLE_LCL), VFEP(NDLE_LCL)
      REAL*8  VKE  (NDLE_LCL, NDLE_LCL)
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      INTEGER IERR
      INTEGER IC, IEDEB, IEFIN
      INTEGER IE, IES
      INTEGER IN, II, ID
      INTEGER HVFT, HPRNE
      INTEGER NO
      INTEGER KNE  (NNEL_LCL)
      INTEGER KLOCE(NNEL_LCL)
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRN_T),    POINTER :: IPRN
      TYPE (SV2D_XPRG_T),    POINTER :: XPRG
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA
      IPRN => SELF%IPRN
      XPRG => SELF%XPRG

C---     Reshape the arrays
      VPRN(1:GDTA%NNELV) => VPRN_LCL

C---     Fonctions de calcul
      HVFT = 0    ! VTABLE par défaut
      IERR = OB_VTBL_REQMTH(SELF%HVTBL, SV2D_VT_CLCPRNES, HPRNE)

C---     Limites d'itération
      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL126_TYP)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 1)
      IEDEB = GDTA%KCLLIM(5,IL)
      IEFIN = GDTA%KCLLIM(6,IL)

C---     Boucle sur la limite
C        ====================
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         KNE(:) = (/ GDTA%KNGS(1,IES), GDTA%KNGS(3,IES) /)

C---        Table KLOCE de l'élément
         KLOCE(:) = EDTA%KLOCN(3,KNE(:))

C---        Transfert des données
         VDLE(:) = EDTA%VDLG (3,KNE(:))
         VCLE(:) = EDTA%VDIMP(3,KNE(:))
         VPRN(:) = EDTA%VPRNO(IPRN%Z,KNE(:))

C---        {R(u)}
         IERR = SV2D_XCL126_ASMRE(VFE,
     &                            GDTA%VDJS(:,IES),
     &                            VPRN,
     &                            VCLE,
     &                            VDLE)

C---        Initialise la matrice des dérivées
         CALL DINIT(NDLE_LCL*NDLE_LCL, ZERO, VKE, 1)

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         DO ID=1,NDLE_LCL
            IF (KLOCE(ID) .EQ. 0) GOTO 299
            NO = KNE(ID)

C---           Perturbe le DDL
            VDL_ORI = VDLE(ID)
            VDL_DEL = XPRG%PNUMR_DELPRT * VDLE(ID)
     &              + SIGN(XPRG%PNUMR_DELMIN, VDLE(ID))
            VDL_INV = XPRG%PNUMR_OMEGAKT / VDL_DEL
            VDLE(ID) = VDLE(ID) + VDL_DEL
            EDTA%VDLG(3,NO) = VDLE(ID)

C---           Calcule les propriétés nodales perturbées
            IF (XPRG%PNUMR_PRTPRNO) THEN
               !IERR = SV2D_XCL126_CLC(IL, BELF)   ! CL perturbé dans EDTA%VDIMP
               VCLE(:) = EDTA%VDIMP(3,KNE(:))
            ENDIF

C---           {R(u+du_id)}
            IERR = SV2D_XCL126_ASMRE(VFEP,
     &                               GDTA%VDJS(:,IES),
     &                               VPRN,
     &                               VCLE,
     &                               VDLE)

C---           Restaure la valeur originale
            VDLE(ID) = VDL_ORI
            EDTA%VDLG(3,NO) = VDLE(ID)

C---           - ({R(u+du_id)} - {R(u)})/du
            DO II=1, NDLE_LCL
               VKE(II,ID) = (VFE(II) - VFEP(II)) * VDL_INV
            ENDDO

299         CONTINUE
         ENDDO

C---       Assemble la matrice
         IERR = F_ASM(HMTX, NDLE_LCL, KLOCE, VKE)
         IF (ERR_BAD()) CALL ERR_RESET()

      ENDDO

C---     Restaure les propriétés nodales perturbées
      IF (XPRG%PNUMR_PRTPRNO) THEN
         !IERR = SV2D_XCL126_CLC(IL, BELF)
      ENDIF

      SV2D_XCL126_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL126_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL126_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>126</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge Q is distributed on the wetted part of the boundary, taking into account the current water level h.
C  The distribution is based on a constant velocity on the whole section.
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 126</li>
C     <li>Values: Q</li>
C     <li>Units: m^3/s</li>
C     <li>Example:  126   -500</li>
C  </ul>
C</comment>

      SV2D_XCL126_REQHLP = 'bc_type_126'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL126_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL126_HLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl126.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl126.hlp')

      SV2D_XCL126_HLP = ERR_TYP()
      RETURN
      END
