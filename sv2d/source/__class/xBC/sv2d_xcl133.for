C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Impose les valeurs de h d'une autre limite. Les valeurs sont prises
C     dans l'ordre, noeud par noeud.
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL133_000
C     INTEGER SV2D_XCL133_999
C     INTEGER SV2D_XCL133_COD
C     INTEGER SV2D_XCL133_CLC
C     INTEGER SV2D_XCL133_ASMF
C     INTEGER SV2D_XCL133_ASMKU
C     INTEGER SV2D_XCL133_ASMK
C     INTEGER SV2D_XCL133_ASMKT
C     INTEGER SV2D_XCL133_HLP
C   Private:
C     INTEGER SV2D_XCL133_INIVTBL
C     CHARACTER*(16) SV2D_XCL133_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_XCL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_XCL133_DATA_000

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl133.fc'

      DATA SV2D_XCL133_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL133_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL133_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl133.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL133_INIVTBL()

      SV2D_XCL133_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL133_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL133_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL133_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_INIVTBL()

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl133.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_XCL133_HLP
      EXTERNAL SV2D_XCL133_COD
      EXTERNAL SV2D_XCL133_CLC
      EXTERNAL SV2D_XCL133_ASMF
      EXTERNAL SV2D_XCL133_ASMKU
      EXTERNAL SV2D_XCL133_ASMK
      EXTERNAL SV2D_XCL133_ASMKT
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL133_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_XCL133_TPN)
!!!      IRET = C_ST_CRC32(SV2D_XCL133_TPN(1:LTPN), SV2D_XCL133_TPC)
!!!      IERR = SV2D_XCL_INIVTBL2(HVFT, SV2D_XCL133_TPN)
      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL133_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL133_TYP,
     &                     SV2D_XCL133_HLP)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_COD,  SV2D_XCL133_TYP,
     &                    SV2D_XCL133_COD)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_CLC,  SV2D_XCL133_TYP,
     &                    SV2D_XCL133_CLC)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMF, SV2D_XCL133_TYP,
     &                    SV2D_XCL133_ASMF)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMKU,SV2D_XCL133_TYP,
     &                    SV2D_XCL133_ASMKU)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMK, SV2D_XCL133_TYP,
     &                    SV2D_XCL133_ASMK)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMKT,SV2D_XCL133_TYP,
     &                    SV2D_XCL133_ASMKT)

      SV2D_XCL133_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL133_COD
C
C Description:
C     La fonction SV2D_XCL133_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose les valeurs de h d'une
C     autre limite. Les valeurs sont prises dans l'ordre, noeud par noeud.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des ddl imposés
C
C Notes:
C     Valeurs lues: il_src, a0, a1
C************************************************************************
      FUNCTION SV2D_XCL133_COD(IL, BELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL133_COD
CDEC$ ENDIF

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl133.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_DST, ID_DST
      INTEGER IL_SRC, ID_SRC
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL133_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, 3)

C---     Les indices
      IC = GDTA%KCLLIM(2, IL)
      IV = EDTA%KCLCND(3, IC)

C---     Assigne les codes
      IL_DST = IL
      ID_DST = 3        ! h
      IL_SRC = NINT( EDTA%VCLCNV(IV) )
      ID_SRC = 3        ! h
      IERR = SV2D_XCL_COD2L(IL_DST, ID_DST, EA_TPCL_WEAKDIR,
     &                      IL_SRC, ID_SRC, SV2D_XCL_COPIE,
     &                      GDTA%KCLLIM,
     &                      GDTA%KCLNOD,
     &                      GDTA%KCLELE,
     &                      EDTA%KDIMP)
      IF (ERR_GOOD()) EDTA%VCLCNV(IV) = IL_SRC

      SV2D_XCL133_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL133_CLC
C
C Description:
C     La fonction SV2D_XCL133_CLC assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose les valeurs de h d'une
C     autre limite. Les valeurs sont prises dans l'ordre, noeud par noeud.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_CLC(IL, BELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL133_CLC
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE


      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN)    :: GDTA
      TYPE (LM_EDTA_T), INTENT(INOUT) :: EDTA

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl133.fc'

      REAL*8  A0, A1
      INTEGER I, IC, IV
      INTEGER IL_DST, IL_SRC
      INTEGER INDEB_DST, INFIN_DST
      INTEGER INDEB_SRC, INFIN_SRC
      INTEGER IN_DST, IN_SRC
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL133_TYP)
C-----------------------------------------------------------------------

      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 3)
      IV = EDTA%KCLCND(3,IC)
      IL_SRC = NINT( EDTA%VCLCNV(IV) )
      A0 = EDTA%VCLCNV(IV+1)
      A1 = EDTA%VCLCNV(IV+2)

      IL_DST = IL
      INDEB_DST = GDTA%KCLLIM(3, IL_DST)
      INFIN_DST = GDTA%KCLLIM(4, IL_DST)
      INDEB_SRC = GDTA%KCLLIM(3, IL_SRC)
D     INFIN_SRC = GDTA%KCLLIM(4, IL_SRC)
D     CALL ERR_ASR((INFIN_DST-INDEB_DST) .EQ. (INFIN_SRC-INDEB_SRC))

      DO I = 0, (INFIN_DST-INDEB_DST)
         IN_DST = GDTA%KCLNOD(INDEB_DST+I)
         IN_SRC = GDTA%KCLNOD(INDEB_SRC+I)

         EDTA%VDIMP(3, IN_DST) = A0 + A1*EDTA%VDLG(3, IN_SRC)
      ENDDO

      SV2D_XCL133_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL133_ASMF
C
C Description:
C     La fonction SV2D_XCL133_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose les valeurs de h d'une
C     autre limite. Les valeurs sont prises dans l'ordre, noeud par noeud.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des ddl imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_ASMF(IL, GDTA, EDTA, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL133_ASMF
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl133.fc'

      INTEGER IERR
      INTEGER I, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8  C, V1, V3
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL133_TYP)
C-----------------------------------------------------------------------

      IEDEB = GDTA%KCLLIM(5,IL)
      IEFIN = GDTA%KCLLIM(6,IL)

C---     Assemble
      DO I = IEDEB, IEFIN
         IES = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)

C---        Valeurs nodales
         V1 = EDTA%VDIMP(3,NO1)
         V3 = EDTA%VDIMP(3,NO3)

C---        Assemblage du vecteur global lumped
         C = GDTA%VDJS(3,IES)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO1), C*V1, VFG)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO3), C*V3, VFG)

      ENDDO

      SV2D_XCL133_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL133_ASMKU
C
C Description:
C     La fonction SV2D_XCL133_ASMKU est vide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_ASMKU(IL, GDTA, EDTA, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL133_ASMKU
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl133.fc'

      INTEGER IERR
      INTEGER I, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8  C, V1, V3
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL133_TYP)
C-----------------------------------------------------------------------

      IEDEB = GDTA%KCLLIM(5,IL)
      IEFIN = GDTA%KCLLIM(6,IL)

C---     Assemble
      DO I = IEDEB, IEFIN
         IES = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)

C---        Valeurs nodales
         V1 = EDTA%VDLG(3,NO1)
         V3 = EDTA%VDLG(3,NO3)

C---        Assemblage du vecteur global lumped
         C = GDTA%VDJS(3,IES)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO1), C*V1, VFG)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO3), C*V3, VFG)

      ENDDO

      SV2D_XCL133_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2DASMK
C
C Description:
C     La fonction SV2DASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_ASMK(IL,
     &                          GDTA,
     &                          EDTA,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL133_ASMK
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER NDLE_LCL
      PARAMETER (NDLE_LCL = 2)

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl133.fc'

      INTEGER IERR
      INTEGER IEDEB, IEFIN
      INTEGER IE, IES, NO1, NO3
      REAL*8  ML
C----------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL133_TYP)
D     CALL ERR_PRE(EDTA%NDLEV .GE. NDLE_LCL)
C-----------------------------------------------------------------

C---     Initialise la matrice élémentaire
      CALL DINIT(NDLE_LCL*EDTA%NDLEV, ZERO, VKE, 1)

C---     Indices des éléments
      IEDEB = GDTA%KCLLIM(5,IL)
      IEFIN = GDTA%KCLLIM(6,IL)

C---     Boucle sur les éléments de la limite
C        ====================================
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)

C---        Matrice masse lumpded
         ML = GDTA%VDJS(3,IES)
         VKE(1,1) = ML
         VKE(2,2) = ML

C---        Table kloce
         KLOCE(1) = EDTA%KLOCN(3,NO1)
         KLOCE(2) = EDTA%KLOCN(3,NO3)

C---       Assemblage de la matrice
         IERR = F_ASM(HMTX, NDLE_LCL, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      SV2D_XCL133_ASMK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL133_ASMKT
C
C Description:
C     La fonction SV2D_XCL133_ASMKT calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_ASMKT(IL,
     &                           GDTA,
     &                           EDTA,
     &                           HMTX,
     &                           F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL133_ASMKT
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl133.fc'

      INTEGER   IERR
C----------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL133_TYP)
C-----------------------------------------------------------------

      IERR = SV2D_XCL133_ASMK(IL, GDTA, EDTA, HMTX, F_ASM)

      SV2D_XCL133_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL133_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL133_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>133</b>: <br>
C  Weak Dirichlet condition on the water level.
C  The condition imposes on the limit the water level of another limit. The values are taken in order, node by node.
C  <p>
C  Parameters (a0, a1) can be used to transform the source water level to the final form
C  <pre>h_dst = a0 + a1*h_src</pre>
C  <ul>
C     <li>Kind: Weak Dirichlet</li>
C     <li>Code: 133</li>
C     <li>Values: name, a0, a1</li>
C     <li>Units: [-], m, [-]</li>
C     <li>Example:  125 $__Amont_deversoir__$ 0.0 1.0</li>
C  </ul>
C</comment>

      SV2D_XCL133_REQHLP = 'bc_type_133'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL133_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL133_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL133_HLP
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl133.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl133.hlp')

      SV2D_XCL133_HLP = ERR_TYP()
      RETURN
      END
