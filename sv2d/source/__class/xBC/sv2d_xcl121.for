C************************************************************************
C --- Copyright (c) INRS 2010-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C     Sollicitation de débit via la forme faible de la continuité.
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL121_M

      USE SV2D_XCL000_M
      IMPLICIT NONE

      TYPE, EXTENDS(SV2D_XCL000_T) :: SV2D_XCL121_T
      CONTAINS
         PROCEDURE, PUBLIC :: COD   => SV2D_XCL121_COD
         PROCEDURE, PUBLIC :: ASMF  => SV2D_XCL121_ASMF
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD  => SV2D_XCL121_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP  => SV2D_XCL121_HLP
      END TYPE SV2D_XCL121_T

      PUBLIC :: SV2D_XCL121_CTR

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL121_CTR
C
C Description:
C     Le constructeur <code>SV2D_XCL121_CTR</code> construit une C.L.
C     de type <code>121</code>.
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL121_CTR() RESULT(SELF)

      TYPE(SV2D_XCL121_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL121_CTR

C************************************************************************
C Sommaire:  SV2D_XCL121_COD
C
C Description:
C     La méthode <code>SV2D_XCL121_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP       Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL121_COD(SELF, KDIMP)

      CLASS(SV2D_XCL121_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'

      INTEGER IERR
      INTEGER I, IN, INDEB, INFIN
      INTEGER NNOD, NVAL

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 1,-1,               ! Nb de noeuds  (min, max)
     &                           -1,-1,               ! Nb d'éléments (min, max)
     &                           -1,-1,               ! Nb de limites (min, max)
     &                            1,-1/), (/2, 4/))   ! Nb de valeurs (min, max)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SELF%CHK(KCLDIM)
      IF (ERR_GOOD()) THEN
         NNOD = SIZE(SELF%KNOD, 1)
         NVAL = SIZE(SELF%VCND, 1)
         IF (NVAL .NE. 1 .AND. NVAL .NE. NNOD) GOTO 9900
      ENDIF

C---     Assigne les codes
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      KDIMP(3,IN) = IBSET(KDIMP(3,IN), SV2D_XCL_SLC_DEBIT)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_VALEURS_VALIDES', ': 1 or ', NNOD
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_NBR_NOEUDS', '= ', NNOD
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_NBR_VAL', '= ', NVAL
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL121_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL121_COD

C************************************************************************
C Sommaire:  SV2D_XCL121_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL121_ASMF(SELF, ELEM, VFG)

      USE LM_ELEM_M
      
      CLASS(SV2D_XCL121_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(IN) :: VFG(:)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'

      INTEGER IERR
      INTEGER I, IC, IN, IV
      INTEGER INDEB, INFIN, IVDEB, IVFIN
D     INTEGER NNOD, NVAL
      INTEGER, POINTER :: KLOCN(:, :)
C-----------------------------------------------------------------------

      ! ---  Récupère les attributs
      KLOCN  => ELEM%EDTA%KLOCN

      ! ---  Les indices
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      IVDEB = LBOUND(SELF%VCND, 1) 
      IVFIN = UBOUND(SELF%VCND, 1) 
D     NNOD = INFIN - INDEB + 1
D     NVAL = IVFIN - IVDEB + 1
D     CALL ERR_ASR(NVAL .EQ. 1 .OR. NVAL .EQ. NNOD)

      ! ---  Assemble les sollicitations
      IV = IVDEB - 1
      DO I = INDEB, INFIN
         IF (IV .LT. IVFIN) IV = IV + 1
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      IERR = SP_ELEM_ASMFE(1, KLOCN(3:,IN), SELF%VCND(IV:), VFG)
      ENDDO

      SV2D_XCL121_ASMF = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL121_ASMF

C************************************************************************
C Sommaire: SV2D_XCL121_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL121_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL121_HLP()

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl121.hlp')

      SV2D_XCL121_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL121_HLP

C************************************************************************
C Sommaire:  SV2D_XCL121_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL121_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL121_CMD()

C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>121</b>: <br>
C  It makes use of the natural boundary condition arising
C  from the weak form of the continuity equation.
C  <p>
C  The discharge sollicitation is imposed node by node.
C  Conceptually the condition is described by:
C  <pre>    (q0, q1, ..., qn)</pre>
C  where qi is the specific discharge at node i.
C  As a special case, if only 1 value is specified, it will be used
C  for all nodes.
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 121</li>
C     <li>Values: q0, q1, q2 ... </li>
C     <li>Units: m^2/s</li>
C     <li>Example:  121   1.0 1.0 1.0</li>
C  </ul>
C</comment>

      SV2D_XCL121_CMD = 'bc_type_121'
      RETURN
      END FUNCTION SV2D_XCL121_CMD
      
      END MODULE SV2D_XCL121_M

      