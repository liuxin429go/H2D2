C************************************************************************
C --- Copyright (c) INRS 2010-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C     NoOp condition, allowing for example to define a source limit.
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL100_M

      USE SV2D_XCL000_M
      IMPLICIT NONE

      TYPE, EXTENDS(SV2D_XCL000_T) :: SV2D_XCL100_T
      CONTAINS
         PROCEDURE, PUBLIC :: COD   => SV2D_XCL100_COD
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD  => SV2D_XCL100_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP  => SV2D_XCL100_HLP
      END TYPE SV2D_XCL100_T

      PUBLIC :: SV2D_XCL100_CTR

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL100_CTR
C
C Description:
C     Le constructeur <code>SV2D_XCL100_CTR</code> construit une C.L.
C     de type <code>100</code>.
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL100_CTR() RESULT(SELF)

      TYPE(SV2D_XCL100_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL100_CTR

C************************************************************************
C Sommaire:  SV2D_XCL100_COD
C
C Description:
C     La méthode <code>SV2D_XCL100_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL100_COD(SELF, KDIMP)

      CLASS(SV2D_XCL100_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER I, IN, INDEB, INFIN

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 1,-1,               ! Nb de noeuds  (min, max)
     &                           -1,-1,               ! Nb d'éléments (min, max)
     &                           -1,-1,               ! Nb de limites (min, max)
     &                            0, 0/), (/2, 4/))   ! Nb de valeurs (min, max)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SELF%CHK(KCLDIM)

C---     Assigne les codes
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      KDIMP(:,IN) = IBSET(KDIMP(:,IN), EA_TPCL_NOOP)
      ENDDO

      SV2D_XCL100_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL100_COD

C************************************************************************
C Sommaire: SV2D_XCL100_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL100_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL100_HLP()

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xc100.hlp')

      SV2D_XCL100_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL100_HLP

C************************************************************************
C Sommaire:  SV2D_XCL100_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL100_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL100_CMD()

C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>100</b>: <br>
C  This is a noOp condition, allowing for example to define a source limit.
C     <ul>
C     <li>Kind: noOp</li>
C     <li>Code: 100</li>
C     <li>Values: </li>
C     <li>Units: </li>
C     <li>Example:  100</li>
C     </ul>
C</comment>

      SV2D_XCL100_CMD = 'bc_type_100'
      RETURN
      END FUNCTION SV2D_XCL100_CMD
      
      END MODULE SV2D_XCL100_M

      