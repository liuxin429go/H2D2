C************************************************************************
C --- Copyright (c) INRS 2010-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C     Condition astronomique (Dirichlet) sur le niveau d'eau h.
C     Le niveau est interpolé entre les noeuds extrêmes.
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL204_M

      USE SV2D_XCL000_M
      IMPLICIT NONE

      TYPE, EXTENDS(SV2D_XCL000_T) :: SV2D_XCL204_T
      CONTAINS
         PROCEDURE, PUBLIC :: COD   => SV2D_XCL204_COD
         PROCEDURE, PUBLIC :: PRC   => SV2D_XCL204_PRC
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD  => SV2D_XCL204_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP  => SV2D_XCL204_HLP
      END TYPE SV2D_XCL204_T

      PUBLIC :: SV2D_XCL204_CTR

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL204_CTR
C
C Description:
C     Le constructeur <code>SV2D_XCL204_CTR</code> construit une C.L.
C     de type <code>204</code>.
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL204_CTR() RESULT(SELF)

      TYPE(SV2D_XCL204_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL204_CTR

C************************************************************************
C Sommaire:  SV2D_XCL204_COD
C
C Description:
C     La méthode <code>SV2D_XCL204_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP       Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL204_COD(SELF, KDIMP)

      CLASS(SV2D_XCL204_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INCLUDE 'eacdcl.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sptide.fi'

      INTEGER IERR
      INTEGER I, IN, INDEB, INFIN
      INTEGER IV1, IVDEB1, IVFIN1
      INTEGER IV2, IVDEB2, IVFIN2
      INTEGER NVAL
      REAL*8  C1, C2

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 1,-1,               ! Nb de noeuds  (min, max)
     &                           -1,-1,               ! Nb d'éléments (min, max)
     &                            1,-1,               ! Nb de limites (min, max)
     &                            8,-1/), (/2, 4/))   ! Nb de valeurs (min, max)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SELF%CHK(KCLDIM)
      NVAL = SIZE(SELF%VCND, 1)
      IF (SELF%IK .NE. EG_TPLMT_1SGMT) GOTO 9901
      IF (MOD(NVAL, 2)   .NE. 0) GOTO 9902   ! nval = 2*(1 + n*3)
      IF (MOD(NVAL/2, 3) .NE. 1) GOTO 9902   ! n multiple de 3 + 1
      IF ((NVAL/2-1)/3   .LT. 1) GOTO 9902   ! n >= 1

C---     Contrôles les composantes
      IVDEB1 = LBOUND(SELF%VCND, 1)
      IVFIN1 = IVDEB1 + SIZE(SELF%VCND, 1)/2 - 1
      IVDEB2 = IVFIN1 + 1
      IVFIN2 = UBOUND(SELF%VCND, 1)
      IVDEB1 = IVDEB1 + 1        ! Saute T0
      IVDEB2 = IVDEB2 + 1        ! Saute T0
      IV2 = IVDEB2-3
      DO IV1=IVDEB1,IVFIN1,3
         IV2 = IV2 + 3
         C1 = SELF%VCND(IV1)     ! Composante
         C2 = SELF%VCND(IV2)     ! Composante
         IF (NINT(C1) .NE. NINT(C2)) GOTO 9903
         IF (.NOT. SP_TIDE_CMPEXIST(NINT(C1))) GOTO 9904
      ENDDO

C---     Assigne les codes
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_DIRICHLET)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_LIMITE_UN_SEGMENT_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_OBTIENT', ': ', NVAL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_ATTEND', ': ', '2*(1 + N*3)'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A)') 'ERR_CND_COMPOSANTE_NON_CONCORDANTES', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A)') 'ERR_UNKNOWN_TIDE_COMPONENT', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL204_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL204_COD

C************************************************************************
C Sommaire:  SV2D_XCL204_PRC
C
C Description:
C     La méthode <code>SV2D_XCL204_PRC</code> fait l'étape de pré-calcul,
C     résultats qui ne dépendent pas des DDL.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     VDIMP       Valeurs des DDL imposés
C
C Notes:
C     Conceptuellement
C     (
C        T0
C        (                       ! Noeud début
C           (c,a,p),
C           (c,a,p), ...
C        ),
C        T0
C        (                       ! Noeud fin
C           (c,a,p),
C           (c,a,p), ...
C        )
C     )
C     Les composantes doivent être dans le même ordre
C************************************************************************
      INTEGER FUNCTION SV2D_XCL204_PRC(SELF, ELEM, VDIMP)

      CLASS(SV2D_XCL204_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(INOUT) :: VDIMP(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'sptide.fi'

      INTEGER I, IN, INDEB, INFIN
      INTEGER IV1, IVDEB1, IVFIN1
      INTEGER IV2, IVDEB2, IVFIN2
      REAL*8  H, C, A, D
      REAL*8  C1, A1, D1
      REAL*8  C2, A2, D2
      REAL*8  T, T0
      REAL*8  VN1, VN2
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SIZE(VDIMP, 1) .LE. SIZE(SELF%VCND, 1))
C-----------------------------------------------------------------------

      IVDEB1 = LBOUND(SELF%VCND, 1)
      IVFIN1 = IVDEB1 + SIZE(SELF%VCND, 1)/2 - 1
      IVDEB2 = IVFIN1 + 1
      IVFIN2 = UBOUND(SELF%VCND, 1)

      T0 = SELF%VCND(IVDEB1)   ! Temps de ref.
      T  = ELEM%EDTA%TEMPS     ! Temps de la simu.

      IVDEB1 = IVDEB1 + 1      ! Saute T0
      IVDEB2 = IVDEB2 + 1      ! Saute T0

      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I=INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .LE. 0) GOTO 199

         VN1 = 1.0D0 - SELF%VDST(I)
         VN2 = SELF%VDST(I)

         H = 0.0D0
         IV2 = IVDEB2-3
         DO IV1=IVDEB1,IVFIN1,3
            IV2 = IV2 + 3
            C1 = SELF%VCND(IV1+0)     ! composante
            A1 = SELF%VCND(IV1+1)     ! amplitude
            D1 = SELF%VCND(IV1+2)     ! phi
            C2 = SELF%VCND(IV2+0)     ! composante
            A2 = SELF%VCND(IV2+1)     ! amplitude
            D2 = SELF%VCND(IV2+2)     ! phi
D           CALL ERR_ASR(C1 .EQ. C2)
            C = NINT(C1)
            A = VN1*A1 + VN2*A2
            D = VN1*D1 + VN2*D2
            H = H + SP_TIDE_TTDE_HD(INT(C), A, D, T, T0, .TRUE.)
         ENDDO
D        CALL ERR_ASR(IV2 .EQ. IVFIN2)
         VDIMP(3,IN) = H

199      CONTINUE
      ENDDO

      SV2D_XCL204_PRC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL204_PRC

C************************************************************************
C Sommaire: SV2D_XCL204_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL204_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL204_HLP()

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl204.hlp')

      SV2D_XCL204_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL204_HLP

C************************************************************************
C Sommaire:  SV2D_XCL204_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL204_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL204_CMD()

C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>204</b>: <br>
C  Astronomical tide Dirichlet condition on the water level.
C  The condition is interpolated between boundary start and end node.
C  <p>
C  Conceptually the condition is described by:
C  <pre>
C         (
C            (                     ! Start node
C               T0
C               (c,a,p),
C               (c,a,p), ...
C            ),
C            (                     ! End node
C               T0
C               (c,a,p),
C               (c,a,p), ...
C            )
C         )</pre>
C  where T0 is the reference time,
C  c the code name of the tide constituent,
C  a the amplitude and
C  p the phase shift in degrees.
C  The water level H is the sum of all constituent contributions.
C  Amplitude and phase shift are interpolated between
C  boundary start and end node.
C  <p>
C  Even if T0 is repeated, it is expected that T0 (start node) is
C  identical to T0 (end node).
C  <p>
C  <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 204</li>
C     <li>Values: T0, c, a, p, c, a, p, ... </li>
C     <li>Units: [s] ([-] [m] [deg])  [s] ([-] [m] [deg])</li>
C     <li>Example:  204   1234567890.0   m2  2.0  -74.8 1234567890.0   m2  2.5  -74.8</li>
C  </ul>
C  (Status: Experimental).
C</comment>

      SV2D_XCL204_CMD = 'bc_type_204'
      RETURN
      END FUNCTION SV2D_XCL204_CMD

      END MODULE SV2D_XCL204_M
 