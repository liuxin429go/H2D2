C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Frontière ouverte en débit.
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL141_000
C     INTEGER SV2D_XCL141_999
C     INTEGER SV2D_XCL141_COD
C     INTEGER SV2D_XCL141_ASMK
C     INTEGER SV2D_XCL141_ASMKT
C     INTEGER SV2D_XCL141_ASMKU
C     INTEGER SV2D_XCL141_HLP
C   Private:
C     INTEGER SV2D_XCL141_INIVTBL
C     SUBROUTINE SV2D_XCL141_ASMRE_S
C     SUBROUTINE SV2D_XCL141_ASMKE_S
C     CHARACTER*(16) SV2D_XCL141_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_XCL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_XCL141_DATA_000

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl141.fc'

      DATA SV2D_XCL141_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL141_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL141_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL141_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl141.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL141_INIVTBL()

      SV2D_XCL141_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL141_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL141_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl141.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL141_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL141_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL141_INIVTBL()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl141.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl141.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_XCL141_HLP
      EXTERNAL SV2D_XCL141_COD
      EXTERNAL SV2D_XCL141_ASMK
      EXTERNAL SV2D_XCL141_ASMKU
      EXTERNAL SV2D_XCL141_ASMKT
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL141_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_XCL141_TPN)
!!!      IRET = C_ST_CRC32(SV2D_XCL141_TPN(1:LTPN), SV2D_XCL141_TPC)
!!!      IERR = SV2D_XCL_INIVTBL2(HVFT, SV2D_XCL141_TPN)
      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL141_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL141_TYP,
     &                     SV2D_XCL141_HLP)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_COD,  SV2D_XCL141_TYP,
     &                    SV2D_XCL141_COD)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMK, SV2D_XCL141_TYP,
     &                    SV2D_XCL141_ASMK)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMKU,SV2D_XCL141_TYP,
     &                    SV2D_XCL141_ASMKU)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMKT,SV2D_XCL141_TYP,
     &                    SV2D_XCL141_ASMKT)

      SV2D_XCL141_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL141_COD
C
C Description:
C     La fonction SV2D_XCL141_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose une limite ouverte en débit.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL141_COD(IL, BELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL141_COD
CDEC$ ENDIF

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl141.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl141.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL141_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, 0)

C---     Les indices
      IC = GDTA%KCLLIM(2, IL)

C---     Assigne les codes
      INDEB = GDTA%KCLLIM(3, IL)
      INFIN = GDTA%KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0)
     &      EDTA%KDIMP(3,IN) = IBSET(EDTA%KDIMP(3,IN),
     &                               EA_TPCL_OUVERT)
      ENDDO

      SV2D_XCL141_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL141_ASMK
C
C Description:
C     La fonction SV2D_XCL141_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La matrice est une matrice élémentaire pour le T6L
C     La matrice Ke peut être nulle.
C************************************************************************
      FUNCTION SV2D_XCL141_ASMK(IL,
     &                          GDTA,
     &                          EDTA,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL141_ASMK
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmgeom_subt3.fi'
      INCLUDE 'sv2d_subt3.fi'
      INCLUDE 'sv2d_xcl141.fc'

      INTEGER, PARAMETER :: NDLN_LCL = 3
      INTEGER, PARAMETER :: NNEL_LCL = 6

      INTEGER IERR
      INTEGER IEDEB, IEFIN
      INTEGER IE, IES, IEV, ICT
      INTEGER II, IN, ID, IDG
      REAL*8  VDLE (NDLN_LCL, NNEL_LCL)
      REAL*8  VRHS (NDLN_LCL, NNEL_LCL)
C----------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL141_TYP)
D     CALL ERR_PRE(EDTA%NDLN  .EQ. NDLN_LCL)
C-----------------------------------------------------------------

      IEDEB = GDTA%KCLLIM(5,IL)
      IEFIN = GDTA%KCLLIM(6,IL)

C---     Initialise le RHS
      VRHS(:,:) = ZERO

C---     Boucle sur les éléments de la limite
C        ====================================
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Element parent et côté
         IEV = GDTA%KNGS(4,IES)
         ICT = GDTA%KNGS(5,IES)

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = EDTA%KLOCN(:,GDTA%KNGV(:,IEV))

C---        Transfert des DDL
         VDLE(:,:) = EDTA%VDLG (:,GDTA%KNGV(:,IEV))

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO

C---        Boucle de perturbation sur les DDL
C           ==================================
         DO II=1,GDTA%NNELS
            IN = KNEL3L(II, ICT)
            IDG = (IN-1)*EDTA%NDLN

            DO ID=1,EDTA%NDLN
               IDG = IDG + 1
               IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---              Perturbe le DDL
               VRHS(ID,IN) = UN

C---              R(u). i
               CALL SV2D_XCL141_ASMRE_S(VKE(1,IDG),
     &                                 GDTA%VDJV(1,IEV),
     &                                 GDTA%VDJS(1,IES),
     &                                 VDLE,
     &                                 VRHS,
     &                                 ICT)

C---              Restaure la valeur originale
               VRHS(ID,IN) = ZERO

199            CONTINUE
            ENDDO
         ENDDO

C---       Assemblage de la matrice
         IERR = F_ASM(HMTX, EDTA%NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF
      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF
      
      SV2D_XCL141_ASMK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL141_ASMKT
C
C Description:
C     La fonction SV2D_XCL141_ASMKT calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL141_ASMKT(IL,
     &                           GDTA,
     &                           EDTA,
     &                           HMTX,
     &                           F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL141_ASMKT
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl141.fc'

      INTEGER   IERR
C----------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL141_TYP)
C-----------------------------------------------------------------

      IERR = SV2D_XCL141_ASMK(IL, GDTA, EDTA, HMTX, F_ASM)

      SV2D_XCL141_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL141_ASMKU
C
C Description:
C     La fonction SV2D_XCL141_ASMKU calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL141_ASMKU(IL, GDTA, EDTA, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL141_ASMKU
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl141.fc'

      INTEGER, PARAMETER :: NDLN_LCL = 3
      INTEGER, PARAMETER :: NNEL_LCL = 6

      INTEGER   IERR
      INTEGER   IEDEB, IEFIN
      INTEGER   IE, IES, IEV, ICT
      REAL*8    VDLE (NDLN_LCL, NNEL_LCL)
C----------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL141_TYP)
D     CALL ERR_PRE(EDTA%NDLN  .EQ. NDLN_LCL)
D     CALL ERR_PRE(EDTA%NDLEV .EQ. NDLN_LCL*NNEL_LCL)
C-----------------------------------------------------------------

      IEDEB = GDTA%KCLLIM(5,IL)
      IEFIN = GDTA%KCLLIM(6,IL)

C---     Boucle sur les éléments de la limite
C        ====================================
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Élément parent et coté
         IEV = GDTA%KNGS(4,IES)
         ICT = GDTA%KNGS(5,IES)

C---        Transfert des DDL
         VDLE(:,:) = EDTA%VDLG(:,GDTA%KNGV(:,IEV))

C---        Initialise le vecteur élémentaire
         VFE(:,:) = ZERO

C---        [K(u)].{u}
         CALL SV2D_XCL141_ASMRE_S(VFE,
     &                           GDTA%VDJV(1,IEV),
     &                           GDTA%VDJS(1,IES),
     &                           VDLE,
     &                           VDLE,
     &                           ICT)

C---       Assemblage du vecteur global
         KLOCE(:,:) = EDTA%KLOCN(:,GDTA%KNGV(:,IEV))
         IERR = SP_ELEM_ASMFE(EDTA%NDLEV, KLOCE, VFE, VFG)

      ENDDO

      SV2D_XCL141_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL141b_ASMRE_S
C
C Description:
C     La fonction SV2D_XCL141b_ASMRE_S
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_XCL141_ASMRE_S(VRES,
     &                              GDTA%VDJV,
     &                              GDTA%VDJS,
     &                              VDLE,
     &                              VRHS,
     &                              ICOTE)

      USE SV2D_XBS_M
      IMPLICIT NONE


      REAL*8   VRES (EDTA%NDLEV)
      REAL*8   GDTA%VDJV (GDTA%NDJV)
      REAL*8   GDTA%VDJS (GDTA%NDJS)
      REAL*8   VDLE (EDTA%NDLN, GDTA%NNELV)
      REAL*8   VRHS (EDTA%NDLN, GDTA%NNELV)
      INTEGER  ICOTE

      INCLUDE 'sv2d_xcl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_subt3.fi'
      INCLUDE 'sv2d_xcl141.fc'

      INTEGER IKH1, IKH3
      INTEGER NO1, NO2, NO3
      REAL*8  Q1, Q2, Q3
      REAL*8  VNX, VNY, C, M(2)
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNEL3L(1, ICOTE)
      NO2 = KNEL3L(2, ICOTE)
      NO3 = KNEL3L(3, ICOTE)

C---     Indices dans VKE
      IKH1 = KLOCET3(3, 1, ICOTE)
      IKH3 = KLOCET3(6, 2, ICOTE)

C---     Coefficient
      VNX =  GDTA%VDJS(2)       ! VNX =  VTY
      VNY = -GDTA%VDJS(1)       ! VNY = -VTX
      C = UN_12*GDTA%VDJS(3)    ! UN_6 * DJL2

C---     Débits normaux
      Q1 = VRHS(1,NO1)*VNX + VRHS(2,NO1)*VNY
      Q2 = VRHS(1,NO2)*VNX + VRHS(2,NO2)*VNY
      Q3 = VRHS(1,NO3)*VNX + VRHS(2,NO3)*VNY

C---     Assemblage du vecteur global (mixte L2-L3L)
      VRES(IKH1) = VRES(IKH1) + C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
      VRES(IKH3) = VRES(IKH3) + C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL141_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL141_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL141_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>141</b>: <br>
C  Open boundary condition for discharge.
C</comment>

      SV2D_XCL141_REQHLP = 'bc_type_141'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL141_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL141_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL141_HLP
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl141.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl141.hlp')

      SV2D_XCL141_HLP = ERR_TYP()
      RETURN
      END
