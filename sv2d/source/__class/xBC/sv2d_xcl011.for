C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL011_000
C     INTEGER SV2D_XCL011_999
C   Private:
C     INTEGER SV2D_XCL011_INIVTBL
C     INTEGER SV2D_XCL011_COD
C     INTEGER SV2D_XCL011_ASMF
C     CHARACTER*(16) SV2D_XCL011_REQHLP
C     INTEGER SV2D_XCL011_HLP
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL011_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL011_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL011_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl011.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl011.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL011_INIVTBL()

      SV2D_XCL011_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL011_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL011_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl011.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL011_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL011_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL011_INIVTBL()

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl011.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl011.fc'

      INTEGER IERR
      INTEGER HVFT
      EXTERNAL SV2D_XCL011_HLP
      EXTERNAL SV2D_XCL011_COD
      EXTERNAL SV2D_XCL011_ASMF
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL011_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL011_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC(HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL011_TYP,
     &                    SV2D_XCL011_HLP)
      IERR=SV2D_XCL_AJTFNC(HVFT, SV2D_XCL_FUNC_COD, SV2D_XCL011_TYP,
     &                    SV2D_XCL011_COD)
      IERR=SV2D_XCL_AJTFNC(HVFT, SV2D_XCL_FUNC_ASMF,SV2D_XCL011_TYP,
     &                    SV2D_XCL011_ASMF)

      SV2D_XCL011_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL011_COD
C
C Description:
C     La fonction SV2D_XCL011_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL011_COD(IL, BELF)

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl011.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl011.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 2,-1,
     &                            1,-1,
     &                            1,-1,
     &                            1, 1/), (/2, 4/))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Contrôles
D     IC   = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL011_TYP)
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, KCLDIM)

C---     Assigne les codes
      INDEB = GDTA%KCLLIM(3, IL)
      INFIN = GDTA%KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0)
     &      EDTA%KDIMP(3,IN) = IBSET(EDTA%KDIMP(3,IN),
     &                               SV2D_XCL_DEBIT)
      ENDDO

      SV2D_XCL011_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL011_ASMF
C
C Description:
C     La fonction SV2D_XCL011_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des DDL imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des DDL imposés
C
C Notes:
C     D'après la loi de Manning:
C        u = H**(2/3) / n * pente
C     en posant la pente constante (==1), le débit spécifique est
C        q = H**(5/3) / n
C     qui est intégré.
C************************************************************************
      FUNCTION SV2D_XCL011_ASMF(IL, BELF, VFG)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl011.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xcl011.fc'

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0

      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRN_T),    POINTER :: IPRN
      REAL*8  F1, F3, P1, P3
      REAL*8  QTOT, QSPEC
      REAL*8  Q1, Q3, QS, QG
      REAL*8  C
      REAL*8  VFE  (2)
      INTEGER KLOCE(2)
      INTEGER IERR, I_ERROR
      INTEGER I, IC, IE, IV
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL011_TYP)
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IPRN=> SELF%IPRN

C---     Récupère les données
      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .GE. 1)
      IV = EDTA%KCLCND(3,IC)
      QTOT = EDTA%VCLCNV(IV)

      IEDEB = GDTA%KCLLIM(5, IL)
      IEFIN = GDTA%KCLLIM(6, IL)

C---     Intègre sur la limite
      QS = 0.0D0
      DO I = IEDEB, IEFIN
         IE = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IE)
         NO3 = GDTA%KNGS(3,IE)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Valeurs nodales
         F1 = MAX(EDTA%VPRNO(IPRN%N, NO1), PETIT)    ! Manning
         P1 =     EDTA%VPRNO(IPRN%H, NO1)            ! Profondeur
         F3 = MAX(EDTA%VPRNO(IPRN%N, NO3), PETIT)
         P3 =     EDTA%VPRNO(IPRN%H, NO3)

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q3 = (P3**CINQ_TIER) / F3

C---        Intègre
         QS = QS + GDTA%VDJS(3,IE)*(Q1+Q3)
199      CONTINUE
      ENDDO

C---    Sync et contrôle
      CALL MPI_ALLREDUCE(QS, QG, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      QS = QG
      IF (QS .LT. PETIT) GOTO 9900

C---     Débit spécifique
      QSPEC = QTOT / QS

C---     Assemble
      DO I = IEDEB, IEFIN
         IE = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IE)
         NO3 = GDTA%KNGS(3,IE)

C---        Coefficient
         C = -UN_3*QSPEC*GDTA%VDJS(3,IE)

C---        Valeurs nodales
         F1 = MAX(EDTA%VPRNO(IPRN%N, NO1), PETIT)    ! Manning
         P1 =     EDTA%VPRNO(IPRN%H, NO1)            ! Profondeur
         F3 = MAX(EDTA%VPRNO(IPRN%N, NO3), PETIT)
         P3 =     EDTA%VPRNO(IPRN%H, NO3)

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q3 = (P3**CINQ_TIER) / F3

C---        Contribution élémentaire
         VFE(1) = C*(Q1+Q1 + Q3)
         VFE(2) = C*(Q3+Q3 + Q1)

C---        Assemblage du vecteur global
         KLOCE(:) = (/ EDTA%KLOCN(3, NO1), EDTA%KLOCN(3, NO3) /)
         IERR = SP_ELEM_ASMFE(2, KLOCE, VFE, VFG)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_XCL011_TYP
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL011_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL011_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL011_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL011_REQHLP
C------------------------------------------------------------------------

C<comment>
C *****<br>
C This boundary condition has been DEPRECATED. Please use condition <b>125</b> instead.<br>
C *****
C <p>
C  Boundary condition of type <b>011</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge Q is distributed on the wetted part of the boundary, taking into account the current water level h.
C  The velocity distribution is based on a Manning formulation with varying coefficient (i.e. depth and Manning dependent).
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 11</li>
C     <li>Values: Q</li>
C     <li>Units: m^3/s</li>
C     <li>Example:  11   -500</li>
C  </ul>
C</comment>

      SV2D_XCL011_REQHLP = 'bc_type_011'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL011_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL011_HLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl011.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl011.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl011.hlp')

      SV2D_XCL011_HLP = ERR_TYP()
      RETURN
      END
