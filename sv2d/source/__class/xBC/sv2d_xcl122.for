C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Sollicitation de débit via la forme faible de la continuité.
C     Débit global redistribué en fonction du niveau d'eau fixe spécifié.
C     Formulation HYDROSIM
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL122_000
C     INTEGER SV2D_XCL122_999
C     INTEGER SV2D_XCL122_COD
C     INTEGER SV2D_XCL122_PRC
C     INTEGER SV2D_XCL122_ASMF
C     INTEGER SV2D_XCL122_HLP
C   Private:
C     INTEGER SV2D_XCL122_INIVTBL
C     CHARACTER*(16) SV2D_XCL122_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_XCL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_XCL122_DATA_000

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl122.fc'

      DATA SV2D_XCL122_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL122_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL122_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL122_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl122.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl122.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL122_INIVTBL()

      SV2D_XCL122_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL122_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL122_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl122.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL122_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL122_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL122_INIVTBL()

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl122.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl122.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_XCL122_HLP
      EXTERNAL SV2D_XCL122_COD
      EXTERNAL SV2D_XCL122_PRC
      EXTERNAL SV2D_XCL122_ASMF
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL122_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_XCL122_TPN)
!!!      IRET = C_ST_CRC32(SV2D_XCL122_TPN(1:LTPN), SV2D_XCL122_TPC)
!!!      IERR = SV2D_XCL_INIVTBL2(HVFT, SV2D_XCL122_TPN)
      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL122_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL122_TYP,
     &                     SV2D_XCL122_HLP)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_COD,  SV2D_XCL122_TYP,
     &                    SV2D_XCL122_COD)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_PRC,  SV2D_XCL122_TYP,
     &                    SV2D_XCL122_PRC)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMF, SV2D_XCL122_TYP,
     &                    SV2D_XCL122_ASMF)
      SV2D_XCL122_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL122_COD
C
C Description:
C     La fonction SV2D_XCL122_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau fixe spécifié.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL122_COD(IL, BELF)

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl122.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl122.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 2,-1,
     &                            1,-1,
     &                            1,-1,
     &                            2, 2/), (/2, 4/))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Contrôles
D     IC   = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL122_TYP)
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, KCLDIM)

C---     Assigne les codes
      INDEB = GDTA%KCLLIM(3, IL)
      INFIN = GDTA%KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0)
     &      EDTA%KDIMP(3,IN) = IBSET(EDTA%KDIMP(3,IN),
     &                               SV2D_XCL_DEBIT_H)
      ENDDO

      SV2D_XCL122_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL122_PRC
C
C Description:
C     La fonction privée SV2D_XCL122_PRC calcule les valeurs de condition
C     limite en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau fixe spécifié.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des DDL imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL122_PRC(IL, BELF)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl122.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xcl122.fc'

      INTEGER IERR, I_ERROR
      INTEGER IC, IV
      INTEGER I, IN, IE, IES
      INTEGER INDEB, INFIN
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8  P1, P3
      REAL*8  H
      REAL*8  QTOT, QSPEC
      REAL*8  QS, QG
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRN_T),    POINTER :: IPRN
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL122_TYP)
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA
      IPRN => SELF%IPRN

      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 2)
      IV = EDTA%KCLCND(3,IC)
      QTOT = EDTA%VCLCNV(IV)
      H    = EDTA%VCLCNV(IV+1)

      INDEB = GDTA%KCLLIM(3, IL)
      INFIN = GDTA%KCLLIM(4, IL)
      IEDEB = GDTA%KCLLIM(5, IL)
      IEFIN = GDTA%KCLLIM(6, IL)

C---     Intègre la surface mouillée
      QS = 0.0D0
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Valeurs nodales
         P1 = MAX(H-EDTA%VPRNO(IPRN%Z, NO1), ZERO)     ! Profondeur
         P3 = MAX(H-EDTA%VPRNO(IPRN%Z, NO3), ZERO)

C---        Intègre
         QS = QS + GDTA%VDJS(3,IES)*(P1+P3)
199      CONTINUE
      ENDDO

C---     Sync et contrôle
      CALL MPI_ALLREDUCE(QS, QG, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      QS   = QG
      IF (QS .LT. PETIT) GOTO 9900

C---     Débit spécifique
      QSPEC = QTOT / QS

C---     Impose les valeurs
      DO I = INDEB, INFIN
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0) THEN
            IF ((H-EDTA%VPRNO(IPRN%Z,IN)) .GT. ZERO) THEN
               EDTA%VDIMP(3,IN) = QSPEC
            ELSE
               EDTA%VDIMP(3,IN) = ZERO
            ENDIF
         ENDIF
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_XCL122_TYP
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL122_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL122_ASMF
C
C Description:
C     La fonction SV2D_XCL122_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau fixe spécifié.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des DDL imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL122_ASMF(IL, BELF, VFG)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl122.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xcl122.fc'
      
      INTEGER, PARAMETER :: NDLE_LCL = 2
      
      REAL*8  H
      REAL*8  C
      REAL*8  P1, P3, Q1, Q3, C1, C3
      REAL*8  VFE(NDLE_LCL)
      INTEGER IERR
      INTEGER IC, IV
      INTEGER IE, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      INTEGER KLOCE(NDLE_LCL)
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRN_T),    POINTER :: IPRN
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA
      IPRN => SELF%IPRN

      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL122_TYP)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 2)
      IV = EDTA%KCLCND(3,IC)
      H  = EDTA%VCLCNV(IV+1)

      IEDEB = GDTA%KCLLIM(5, IL)
      IEFIN = GDTA%KCLLIM(6, IL)

C---     Assemble
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)

C---        Coefficient
         C = -UN_3*GDTA%VDJS(3,IES)

C---        Valeurs nodales
         P1 = MAX(H-EDTA%VPRNO(IPRN%Z,NO1), ZERO)    ! Profondeur
         P3 = MAX(H-EDTA%VPRNO(IPRN%Z,NO3), ZERO)
         Q1 = EDTA%VDIMP(3,NO1)                       ! Débit spec.
         Q3 = EDTA%VDIMP(3,NO3)
         C1 = C*Q1*P1
         C3 = C*Q3*P3
         VFE   = (/ (C1+C1 + C3), (C3+C3 + C1) /)

C---        Assemblage du vecteur global
         KLOCE = (/ EDTA%KLOCN(3,NO1), EDTA%KLOCN(3,NO3) /)
         IERR = SP_ELEM_ASMFE(NDLE_LCL, KLOCE, VFE, VFG)
      ENDDO

      SV2D_XCL122_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL122_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL122_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL122_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>122</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge Q is distributed on the wetted part of the boundary, assuming a constant water level h.
C  The distribution is based on a constant velocity on the whole section.
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 122</li>
C     <li>Values: Q, h</li>
C     <li>Units: m^3/s m</li>
C     <li>Example:  122   -500 10</li>
C  </ul>
C</comment>

      SV2D_XCL122_REQHLP = 'bc_type_122'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL122_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL122_HLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl122.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl122.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl122.hlp')

      SV2D_XCL122_HLP = ERR_TYP()
      RETURN
      END
