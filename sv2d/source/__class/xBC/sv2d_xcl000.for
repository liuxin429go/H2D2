C************************************************************************
C --- Copyright (c) INRS 2010-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C     Classe de base des C.L.
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL000_M

      USE LM_ELEM_M, ONLY : LM_ELEM_T
      IMPLICIT NONE

      TYPE :: SV2D_XCL000_T
         INTEGER, PUBLIC :: IL                  ! Indice de la C.L.
         INTEGER, PUBLIC :: IT                  ! Type de la C.L.
         INTEGER, PUBLIC :: IK                  ! Genre de la C.L.
         INTEGER, PUBLIC, POINTER :: KNOD(:)    ! Noeuds
         INTEGER, PUBLIC, POINTER :: KELE(:)    ! Éléments
         REAL*8,  PUBLIC, POINTER :: VDST(:)    ! Distance au 1er noeud
         REAL*8,  PUBLIC, POINTER :: VCND(:)    ! Valeurs
      CONTAINS
         PROCEDURE, PUBLIC :: INI   => SV2D_XCL000_INI

         PROCEDURE, PUBLIC :: COD   => SV2D_XCL000_COD
         PROCEDURE, PUBLIC :: PRC   => SV2D_XCL000_PRC
         PROCEDURE, PUBLIC :: CLC   => SV2D_XCL000_CLC
         PROCEDURE, PUBLIC :: ASMF  => SV2D_XCL000_ASMF
         PROCEDURE, PUBLIC :: ASMKU => SV2D_XCL000_ASMKU
         PROCEDURE, PUBLIC :: ASMK  => SV2D_XCL000_ASMK
         PROCEDURE, PUBLIC :: ASMKT => SV2D_XCL000_ASMKT
         PROCEDURE, PUBLIC :: ASMM  => SV2D_XCL000_ASMM
         PROCEDURE, PUBLIC :: ASMMU => SV2D_XCL000_ASMMU
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD  => SV2D_XCL000_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP  => SV2D_XCL000_HLP
         
         ! ---  Méthodes protégées
         PROCEDURE, PUBLIC :: CHK   => SV2D_XCL000_CHK
      END TYPE SV2D_XCL000_T

      !! PUBLIC :: SV2D_XCL000_CTR  ! Pas de constructeur ici
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: SV2D_XCL000_DTR
      END INTERFACE DEL
      
      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL000_DTR
C
C Description:
C     Le destructeur <code>SV2D_XCL000_DTR</code> déconnecte les
C     pointeurs et désalloue la mémoire.
C     Le destructeur est générique pour tous les héritiers qui 
C     n'ajoutent pas d'attributs.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER 
     &FUNCTION SV2D_XCL000_DTR(SELF)

      CLASS(SV2D_XCL000_T), INTENT(INOUT), POINTER :: SELF

      INCLUDE 'err.fi'
      
      INTEGER IRET
C-----------------------------------------------------------------------

      IF (.NOT. ASSOCIATED(SELF)) GOTO 9999
      
      SELF%IL = -1
      SELF%IT = -1
      SELF%IK = -1
      SELF%KNOD => NULL()
      SELF%KELE => NULL()
      SELF%VDST => NULL()
      SELF%VCND => NULL()

      DEALLOCATE(SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900
      SELF => NULL()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_DEALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE      
      SV2D_XCL000_DTR = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_DTR

C************************************************************************
C Sommaire:  SV2D_XCL000_INI
C
C Description:
C     La méthode <code>SV2D_XCL000_INI</code> initialise la classe à 
C     partir des paramètres.
C
C Entrée:
C     SELF        L'objet
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     VCLCND         Valeurs associées aux conditions
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER 
     &FUNCTION SV2D_XCL000_INI(SELF,
     &                         IL,
     &                         IT,
     &                         IK,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         VCLCND)

      CLASS(SV2D_XCL000_T), INTENT(INOUT) :: SELF
      INTEGER, INTENT(IN) :: IL
      INTEGER, INTENT(IN) :: IT
      INTEGER, INTENT(IN) :: IK
      INTEGER, TARGET, INTENT(IN) :: KCLNOD(:)
      INTEGER, TARGET, INTENT(IN) :: KCLELE(:)
      REAL*8,  TARGET, INTENT(IN) :: VCLDST(:)
      REAL*8,  TARGET, INTENT(IN) :: VCLCND(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SELF%IL = IL
      SELF%IT = IT
      SELF%IK = IK
      SELF%KNOD => KCLNOD
      SELF%KELE => KCLELE
      SELF%VDST => VCLDST
      SELF%VCND => VCLCND

      SV2D_XCL000_INI = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_INI

C************************************************************************
C Sommaire:  SV2D_XCL000_CHKDIM
C
C Description:
C     La méthode utilitaire protégée SV2D_XCL000_CHKDIM contrôle les dimensions
C     d'une condition limite. Si le nombre de valeurs KDIM est non négatif,
C     alors le nombre de valeurs sera également contrôlé.
C
C Entrée:
C     SELF        L'objet
C     KCLDIM      Table des dim min et max pour les
C                 noeuds, éléments, segments, valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_CHK(SELF, KCLDIM)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: KCLDIM(:, :)

      INCLUDE 'err.fi'
!      INCLUDE 'mpif.h'
!      INCLUDE 'mputil.fi'

      INTEGER I_ERROR

      INTEGER IC, N, NVAL
      INTEGER ISZ_L(3), ISZ_G(3)
C-----------------------------------------------------------------------

      ISZ_L(1) = SIZE(SELF%KNOD, 1)
      ISZ_L(2) = SIZE(SELF%KELE, 1)
      ISZ_L(3) = -1  !! ??? SIZE(SELF%KELE, 1)
!      CALL MPI_ALLREDUCE(ISZ_L, ISZ_G, 2, MP_TYPE_INT(),
!     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)

C---     Les noeuds
      N = KCLDIM(1,1)
      IF (N .GE. 0 .AND. ISZ_G(1) .LT. N) GOTO 9900
      N = KCLDIM(2,1)
      IF (N .GE. 0 .AND. ISZ_G(1) .GT. N) GOTO 9900

C---     Les éléments
      N = KCLDIM(1,2)
      IF (N .GE. 0 .AND. ISZ_G(2) .LT. N) GOTO 9901
      N = KCLDIM(2,2)
      IF (N .GE. 0 .AND. ISZ_G(2) .GT. N) GOTO 9901

C---     Les segments
!      N = KCLDIM(1,3)
!      IF (N .GE. 0 .AND. ISZ_G(3) .LT. N) GOTO 9902
!      N = KCLDIM(2,3)
!      IF (N .GE. 0 .AND. ISZ_G(3) .GT. N) GOTO 9902

C---     Les valeurs
      NVAL = SIZE(SELF%VCND)
      N = KCLDIM(1,4)
      IF (N .GE. 0 .AND. NVAL .LT. N) GOTO 9903
      N = KCLDIM(2,4)
      IF (N .GE. 0 .AND. NVAL .GT. N) GOTO 9903

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_NOD_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(A)') 'ERR_NBR_ELE_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_SEG_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  WRITE(ERR_BUF, '(A,I6)') 'MSG_LIMITE: ', SELF%IL
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL000_CHK = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_CHK

C************************************************************************
C Sommaire:  SV2D_XCL000_COD
C
C Description:
C     La méthode <code>SV2D_XCL000_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_COD(SELF, KDIMP)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_COD

C************************************************************************
C Sommaire:  SV2D_XCL000_PRC
C
C Description:
C     La méthode <code>SV2D_XCL000_PRC</code> fait l'étape de pré-calcul,
C     résultats qui ne dépendent pas des DDL.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_PRC(SELF, ELEM, VDIMP)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(INOUT) :: VDIMP(:,:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_PRC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_PRC

C************************************************************************
C Sommaire:  SV2D_XCL000_CLC
C
C Description:
C     La méthode <code>SV2D_XCL000_CLC</code> fait l'étape de calcul, 
C     résultats qui dépendent des DDL mais ne participent pas
C     directement à l'assemblage.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_CLC(SELF, ELEM)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_CLC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_CLC

C************************************************************************
C Sommaire:  SV2D_XCL000_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_ASMF(SELF, ELEM, VFG)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(IN) :: VFG(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_ASMF = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_ASMF

C************************************************************************
C Sommaire:  SV2D_XCL000_ASMKU
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_ASMKU(SELF, ELEM, VFG)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8,  INTENT(IN) :: VFG(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_ASMKU = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_ASMKU

C************************************************************************
C Sommaire: SV2D_XCL000_ASMK
C
C Description:
C     La méthode <code>SV2D_XCL000_ASMK</code> calcule le matrice
C     de rigidité élémentaire. L'assemblage de la matrice globale est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_ASMK(SELF, ELEM, HMTX, F_ASM)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_ASMK = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_ASMK

C************************************************************************
C Sommaire: SV2DASMKT
C
C Description:
C     La méthode <code>SV2D_XCL000_ASMKT</code> calcule le matrice
C     de rigidité élémentaire. L'assemblage de la matrice globale est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_ASMKT(SELF, ELEM, HMTX, F_ASM)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_ASMKT = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_ASMKT

C************************************************************************
C Sommaire: SV2D_XCL000_ASMM
C
C Description:
C     ASSEMBLAGE DE LA MATRICE MASSE
C
C Entrée:
C
C Sortie: VKG
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_ASMM(SELF, ELEM, HMTX, F_ASM)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'err.fi'
C-----------------------------------------------------------------

      SV2D_XCL000_ASMM = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_ASMM

C************************************************************************
C Sommaire:  SV2D_XCL000_ASMMU
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_ASMMU(SELF, ELEM, VFG)

      CLASS(SV2D_XCL000_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(IN) :: VFG(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_ASMMU = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_ASMMU

C************************************************************************
C Sommaire:  SV2D_XCL000_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL000_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL000_HLP()

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL000_HLP

C************************************************************************
C Sommaire:  SV2D_XCL000_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL000_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL000_CMD()

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL000_CMD = 'dummy'
      RETURN
      END FUNCTION SV2D_XCL000_CMD

      END MODULE SV2D_XCL000_M

      