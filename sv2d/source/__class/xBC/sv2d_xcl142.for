C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Impose une loi de seuil.
C     Impose comme sollicitation le débit donné par la loi de seuil et
C     calculé à partir du niveau aux noeuds de la limite.
C        Q = Q0 + A*(H-Z0)**B
C     Valeur lues:
C        Q0, Z0, A, B
C     Le débit total est calculé par rapport au niveau moyen de la section,
C     et redistribué suivant une formulation Manning variable.
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL142_000
C     INTEGER SV2D_XCL142_999
C     INTEGER SV2D_XCL142_COD
C     INTEGER SV2D_XCL142_ASMF
C   Private:
C     INTEGER SV2D_XCL142_INIVTBL
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_XCL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_XCL142_DATA_000

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl142.fc'

      DATA SV2D_XCL142_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL142_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL142_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL142_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl142.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl142.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL142_INIVTBL()

      SV2D_XCL142_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL142_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL142_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl142.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL142_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL142_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL142_INIVTBL()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl142.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl142.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_XCL142_HLP
      EXTERNAL SV2D_XCL142_COD
      EXTERNAL SV2D_XCL142_ASMF
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL142_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_XCL142_TPN)
!!!      IRET = C_ST_CRC32(SV2D_XCL142_TPN(1:LTPN), SV2D_XCL142_TPC)
!!!      IERR = SV2D_XCL_INIVTBL2(HVFT, SV2D_XCL142_TPN)
      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL142_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL142_TYP,
     &                     SV2D_XCL142_HLP)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_COD, SV2D_XCL142_TYP,
     &                    SV2D_XCL142_COD)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMF,SV2D_XCL142_TYP,
     &                    SV2D_XCL142_ASMF)

      SV2D_XCL142_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL142_COD
C
C Description:
C     La fonction SV2D_XCL142_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose une loi de seuil de la
C     forme <code>Q = Q0 + A*(h-Z0)**B</code>.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des ddl imposés
C
C Notes:
C     Valeurs lues: q0, z0, a, b
C************************************************************************
      FUNCTION SV2D_XCL142_COD(IL, BELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL142_COD
CDEC$ ENDIF

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl142.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl142.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL142_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, 4)

C---     Les indices
      IC = GDTA%KCLLIM(2, IL)

C---     Assigne les codes
      INDEB = GDTA%KCLLIM(3, IL)
      INFIN = GDTA%KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0)
     &      EDTA%KDIMP(3,IN) = IBSET(EDTA%KDIMP(3,IN),
     &                               SV2D_XCL_SEUIL)
      ENDDO

      SV2D_XCL142_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL142_ASMF
C
C Description:
C     La fonction SV2D_XCL142_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global sur le seuil
C     calculé avec le niveau moyen et qui sera redistribué avec un
C     Manning variable.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des ddl imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL142_ASMF(IL, GDTA, EDTA, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL142_ASMF
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl142.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl142.fc'

      INTEGER IERR, I_ERROR
      INTEGER I, IC, IE, IV
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO2, NO3
      REAL*8  Z0, Q0, A, B
      REAL*8  F1, F2, F3, P1, P2, P3
      REAL*8  H1, H3, HM, BH
      REAL*8  Q1, Q2, Q3, QS, QG
      REAL*8  QTOT, QSPEC
      REAL*8  C, M(2)
      REAL*8  VL(3), VG(3)

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL142_TYP)
C-----------------------------------------------------------------------

      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 4)
      IV = EDTA%KCLCND(3,IC)
      Q0 = EDTA%VCLCNV(IV)
      Z0 = EDTA%VCLCNV(IV+1)
      A  = EDTA%VCLCNV(IV+2)
      B  = EDTA%VCLCNV(IV+3)

      IEDEB = GDTA%KCLLIM(5, IL)
      IEFIN = GDTA%KCLLIM(6, IL)

C---     Intègre sur la limite
      QS = ZERO
      HM = ZERO
      BH = ZERO     ! Largeur
      DO I = IEDEB, IEFIN
         IE = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IE)
         NO2 = GDTA%KNGS(2,IE)
         NO3 = GDTA%KNGS(3,IE)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Valeurs nodales
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)    ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)            ! Profondeur
         F2 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     EDTA%VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)

         H1 =     EDTA%VDLG(3, NO1)                        ! h
         H3 =     EDTA%VDLG(3, NO3)                        ! h

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q2 = (P2**CINQ_TIER) / F2
         Q3 = (P3**CINQ_TIER) / F3

C---        Intègre
         QS = QS + UN_2*GDTA%VDJS(3,IE)*(Q1+Q2+Q2+Q3)
         HM = HM +      GDTA%VDJS(3,IE)*(H1+H3)
         BH = BH + DEUX*GDTA%VDJS(3,IE)
199      CONTINUE
      ENDDO

C---    Sync et contrôle
      VL = (/ QS, HM, BH /)
      CALL MPI_ALLREDUCE(VL, VG, 3, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      QS = VG(1)
      HM = VG(2)
      BH = VG(3)
D     CALL ERR_ASR(BH .GT. ZERO)
      IF (ABS(Q0) .GT. PETIT .AND. QS .LT. PETIT) GOTO 9900
      QS = MAX(QS, PETIT)

C---     Débit totale
      HM = HM / BH
      HM = MAX(HM, Z0)
      QTOT = Q0 + A*(HM-Z0)**B

C---     Débit spécifique
      QSPEC = QTOT / QS

C---     Assemble
      DO I = IEDEB, IEFIN
         IE = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IE)
         NO2 = GDTA%KNGS(2,IE)
         NO3 = GDTA%KNGS(3,IE)

C---        Coefficient
         C = -UN_12*GDTA%VDJS(3,IE)*QSPEC        ! UN_6*DJL2

C---        Valeurs nodales
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)    ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)            ! Profondeur
         F2 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     EDTA%VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q2 = (P2**CINQ_TIER) / F2
         Q3 = (P3**CINQ_TIER) / F3

C---        Assemblage du vecteur global (mixte L2-L3L)
         M(1) = C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
         M(2) = C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO1), M(1), VFG)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO3), M(2), VFG)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_XCL142_TYP
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_NIVEAU: ', HM
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_DEBIT: ', QTOT
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_SURFACE: ', QS
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL142_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL142_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL142_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL142_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>142</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge Q on the section is calculated according to a weir rating relation
C  <pre>
C        Q = Q0 + a*(h-z0)^b
C  </pre>
C  where h is the section mean water. It is distributed on the wetted part of the boundary.
C  The velocity distribution is based on a Manning formulation with constant coefficient (i.e. depth dependent).
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Weir rating relation</li>d
C     <li>Code: 142</li>
C     <li>Values: Q0, z0, a, b</li>
C     <li>Units: m^3/s m - -</li>
C     <li>Example:  142   -500 10 1 0.79</li>
C  </ul>

      SV2D_XCL142_REQHLP = 'bc_type_142'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL142_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL142_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL142_HLP
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl142.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl142.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl142.hlp')

      SV2D_XCL142_HLP = ERR_TYP()
      RETURN
      END
