C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y4_cbs_pslprno.for,v 1.2 2015/11/26 22:23:33 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XY4_PSLPRNO
C   Private:
C     INTEGER SV2D_XY4_INI_IPRNO
C
C************************************************************************

      SUBMODULE(SV2D_XY4_M) SV2D_XY4_PSLPRNO

      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_XY4_PSLPRNO
C
C Description:
C     Traitement post-lecture des propriétés nodales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XY4_PSLPRNO(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XY4_PSLPRNO
CDEC$ ENDIF

      USE SV2D_IPRN_M, ONLY: SV2D_IPRN_T

      CLASS(SV2D_XY4_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE (SV2D_IPRN_T), POINTER :: IPRN
C-----------------------------------------------------------------------

C---     Récupère les données
      IPRN => SELF%IPRN

C---     Assigne les indices      
      IPRN%Z          =  1
      IPRN%N          =  2
      IPRN%ICE_E      =  3
      IPRN%ICE_N      =  4
      IPRN%WND_X      =  5
      IPRN%WND_Y      =  6
      IPRN%U          =  7
      IPRN%V          =  8
      IPRN%H          =  9
      IPRN%COEFF_CNVT = 10
      IPRN%COEFF_GRVT = 11
      IPRN%COEFF_FROT = 12
      IPRN%COEFF_DIFF = 13
      IPRN%COEFF_PE   = 14
      IPRN%COEFF_DMPG = 15
      IPRN%COEFF_VENT = 16
      IPRN%DECOU_DRCY = 17
      IPRN%COEFF_PORO = 18
      IPRN%DECOU_PENA = -1

      SV2D_XY4_PSLPRNO = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY4_PSLPRNO

      END SUBMODULE SV2D_XY4_PSLPRNO
      