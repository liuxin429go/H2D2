C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y4_cbs_clcprno.for,v 1.7 2015/11/26 22:23:33 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_ZY4_CLCPRNO
C   Private:
C     INTEGER SV2D_ZY4_CLCPRNEV
C     INTEGER SV2D_ZY4_CLCPRNES
C     INTEGER SV2D_ZY4_CLCPRN_1N
C
C************************************************************************

      MODULE SV2D_ZY4_CLCPRNEV_M

      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: SV2D_ZY4_CLCPRN_1N
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG. Le calcul est
C     fait sur un noeud.
C
C Entrée:
C      REAL*8  VPRG        Les PRopriétés GLobales
C      REAL*8  VDLN        Le Degré de Liberté Nodaux
C
C Sortie:
C      REAL*8  VPRN        Les PRopriétés Nodales
C      INTEGER IERR
C
C Notes:
C     En PRFA, un noeud milieu va se retrouver rehaussé si un noeud sommet
C     est découvert. Il y a un impact sur le coefficient de frottement
C     qui est diminué.
C     ULIM vise à "freiner" les fortes vitesses, celles au-delà de UMAX.
C     Jusqu'à UMAX, la loi est linéaire, puis exp 1/3
C     Le vent est mis à 0 en présence de glace. Mais si le vent est relatif,
C     il y aura quand même la contribution du courant.
C************************************************************************
      INTEGER FUNCTION SV2D_ZY4_CLCPRN_1N(VPRN,
     &                                    VDLN,
     &                                    IPRN,
     &                                    XPRG)

      USE SV2D_IPRN_M
      USE SV2D_XPRG_M
      USE COMPLEXIFY
      IMPLICIT NONE

      COMPLEX*16, INTENT(INOUT) :: VPRN (:)
      COMPLEX*16, INTENT(IN)    :: VDLN (:)
      TYPE(SV2D_IPRN_T), INTENT(IN) :: IPRN
      TYPE(SV2D_XPRG_T), INTENT(IN) :: XPRG

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_enum.fi'
      INCLUDE 'sv2d_fnct.fi'

      REAL*8, PARAMETER :: RHO_AIR = 1.2475D+00
      REAL*8, PARAMETER :: RHO_EAU = 1.0000D+03
      REAL*8, PARAMETER :: RHO_REL = RHO_AIR / RHO_EAU

      REAL*8, PARAMETER :: R_4_3N = -4.0D0/3.0D0
      REAL*8, PARAMETER :: R_1_3  =  1.0D0/3.0D0

      COMPLEX*16 ALFA
      COMPLEX*16 H1, H2
      COMPLEX*16 PRFE, PRFA, UN_PRFA
      COMPLEX*16 VX, VY
      COMPLEX*16 WX, WY, VENT
      COMPLEX*16 VN1, VN2
      REAL*8  PORO, VMAN, VFLN, FCVT, FGRA, VDIF, VDRC
      REAL*8  NTOT, FROTT
      REAL*8  VMOD, WMOD
C-----------------------------------------------------------------------
      REAL*8 ULIM, UEXP, U
      UEXP(U) = (U-XPRG%DECOU_UMAX+UN)**R_1_3 + XPRG%DECOU_UMAX-UN
      ULIM(U) = MIN(UEXP(MAX(U,XPRG%DECOU_UMAX)), U)
C-----------------------------------------------------------------------

C---     Profondeur
      PRFE = VPRN(IPRN%V)                 ! Prof effective nodale
      PRFA = VPRN(IPRN%H)                 ! Prof absolue linéaire
      UN_PRFA = UN / PRFA                 ! Inverse prof absolue

C---     Alfa
      H1 = XPRG%DECOU_HMIN
      H2 = XPRG%DECOU_HTRG
      ALFA = (PRFE-H1)/(H2-H1)
      ALFA = MIN(UN, MAX(ZERO, ALFA))
      VN1  = UN - ALFA
      VN2  = ALFA

C---     Module de la vitesse
      VX = VDLN(1) * UN_PRFA
      VY = VDLN(2) * UN_PRFA
      VMOD = HYPOT(VX, VY)

C---     Module du vent relatif
      WX = VPRN(IPRN%WND_X) - XPRG%CMULT_VENT_REL*VX
      WY = VPRN(IPRN%WND_Y) - XPRG%CMULT_VENT_REL*VY
      WMOD = HYPOT(WX, WY)
      VENT = RHO_REL * SP_HDRO_CW(WMOD, XPRG%FCT_CW_VENT) * WMOD

C---     Manning global
      NTOT = HYPOT(VPRN(IPRN%N), VPRN(IPRN%ICE_N))

C---     Paramètres variables pour le découvrement
      VMOD = VN1*ULIM(VMOD)          + VN2*VMOD
      PORO = VN1*XPRG%DECOU_PORO     + VN2 !*UN
      VFLN = VN1*XPRG%DECOU_AMORT    + VN2*XPRG%STABI_AMORT
      VMAN = VN1*XPRG%DECOU_MAN      + VN2*XPRG%CMULT_MAN*NTOT
      FCVT = VN1*XPRG%DECOU_CON_FACT + VN2*XPRG%CMULT_CON
      FGRA = VN1*XPRG%DECOU_GRA_FACT + VN2*XPRG%CMULT_GRA
      VDIF = VN1*XPRG%DECOU_DIF_NU  !+ VN2*0.0
      VDRC = VN1*XPRG%DECOU_DRC_NU   + VN2*XPRG%STABI_DARCY
      VENT =                         + VN2*XPRG%CMULT_VENT*VENT

C---     Frottement
!      L'utilisation de la masse lumpée pour découpler le frottement
!      mène à des résultats très différents d'Hydrosim.
!      FROTT = XPRG%GRAVITE*VMAN*VMAN * UMOD * (UN_PRFA**R_4_3)
      FROTT = VFLN + XPRG%GRAVITE*VMAN*VMAN * VMOD * PRFA**R_4_3N    ! Hydrosim

C---     Valeurs nodales
      VPRN(IPRN%U)          = VDLN(1) * UN_PRFA ! U
      VPRN(IPRN%V)          = VDLN(2) * UN_PRFA ! V
      VPRN(IPRN%H)          = PRFA              ! Prof absolue
      VPRN(IPRN%COEFF_FROT) = FROTT             ! g n2 |u| / H**(4/3)
      VPRN(IPRN%COEFF_CNVT) = FCVT              ! Facteur de convection
      VPRN(IPRN%COEFF_GRVT) = FGRA              ! Facteur de gravité
      VPRN(IPRN%DECOU_DIFF) = VDIF              ! Diffusion de decou.
      VPRN(IPRN%DECOU_DRCY) = VDRC              ! Darcy
      VPRN(IPRN%COEFF_VENT) = VENT              ! cw |w| rho_air/rho_eau
      VPRN(IPRN%COEFF_PORO) = PORO

      SV2D_ZY4_CLCPRN_1N = ERR_TYP()
      RETURN
      END FUNCTION SV2D_ZY4_CLCPRN_1N

      END MODULE SV2D_ZY4_CLCPRNEV_M

C************************************************************************
C Sommaire: SV2D_ZY4_CLCPRNEV
C
C Description:
C     Calcul des propriétés nodales d'un élément de volume,
C     dépendantes de VDLE. Version complexe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C     A cause de l'appel via les functors, ce ne sont pas des pointeurs F90
C     qui sont passé. Il faut donc un reshape.
C************************************************************************
      FUNCTION SV2D_ZY4_CLCPRNEV(HOBJ, VDLE_F, VPRN_F)

      USE SV2D_ZY4_M
      USE SV2D_ZY4_CLCPRNEV_M
      USE COMPLEXIFY
      IMPLICIT NONE

      INTEGER HOBJ
      COMPLEX*16, TARGET :: VDLE_F(*) ! cf note
      COMPLEX*16, TARGET :: VPRN_F(*) ! cf note

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_zy4.fc'

      INTEGER IERR
      INTEGER IN
      INTEGER IPV, IPH
      COMPLEX*16  BATHY, EPAIGL, PROF
      COMPLEX*16  PRFE, PRFA
      COMPLEX*16, POINTER :: VPRN(:,:)
      COMPLEX*16, POINTER :: VDLE(:,:)
      TYPE (SV2D_XBS_T), POINTER :: SELF
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),     POINTER :: IPRN
      TYPE (SV2D_XPRG_T),     POINTER :: XPRG

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Récupère les données
      SELF => SV2D_XBS_REQSELF(HOBJ)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IPRN => SELF%IPRN
      XPRG => SELF%XPRG

C---     Reshape the arrays
      VDLE(1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_F(1:EDTA%NDLN*GDTA%NNELV)
      VPRN(1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_F(1:EDTA%NPRNO*GDTA%NNELV)
      
C---     Indices dans VPRNO
      IPV = IPRN%V
      IPH = IPRN%H

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2
      VDLE(3,NO4) = (VDLE(3,NO3)+VDLE(3,NO5))*UN_2
      VDLE(3,NO6) = (VDLE(3,NO5)+VDLE(3,NO1))*UN_2

C---     Profondeurs effective et absolue
      DO IN=1,GDTA%NNELV
         BATHY   = VPRN(IPRN%Z,IN)
         EPAIGL  = 0.9D0 * VPRN(IPRN%ICE_E,IN)
         PROF    = VDLE(3,IN) - BATHY
         EPAIGL  = MIN(EPAIGL, PROF)
         EPAIGL  = MAX(EPAIGL, ZERO)
         PRFE    = PROF - EPAIGL                ! Prof effective
         PRFA    = MAX(PRFE, XPRG%DECOU_HMIN)   ! Prof absolue
         VPRN(IPRN%V,IN) = PRFE           ! Prof effective temp.
         VPRN(IPRN%H,IN) = PRFA           ! Prof absolue
      ENDDO

C---     Impose les profondeurs sur les noeuds milieux
      VPRN(IPV,NO2) = (VPRN(IPV,NO1)+VPRN(IPV,NO3))*UN_2
      VPRN(IPV,NO4) = (VPRN(IPV,NO3)+VPRN(IPV,NO5))*UN_2
      VPRN(IPV,NO6) = (VPRN(IPV,NO5)+VPRN(IPV,NO1))*UN_2

C        Profondeur linéaire
C        Noeud rehaussé si un noeud sommet est découvert
      VPRN(IPH,NO2) = (VPRN(IPH,NO1)+VPRN(IPH,NO3))*UN_2
      VPRN(IPH,NO4) = (VPRN(IPH,NO3)+VPRN(IPH,NO5))*UN_2
      VPRN(IPH,NO6) = (VPRN(IPH,NO5)+VPRN(IPH,NO1))*UN_2

C---     Boucle sur les noeuds
      DO IN=1,GDTA%NNELV
         IERR = SV2D_ZY4_CLCPRN_1N(VPRN(1:,IN),VDLE(1:,IN),IPRN,XPRG)
      ENDDO

      SV2D_ZY4_CLCPRNEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_ZY4_CLCPRNES
C
C Description:
C     Calcul des propriétés nodales d'un élément de surface,
C     dépendantes de VDLE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      FUNCTION SV2D_ZY4_CLCPRNES(HOBJ, VDLE_F, VPRN_F)

      USE SV2D_ZY4_M
      USE SV2D_ZY4_CLCPRNEV_M
      USE COMPLEXIFY
      IMPLICIT NONE

      INTEGER HOBJ
      COMPLEX*16, TARGET :: VDLE_F(*) ! cf note
      COMPLEX*16, TARGET :: VPRN_F(*) ! cf note

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_zy4.fc'

      INTEGER IERR
      INTEGER IN
      INTEGER IPV, IPH
      COMPLEX*16 BATHY, EPAIGL, PROF
      COMPLEX*16 PRFE, PRFA
      COMPLEX*16, POINTER :: VPRN(:,:)
      COMPLEX*16, POINTER :: VDLE(:,:)
      TYPE (SV2D_XBS_T), POINTER :: SELF
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),     POINTER :: IPRN
      TYPE (SV2D_XPRG_T),     POINTER :: XPRG

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
C-----------------------------------------------------------------------

C---     Récupère les données
      SELF => SV2D_XBS_REQSELF(HOBJ)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IPRN => SELF%IPRN
      XPRG => SELF%XPRG

C---     Reshape the arrays
      VDLE(1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_F(1:EDTA%NDLN*GDTA%NNELV)
      VPRN(1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_F(1:EDTA%NPRNO*GDTA%NNELV)
      
C---     Indices dans VPRNO
      IPV = IPRN%V
      IPH = IPRN%H

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2

C---     Profondeurs effective et absolue
      DO IN=1,GDTA%NNELS
         BATHY   = VPRN(IPRN%Z,IN)
         EPAIGL  = 0.9D0 * VPRN(IPRN%ICE_E,IN)
         PROF    = VDLE(3,IN) - BATHY
         EPAIGL  = MIN(EPAIGL, PROF)
         EPAIGL  = MAX(EPAIGL, ZERO)
         PRFE    = PROF - EPAIGL                ! Prof effective
         PRFA    = MAX(PRFE, XPRG%DECOU_HMIN)   ! Prof absolue
         VPRN(IPRN%V,IN) = PRFE           ! Prof effective temp.
         VPRN(IPRN%H,IN) = PRFA           ! Prof absolue
      ENDDO

C---     Impose les profondeurs sur les noeuds milieux
      VPRN(IPV,NO2) = (VPRN(IPV,NO1) + VPRN(IPV,NO3))*UN_2

C        Profondeur linéaire
C        Noeud rehaussé si un noeud sommet est découvert
      VPRN(IPH,NO2) = (VPRN(IPH,NO1) + VPRN(IPH,NO3))*UN_2

C---     Boucle sur les noeuds
      DO IN=1,GDTA%NNELS
         IERR = SV2D_ZY4_CLCPRN_1N(VPRN(1:,IN), VDLE(1:,IN), IPRN, XPRG)
      ENDDO

      SV2D_ZY4_CLCPRNES = ERR_TYP()
      RETURN
      END

