C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_gy4.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
C
C Notes:
C  Les éléments sont des classes avec CTR et DTR. 2 cas de figures:
C  1. Statique: ils enregistrent les fonctions dans 000 et les appels
C     se font avec HOBJ comme premier paramètre.
C  2. Dynamique: ils enregistrent les méthodes.
C
C Functions:
C   Public:
C     INTEGER SV2D_GY4_000
C     INTEGER SV2D_GY4_999
C     INTEGER SV2D_GY4_CTR
C     INTEGER SV2D_GY4_DTR
C     INTEGER SV2D_GY4_INI
C     INTEGER SV2D_GY4_RST
C     INTEGER SV2D_GY4_REQHBASE
C     LOGICAL SV2D_GY4_HVALIDE
C   Private:
C     SUBROUTINE SV2D_GY4_REQSELF
C     INTEGER SV2D_GY4_INIVTBL
C     INTEGER SV2D_GY4_INIPRMS
C
C************************************************************************

      MODULE SV2D_GY4_M

      USE SV2D_XY4_M
      IMPLICIT NONE

      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: SV2D_GY4_HBASE = 0

C---     Attributs privés
      TYPE, EXTENDS(SV2D_XY4_SELF_T) :: SV2D_GY4_SELF_T
!        pass
      END TYPE SV2D_GY4_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction protégée SV2D_GY4_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La méthode est virtuelle dans le sens ou elle fonctionne
C     pour un objet qui a hérité de LM_ELEM. Par contre, on ne
C     peut pas contrôler le type.
C************************************************************************
      FUNCTION SV2D_GY4_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (SV2D_GY4_SELF_T), POINTER :: SV2D_GY4_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (SV2D_GY4_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_GOOD())
C------------------------------------------------------------------------

      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL C_F_POINTER(CELF, SELF)

      SV2D_GY4_REQSELF => SELF
      RETURN
      END FUNCTION SV2D_GY4_REQSELF

      END MODULE SV2D_GY4_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_000
CDEC$ ENDIF

      USE SV2D_GY4_M
      IMPLICIT NONE

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(SV2D_GY4_HBASE,
     &       'Finite Element Saint-Venant 2D - Y4 - GPU')

      SV2D_GY4_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_999
CDEC$ ENDIF

      USE SV2D_GY4_M
      IMPLICIT NONE

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER  IERR
      EXTERNAL SV2D_GY4_DTR
C------------------------------------------------------------------------

      IF (SV2D_GY4_HBASE .NE. 0) THEN
         IERR = OB_OBJN_999(SV2D_GY4_HBASE, SV2D_GY4_DTR)
         SV2D_GY4_HBASE = 0
      ENDIF

      SV2D_GY4_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La classe est concrète, HKID peut ne pas exister.
C************************************************************************
      FUNCTION SV2D_GY4_CTR(HOBJ, HKID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_CTR
CDEC$ ENDIF

      USE SV2D_GY4_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HKID

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_lc.fi'
      INCLUDE 'lmutil.fi'
      INCLUDE 'sv2d_xy4.fi'
      INCLUDE 'sv2d_gy4.fc'

      INTEGER IERR, IRET
      INTEGER DOOWN
      INTEGER HELEM, HGEOM
      TYPE (SV2D_GY4_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF
C------------------------------------------------------------------------
!!D      CALL ERR_ASR(OB_OBJN_REQDTA(HKID, CELF) .EQ. ERR_OK)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      DOOWN = F_LC_L2I(.FALSE.)
      IF (HKID .NE. 0) THEN
         IERR = OB_OBJN_REQDTA(HKID, CELF)
         IF (ERR_GOOD()) SELF => SV2D_GY4_REQSELF(HKID)
      ENDIF
      IF (.NOT. ASSOCIATED(SELF)) THEN
         CALL ERR_RESET()
         ALLOCATE (SELF, STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         DOOWN = F_LC_L2I(ASSOCIATED(SELF))
      ENDIF

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   SV2D_GY4_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
!!!      IF (ERR_GOOD()) IERR = SV2D_GY4_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. SV2D_GY4_HVALIDE(HOBJ))

C---     Construis le parent
      HELEM = 0
      IF (ERR_GOOD()) IERR = SV2D_XY4_CTR(HELEM, HOBJ)

C---     Remplis la table virtuelle
      IF (ERR_GOOD()) IERR = SV2D_GY4_INIVTBL(HOBJ)

C---     Push les attributs dans la table virtuelle
      HGEOM = 0
      IF (ERR_GOOD()) HGEOM = LM_UTIL_REQxxx(HELEM, IX_GEO)
      IF (ERR_GOOD()) IERR = LM_UTIL_AJTxxx(HOBJ, IX_KID, HKID)
      IF (ERR_GOOD()) IERR = LM_UTIL_AJTxxx(HOBJ, IX_ELE, HELEM)
      IF (ERR_GOOD()) IERR = LM_UTIL_AJTxxx(HOBJ, IX_GEO, HGEOM)
      IF (ERR_GOOD()) IERR = LM_UTIL_AJTxxx(HOBJ, IX_DEL, DOOWN)

C---     Initialise les dimensions
      IF (ERR_GOOD()) IERR = SV2D_GY4_INIPRMS(HOBJ)

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      SV2D_GY4_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_DTR
CDEC$ ENDIF

      USE SV2D_GY4_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_lc.fi'
      INCLUDE 'lmutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'sv2d_xy4.fi'

      INTEGER IERR
      INTEGER HPRNT
      TYPE (SV2D_GY4_SELF_T), POINTER :: SELF
      LOGICAL DOOWN
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_GY4_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK
      HPRNT = 0
      DOOWN = .FALSE.
      SELF => NULL()

C---     Les attributs
      SELF => SV2D_GY4_REQSELF(HOBJ)
      IF (OB_VTBL_HVALIDE(SELF%HVTBL)) THEN
         HPRNT = LM_UTIL_REQxxx(HOBJ, IX_ELE)
         DOOWN = F_LC_I2L(LM_UTIL_REQxxx(HOBJ, IX_DEL))
      ENDIF

C---     Reset l'objet
      IERR = SV2D_GY4_RST(HOBJ)

C---     Détruis le parent
      IF (SV2D_XY4_HVALIDE(HPRNT)) IERR = SV2D_XY4_DTR(HPRNT)

C---     Efface du registre
      IERR = OB_OBJN_DTR(HOBJ, SV2D_GY4_HBASE)
      HOBJ = 0

C---     Désalloue la structure
      IF (DOOWN) THEN
         IF (ASSOCIATED(SELF)) THEN
            DEALLOCATE(SELF)
         ENDIF
      ENDIF

      SV2D_GY4_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_INI(HOBJ,
     &                      HCONF,
     &                      HGRID,
     &                      HDLIB,
     &                      HCLIM,
     &                      HSOLC,
     &                      HSOLR,
     &                      HPRGL,
     &                      HPRNO,
     &                      HPREV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCONF
      INTEGER HGRID
      INTEGER HDLIB
      INTEGER HCLIM
      INTEGER HSOLC
      INTEGER HSOLR
      INTEGER HPRGL
      INTEGER HPRNO
      INTEGER HPREV

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmutil.fi'
      INCLUDE 'sv2d_xy4.fi'

      INTEGER  IERR
      INTEGER  HPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_GY4_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SV2D_GY4_RST(HOBJ)

C---     Récupère les attributs
      HPRNT = 0
      IF (ERR_GOOD()) THEN
         HPRNT = LM_UTIL_REQxxx(HOBJ, IX_ELE)
D        CALL ERR_ASR(SV2D_XY4_HVALIDE (HPRNT))
      ENDIF

C---     Initialise le parent
      IF (ERR_GOOD()) IERR = SV2D_XY4_INI(HPRNT,
     &                                    HCONF,
     &                                    HGRID,
     &                                    HDLIB,
     &                                    HCLIM,
     &                                    HSOLC,
     &                                    HSOLR,
     &                                    HPRGL,
     &                                    HPRNO,
     &                                    HPREV)

      SV2D_GY4_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_GY4_INIVTBL
C
C Description:
C     La fonction privée SV2D_GY4_INIVTBL initialise la table virtuelle pour
C     la classe.
C
C Entrée:
C     INTEGER HT        Handle sur la table virtuelle
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_INIVTBL(HOBJ)

      USE SV2D_GY4_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmfcod.fi'
      INCLUDE 'sv2d_fnct.fi'
      INCLUDE 'sv2d_xbs.fi'
      INCLUDE 'sv2d_xbs.fc'
      INCLUDE 'sv2d_gy4.fc'

      INTEGER I
      INTEGER HT, HO
      TYPE (SV2D_GY4_SELF_T), POINTER :: SELF

      EXTERNAL SV2D_XBS_DUMMY
      EXTERNAL SV2D_XBS_EMPTY
      EXTERNAL SV2D_XBS_ASMRES_G
      EXTERNAL SV2D_GY4_PRCPRGL
      EXTERNAL SV2D_GY4_PSLPRGL
      EXTERNAL SV2D_GY4_PSLPRNO
      
      CHARACTER*(4), PARAMETER :: DLL = 'sv2d'
C-----------------------------------------------------------------------
      INTEGER AJT
      INTEGER ID
      CHARACTER*(16) NF
      AJT(ID,NF)=OB_VTBL_AJTMSO(HT, ID, HO, DLL, NF(1:SP_STRN_LEN(NF)))
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_GY4_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Les attributs
      SELF => SV2D_GY4_REQSELF(HOBJ)
      HT = SELF%HVTBL
      HO = HOBJ
D     CALL ERR_ASR(OB_VTBL_HVALIDE(HT))

C---     Méthodes virtuelles de gestion
      I = AJT(LM_VTBL_FNC_REQHVALD,'SV2D_GY4_HVALIDE')

C---     Méthodes de calcul
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_ASMF,    HO, SV2D_XBS_DUMMY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_ASMK,    HO, SV2D_XBS_DUMMY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_ASMKT,   HO, SV2D_XBS_DUMMY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_ASMKU,   HO, SV2D_XBS_DUMMY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_ASMM,    HO, SV2D_XBS_DUMMY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_ASMMU,   HO, SV2D_XBS_DUMMY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_ASMRES,  HO, SV2D_XBS_ASMRES_G)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_CLCCLIM, HO, SV2D_XBS_EMPTY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_CLCDLIB, HO, SV2D_XBS_EMPTY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_CLCPRES, HO, SV2D_XBS_EMPTY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_CLCPREV, HO, SV2D_XBS_EMPTY)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_CLCPRNO, HO, SV2D_XBS_EMPTY)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRCCLIM, HO, SV2D_XBS_PRCCLIM)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRCDLIB, HO, SV2D_XBS_PRCDLIB)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRCPRES, HO, SV2D_XBS_PRCPRES)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRCPREV, HO, SV2D_XBS_PRCPREV)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRCPRGL, HO, SV2D_GY4_PRCPRGL)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRCPRNO, HO, SV2D_XBS_PRCPRNO)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRCSOLC, HO, SV2D_XBS_PRCSOLC)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRCSOLR, HO, SV2D_XBS_PRCSOLR)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_HLPCLIM, HO, SV2D_XBS_HLPCLIM)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_HLPPRGL, HO, SV2D_XBS_HLPPRGL)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_HLPPRNO, HO, SV2D_XBS_HLPPRNO)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRNCLIM, HO, SV2D_XBS_PRNCLIM)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRNPRGL, HO, SV2D_XBS_PRNPRGL)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PRNPRNO, HO, SV2D_XBS_PRNPRNO)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PSCPRNO, HO, SV2D_XBS_PSCPRNO)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PSLCLIM, HO, SV2D_XBS_PSLCLIM)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PSLPREV, HO, SV2D_XBS_PSLPREV)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PSLPRGL, HO, SV2D_GY4_PSLPRGL)
      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PSLPRNO, HO, SV2D_GY4_PSLPRNO)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PSLSOLC, HO, SV2D_XBS_PSLSOLC)
!      I = OB_VTBL_AJTMTH(HT, LM_VTBL_FNC_PSLSOLR, HO, SV2D_XBS_PSLSOLR)
      
C---     Fonctions virtuelles à définir par les héritiers
      I = OB_VTBL_AJTMTH(HT, SV2D_VT_CLCPRNEV, HO, SV2D_XBS_DUMMY)
      I = OB_VTBL_AJTMTH(HT, SV2D_VT_CLCPRNES, HO, SV2D_XBS_DUMMY)
      I = OB_VTBL_AJTMTH(HT, SV2D_VT_CLCPREVE, HO, SV2D_XBS_DUMMY)
      I = OB_VTBL_AJTMTH(HT, SV2D_VT_CLCPRESE, HO, SV2D_XBS_DUMMY)

      SV2D_GY4_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée SV2D_GY4_INIPRMS initialise les paramètres de
C     dimension de l'objet.
C     Vide, ne rajoute rien au parent.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_INIPRMS(HOBJ)

      USE SV2D_GY4_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_gy4.fc'

      INTEGER IERR
      TYPE (LM_EDTA_T), POINTER :: ELEM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_GY4_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les données
      ELEM => SV2D_XBS_REQEDTA(HOBJ)

C---     Assigne les dimensions
      ELEM%NPRGLL = 19                 ! NB DE PROPRIÉTÉS GLOBALES LUES
      ELEM%NPRGL  = ELEM%NPRGLL        ! NB DE PROPRIÉTÉS GLOBALES
      ELEM%NPRNO  =  6                 ! NB DE PROPRIÉTÉS NODALES
      ELEM%NPRNOC =  7                 ! NB DE PROPRIÉTÉS NODALES CALCULÉES
      ELEM%NPREV  =  0                 ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME
      ELEM%NPRES  =  0                 ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE SURFACE

      SV2D_GY4_INIPRMS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_RST
CDEC$ ENDIF

      USE SV2D_GY4_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmutil.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'sv2d_xy4.fi'

      INTEGER IERR
      INTEGER HPRNT
      TYPE (SV2D_GY4_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_GY4_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK
      HPRNT = 0

C---     Récupère les attributs
      SELF => SV2D_GY4_REQSELF(HOBJ)
      IF (OB_VTBL_HVALIDE(SELF%HVTBL)) THEN
         HPRNT = LM_UTIL_REQxxx(HOBJ, IX_ELE)
D        CALL ERR_ASR(SV2D_XY4_HVALIDE (HPRNT))
      ENDIF

C---     Reset le parent
      IF (SV2D_XY4_HVALIDE(HPRNT)) IERR = SV2D_XY4_RST(HPRNT)

      SV2D_GY4_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SV2D_GY4_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_REQHBASE
CDEC$ ENDIF

      USE SV2D_GY4_M
      IMPLICIT NONE

      INCLUDE 'sv2d_gy4.fi'
C------------------------------------------------------------------------

      SV2D_GY4_REQHBASE = SV2D_GY4_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SV2D_GY4_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_HVALIDE
CDEC$ ENDIF

      USE SV2D_GY4_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfcod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'obvtbl.fi'

      INTEGER IERR
      INTEGER HMETH
      LOGICAL HVALIDE, TVALIDE
      TYPE (SV2D_GY4_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      HVALIDE = .FALSE.
      TVALIDE = OB_OBJN_TVALIDE(HOBJ, SV2D_GY4_HBASE)
      IF (TVALIDE) THEN
         HVALIDE = OB_OBJN_HVALIDE(HOBJ, SV2D_GY4_HBASE)
      ELSEIF (HOBJ .NE. 0) THEN
         HMETH = 0
         SELF => SV2D_GY4_REQSELF(HOBJ)
         IF (ERR_GOOD()) IERR = OB_VTBL_REQMTH (SELF%HVTBL,
     &                                          LM_VTBL_FNC_REQHVALD,
     &                                          HMETH)
         IF (ERR_GOOD()) HVALIDE = (SO_FUNC_CALL0(HMETH) .NE. 0)
      ENDIF

      SV2D_GY4_HVALIDE = HVALIDE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_GY4_REQNBPE retourne le nombre de bytes par élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_REQNBPE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_REQNBPE
CDEC$ ENDIF

      USE SV2D_GY4_M
      IMPLICIT NONE

      INCLUDE 'sv2d_gy4.fi'
      
      INTEGER N8

      INTEGER, PARAMETER :: NNEL = 6
      INTEGER, PARAMETER :: NDLN = 3
      INTEGER, PARAMETER :: NPRN = 6
      INTEGER, PARAMETER :: NDJV = 5
      INTEGER, PARAMETER :: NPRE = 8
C------------------------------------------------------------------------

      N8 = NNEL * (NDLN +   ! VRES
     &             NDLN +   ! VDLG
     &             NPRN)    ! VPRN
     &  +  NDJV + NPRE 
      
      SV2D_GY4_REQNBPE = N8*8
      RETURN
      END

