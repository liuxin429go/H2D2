C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Numérotation
C Objet:   Algorithme de partitionnement pt-ScoTCH
C Type:    Concret
C Note:
C     Structure de la table de redistribution locale: KDISTR(NPROC+2, NNL)
C        NNO_PR1 NNO_PR2 ...  NNO_PRn NNO_Glb IPROC   ! .LE. 0 si absent
C
C Functions:
C   Public:
C     INTEGER NM_STCH_000
C     INTEGER NM_STCH_999
C     INTEGER NM_STCH_CTR
C     INTEGER NM_STCH_DTR
C     INTEGER NM_STCH_INI
C     INTEGER NM_STCH_RST
C     INTEGER NM_STCH_REQHBASE
C     LOGICAL NM_STCH_HVALIDE
C     INTEGER NM_STCH_PART
C     INTEGER NM_STCH_RENUM
C     INTEGER NM_STCH_GENNUM
C     INTEGER NM_STCH_SAUVE
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_STCH_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmstch.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmstch.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(NM_STCH_NOBJMAX,
     &                   NM_STCH_HBASE,
     &                   'Scotch renumbering')

      NM_STCH_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_STCH_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmstch.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmstch.fc'

      INTEGER  IERR
      EXTERNAL NM_STCH_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(NM_STCH_NOBJMAX,
     &                   NM_STCH_HBASE,
     &                   NM_STCH_DTR)

      NM_STCH_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_STCH_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmstch.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmstch.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   NM_STCH_NOBJMAX,
     &                   NM_STCH_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NM_STCH_HVALIDE(HOBJ))
         IOB = HOBJ - NM_STCH_HBASE

         NM_STCH_HPRNT(IOB) = 0
         NM_STCH_HFPRT(IOB) = 0
         NM_STCH_HFRNM(IOB) = 0
      ENDIF

      NM_STCH_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_STCH_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmstch.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmstch.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = NM_STCH_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   NM_STCH_NOBJMAX,
     &                   NM_STCH_HBASE)

      NM_STCH_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_STCH_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmstch.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_scotch.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'nmstch.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HFPRT, HFRNM

      EXTERNAL C_PSCTCH_PART
      EXTERNAL C_PSCTCH_RENUM
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = NM_STCH_RST(HOBJ)

C---     CONSTRUIS LES FONCTIONS
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFPRT)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFPRT, C_PSCTCH_PART)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFRNM)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFRNM, C_PSCTCH_RENUM)

C---     CONSTRUIS LE PARENT
      IF (ERR_GOOD()) IERR = NM_NBSE_CTR   (HPRNT)
      IF (ERR_GOOD()) IERR = NM_NBSE_INI   (HPRNT, HFPRT, HFRNM)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - NM_STCH_HBASE
         NM_STCH_HPRNT(IOB) = HPRNT
         NM_STCH_HFPRT(IOB) = HFPRT
         NM_STCH_HFRNM(IOB) = HFRNM
      ENDIF

      NM_STCH_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_STCH_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmstch.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'nmstch.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HFPRT, HFRNM
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_STCH_HBASE
      HPRNT = NM_STCH_HPRNT(IOB)
      HFPRT = NM_STCH_HFPRT(IOB)
      HFRNM = NM_STCH_HFRNM(IOB)

C---     DETRUIS LES ATTRIBUTS
      IF (SO_FUNC_HVALIDE(HFRNM)) IERR = SO_FUNC_DTR(HFRNM)
      IF (SO_FUNC_HVALIDE(HFPRT)) IERR = SO_FUNC_DTR(HFPRT)
      IF (NM_NBSE_HVALIDE(HPRNT)) IERR = NM_NBSE_DTR(HPRNT)

C---     RESET
      NM_STCH_HPRNT(IOB) = 0
      NM_STCH_HFPRT(IOB) = 0
      NM_STCH_HFRNM(IOB) = 0

      NM_STCH_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_STCH_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_STCH_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmstch.fi'
      INCLUDE 'nmstch.fc'
C------------------------------------------------------------------------

      NM_STCH_REQHBASE = NM_STCH_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_STCH_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_STCH_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmstch.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmstch.fc'
C------------------------------------------------------------------------

      NM_STCH_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  NM_STCH_NOBJMAX,
     &                                  NM_STCH_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Partitionne un maillage.
C
C Description:
C     La fonction NM_STCH_PART partitionne le maillage donné par HELEM en NPROC
C     sous-domaines. Si NPROC .LE. 0, alors on partitionne pour le nombre
C     de process dans le groupe MPI (cluster).
C     <p>
C     La partition n'est pas utilisable immédiatement car il n'y a pas
C     redistribution des données entre les process. La redistribution a lieu au
C     chargement des données. La partition doit donc être sauvée puis chargée.
C     Le maillage devra également être relu.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HELEM       Handle sur les connectivités du maillage à partitionner
C     NPART       Nombre de sous-domaines demandés
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_STCH_PART (HOBJ, HELEM, NPART)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_PART
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM
      INTEGER NPART

      INCLUDE 'c_scotch.fi'
      INCLUDE 'nmstch.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'nmstch.fc'

      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER HPRNT
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.scotch'

C---     En-tête de commande
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_CMD_PTSCOTCH_PART')
      CALL LOG_INCIND()

C---     Log self
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', ': ', TXT(1:LTXT)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Récupère les attributs
      IOB = HOBJ - NM_STCH_HBASE
      HPRNT = NM_STCH_HPRNT(IOB)

C---     Appel le parent
      IERR  = NM_NBSE_PART(HPRNT, HELEM, NPART)

C---     Vérifie l'erreur et assigne le bon message
      IF (ERR_BAD()) THEN
         IRET = C_PSCTCH_ERRMSG(ERR_BUF)
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      CALL LOG_DECIND()
      NM_STCH_PART = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Renumérote un maillage.
C
C Description:
C     La fonction NM_STCH_RENUM renumérote le maillage HELEM afin d'en
C     réduire la largeur de bande.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HELEM       Handle sur les connectivités du maillage à partitionner
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_STCH_RENUM (HOBJ, HELEM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_RENUM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM

      INCLUDE 'nmstch.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'nmstch.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.scotch'

C---     En-tête de commande
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_CMD_PTSCOTCH_RENUM')
      CALL LOG_INCIND()

C---     Log self      
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', ': ', TXT(1:LTXT)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Récupère les attributs
      IOB = HOBJ - NM_STCH_HBASE
      HPRNT = NM_STCH_HPRNT(IOB)

C---     Appel le parent
      IERR  = NM_NBSE_RENUM(HPRNT, HELEM)

      CALL LOG_DECIND()
      NM_STCH_RENUM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Construis une renumérotation.
C
C Description:
C     La fonction NM_STCH_GENNUM retourne un renumérotation globale
C     nouvellement créée à partir des données de l'objet. Il est de
C     la responsabilité de l'utilisateur de disposer de HNUM.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     HNUM        Handle sur la renumérotation
C
C Notes:
C************************************************************************
      FUNCTION NM_STCH_GENNUM (HOBJ, HNUM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_GENNUM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUM

      INCLUDE 'nmstch.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'nmstch.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.scotch'

C---     En-tête de commande
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_CMD_PTSCOTCH_GENNUM')
      CALL LOG_INCIND()

C---     Log self
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', ': ', TXT(1:LTXT)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Récupère les attributs
      IOB = HOBJ - NM_STCH_HBASE
      HPRNT = NM_STCH_HPRNT(IOB)

C---     Appel le parent
      IERR  = NM_NBSE_GENNUM(HPRNT, HNUM)

      CALL LOG_DECIND()
      NM_STCH_GENNUM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris la table de redistribution.
C
C Description:
C     La fonction NM_STCH_SAUVE écris la table de redistribution dans le
C     fichier NOMFIC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_STCH_SAUVE (HOBJ, NOMFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_STCH_SAUVE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC

      INCLUDE 'nmstch.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'nmstch.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.scotch'

C---     En-tête de commande
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_CMD_PTSCOTCH_SAUVE')
      CALL LOG_INCIND()

C---     Log self
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', ': ', TXT(1:LTXT)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Récupère les attributs
      IOB = HOBJ - NM_STCH_HBASE
      HPRNT = NM_STCH_HPRNT(IOB)

C---     Appel le parent
      IERR  = NM_NBSE_SAUVE(HPRNT, NOMFIC)

      CALL LOG_DECIND()
      NM_STCH_SAUVE = ERR_TYP()
      RETURN
      END
