C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_GOP12L
C         FTN (Sub)Module: LM_GOP12L_ASMESCL_M
C            Public:
C               MODULE INTEGER LM_GOP12L_ASMESCL
C            Private:
C               INTEGER LM_GOP12L_ASMESCL_0
C
C************************************************************************

      SUBMODULE(LM_GOP12L_M) LM_GOP12L_ASMESCL_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire:  LM_GOP12L_ASMESCL
C
C Description:
C     La fonction LM_GOP12L_ASMESCL assemble les éléments de surface
C     des conditions limites.
C
C Entrée(et sorties)
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_GOP12L_ASMESCL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOP12L_ASMESCL
CDEC$ ENDIF

      CLASS(LM_GOP12L_T), INTENT(INOUT), TARGET :: SELF
      INTEGER HOBJ

      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER IERR
      INTEGER ICL, IEL
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA =>SELF%GDTA
D     CALL ERR_PRE(GDTA%NNL   .GE. 6)
D     CALL ERR_PRE(GDTA%NCELS .EQ. 6)

C---     Initialise les distances
      GDTA%VCLDST(1:GDTA%NCLNOD) = 0.0D0

C---     Boucle sur les limites
      DO ICL=1, GDTA%NCLLIM

C---        Détermine les éléments de la limite
         IF (ERR_GOOD()) IERR = LM_GOP12L_ASMESCL_0(ICL, GDTA)

      ENDDO

      LM_GOP12L_ASMESCL = ERR_TYP()
      RETURN
      END FUNCTION LM_GOP12L_ASMESCL

C************************************************************************
C Sommaire:  LM_GOT6L_ASMESCL_0
C
C Description:
C     La fonction privée LM_GOT6L_ASMESCL_0 extrait les éléments de la
C     limite ICL en se basant sur les noeuds de cette limite.
C
C Entrée:
C
C Sortie:
C     KCLELE      Table des éléments des limites
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOP12L_ASMESCL_0(ICL, GDTA)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      INTEGER, INTENT(IN) :: ICL
      TYPE (LM_GDTA_T), INTENT(INOUT) :: GDTA

      INCLUDE 'lmgop12l.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IEP, IE, IN
      INTEGER INOCLINF, INOCLSUP
      INTEGER IELCLINF
      INTEGER IELE
      INTEGER NHIT
      INTEGER NP1, NP2, NP3
C-----------------------------------------------------------------------

      CALL LOG_TODO('LM_GOP12L_ASMESCL jamais teste - meme pas le 2d' //
     &              ', PEUT-ETRE OK!!')

      IF (ICL .EQ. 1) THEN
         IELE = 0
      ELSE
         IELE = GDTA%KCLLIM(6,ICL-1)
      ENDIF

      GDTA%KCLLIM(5,ICL) = IELE + 1

      INOCLINF = GDTA%KCLLIM(3,ICL)
      INOCLSUP = GDTA%KCLLIM(4,ICL)
      IELCLINF = GDTA%KCLLIM(5,ICL)

C---     Boucle sur les éléments de surface
      DO IEP=1,GDTA%NELLS
         NP1 = GDTA%KNGS(1,IEP)
         NP2 = GDTA%KNGS(2,IEP)
         NP3 = GDTA%KNGS(3,IEP)

C---        Cherche les noeuds
         NHIT = 0
         DO IN=INOCLINF,INOCLSUP
            IF (GDTA%KCLNOD(IN) .EQ. NP1) NHIT=NHIT + 1
            IF (GDTA%KCLNOD(IN) .EQ. NP2) NHIT=NHIT + 1
            IF (GDTA%KCLNOD(IN) .EQ. NP3) NHIT=NHIT + 1
            IF (NHIT .EQ. 3) GOTO 189
         ENDDO
189      CONTINUE
         IF (NHIT .NE. 3) GOTO 199

C---        Contrôle les dédoublements
         DO IE=IELCLINF, IELE
            IF (GDTA%KCLELE(IE) .EQ. IEP) GOTO 9900
         ENDDO

C---        Ajoute l'élément de limite
         IELE = IELE + 1
         GDTA%KCLELE(IELE) = IEP

199      CONTINUE
      ENDDO

      GDTA%KCLLIM(6,ICL) = IELE
      GDTA%KCLLIM(7,ICL) = EG_TPLMT_INDEFINI

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_MSG, '(A)') 'ERR_ELEMENT_LIMITE_DOUBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LM_GOP12L_ASMESCL_0 = ERR_TYP()
      RETURN
      END FUNCTION LM_GOP12L_ASMESCL_0

      END SUBMODULE LM_GOP12L_ASMESCL_M
