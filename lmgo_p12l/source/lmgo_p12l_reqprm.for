C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Élément:
C************************************************************************

C************************************************************************
C Sommaire: Retourne les paramètres de l'élément.
C
C Description:
C     La fonction <code>LMGO_P12L_REQPRM</code> retourne tous les
C     paramètres caractéristiques de l'élément.
C
C Entrée:
C
C Sortie:
C           ITPEV :
C           ITPES :
C           NDIM :
C           NNELV :
C           NNELS :
C           NCELV :
C           NCELS :
C           NDJV :
C           NDJS :
C
C Notes:
C      NNELV doit être égal à NNELV de l'élément géométrique. C'est une
C            condition dans GR_ELEM_CTRLFRML. Ce nombre vient de ce qui
C            est écrit dans le .ele.
C      NDIM  assigné à EG_FRML_NDIM lequel est assigné à EG_CMMN_NDIM.
C            mais jamais utilisé car EG_CMMN_NDIM est ensuite réassigné
C            à partir du fichier .cor. C'est ce dernier qui domine les
C            dim. A noter que NDIM sert (seulement) à déterminer la grandeur
C            de VCORG.
C
C************************************************************************
      SUBROUTINE LMGO_P12L_REQPRM(NDIM,
     &                            ITPEV, NNELV, NCELV, NDJV,
     &                            ITPES, NNELS, NCELS, NDJS,
     &                            ITPEZ, NNELZ, NCELZ, NDJZ, NEV2EZ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_P12L_REQPRM
CDEC$ ENDIF

      INTEGER NDIM
      INTEGER ITPEV, NNELV, NCELV, NDJV
      INTEGER ITPES, NNELS, NCELS, NDJS
      INTEGER ITPEZ, NNELZ, NCELZ, NDJZ, NEV2EZ

      INCLUDE 'egtpgeo.fi'
C-----------------------------------------------------------------------

      NDIM   =  3              ! NB DE DIMENSIONS

C---     Volume
      ITPEV  =  EG_TPGEO_P12L  ! TYPE DE L'ELEMENT DE VOLUME
      NNELV  =  6              ! NB DE NOEUDS PAR ÉLÉMENT DE VOLUME
      NCELV  =  NNELV          ! NB DE CONNECTIVITÉS PAR ÉLÉMENT DE VOLUME
      NDJV   =  5              ! NB DE MÉTRIQUES PAR ÉLÉMENT DE VOLUME INDÉPENDANTES DE LA SOLUTION
                               ! variables : ksi,x | eta,x | zeta,x | ksi,y | eta,y | zeta,x | zeta,y | zeta,z | det2d | detJ |
                               ! 2D?           o   |  o    |   n    |   o   |   o   |   o= 0 |   o=0  |    n   |   o   |   n  |
                               ! ce sont les variables du NDJV 2D
C---     Surface
      ITPES  =  EG_TPGEO_L3L   ! TYPE DE L'ELEMENT DE SURFACE ASSOCIÉ
      NNELS  =  3              ! NB DE NOEUDS PAR ÉLÉMENT DE SURFACE
      NCELS  =  NNELS + 2      ! NB DE CONNECTIVITÉS PAR ÉLÉMENT DE SURFACE : 2 + element parent + côté
      NDJS   =  3              ! NB DE MÉTRIQUES PAR ÉLÉMENT DE SURFACE A MODIFIER
C---     Split
      ITPEZ  =  EG_TPGEO_INDEFINI    ! TYPE DE L'ELEMENT SPLIT
      NNELZ  =  0              ! NB DE NOEUDS
      NCELZ  =  NNELZ          ! NB DE CONNECTIVITÉS
      NDJZ   =  0              ! NB DE MÉTRIQUES
      NEV2EZ =  0              ! NB ELE VOL à ELE SPLIT

      RETURN
      END

