C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_P12L_CLCJELV
C
C Description:
C     La fonction LMGO_P12L_CLCJELV calcule les métriques 2D et indépendantes
C     du point d'intégration pour des éléments de volume de type P12L.
C     C'est la fonction LMGO_P12L_EVAJELV qui calcule les métriques en un point,
C     à partir des valeurs pré-calculées ici.
C
C Entrée:
C     NDIM                 Nombre de DIMension des coordonnées des noeuds = 2
C     NNT                  Nombre de Noeud Total
C     NCELV                Nombre de Connectivité par ELéments de Volume
C     NELV                 Nombre d'ELéments de Volume
C     NDJV                 Nombre de métrique par élément de volume pouvant être calculées indép. de la solution  =5
C     VCORG(NDIM,  NNT)    Table des COoRdonnées Globales
C     KNGV (NCELV, NELV)   Table des coNectivités Globales de Volume
C
C Sortie:
C     VDJV (NDJV,  NELV)   Table des métriques des éléments de volume
C                          pouvant être calculées indépendemment de la solution
C                          ce sont les métriques 2D
C
C     métrique : ksi,x | eta,x | zeta,x | ksi,y | eta,y | zeta,x | zeta,y | zeta,z | det2d | detJ |
C       2D?        o   |  o    |   n    |   o   |   o   |   o= 0 |   o=0  |    n   |   o   |   n  |
C     Seules les métriques 2D sont calculées.
C
C Notes:
C    3. Voir métriques.ps pour connaître la matrice jacobienne et les autres métriques
C    4. Puisque c'est du calcul 2D est-ce que ça ne relève pas plutôt de la surface ??
C    5. Les vrais valeurs de ksi,x eta,x ksi,y et eta,y sont retrouvées en divisant
C       par detJ2D. Un detJ2D nul cause problème. L'élément est dégénéré.
C    6. Il faudrait diviser ici par le detJ pour avoir directement les vrais
C       métriques. Avant de modifier, il faut s'assurer de toutes les utilisations
C       des "fausses métriques", entre autre comme normale aux côtés des éléments.
C************************************************************************
      FUNCTION LMGO_P12L_CLCJELV(NDIM,
     &                           NNT,
     &                           NCELV,
     &                           NELV,
     &                           NDJV,
     &                           VCORG,
     &                           KNGV,
     &                           VDJV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_P12L_CLCJELV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDIM
      INTEGER NNT
      INTEGER NCELV
      INTEGER NELV
      INTEGER NDJV
      REAL*8  VCORG(NDIM,  NNT)
      INTEGER KNGV (NCELV, NELV)
      REAL*8  VDJV (NDJV,  NELV)

      INCLUDE 'lmgo_p12l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IE, IN
      INTEGER NO1, NO2, NO3

      REAL*8 ZERO
      PARAMETER (ZERO = 0.0D0)

C--- Voir lmgo_p12l_reqprm.for
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDIM  .EQ. 2)
D     CALL ERR_PRE(NNT   .GE. 3)
D     CALL ERR_PRE(NCELV .EQ. 6)
D     CALL ERR_PRE(NDJV  .EQ. 5)
C-----------------------------------------------------------------------

C---     BOUCLE SUR LES ÉLÉMENTS
      DO IE=1,NELV
         NO1  = KNGV(1,IE)
         NO2  = KNGV(3,IE)
         NO3  = KNGV(5,IE)

C---        CALCUL DES MÉTRIQUES 2d
         VDJV(1,IE) = VCORG(2,NO3) - VCORG(2,NO1)                   ! Ksi,x
         VDJV(2,IE) = VCORG(2,NO1) - VCORG(2,NO2)                   ! Eta,x
         VDJV(3,IE) = VCORG(1,NO1) - VCORG(1,NO3)                   ! Ksi,y
         VDJV(4,IE) = VCORG(1,NO2) - VCORG(1,NO1)                   ! Eta,y
         VDJV(5,IE) = VDJV(1,IE)*VDJV(4,IE) - VDJV(2,IE)*VDJV(3,IE) ! DetJ2D

C---        IMPRIME LES ELEMENTS A DETERMINANT NEGATIF
         IF (VDJV(5,IE) .LE. ZERO) THEN
            WRITE(ERR_BUF, '(A)') 'ERR_DETJ2D_NEGATIF'
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
            WRITE(LOG_BUF, '(A,I9,A,16I9)')
     &         'ERR_DETJ2D_NEGATIF:', IE, '--', (KNGV(IN,IE),IN=1,NCELV)
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF

      ENDDO

      LMGO_P12L_CLCJELV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  LMGO_P12L_EVAJELV
C
C Description:
C     La fonction LMGO_P12L_EVAJELV évalue les métriques 3D de l'élément de volume
C     P12L en un point de Gauss donné.
C
C Entrée:
C     NDIM              Nombre de DIMension des coordonnées des noeuds (== 2)
C     NDIMELV           Nombre de DIMension de l'ELément de Volume (== 3)
C     NNELV             Nombre de Noeuds dans un ELément de Volume (==6)
C     NDJV              Nombre de métrique 2D de l'élément de Volume (== 5)
C     VCORE             Table des COoRdonnées x, y Elémentaires !PAS UTILISÉ
C     VZCORE            Table des COoRdonnées en Z Elémentaires (zs et zf)
C     VDJE              Table des métriques élémentaires 2D,
C                       indépendantes de la solution et déjà calculées.
C     VCOPG             Table des COordonnées du Point de Gauss sur l'élément de référence à évaluer.
C
C Sortie:
C     VJP12L            Table des métriques au point de Gauss sur l'élément P12L
C     DJP12L            Déterminant du Jacobien au point de Gauss sur l'élément P12L
C     VJP6              Table des métriques au point de Gauss sur le sous-élément P6
C     DJP6              Déterminant du Jacobien au point de Gauss sur le sous-élément P6
C     IP6               Indice du sous-élément
C
C Notes:
C     1. En utilisant VJP12L comme une matrice, on dédouble l'information par rapport
C        à VDJE et on a des cases qui valent toujours 0 (dxi/dz, deta/dz). Par contre,
C        la matrice facilite les multiplications; pour l'instant on opte pour la facilité.
C     2. Un DJP12L nul se produira que si detJ2D==0 ou si zs==zf
C     3. Les métriques 2D ne seront pas tout à fait les même pcq ici on
C        trouve les "vrais" dxi_i/dx_j et non moyennant un produit par le detJ.
C************************************************************************
      FUNCTION LMGO_P12L_EVAJELV(NDIM,
     &                           NDIMELV,
     &                           NNELV,
     &                           NDJV,
     &                           VCORE, !pas utilisé
     &                           VZCORE,
     &                           VDJE,
     &                           VCOPG,
     &                           VJP12L,
     &                           DJP12L,
     &                           VJP6,
     &                           DJP6,
     &                           IP6)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_P12L_EVAJELV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDIM
      INTEGER NDIMELV
      INTEGER NNELV
      INTEGER NDJV
      REAL*8  VCORE (NDIM, NNELV) !PAS UTILISÉ
      REAL*8  VZCORE(   2, NNELV)
      REAL*8  VDJE  (NDJV)
      REAL*8  VCOPG (NDIMELV)
      REAL*8  VJP12L(NDIMELV, NDIMELV)
      REAL*8  DJP12L
      REAL*8  VJP6  (NDIMELV, NDIMELV)
      REAL*8  DJP6
      INTEGER IP6

      INCLUDE 'lmgo_p12l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'eacnst.fi'

      REAL*8 DJT6L            ! Déterminant 2D horizontal du P12L

      REAL*8 ZF1, ZF2, ZF3
      REAL*8 ZS1, ZS2, ZS3
      REAL*8 XI, ETA, ZETA    ! Point d'évaluation
      REAL*8 P1, P2, P3, P    ! Profondeurs (noeuds et point d'éval.)
      REAL*8 COEF1, COEF2, COEF3, COEF4   ! Variables de travail
D     REAL*8 N1_T3

C--- Voir lmgo_p12l_reqprm.for
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDIM    .EQ. 2)
D     CALL ERR_PRE(NDIMELV .EQ. 3)
D     CALL ERR_PRE(NNELV   .EQ. 6)
D     CALL ERR_PRE(NDJV    .EQ. 5)
D     N1_T3 = UN - VCOPG(1) - VCOPG(2)
D     CALL ERR_PRE(VCOPG(1) .GE. ZERO .AND. VCOPG(1) .LE. UN)
D     CALL ERR_PRE(VCOPG(2) .GE. ZERO .AND. VCOPG(2) .LE. UN)
D     CALL ERR_PRE(N1_T3 .GE. ZERO .AND. N1_T3 .LE. UN)
D     CALL ERR_PRE(VCOPG(3) .GE. -UN  .AND. VCOPG(3) .LE. UN)
C-----------------------------------------------------------------------

C---     LES "Z"
      ZF1 = VZCORE(1,1)    ! Doivent être linéaires, donc
      ZS1 = VZCORE(2,1)    ! on ne lit que les noeuds géométriques
      ZF2 = VZCORE(1,3)
      ZS2 = VZCORE(2,3)
      ZF3 = VZCORE(1,5)
      ZS3 = VZCORE(2,5)

C---     COORDONNÉE AU POINT D'ÉVALUATION
      XI   = VCOPG(1)
      ETA  = VCOPG(2)
      ZETA = VCOPG(3)

C---     PROFONDEUR EN XI, ETA, ZETA
      P1 = ZS1 - ZF1
      P2 = ZS2 - ZF2
      P3 = ZS3 - ZF3
      P = (UN - XI - ETA)*P1 + XI*P2 + ETA*P3
D     CALL ERR_ASR(P .GT. ZERO)

C---     MÉTRIQUES CONSTANTES
      DJT6L = VDJE(5)
D     CALL ERR_ASR(DJT6L .GT. ZERO)
      VJP12L(1,1) = VDJE(1)/DJT6L   ! dxi/dx (le vrai !)
      VJP12L(2,1) = VDJE(3)/DJT6L   ! dxi/dy (le vrai !)
      VJP12L(3,1) = ZERO            ! dxi/dz
      VJP12L(1,2) = VDJE(2)/DJT6L   ! deta/dx (le vrai!)
      VJP12L(2,2) = VDJE(4)/DJT6L   ! deta/dy (le vrai !)
      VJP12L(3,2) = ZERO            ! deta/dz

C---     MÉTRIQUES DÉPENDANTES DU POINT D'ÉVALUATION
      COEF1 = VDJE(1)*(ZF1-ZF2) + VDJE(2)*(ZF1-ZF3)
      COEF2 = VDJE(1)*(ZS1-ZS2) + VDJE(2)*(ZS1-ZS3)
      COEF3 = VDJE(3)*(ZF1-ZF2) + VDJE(4)*(ZF1-ZF3)
      COEF4 = VDJE(3)*(ZS1-ZS2) + VDJE(4)*(ZS1-ZS3)
      VJP12L(1,3) = ( COEF1+COEF2+ZETA*(-COEF1+COEF2) )/(P*DJT6L)
      VJP12L(2,3) = ( COEF3+COEF4+ZETA*(-COEF3+COEF4) )/(P*DJT6L)
      VJP12L(3,3) = DEUX / P

C---     Déterminant du Jacobien du P12L
      DJP12L = UN_2 * P * DJT6L

C--      DÉTERMINE LE SOUS-ÉLÉMENT
      IF (XI .GT. UN_2)THEN
         IP6 = 2
      ELSEIF (ETA .GT. UN_2) THEN
         IP6 = 3
      ELSEIF (XI + ETA .LE. UN_2) THEN
         IP6 = 1
      ELSE
         IP6 = 4
      END IF

C--      MÉTRIQUES DU P6
      IF (IP6 .EQ. 4) THEN
         !ajout d'un facteur (-1)
         VJP6(1,1) = - VJP12L(1,1) * DEUX    ! dxi/dx (du P6 le vrai !)
         VJP6(2,1) = - VJP12L(2,1) * DEUX    ! dxi/dy (du P6 le vrai !)
         VJP6(3,1) = ZERO                    ! dxi/dz du P6
         VJP6(1,2) = - VJP12L(1,2) * DEUX    ! deta/dx (du P6 le vrai !)
         VJP6(2,2) = - VJP12L(2,2) * DEUX    ! deta/dy (du P6 le vrai !)
         VJP6(3,2) = ZERO                    ! deta/dz du P6
      ELSE
         !pas de facteur (-1)
         VJP6(1,1) =  VJP12L(1,1) * DEUX     ! dxi/dx (du P6 le vrai !)
         VJP6(2,1) =  VJP12L(2,1) * DEUX     ! dxi/dy (du P6 le vrai !)
         VJP6(3,1) = ZERO                    ! dxi/dz du P6
         VJP6(1,2) =  VJP12L(1,2) * DEUX     ! deta/dx (du P6 le vrai !)
         VJP6(2,2) =  VJP12L(2,2) * DEUX     ! deta/dy (du P6 le vrai !)
         VJP6(3,2) = ZERO                    ! deta/dz du P6
      END IF
      VJP6(1,3) = VJP12L(1,3)
      VJP6(2,3) = VJP12L(2,3)
      VJP6(3,3) = VJP12L(3,3)

C---     Déterminant du Jacobien du P6
      DJP6 = UN_4 * DJP12L

      LMGO_P12L_EVAJELV = ERR_TYP()
      RETURN
      END
