C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_SP_HDRO_CMD
C     CHARACTER*(32) IC_SP_HDRO_REQCMD
C   Private:
C     SUBROUTINE IC_SP_HDRO_AID
C     INTEGER IC_SP_HDRO_PRN
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SP_HDRO_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SP_HDRO_CMD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'sphdro_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'sphdro_ic.fc'

      REAL*8  A0, A1, A2
      INTEGER IERR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SP_HDRO_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---        Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>A0</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, A0)
C     <comment>A1</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, A1)
C     <comment>A2</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 3, A2)
      IF (IERR .NE. 0) GOTO 9901

C---     Construis et initialise l'objet
      IF (ERR_GOOD()) IERR = SP_HDRO_CWUSRCNF(A0, A1, A2)

C---     Imprime
      IF (ERR_GOOD()) IERR = IC_SP_HDRO_PRN()

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
         WRITE(IPRM, '(A)') ' '
      ENDIF

C<comment>
C  The function <b>set_cw_coef</b> assign the parameters.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_AUCUN_PARAMETRE_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SP_HDRO_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_SP_HDRO_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SP_HDRO_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SP_HDRO_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sphdro_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The function <b>set_cw_coef</b> assign the coefficients for the user
C  defined wind friction coefficient relation. The three coefficients
C  (a0, a1, a2) define a quadratic form:
C  <pre> a0 + a1*w + a2*w^2</pre>
C  where w is the wind intensity.
C</comment>
      IC_SP_HDRO_REQCMD = 'set_cw_coef'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SP_HDRO_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sphdro_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SP_HDRO_PRN()

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sphdro.fc'
      INCLUDE 'sphdro_ic.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_USR_WIND_FNC'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()

C---     Impression des paramètres du bloc
      WRITE(LOG_BUF,'(2A, 1PE14.6E3)') 'A0', ' = ', SP_HDRO_USRA
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,'(2A, 1PE14.6E3)') 'A1', ' = ', SP_HDRO_USRB
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,'(2A, 1PE14.6E3)') 'A2', ' = ', SP_HDRO_USRC
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_SP_HDRO_PRN = ERR_TYP()
      RETURN
      END
