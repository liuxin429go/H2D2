C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  SousProgrammes utilitaires
C Sous-Groupe:  Least-SQuaRe
C Sommaire:
C************************************************************************

C************************************************************************
C Sommaire: Regression linéaire.
C
C Description:
C     La fonction <code>SP_LSQR_LINREG(...)</code> retourne les coefficients
C     de la régression linéaire Y = A*X + B.
C
C Entrée:
C     N        Nombre de points
C     X        les abscisses
C     Y        Les ordonnées
C
C Sortie:
C     A, B     Les coefficients de la regression
C
C Notes:
C     Intel 11.1 ne définit pas la fonction NORM2
C
C************************************************************************
      FUNCTION SP_LSQR_LINREG(N, X, Y, A, B)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_LSQR_LINREG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: N
      REAL*8,  INTENT(IN) :: X(N), Y(N)
      REAL*8,  INTENT(OUT):: A, B

      INCLUDE 'splsqr.fi'

      REAL*8  SX, SY, SXX, SXY
      REAL*8  D
C------------------------------------------------------------------------

      SX  = SUM( X(:) )
      SY  = SUM( Y(:) )
      SXX = SUM( X(:) * X(:) )
      SXY = SUM( X(:) * Y(:) )

C---     Les coefficients
      A = (SX*SY - N*SXY) / (SX*SX - N*SXX)
      B = (SY - A*SX) / N

C---     Le résidu
      D = SQRT( DOT_PRODUCT(Y(:)-A*X(:)-B, Y(:)-A*X(:)-B) )

      SP_LSQR_LINREG = D
      RETURN
      END FUNCTION SP_LSQR_LINREG
