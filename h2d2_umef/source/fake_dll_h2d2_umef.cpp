//************************************************************************
// H2D2 - External declaration of public symbols
// Module: h2d2_umef
// Entry point: extern "C" void fake_dll_h2d2_umef()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:47:07.611737
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_SP_HDRO
F_PROT(IC_SP_HDRO_CMD, ic_sp_hdro_cmd);
F_PROT(IC_SP_HDRO_REQCMD, ic_sp_hdro_reqcmd);
 
// ---  class SP_ADFT
F_PROT(SP_ADFT_EPS, sp_adft_eps);
 
// ---  class SP_ELEM
F_PROT(SP_ELEM_ASMCLIM, sp_elem_asmclim);
F_PROT(SP_ELEM_ASMFE, sp_elem_asmfe);
F_PROT(SP_ELEM_ASMKEL, sp_elem_asmkel);
F_PROT(SP_ELEM_NEQ2NDLT, sp_elem_neq2ndlt);
F_PROT(SP_ELEM_NDLT2NEQ, sp_elem_ndlt2neq);
F_PROT(SP_ELEM_CLIM2VAL, sp_elem_clim2val);
F_PROT(SP_ELEM_DIMILU0, sp_elem_dimilu0);
F_PROT(SP_ELEM_DIMILUN, sp_elem_dimilun);
F_PROT(SP_ELEM_INDILU0, sp_elem_indilu0);
F_PROT(SP_ELEM_INDILUN, sp_elem_indilun);
F_PROT(SP_ELEM_CTRCON, sp_elem_ctrcon);
F_PROT(SP_ELEM_CTRSRF, sp_elem_ctrsrf);
F_PROT(SP_ELEM_HESSELLPS, sp_elem_hessellps);
F_PROT(SP_ELEM_COLO, sp_elem_colo);
F_PROT(SP_ELEM_BLCS, sp_elem_blcs);
 
// ---  class SP_GMRS
F_PROT(SP_GMRS_ALLOC, sp_gmrs_alloc);
F_PROT(SP_GMRS_DEALLOC, sp_gmrs_dealloc);
F_PROT(SP_GMRS_SOLV, sp_gmrs_solv);
F_PROT(SP_GMRS_REDM, sp_gmrs_redm);
 
// ---  class SP_HDRO
F_PROT(SP_HDRO_ASGVMIN, sp_hdro_asgvmin);
F_PROT(SP_HDRO_TMAILD, sp_hdro_tmaild);
F_PROT(SP_HDRO_PECLET, sp_hdro_peclet);
F_PROT(SP_HDRO_CFL, sp_hdro_cfl);
F_PROT(SP_HDRO_CORIOLIS, sp_hdro_coriolis);
F_PROT(SP_HDRO_CWUSRCNF, sp_hdro_cwusrcnf);
F_PROT(SP_HDRO_CW, sp_hdro_cw);
F_PROT(SP_HDRO_CWNAME, sp_hdro_cwname);
F_PROT(SP_HDRO_TMAILD_Z, sp_hdro_tmaild_z);
F_PROT(SP_HDRO_PECLET_Z, sp_hdro_peclet_z);
 
// ---  class SP_LDUD
F_PROT(SP_LDUD_FACT, sp_ldud_fact);
F_PROT(SP_LDUD_SOLV, sp_ldud_solv);
 
// ---  class SP_LSQR
F_PROT(SP_LSQR_LINREG, sp_lsqr_linreg);
 
// ---  class SP_LSTC
F_PROT(SP_LSTC_INIT, sp_lstc_init);
F_PROT(SP_LSTC_AJTCHN, sp_lstc_ajtchn);
F_PROT(SP_LSTC_REQCHN, sp_lstc_reqchn);
F_PROT(SP_LSTC_REQDIM, sp_lstc_reqdim);
 
// ---  class SP_MORS
F_PROT(SP_MORS_DIMILUN, sp_mors_dimilun);
F_PROT(SP_MORS_CMLTBL, sp_mors_cmltbl);
F_PROT(SP_MORS_ASMINDCCS, sp_mors_asmindccs);
F_PROT(SP_MORS_ASMINDCRS, sp_mors_asmindcrs);
F_PROT(SP_MORS_TRIIND, sp_mors_triind);
F_PROT(SP_MORS_CCSCRS, sp_mors_ccscrs);
F_PROT(SP_MORS_XTRJD, sp_mors_xtrjd);
F_PROT(SP_MORS_ASMKE, sp_mors_asmke);
F_PROT(SP_MORS_ASMFE, sp_mors_asmfe);
F_PROT(SP_MORS_ASGCLIM, sp_mors_asgclim);
F_PROT(SP_MORS_FACT, sp_mors_fact);
F_PROT(SP_MORS_SOLV, sp_mors_solv);
F_PROT(SP_MORS_SCAL, sp_mors_scal);
F_PROT(SP_MORS_ADDD, sp_mors_addd);
F_PROT(SP_MORS_MULD, sp_mors_muld);
F_PROT(SP_MORS_MULV, sp_mors_mulv);
F_PROT(SP_MORS_LUMP, sp_mors_lump);
F_PROT(SP_MORS_DMIN, sp_mors_dmin);
F_PROT(SP_MORS_LISMAT, sp_mors_lismat);
F_PROT(SP_MORS_DMPMAT, sp_mors_dmpmat);
 
// ---  class SP_MTQ
F_PROT(SP_MTQ_ASGPRM, sp_mtq_asgprm);
F_PROT(SP_MTQ_CLCH, sp_mtq_clch);
F_PROT(SP_MTQ_CLCQ, sp_mtq_clcq);
 
// ---  class SP_PEAU
F_PROT(SP_PEAU_FILTRE, sp_peau_filtre);
 
// ---  class SP_SKYL
F_PROT(SP_SKYL_ASMHCL, sp_skyl_asmhcl);
F_PROT(SP_SKYL_CLCIND, sp_skyl_clcind);
F_PROT(SP_SKYL_ASMKE, sp_skyl_asmke);
F_PROT(SP_SKYL_FACT, sp_skyl_fact);
F_PROT(SP_SKYL_SOLV, sp_skyl_solv);
F_PROT(SP_SKYL_SCAL, sp_skyl_scal);
F_PROT(SP_SKYL_MULD, sp_skyl_muld);
F_PROT(SP_SKYL_MULV, sp_skyl_mulv);
F_PROT(SP_SKYL_DMPMAT, sp_skyl_dmpmat);
 
// ---  class SP_SPLIT
F_PROT(SP_SPLIT_SPLIT, sp_split_split);
 
// ---  class SP_TIDE
F_PROT(SP_TIDE_CFGTTD, sp_tide_cfgttd);
F_PROT(SP_TIDE_READTTD, sp_tide_readttd);
F_PROT(SP_TIDE_READFIC, sp_tide_readfic);
F_PROT(SP_TIDE_ADDCMP, sp_tide_addcmp);
F_PROT(SP_TIDE_ADDCMP2, sp_tide_addcmp2);
F_PROT(SP_TIDE_CMPEXIST, sp_tide_cmpexist);
F_PROT(SP_TIDE_TTDE_HD, sp_tide_ttde_hd);
F_PROT(SP_TIDE_NOAA_HD, sp_tide_noaa_hd);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_h2d2_umef()
{
   static char libname[] = "h2d2_umef";
 
   // ---  class IC_SP_HDRO
   F_RGST(IC_SP_HDRO_CMD, ic_sp_hdro_cmd, libname);
   F_RGST(IC_SP_HDRO_REQCMD, ic_sp_hdro_reqcmd, libname);
 
   // ---  class SP_ADFT
   F_RGST(SP_ADFT_EPS, sp_adft_eps, libname);
 
   // ---  class SP_ELEM
   F_RGST(SP_ELEM_ASMCLIM, sp_elem_asmclim, libname);
   F_RGST(SP_ELEM_ASMFE, sp_elem_asmfe, libname);
   F_RGST(SP_ELEM_ASMKEL, sp_elem_asmkel, libname);
   F_RGST(SP_ELEM_NEQ2NDLT, sp_elem_neq2ndlt, libname);
   F_RGST(SP_ELEM_NDLT2NEQ, sp_elem_ndlt2neq, libname);
   F_RGST(SP_ELEM_CLIM2VAL, sp_elem_clim2val, libname);
   F_RGST(SP_ELEM_DIMILU0, sp_elem_dimilu0, libname);
   F_RGST(SP_ELEM_DIMILUN, sp_elem_dimilun, libname);
   F_RGST(SP_ELEM_INDILU0, sp_elem_indilu0, libname);
   F_RGST(SP_ELEM_INDILUN, sp_elem_indilun, libname);
   F_RGST(SP_ELEM_CTRCON, sp_elem_ctrcon, libname);
   F_RGST(SP_ELEM_CTRSRF, sp_elem_ctrsrf, libname);
   F_RGST(SP_ELEM_HESSELLPS, sp_elem_hessellps, libname);
   F_RGST(SP_ELEM_COLO, sp_elem_colo, libname);
   F_RGST(SP_ELEM_BLCS, sp_elem_blcs, libname);
 
   // ---  class SP_GMRS
   F_RGST(SP_GMRS_ALLOC, sp_gmrs_alloc, libname);
   F_RGST(SP_GMRS_DEALLOC, sp_gmrs_dealloc, libname);
   F_RGST(SP_GMRS_SOLV, sp_gmrs_solv, libname);
   F_RGST(SP_GMRS_REDM, sp_gmrs_redm, libname);
 
   // ---  class SP_HDRO
   F_RGST(SP_HDRO_ASGVMIN, sp_hdro_asgvmin, libname);
   F_RGST(SP_HDRO_TMAILD, sp_hdro_tmaild, libname);
   F_RGST(SP_HDRO_PECLET, sp_hdro_peclet, libname);
   F_RGST(SP_HDRO_CFL, sp_hdro_cfl, libname);
   F_RGST(SP_HDRO_CORIOLIS, sp_hdro_coriolis, libname);
   F_RGST(SP_HDRO_CWUSRCNF, sp_hdro_cwusrcnf, libname);
   F_RGST(SP_HDRO_CW, sp_hdro_cw, libname);
   F_RGST(SP_HDRO_CWNAME, sp_hdro_cwname, libname);
   F_RGST(SP_HDRO_TMAILD_Z, sp_hdro_tmaild_z, libname);
   F_RGST(SP_HDRO_PECLET_Z, sp_hdro_peclet_z, libname);
 
   // ---  class SP_LDUD
   F_RGST(SP_LDUD_FACT, sp_ldud_fact, libname);
   F_RGST(SP_LDUD_SOLV, sp_ldud_solv, libname);
 
   // ---  class SP_LSQR
   F_RGST(SP_LSQR_LINREG, sp_lsqr_linreg, libname);
 
   // ---  class SP_LSTC
   F_RGST(SP_LSTC_INIT, sp_lstc_init, libname);
   F_RGST(SP_LSTC_AJTCHN, sp_lstc_ajtchn, libname);
   F_RGST(SP_LSTC_REQCHN, sp_lstc_reqchn, libname);
   F_RGST(SP_LSTC_REQDIM, sp_lstc_reqdim, libname);
 
   // ---  class SP_MORS
   F_RGST(SP_MORS_DIMILUN, sp_mors_dimilun, libname);
   F_RGST(SP_MORS_CMLTBL, sp_mors_cmltbl, libname);
   F_RGST(SP_MORS_ASMINDCCS, sp_mors_asmindccs, libname);
   F_RGST(SP_MORS_ASMINDCRS, sp_mors_asmindcrs, libname);
   F_RGST(SP_MORS_TRIIND, sp_mors_triind, libname);
   F_RGST(SP_MORS_CCSCRS, sp_mors_ccscrs, libname);
   F_RGST(SP_MORS_XTRJD, sp_mors_xtrjd, libname);
   F_RGST(SP_MORS_ASMKE, sp_mors_asmke, libname);
   F_RGST(SP_MORS_ASMFE, sp_mors_asmfe, libname);
   F_RGST(SP_MORS_ASGCLIM, sp_mors_asgclim, libname);
   F_RGST(SP_MORS_FACT, sp_mors_fact, libname);
   F_RGST(SP_MORS_SOLV, sp_mors_solv, libname);
   F_RGST(SP_MORS_SCAL, sp_mors_scal, libname);
   F_RGST(SP_MORS_ADDD, sp_mors_addd, libname);
   F_RGST(SP_MORS_MULD, sp_mors_muld, libname);
   F_RGST(SP_MORS_MULV, sp_mors_mulv, libname);
   F_RGST(SP_MORS_LUMP, sp_mors_lump, libname);
   F_RGST(SP_MORS_DMIN, sp_mors_dmin, libname);
   F_RGST(SP_MORS_LISMAT, sp_mors_lismat, libname);
   F_RGST(SP_MORS_DMPMAT, sp_mors_dmpmat, libname);
 
   // ---  class SP_MTQ
   F_RGST(SP_MTQ_ASGPRM, sp_mtq_asgprm, libname);
   F_RGST(SP_MTQ_CLCH, sp_mtq_clch, libname);
   F_RGST(SP_MTQ_CLCQ, sp_mtq_clcq, libname);
 
   // ---  class SP_PEAU
   F_RGST(SP_PEAU_FILTRE, sp_peau_filtre, libname);
 
   // ---  class SP_SKYL
   F_RGST(SP_SKYL_ASMHCL, sp_skyl_asmhcl, libname);
   F_RGST(SP_SKYL_CLCIND, sp_skyl_clcind, libname);
   F_RGST(SP_SKYL_ASMKE, sp_skyl_asmke, libname);
   F_RGST(SP_SKYL_FACT, sp_skyl_fact, libname);
   F_RGST(SP_SKYL_SOLV, sp_skyl_solv, libname);
   F_RGST(SP_SKYL_SCAL, sp_skyl_scal, libname);
   F_RGST(SP_SKYL_MULD, sp_skyl_muld, libname);
   F_RGST(SP_SKYL_MULV, sp_skyl_mulv, libname);
   F_RGST(SP_SKYL_DMPMAT, sp_skyl_dmpmat, libname);
 
   // ---  class SP_SPLIT
   F_RGST(SP_SPLIT_SPLIT, sp_split_split, libname);
 
   // ---  class SP_TIDE
   F_RGST(SP_TIDE_CFGTTD, sp_tide_cfgttd, libname);
   F_RGST(SP_TIDE_READTTD, sp_tide_readttd, libname);
   F_RGST(SP_TIDE_READFIC, sp_tide_readfic, libname);
   F_RGST(SP_TIDE_ADDCMP, sp_tide_addcmp, libname);
   F_RGST(SP_TIDE_ADDCMP2, sp_tide_addcmp2, libname);
   F_RGST(SP_TIDE_CMPEXIST, sp_tide_cmpexist, libname);
   F_RGST(SP_TIDE_TTDE_HD, sp_tide_ttde_hd, libname);
   F_RGST(SP_TIDE_NOAA_HD, sp_tide_noaa_hd, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
