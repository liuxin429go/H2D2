C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sommaire: Fonctions de calcul génériques d'hydrologie
C
C Groupe:  SousProgrammes utilitaires
C Sous-Groupe:  HyDROlogie
C
C Functions:
C   Public:
C     REAL*8 SP_HDRO_ASGVMIN
C     REAL*8 SP_HDRO_TMAILD
C     REAL*8 SP_HDRO_PECLET
C     REAL*8 SP_HDRO_CFL
C     INTEGER SP_HDRO_CWUSRCNF
C     REAL*8 SP_HDRO_CW
C   Private:
C     REAL*8 SP_HDRO_TEZDUYAR
C     REAL*8 SP_HDRO_WU1969
C     REAL*8 SP_HDRO_WU1980
C     REAL*8 SP_HDRO_SMITH1980
C     REAL*8 SP_HDRO_SHEPPARD1958
C     REAL*8 SP_HDRO_KONDO1975
C     REAL*8 SP_HDRO_KUMAR2008_WA1
C     REAL*8 SP_HDRO_KUMAR2008_WA2
C     REAL*8 SP_HDRO_KUMAR2008_WA3
C     REAL*8 SP_HDRO_KUMAR2008_WA4
C     REAL*8 SP_HDRO_CWUSR
C
C************************************************************************

C************************************************************************
C Sommaire: SP_HDRO
C
C Description:
C     Le block data <code>SP_HDRO(...)</code> initialise les variables
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SP_HDRO

      INCLUDE 'sphdro.fi'
      INCLUDE 'sphdro.fc'

      DATA SP_HDRO_GRA  / 9.81D+00/
      DATA SP_HDRO_HMIN / 1.00D-04/
      DATA SP_HDRO_VMIN / 1.00D-08/
      DATA SP_HDRO_USRA / SP_HDRO_RTAG /
      DATA SP_HDRO_USRB / SP_HDRO_RTAG /
      DATA SP_HDRO_USRC / SP_HDRO_RTAG /

      END

C************************************************************************
C Sommaire: SP_HDRO_ASGVMIN
C
C Description:
C     La fonction <code>SP_HDRO_ASGVMIN(...)</code> permet d'assigner la
C     le module min de la vitesse utilisé dans le calcul de la taille de
C     maille directionnelle. 2 lignes de défense:
C     1) si h < hmin, |v| = max(|v|, sqrt(gra*h))
C     2) si |v| = max (|v| , vmin)
C
C
C Entrée:
C     VMIN
C     HMIN
C     GRA
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_HDRO_ASGVMIN(VMIN, HMIN, GRA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_HDRO_ASGVMIN
CDEC$ ENDIF
      IMPLICIT NONE

      REAL*8 VMIN
      REAL*8 HMIN
      REAL*8 GRA

      INCLUDE 'sphdro.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sphdro.fc'
C-----------------------------------------------------------------------

C---     Contrôles

C---     Taille de maille directionnelle
      SP_HDRO_VMIN = VMIN
      SP_HDRO_HMIN = HMIN
      SP_HDRO_GRA  = GRA

      SP_HDRO_ASGVMIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_HDRO_TEZDUYAR
C
C Description:
C     La fonction <code>SP_HDRO_TEZDUYAR(...)</code> calcule le taille de
C     maille directionnelle sur l'élément de référence.
C
C Entrée:
C     REAL*8 V1         ! Projection normalisée de la vitesse sur le coté 2-1
C     REAL*8 V2         ! Projection normalisée de la vitesse sur le coté 3-1
C
C Sortie:
C     REAL*8 V1         ! Composante de la taille de maille sur le coté 2-1
C     REAL*8 V2         !
C
C Notes:
C     T.E. Tezduyar, Y. Osawa. Finite element stabilization parameters
C     computed from element matrices and vectors.
C     Comput. Methods Appl. Mech. Engrg. 190 (200) 411-430
C************************************************************************
      FUNCTION SP_HDRO_TEZDUYAR(V1, V2)

      IMPLICIT NONE

      REAL*8 V1
      REAL*8 V2

      INCLUDE 'sphdro.fi'
      INCLUDE 'sphdro.fc'

      REAL*8 RX, RY
C-----------------------------------------------------------------------

      IF (V1 .GT. 0.0D0) THEN
         IF (V2 .GT. 0.0D0) THEN
            RX = V1 / (V1+V2)
            RY = 1.0D0 - RX
         ELSE
            IF (V1 .GT. -V2) THEN
               RX = 1.0D0
               RY = V2/V1
            ELSE
               RX = -V1/V2
               RY = -1.0D0
            ENDIF
         ENDIF
      ELSE
         IF (V2 .LT. 0.0D0) THEN
            RX = - V1 / (V1+V2)
            RY = - 1.0D0 - RX
         ELSE
            IF (V2 .GT. ABS(V1)) THEN
               RX = V1/V2
               RY = 1.0D0
            ELSE
               RX = -1.0D0
               RY = -V2/V1
            ENDIF
         ENDIF
      ENDIF
      V1 = 0.5D0*RX
      V2 = 0.5D0*RY

      SP_HDRO_TEZDUYAR = 0.5D0 * HYPOT(RX, RY)
      RETURN
      END

C************************************************************************
C Sommaire: SP_HDRO_TMAILD
C
C Description:
C     La fonction <code>SP_HDRO_TMAILD(...)</code> calcule le taille de
C     maille directionnelle.
C
C Entrée:
C     REAL*8 U          ! Composantes de la vitesse
C     REAL*8 V
C     REAL*8 H          ! Profondeur
C     REAL*8 T1X        ! Métriques tangentes
C     REAL*8 T1Y
C     REAL*8 T2X
C     REAL*8 T2Y
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_HDRO_TMAILD(U, V, H, T1X, T1Y, T2X, T2Y)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_HDRO_TMAILD
CDEC$ ENDIF
      IMPLICIT NONE

      REAL*8 U
      REAL*8 V
      REAL*8 H
      REAL*8 T1X
      REAL*8 T1Y
      REAL*8 T2X
      REAL*8 T2Y

      INCLUDE 'sphdro.fi'
      INCLUDE 'sphdro.fc'

      REAL*8 VMODL, VELOC, VELOCX, VELOCY
      REAL*8 VONDE
      REAL*8 TM1, TM2, TM3
      REAL*8 TMAILD
      REAL*8 V1, V2

      REAL*8, PARAMETER :: RAC2    = 1.4142135623730950488D0
      REAL*8, PARAMETER :: UN_RAC2 = 0.7071067811865475244D0
      INTEGER, PARAMETER :: ICOD = SP_HDRO_TM_HYDROSIM
C-----------------------------------------------------------------------

C---     Normalise les vitesses petites
      VMODL = HYPOT(U, V)
      IF (H .LT. SP_HDRO_HMIN) THEN
         VONDE = SQRT(SP_HDRO_GRA*H)
         VMODL = MAX(VMODL, VONDE)
      ENDIF
      IF (VMODL .LT. SP_HDRO_VMIN) THEN
         VELOC  = SP_HDRO_VMIN
         VELOCX = VELOC*UN_RAC2
         VELOCY = VELOCX
      ELSE
         VELOC  = VMODL
         VELOCX = U
         VELOCY = V
      ENDIF

C---     Taille de maille sur chaque côté
      TM1 = (T1X*VELOCX + T1Y*VELOCY) / VELOC
      TM2 = (T2X*VELOCX + T2Y*VELOCY) / VELOC
      TM3 = ((T1X-T2X)*VELOCX + (T1Y-T2Y)*VELOCY) / VELOC

C---     Taille de maille directionnelle
      GOTO (10, 20, 30, 40, 50) ICOD+1

10    CONTINUE    ! detj
D        CALL ERR_ASR(ICOD .EQ. SP_HDRO_TM_DETJ)
         TMAILD = SQRT(T1X*T2Y - T2X*T1Y)
      GOTO 9999
20    CONTINUE    ! Géométrique
D        CALL ERR_ASR(ICOD .EQ. SP_HDRO_TM_GEOM)
         TMAILD = MAX( SQRT(TM1*TM1), SQRT(TM2*TM2) )
      GOTO 9999
30    CONTINUE    ! Hydrosim
D        CALL ERR_ASR(ICOD .EQ. SP_HDRO_TM_HYDROSIM)
         TMAILD = RAC2*SQRT(TM1*TM1 + TM2*TM2 + TM3*TM3) / 3.0D0
      GOTO 9999
40    CONTINUE    ! Atkin
D        CALL ERR_ASR(ICOD .EQ. SP_HDRO_TM_ATKIN)
         TMAILD = (SQRT(TM1*TM1)+SQRT(TM2*TM2)+SQRT(TM3*TM3)) / 4.0D0
      GOTO 9999
50    CONTINUE    ! Tezduyar
D        CALL ERR_ASR(ICOD .EQ. SP_HDRO_TM_TEZDUYAR)
         V1 = TM1 / HYPOT(T1X, T1Y)  ! Normalise
         V2 = TM2 / HYPOT(T2X, T2Y)
         TMAILD = SP_HDRO_TEZDUYAR(V1, V2)
         V1 = V1 * HYPOT(T1X, T1Y)   ! Dé-normalise
         V2 = V2 * HYPOT(T2X, T2Y)
         TMAILD = HYPOT(V1, V2)
      GOTO 9999

9999  CONTINUE
      SP_HDRO_TMAILD = TMAILD
      RETURN
      END

C************************************************************************
C Sommaire: SP_HDRO_PECLET
C
C Description:
C     La fonction <code>SP_HDRO_PECLET(...)</code> calcule le coefficient
C     de Peclet (v * L ) / nu.
C
C Entrée:
C     REAL*8 NU
C     REAL*8 U          ! Composantes de la vitesse
C     REAL*8 V
C     REAL*8 H          ! Profondeur
C     REAL*8 T1X        ! Métriques tangentes
C     REAL*8 T1Y
C     REAL*8 T2X
C     REAL*8 T2Y
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_HDRO_PECLET(NU, U, V, H, T1X, T1Y, T2X, T2Y)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_HDRO_PECLET
CDEC$ ENDIF
      IMPLICIT NONE

      REAL*8 NU
      REAL*8 U
      REAL*8 V
      REAL*8 H
      REAL*8 T1X
      REAL*8 T1Y
      REAL*8 T2X
      REAL*8 T2Y

      INCLUDE 'sphdro.fi'

      REAL*8 TMAIL
      REAL*8 VMOD
C-----------------------------------------------------------------------

C---     Taille de maille directionnelle
      TMAIL = SP_HDRO_TMAILD(U, V, H, T1X, T1Y, T2X, T2Y)

C---     Norme de la vitesse
      VMOD = HYPOT(U, V)
!!      VMOD = MAX(VMOD, SP_HDRO_VMIN)     ???

C---     Le Peclet ( v * L / nu)
      SP_HDRO_PECLET = VMOD * TMAIL / NU
      RETURN
      END

C************************************************************************
C Sommaire: SP_HDRO_CFL
C
C Description:
C     La fonction <code>SP_HDRO_CFL(...)</code> calcule le CFL.
C
C Entrée:
C     REAL*8 DT         ! Le pas de temps
C     REAL*8 G          ! La constante gravitationnelle
C     REAL*8 U          ! Composantes de la vitesse
C     REAL*8 V
C     REAL*8 H          ! Profondeur
C     REAL*8 T1X        ! Métriques tangentes
C     REAL*8 T1Y
C     REAL*8 T2X
C     REAL*8 T2Y
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_HDRO_CFL(DT, G, U, V, H, T1X, T1Y, T2X, T2Y)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_HDRO_CFL
CDEC$ ENDIF
      IMPLICIT NONE

      REAL*8 DT
      REAL*8 G
      REAL*8 U
      REAL*8 V
      REAL*8 H
      REAL*8 T1X
      REAL*8 T1Y
      REAL*8 T2X
      REAL*8 T2Y

      INCLUDE 'sphdro.fi'
      INCLUDE 'sphdro.fc'

      REAL*8 TMAIL
      REAL*8 VMOD, VOND
C-----------------------------------------------------------------------

C---     Taille de maille directionnelle
      TMAIL = SP_HDRO_TMAILD(U, V, H, T1X, T1Y, T2X, T2Y)

C---     Norme de la vitesse
      VMOD = HYPOT(U, V)
      VOND = SQRT(G * MAX(H, SP_HDRO_HMIN))

C---     Le CFL ( v * DT / DL)
      SP_HDRO_CFL = (VMOD+VOND) * DT / TMAIL
      RETURN
      END

C************************************************************************
C Sommaire: SP_HDRO_CORIOLIS
C
C Description:
C     La fonction <code>SP_HDRO_CORIOLIS(...)</code> calcule le
C     coefficient de Coriolis.
C
C Entrée:
C     VLAT     Latitude du lieux en degrés décimaux
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_HDRO_CORIOLIS(VLAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_HDRO_CORIOLIS
CDEC$ ENDIF
      
      USE TRIGD_M
      IMPLICIT NONE

      REAL*8 VLAT

      INCLUDE 'sphdro.fi'

      REAL*8, PARAMETER :: VROTTERRE = 360.0D0/(24.0D0*3600.0D0)
C-----------------------------------------------------------------------

      SP_HDRO_CORIOLIS = 2.0D0*DEG2RAD(VROTTERRE)*SIND(VLAT)
      RETURN
      END

C************************************************************************
C Sommaire: SP_HDRO_PROF
C
C Description:
C     La fonction <code>SP_HDRO_PROF(...)</code> calcule la
C     profondeur réalle.
C
C Entrée:
C     H     Niveau de l'eau
C     Z     Cote du fond
C     E     Épaisseur de glace
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_HDRO_PROF(H, Z, E)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_HDRO_PROF
CDEC$ ENDIF
      
      IMPLICIT NONE
      REAL*8, INTENT(IN) :: H
      REAL*8, INTENT(IN) :: Z
      REAL*8, INTENT(IN) :: E

      INCLUDE 'sphdro.fi'
      
      REAL*8, PARAMETER :: ZERO = 0.0D0
      
      REAL*8 EPAIGL, PROF
C-----------------------------------------------------------------------

      PROF   = H - Z
      EPAIGL = 0.9D0 * E
      EPAIGL = MIN(EPAIGL, PROF)
      EPAIGL = MAX(EPAIGL, ZERO)
      
      SP_HDRO_PROF = PROF - EPAIGL
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     Coefficient de friction du vent.
C     Formule de Wu (1969).
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C     Wu, Jin. Wind stress and surface roughness at air-sea interface
C     J. Geophys. Res. 74:2, 2156-2202.
C     http://dx.doi.org/10.1029/JB074i002p00444
C****************************************************************
      FUNCTION SP_HDRO_WU1969(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'eacnst.fi'
      INCLUDE 'sphdro.fc'

      REAL*8 CW

      REAL*8 QUINZE
      PARAMETER (QUINZE = 15.000000000000000000000D0)
C-----------------------------------------------------------------------

C-----  CW = 1.25E-3*W^(-1/5) POUR    W<1  EN m/s
C-----  CW = 0.50E-3*W^(1/2)  POUR  1<W<15 EN m/s
C-----  CW = 2.60E-3          POUR 15<W    EN m/s
      IF (W .EQ. ZERO) THEN
         CW = 1.25D-3
      ELSEIF (W .LE. UN) THEN
         CW = 1.25D-3*W**(-0.2D0)
      ELSEIF (W .LE. QUINZE) THEN
         CW = 0.50D-3*W**(0.5D0)
      ELSE
         CW = 2.60D-3
      ENDIF

      SP_HDRO_WU1969 = CW
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     Coefficient de friction du vent.
C     Formule de Wu (1980).
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C     Jin Wu, 1980: Wind-Stress coefficients over Sea surface near
C     Neutral Conditions-A Revisit. J. Phys. Oceanogr., 10, 727–740.
C     doi: http://dx.doi.org/10.1175/1520-0485(1980)010<0727:WSCOSS>2.0.CO;2
C****************************************************************
      FUNCTION SP_HDRO_WU1980(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'sphdro.fc'

      REAL*8 A, B
      PARAMETER (A = 0.8000D-03)
      PARAMETER (B = 0.0650D-03)
C-----------------------------------------------------------------------

      SP_HDRO_WU1980 = (A + B*W)
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     Coefficient de friction du vent.
C     Formule de Smith (1980).
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C     Stuart D. Smith, 1980: Wind Stress and Heat Flux over the
C     Ocean in Gale Force Winds. J. Phys. Oceanogr., 10, 709–726.
C     doi: http://dx.doi.org/10.1175/1520-0485(1980)010<0709:WSAHFO>2.0.CO;2C
C****************************************************************
      FUNCTION SP_HDRO_SMITH1980(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'sphdro.fc'

      REAL*8 A, B
      PARAMETER (A = 0.6100D-03)
      PARAMETER (B = 0.0630D-03)
C-----------------------------------------------------------------------

      SP_HDRO_SMITH1980 = (A + B*W)
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     Coefficient de friction du vent.
C     Formule de Sheppard (1958).
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_HDRO_SHEPPARD1958(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'sphdro.fc'

      REAL*8, PARAMETER :: A = 0.8000D-03
      REAL*8, PARAMETER :: B = 0.1140D-03
C-----------------------------------------------------------------------

      SP_HDRO_SHEPPARD1958 = (A + B*W)
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     Coefficient de friction du vent.
C     Formule de Kondo (1975).
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_HDRO_KONDO1975(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'sphdro.fc'

      REAL*8, PARAMETER :: A = 1.2000D-03
      REAL*8, PARAMETER :: B = 0.0250D-03
C-----------------------------------------------------------------------

      SP_HDRO_KONDO1975 = (A + B*W)
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     COEFFICIENT DE FRICTION DU VENT
C     Formule de Kumar & al. (2008)
C     Wave Age c_p/u* < 15.
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C     R. Rajesh Kumar & B. Prasad Kumar & A. N. V. Satyanarayana &
C     D. Bala Subrahamanyam & A. D. Rao & S. K. Dube
C     Parameterization of sea surface drag under varying sea
C     state and its dependence on wave age.
C     Nat Hazards (2009) 49:187–197. DOI 10.1007/s11069-008-9309-4
C****************************************************************
      FUNCTION SP_HDRO_KUMAR2008_WA1(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'sphdro.fc'

      REAL*8, PARAMETER :: A = 0.59700D-03
      REAL*8, PARAMETER :: B = 0.10100D-03
      REAL*8, PARAMETER :: C = 0.00255D-03
C-----------------------------------------------------------------------

      SP_HDRO_KUMAR2008_WA1 = A + W*(B + W*C)
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     COEFFICIENT DE FRICTION DU VENT
C     Formule de Kumar & al. (2008)
C     Wave Age 15 < c_p/u* < 20.
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C     R. Rajesh Kumar & B. Prasad Kumar & A. N. V. Satyanarayana &
C     D. Bala Subrahamanyam & A. D. Rao & S. K. Dube
C     Parameterization of sea surface drag under varying sea
C     state and its dependence on wave age.
C     Nat Hazards (2009) 49:187–197. DOI 10.1007/s11069-008-9309-4
C****************************************************************
      FUNCTION SP_HDRO_KUMAR2008_WA2(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'sphdro.fc'

      REAL*8, PARAMETER :: A = 0.51800D-03
      REAL*8, PARAMETER :: B = 0.07740D-03
      REAL*8, PARAMETER :: C = 0.00359D-03
C-----------------------------------------------------------------------

      SP_HDRO_KUMAR2008_WA2 = A + W*(B + W*C)
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     COEFFICIENT DE FRICTION DU VENT
C     Formule de Kumar & al. (2008)
C     Wave Age 20 < c_p/u* < 25
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C     R. Rajesh Kumar & B. Prasad Kumar & A. N. V. Satyanarayana &
C     D. Bala Subrahamanyam & A. D. Rao & S. K. Dube
C     Parameterization of sea surface drag under varying sea
C     state and its dependence on wave age.
C     Nat Hazards (2009) 49:187–197. DOI 10.1007/s11069-008-9309-4
C****************************************************************
      FUNCTION SP_HDRO_KUMAR2008_WA3(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'sphdro.fc'

      REAL*8, PARAMETER :: A = 0.60900D-03
      REAL*8, PARAMETER :: B = 0.06370D-03
      REAL*8, PARAMETER :: C = 0.00295D-03
C-----------------------------------------------------------------------

      SP_HDRO_KUMAR2008_WA3 = A + W*(B + W*C)
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     COEFFICIENT DE FRICTION DU VENT
C     Formule de Kumar & al. (2008)
C     Wave Age c_p/u* > 25
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C     R. Rajesh Kumar & B. Prasad Kumar & A. N. V. Satyanarayana &
C     D. Bala Subrahamanyam & A. D. Rao & S. K. Dube
C     Parameterization of sea surface drag under varying sea
C     state and its dependence on wave age.
C     Nat Hazards (2009) 49:187–197. DOI 10.1007/s11069-008-9309-4
C****************************************************************
      FUNCTION SP_HDRO_KUMAR2008_WA4(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'sphdro.fc'

      REAL*8, PARAMETER :: A =  0.81200D-03
      REAL*8, PARAMETER :: B = -0.01720D-03
      REAL*8, PARAMETER :: C =  0.00906D-03
C-----------------------------------------------------------------------

      SP_HDRO_KUMAR2008_WA4 = A + W*(B + W*C)
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     COEFFICIENT DE FRICTION DU VENT.
C     Formule d'ordre 2 gérée par l'utilisateur.
C     Les coefficients sont assignables par SP_HDRO_CWUSRCNF()
C
C Entrée:
C     W     Module de la vitesse du vent par rapport à l'eau
C
C Sortie:
C
C Notes:
C****************************************************************
      FUNCTION SP_HDRO_CWUSR(W)

      IMPLICIT NONE

      REAL*8 W

      INCLUDE 'sphdro.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_HDRO_USRA .NE. SP_HDRO_RTAG)
C-----------------------------------------------------------------------

      SP_HDRO_CWUSR = SP_HDRO_USRA + W*(SP_HDRO_USRB + W*SP_HDRO_USRC)
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     La fonction SP_HDRO_CWUSRCNF(..) permet d'assigner les
C     coefficients A, B, C de la forme quadratique A + B*W * C*W^2
C     pour calculer le coefficient de traînée dû au vent.
C
C Entrée:
C     A, B, C     Coefficients de la forme quadratique
C
C Sortie:
C
C Notes:
C****************************************************************
      FUNCTION SP_HDRO_CWUSRCNF(A, B, C)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_HDRO_CWUSRCNF
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8 A, B, C

      INCLUDE 'sphdro.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sphdro.fc'
C-----------------------------------------------------------------------

      SP_HDRO_USRA = A
      SP_HDRO_USRB = B
      SP_HDRO_USRC = C

      SP_HDRO_CWUSRCNF = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     Coefficient de friction du vent.
C
C Entrée:
C     W        Module de la vitesse du vent par rapport à l'eau
C     ICOD     Code de la formule à appliquer
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_HDRO_CW(W, ICOD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_HDRO_CW
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  W
      INTEGER ICOD

      INCLUDE 'sphdro.fi'
      INCLUDE 'sphdro.fc'

      REAL*8 CW
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ICOD .GT. SP_HDRO_CW_INDEFINI)
D     CALL ERR_PRE(ICOD .LT. SP_HDRO_CW_TAG_LAST)
C-----------------------------------------------------------------------

      SELECT CASE(ICOD)
         CASE(SP_HDRO_CW_NUL)
            CW = 0.0D0
         CASE(SP_HDRO_CW_WU1969)
            CW = SP_HDRO_WU1969(W)
         CASE(SP_HDRO_CW_WU1980)
            CW = SP_HDRO_WU1980(W)
         CASE(SP_HDRO_CW_SMITH1980)
            CW = SP_HDRO_SMITH1980(W)
         CASE(SP_HDRO_CW_SHEPPARD1958)
            CW = SP_HDRO_SHEPPARD1958(W)
         CASE(SP_HDRO_CW_KONDO1975)
            CW = SP_HDRO_KONDO1975(W)
         CASE(SP_HDRO_CW_KUMAR2008_WA1)
            CW = SP_HDRO_KUMAR2008_WA1(W)
         CASE(SP_HDRO_CW_KUMAR2008_WA2)
            CW = SP_HDRO_KUMAR2008_WA2(W)
         CASE(SP_HDRO_CW_KUMAR2008_WA3)
            CW = SP_HDRO_KUMAR2008_WA3(W)
         CASE(SP_HDRO_CW_KUMAR2008_WA4)
            CW = SP_HDRO_KUMAR2008_WA4(W)
         CASE(SP_HDRO_CW_USR)
            CW = SP_HDRO_CWUSR(W)
      END SELECT

      SP_HDRO_CW = CW
      RETURN
      END

C**************************************************************
C Sommaire:
C
C Description:
C     Coefficient de friction du vent.
C
C Entrée:
C     W        Module de la vitesse du vent par rapport à l'eau
C     ICOD     Code de la formule à appliquer
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_HDRO_CWNAME(ICOD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_HDRO_CWNAME
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ICOD

      INCLUDE 'sphdro.fi'
      INCLUDE 'sphdro.fc'

      CHARACTER*(32) CN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ICOD .GT. SP_HDRO_CW_INDEFINI)
D     CALL ERR_PRE(ICOD .LT. SP_HDRO_CW_TAG_LAST)
C-----------------------------------------------------------------------

      SELECT CASE(ICOD)
         CASE(SP_HDRO_CW_NUL)
            CN = 'SP_HDRO_CW_NUL'
         CASE(SP_HDRO_CW_WU1969)
            CN = 'SP_HDRO_CW_WU1969'
         CASE(SP_HDRO_CW_WU1980)
            CN = 'SP_HDRO_CW_WU1980'
         CASE(SP_HDRO_CW_SMITH1980)
            CN = 'SP_HDRO_CW_SMITH1980'
         CASE(SP_HDRO_CW_SHEPPARD1958)
            CN = 'SP_HDRO_CW_SHEPPARD1958'
         CASE(SP_HDRO_CW_KONDO1975)
            CN = 'SP_HDRO_CW_KONDO1975'
         CASE(SP_HDRO_CW_KUMAR2008_WA1)
            CN = 'SP_HDRO_CW_KUMAR2008_WA1'
         CASE(SP_HDRO_CW_KUMAR2008_WA2)
            CN = 'SP_HDRO_CW_KUMAR2008_WA2'
         CASE(SP_HDRO_CW_KUMAR2008_WA3)
            CN = 'SP_HDRO_CW_KUMAR2008_WA3'
         CASE(SP_HDRO_CW_KUMAR2008_WA4)
            CN = 'SP_HDRO_CW_KUMAR2008_WA4'
         CASE(SP_HDRO_CW_USR)
            CN = 'SP_HDRO_CW_USR'
      END SELECT

      SP_HDRO_CWNAME = CN
      RETURN
      END
