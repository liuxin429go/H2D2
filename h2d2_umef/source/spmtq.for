C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C************************************************************************


C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H_moyen(Z_am, H_am, Z_av, H_av)

      IMPLICIT NONE

      REAL*8 Z_am, H_am
      REAL*8 Z_av, H_av

      INCLUDE 'spmtq.fc'

      REAL*8 H_mo, c_av, c_am
C-----------------------------------------------------------------------

C---     Profondeur moyenne
      IF ((H_am .GT. SP_MTQ_H) .AND. (H_av .GT. SP_MTQ_H)) THEN
         H_mo = SP_MTQ_H
      ELSE
         c_av = H_av*(SP_MTQ_EH+H_av*(SP_MTQ_CH+H_av*SP_MTQ_AH))
         c_am = H_am*(SP_MTQ_FH+H_am*(SP_MTQ_DH+H_am*SP_MTQ_BH))
         H_mo = SP_MTQ_GH + c_av + c_am
         H_mo = min(H_mo, SP_MTQ_H)
         H_mo = max(H_mo, 0.001D0)
      ENDIF

      H_moyen = H_mo
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION Q_route(Z_am, H_am, Z_av, H_av)

      IMPLICIT NONE

      REAL*8 Z_am, H_am
      REAL*8 Z_av, H_av

      INCLUDE 'spmtq.fc'

      REAL*8 Q_r, dZ
      REAL*8 Z_rt

      REAL*8 DEUX_3
      PARAMETER(DEUX_3 = 2.0D0 / 3.0D0)
C-----------------------------------------------------------------------

      Z_rt  = SP_MTQ_Z_ROUTE

      IF (Z_am .LE. Z_rt) THEN
         Q_r = 0.0D0
      ELSE
         dZ = Z_am-Z_rt
         Q_r = 0.65D0 / SQRT(1.0D0 + (Z_am-Z_rt)/(H_am-dZ))
         dZ  = DEUX_3 * dZ
         Q_r = Q_r * SP_MTQ_B * sqrt(9.81D0 * dZ**3)
      ENDIF

      Q_route = Q_r
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION Q_orifice(Z_am, H_am, Z_av, H_av)

      IMPLICIT NONE

      REAL*8 Z_am, H_am
      REAL*8 Z_av, H_av

      INCLUDE 'spmtq.fc'

      REAL*8 dH
      REAL*8 Q_o
C-----------------------------------------------------------------------

C---     Débit par l'orifice
      dH = H_am+H_am - SP_MTQ_H
      Q_o = SP_MTQ_C * SP_MTQ_Ap * sqrt(9.81D0 * dH)

      Q_orifice = Q_o
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION Q_controle_entree(H_am)

      IMPLICIT NONE

      REAL*8 H_am

      INCLUDE 'spmtq.fc'

      REAL*8 Q_ce
C-----------------------------------------------------------------------

      Q_ce = SP_MTQ_DQ
     &     + H_am*(SP_MTQ_CQ + H_am*(SP_MTQ_BQ + H_am*SP_MTQ_AQ))

      Q_controle_entree = Q_ce
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION Q_controle_sortie(Z_am, Z_av, H_mo)

      IMPLICIT NONE

      REAL*8 Z_am
      REAL*8 Z_av
      REAL*8 H_mo

      INCLUDE 'spmtq.fc'

      REAL*8 A, R, rh
      REAL*8 Q_cs

      REAL*8 QUATRE_3
      PARAMETER (QUATRE_3 = 4.0D0 / 3.0D0)
C-----------------------------------------------------------------------

      R = 0.0D0
      IF     (NINT(SP_MTQ_TYPE) .EQ. 1) THEN
         IF (H_mo .LT. SP_MTQ_H) THEN
            A = SP_MTQ_B * H_mo
            R = A / (SP_MTQ_B + H_mo+H_mo)
         ELSE
            A = SP_MTQ_Ap
            R = SP_MTQ_Rp
         ENDIF
      ELSEIF (NINT(SP_MTQ_TYPE) .EQ. 2) THEN
         IF (H_mo .LT. SP_MTQ_H) THEN
            rh = H_mo / SP_MTQ_H
            A = rh*(0.4704D0 + rh*(1.6493D0 - rh*1.1055D0)) - 0.0090D0
            R = rh*(2.2051D0 + rh*(0.1604D0 - rh*1.3521D0)) - 0.0351D0
            A = A * SP_MTQ_Ap
            R = R * SP_MTQ_Rp
         ELSE
            A = SP_MTQ_Ap
            R = SP_MTQ_Rp
         ENDIF
      ENDIF

      A = 2.0D0 * 9.81D0 * (Z_am-Z_av) * A*A
      R = SP_MTQ_N*SP_MTQ_N / R**QUATRE_3
      R = SP_MTQ_Ke + 19.6D0*R*SP_MTQ_L + 1.0D0
      Q_cs = SQRT(A / R)

      Q_controle_sortie = Q_cs
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION Q_ponceau(Z_am0, Z_av0)

      IMPLICIT NONE

      REAL*8 Z_am0
      REAL*8 Z_av0

      INCLUDE 'spmtq.fi'
      INCLUDE 'spmtq.fc'

      REAL*8 H_am, H_av, H_mo
      REAL*8 Z_am, Z_av
      REAL*8 Q_p, Q_r, Q_o, Q_cs, Q_ce
      LOGICAL INVERSE
C-----------------------------------------------------------------------

C---     Prof au dessus des radiers
      IF (Z_am0 .GE. Z_av0) THEN
         Z_am = Z_am0
         Z_av = Z_av0
         H_am = Z_am - SP_MTQ_Z_RADAM
         H_av = Z_av - SP_MTQ_Z_RADAV
         INVERSE = .FALSE.
      ELSE                                ! Inverse pour Zam < Zav
         Z_am = Z_av0
         Z_av = Z_am0
         H_am = Z_am - SP_MTQ_Z_RADAV
         H_av = Z_av - SP_MTQ_Z_RADAM
         INVERSE = .TRUE.
      ENDIF

C---     Décharge par-dessus la route
      Q_r = Q_route(Z_am, H_am, Z_av, H_av)

C---     Écoulement comme un orifice
      Q_o = 0.0D0
      IF (H_am .GT. SP_MTQ_HHMAX*SP_MTQ_H) THEN
         Q_o = Q_orifice(Z_am, H_am, Z_av, H_av)
         Q_p = Q_o + Q_r

C---     Contrôles à la sortie ou à l'entrée
      ELSE
         H_mo = H_moyen(Z_am, H_am, Z_av, H_av)

         Q_cs = Q_controle_sortie(Z_am, Z_av, H_mo)
         Q_p = Q_cs + Q_r

         !!if (H_mo >= SP_MTQ_H) then  ! dans le document
         if (H_mo > SP_MTQ_H) then
            Q_ce = Q_controle_entree(H_am)
            Q_p = min(Q_ce+Q_o, Q_p)
         ENDIF
      ENDIF

C---     Inverse le débit pour Zam < Zav
      IF (INVERSE) THEN
         Q_p = -Q_p
      ENDIF

      Q_ponceau = Q_p
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_MTQ_ASGPRM(N, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MTQ_ASGPRM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER N
      REAL*8  V(N)

      INCLUDE 'spmtq.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spmtq.fc'

      INTEGER I
C-----------------------------------------------------------------------
D     CALL ERR_PRE(N .EQ. SP_MTQ_VA_DIM)
C-----------------------------------------------------------------------

      DO I=1,SP_MTQ_VA_DIM
         SP_MTQ_VA(I) = V(I)
      ENDDO

      SP_MTQ_ASGPRM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_MTQ_CLCH(Z_AM, Z_AV, Q_AM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MTQ_CLCH
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8 Z_am
      REAL*8 Z_av
      REAL*8 Q_am

      INCLUDE 'spmtq.fi'
      INCLUDE 'spmtq.fc'

      REAL*8 dQ, dZ, dH
      REAL*8 Q, Z
      REAL*8 EPS
      PARAMETER (EPS = 1.0D-7)
      INTEGER IT, NITER
C-----------------------------------------------------------------------

      NITER = 5
      Z = Z_am
      DO IT=1, NITER
         dZ = EPS*Z
         Q  = Q_ponceau(Z,    Z_av)
         dQ = Q_ponceau(Z+dZ, Z_av) - Q
         dH = - dZ * (Q-Q_am) / dQ
         Z = Z + dH
         IF (ABS(dH) .LT. 1.0D-8) GOTO 199
      ENDDO
199   CONTINUE

      SP_MTQ_CLCH = Z
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_MTQ_CLCQ(Z_AM, Z_AV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MTQ_CLCQ
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8 Z_am
      REAL*8 Z_av

      INCLUDE 'spmtq.fi'
      INCLUDE 'spmtq.fc'
C-----------------------------------------------------------------------

      SP_MTQ_CLCQ = Q_ponceau(Z_am, Z_av)
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      PROGRAM MAIN
!!!
!!!      IMPLICIT NONE
!!!
!!!      INCLUDE 'spmtq.fi'
!!!
!!!      REAL*8  DUMMY, V(24)
!!!      REAL*8  ZAM0, ZAM, ZAV0, ZAV
!!!      REAL*8  ZAMS(11), ZAVS(11)
!!!      INTEGER I, IERR, IZAM, IZAV
!!!      CHARACTER*16 TXT
!!!      DATA ZAMS/0.6D0, 0.7D0, 0.8D0, 0.9D0,
!!!     &          1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0, 6.5D0 /
!!!      DATA ZAVS/0.01D0, 0.4D0, 0.7D0,
!!!     &          1.0D0, 1.3D0, 1.6D0, 1.9D0, 2.2D0, 2.5D0, 2.8D0, 3.1D0 /
!!!C-----------------------------------------------------------------------
!!!
!!!      OPEN(UNIT  = 15,
!!!     &     FILE  = 'coefficients.txt',
!!!     &     FORM  = 'FORMATTED',
!!!     &     STATUS= 'OLD')
!!!
!!!      READ(15,*)
!!!      READ(15,*)
!!!      DO I=1,24
!!!         READ(15,*) TXT, V(I)
!!!      ENDDO
!!!
!!!      IERR = SP_MTQ_ASGPRM(24, V)
!!!
!!!      DO IZAV=1,11
!!!         ZAV = ZAVS(IZAV)
!!!         DO IZAM=1,11
!!!            ZAM = ZAMS(IZAM)
!!!            WRITE(6,*) ZAM, ZAV, SP_MTQ_CLCQ(ZAM, ZAV)
!!!         ENDDO
!!!         WRITE(6,*) '----------------------'
!!!      ENDDO
!!!
!!!      END
