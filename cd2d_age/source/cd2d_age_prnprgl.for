C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Age d'une particule (AGE)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_AGE_PRNPRGL
C
C Description:
C     IMPRESSION DES PROPRIETES GLOBALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_AGE_PRNPRGL(NPRGL,
     &                            VPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_AGE_PRNPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INTEGER NPRGL
      REAL*8  VPRGL(LM_CMMN_NPRGL)

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER I
C-----------------------------------------------------------------------

C---     APPEL LE PARENT
      CALL CD2D_BSE_PRNPRGL(LM_CMMN_NPRGL, VPRGL)

C---     IMPRESSION DU TITRE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_CINETIQUE_LUES:')

C---     IMPRESSION DE L'INFO
      CALL LOG_INCIND()
      I = CD2D_BSE_NPRGLL
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_FACTEUR_AGE',VPRGL(I))
      CALL LOG_DECIND()

      RETURN
      END

