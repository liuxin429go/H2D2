C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Age d'une particule (AGE)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_AGE_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_AGE_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_AGE_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IP
C-----------------------------------------------------------------------

      IP = CD2D_BSE_NPRGLL + 1

C---     CONTROLE DE POSITIVITE
      IERR = 0
      IF (VPRGL(IP) .LT. ZERO) THEN
         CALL LOG_ECRIS('ERR_PROP_GLOB_NEGATIVE:')
         CALL LOG_INCIND()
         IERR = CD2D_BSE_PRN1PRGL(IP,'MSG_FACTEUR_AGE',VPRGL(IP))
         CALL LOG_DECIND()
         IERR = -1
      ENDIF
      IF (IERR .EQ. -1) THEN
         IERR = ERR_TYP()
         RETURN
      ENDIF

C---     PROPRIÉTÉ EGALE A PETIT SI ZERO
      VPRGL(IP) = MAX(VPRGL(IP), PETIT)

C---     APPEL L'ELEMENT DE BASE
      CALL CD2D_BSE_PSLPRGL (VPRGL, IERR)

      IERR = ERR_TYP()
      RETURN
      END

