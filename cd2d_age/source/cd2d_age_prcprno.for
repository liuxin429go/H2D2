C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Age d'une particule (AGE)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_AGE_PRCPRNO
C
C Description:
C     Pré-traitement au calcul des propriétés nodales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C      REAL*8    VCORG      Table des COoRdonnées Globales
C      INTEGER   KNGV       Table des coNectivités Globales de Volume
C      REAL*8    VDJV       Table des métriques (Determinant, Jacobien) de Volume
C      REAL*8    VPRGL      Table des PRopriétés GLobales
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Sortie:
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_AGE_PRCPRNO (VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_AGE_PRCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     APPELLE LE PARENT
      CALL CD2D_BSEN_PRCPRNO(VCORG,
     &                       KNGV,
     &                       VDJV,
     &                       VPRGL,
     &                       VPRNO,
     &                       IERR)

C---     CALCUL DES TERMES PUITS-SOURCES
      CALL CD2D_AGE_PRCTPS (VPRGL,
     &                      VPRNO)

      RETURN
      END

C************************************************************************
C Sommaire :  CD2D_AGE_PRCTPS
C
C Description:
C     PRE-CALCUL DU TERME PUITS-SOURCE
C
C Entrée:
C   VPRGL       Propriétés globales
C   VPRNO       Propriétés nodales
C
C Sortie:
C   VPRNO
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_AGE_PRCTPS(VPRGL, VPRNO)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8 VPRGL(LM_CMMN_NPRGL)
      REAL*8 VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)

      INCLUDE 'cd2d_bse.fi'

      INTEGER IN
      INTEGER IPH, IPA, IPB, IGA
      REAL*8  H, COEF
C-----------------------------------------------------------------------

C---     INDICES DANS VPRNO
      IPH = 3
      IPA = LM_CMMN_NPRNOL + 6
      IPB = LM_CMMN_NPRNOL + 7

C---     INDICES DANS VPRGL
      IGA = CD2D_BSE_NPRGLL + 1

C---     BOUCLE SUR SUR LES NOEUDS
      COEF = VPRGL(IGA)
      DO IN =1,EG_CMMN_NNL
         H = VPRNO(IPH, IN)
         VPRNO(IPA, IN) = ZERO      ! Terme linéaire
         VPRNO(IPB, IN) = H*COEF    ! Terme constant
      ENDDO

      RETURN
      END
