//************************************************************************
// H2D2 - External declaration of public symbols
// Module: cd2d_age
// Entry point: extern "C" void fake_dll_cd2d_age()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:46:55.383000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class CD2D_AGE
F_PROT(CD2D_AGE_000, cd2d_age_000);
F_PROT(CD2D_AGE_REQHBASE, cd2d_age_reqhbase);
F_PROT(CD2D_AGE_ASGCMN, cd2d_age_asgcmn);
F_PROT(CD2D_AGE_HLPPRGL, cd2d_age_hlpprgl);
F_PROT(CD2D_AGE_HLPPRNO, cd2d_age_hlpprno);
F_PROT(CD2D_AGE_PRCPRNO, cd2d_age_prcprno);
F_PROT(CD2D_AGE_PRNPRGL, cd2d_age_prnprgl);
F_PROT(CD2D_AGE_PSLPRGL, cd2d_age_pslprgl);
F_PROT(CD2D_AGE_REQNFN, cd2d_age_reqnfn);
F_PROT(CD2D_AGE_REQPRM, cd2d_age_reqprm);
F_PROT(CD2D_AGE_PST_SIM_000, cd2d_age_pst_sim_000);
F_PROT(CD2D_AGE_PST_SIM_999, cd2d_age_pst_sim_999);
F_PROT(CD2D_AGE_PST_SIM_CTR, cd2d_age_pst_sim_ctr);
F_PROT(CD2D_AGE_PST_SIM_DTR, cd2d_age_pst_sim_dtr);
F_PROT(CD2D_AGE_PST_SIM_INI, cd2d_age_pst_sim_ini);
F_PROT(CD2D_AGE_PST_SIM_RST, cd2d_age_pst_sim_rst);
F_PROT(CD2D_AGE_PST_SIM_REQHBASE, cd2d_age_pst_sim_reqhbase);
F_PROT(CD2D_AGE_PST_SIM_HVALIDE, cd2d_age_pst_sim_hvalide);
F_PROT(CD2D_AGE_PST_SIM_ACC, cd2d_age_pst_sim_acc);
F_PROT(CD2D_AGE_PST_SIM_FIN, cd2d_age_pst_sim_fin);
F_PROT(CD2D_AGE_PST_SIM_XEQ, cd2d_age_pst_sim_xeq);
F_PROT(CD2D_AGE_PST_SIM_ASGHSIM, cd2d_age_pst_sim_asghsim);
F_PROT(CD2D_AGE_PST_SIM_REQHVNO, cd2d_age_pst_sim_reqhvno);
F_PROT(CD2D_AGE_PST_SIM_REQNOMF, cd2d_age_pst_sim_reqnomf);
 
// ---  class IC_CD2D
F_PROT(IC_CD2D_AGE_XEQCTR, ic_cd2d_age_xeqctr);
F_PROT(IC_CD2D_AGE_XEQMTH, ic_cd2d_age_xeqmth);
F_PROT(IC_CD2D_AGE_OPBDOT, ic_cd2d_age_opbdot);
F_PROT(IC_CD2D_AGE_REQCLS, ic_cd2d_age_reqcls);
F_PROT(IC_CD2D_AGE_REQHDL, ic_cd2d_age_reqhdl);
F_PROT(IC_CD2D_AGE_PST_SIM_CMD, ic_cd2d_age_pst_sim_cmd);
F_PROT(IC_CD2D_AGE_PST_SIM_REQCMD, ic_cd2d_age_pst_sim_reqcmd);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_cd2d_age()
{
   static char libname[] = "cd2d_age";
 
   // ---  class CD2D_AGE
   F_RGST(CD2D_AGE_000, cd2d_age_000, libname);
   F_RGST(CD2D_AGE_REQHBASE, cd2d_age_reqhbase, libname);
   F_RGST(CD2D_AGE_ASGCMN, cd2d_age_asgcmn, libname);
   F_RGST(CD2D_AGE_HLPPRGL, cd2d_age_hlpprgl, libname);
   F_RGST(CD2D_AGE_HLPPRNO, cd2d_age_hlpprno, libname);
   F_RGST(CD2D_AGE_PRCPRNO, cd2d_age_prcprno, libname);
   F_RGST(CD2D_AGE_PRNPRGL, cd2d_age_prnprgl, libname);
   F_RGST(CD2D_AGE_PSLPRGL, cd2d_age_pslprgl, libname);
   F_RGST(CD2D_AGE_REQNFN, cd2d_age_reqnfn, libname);
   F_RGST(CD2D_AGE_REQPRM, cd2d_age_reqprm, libname);
   F_RGST(CD2D_AGE_PST_SIM_000, cd2d_age_pst_sim_000, libname);
   F_RGST(CD2D_AGE_PST_SIM_999, cd2d_age_pst_sim_999, libname);
   F_RGST(CD2D_AGE_PST_SIM_CTR, cd2d_age_pst_sim_ctr, libname);
   F_RGST(CD2D_AGE_PST_SIM_DTR, cd2d_age_pst_sim_dtr, libname);
   F_RGST(CD2D_AGE_PST_SIM_INI, cd2d_age_pst_sim_ini, libname);
   F_RGST(CD2D_AGE_PST_SIM_RST, cd2d_age_pst_sim_rst, libname);
   F_RGST(CD2D_AGE_PST_SIM_REQHBASE, cd2d_age_pst_sim_reqhbase, libname);
   F_RGST(CD2D_AGE_PST_SIM_HVALIDE, cd2d_age_pst_sim_hvalide, libname);
   F_RGST(CD2D_AGE_PST_SIM_ACC, cd2d_age_pst_sim_acc, libname);
   F_RGST(CD2D_AGE_PST_SIM_FIN, cd2d_age_pst_sim_fin, libname);
   F_RGST(CD2D_AGE_PST_SIM_XEQ, cd2d_age_pst_sim_xeq, libname);
   F_RGST(CD2D_AGE_PST_SIM_ASGHSIM, cd2d_age_pst_sim_asghsim, libname);
   F_RGST(CD2D_AGE_PST_SIM_REQHVNO, cd2d_age_pst_sim_reqhvno, libname);
   F_RGST(CD2D_AGE_PST_SIM_REQNOMF, cd2d_age_pst_sim_reqnomf, libname);
 
   // ---  class IC_CD2D
   F_RGST(IC_CD2D_AGE_XEQCTR, ic_cd2d_age_xeqctr, libname);
   F_RGST(IC_CD2D_AGE_XEQMTH, ic_cd2d_age_xeqmth, libname);
   F_RGST(IC_CD2D_AGE_OPBDOT, ic_cd2d_age_opbdot, libname);
   F_RGST(IC_CD2D_AGE_REQCLS, ic_cd2d_age_reqcls, libname);
   F_RGST(IC_CD2D_AGE_REQHDL, ic_cd2d_age_reqhdl, libname);
   F_RGST(IC_CD2D_AGE_PST_SIM_CMD, ic_cd2d_age_pst_sim_cmd, libname);
   F_RGST(IC_CD2D_AGE_PST_SIM_REQCMD, ic_cd2d_age_pst_sim_reqcmd, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
