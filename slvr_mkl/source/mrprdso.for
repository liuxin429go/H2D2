C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER MR_PRDS_000
C     INTEGER MR_PRDS_999
C     INTEGER MR_PRDS_CTR
C     INTEGER MR_PRDS_DTR
C     INTEGER MR_PRDS_INI
C     INTEGER MR_PRDS_RST
C     INTEGER MR_PRDS_REQHBASE
C     LOGICAL MR_PRDS_HVALIDE
C     LOGICAL MR_PRDS_ESTDIRECTE
C     INTEGER MR_PRDS_ASMMTX
C     INTEGER MR_PRDS_FCTMTX
C     INTEGER MR_PRDS_RESMTX
C     INTEGER MR_PRDS_XEQ
C   Private:
C     SUBROUTINE MKL_PARDISO_PIVOT
C     INTEGER MR_PRDS_INIPRM
C     INTEGER MR_PRDS_PRNPRM
C     INTEGER MR_PRDS_ASGERR
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrprdso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_PRDS_NOBJMAX,
     &                   MR_PRDS_HBASE,
     &                   'Matrix Solver MKL Pardiso')

      MR_PRDS_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrprdso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER  IERR
      EXTERNAL MR_PRDS_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_PRDS_NOBJMAX,
     &                   MR_PRDS_HBASE,
     &                   MR_PRDS_DTR)

      MR_PRDS_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_PRDS_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrprdso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_PRDS_NOBJMAX,
     &                   MR_PRDS_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_PRDS_HVALIDE(HOBJ))
         IOB = HOBJ - MR_PRDS_HBASE

         MR_PRDS_DMIN(IOB) = 0.0D0
         MR_PRDS_HMTX(IOB) = 0
         MR_PRDS_HASM(IOB) = 0
         MR_PRDS_ICRC(IOB) = 0
         MR_PRDS_LPTR(IOB) = 0
         MR_PRDS_LPRM(IOB) = 0
         MR_PRDS_LTRV(IOB) = 0
      ENDIF

      MR_PRDS_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_PRDS_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrprdso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_PRDS_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_PRDS_NOBJMAX,
     &                   MR_PRDS_HBASE)

      MR_PRDS_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_PRDS_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrprdso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrprdso.fc'

      REAL*8  DMIN
      INTEGER IOB
      INTEGER IERR
      INTEGER HASM
      INTEGER HMTX
      INTEGER LPTR, LPRM
      LOGICAL ISOOC
      INTEGER, PARAMETER :: ILU = 0
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôles
      DMIN = 1.0D-13
      ISOOC = .FALSE.
      IF (DMIN .LT. 1.0D-18) GOTO 9900
      IF (DMIN .GT. 1.0D+00) GOTO 9900

C---     Reset les données
      IERR = MR_PRDS_RST(HOBJ)

C---     Initialise la matrice
      IF (ERR_GOOD()) IERR = MX_MORS_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_MORS_INI(HMTX, ILU)

C---     Initialise l'assembleur
      IF (ERR_GOOD()) IERR = MA_MORS_CTR(HASM)
      IF (ERR_GOOD()) IERR = MA_MORS_INI(HASM, HMTX)

C---     Dimensionne et initialise la table des pointeurs PARDISO
      LPTR = 0
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLIN8(64, LPTR)     ! Table I8 initialisée à 0
      ENDIF

C---     Dimensionne et initialise la table des paramètres PARDISO
      LPRM = 0
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLINT(64, LPRM)
         IF (ERR_GOOD())
     &      IERR = MR_PRDS_INIPRM(KA(SO_ALLC_REQKIND(KA,LPRM)),
     &                            KA(SO_ALLC_REQKIND(KA,LPTR)))
      ENDIF

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_PRDS_HBASE
         MR_PRDS_DMIN(IOB) = DMIN
         MR_PRDS_HMTX(IOB) = HMTX
         MR_PRDS_HASM(IOB) = HASM
         MR_PRDS_ICRC(IOB) = 0
         MR_PRDS_LPTR(IOB) = LPTR
         MR_PRDS_LPRM(IOB) = LPRM
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,1PE26.17E3)') 'ERR_VALEUR_INVALIDE', ':', DMIN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_DOMAINE_VALIDE',': [1.0e-18, 1.0]'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MR_PRDS_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_PRDS_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrprdso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HASM
      INTEGER HMTX
      INTEGER LPTR, LPRM, LTRV

      INTEGER NEQL, LIAP, LJAP, LKG
      INTEGER IERROR, IDUM, MAXFCT, MNUM, MTYPE, PHASE, NRHS, MSGLVL
      REAL*8  DDUM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MR_PRDS_HBASE

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_PRDS_HBASE
      HASM = MR_PRDS_HASM(IOB)
      HMTX = MR_PRDS_HMTX(IOB)
      LPTR = MR_PRDS_LPTR(IOB)
      LPRM = MR_PRDS_LPRM(IOB)
      LTRV = MR_PRDS_LTRV(IOB)

C---     DESALLOUE LA MÉMOIRE PARDISO
      IF (SO_ALLC_HEXIST(LPTR) .AND. MX_MORS_HVALIDE(HMTX)) THEN
         NEQL = MX_MORS_REQNEQL(HMTX)
         LIAP = MX_MORS_REQLIAP(HMTX)
         LJAP = MX_MORS_REQLJAP(HMTX)
         LKG  = MX_MORS_REQLKG (HMTX)

         MAXFCT= 1
         MNUM  = 1
         MTYPE = 1   ! real and structurally symmetric matrix
         PHASE = -1  ! release all internal memory for all matrices
         NRHS  = 0
         MSGLVL= 0
         IERROR= 0
         CALL PARDISO (KA(SO_ALLC_REQKIND(KA,LPTR)),
     &                 MAXFCT, MNUM, MTYPE,
     &                 PHASE,
     &                 NEQL,
     &                 VA(SO_ALLC_REQVIND(VA,LKG)),
     &                 KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                 KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                 IDUM,
     &                 NRHS,
     &                 KA(SO_ALLC_REQKIND(KA,LPRM)),
     &                 MSGLVL,
     &                 DDUM,
     &                 DDUM,
     &                 IERROR)
      ENDIF

      IF (MA_MORS_HVALIDE(HASM)) IERR = MA_MORS_DTR(HASM)
      IF (MX_MORS_HVALIDE(HMTX)) IERR = MX_MORS_DTR(HMTX)
      IF (SO_ALLC_HEXIST(LPTR))  IERR = SO_ALLC_ALLIN8(0, LPTR)
      IF (SO_ALLC_HEXIST(LPRM))  IERR = SO_ALLC_ALLINT(0, LPRM)
      IF (SO_ALLC_HEXIST(LTRV))  IERR = SO_ALLC_ALLRE8(0, LTRV)

      MR_PRDS_DMIN (IOB) = 0.0D0
      MR_PRDS_HMTX (IOB) = 0
      MR_PRDS_HASM (IOB) = 0
      MR_PRDS_ICRC (IOB) = 0
      MR_PRDS_LPTR (IOB) = 0
      MR_PRDS_LPRM (IOB) = 0
      MR_PRDS_LTRV (IOB) = 0
      MR_PRDS_LOOC (IOB) = .FALSE.

      MR_PRDS_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MR_PRDS_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrprdso.fi'
      INCLUDE 'mrprdso.fc'
C------------------------------------------------------------------------

      MR_PRDS_REQHBASE = MR_PRDS_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_PRDS_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrprdso.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrprdso.fc'
C------------------------------------------------------------------------

      MR_PRDS_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_PRDS_NOBJMAX,
     &                                  MR_PRDS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la méthode de résolution est directe
C
C Description:
C     La fonction MR_PRDS_ESTDIRECTE retourne .TRUE. si la méthode de
C     résolution est directe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_ESTDIRECTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_ESTDIRECTE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrprdso.fi'
C------------------------------------------------------------------------

      MR_PRDS_ESTDIRECTE = .TRUE.
      RETURN
      END

C************************************************************************
C Sommaire: Assemble le système matriciel.
C
C Description:
C     La fonction MR_PRDS_ASMMTX assemble le système matriciel.
C     La matrice est dimensionnée et assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_ASMMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_K,
     &                        F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_ASMMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'mrprdso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrprdso.fc'

      REAL*8  DMIN
      INTEGER IERR
      INTEGER IOB
      INTEGER NEQL
      INTEGER HASM
      INTEGER HMTX
      INTEGER LTRV
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.assemblage')

C---     Récupère les attributs
      IOB = HOBJ - MR_PRDS_HBASE
      DMIN = MR_PRDS_DMIN(IOB)
      HMTX = MR_PRDS_HMTX(IOB)
      HASM = MR_PRDS_HASM(IOB)
      LTRV = MR_PRDS_LTRV(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))

C---     Dimensionne la matrice
      IF (ERR_GOOD()) IERR = MX_MORS_DIMMAT(HMTX, HSIM)

C---     (Re)-dimensionne la table de travail
      IF (ERR_GOOD()) THEN
         NEQL = MX_MORS_REQNEQL(HMTX)
         IERR = SO_ALLC_ALLRE8(NEQL, LTRV)
         IF (ERR_GOOD()) MR_PRDS_LTRV(IOB) = LTRV
      ENDIF

C---     Assemble la matrice
      IF (ERR_GOOD()) IERR = MA_MORS_ASMMTX(HASM, HSIM, HALG, F_K)

C---     Pivot min
      IF (ERR_GOOD()) IERR = MX_MORS_DIAGMIN(HMTX, DMIN)

      CALL TR_CHRN_STOP('h2d2.reso.mat.assemblage')
      MR_PRDS_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise le système matriciel
C
C Description:
C     La fonction MR_PRDS_FCTMTX factorise le système matriciel. La matrice
C     doit être assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_FCTMTX(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_FCTMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'mrprdso.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spldud.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER IERR, IERROR
      INTEGER IOB
      INTEGER ICRC
      INTEGER HMTX
      INTEGER LPTR, LPRM
      INTEGER LIAP, LJAP, LKG
      INTEGER NEQL
      INTEGER MAXFCT, MNUM, MTYPE, PHASE, NRHS, MSGLVL, IDUM
      REAL*8  DDUM
      LOGICAL DOINIT, ISOOC
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.reso.solve.pardiso'
      
C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.factorisation')

C---     Récupère les attributs
      IOB = HOBJ - MR_PRDS_HBASE
      HMTX = MR_PRDS_HMTX(IOB)
      LPTR = MR_PRDS_LPTR(IOB)
      LPRM = MR_PRDS_LPRM(IOB)
      ISOOC= MR_PRDS_LOOC(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     Récupère les données de la matrice
      ICRC = MX_MORS_REQCRC32(HMTX)
      NEQL = MX_MORS_REQNEQL (HMTX)
      LIAP = MX_MORS_REQLIAP (HMTX)
      LJAP = MX_MORS_REQLJAP (HMTX)
      LKG  = MX_MORS_REQLKG  (HMTX)

C---     Test si la matrice doit être dimensionnée
      DOINIT = (MR_PRDS_ICRC(IOB) .EQ. 0) .OR.
     &         (MR_PRDS_ICRC(IOB) .NE. ICRC)

C---     Factorise
      IF (ERR_GOOD()) THEN
         MAXFCT= 1
         MNUM  = 1
         MTYPE = 1      ! real and structurally symmetric matrix
         IF (DOINIT) THEN
            PHASE = 12  ! symbolic and factorize
         ELSE
            PHASE = 22  ! only factorize
         ENDIF
         NRHS  = 0
         MSGLVL= 0
         IF (LOG_ESTZONEACTIVE(LOG_ZNE, LOG_LVL_VERBOSE)) MSGLVL= 1
         IERROR= 0
         CALL PARDISO (KA(SO_ALLC_REQKIND(KA,LPTR)),
     &                 MAXFCT, MNUM, MTYPE,
     &                 PHASE,
     &                 NEQL,
     &                 VA(SO_ALLC_REQVIND(VA,LKG)),
     &                 KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                 KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                 IDUM,
     &                 NRHS,
     &                 KA(SO_ALLC_REQKIND(KA,LPRM)),
     &                 MSGLVL,
     &                 DDUM,  ! B
     &                 DDUM,  ! X
     &                 IERROR)
         IERR = MR_PRDS_ASGERR(IERROR, 'MSG_PARDISO_FACTORIZATION')
      ENDIF

C---     Conserve le CRC
      IF (ERR_GOOD()) MR_PRDS_ICRC(IOB) = ICRC

      CALL TR_CHRN_STOP('h2d2.reso.mat.factorisation')
      MR_PRDS_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel
C
C Description:
C     La fonction MR_PRDS_RESMTX résout le système matriciel avec
C     un second membre. Le second membre est assemblé si les handles
C     sur la simulation HSIM et sur l'algorithme HALG sont tous deux non nuls,
C     sinon on résout directement avec VSOL.
C     La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_RESMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_F,
     &                        VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_RESMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_F

      INCLUDE 'mrprdso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER IERR, IERROR
      INTEGER IOB
      INTEGER HMTX, HASM
      INTEGER LPTR, LPRM, LTRV
      INTEGER LIAP, LJAP, LKG
      INTEGER NEQL
      INTEGER MAXFCT, MNUM, MTYPE, PHASE, NRHS, MSGLVL, IDUM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.reso.solve.pardiso'
      
C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.resolution')

C---     Récupère les attributs
      IOB = HOBJ - MR_PRDS_HBASE
      HMTX = MR_PRDS_HMTX(IOB)
      HASM = MR_PRDS_HASM (IOB)
      LPTR = MR_PRDS_LPTR(IOB)
      LPRM = MR_PRDS_LPRM(IOB)
      LTRV = MR_PRDS_LTRV(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))
D     CALL ERR_ASR(LPTR .NE. 0)
D     CALL ERR_ASR(LPRM .NE. 0)
D     CALL ERR_ASR(LTRV .NE. 0)

C---     Récupère les pointeurs
      NEQL = MX_MORS_REQNEQL(HMTX)
      LIAP = MX_MORS_REQLIAP(HMTX)
      LJAP = MX_MORS_REQLJAP(HMTX)
      LKG  = MX_MORS_REQLKG (HMTX)

C---     Assemble le membre de droite
      IF (ERR_GOOD() .AND. HSIM .NE. 0 .AND. HALG .NE. 0)
     &   IERR = MA_MORS_ASMRHS(HASM, HSIM, HALG, F_F, VSOL)

C---     Initialise X : ceinture et bretelles
      IF (ERR_GOOD())
     &   CALL DINIT(NEQL, 0.0D0, VA(SO_ALLC_REQVIND(VA,LTRV)), 1)

C---     Résous
      IF (ERR_GOOD()) THEN
         MAXFCT= 1
         MNUM  = 1
         MTYPE = 1   ! real and structurally symmetric matrix
         PHASE = 33  ! only solve
         NRHS  = 1
         MSGLVL= 0
         IF (LOG_ESTZONEACTIVE(LOG_ZNE, LOG_LVL_VERBOSE)) MSGLVL= 1
         IERROR= 0
         CALL PARDISO (KA(SO_ALLC_REQKIND(KA,LPTR)),
     &                 MAXFCT, MNUM, MTYPE,
     &                 PHASE,
     &                 NEQL,
     &                 VA(SO_ALLC_REQVIND(VA,LKG)),
     &                 KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                 KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                 IDUM,
     &                 NRHS,
     &                 KA(SO_ALLC_REQKIND(KA,LPRM)),
     &                 MSGLVL,
     &                 VSOL,                          ! B
     &                 VA(SO_ALLC_REQVIND(VA,LTRV)),  ! X
     &                 IERROR)
         IF (IERROR .EQ. -4) THEN
            LOG_BUF = 'MSG_PARDISO_ITERATIVE_REFINEMENT_NON_CONVERGING'
            CALL LOG_WRN(LOG_ZNE, LOG_BUF)
         ELSE
            IERR = MR_PRDS_ASGERR(IERROR, 'MSG_PARDISO_SOLVE')
         ENDIF
      ENDIF

C---     Solution dans VSOL
      IF (ERR_GOOD())
     &   CALL DCOPY(NEQL, VA(SO_ALLC_REQVIND(VA,LTRV)), 1, VSOL, 1)

C---     Imprime les stats
      IF (ERR_GOOD())
     &   IERR = MR_PRDS_PRNPRM(KA(SO_ALLC_REQKIND(KA,LPRM)))

      CALL TR_CHRN_STOP('h2d2.reso.mat.resolution')
      MR_PRDS_RESMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel.
C
C Description:
C     La fonction MR_PRDS_XEQ résous complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PRDS_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrprdso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER IERR
      INTEGER HASM
      INTEGER LPRM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.resolution')

C---     Assemble la matrice
      IF(ERR_GOOD())IERR=MR_PRDS_ASMMTX(HOBJ,HSIM,HALG,F_K,F_KU)

C---     Factorise la matrice
      IF(ERR_GOOD())IERR=MR_PRDS_FCTMTX(HOBJ)

C---     Résous
      IF(ERR_GOOD())IERR=MR_PRDS_RESMTX(HOBJ,HSIM,HALG,F_F,VSOL)

      CALL TR_CHRN_STOP('h2d2.reso.mat.resolution')
      MR_PRDS_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise les paramètres PARDISO.
C
C Description:
C     La fonction privée MR_PRDS_INIPRM initialise les paramètres PARDISO.
C
C Entrée:
C     IPARAM(64)     Tables des paramètres PARDISO
C
C Sortie:
C     IPARAM(64)     Tables des paramètres PARDISO initialisée
C
C Notes:
C     Conditional Numerical Reproducibility (CNR):
C        Setting iparm(2) = 3 prevents the use of CNR mode (iparm(34) > 0)
C        because Intel MKL PARDISO uses dynamic parallelism.
C************************************************************************
      FUNCTION MR_PRDS_INIPRM(IPARM, IPTR)

      IMPLICIT NONE

      INTEGER IPARM(64)
      INTEGER IPTR (*)

      INCLUDE 'err.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER I
      INTEGER MTYPE
      
!$    INTEGER OMP_GET_MAX_THREADS
C------------------------------------------------------------------------

      MTYPE = 1   ! real and structurally symmetric matrix
      CALL PARDISOINIT(IPTR, MTYPE, IPARM)

!      iparm(1) = 1   ! no solver default
!      iparm(2) = 2   ! Fill-in reordering for the input matrix (0: min deg, 1: METIS, 2: ParMETIS) 
!      iparm(3) = 0   ! not in use
!      iparm(4) = 0   ! no iterative-direct algorithm
!      iparm(5) = 0   ! no user fill-in reducing permutation
!      iparm(6) = 0   ! store the solution in x
!      iparm(7) = 0   ! Output: number of performed iterative refinement steps
      iparm(8) = 9   ! number of iterative refinement steps
!      iparm(9) = 0   ! not in use
!      iparm(10) = 13 ! perturbe the pivot elements with 1E-13
!      iparm(11) = 1  ! scaling (0:disable, 1: enable)
!      iparm(12) = 0  ! not in use
      iparm(13) = 1  ! improved accuracy using (non-)symmetric weighted matchings
!      iparm(14) = -1 ! Output: number of perturbed pivots
!      iparm(15) = -1 ! Output: peak memory symbolic factorization
!      iparm(16) = -1 ! Output: permanent memory symbolic factorization
!      iparm(17) = -1 ! Output: memory numerical factorization and solution
!      iparm(18) = -1 ! Output: number of nonzeros in the factor LU
      iparm(19) = 0  ! Output: Mflops for LU factorization
!      iparm(20) = -1 ! Output: CG/CGS diagnostics
!      iparm(21) = 0  ! no pivoting for symmetric indefinite matrices
!      iparm(22) = 0  ! Output: Number of positive eigenvalues
!      iparm(23) = 0  ! Output: Number of negative eigenvalues
!      iparm(24) = 0   ! Parallel factorization control (0: old, 1: new, 10: improved)
       iparm(25) = 1  ! Parallel forward/backward solve control.
!      iparm(27) = 1  ! matrix checker (0: nocheck, 1: check ia and ja)
!      iparm(28) = 0  ! double precision

!$    iparm(34) = OMP_GET_MAX_THREADS()  ! Optimal number of OpenMP threads for conditional numerical reproducibility (CNR) mode

!      iparm(56) = 0  ! mkl_pardiso_pivot
      iparm(60) = 0  ! no OOC

      MR_PRDS_INIPRM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne un paramètres PARDISO.
C
C Description:
C     La fonction MR_PRDS_ASGPRM assigne le paramètre PARDISO d'indice IDX
C     à la valeur IVAL.
C
C Entrée:
C     IPARAM(64)     Tables des paramètres PARDISO
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_ASGPRM(HOBJ, IDX, IVAL)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IDX
      INTEGER IVAL

      INCLUDE 'mrprdso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrprdso.fc'
      
      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - MR_PRDS_HBASE
      IF (MR_PRDS_ICRC(IOB) .NE. 0) GOTO 9900
      IERR = SO_ALLC_ASGIN4(MR_PRDS_LPRM(IOB), IDX, IVAL)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ASG_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT('MSG_ASG_AVANT_FACTORISATION')
      GOTO 9999

9999  CONTINUE
      MR_PRDS_ASGPRM = ERR_TYP()
      RETURN
      END

C************************************************************************
C     Pas certain comment ça marche
C     Désactivé par iparm(56)
C************************************************************************
      SUBROUTINE MKL_PARDISO_PIVOT(AI, BI, EPS)

      IMPLICIT NONE

      REAL*8 AI
      REAL*8 BI
      REAL*8 EPS
C------------------------------------------------------------------------

      IF     (BI .GT. EPS .OR. BI .LT. -EPS) THEN
!        pass
      ELSEIF (BI .GT. 0.0D0) THEN
         BI = EPS
      ELSE
         BI = -EPS
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Imprime les paramètres PARDISO.
C
C Description:
C     La fonction privée MR_PRDS_PRNPRM imprime les statistiques de
C     Pardiso.
C
C Entrée:
C     IPARAM(64)     Tables des paramètres PARDISO
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_PRNPRM(IPARM)

      IMPLICIT NONE

      INTEGER IPARM(64)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mrprdso.fc'

      INTEGER IMAXMEM
C------------------------------------------------------------------------

C---     Zone de LOG
      LOG_ZNE = 'h2d2.reso.solve.pardiso'

C---     Entête
      CALL LOG_VRBS(LOG_ZNE, 'MSG_STATS_PARDISO')
      CALL LOG_INCIND()

      IF (IPARM(7) .NE. -1) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Number of performed iterative refinement steps#<49>#',
     &         ':', IPARM(7)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(14) .NE. -1) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Number of perturbed pivot#<49>#',
     &         ':', IPARM(14)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(15) .NE. -1) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Peak memory symbolic factorization#<45>#',
     &         '(kB):', IPARM(15)
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(16) .NE. -1) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Permanent memory symbolic factorization#<45>#',
     &         '(kB):', IPARM(16)
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(17) .NE. -1) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Memory numerical factorization and solution#<45>#',
     &         '(kB):', IPARM(17)
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

      IMAXMEM = MAX(IPARM(15),IPARM(16)+IPARM(17))
      IF (IMAXMEM .NE. IPARM(15)) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Total peak memory solver consumption#<45>#',
     &         '(kB):', IMAXMEM
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(18) .NE. -1) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Number of nonzeros in the factor LU#<49>#',
     &         ':', IPARM(18)
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(19) .GT. 0) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Mflops for LU factorization#<49>#',
     &         ':', IPARM(19)
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(20) .NE. -1) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'CG/CGS diagnostics#<49>#',
     &         ':', IPARM(20)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(22) .NE. -1) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Number of positive eigenvalues#<49>#',
     &         ':', IPARM(22)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(23) .NE. -1) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Number of negative eigenvalues#<49>#',
     &         ':', IPARM(23)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(30) .GT. 0) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Number of zero or negative pivots#<49>#',
     &         ':', IPARM(30)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

      IF (IPARM(63) .GT. 0) THEN
         WRITE(LOG_BUF, '(2A,I12)')
     &         'Minimum OOC memory#<49>#',
     &         ':', IPARM(63)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

      CALL LOG_DECIND()

      MR_PRDS_PRNPRM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Traduis l'erreur PARDISO en erreur standard.
C
C Description:
C     La fonction privée MR_PRDS_ASGERR transforme le message d'erreur
C     retourné par PARDISO en une erreur standard.
C
C Entrée:
C     INTEGER       IERR            Code d'erreur de PARDISO
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PRDS_ASGERR(IERR, MSG)

      IMPLICIT NONE

      INTEGER IERR
      CHARACTER*(*) MSG

      INCLUDE 'err.fi'
      INCLUDE 'mrprdso.fc'
C----------------------------------------------------------------------------

      IF (IERR .LT. 0) THEN
         IF     (IERR .EQ. -1) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_INPUT_INCONSISTENT')
         ELSEIF (IERR .EQ. -2) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_NOT_ENOUGH_MEMORY')
         ELSEIF (IERR .EQ. -3) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_REORDERING_PROBLEM')
         ELSEIF (IERR .EQ. -4) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_ZERO_PIVOT')
         ELSEIF (IERR .EQ. -5) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_INTERNAL_ERROR')
         ELSEIF (IERR .EQ. -6) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_REORDERING_FAILED')
         ELSEIF (IERR .EQ. -7) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_DIAGONAL_MATRIC_PROBLEM')
         ELSEIF (IERR .EQ. -8) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_32BIT_INTEGER_OVERFLOW')
         ELSEIF (IERR .EQ. -9) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_OOC_NOT_ENOUGH_MEMORY')
         ELSEIF (IERR .EQ. -10) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_OOC_OPENING_TEMP_FILES')
         ELSEIF (IERR .EQ. -11) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_OOC_RW_ERROR')
         ELSE
            CALL ERR_ASG(ERR_ERR, 'ERR_PARDISO_UNCLASSIFIED_ERROR')
         ENDIF
         CALL ERR_AJT(MSG)
      ENDIF

      MR_PRDS_ASGERR = ERR_TYP()
      RETURN
      END
