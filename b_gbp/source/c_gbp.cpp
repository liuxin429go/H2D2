//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  Mini-Dump
// Sommaire: Fonctions C de mini-dump
//
// Notes:
// http://software.intel.com/sites/products/documentation/hpc/compilerpro/en-us/fortran/win/compiler_f/bldaps_for/win/bldaps_use_sqq.htm
// Sous Windows, compilateur Intel, les exceptions FORTRAN et l'affichage
// de la pile d'appel sont contrôlées par la variable d'environnement
// FOR_IGNORE_EXCEPTIONS. Si FOR_IGNORE_EXCEPTIONS est définie, le Fortran RTL
// laisse passer les exceptions au C-RTL et c'est là que breakpad intervient.
// On a donc 3 comportement possibles:
//    1) sans FOR_IGNORE_EXCEPTIONS: affichage de la pile par le Fortran
//    2) avec FOR_IGNORE_EXCEPTIONS, sans breakpad: possibilité de débugger
//    3) avec FOR_IGNORE_EXCEPTIONS, avec breakpad: mini-dump
//
//************************************************************************
#include "c_gbp.h"

#include <crash_reporter.h>
#include <getDumpPath.h>

#include <algorithm>
#include <sstream>
#include <string>
#if defined(H2D2_WINDOWS)
#  include "client/windows/handler/exception_handler.h"
#  include <shellapi.h>
#  include <shlobj.h>
#elif defined(H2D2_UNIX)
#  include <sys/ucontext.h>
#  include "client/linux/handler/exception_handler.h"
#  include <libgen.h>
#  include <pthread.h>
#  include <stdio.h>
#  include <string.h>
#  include <linux/limits.h>
#else
#  error Unsupported operating system
#endif

namespace gbp = google_breakpad;

static gbp::ExceptionHandler* ehP = NULL;
static TTString reporterPath;


//************************************************************************
// Sommaire:
//
// Description:
//    La fonction statique <code>compareAndSwapInt</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
long compareAndSwapInt(volatile long* ptr,
                       long new_value,
                       long old_value)
{
#if   defined(H2D2_WINDOWS)
   return InterlockedCompareExchange(ptr, new_value, old_value);
#elif defined(H2D2_UNIX)
   static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

   pthread_mutex_lock(&mutex);

   long retval = *ptr;
   if (*ptr == old_value)
      *ptr = new_value;

   pthread_mutex_unlock(&mutex);

   return retval;
#endif
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction statique <code>getCommandLine</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
TTString getCommandLine()
{
#if   defined(H2D2_WINDOWS)
   return TTString( GetCommandLineW() );

#elif defined(H2D2_UNIX)
   char path[PATH_MAX];

   char cmd[16];
   sprintf(cmd, "ps %d", getpid());

   //---  Read second line
   FILE* fd = popen(cmd, "r");
   fgets(path, PATH_MAX, fd);
   fgets(path, PATH_MAX, fd);
   pclose(fd);

   //---  Get 5th argument
   std::istringstream is(path);
   std::string item;
   for (int i=0; i < 5; ++i) is >> item;

   //---  Full path
   realpath(item.c_str(), path);

   return TTString(path);
#endif
}

//************************************************************************
// Sommaire:
//
// Description:
//    The static function <code>getReporterPath</code> build for future use
//    the path of the crash reporter that is expected to be located in the
//    same directory as the main executable.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
void getReporterPath(TTString& rPath)
{
#if defined(H2D2_WINDOWS)
   const TTChar dirsep   = '\\';
   LPWSTR *szArglist = NULL;
   int nArgs;

   TTString cmdLine = getCommandLine();
   szArglist = ::CommandLineToArgvW(cmdLine.c_str(), &nArgs);
   if (szArglist == NULL) throw "";

   TTChar drive[4] = {0};
   TTChar dir[MAX_PATH] = {0};
   _wsplitpath(szArglist[0], drive, dir, NULL, NULL);
#else
   const TTChar dirsep   = '/';
   TTChar drive[4]  = {0};
   TTChar dir[PATH_MAX] = {0};
   TTString cmdLine = getCommandLine();
   cmdLine = cmdLine.substr(0, cmdLine.find_last_of(dirsep));
   strcpy(dir, cmdLine.c_str());
#endif

   TTOStream os;
   if (GBP_STRLEN(drive) > 0) os << drive << dirsep;
   if (GBP_STRLEN(dir)   > 0) os << dir   << dirsep;
   os << GBP_C2W("crash_reporter") << std::ends;
   rPath = os.str();

#if defined(H2D2_WINDOWS)
   // Free memory allocated for CommandLineToArgvW arguments.
   ::LocalFree(szArglist);
#endif
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction statique <code>filterCallback</code>
//    This callback is executed when the process has crashed and *before*
//    the crash dump is created. To prevent duplicate crash reports we
//    make every thread calling this method, except the very first one,
//    go to sleep.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Taken from breakpad_win.cc
//************************************************************************
#if defined(H2D2_WINDOWS)
static bool filterCallback(void* pContext,
                           EXCEPTION_POINTERS*,
                           MDRawAssertionInfo*)
{
   static long handling_exception = 0;
   if (compareAndSwapInt(&handling_exception, 1, 0) == 1)
   {
      Sleep(INFINITE);
   }

  return true;    // tell Breakpad that we want a crash dump.
}
#elif defined(H2D2_UNIX)
static bool filterCallback(void*)
{
   static long handling_exception = 0;
   if (compareAndSwapInt(&handling_exception, 0, 1) == 1)
   {
      sleep(3600);   // 1h should be enough
   }

   return true;    // tell Breakpad that we want a crash dump.
}
#else
#  error Invalid operating system
#endif

//************************************************************************
// Sommaire:
//
// Description:
//    The static function <code>dumpCallback</code> is a callback executed
//    when the process has crashed, after the crash dump has been created.
//    We need to minimize the amount of work done here since we have
//    potentially corrupted process. Our job is to spawn another instance
//    of chrome which will show a 'chrome has crashed' dialog. This code needs
//    to live in the exe and thus has no access to facilities such as the
//    i18n helpers.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
#if defined(H2D2_WINDOWS)
static bool dumpCallback(const wchar_t* dump_path,
                         const wchar_t* minidump_id,
                         void* context,
                         EXCEPTION_POINTERS* exinfo,
                         MDRawAssertionInfo* assertion,
                         bool succeeded)
{
   printf("H2D2 crash report - Dumping\n");
   printf("   path: %S\n", dump_path);
   printf("   file: %S\n", minidump_id);

   // ---  Build command line
   std::wostringstream os;
   os << reporterPath.c_str() << L" \"" << dump_path << L"\" " << minidump_id << L".dmp" << std::ends;
   TTString commandLine = os.str();

   // ---  Create process
   LPWSTR commandLineP = const_cast<wchar_t*>(commandLine.c_str());
   STARTUPINFOW        si = {sizeof(si)};
   PROCESS_INFORMATION pi;
   if (::CreateProcessW(NULL,
                        commandLineP,
                        NULL, NULL, FALSE,
                        CREATE_UNICODE_ENVIRONMENT, NULL, NULL, &si, &pi))
   {
      ::CloseHandle(pi.hProcess);
      ::CloseHandle(pi.hThread);
   }
   else
   {
      LPVOID  lpMsgBuf;
      DWORD dw = GetLastError();

      FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );

        printf("CreateProcessW: %s \n", lpMsgBuf);
   }

   return succeeded;
}
#elif defined(H2D2_UNIX)
static bool dumpCallback(const char* dump_path,
                         const char* minidump_id,
                         void*       /*context*/,
                         bool        succeeded)
{
   printf("H2D2 crash report - Dumping\n");
   printf("   path: %s\n", dump_path);
   printf("   file: %s\n", minidump_id);
   return succeeded;
}
#endif

//************************************************************************
// Sommaire:   Initialise le mini-dump.
//
// Description:
//    La fonction <code>C_GBP_INIT</code> initialise le système de
//    mini-dump. Il crée l'"exception handler"
//
// Entrée:
//
// Sortie:
//
// Notes:
//    On utilise pas l'option "out of process" car elle nécessite un
//    process externe.
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_GBP_INIT()
{
   assert(ehP == NULL);

   // ---  Retourne si H2D2_DUMP n'est pas défini
   if (getenv("H2D2_DUMP") == NULL) return 0;

   // ---  Charge le chemin du crash_reporter ici pour faire le moins
   // ---  de traitement possible dans dumpCallback
   getReporterPath(reporterPath);

   // ---  Les path des dumps et du checkpoint
   TTString dumpPath;
   TTString checkpointPath;   // non utilisé
   getDumpPath(dumpPath, checkpointPath);

   // ---  Crée l'exception handler
   ehP = new gbp::ExceptionHandler(dumpPath,
                                   &filterCallback,   // pre  CB
                                   &dumpCallback,     // post CB
                                   NULL,              // callback_context
                                   gbp::ExceptionHandler::HANDLER_ALL);
   return 0;
}

//************************************************************************
// Sommaire:   Décharge n module.
//
// Description:
//    La fonction <code>C_GBP_RELEASE</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_GBP_RELEASE()
{
    if (ehP == NULL)
    {
       delete ehP;
       ehP = NULL;
    }
    return 0;
}


//************************************************************************
//************************************************************************
//************************************************************************
#ifdef C_GBP_MAIN
void crash()
{
  volatile int* a = (int*)(NULL);
  *a = 1;
}
int main(int argc, char* argv[])
{
    int iret = C_GBP_INIT();
    crash();
    return 0;
}
#endif

