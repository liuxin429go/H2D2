//************************************************************************
// H2D2 - External declaration of public symbols
// Module: algo_remesh
// Entry point: extern "C" void fake_dll_algo_remesh()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:46:54.929566
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class GR_RMSH
F_PROT(GR_RMSH_000, gr_rmsh_000);
F_PROT(GR_RMSH_999, gr_rmsh_999);
F_PROT(GR_RMSH_CTR, gr_rmsh_ctr);
F_PROT(GR_RMSH_DTR, gr_rmsh_dtr);
F_PROT(GR_RMSH_INI, gr_rmsh_ini);
F_PROT(GR_RMSH_RST, gr_rmsh_rst);
F_PROT(GR_RMSH_REQHBASE, gr_rmsh_reqhbase);
F_PROT(GR_RMSH_HVALIDE, gr_rmsh_hvalide);
F_PROT(GR_RMSH_XEQ, gr_rmsh_xeq);
F_PROT(GR_RMSH_ASGGRID, gr_rmsh_asggrid);
F_PROT(GR_RMSH_ASGMTRX, gr_rmsh_asgmtrx);
F_PROT(GR_RMSH_AJTMTRX, gr_rmsh_ajtmtrx);
F_PROT(GR_RMSH_ADAPT, gr_rmsh_adapt);
F_PROT(GR_RMSH_REQGRID, gr_rmsh_reqgrid);
F_PROT(GR_RMSH_REQLIMT, gr_rmsh_reqlimt);
 
// ---  class IC_GR_RMSH
F_PROT(IC_GR_RMSH_XEQCTR, ic_gr_rmsh_xeqctr);
F_PROT(IC_GR_RMSH_XEQMTH, ic_gr_rmsh_xeqmth);
F_PROT(IC_GR_RMSH_REQCLS, ic_gr_rmsh_reqcls);
F_PROT(IC_GR_RMSH_REQHDL, ic_gr_rmsh_reqhdl);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_algo_remesh()
{
   static char libname[] = "algo_remesh";
 
   // ---  class GR_RMSH
   F_RGST(GR_RMSH_000, gr_rmsh_000, libname);
   F_RGST(GR_RMSH_999, gr_rmsh_999, libname);
   F_RGST(GR_RMSH_CTR, gr_rmsh_ctr, libname);
   F_RGST(GR_RMSH_DTR, gr_rmsh_dtr, libname);
   F_RGST(GR_RMSH_INI, gr_rmsh_ini, libname);
   F_RGST(GR_RMSH_RST, gr_rmsh_rst, libname);
   F_RGST(GR_RMSH_REQHBASE, gr_rmsh_reqhbase, libname);
   F_RGST(GR_RMSH_HVALIDE, gr_rmsh_hvalide, libname);
   F_RGST(GR_RMSH_XEQ, gr_rmsh_xeq, libname);
   F_RGST(GR_RMSH_ASGGRID, gr_rmsh_asggrid, libname);
   F_RGST(GR_RMSH_ASGMTRX, gr_rmsh_asgmtrx, libname);
   F_RGST(GR_RMSH_AJTMTRX, gr_rmsh_ajtmtrx, libname);
   F_RGST(GR_RMSH_ADAPT, gr_rmsh_adapt, libname);
   F_RGST(GR_RMSH_REQGRID, gr_rmsh_reqgrid, libname);
   F_RGST(GR_RMSH_REQLIMT, gr_rmsh_reqlimt, libname);
 
   // ---  class IC_GR_RMSH
   F_RGST(IC_GR_RMSH_XEQCTR, ic_gr_rmsh_xeqctr, libname);
   F_RGST(IC_GR_RMSH_XEQMTH, ic_gr_rmsh_xeqmth, libname);
   F_RGST(IC_GR_RMSH_REQCLS, ic_gr_rmsh_reqcls, libname);
   F_RGST(IC_GR_RMSH_REQHDL, ic_gr_rmsh_reqhdl, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
