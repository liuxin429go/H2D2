#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
import sys 
import subprocess

def xeqRecursion(dir):
    for f in os.listdir(dir):
        fullPath = os.path.join(dir,f)
        fullPath = os.path.normpath(fullPath)
        if os.path.isdir(fullPath):
            xeqRecursion(fullPath)
        elif (f == 'bld_h2d2_cmd_lst.py'):
            xeqAction(fullPath)
            
def xeqAction(pth):
    print('Path: %s' % pth)
    dir, fic = os.path.split(pth)
    c = 'cd %s && python %s' % (dir, fic)

    retcode = subprocess.call(c, shell=True)
    
def main(argv = None):
    if (argv == None): argv = sys.argv[1:]
    if (len(argv) == 0): argv.append('.')
    for a in argv:
        xeqRecursion(a)

main()
