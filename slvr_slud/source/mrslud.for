C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUD_000()
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_000

      IMPLICIT NONE

      INCLUDE 'mrslud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslud.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_SLUD_NOBJMAX,
     &                   MR_SLUD_HBASE,
     &                   'SuperLU - Distributed')

      MR_SLUD_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUD_999()
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_999

      IMPLICIT NONE

      INCLUDE 'mrslud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslud.fc'

      INTEGER  IERR
      EXTERNAL MR_SLUD_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_SLUD_NOBJMAX,
     &                   MR_SLUD_HBASE,
     &                   MR_SLUD_DTR)

      MR_SLUD_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SLUD_CTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_CTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslud.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_SLUD_NOBJMAX,
     &                   MR_SLUD_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_SLUD_HVALIDE(HOBJ))
         IOB = HOBJ - MR_SLUD_HBASE

         MR_SLUD_XSLU(IOB) = 0
         MR_SLUD_HMTX(IOB) = 0
      ENDIF

      MR_SLUD_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SLUD_DTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_DTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslud.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_SLUD_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_SLUD_NOBJMAX,
     &                   MR_SLUD_HBASE)

      MR_SLUD_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensione
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SLUD_INI(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_INI

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mrslud.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ILU
      INTEGER HMTX
      PARAMETER (ILU = 0)
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MR_SLUD_RST(HOBJ)

C---     INITIALISE MUMPS
      IF (ERR_GOOD()) THEN ????
         IRET = F_MMPS_INITIALIZE()
         IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
      ENDIF

C---     INITIALISE LA MATRICE DISTRIBUÉE
      HMTX = 0
      IF (ERR_GOOD()) IERR = MX_DIST_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_DIST_INI(HMTX,MX_DIST_NUM_GLOBAL,.TRUE.) !! Avec données

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_SLUD_HBASE
         MR_SLUD_XSLU(IOB) = 0
         MR_SLUD_HMTX(IOB) = HMTX
      ENDIF

      MR_SLUD_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SLUD_RST(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_RST

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslud.fi'
      INCLUDE 'c_slud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mrslud.fc'

      INTEGER*8 XSLU       ! handle eXterne
      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - MR_SLUD_HBASE
      HMTX = MR_MMPS_HMTX(IOB)

C---     DETRUIS LA MATRICE DISTRIBUEE
      IF (MX_DIST_HVALIDE(HMTX)) IERR = MX_DIST_DTR(HMTX)

C---     DETRUIS LE SOLVEUR
      IF (MR_SLUD_XSLUS(IOB) .NE. 0) THEN
         IRET = F_MMPS_MAT_DESTROY(MR_MMPS_HMPS(IOB))
         IF (IRET .NE. 0) IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
      ENDIF

C---     RESET SuperLU
      XSLU = MR_SLUD_XSLU(IOB)
      IF (XSLU .NE. 0) IERR = C_SLUD_RESET(XSLU)

C---     RESET LES ATTRIBUTS
      MR_SLUD_XSLU(IOB) = 0
      MR_SLUD_HMTX(IOB) = 0

      MR_SLUD_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MR_SLUD_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUD_REQHBASE()
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_REQHBASE

      IMPLICIT NONE

      INCLUDE 'mrslud.fi'
      INCLUDE 'mrslud.fc'
C------------------------------------------------------------------------

      MR_SLUD_REQHBASE = MR_SLUD_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_SLUD_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUD_HVALIDE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_HVALIDE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrslud.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrslud.fc'
C------------------------------------------------------------------------

      MR_SLUD_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_SLUD_NOBJMAX,
     &                                  MR_SLUD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la méthode de résolution est directe
C
C Description:
C     La fonction MR_SLUD_ESTDIRECTE retourne .TRUE. si la méthode de
C     résolution est directe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUD_ESTDIRECTE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_ESTDIRECTE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrsludfi'
C------------------------------------------------------------------------

      MR_SLUD_ESTDIRECTE = .TRUE.
      RETURN
      END

C************************************************************************
C Sommaire: Assemble le système matriciel.
C
C Description:
C     La fonction MR_SLUD_ASMMTX assemble le système matriciel.
C     La matrice est dimensionnée et assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur la simulation
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUD_ASMMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_K,
     &                        F_KU)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_ASMMTX

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'mrslud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mrslud.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HASM
      INTEGER HMTX

      EXTERNAL MX_DIST_ASMKE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Demarre le chrono
      CALL TMR_START('h2d2.reso.mat.assemblage')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_SLUD_HBASE
      HMTX  = MR_SLUD_HMTX(IOB)
D     CALL ERR_ASR(MX_DIST_HVALIDE(HMTX))

C---     DIMENSIONNE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_DIST_DIMMAT(HMTX, HSIM)

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = F_K(HALG, HMTX, MX_DIST_ASMKE)

      CALL TMR_STOP('h2d2.reso.mat.assemblage')
      MR_SLUD_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise le système matriciel
C
C Description:
C     La fonction MR_SLUD_FCTMTX factorise le système matriciel. La matrice
C     doit être assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUD_FCTMTX(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_FCTMTX

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'mrslud.fi'
      INCLUDE 'c_slud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrslud.fc'

      INTEGER*8 XSLU       ! handle eXterne
      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER KA(1)
      INTEGER LIAP
      INTEGER LJAP
      INTEGER LKG
      INTEGER NEQ
      INTEGER NKGP
      LOGICAL ESTCCS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Demarre le chrono
      CALL TMR_START('h2d2.reso.mat.factorisation')

C---     Récupère les attributs
      IOB = HOBJ - MR_SLUD_HBASE
      XSLU = MR_SLUD_XSLU(IOB)
      HMTX = MR_SLUD_HMTX(IOB)
D     CALL ERR_ASR(MX_DIST_HVALIDE(HMTX))

C---     Récupère les attributs de la matrice distribuée
      IF (ERR_GOOD()) THEN
         LLCGL = MX_DIST_REQLLCGL(HMTX)
         LGLPP = MX_DIST_REQLGLPP(HMTX)
         NEQT  = MX_DIST_REQNEQT (HMTX)
      ENDIF

C---     Récupère les attributs de la matrice morse
      IF (ERR_GOOD()) THEN
         HMMR = MX_DIST_REQHMORS(HMTX)
D        CALL ERR_ASR(MX_MORS_HVALIDE(HMMR))
         NEQ  = MX_MORS_REQDIM (HMMR)
D        CALL ERR_ASR(NEQ .EQ. MX_DIST_REQDIM(HMTX))
         NKGP = MX_MORS_REQNKGP(HMMR)
         LIAP = MX_MORS_REQLIAP(HMMR)
         LJAP = MX_MORS_REQLJAP(HMMR)
         LKG  = MX_MORS_REQLKG (HMMR)
      ENDIF

C---     Crée la matrice SuperLU
      IF (XSLU .EQ. 0) THEN
         ESTCCS = .FALSE.
         IERR = C_SLUD_MAT_CREATE_NIT(XSLU, NEQ, ESTCCS)
         IF (IERR .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_SUPERLU_INIT')
         IF (ERR_GOOD()) MR_SLUD_XSLU(IOB) = XSLU
      ENDIF

C---     Factorise
      IF (ERR_GOOD()) THEN
         IERR = C_SLUD_FACT (XSLU,
     &                       NEQ,
     &                       NKGP,
     &                       VA(SO_ALLC_REQVIND(VA,LKG)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)))
         IF (IERR .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_SUPERLU_FACT')
      ENDIF

      CALL TMR_STOP('h2d2.reso.mat.factorisation')
      MR_SLUD_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Resous le système matriciel
C
C Description:
C     La fonction MR_SLUD_RESMTX résoud le système matriciel avec
C     un second membre. Le second membre est assemblé si les handles
C     sur la simulation HSIM et sur l'algorithme HALG sont tous deux non nuls,
C     sinon on résoud directement avec VSOL.
C     La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur la simulation
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du systeme matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUD_RESMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_F,
     &                        VSOL)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_RESMTX

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_F

      INCLUDE 'mrslud.fi'
      INCLUDE 'c_slud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mrslud.fc'

      INTEGER*8 XSLU       ! handle eXterne
      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX, HASM
      INTEGER NEQ
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Demarre le chrono
      CALL TMR_START('h2d2.reso.mat.resolution')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_SLUD_HBASE
      HMTX = MR_SLUD_HMTX(IOB)
      HASM = MR_SLUD_HASM (IOB)
      XSLU = MR_SLUD_XSLU(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))
D     CALL ERR_ASR(XSLU .NE. 0)

C---     RECUPERE LES DONNEES
      NEQ = MX_MORS_REQDIM (HMTX)

C---     ASSEMBLE LE MEMBRE DE DROITE
      IF (ERR_GOOD() .AND. HSIM .NE. 0 .AND HALG .NE. 0)
     &   IERR = MA_MORS_ASMRHS(HASM, HSIM, HALG, F_F, VSOL)

C---     RESOUD
      IF (ERR_GOOD()) THEN
         IERR = C_SLUD_SOLVE (XSLU,
     &                        NEQ,
     &                        1,
     &                        VFG)
         IF (IERR .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_SUPERLU_SOLVE')
      ENDIF

      CALL TMR_STOP('h2d2.reso.mat.resolution')
      MR_SLUD_RESMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résoud le système matriciel.
C
C Description:
C     La fonction MR_SLUD_XEQ resoud complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur la simulation
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du systeme matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_SLUD_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SLUD_XEQ

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrslud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrslud.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SLUD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ASSEMBLE LA MATRICE
      IF(ERR_GOOD())IERR=MR_SLUD_ASMMTX(HOBJ,HSIM,HALG,F_K,F_KU)

C---     FACTORISE LA MATRICE
      IF(ERR_GOOD())IERR=MR_SLUD_FCTMTX(HOBJ)

C---     RESOUS
      IF(ERR_GOOD())IERR=MR_SLUD_RESMTX(HOBJ,HSIM,HALG,F_F,VSOL)

      MR_SLUD_XEQ = ERR_TYP()
      RETURN
      END
