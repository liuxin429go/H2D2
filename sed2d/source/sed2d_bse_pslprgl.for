C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SED2D_BSE_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_BSE_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BSE_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sed2d.fi'

      INTEGER I, ID
C-----------------------------------------------------------------------

C---     CONTROLE DE POSITIVITE
      IERR = 0
      DO I=1,20
         IF (VPRGL(I) .LT. ZERO) THEN
            WRITE(LOG_BUF,'(2A,I1,A,1PE14.6E3)')
     &         'ERR_PROP_GLOB_NEGATIVE', ': V(', I, ')= ', VPRGL(I)
            CALL LOG_ECRIS(LOG_BUF)
            IERR = -1
         ENDIF
      ENDDO
      IF (IERR .EQ. -1) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_PROP_GLOB_NEGATIVE')
      ENDIF

C---     EMPIRICAL FORMULA TO COMPUTE TOTAL STRESS
      ID = NINT(VPRGL(17))
      IF     (ID .EQ. 1) THEN
      ELSEIF (ID .EQ. 2) THEN
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_INVALID_TOTAL_STRESS_FORMULA')
      ENDIF

C---     EMPIRICAL FORMULA TO COMPUTE EFFECTIVE STRESS
      ID = NINT(VPRGL(18))
      IF     (ID .EQ. 1) THEN
      ELSEIF (ID .EQ. 2) THEN
      ELSEIF (ID .EQ. 3) THEN
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_INVALID_EFFECTIVE_STRESS_FORMULA')
      ENDIF

C---     INITIALIZE THE TRANSPORT RATE EQUATION EQUATION
      ID = NINT(VPRGL(20))
      IF     (ID .EQ. 1) THEN
      ELSEIF (ID .EQ. 2) THEN
      ELSEIF (ID .EQ. 3) THEN
      ELSEIF (ID .EQ. 4) THEN
      ELSEIF (ID .EQ. 5) THEN
      ELSEIF (ID .EQ. 6) THEN
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_INVALID_BLEQ')
      ENDIF

      IERR = ERR_TYP()
      RETURN
      END

