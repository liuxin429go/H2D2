C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_MSS_PRNPRGL
C
C Description:
C     IMPRESSION DES PROPRIÉTÉS GLOBALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_MSS_PRNPRGL(NPRGL, VPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PRNPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INTEGER NPRGL
      REAL*8  VPRGL(NPRGL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sed2d.fi'

      INTEGER I,ID
      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPRGL .GT. 18)
C-----------------------------------------------------------------------

C---     IMPRESSION DU TITRE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_BEDLOAD_SEDIMENT_TRANSPORT:')

C---     IMPRESSION DE L'INFO
      CALL LOG_INCIND()
      I = 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_GRAVITY',VPRGL(I))

C---     PROPERTIES FOR WATER
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_WATER_DENSITY',VPRGL(I))
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_WATER_VISCOSITY',VPRGL(I))

C---     PROPERTIES FOR SEDIMENT
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_SEDIMENT_DENSITY',VPRGL(I))
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_BED_POROSITY',VPRGL(I))

C---     ACTIVE LAYER THICKNESS
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_ACTIVE_LAYER_THICK',VPRGL(I))

C---     NUMERICAL VISCOSITY - TAI
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_VISCOSITY_TAI',VPRGL(I))

C---     NUMERICAL VISCOSITY - DZC
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_VISCOSITY_Z_CRI_SLOPE',VPRGL(I))

C---     NUMERICAL VISCOSITY - TSI
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_VISCOSITY_TSI',VPRGL(I))

C---     NUMERICAL VISCOSITY - DTAI
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_VISCOSITY_DTAI',VPRGL(I))

C---     NUMERICAL VISCOSITY - DTSI
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_VISCOSITY_DTSI',VPRGL(I))

C---     DAMPING - TAI
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_DAMPING_TAI',VPRGL(I))

C---     DAMPING - TSI
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_DAMPING_TSI',VPRGL(I))

C---     EXCHANGE PARAMETER
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_EXCH_PARAMETER',VPRGL(I))

C---     BED SHEAR DIRECTION CORRECTION FOR HELICAL FLOW
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_CORRECT_HELIC_FLOW',VPRGL(I))

C---     BED SHEAR DIRECTION CORRECTION FOR BED SLOPE
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_CORRECT_BEDSLOPE',VPRGL(I))

C---     TOTAL SHEAR STRESS EQUATION
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_TOTAL_SHEAR_EQ',VPRGL(I))

C---     EFFECTIVE SHEAR STRESS EQUATION
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_EFFECTIVE_SHEAR_EQ',VPRGL(I))

C---     DIMENSIONLESS CRITICAL SHEAR STRESS
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_TAUSTR_CRITICAL',VPRGL(I))

C---     TRANSPORT RATE EQUATION
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_BEDLOAD_EQ_TYPE',VPRGL(I))

C---     MINIMAL SUB-LAYER THICKNESS
      I = I + 1
      IERR = SED2D_BSE_PRN1PRGL(I,'MSG_MIN_SUBLAYER_THICKNESS',VPRGL(I))

C---     SEDIMENT SIZES
      DO ID = 1,LM_CMMN_NPRGL - 21 !!SL 20
         I = I + 1
         IERR = SED2D_BSE_PRN1PRGL(I,'MSG_SEDIMENT_SIZE_PHI_UNITS',
     &                             VPRGL(I))
      ENDDO
      CALL LOG_DECIND()

      RETURN
      END
