C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_EXI
C
C Description:
C     computes interfacial grain size fractions
C
C Entrée:
C      REAL*8  TL1          ! thickness of size class per square meter of 1st layer
C      REAL*8  TL2          ! thickness of size class per square meter of 2nd layer
C      REAL*8  TSUBL        ! total thickness of sublayer
C      REAL*8  QB           ! volume of size class transported sediment
C      REAL*8  DZ           ! change in bed elevation (degrading or aggrading)
C
C Sortie:
C     REAL*8 EX            ! mass of size class exchanged sediments
C
C Notes: 1) need fraction of transport for exchange rate in sedimentation case
C************************************************************************
      FUNCTION SED2D_EXI(TLA,
     &                   TLS,
     &                   TSUBL,
     &                   QB,!P QBI for correction sedimentation case see note 1
     &                   DZ)

      IMPLICIT NONE

      REAL*8  TLA,TLS,TSUBL
      REAL*8  QB
      REAL*8  DZ !div(qs)

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d_exi.fc'
      INCLUDE 'log.fi'

      REAL*8  EX
C-----------------------------------------------------------------------

C--- IF THE BED IS DEGRADING
C--- SEDIMENT IS TAKEN FROM THE SUBLAYER AND PUT IN THE ACTIVE LAYER
C--- MASS DEPENDS ON RELATIVE PRESENCE IN THE 2ND LAYER
      IF (DZ .LE. ZERO)THEN
         EX  = TLS/TSUBL
C--- IF THE BED IS AGRADING
C--- SEDIMENT IS TAKEN FROM ACTIVE LAYER OR TRANSPORT AND STORED IN SUBLAYER
C--- MASS DEPENDS ON RELATIVE PRESENCE IN ACTIVE LAYER
      ELSE
!P         IF (QB .LE. ZERO) THEN
!P            EX = QBI*(UN - SED2D_ALPHA) + SED2D_ALPHA * TLA/SED2D_LA
!P         ELSE
!P            EX = QBI/QB*(UN - SED2D_ALPHA) + SED2D_ALPHA * TLA/SED2D_LA
!P         ENDIF
          EX  = QB*(UN - SED2D_ALPHA) + SED2D_ALPHA * TLA/SED2D_LA !P QB should be FQBI see note 1
      ENDIF

C--- CHECK VALUE OF EX-SIZE 0<EX<1
D     CALL ERR_PST(EX .GE. ZERO .AND. EX .LE. UN)

      SED2D_EXI = EX
      RETURN
      END
