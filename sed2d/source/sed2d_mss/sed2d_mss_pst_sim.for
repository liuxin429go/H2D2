C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: SED2D_MSS_PST
C     INTEGER SED2D_MSS_PST_SIM_000
C     INTEGER SED2D_MSS_PST_SIM_999
C     INTEGER SED2D_MSS_PST_SIM_CTR
C     INTEGER SED2D_MSS_PST_SIM_DTR
C     INTEGER SED2D_MSS_PST_SIM_INI
C     INTEGER SED2D_MSS_PST_SIM_RST
C     INTEGER SED2D_MSS_PST_SIM_REQHBASE
C     LOGICAL SED2D_MSS_PST_SIM_HVALIDE
C     INTEGER SED2D_MSS_PST_SIM_ACC
C     INTEGER SED2D_MSS_PST_SIM_FIN
C     INTEGER SED2D_MSS_PST_SIM_XEQ
C     INTEGER SED2D_MSS_PST_SIM_ASGHSIM
C     INTEGER SED2D_MSS_PST_SIM_REQHVNO
C     CHARACTER*256 SED2D_MSS_PST_SIM_REQNOMF
C     INTEGER SED2D_MSS_PST_SIM_LOG
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SED2D_MSS_PST_SIM_NOBJMAX,
     &                   SED2D_MSS_PST_SIM_HBASE,
     &                   'SED2D - Post-traitement Simulation')

      SED2D_MSS_PST_SIM_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER  IERR
      EXTERNAL SED2D_MSS_PST_SIM_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SED2D_MSS_PST_SIM_NOBJMAX,
     &                   SED2D_MSS_PST_SIM_HBASE,
     &                   SED2D_MSS_PST_SIM_DTR)

      SED2D_MSS_PST_SIM_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur par défaut de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SED2D_MSS_PST_SIM_NOBJMAX,
     &                   SED2D_MSS_PST_SIM_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
         IOB = HOBJ - SED2D_MSS_PST_SIM_HBASE

         SED2D_MSS_PST_SIM_HPRNT(IOB) = 0
      ENDIF

      SED2D_MSS_PST_SIM_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SED2D_MSS_PST_SIM_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SED2D_MSS_PST_SIM_NOBJMAX,
     &                   SED2D_MSS_PST_SIM_HBASE)

      SED2D_MSS_PST_SIM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL SED2D_MSS_PST_SIM_CLC, SED2D_MSS_PST_SIM_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SED2D_MSS_PST_SIM_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR=SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR=SO_FUNC_INIMTH(HFCLC,
     &                                    HOBJ,
     &                                    SED2D_MSS_PST_SIM_CLC)
      IF (ERR_GOOD()) IERR=SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR=SO_FUNC_INIMTH(HFLOG,
     &                                    HOBJ,
     &                                    SED2D_MSS_PST_SIM_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD()) IERR=PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD()) IERR=PS_SIMU_INI(HPRNT,
     &                                 HFCLC, HFLOG,
     &                                 NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - SED2D_MSS_PST_SIM_HBASE
         SED2D_MSS_PST_SIM_HPRNT(IOB) = HPRNT
      ENDIF

      SED2D_MSS_PST_SIM_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - SED2D_MSS_PST_SIM_HBASE
      HPRNT = SED2D_MSS_PST_SIM_HPRNT(IOB)

C---     Détruis les attributs
      IF (PS_SIMU_HVALIDE(HPRNT)) IERR = PS_SIMU_DTR(HPRNT)

C---     Reset
      SED2D_MSS_PST_SIM_HPRNT(IOB) = 0

      SED2D_MSS_PST_SIM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SED2D_MSS_PST_SIM_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'
C------------------------------------------------------------------------

      SED2D_MSS_PST_SIM_REQHBASE = SED2D_MSS_PST_SIM_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SED2D_MSS_PST_SIM_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'
C------------------------------------------------------------------------

      SED2D_MSS_PST_SIM_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                        SED2D_MSS_PST_SIM_NOBJMAX,
     &                                        SED2D_MSS_PST_SIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SED2D_MSS_PST_SIM_HPRNT(HOBJ - SED2D_MSS_PST_SIM_HBASE)
      SED2D_MSS_PST_SIM_ACC = PS_SIMU_ACC(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SED2D_MSS_PST_SIM_HPRNT(HOBJ - SED2D_MSS_PST_SIM_HBASE)
      SED2D_MSS_PST_SIM_FIN = PS_SIMU_FIN(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'hsdlib.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sed2d_msscnst.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IERR
      INTEGER HDLIB, HPRNT
      INTEGER NDLN, NPOST
      INCLUDE 'sed2d.fc'
!P      INTEGER NPOST
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIM))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Calcul NPOST
      HDLIB = HS_SIMD_REQHDLIB(HSIM)
      NDLN  = HS_DLIB_REQNDLN (HDLIB)
!P      NPOST = 3 + SED2D_MSSNLAYER*(NDLN-1)
!P      NPOST = 3 + SED2D_MSSNLAYER*(NDLN)
      NPOST = 3 + SED2D_MSSNLAYER*SED2D_MSSNCLASS

C---     Transfert l'appel au parent
      HPRNT = SED2D_MSS_PST_SIM_HPRNT(HOBJ - SED2D_MSS_PST_SIM_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, NPOST)

      SED2D_MSS_PST_SIM_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'hsdlib.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sed2d_msscnst.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IERR
      INTEGER HDLIB, HPRNT
      INTEGER NDLN, NPOST
      INCLUDE 'sed2d.fc'
!P      INTEGER NPOST
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIM))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Calcul NPOST
      HDLIB = HS_SIMD_REQHDLIB(HSIM)
      NDLN  = HS_DLIB_REQNDLN (HDLIB)
!P      NPOST = 3 + SED2D_MSSNLAYER*(NDLN-1)
      NPOST = 3 + SED2D_MSSNLAYER*SED2D_MSSNCLASS

C---     Transfert l'appel au parent
      HPRNT = SED2D_MSS_PST_SIM_HPRNT(HOBJ - SED2D_MSS_PST_SIM_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, NPOST)

      SED2D_MSS_PST_SIM_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_MSS_PST_SIM_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SED2D_MSS_PST_SIM_HPRNT(HOBJ-SED2D_MSS_PST_SIM_HBASE)
      SED2D_MSS_PST_SIM_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_MSS_PST_SIM_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PST_SIM_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SED2D_MSS_PST_SIM_HPRNT(HOBJ-SED2D_MSS_PST_SIM_HBASE)
      SED2D_MSS_PST_SIM_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée SED2D_MSS_PST_SIM_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_CLC(HOBJ,
     &                               HELE,
     &                               NPST,
     &                               NNL,
     &                               VPOST)

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELE
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IERR
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les données
      GDTA => LM_ELEM_REQGDTA(HELE)
      EDTA => LM_ELEM_REQEDTA(HELE)

C---     Fait le calcul
      IERR = SED2D_MSS_PST_SIM_CLC2(HOBJ,
     &                              GDTA%KNGV,
     &                              GDTA%VDJV,
     &                              EDTA%VPRNO,
     &                              EDTA%VPREV,
     &                              EDTA%VDLG,
     &                              NPST,
     &                              NNL,
     &                              VPOST)

      SED2D_MSS_PST_SIM_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée SED2D_MSS_PST_SIM_CLC fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_CLC2(HOBJ,
     &                           KNGV,
     &                           VDJV,
     &                           VPRNO,
     &                           VPREV,
     &                           VDLG,
     &                           NPST,
     &                           NNL,
     &                           VPOST)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER HOBJ
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_msscnst.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INCLUDE 'log.fi'

      INTEGER IN, IP, IL, ID
      INTEGER IDZ, IDTL2, IDQB
!P      INTEGER IDFLI, IDFL
      INTEGER IDTLI, IDTL
D       CALL LOG_TODO('PST_SIM_CLC2 NPST CHECK LIFTED')
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
!PD     CALL ERR_PRE(NPST .EQ. 3+SED2D_MSSNLAYER*(LM_CMMN_NDLN-1))
!PD     CALL ERR_PRE(NPST .EQ. 3+SED2D_MSSNLAYER*(LM_CMMN_NDLN))
D     CALL ERR_PRE(NPST .EQ. 3+SED2D_MSSNLAYER*SED2D_MSSNCLASS)
D     CALL ERR_PRE(NNL  .EQ. EG_CMMN_NNL)
C-----------------------------------------------------------------------

!P      IDZ   = LM_CMMN_NDLN
      IDZ   = 5
      IDTL2 = 5 + SED2D_MSSNCLASS
!P      IDTL2 = 6
!P      IDFLI = 6
      IDTLI = 5
!P      IDQB  = 7 + SED2D_MSSNCLASS*SED2D_MSSNLAYER
      IDQB  = 6 + SED2D_MSSNCLASS*SED2D_MSSNLAYER

C---     INITIALISATION DE VPOST
      CALL DINIT(NPST*NNL, ZERO, VPOST, 1)

C---     CHARGEMENT DE VPOST
      DO IN=1,NNL

!P         VPOST(1,IN) = VDLG (IDZ,  IN)    ! BED ELEVATION
         VPOST(1,IN) = VPRNO(IDZ,  IN)    ! BED ELEVATION PREVIOUS??
!P         VPOST(2,IN) = VPRNO(IDTL2,IN)    ! SUBLAYER THICKNESS
         VPOST(2,IN) = 0.0D+0
         DO ID = 1,SED2D_MSSNCLASS
            VPOST(2,IN) = VPOST(2,IN) + VPRNO(IDTL2+ID,IN)
         ENDDO
         VPOST(3,IN) = VPRNO(IDQB, IN)    ! SEDIMENT TRANSPORT RATE

C---        BED SIZELAYER FRACTIONS
         IP   = 3
!P         IDFL = IDFLI
         IDTL = IDTLI
         DO IL = 1, SED2D_MSSNLAYER
            DO ID = 1, SED2D_MSSNCLASS
               IP = IP + 1
!P               IDFL = IDFL + 1
               IDTL = IDTL + 1
!P               VPOST(IP,IN) = VPRNO(IDFL,IN)
               VPOST(IP,IN) = VPRNO(IDTL,IN)
D     WRITE(LOG_BUF, '(A,I5,2(A,I5,A,1PE15.8))')
D    &'IN: ',IN,' IP: ',IP, ' VPOST: ',
D    &VPOST(IP,IN),' IDTL: ',IDTL,' VPRNO: ',VPRNO(IDTL,IN)
D              CALL LOG_ECRIS(LOG_BUF)
            ENDDO
         ENDDO

      ENDDO


      SED2D_MSS_PST_SIM_CLC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_MSS_PST_SIM_LOG
C
C Description:
C     La fonction privée SED2D_MSS_PST_SIM_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_MSS_PST_SIM_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'sed2d_mss_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'sed2d_mss_pst_sim.fc'

      INTEGER IERR
      INTEGER IP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_MSS_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .GE. 3)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SED2D_MSSPOST_SIM:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<15>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)') 'MSG_VAL#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR,  1, NPST, NNL, VPOST, 'Z')
      IERR = PS_PSTD_LOGVAL(HNUMR,  2, NPST, NNL, VPOST, 'TL2')
      IERR = PS_PSTD_LOGABS(HNUMR,  3, NPST, NNL, VPOST, 'QB')
      DO IP=4,NPST
!P         IERR = PS_PSTD_LOGVAL(HNUMR, IP, NPST, NNL, VPOST, 'FLI')
         IERR = PS_PSTD_LOGVAL(HNUMR, IP, NPST, NNL, VPOST, 'TLI')
      ENDDO
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      SED2D_MSS_PST_SIM_LOG = ERR_TYP()
      RETURN
      END
