C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Assemble
C
C Description:
C     La fonction SED2D_MSS_ASGCMN copie les valeurs du common externe
C     passés en paramètre sous la forme d'un vecteur sur les valeurs
C     du common local à la DLL.
C
C Entrée:
C     KA       Les valeurs du common
C
C Sortie:
C
C Notes:
C     les common(s) sont privés au DLL, il faut donc propager les
C     changement entre l'externe et l'interne.
C
C************************************************************************
      FUNCTION SED2D_MSS_ASGCMN(EG_KA, EA_KA, EA_VA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_ASGCMN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER SED2D_MSS_ASGCMN
      INTEGER EG_KA(*)
      INTEGER EA_KA(*)
      REAL*8  EA_VA(*)

      INCLUDE 'err.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER I
C-----------------------------------------------------------------------

      DO I=1, EG_CMMN_KA_DIM
         EG_CMMN_KA(I) = EG_KA(I)
      ENDDO

      DO I=1, LM_CMMN_KA_DIM
         LM_CMMN_KA(I) = EA_KA(I)
      ENDDO

      DO I=1, LM_CMMN_VA_DIM
         LM_CMMN_VA(I) = EA_VA(I)
      ENDDO

      SED2D_MSS_ASGCMN = ERR_TYP()
      RETURN
      END
