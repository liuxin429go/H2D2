C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de sediment continuity 2-D
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SED2D_MGS8_REQPRM
C
C Description:
C     PARAMETRES DE L'ELEMENT
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_MGS8_REQPRM(TGELV,
     &                           TGELS,
     &                           NNELV,
     &                           NNELS,
     &                           NPRGL,
     &                           NPRGLL,
     &                           NPRNO,
     &                           NPRNOL,
     &                           NPREV,
     &                           NPREVL,
     &                           NPRES,
     &                           NSOLC,
     &                           NSOLCL,
     &                           NSOLR,
     &                           NSOLRL,
     &                           NDLN,
     &                           NDLEV,
     &                           NDLES,
     &                           ASURFACE,
     &                           ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MGS8_REQPRM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER TGELV
      INTEGER TGELS
      INTEGER NNELV
      INTEGER NNELS
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES
      LOGICAL ASURFACE
      LOGICAL ESTLIN

      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d.fc'

      INTEGER NCLASS
C-----------------------------------------------------------------------

C---     INITIALISE LES PARAMETRES DE L'ÉLÉMENT PARENT
      CALL SED2D_MSS_REQPRM(TGELV,
     &                     TGELS,
     &                     NNELV,
     &                     NNELS,
     &                     NPRGL,
     &                     NPRGLL,
     &                     NPRNO,
     &                     NPRNOL,
     &                     NPREV,
     &                     NPREVL,
     &                     NPRES,
     &                     NSOLC,
     &                     NSOLCL,
     &                     NSOLR,
     &                     NSOLRL,
     &                     NDLN,
     &                     NDLEV,
     &                     NDLES,
     &                     ASURFACE,
     &                     ESTLIN)

C---     INITIALISE LES PARAMETRES DE L'ELEMENT
      SED2D_NCLASS = 8
      NCLASS = SED2D_NCLASS
      NDLN   = NDLN + 2*NCLASS      ! NUMBER OF DEGREES OF FREEDOM
                                    ! PER NODE !P active and sub-layer
      NDLEV  = NDLN * NNELV         ! NUMBER OF DEGREES OF FREEDOM
                                    ! PER ELEMENT VOLUME
      NDLES  = NDLN * NNELS         ! NUMBER OF DEGREES OF FREEDOM
                                    ! PER ELEMENT SURFACE
      NPRGLL = NPRGLL + NCLASS      ! NUMBER OF GLOBAL PROPERTIES READ
      NPRGL  = NPRGLL               ! NUMBER OF GLOBAL PROPERTIES
      NPRNOL = NPRNOL +             ! NUMBER OF NODAL PROPERTIES READ
     &         NCLASS*SED2D_NLAYER  ! + TLI(NCLASS,NLAYER)
      NPRNO  = NPRNO                ! NUMBER OF NODAL PROPERTIES
     &         + NCLASS*SED2D_NLAYER! + TLI(NCLASS,NLAYER)
     &         + NCLASS             ! + VQBI(NCLASS)

      NSOLC  = NDLN                 ! NUMBER OF CONCENTERATED LOADS
      NSOLCL = NDLN                 ! NUMBER OF CONCENTERATED LOADS READ
      NSOLR  = 1                    ! NUMBER OF DISTRIBUTED LOADS
      NSOLRL = 1                    ! NUMBER OF DISTRIBUTED LOADS READ

      RETURN
      END
      