C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C     FUNCTION SED2D_BLEQ_TEST4
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes :
C************************************************************************

C************************************************************************
C Sommaire: Computes bedload transport rate - used for testing purpose
C
C Description:
C     The subroutine SED2D_BLEQ_TEST4 computes bedload transport rate
C     It is used for sed2d-sv2d link testing
C
C
C Entrée:
C
C
C Sortie:
C     REAL*8  QB
C
C Notes:
C  Reference:
C************************************************************************
      FUNCTION SED2D_BLEQ_TEST4(H,
     &                          UBAR,
     &                          USTR,
     &                          IN,
     &                          D50,
     &                          ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BLEQ_TEST4
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  H
      REAL*8  UBAR
      REAL*8  USTR
!!      REAL*8  N
      REAL*8  D50
      INTEGER ID
      INTEGER IN

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d_bleq_test4.fi'

      REAL*8 QB
      REAL*8 DQB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_BLEQ .EQ. SED2D_BLEQT_TEST4)
C-----------------------------------------------------------------------

C---     TRANSPORT RATE FOR TESTING SED2D-SV2D LINKING
!P      DQB = -SED2D_DMT(ID)/1.0D+03
      DQB = 1.0D-6
      QB  = 1.0D-03

      IF (IN .LE. 5) THEN
      ELSEIF (IN .LE. 10) THEN
         QB = QB + DQB*1**2
      ELSEIF (IN .LE. 15) THEN
         QB = QB + DQB*2**2
      ELSEIF (IN .LE. 20) THEN
         QB = QB + DQB*3**2
      ELSEIF (IN .LE. 25) THEN
         QB = QB + DQB*4**2
      ELSEIF (IN .LE. 30) THEN
         QB = QB + DQB*5**2
      ELSEIF (IN .LE. 35) THEN
         QB = QB + DQB*6**2
      ELSEIF (IN .LE. 40) THEN
         QB = QB + DQB*7**2
      ELSE
         QB = QB + DQB*8**2
      ENDIF
D     CALL ERR_ASR(QB .GE. 0.0D0)

      SED2D_BLEQ_TEST4 = QB
      RETURN
      END

C************************************************************************
C Sommaire : SED2D_BLEQ_TEST4_PARM
C
C Description:
C
C
C Entrée:
C
C
C Sortie:
C
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_BLEQ_TEST4_PARM (ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BLEQ_TEST4_PARM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ID
C-----------------------------------------------------------------------

      RETURN
      END
      