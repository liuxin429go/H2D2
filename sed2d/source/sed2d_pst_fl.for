C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER SED2D_PST_FL_000
C     INTEGER SED2D_PST_FL_999
C     INTEGER SED2D_PST_FL_CTR
C     INTEGER SED2D_PST_FL_DTR
C     INTEGER SED2D_PST_FL_INI
C     INTEGER SED2D_PST_FL_RST
C     INTEGER SED2D_PST_FL_REQHBASE
C     LOGICAL SED2D_PST_FL_HVALIDE
C     INTEGER SED2D_PST_FL_ACC
C     INTEGER SED2D_PST_FL_FIN
C     INTEGER SED2D_PST_FL_XEQ
C     INTEGER SED2D_PST_FL_ASGHSIM
C     INTEGER SED2D_PST_FL_REQHVNO
C     CHARACTER*256 SED2D_PST_FL_REQNOMF
C   Private:
C     INTEGER SED2D_PST_FL_CLC
C     INTEGER SED2D_PST_FL_LOG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_FL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SED2D_PST_FL_NOBJMAX,
     &                   SED2D_PST_FL_HBASE,
     &                   'SED2D - Post-treatment on Fraction Layer')

      SED2D_PST_FL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_FL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER  IERR
      EXTERNAL SED2D_PST_FL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SED2D_PST_FL_NOBJMAX,
     &                   SED2D_PST_FL_HBASE,
     &                   SED2D_PST_FL_DTR)

      SED2D_PST_FL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SED2D_PST_FL_NOBJMAX,
     &                   SED2D_PST_FL_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SED2D_PST_FL_HVALIDE(HOBJ))
         IOB = HOBJ - SED2D_PST_FL_HBASE

         SED2D_PST_FL_HSIM(IOB) = 0
         SED2D_PST_FL_KFL (1, IOB) = 0
      ENDIF

      SED2D_PST_FL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_FL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SED2D_PST_FL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SED2D_PST_FL_NOBJMAX,
     &                   SED2D_PST_FL_HBASE)

      SED2D_PST_FL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_INI(HOBJ, NFL, KFL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NFL
      INTEGER KFL(*)

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFL
      INTEGER IC
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_FL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôles
      IF (NFL .NE. SED2D_NLAYER) GOTO 9900
      DO IC=1,NFL
         HFL = KFL(IC)
         IF (.NOT. DT_VNOD_HVALIDE(HFL))  GOTO 9901
         IF (DT_VNOD_REQNVNO(HFL) .NE. 1) GOTO 9902
      ENDDO

C---     Reset les données
      IERR = SED2D_PST_FL_RST(HOBJ)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - SED2D_PST_FL_HBASE
         SED2D_PST_FL_HSIM(IOB) = 0
         DO IC=1,NFL
            SED2D_PST_FL_KFL(IC, IOB) = KFL(IC)
         ENDDO
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I3,A,I3)') 'ERR_NBR_CLASSES_INVALIDE', ': ',
     &                               NFL, '/', SED2D_NLAYER
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HFL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I3,A,I3)') 'ERR_NBR_VNO_INVALIDE', ': ',
     &                               DT_VNOD_REQNVNO(HFL), '/', 1
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SED2D_PST_FL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_FL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SED2D_PST_FL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SED2D_PST_FL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_FL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'
C------------------------------------------------------------------------

      SED2D_PST_FL_REQHBASE = SED2D_PST_FL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SED2D_PST_FL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_FL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'
C------------------------------------------------------------------------

      SED2D_PST_FL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       SED2D_PST_FL_NOBJMAX,
     &                                       SED2D_PST_FL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SED2D_PST_FL_ACC copie les données entre les PRNO
C     et les VNO qui composent l'objet. Elle devait permettre de
C     réactualiser les VNO qui construisent le PRNO, afin que lors
C     du prochaine chargement il n'y aie pas écrasement de données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_ACC
CDEC$ ENDIF

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'eacmmn.fc'

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_pst_fl.fc'

      REAL*8  TSIM
      INTEGER IERR
      INTEGER IOB
      INTEGER IL
      INTEGER HFL
      INTEGER HSIM, HFCLC, HNUMR
      INTEGER NNL
      INTEGER NPOST
      INTEGER L_PST
      EXTERNAL SED2D_PST_FL_CLC
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_FL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      NPOST = SED2D_NCLASS

C---     Récupère les attributs
      IOB  = HOBJ - SED2D_PST_FL_HBASE
      HSIM = SED2D_PST_FL_HSIM(IOB)

C---     Récupère les données de simulation
      TSIM  = LM_ELEM_REQTEMPS(HSIM)
      NNL   = LM_ELEM_REQPRM  (HSIM, LM_GEOM_PRM_NNL)
      HNUMR = LM_ELEM_REQHNUMC(HSIM)

C---     Alloue l'espace pour la solution
      L_PST = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NPOST*NNL, L_PST)

C---     Construit et initialise le call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,HOBJ,SED2D_PST_FL_CLC)

C---     Loop on layers
C        ==============
       DO IL=1,SED2D_NLAYER
         IF (ERR_BAD()) GOTO 199

C---        Handle sur le vno dest.
         HFL = SED2D_PST_FL_KFL(IL, IOB)
D        CALL ERR_ASR(DT_VNOD_HVALIDE(HFL))

C---        Contrôles
         IF (DT_VNOD_REQNNL (HFL) .NE. NNL)    GOTO 9900
         IF (DT_VNOD_REQNVNO(HFL) .NE. NPOST)  GOTO 9901

!C---        Calcul
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CALL(HFCLC,
     &                          HSIM, 
     &                          SO_ALLC_CST2B(IL),
     &                          SO_ALLC_CST2B(NPOST),
     &                          SO_ALLC_CST2B(NNL),
     &                          SO_ALLC_CST2B(L_PST,0_2))

C---        Met à jour les données
         IF (ERR_GOOD())
     &      IERR = DT_VNOD_ASGVALS (HFL,
     &                             HNUMR,
     &                             VA(SO_ALLC_REQVIND(VA, L_PST)),
     &                             TSIM)

      ENDDO
199   CONTINUE

C---     Détruit le call-back
      IF (SO_FUNC_HVALIDE(HFCLC)) IERR = SO_FUNC_DTR(HFCLC)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNL_INCOMPATIBLES', ': ',
     &                               DT_VNOD_REQNNL (HFL), '/', NNL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NVNO_INCOMPATIBLES', ': ',
     &                               DT_VNOD_REQNVNO(HFL), '/', NPOST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (L_PST .NE. 0) IERR = SO_ALLC_ALLRE8(0, L_PST)
      SED2D_PST_FL_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IL
      INTEGER HSIMD, HNUMR, HFL
      INTEGER NNL_D, NDLN_D
      INTEGER LVNO_D
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_FL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB   = HOBJ - SED2D_PST_FL_HBASE
      HSIMD = SED2D_PST_FL_HSIM(IOB)
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIMD))

C---     Récupère la numérotation
      HNUMR  = LM_ELEM_REQHNUMC(HSIMD)

C---     Loop on layers
C        ==============
       DO IL=1,SED2D_NLAYER
         IF (ERR_BAD()) GOTO 199

C---        Handle sur le vno dest.
         HFL = SED2D_PST_FL_KFL(IL, IOB)
D        CALL ERR_ASR(DT_VNOD_HVALIDE(HFL))

C---        Récupère les données du vno dest.
         NNL_D  = DT_VNOD_REQNNL (HFL)
         NDLN_D = DT_VNOD_REQNVNO(HFL)
         LVNO_D = DT_VNOD_REQLVNO(HFL)

C---        Log
         IERR = SED2D_PST_FL_LOG(HOBJ,
     &                           IL,
     &                           HNUMR,
     &                           NDLN_D,
     &                           NNL_D,
     &                           VA(SO_ALLC_REQVIND(VA,LVNO_D)))

      ENDDO
199   CONTINUE

      SED2D_PST_FL_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER IERR
      INTEGER HDLIB
      REAL*8  TSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_FL_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère le temps
      TSIM  = LM_ELEM_REQTEMPS(HSIM)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC   (HSIM)

C---     Conserve la simulation (dimensionne)
      IF (ERR_GOOD()) IERR = SED2D_PST_FL_ASGHSIM(HOBJ, HSIM)

C---     Accumule
      IF (ERR_GOOD()) IERR = SED2D_PST_FL_ACC(HOBJ)

C---     Finalise
      IF (ERR_GOOD()) IERR = SED2D_PST_FL_FIN(HOBJ)

      SED2D_PST_FL_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_FL_ASGHSIM
C
C Description:
C     La fonction SED2D_PST_FL_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_FL_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
D     IOB = HOBJ - SED2D_PST_FL_HBASE
D     CALL ERR_PRE(SED2D_PST_FL_HSIM(IOB) .EQ. 0 .OR.
D    &             SED2D_PST_FL_HSIM(IOB) .EQ. HSIM)
C------------------------------------------------------------------------

      IOB = HOBJ - SED2D_PST_FL_HBASE
      SED2D_PST_FL_HSIM(IOB) = HSIM

      SED2D_PST_FL_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_FL_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_FL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_REQ_INVALIDE')
      SED2D_PST_FL_REQHVNO = 0
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_FL_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_FL_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_FL_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_fl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_fl.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_FL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_REQ_INVALIDE')
      SED2D_PST_FL_REQNOMF = 'invalid file name'
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_PST_FL_CLC
C
C Description:
C     La fonction privée SED2D_PST_FL_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_FL_CLC(HOBJ,
     &                          HELM,
     &                          IL,
     &                          NPOST,
     &                          NNL,
     &                          VPOST)

      USE LM_ELEM_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'

      INTEGER HOBJ
      INTEGER HELM
      INTEGER IL
      INTEGER NPOST
      INTEGER NNL
      REAL*8  VPOST (NPOST, NNL)

      INCLUDE 'sed2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER IN, ID
      INTEGER IDBASE, IDFL
      INTEGER IDFLI
      PARAMETER (IDFLI = 6)
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
C     CALL ERR_ASR(NPOST .EQ. SED2D_NCLASS)
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA => LM_ELEM_REQEDTA(HELM)

      IDBASE = IDFLI + (IL - 1)*SED2D_NCLASS

C---     Initialisation de VPOST
      CALL DINIT(NPOST*NNL, ZERO, VPOST, 1)

C---     Chargement de VPOST
      DO IN=1,NNL
         DO ID=1,SED2D_NCLASS
            IDFL = IDBASE + ID
            VPOST(ID,IN) = EDTA%VPRNO(IDFL,IN)
         ENDDO
      END DO

      SED2D_PST_FL_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_PST_FL_LOG
C
C Description:
C     La fonction privée SED2D_PST_FL_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_FL_LOG(HOBJ, IL, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IL
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'eacmmn.fc'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_pst_fl.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPST .EQ. SED2D_NCLASS)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SED2D_POST_FL:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_FLAYER#<15>#:', IL
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<15>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)')    'MSG_VAL#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR,  1, NPST, NNL, VPOST, 'FL')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      SED2D_PST_FL_LOG = ERR_TYP()
      RETURN
      END
