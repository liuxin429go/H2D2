C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_FRC_PSCPRNO
C
C Description:
C     1) stores the computed z as nodal value
C     2) updates the bed layer thickness and size fractions
C
C Entrée: VCORG,KNG,VDJ,VPRGL,VPRNO
C
C Sortie: VPRNO
C
C Notes:
C  La boucle d'imposition des noeuds milieu devait être dans un PSCDLIB
C************************************************************************
      SUBROUTINE SED2D_FRC_PSCPRNO(VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             VDLG,
     &                             MODIF,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_FRC_PSCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      LOGICAL MODIF
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'

      REAL*8  FLI (SED2D_NLAYER, SED2D_NCLSMAX)
      REAL*8  FEXI(SED2D_NCLSMAX)
      REAL*8  FAI (SED2D_NCLSMAX)
      REAL*8  DELZ
      REAL*8  TL2
      INTEGER IC, IE
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      INTEGER ID,IL,IN,IDZ,IDZPRE,IDTL2,IDFLI,IDFQBI,IDFL,IX
C-----------------------------------------------------------------------

C---     INDICES
      IDZ       = LM_CMMN_NDLN
      IDZPRE    = 5
      IDTL2     = 6
      IDFLI     = 6
      IDFQBI    = 7 + SED2D_NCLASS*SED2D_NLAYER

C---     Impose les noeuds milieux linéaire
!$omp  parallel
!$omp& default(shared)
!$omp& private(IE, IC, ID)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1 = KNGV(1,IE)
         NO2 = KNGV(2,IE)
         NO3 = KNGV(3,IE)
         NO4 = KNGV(4,IE)
         NO5 = KNGV(5,IE)
         NO6 = KNGV(6,IE)

         DO ID=1,LM_CMMN_NDLN
            VDLG(ID,NO2) = UN_2*(VDLG(ID,NO1) + VDLG(ID,NO3))
            VDLG(ID,NO4) = UN_2*(VDLG(ID,NO3) + VDLG(ID,NO5))
            VDLG(ID,NO6) = UN_2*(VDLG(ID,NO5) + VDLG(ID,NO1))
         ENDDO
      ENDDO
!$omp end do
      ENDDO
!$omp end parallel

C---     LOOP OVER THE NODES
      DO IN = 1, EG_CMMN_NNL
C---        LIMIT ACTIVE LAYER GRAIN SIZE FRACTIONS TO BE POSITIVE
         DO ID = 1,SED2D_NCLASS
            FAI(ID) = VDLG(ID,IN)
         ENDDO
         IF (MAXVAL(FAI(1:SED2D_NCLASS)) .LE. ZERO) THEN
            IDFL = IDFLI + SED2D_NCLASS
            DO ID = 1,SED2D_NCLASS
               IDFL    = IDFL + 1
               FAI(ID) = VPRNO(IDFL,IN)
            ENDDO
         ELSE
            DO ID = 1,SED2D_NCLASS
               FAI(ID) = MAX(FAIMIN,VDLG(ID,IN))
            ENDDO
         ENDIF

C---        ENSURE SUM OF SURFACE GRAIN SIZE FRACTIONS TO BE 100%
         CALL SED2D_NORMALIZE(SED2D_NCLASS, FAI)
         DO ID = 1,SED2D_NCLASS
            VDLG(ID,IN) = FAI(ID)
         ENDDO

C---        UPDATE BED LAYER DATA
C---        PREVIOUS SIZELAYER DATA
         IDFL  = IDFLI
         DO IL = 1, SED2D_NLAYER
            DO ID = 1, SED2D_NCLASS
               IDFL       = IDFL + 1
               FLI(IL,ID) = VPRNO(IDFL,IN)
            ENDDO
         ENDDO

C---        PREVIOUS SUBLAYER THICKNESS
         TL2 =  VPRNO(IDTL2,IN)

C---        COMPUTE CHANGE IN BED ELEVATION
         DELZ = VDLG(IDZ,IN) - VPRNO(IDZPRE,IN)
D      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'DELZ: ',DELZ
D      CALL LOG_ECRIS(LOG_BUF)

C---        CALCULATE EXCHANGE RATE FRACTIONS
         DO ID = 1, SED2D_NCLASS
            FEXI(ID)   = SED2D_FEXI(FLI(1,ID),
     &                              FLI(2,ID),
     &                              VPRNO(IDFQBI+ID,IN),
     &                              DELZ)
         ENDDO

C---        UPDATE BED LAYER SIZE FRACTIONS
         CALL SED2D_UPDATE(SED2D_NCLASS,
     &                     FLI,
     &                     FEXI,
     &                     FAI,
     &                     TL2,
     &                     DELZ)

C---        STORE SIZELAYER DATA
         IDFL  = IDFLI
         DO IL = 1, SED2D_NLAYER
            DO ID = 1, SED2D_NCLASS
               IDFL = IDFL + 1
               VPRNO(IDFL,IN) = FLI(IL,ID)
            ENDDO
         ENDDO

C---        STORE SUB LAYER THICKNESS
         VPRNO(IDTL2,IN) = TL2

C---        UPDATE BED ELEVATION
         VPRNO(IDZPRE,IN) = VDLG(IDZ,IN)

      ENDDO

      MODIF = .TRUE.
      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_UPDATE
C
C Description:
C     1) updates the bed layer thickness and size fractions
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SED2D_UPDATE(NCLASS,
     &                        FL,
     &                        FEX,
     &                        FA,
     &                        T2,
     &                        DELZ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_UPDATE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_cnst.fi'

      INTEGER NCLASS
      REAL*8  FL (SED2D_NLAYER, NCLASS)
      REAL*8  FEX(NCLASS)
      REAL*8  FA (NCLASS)
      REAL*8  T2
      REAL*8  DELZ

      INCLUDE 'err.fi'
      INCLUDE 'sed2d.fi'

      REAL*8  STORE(SED2D_NCLSMAX)
      REAL*8  TOTSTORE
      REAL*8  TL2MAX
      INTEGER IERR
      INTEGER ID,IL
C-----------------------------------------------------------------------

C---     PARAMETERS
      TL2MAX = 1.5D0*SED2D_LA

C---    UPDATE SEDIMENT INFO FOR EACH BED LAYER
      TOTSTORE = 0.0D0
      IF (DELZ .GE. 0.0D0) THEN
        DO ID = 1,NCLASS
            STORE(ID) = T2*FL(2,ID) + DELZ*FEX(ID)
            TOTSTORE  = TOTSTORE+STORE(ID)
        ENDDO

C---       UPDATE 2ND LAYER SIZE FRACTIONS
        DO ID = 1,NCLASS
           FL(2,ID) = STORE(ID)/TOTSTORE
        ENDDO
      ENDIF

C---     UPDATE SUB LAYER THICKNESS
      T2 = T2 + DELZ

      IF (T2 .GT. TL2MAX) THEN
C---       UPDATE 3RD, 4TH, 5TH, ... LAYER SIZE FRACTIONS
        DO IL = SED2D_NLAYER,3,-1
           DO ID = 1,NCLASS
              FL(IL,ID) = FL(IL-1,ID)
           ENDDO
        ENDDO

C---       UPDATE SUB LAYER THICKNESS
        T2 = MIN(TL2MAX, T2 - SED2D_LA)

      ELSE IF(T2 .LT. 0.0D0) THEN    ! when 2nd layer vanishes

C---       UPDATE 2ND, 3RD, 4TH, 5TH, ... LAYER SIZE FRACTIONS
        DO IL = 2, SED2D_NLAYER-1
           DO ID = 1,NCLASS
              FL(IL,ID) = FL(IL+1,ID)
           ENDDO
        ENDDO

C---        UPDATE SUB LAYER THICKNESS
        T2 = MAX(TL2MIN,T2 + SED2D_LA)
      ENDIF

C---        UPDATE 1ST LAYER SIZE FRACTIONS
         DO ID = 1,NCLASS
            FL(1,ID) = FA(ID)
          ENDDO

      IERR = ERR_TYP()
      RETURN
      END