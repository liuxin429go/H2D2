C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_FRC_PRNPRNO
C
C Description:
C     PRINTING NODAL PROPERTIES:
C     1) qx
C     2) qy
C     3) H
C     4) n
C     5) zpre                                  idzpre    = 5
C     6) tl2                                   idtl2     = 6
C     6 + 1)                fli(1,1)
C     6 + i)                fli(i,1)-----------idfli1    = 6
C     6 + nc)               fli(nc,1)
C     6 + nc + 1)           fli(1,2)
C     6 + nc + i)           fli(i,2)-----------idfli2    = 6 + nc
C     6 + nc + nc)          fli(nc,2)
C
C     6 + (j-1)*nc + i)     fli(i,j)-----------idflij    = 6 + (j-1)*nc + i
C
C     6 + (nl-1)*nc + 1)    fli(1,nl)
C     6 + (nl-1)*nc + i)    fli(i,nl)----------idflinl    = 6 + nl*nc
C     6 + nl*nc)            fli(nc,nl)
c     7 + nl*nc)            qb-----------------idqb        = 7 + nl*nc
C     7 + nl*nc + 1)        fqbi(1)
C     7 + nl*nc + i)        fqbi(i)------------idfqbi    = 7 + nl*nc
C     7 + (nl+1)*nc)        fqbi(nc)
C     8 + (nl+1)*nc)        theta--------------idtheta   = 8 + nl*nc
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C
C************************************************************************
      SUBROUTINE SED2D_FRC_PRNPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_FRC_PRNPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     PRINTING NODAL PROPERTIES
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('NODAL PROPERTIES READ FROM SV2D:')
      CALL LOG_INCIND()
      CALL LOG_ECRIS('X-COMPONENT VELOCITY u;')
      CALL LOG_ECRIS('Y-COMPONENT VELOCITY v;')
      CALL LOG_ECRIS('WATER DEPTH H;')
      CALL LOG_ECRIS('MANNING ROUGHNESS n;')
      CALL LOG_DECIND()

      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('NODAL PROPERTIES CALCULATED:')
      CALL LOG_INCIND()
      CALL LOG_ECRIS('PREVIOUS BED ELEVATION, ZPRE;')
      CALL LOG_ECRIS('SUBLAYER THICKNESS, TL2;')
      CALL LOG_ECRIS('BED LAYER SIZE FRACTIONS, FLI;')
      CALL LOG_ECRIS('TOTAL TRANSPORT RATE, QB;')
      CALL LOG_ECRIS('TRANSPORT RATE FRACTIONS, FQBI;')
      CALL LOG_ECRIS('MEAN FLOW DIRECTION, THETA;')
      CALL LOG_DECIND()

      RETURN
      END
