C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SED2D_FRC_PRCPREV
C
C Description:
C     Computes direction of shear over each element and corrects
C     for helical flow effects using the procedures of Abad et al. (2008)
C
C Entrée:
C
C Sortie:
C     REAL*8 VPREV(1,EG_CMMN_NELV)     !deviation of shear direction due
C                                      !to helical flow and bed slope effects
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_FRC_PRCPREV(VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_FRC_PRCPREV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV(LM_CMMN_NPREV, EG_CMMN_NELV)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'

      REAL*8  VKX,VEX,VKY,VEY,VSX,VSY,DETJ
      REAL*8  FLI1(SED2D_NCLSMAX)
      REAL*8  U1,V1,H1,N1,ZPRE1,U2,V2,H2,N2,ZPRE2,U3,V3,H3,N3,ZPRE3
      REAL*8  U,V,H,N,UBAR2
      REAL*8  DUDX,DUDY,DVDX,DVDY,DZDX,DZDY
      REAL*8  RC1,RC2,R,DELTAH
      REAL*8  USTR,TAUSTR
      REAL*8  D50
      REAL*8  DELTAX,DELTAY,DELTA
      REAL*8  TROIS_DEUX
      REAL*8  UN_FS
      REAL*8  A
      REAL*8  RD_UN
      INTEGER IDFLI1,IDL1
      INTEGER IC,IE,ID
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
      CALL LOG_TODO('sed2d_frc_prcprev is deprecated. To be removed.')
      CALL ERR_ASR(.FALSE.)

      CALL SED2D_BSE_PRCPREV(VCORG,
     &                       KNGV,
     &                       VDJV,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       IERR)

C---     PARAMETERS
      IDFLI1      = 6

C-------  LOOP OVER THE ELEMENTS
C         =======================
      DO IC = 1,EG_CMMN_NELCOL
      DO IE = EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        ELEMENT CONNECTIVITY FOR T3
         NO1 = KNGV(1,IE)
         NO2 = KNGV(3,IE)
         NO3 = KNGV(5,IE)

C---        BED SHEAR DIRECTION CORRECTION FOR TRANSVERSAL BED SLOPE
C---        COMPUTE D50 FOR ACTIVE LAYER SIZE FRACTIONS
         DO ID = 1, SED2D_NCLASS
            IDL1      = IDFLI1+ID
            FLI1(ID)  = UN_3*(VPRNO(IDL1,NO1) +
     &                        VPRNO(IDL1,NO2) +
     &                        VPRNO(IDL1,NO3))
         ENDDO
         D50 = SED2D_SIZEPARM(SED2D_NCLASS,
     &                        0.5D0,
     &                        FLI1)

      ENDDO
      ENDDO

      IERR = ERR_TYP()
      RETURN
      END
