C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction SED2D_FRC_REQNFN retourne le nom de la fonction
C     associé au code demandé. Le nom combine le nom de la DLL et
C     de la fonction sous la forme "nomDll@nomFonction".
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     KFNC     Nom de la fonction stocké dans une table INTEGER
C
C Notes:
C************************************************************************
      FUNCTION SED2D_FRC_REQNFN(KFNC, IFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_FRC_REQNFN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER SED2D_FRC_REQNFN
      INTEGER KFNC(*)
      INTEGER IFNC

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'

      INTEGER        SED2D_BSE_REQNFN
      INTEGER        I
      INTEGER        IERR
      INTEGER        JF(16)
      CHARACTER*(64) NF
      EQUIVALENCE (NF, JF)
      LOGICAL        SAUTE
C-----------------------------------------------------------------------

      IERR = SED2D_BSE_REQNFN(KFNC, IFNC)
      IF (.NOT. ERR_GOOD()) GOTO 9999

      SAUTE = .FALSE.
      IF (IFNC .EQ. EA_FUNC_ASMF) THEN
                                    NF = 'SED2D_FRC_ASMF@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMK) THEN
                                    NF = 'SED2D_FRC_ASMK@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKT) THEN
                                    NF = 'SED2D_FRC_ASMKT@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKU) THEN
                                    NF = 'SED2D_FRC_ASMKU@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMM) THEN
                                    NF = 'SED2D_FRC_ASMM@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMMU) THEN
                                    NF = 'SED2D_FRC_ASMMU@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRGL) THEN
                                    NF = 'SED2D_FRC_PRCPRGL@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRNO) THEN
                                    NF = 'SED2D_FRC_PRCPRNO@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRGL) THEN
                                    NF = 'SED2D_FRC_PRNPRGL@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRNO) THEN
                                    NF = 'SED2D_FRC_PRNPRNO@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSCPRNO) THEN
                                    NF = 'SED2D_FRC_PSCPRNO@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPRNO) THEN
                                    NF = 'SED2D_BSE_PSLPRNO@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_REQNFN) THEN
                                    NF = 'SED2D_FRC_REQNFN@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_REQPRM) THEN
                                    NF = 'SED2D_FRC_REQPRM@sed2d'
      ELSE
!         CALL ERR_ASG(ERR_FTL, 'ERR_CODE_FNCT_INVALIDE')
         SAUTE = .TRUE.
      ENDIF

      IF (ERR_GOOD() .AND. .NOT. SAUTE) THEN
         DO I=1,16
            KFNC(I) = JF(I)
         ENDDO
      ENDIF

9999  CONTINUE

      SED2D_FRC_REQNFN = ERR_TYP()
      RETURN
      END
