C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de sediment continuity 2-D
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: Assigne les valeurs au common.
C
C Description:
C     La fonction SED2D_GS5_ASGCMN permet d'assigner les valeurs au common.
C
C     Chaque DLL possède une copie propre du common. Avant d'appeler une
C     fonctionnalité de la DLL, il faut mettre à jour le common. Cette
C     mécanique ne fonctionne que dans un contexte ou la DLL n'est utilisée
C     que par un seul process. De plus en multi-tâche, il faut que toute les
C     tâches utilises les mêmes valeurs de common.
C
C Entrée:
C     KA    Vecteur des valeurs du common
C
C Sortie:
C
C Notes:
C     Comme on utilise la DLL du parent (SED2D_FRC_nnn), celle-ci doit
C     également être mise à jour.
C************************************************************************
      FUNCTION SED2D_GS5_ASGCMN(EG_KA, EA_KA, EA_VA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_GS5_ASGCMN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER SED2D_GS5_ASGCMN
      INTEGER EG_KA(*)
      INTEGER EA_KA(*)
      REAL*8  EA_VA(*)

      INCLUDE 'err.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER I
      INTEGER IERR
      INTEGER SED2D_FRC_ASGCMN
C-----------------------------------------------------------------------

      DO I=1, EG_CMMN_KA_DIM
         EG_CMMN_KA(I) = EG_KA(I)
      ENDDO

      DO I=1, LM_CMMN_KA_DIM
         LM_CMMN_KA(I) = EA_KA(I)
      ENDDO

      DO I=1, LM_CMMN_VA_DIM
         LM_CMMN_VA(I) = EA_VA(I)
      ENDDO

      IERR = SED2D_FRC_ASGCMN(EG_KA, EA_KA, EA_VA)

      SED2D_GS5_ASGCMN = ERR_TYP()
      RETURN
      END
