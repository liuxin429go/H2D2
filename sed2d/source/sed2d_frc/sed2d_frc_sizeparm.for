C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_SIZEPARM
C
C Description:
C     Computes size of percentages for a grain-size distribution
C
C Entrée:
C
C Sortie:
C     REAL*8 SIZE
C
C Notes:
C************************************************************************
      FUNCTION SED2D_SIZEPARM(NCLASS, PC, SIZEDATA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_SIZEPARM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NCLASS
      REAL*8  SIZEDATA(NCLASS)
      REAL*8  PC

      INCLUDE 'sed2d.fi'

      REAL*8  CUMPC,SIZE,LOW,PHIPC
      INTEGER ID
C-----------------------------------------------------------------------

      CUMPC = 0.0D0
      DO ID = 1,NCLASS
         CUMPC = CUMPC+SIZEDATA(ID)
         IF (CUMPC .GT. PC) THEN
            LOW   = CUMPC-SIZEDATA(ID)
            PHIPC = SED2D_PHI(ID)+
     &              (PC-LOW)*(SED2D_PHI(ID+1)-SED2D_PHI(ID))/(CUMPC-LOW)
            SIZE  = 2.0D0**PHIPC/1000.0D0
            GO TO 199
         ENDIF
         IF (ID .EQ. 1 .AND. CUMPC .GT. PC) SIZE=0.0D0
      ENDDO
199   CONTINUE

      SED2D_SIZEPARM = SIZE
      RETURN
      END
   