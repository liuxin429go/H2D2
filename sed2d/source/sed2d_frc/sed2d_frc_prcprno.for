C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SED2D_FRC_PRCPRNO
C
C Description:
C     Pre-computing nodal properties.
C     Pre-computation doesn't depend on the degrees of freedom (VDLG).
C
C Entrée:
C      REAL*8    VCORG      Table des COoRdonnées Globales
C      INTEGER   KNGV       Table des coNectivités Globales de Volume
C      REAL*8    VDJV       Table des métriques (Determinant, Jacobien) de Volume
C      REAL*8    VPRGL      Table des PRopriétés GLobales
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Sortie:
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_FRC_PRCPRNO (VCORG,
     &                              KNGV,
     &                              VDJV,
     &                              VPRGL,
     &                              VPRNO,
     &                              IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_FRC_PRCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'sed2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_bleq_aw.fi'
      INCLUDE 'sed2d_bleq_wu.fi'
      INCLUDE 'sed2d_bleq_test.fi'
      INCLUDE 'sed2d_bleq_test2.fi'
      INCLUDE 'sed2d_bleq_test3.fi'
      INCLUDE 'sed2d_bleq_test4.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'log.fi'

      REAL*8  FLI1 (SED2D_NCLSMAX)
      REAL*8  QBI  (SED2D_NCLSMAX)
      REAL*8  QBAI (SED2D_NCLSMAX)
      REAL*8  U,V,H,N,NEFF,UBAR,THETA
      REAL*8  RD_UN,DSTR,USTR,USTR2,USTR2MIN,USTREFF
      REAL*8  D50
      REAL*8  QB
      REAL*8  VKX, VEX, VKY, VEY, DETJ
      REAL*8  ZP1, ZP2, ZP3, DZDX, DZDY
      REAL*8  UNRM, VNRM, TANLN, TANTR, R_DEY
      INTEGER IC, IE
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      INTEGER IN,ID,IDQB,IDFQBI,IDFLI1,IDFLI2,IDTHETA,IDZPRE
      INTEGER IZX, IZY, IML
C-----------------------------------------------------------------------

C---     INDICES
      IDZPRE    = 5
      IDFLI1    = 6
      IDFLI2    = IDFLI1 + SED2D_NCLASS
      IDQB      = 7 + SED2D_NCLASS*SED2D_NLAYER
      IDFQBI    = IDQB
      IDTHETA   = 8 + SED2D_NCLASS*(SED2D_NLAYER + 1)
      IZX       = LM_CMMN_NPRNO-2   ! squat pour le calcul de grad(z)
      IZY       = LM_CMMN_NPRNO-1
      IML       = LM_CMMN_NPRNO

C---     PARAMETERS
      RD_UN     = SED2D_RHOS / SED2D_RHO_EAU - UN

C---     Force ZPRE linéaire
      CALL DINIT(EG_CMMN_NNL, ZERO, VPRNO(IZX,1), LM_CMMN_NPRNO)
      CALL DINIT(EG_CMMN_NNL, ZERO, VPRNO(IZY,1), LM_CMMN_NPRNO)
      CALL DINIT(EG_CMMN_NNL, ZERO, VPRNO(IML,1), LM_CMMN_NPRNO)
!$omp  parallel
!$omp& default(shared)
!$omp& private(IE, IC)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

C---        ZPRE linéaire
         VPRNO(IDZPRE,NO2) = UN_2*(VPRNO(IDZPRE,NO1)+VPRNO(IDZPRE,NO3))
         VPRNO(IDZPRE,NO4) = UN_2*(VPRNO(IDZPRE,NO3)+VPRNO(IDZPRE,NO5))
         VPRNO(IDZPRE,NO6) = UN_2*(VPRNO(IDZPRE,NO5)+VPRNO(IDZPRE,NO1))

C---        Métriques du T3
         VKX = VDJV(1,IE)
         VEX = VDJV(2,IE)
         VKY = VDJV(3,IE)
         VEY = VDJV(4,IE)
         DETJ = VDJV(5,IE)

C---        Valeurs des sommets
         ZP1 = VPRNO(IDZPRE,NO1)
         ZP2 = VPRNO(IDZPRE,NO3)
         ZP3 = VPRNO(IDZPRE,NO5)

C---       Calcul des dérivées
         DZDX = VKX*(ZP2-ZP1) + VEX*(ZP3-ZP1)
         DZDY = VKY*(ZP2-ZP1) + VEY*(ZP3-ZP1)

C---        Assemble les dérivées
         VPRNO(IZX,NO1) = VPRNO(IZX,NO1) + DZDX ! * UN_6
         VPRNO(IZY,NO1) = VPRNO(IZY,NO1) + DZDY ! * UN_6
         VPRNO(IML,NO1) = VPRNO(IML,NO1) + DETJ ! * UN_6
         VPRNO(IZX,NO2) = VPRNO(IZX,NO2) + DZDX
         VPRNO(IZY,NO2) = VPRNO(IZY,NO2) + DZDY
         VPRNO(IML,NO2) = VPRNO(IML,NO2) + DETJ
         VPRNO(IZX,NO3) = VPRNO(IZX,NO3) + DZDX
         VPRNO(IZY,NO3) = VPRNO(IZY,NO3) + DZDY
         VPRNO(IML,NO3) = VPRNO(IML,NO3) + DETJ
         VPRNO(IZX,NO4) = VPRNO(IZX,NO4) + DZDX
         VPRNO(IZY,NO4) = VPRNO(IZY,NO4) + DZDY
         VPRNO(IML,NO4) = VPRNO(IML,NO4) + DETJ
         VPRNO(IZX,NO5) = VPRNO(IZX,NO5) + DZDX
         VPRNO(IZY,NO5) = VPRNO(IZY,NO5) + DZDY
         VPRNO(IML,NO5) = VPRNO(IML,NO5) + DETJ
         VPRNO(IZX,NO6) = VPRNO(IZX,NO6) + DZDX
         VPRNO(IZY,NO6) = VPRNO(IZY,NO6) + DZDY
         VPRNO(IML,NO6) = VPRNO(IML,NO6) + DETJ
      ENDDO
!$omp end do
      ENDDO
!$omp end parallel

C---     Loop on the nodes
      DO IN = 1,EG_CMMN_NNL

C---        Flow depth, flow velocity and roughness
         H     = MAX(PROFMIN,VPRNO(3,IN)- VPRNO(IDZPRE,IN))
!!         WRITE(LOG_BUF, '(A,1I,A,1PE14.6E3)')
!!     &   'IN: ', IN, ' VPRNO: ', VPRNO(IDZPRE,IN)
!!         CALL LOG_ECRIS(LOG_BUF)

         U     = VPRNO(1,IN) / H
         V     = VPRNO(2,IN) / H
         N     = VPRNO(4,IN)
         IF (H .GT. PROFMIN)THEN
            UBAR  = MIN(0.64D0*SED2D_GRAVITE*H,U*U+V*V) !MAXIMUM FROUDE NUMBER = 0.8 (FR^2 = 0.64)
            UBAR  = SQRT(UBAR)
         ELSE
            UBAR = ZERO
         ENDIF

C---        Mean flow direction
         THETA = ATAN2(V,U)
         UNRM  = COS(THETA)
         VNRM  = SIN(THETA)

C---        Bed slope
         DZDX  = VPRNO(IZX,IN) / VPRNO(IML,IN)
         DZDY  = VPRNO(IZY,IN) / VPRNO(IML,IN)
         TANLN =   -( DZDX*UNRM + DZDY*VNRM) ! Long. (+=down)
         TANTR = ABS(-DZDX*VNRM + DZDY*UNRM) ! Trans.

C---        D50 for active layer size fractions
         DO ID = 1, SED2D_NCLASS
            FLI1(ID)  = VPRNO(IDFLI1+ID,IN)
         ENDDO
         D50 = SED2D_SIZEPARM(SED2D_NCLASS,
     &                        0.5D0,
     &                        FLI1)

C---        Total bed shear velocity using Manning law
         USTR = ZERO
         IF (SED2D_FRCT_TOTAL .EQ. SED2D_FRCT_TOTAL_MAN) THEN
            USTR = SQRT(SED2D_GRAVITE)*UBAR*N /(H**UN_6)

C---        Total bed shear velocity using modified Manning law
         ELSEIF (SED2D_FRCT_TOTAL .EQ. SED2D_FRCT_TOTAL_MODIF_MAN) THEN ! Modified to produce smoother stress
            USTR = SQRT(SED2D_GRAVITE)*UBAR*N * (H**UN_6)               ! distribution in complex bed setup
D        ELSE
D           CALL ERR_ASR(SED2D_FRCT_TOTAL .NE.
D    &                   SED2D_FRCT_TOTAL_UNDEFINED)
         ENDIF

C---        Effective bed shear velocity
         USTREFF = ZERO
         IF (SED2D_FRCT_EFF .EQ. SED2D_FRCT_EFF_TOT) THEN               ! No shear stress partitioning (effective stress = total stress)
            USTREFF  = USTR
         ELSEIF (SED2D_FRCT_EFF .EQ. SED2D_FRCT_EFF_ENGL) THEN          ! Effective stress using Engelund and Hansen, 1967
            DSTR     = SED2D_GRAVITE*RD_UN*D50
            USTR2    = USTR*USTR
            USTR2MIN = MIN(USTR2,6.0D-02*DSTR+4.0D-01*USTR2*USTR2/DSTR)
            USTREFF  = SQRT(USTR2MIN)
         ELSEIF (SED2D_FRCT_EFF .EQ. SED2D_FRCT_EFF_WU) THEN            ! Effective stress using Wu and Wang, 1999
            NEFF     = D50**UN_6/20.0D0
!!            USTREFF  = USTR/N*NEFF                                      ! Power 2/3?? Wu p.430 formula (17)
            USTREFF  = USTR*(NEFF/N)**0.75D0
D        ELSE
D           CALL ERR_ASR(SED2D_FRCT_EFF .NE. SED2D_FRCT_EFF_UNDEFINED)
         ENDIF

C---        Correct for bed slope
         TANLN = MIN(TANLN, SED2D_VISC_Z_DZC*0.99D0)
         TANTR = MIN(TANTR, SED2D_VISC_Z_DZC*0.99D0)
         R_DEY = (UN - TANLN/SED2D_VISC_Z_DZC)**0.75D0
     &         * (UN - TANTR/SED2D_VISC_Z_DZC)**0.37D0
         USTREFF = USTREFF/SQRT(R_DEY)

C---        Bedload transport rate
         QB    = ZERO
         IF (SED2D_BLEQ .EQ. SED2D_BLEQT_AW) THEN
            DO ID = 1, SED2D_NCLASS
               QBI(ID) = SED2D_BLEQ_AW(H, UBAR, USTREFF, N, D50, ID)
            ENDDO
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_WU)THEN
            DO ID = 1, SED2D_NCLASS
               QBI(ID) = SED2D_BLEQ_WU(H, UBAR, USTREFF, N, D50, ID)
            ENDDO
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_TEST)THEN
            DO ID = 1, SED2D_NCLASS
               QBI(ID) = SED2D_BLEQ_TEST(H, UBAR, USTREFF, N, D50,ID)
            ENDDO
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_TEST2)THEN
            DO ID = 1, SED2D_NCLASS
               QBI(ID) = SED2D_BLEQ_TEST2(H, UBAR, USTREFF, N, D50, ID)
            ENDDO
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_TEST3)THEN
            DO ID = 1, SED2D_NCLASS
               QBI(ID) = SED2D_BLEQ_TEST3(H, UBAR, USTREFF, IN, D50, ID)
            ENDDO
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_TEST4)THEN
            DO ID = 1, SED2D_NCLASS
               QBI(ID) = SED2D_BLEQ_TEST4(H, UBAR, USTREFF, IN, D50, ID)
            ENDDO
D        ELSE
D           CALL ERR_ASR(SED2D_BLEQ .NE. SED2D_BLEQ_UNDEFINED)
         ENDIF

C---        Actual and total transport rate
         DO ID = 1, SED2D_NCLASS
            QBAI(ID) = QBI(ID)*VPRNO(IDFLI1+ID,IN)
            QB       = QB + QBAI(ID)
         ENDDO

C---        Transport rate fractions
         VPRNO(IDQB,IN) = QB
         DO ID = 1, SED2D_NCLASS
            IF (QB .GT. ZERO) THEN
               VPRNO(IDFQBI+ID,IN) = QBAI(ID)/QB
            ELSE
               VPRNO(IDFQBI+ID,IN) = VPRNO(IDFLI2+ID,IN)
            ENDIF
         ENDDO
         VPRNO(IDTHETA,IN) = THETA

      ENDDO

      IERR = ERR_TYP()
      RETURN
      END
