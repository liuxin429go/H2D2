C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines: CNF2RES,
C               CNF2FC,
C               CNF2KU,
C               CNF2MU,
C               CNF2FV,
C               CNF2KT,
C               CNF2KTE,
C               CNF2MKT,
C               CNF2MKTE,
C               CNF2ML,
C               CNF2LT,
C               CNF2LTE,
C               CNF2DIM,
C               CNF2IND,
C               CNF2DIME,
C               CNF2INDE,
C               CNF2KDLE
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  CNF2KU
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : VDLG=C
C
C************************************************************************
      SUBROUTINE SED2D_FRC_ASMKU(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_FRC_ASMKU
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NEQL)

      INCLUDE 'sed2d.fi'

      INTEGER SED2D_NNELT3
      INTEGER SED2D_NDLEMAX
      PARAMETER (SED2D_NNELT3  =  3)
      PARAMETER (SED2D_NDLEMAX = SED2D_NNELT3*SED2D_NDLNMAX)

      INTEGER   KLOCE(SED2D_NDLEMAX)
      REAL*8    VFE  (SED2D_NDLEMAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. SED2D_NDLNMAX)
C-----------------------------------------------------------------

      CALL SED2D_FRC_ASMKU_V(KLOCE,
     &                      VFE,
     &                      VCORG,
     &                      KLOCN,
     &                      KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VPRES,
     &                      VSOLC,
     &                      VSOLR,
     &                      KDIMP,
     &                      VDIMP,
     &                      KEIMP,
     &                      VDLG,
     &                      VFG)

C      CALL SED2D_FRC_ASMKU_S(KLOCE,
C     &                      VFE,
C     &                      VCORG,
C     &                      KLOCN,
C     &                      KNGV,
C     &                      KNGS,
C     &                      VDJV,
C     &                      VDJS,
C     &                      VPRGL,
C     &                      VPRNO,
C     &                      VPREV,
C     &                      VPRES,
C     &                      VSOLC,
C     &                      VSOLR,
C     &                      KDIMP,
C     &                      VDIMP,
C     &                      KEIMP,
C     &                      VDLG,
C     &                      VFG)

      RETURN
      END

C************************************************************************
C Sommaire:  CNF2KU
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : VDLG=C
C
C************************************************************************
      SUBROUTINE SED2D_FRC_ASMKU_V(KLOCE,
     &                            VFE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sed2d.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d.fc'

      INTEGER IERR
      INTEGER IC, IE, ID, ID1, ID2, ID3
      INTEGER NO1, NO2, NO3
      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DETJ
      REAL*8  VKXY, VEXY, VSXY
      REAL*8  DISPZ,DISPFAI
      REAL*8  C1, C2, C3, CS
      REAL*8  DCDX, DCDY
      REAL*8  AMASZ, AMASFAI

C----------------------------- Lapidus - Begin
      REAL*8  THETA1, QB1, QBX1, QBY1, GRD1
      REAL*8  THETA2, QB2, QBX2, QBY2, GRD2
      REAL*8  THETA3, QB3, QBX3, QBY3, GRD3
      REAL*8  Z1, Z2, Z3
      REAL*8  DZDX, DZDY
      REAL*8  DELTA, DELZ, GRAD
      REAL*8  VLAPZ, VLAPDZ
      INTEGER IDZ, IDQB, IDTH
C----------------------------- Lapidus - End
C-----------------------------------------------------------------------

C---     INDICES
      IDZ  = LM_CMMN_NDLN
      IDQB = 7 + SED2D_NCLASS * SED2D_NLAYER
      IDTH = 8 + SED2D_NCLASS * (SED2D_NLAYER + 1)

C---     INITIALISE
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VFE,1)
      CALL IINIT(LM_CMMN_NDLEV,0,KLOCE,1)

C-------  LOOP OVER THE ELEMENTS
C         =======================
      DO 10 IC = 1,EG_CMMN_NELCOL
      DO 20 IE = EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        ELEMENT CONNECTIVITY FOR T3
         NO1 = KNGV(1,IE)
         NO2 = KNGV(3,IE)
         NO3 = KNGV(5,IE)

C---        TABLE FOR INVERSE OF JACOBIAN MATRIX FOR T3
         VKX  = VDJV(1,IE)
         VEX  = VDJV(2,IE)
         VKY  = VDJV(3,IE)
         VEY  = VDJV(4,IE)
         DETJ = VDJV(5,IE)
         VSX  = -(VKX+VEX)
         VSY  = -(VKY+VEY)

C----------------------------- Lapidus Z - Begin
C---        Nodal values
         Z1 = VDLG(IDZ, NO1)
         Z2 = VDLG(IDZ, NO2)
         Z3 = VDLG(IDZ, NO3)

C---        Slope
         DZDX = (VSX*Z1 + VKX*Z2 + VEX*Z3)
         DZDY = (VSY*Z1 + VKY*Z2 + VEY*Z3)
         GRAD = SQRT(DZDX*DZDX + DZDY*DZDY) / DETJ
         IF (GRAD .GE. SED2D_VISC_Z_DZC) THEN
            VLAPZ = GRAD * DETJ
         ELSE
            VLAPZ = 0.0D0
         ENDIF
C----------------------------- Lapidus Z - End

C----------------------------- Lapidus dZ - Begin
C---        Nodal values
         QB1   = VPRNO(IDQB,NO1)    ! Total transport rate
         THETA1= VPRNO(IDTH,NO1)    ! Shear stress direction
         QB2   = VPRNO(IDQB,NO2)
         THETA2= VPRNO(IDTH,NO2)
         QB3   = VPRNO(IDQB,NO3)
         THETA3= VPRNO(IDTH,NO3)

C---        Correct shear direction for helical flow
         DELTA = VPREV(1,IE)
         THETA1= THETA1 - DELTA
         THETA2= THETA2 - DELTA
         THETA3= THETA3 - DELTA

C---        Transport rate
         QBX1  = QB1 * COS(THETA1)
         QBX2  = QB2 * COS(THETA2)
         QBX3  = QB3 * COS(THETA3)
         QBY1  = QB1 * SIN(THETA1)
         QBY2  = QB2 * SIN(THETA2)
         QBY3  = QB3 * SIN(THETA3)

C---        grad(div(Qb)) = {N,x}(<N,x>{Qx}+<N,y>{Qy})
C                         = 1/2D ({sx}; {sy}) (qx+qy)
C                         = 1/2D {sqrt(sx*sx+sy*sy)}.abs(qx+qy)
C           puis, divise par Ml
C                         = 3/D^2 {sqrt(sx*sx+sy*sy)}.abs(qx+qy)
C           moyenne
C                         = 1/D^2 {sqrt(sx*sx+sy*sy)+...}.abs(qx+qy)
C           Lapidus multiplie par h^2 = D
C                         = 1/D {sqrt(sx*sx+sy*sy)+...}.abs(qx+qy)
         DELZ = (VSX*QBX1 + VKX*QBX2 + VEX*QBX3    ! delZ = div(QB)
     &        +  VSY*QBY1 + VKY*QBY2 + VEY*QBY3)
         DELZ = ABS(DELZ)
         GRD1 = SQRT(VSX*VSX + VSY*VSY)
         GRD2 = SQRT(VKX*VKX + VKY*VKY)
         GRD3 = SQRT(VEX*VEX + VEY*VEY)
         VLAPDZ = (GRD1 + GRD2 + GRD3) * DELZ / DETJ
C----------------------------- Lapidus dZ - End

C---        MASS MATRIX FOR DAMPING
         AMASZ   = SED2D_DMPG_Z*UN_24*DETJ
         AMASFAI = SED2D_DMPG_FAI*UN_24*DETJ

C---        ELEMENT CONSTANTES
         DISPZ   = (SED2D_VISC_Z * VLAPZ +
     &              SED2D_VISC_DZ* VLAPDZ) * UN_2/DETJ  ! DISPERSION Z
         DISPFAI = (SED2D_VISC_FAI +
     &              SED2D_VISC_DFAI) * UN_2/DETJ        ! DISPERSION FAI

C---        LOOP OVER DEGREES OF FREEDOM
C---        BED ELEVATION
         ID = LM_CMMN_NDLN

C---        DEGREES OF FREEDOM
         C1 = VDLG(ID,NO1)
         C2 = VDLG(ID,NO2)
         C3 = VDLG(ID,NO3)
         CS = C1 + C2 + C3

C---        Derivatives
         DCDX = DISPZ*(VSX*C1 + VKX*C2 + VEX*C3)
         DCDY = DISPZ*(VSY*C1 + VKY*C2 + VEY*C3)

C---        ASSEMBLE ELEMENT VECTOR
         VFE(ID,1) = (CS+C1)*AMASZ + VSX*DCDX + VSY*DCDY
         VFE(ID,2) = (CS+C2)*AMASZ + VKX*DCDX + VKY*DCDY
         VFE(ID,3) = (CS+C3)*AMASZ + VEX*DCDX + VEY*DCDY

C---        GRAIN SIZE FRACTIONS
         DO ID=1,SED2D_NCLASS

C---           DEGREES OF FREEDOM
            C1 = VDLG(ID,NO1)
            C2 = VDLG(ID,NO2)
            C3 = VDLG(ID,NO3)
            CS = C1 + C2 + C3

C---           Derivatives
            DCDX = DISPFAI*(VSX*C1 + VKX*C2 + VEX*C3)
            DCDY = DISPFAI*(VSY*C1 + VKY*C2 + VEY*C3)

C---           ASSEMBLE ELEMENT VECTOR
            VFE(ID,1) = (CS+C1)*AMASFAI + VSX*DCDX + VSY*DCDY
            VFE(ID,2) = (CS+C2)*AMASFAI + VKX*DCDX + VKY*DCDY
            VFE(ID,3) = (CS+C3)*AMASFAI + VEX*DCDX + VEY*DCDY
         ENDDO

C---        ELEMENT LOCALIZATION TABLE
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NO1)
            KLOCE(ID, 2)= KLOCN(ID, NO2)
            KLOCE(ID, 3)= KLOCN(ID, NO3)
         ENDDO

C---        ASSEMBLE GLOBAL VECTOR
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire:  CNF2KU
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : VDLG=C
C
C************************************************************************
C      SUBROUTINE SED2D_FRC_ASMKU_S(KLOCE,
C     &                            VFE,
C     &                            VCORG,
C     &                            KLOCN,
C     &                            KNGV,
C     &                            KNGS,
C     &                            VDJV,
C     &                            VDJS,
C     &                            VPRGL,
C     &                            VPRNO,
C     &                            VPREV,
C     &                            VPRES,
C     &                            VSOLC,
C     &                            VSOLR,
C     &                            KDIMP,
C     &                            VDIMP,
C     &                            KEIMP,
C     &                            VDLG,
C     &                            VFG)
C
C      IMPLICIT REAL*8 (A-H, O-Z)
C
C      INCLUDE 'eacdcl.fi'
C      INCLUDE 'eacnst.fi'
C      INCLUDE 'spelem.fi'
C      INCLUDE 'eacmmn.fc'
C      INCLUDE 'egcmmn.fc'
C
C      INTEGER KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
C      REAL*8  VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
C      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
C      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
C      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
C      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
C      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
C      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
C      REAL*8  VPRGL (LM_CMMN_NPRGL)
C      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
C      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
C      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
C      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
C      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
C      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
C      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
C      INTEGER KEIMP (EG_CMMN_NELS)
C      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
C      REAL*8  VFG   (LM_CMMN_NEQ)
C
C      INCLUDE 'err.fi'
C      INCLUDE 'log.fi'
C      INCLUDE 'sed2d_cnst.fi'
C
C      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DETJT3
C      REAL*8  VNX, VNY, DETJL2
C      REAL*8  DISC
C      REAL*8   C1, C2, C3, CX, CY
C      REAL*8  DIF1,DIF2
C      REAL*8  AKXX1,AKXX2,AKXY1,AKXY2,AKYY1,AKYY2
C      INTEGER NDLES
C      INTEGER IERR
C      INTEGER IEC,ID,IE,ID1,ID2,ID3
C      INTEGER NP1,NP2,NP3,NO1,NO2,NO3

C-----------------------------------------------------------------------
C      LOGICAL ELE_ESTTYPE1
C      LOGICAL ELE_ESTTYPE2
C      ELE_ESTTYPE1(IE) = BTEST(KEIMP(IE), EA_TPCL_ENTRANT) .AND.
C     &                   (BTEST(KEIMP(IE), EA_TPCL_CAUCHY) .OR.
C     &                    BTEST(KEIMP(IE), EA_TPCL_OUVERT))
C      ELE_ESTTYPE2(IE) = BTEST(KEIMP(IE), EA_TPCL_SORTANT) .AND.
C     &                   BTEST(KEIMP(IE), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------

C---     INDICES
C      NDLES = LM_CMMN_NDLN*2
C
C---     PARAMETERS
C         AKXX1 = SED2D_VISC_ART_XX   !for boundary conditions???
C         AKXX2 = SED2D_VISC_ART_XX
C         AKXY1 = SED2D_VISC_ART_XY
C         AKXY2 = SED2D_VISC_ART_XY
C         AKYY1 = SED2D_VISC_ART_YY
C         AKYY2 = SED2D_VISC_ART_YY

C---     INITIALISE
C      CALL DINIT(LM_CMMN_NDLEV,ZERO,VFE,1)
C      CALL IINIT(LM_CMMN_NDLEV,0,KLOCE,1)
C
C---- BOUCLE POUR ASSEMBLER LES TERMES DE CONTOUR OUVERTS "SORTANTS"
C     ==============================================================
C      DO IEC=1,EG_CMMN_NELS
C         IF (.NOT. ELE_ESTTYPE2(IEC)) GOTO 299
C
C         NP1     = KNGS(1,IEC)
C         NP2     = KNGS(2,IEC)
C         IE      = KNGS(3,IEC)
C         NO1     = KNGV(1,IE)
C         NO2     = KNGV(2,IE)
C         NO3     = KNGV(3,IE)
C
C---        MÉTRIQUES DE L'ELEMENT
C         VNY    = -VDJS(1,IEC)
C         VNX    =  VDJS(2,IEC)
C         DETJL2 =  VDJS(3,IEC)
C
C---        MÉTRIQUES DU T3 PARENT (recalculées car permutés)
C         VKX    = VDJV(1,IE)
C         VEX    = VDJV(2,IE)
C         VKY    = VDJV(3,IE)
C         VEY    = VDJV(4,IE)
C         DETJT3 = VDJV(5,IE )

C---       CONSTANTE DÉPENDANT DU CONTOUR
C         DISC = -UN_2*DETJL2/DETJT3

C---       BOUCLE D'ASSEMBLAGE SUR LES DDL
C         DO ID=1,LM_CMMN_NDLN
C            C1 = VDLG(ID,NO1)
C            C2 = VDLG(ID,NO2)
C            C3 = VDLG(ID,NO3)
C
C---          DÉRIVÉES DU CONTOUR
C            CX = VKX*(C2-C1) + VEX*(C3-C1)
C            CY = VKY*(C2-C1) + VEY*(C3-C1)
C            DIF1 = DISC * ( (AKXX1*CX + AKXY1*CY)*VNX
C     &                  +   (AKXY1*CX + AKYY1*CY)*VNY )
C            DIF2 = DISC * ( (AKXX2*CX + AKXY2*CY)*VNX
C     &                  +   (AKXY2*CX + AKYY2*CY)*VNY )

C---          ASSEMBLAGE
C            VFE(ID,1) = DIF1
C            VFE(ID,2) = DIF2
C         ENDDO

C---       TABLE KLOCE DE LOCALISATION DES DDLS
C         DO ID=1,LM_CMMN_NDLN
C            KLOCE(ID, 1)= KLOCN(ID, NP1)
C            KLOCE(ID, 2)= KLOCN(ID, NP2)
C         ENDDO

C---       ASSEMBLAGE DU VECTEUR GLOBAL
C         IERR = SP_ELEM_ASMFE(NDLES, KLOCE, VFE, VFG)
C
C299      CONTINUE
C      ENDDO
C
C      RETURN
C      END
