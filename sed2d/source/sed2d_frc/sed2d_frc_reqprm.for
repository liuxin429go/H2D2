C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de sediment continuity 2-D
C     Élément T3 - linéaire
C
C Notes:
C     Subroutines de base.
C************************************************************************

C************************************************************************
C Sommaire: Retourne les paramètres de l'élément.
C
C Description:
C     La fonction <code>SED2D_FRC_REQPRM</code> retourne tous les
C     paramètres caractéristiques de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SED2D_FRC_REQPRM(TGELV,
     &                            NPRGL,
     &                            NPRGLL,
     &                            NPRNO,
     &                            NPRNOL,
     &                            NPREV,
     &                            NPREVL,
     &                            NPRES,
     &                            NSOLC,
     &                            NSOLCL,
     &                            NSOLR,
     &                            NSOLRL,
     &                            NDLN,
     &                            NDLEV,
     &                            NDLES,
     &                            ASURFACE,
     &                            ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_FRC_REQPRM
CDEC$ ENDIF

      INTEGER TGELV
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES

      LOGICAL ASURFACE
      LOGICAL ESTLIN

      INCLUDE 'eacnst.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'eacmmn.fc'
C-----------------------------------------------------------------------

C---     INITIALISE LES PARAMETRES DE L'ÉLÉMENT PARENT
      CALL SED2D_BSE_REQPRM(TGELV,
     &                      NPRGL,
     &                      NPRGLL,
     &                      NPRNO,
     &                      NPRNOL,
     &                      NPREV,
     &                      NPREVL,
     &                      NPRES,
     &                      NSOLC,
     &                      NSOLCL,
     &                      NSOLR,
     &                      NSOLRL,
     &                      NDLN,
     &                      NDLEV,
     &                      NDLES,
     &                      ASURFACE,
     &                      ESTLIN)

C---     INITIALISATION DES PARAMETRES INVARIANT DE L'ÉLÉMENT PARENT VIRTUEL
!      TGELV  =  EG_TPGEO_T6L          ! TYPE DE GEOMETRIE DE L'ELEMENT DE VOLUME
!      TGELS  =  EG_TPGEO_L3L          ! TYPE DE GEOMETRIE DE L'ELEMENT DE SURFACE
!      NNELV  =  3                     ! NB DE NOEUDS PAR ÉLÉMENT DE VOLUME
!      NNELS  =  2                     ! NB DE NOEUDS PAR ÉLÉMENT DE SURFACE

C---     INITIALISATION DES PARAMETRES VARIABLES SUJETS À CHANGEMENT CHEZ L'ÉLÉMENT HÉRITIER
!      NPRGLL =  20                    ! NB DE PROPRIÉTÉS GLOBALES LUES
!      NPRGL  =  NPRGLL                ! NB DE PROPRIÉTÉS GLOBALES
      NPRNOL =  6                     ! NB DE PROPRIÉTÉS NODALES LUES
      NPRNO  =  NPRNOL + 2            ! NB DE PROPRIÉTÉS NODALES + QB + THETA
!      NPREVL =  0                     ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME LUES
!      NPREV  =  NPREVL + 1            ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME
                                      !+ DELTA (CORRECTION FOR HELICAL FLOW AND BED SLOPE EFFECTS)
!      NPRES  =  1                     ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE SURFACE
!      NSOLCL =  0                     ! NB DE SOLLICITATIONS CONCENTRÉES LUES
!      NSOLC  =  NSOLCL+1              ! NB DE SOLLICITATIONS CONCENTRÉES
!      NSOLRL =  0                     ! NB DE SOLLICITATIONS RÉPARTIES LUES
!      NSOLR  =  NSOLRL+1              ! NB DE SOLLICITATIONS RÉPARTIES
      NDLN   =  1                     ! NB DE DDL PAR NOEUD
!      NDLEV  =  0                     ! NB DE DDL PAR ELEMENT DE VOLUME
!      NDLES  =  0                     ! NB DE DDL PAR ELEMENT DE SURFACE

      ASURFACE = .TRUE.
      ESTLIN   = .TRUE.

      RETURN
      END

