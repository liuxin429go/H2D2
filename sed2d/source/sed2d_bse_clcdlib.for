C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: SED2D_BSE_CLCDLIB
C
C Description:
C     La fonction SED2D_BSE_CLCDLIB
C
C Entrée:
C
C Sortie:
C
C Notes:
C  Comme l'élement est linéaire (pas de perturbation pour le calcul de Kt),
C  le code pourrait être déplacé dans PRCDLIB.
C************************************************************************
      SUBROUTINE SED2D_BSE_CLCDLIB(KNGV,
     &                             KLOCN,
     &                             KDIMP,
     &                             VDIMP,
     &                             VDLG,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BSE_CLCDLIB
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KLOCN(LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'

      INTEGER IC, IE, ID
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6

      REAL*8 UN_2
      PARAMETER (UN_2 = 0.5D0)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IE, IC, ID)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)

C---     Impose les noeuds milieux linéaire
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1 = KNGV(1,IE)
         NO2 = KNGV(2,IE)
         NO3 = KNGV(3,IE)
         NO4 = KNGV(4,IE)
         NO5 = KNGV(5,IE)
         NO6 = KNGV(6,IE)

         DO ID=1,LM_CMMN_NDLN
            VDLG(ID,NO2) = UN_2*(VDLG(ID,NO1) + VDLG(ID,NO3))
            VDLG(ID,NO4) = UN_2*(VDLG(ID,NO3) + VDLG(ID,NO5))
            VDLG(ID,NO6) = UN_2*(VDLG(ID,NO5) + VDLG(ID,NO1))
         ENDDO
      ENDDO
!$omp end do
      ENDDO

!$omp end parallel

      IERR = ERR_TYP()
      RETURN
      END
