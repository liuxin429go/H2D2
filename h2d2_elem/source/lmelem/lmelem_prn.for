C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_PRN
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_PRN_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Print.
C
C Description:
C     La fonction HS_SIMD_PRNPROP imprime (print) les informations sur
C     toutes les données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_PRN(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_PRN
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      REAL*8 TEMPS
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Charge les données
      TEMPS = 0.0D0
      IF (ERR_GOOD()) IERR = SELF%OPRGL%PASDEB(TEMPS)

C---     Impression des données
      IF (ERR_GOOD()) IERR = SELF%PRNPRGL()
      IF (ERR_GOOD()) IERR = SELF%PRNPRNO()
      IF (ERR_GOOD()) IERR = SELF%PRNCLIM()

      LM_ELEM_PRN = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_PRN

      END SUBMODULE LM_ELEM_PRN_M
      