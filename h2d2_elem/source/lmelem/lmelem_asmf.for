C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_ELEM
C         FTN (Sub)Module: LM_ELEM_ASMF_M
C            Public:
C               MODULE INTEGER LM_ELEM_ASMF
C            Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_ASMF_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: Assemble {F}
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C     VRES     F
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_ASMF(SELF, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ASMF
CDEC$ ENDIF

      CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VRES(*)

      INCLUDE 'err.fi'
      INCLUDE 'eacnst.fi'

      INTEGER IERR
      INTEGER NEQL
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.residu')

C---     Assemble
      NEQL = SELF%EDTA%NEQL
      VRES(1:NEQL) = ZERO
      IERR = SELF%CLCF(VRES(1:NEQL))

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.reso.residu')

      LM_ELEM_ASMF = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASMF

      END SUBMODULE LM_ELEM_ASMF_M
