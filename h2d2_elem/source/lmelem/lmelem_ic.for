C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_LM_ELEM_XEQCTR
C     INTEGER IC_LM_ELEM_XEQMTH
C     CHARACTER*(32) IC_LM_ELEM_REQCLS
C     INTEGER IC_LM_ELEM_REQHDL
C   Private:
C     INTEGER IC_LM_ELEM_XEQMTH_INITF
C     INTEGER IC_LM_ELEM_XEQMTH_INITV
C     INTEGER IC_LM_ELEM_XEQMTH_SAVE
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_LM_ELEM_XEQCTR(IPRM) est une fonction vide car il
C     n'est pas prévu de construire un LM_ELEM via une commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_LM_ELEM_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LM_ELEM_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'lmelem_ic.fi'
      INCLUDE 'err.fi'

C------------------------------------------------------------------------
      CALL ERR_PRE(.FALSE.)
C------------------------------------------------------------------------

      IPRM = ' '

      IC_LM_ELEM_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LM_ELEM_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LM_ELEM_XEQMTH
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'lmelem_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmelem_ic.fc'

      INTEGER      IERR
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
      CLASS(LM_ELEM_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => LM_HELE_REQOMNG(HOBJ)

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the mesh</comment>
         IF (PROP .EQ. 'hgrid') THEN
            IVAL = SELF%OGEO%OGRID%REQHGRID()
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the degrees of freedom (unknowns)</comment>
         ELSEIF (PROP .EQ. 'hdlib') THEN
            IVAL = SELF%ODLIB%REQHVNOD()
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the boundary conditions</comment>
         ELSEIF (PROP .EQ. 'hclim') THEN
            IVAL = SELF%OCLIM%REQHCLIM()
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the concentrated solicitations</comment>
         ELSEIF (PROP .EQ. 'hsolc') THEN
            IVAL = SELF%OSOLC%REQHSOLC()
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the distributed solicitations</comment>
         ELSEIF (PROP .EQ. 'hsolr') THEN
            IVAL = SELF%OSOLR%REQHSOLR()
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the global properties</comment>
         ELSEIF (PROP .EQ. 'hprgl') THEN
            IVAL = SELF%OPRGL%REQHPRGL()
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the nodal properties</comment>
         ELSEIF (PROP .EQ. 'hprno') THEN
            IVAL = SELF%OPRNO%REQHPRNO()
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the elemental properties</comment>
         ELSEIF (PROP .EQ. 'hprel') THEN
            IVAL = SELF%OPREV%REQHPREV()
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>
C     The method <b>init_f</b> initializes the solution from a file.
C     Values are read for each node, at time <b>temps</b>.
C     </comment>
      ELSEIF (IMTH .EQ. 'init_f') THEN
C        <include>IC_LM_ELEM_XEQMTH_INITF@lmelem_ic.for</include>
         IERR = IC_LM_ELEM_XEQMTH_INITF(HOBJ, IPRM)

C     <comment>
C     The method <b>init_v</b> initializes the solution with the
C     values provided.
C     </comment>
      ELSEIF (IMTH .EQ. 'init_v') THEN
C        <include>IC_LM_ELEM_XEQMTH_INITV@lmelem_ic.for</include>
         IERR = IC_LM_ELEM_XEQMTH_INITV(HOBJ, IPRM)

C     <comment>The method <b>save</b> saves the current solution to the specified file.</comment>
      ELSEIF (IMTH .EQ. 'save') THEN
C        <include>IC_LM_ELEM_XEQMTH_SAVE@lmelem_ic.for</include>
         IERR = IC_LM_ELEM_XEQMTH_SAVE(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SELF%PRN()

C     <comment>The method <b>print_prgl</b> prints the global properties of the object.</comment>
      ELSEIF (IMTH .EQ. 'print_prgl') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SELF%PRNPRGL()

C     <comment>The method <b>print_prno</b> prints the nodal properties of the object.</comment>
      ELSEIF (IMTH .EQ. 'print_prno') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SELF%PRNPRNO()

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_HLP(HOBJ)

C     <comment>The method <b>help_prgl</b> displays the help for the global properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prgl') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_HLPPRGL(HOBJ)

C     <comment>The method <b>help_prno</b> displays the help for the nodal properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prno') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_HLPPRNO(HOBJ)

C     <comment>The method <b>help_bc</b> displays the help for the boundary conditions of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_bc') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_HLPCLIM(HOBJ)

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_LM_ELEM_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ n'est pas utilisé pour ces méthodes statiques
C************************************************************************
      FUNCTION IC_LM_ELEM_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LM_ELEM_OPBDOT
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'lmelem_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C        <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_HLP(HOBJ)

C     <comment>The method <b>print_prgl</b> prints the global properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prgl') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_HLPPRGL(HOBJ)

C     <comment>The method <b>print_prno</b> prints the nodal properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prno') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_HLPPRNO(HOBJ)

C     <comment>The method <b>print_bc</b> prints the boundary conditions of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prno') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_HLPCLIM(HOBJ)

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_LM_ELEM_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LM_ELEM_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LM_ELEM_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'lmelem_ic.fi'
C-------------------------------------------------------------------------

      IC_LM_ELEM_REQCLS = '#__dummy_placeholder__#__LM_HELE__'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LM_ELEM_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LM_ELEM_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'lmelem_ic.fi'
      INCLUDE 'lmhele.fi'
C-------------------------------------------------------------------------

      IC_LM_ELEM_REQHDL = LM_HELE_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LM_ELEM_XEQMTH_INITF(HOBJ, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IPRM

      INCLUDE 'lmhele.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmelem_ic.fc'

      REAL*8          TEMPS
      INTEGER         IERR
      INTEGER         ISTAT
      INTEGER         LNOM
      CHARACTER*(256) NOMFIC
      CHARACTER*(256) NOMTMP
      CHARACTER*(  8) MODE
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_INITIALISE'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Initial time</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, TEMPS)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Name of the initialisation file</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 2, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Opening mode ['r', 'rb'] (default 'r')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 3, MODE)
      IF (IERR .NE. 0) MODE = 'r'
      CALL SP_STRN_TRM(NOMFIC)

C---     VALIDE
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9903

C---     IMPRESSION DES PARAMÈTRES
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOMTMP, HOBJ)
         LNOM = SP_STRN_LEN(NOMTMP)
         WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', NOMTMP(1:LNOM)
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TEMPS#<35>#= ', TEMPS
         CALL LOG_ECRIS(LOG_BUF)

         NOMTMP = NOMFIC
         CALL SP_STRN_CLP(NOMTMP, 40)
         LNOM = SP_STRN_LEN(NOMTMP)
         WRITE (LOG_BUF,'(3A)') 'MSG_FICHIER_DONNEES#<35>#', '= ',
     &                           NOMTMP(1:LNOM)
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Initialise
      IF (ERR_GOOD()) THEN
         IERR = LM_HELE_INIFIC(HOBJ, TEMPS, NOMFIC, ISTAT)
      ENDIF

C---     IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_DDL#<35>#= ',
C     &                                 GR_GRID_REQNDLT(HGRD)
C         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     RETOURNE RIEN
      IPRM = ' '

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ',
     &                       MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL LOG_DECIND()
      IC_LM_ELEM_XEQMTH_INITF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LM_ELEM_XEQMTH_INITV(HOBJ, IPRM)

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IPRM

      INCLUDE 'lmhele.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmelem_ic.fc'

      INTEGER NVALMAX
      PARAMETER (NVALMAX = 20)

      INTEGER I
      INTEGER IERR
      INTEGER LNOM
      INTEGER NVAL
      REAL*8  VVAL(NVALMAX)
      REAL*8  TEMPS
      CHARACTER*(256) NOMTMP
      CLASS(LM_ELEM_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C-------  EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_INITIALISE'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Initial time</comment>
      IERR = SP_STRN_TKR(IPRM, ',', 1, TEMPS)
      IF (IERR .NE.  0) GOTO 9901
      I = 0
100   CONTINUE
         IF (I .EQ. NVALMAX) GOTO 9902
         I = I + 1
C        <comment>Initial values, one per dof</comment>
         IERR = SP_STRN_TKR(IPRM, ',', I+1, VVAL(I))
         IF (IERR .EQ. -1) GOTO 199
         IF (IERR .NE.  0) GOTO 9901
      GOTO 100
199   CONTINUE
      NVAL = I-1

C---     CONTROLE
      IF (NVAL .LE. 0)  GOTO 9904

C---     IMPRIME LES PARAMETRES
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOMTMP, HOBJ)
         LNOM = SP_STRN_LEN(NOMTMP)
         WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', NOMTMP(1:LNOM)
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TEMPS#<35>#= ', TEMPS
         CALL LOG_ECRIS(LOG_BUF)

         WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_VAL#<35>#= ', NVAL
         CALL LOG_ECRIS(LOG_BUF)
         DO I=1, NVAL
            WRITE (LOG_BUF,'(A,1PE14.6E3)')'MSG_VALEUR#<35>#= ',VVAL(I)
            CALL LOG_ECRIS(LOG_BUF)
         ENDDO
      ENDIF

C---     INITIALISE
      IF (ERR_GOOD()) THEN
         SELF => LM_HELE_REQOMNG(HOBJ)
         IERR = SELF%INIVAL(TEMPS, VVAL(1:NVAL))
      ENDIF

C---     RETOURNE RIEN
      IPRM = ' '

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I3)') 'MSG_NVAL_MAX',': ',NVALMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VALEURS_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL LOG_DECIND()
      IC_LM_ELEM_XEQMTH_INITV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_LM_ELEM_XEQMTH_SAVE exécute la méthode SAVE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LM_ELEM_XEQMTH_SAVE(HOBJ, IPRM)

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IPRM

      INCLUDE 'ioutil.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmelem_ic.fc'

      INTEGER IERR
      INTEGER ISTAT
      CHARACTER*(256) NOMFIC
      CHARACTER*(256) NOMTMP
      CHARACTER*(  8) MODE
      CLASS(LM_ELEM_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_ECRIS_SOL_FIN'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LECTURE DES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the saving file</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Opening mode ['w', 'a'] (default 'a')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 2, MODE)
      IF (IERR .NE. 0) MODE = 'a'
      CALL SP_STRN_TRM(NOMFIC)

C---     VALIDE
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9904

C---     IMPRESSION DU NOM DE FICHIER
      IF (ERR_GOOD()) THEN
         NOMTMP = NOMFIC
         CALL SP_STRN_CLP(NOMTMP, 40)
         WRITE (LOG_BUF,'(3A)') 'MSG_FICHIER_DONNEES#<35>#', '= ',
     &                           NOMTMP(1:SP_STRN_LEN(NOMTMP))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     EXECUTION
      IF (ERR_GOOD()) THEN
         SELF => LM_HELE_REQOMNG(HOBJ)
         IERR  = SELF%SAUVE(NOMFIC, ISTAT)
      ENDIF

C---     RETOURNE RIEN
      IPRM = ' '

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ',
     &                       MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL LOG_DECIND()
      IC_LM_ELEM_XEQMTH_SAVE = ERR_TYP()
      RETURN
      END
