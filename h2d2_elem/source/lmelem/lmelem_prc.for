C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_CLCPRE
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire: Pre-calcul.
C
C Description:
C     La fonction LM_ELEM_CLCPRE fait le pré-traitement avant
C     calcul sur toutes les données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On peut rentrer ici avec n'importe quel pointeur!!
C************************************************************************
      FUNCTION LM_ELEM_CLCPRE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: LM_ELEM_CLCPRE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     LES PROP. GLOBALES
      IF (ERR_GOOD()) IERR = LM_ELEM_PRCPRGL(HOBJ)

C---     LES PROP. NODALES
      IF (ERR_GOOD()) IERR = LM_ELEM_PRCPRNO(HOBJ)

C---     LES PROP. ELEMENTAIRES (VOLUME ET SURFACE)
      IF (ERR_GOOD()) IERR = LM_ELEM_PRCPREV(HOBJ)
      IF (ERR_GOOD()) IERR = LM_ELEM_PRCPRES(HOBJ)

C---     LES SOLLICITATIONS (RÉPARTIES ET CONCENTRÉES)
      IF (ERR_GOOD()) IERR = LM_ELEM_PRCSOLC(HOBJ)
      IF (ERR_GOOD()) IERR = LM_ELEM_PRCSOLR(HOBJ)

C---     LES COND. LIMITES
      IF (ERR_GOOD()) IERR = LM_ELEM_PRCCLIM(HOBJ)

C---     LES D.D.L.
      IF (ERR_GOOD()) IERR = LM_ELEM_PRCDLIB(HOBJ)

C---     LE PARALLELISME
      IF (ERR_GOOD()) IERR = LM_ELEM_PRCPRLL(HOBJ)

!!!C---     MET A JOUR LE NOMBRE D'EQUATIONS
!!!      IOB  = HOBJ - HS_SIMD_HBASE
!!!      HDLIB = HS_SIMD_HDLIB(IOB)
!!!      IF (ERR_GOOD()) IERR = HS_DLIB_MAJNEQ(HDLIB)

C---     STATUS
      !!!IF (ERR_GOOD()) THEN
      !!!   IOB = HOBJ - HS_SIMD_HBASE
      !!!   LM_ELEM_STTUS(IOB) = MAX(LM_ELEM_STTUS(IOB), HS_SIMD_STTUS_PRC)
      !!!ENDIF

      LM_ELEM_CLCPRE = ERR_TYP()
      RETURN
      END
