C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_CFGGPU
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire: Construis le calculateur
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     HCLC        Handle sur le calculateur GPU
C
C Notes:
C************************************************************************
      FUNCTION LM_ELEM_CFGGPU(HOBJ, HCLC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_CFGGPU
CDEC$ ENDIF

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)  :: HOBJ
      INTEGER, INTENT(OUT) :: HCLC

      INCLUDE 'err.fi'
      INCLUDE 'gpclcl.fi'
      INCLUDE 'grgrid.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmelem.fc'     ! LM_ELEM_CFGGPU devrait être dans .fi

      TYPE (LM_ELEM_T), POINTER :: SELF
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER IERR
      INTEGER HCONF, HDIST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les données
      SELF => LM_ELEM_REQSELF(HOBJ)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     La configuration
      HCONF = 0
      HDIST = 0
      IF (ERR_GOOD() .AND. SELF%HCALC .EQ. 0) THEN
         HCONF = LM_GEOM_REQHCONF(HOBJ)
         HDIST = LM_GEOM_REQHDIST(HOBJ)
      ENDIF

C---     Construis le calculateur
      HCLC = 0
      IF (ERR_GOOD() .AND. SELF%HCALC .EQ. 0) THEN
         IERR = GP_CLCL_CTR(HCLC)
         IERR = GP_CLCL_INI(HCLC,
     &                      HCONF,
     &                      HDIST,
     &                      GDTA,
     &                      EDTA)
         IF (ERR_GOOD()) SELF%HCALC = HCLC
      ENDIF
      IF (ERR_GOOD()) HCLC = SELF%HCALC

      LM_ELEM_CFGGPU = ERR_TYP()
      RETURN
      END
