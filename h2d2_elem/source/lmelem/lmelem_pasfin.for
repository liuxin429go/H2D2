C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_PASFIN
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_PASFIN_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Post-calcul.
C
C Description:
C     La sous-routine LM_ELEM_PASFIN fait tout le traitement
C     post-calcul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_PASFIN(SELF, TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_PASFIN
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
      REAL*8, INTENT(IN) :: TSIM

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER, PARAMETER :: NDOSCT_MAX = 32
      LOGICAL MODIF
      LOGICAL DOSCT(NDOSCT_MAX)
C-----------------------------------------------------------------------

C---     Les prop. nodale
      IF (ERR_GOOD()) IERR = SELF%PSCPRNO(MODIF)

!C---     Les prop. élémentaires (volume et surface)
!      IF (ERR_GOOD()) IERR = LM_ELEM_PSCPREV(HOBJ)
!      IF (ERR_GOOD()) IERR = LM_ELEM_PSCPRES(HOBJ)

!C---     Les sollicitations (réparties et concentrées)
!      IF (ERR_GOOD()) IERR = LM_ELEM_PSCSOLC(HOBJ)
!      IF (ERR_GOOD()) IERR = LM_ELEM_PSCSOLR(HOBJ)

!C---     Les conditions limites
!      IF (ERR_GOOD()) IERR = LM_ELEM_PSCCLIM(HOBJ)

!$omp parallel 
!$omp& default(shared)
!$omp& private(IERR, DOSCT)

!$omp sections

!$omp section
C---     Scatter les données
      IF (ERR_GOOD()) IERR = SELF%OCLIM%PASFIN(TSIM)
      IF (ERR_GOOD()) IERR = SELF%OSOLC%PASFIN(TSIM)
      IF (ERR_GOOD()) IERR = SELF%OSOLR%PASFIN(TSIM)
!$omp section
      DOSCT(:) = MODIF
      IF (ERR_GOOD()) IERR = SELF%OPRGL%PASFIN(TSIM)
      IF (ERR_GOOD()) IERR = SELF%OPRNO%PASFIN(TSIM, DOSCT)
      IF (ERR_GOOD()) IERR = SELF%OPREV%PASFIN(TSIM)
      IF (ERR_GOOD()) IERR = SELF%OPRES%PASFIN(TSIM)
!$omp end sections

      IERR = ERR_OMP_RDC()
!$omp end parallel

C---     Mise à jour des DDL
      IF (ERR_GOOD()) IERR = SELF%ODLIB%PASFIN(TSIM)

C---     Status
      IF (ERR_GOOD()) THEN
         SELF%EDTA%TSIM = TSIM
         SELF%STTUS = MAX(SELF%STTUS, LM_ELEM_STTUS_PSC)
      ENDIF

      LM_ELEM_PASFIN = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_PASFIN

      END SUBMODULE LM_ELEM_PASFIN_M
