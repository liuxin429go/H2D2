C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_CLCPST
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_CLCPST_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Calcul.
C
C Description:
C     La fonction LM_ELEM_CLCPST fait le traitement de calcul sur
C     les données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_CLCPST(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_CLCPST
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Calcule les prop. nodale
      IF (ERR_GOOD()) IERR = SELF%CLCDLIB()

!!!C---     STATUS
!!!      IF (ERR_GOOD()) THEN
!!!         IOB = HOBJ - HS_SIMD_HBASE
!!!         HS_SIMD_STTUS(IOB) = MAX(HS_SIMD_STTUS(IOB), HS_SIMD_STTUS_CLC)
!!!      ENDIF

      LM_ELEM_CLCPST = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_CLCPST

      END SUBMODULE LM_ELEM_CLCPST_M
      
