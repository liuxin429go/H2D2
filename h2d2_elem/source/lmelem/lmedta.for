C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: 
C
C Groupe:  eLeMent
C Objet:   Element DaTA
C Type:    Structure de données
C Note: La structure est publique
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE LM_EDTA_M

      PUBLIC

C---     Structure des données d'approximation
      TYPE :: LM_EDTA_T
         INTEGER :: NDLN 
         INTEGER :: NDLEV
         INTEGER :: NDLES

         INTEGER :: NPRGL
         INTEGER :: NPRGLL
         REAL*8, POINTER :: VPRGL(:)

         INTEGER :: NPRGX
         REAL*8, POINTER :: VPRGX(:)

         INTEGER :: NPRNO 
         INTEGER :: NPRNOL
         INTEGER :: NPRNOC
         REAL*8, POINTER :: VPRNO(:,:)

         INTEGER :: NPREV 
         INTEGER :: NPREVL
         INTEGER :: NPREV_D1
         INTEGER :: NPREV_D2
         REAL*8, POINTER :: VPREV(:,:,:)

         INTEGER :: NPRES
         INTEGER :: NPRES_D1
         INTEGER :: NPRES_D2
         REAL*8, POINTER :: VPRES(:,:,:)

         INTEGER :: NSOLC 
         INTEGER :: NSOLCL
         REAL*8, POINTER :: VSOLC(:,:)

         INTEGER :: NSOLR 
         INTEGER :: NSOLRL
         REAL*8, POINTER :: VSOLR(:,:)

         INTEGER :: NCLCND
         INTEGER :: NCLCNV
         INTEGER, POINTER :: KCLCND(:,:)
         REAL*8,  POINTER :: VCLCNV(:)
         INTEGER, POINTER :: KDIMP(:,:)
         REAL*8,  POINTER :: VDIMP(:,:)
         INTEGER, POINTER :: KEIMP(:)

         INTEGER :: NDLP 
         INTEGER :: NDLL 
         INTEGER :: NDLT 
         INTEGER :: NEQP 
         INTEGER :: NEQL 
         INTEGER :: NEQT 
         INTEGER, POINTER :: KLOCN(:,:)
         REAL*8,  POINTER :: VDLG (:,:)

         REAL*8 :: TEMPS
      CONTAINS
         PROCEDURE, PUBLIC :: RST => LM_EDTA_RST
      END TYPE LM_EDTA_T

      CONTAINS
      
C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_EDTA_RST(SELF)
      
      CLASS(LM_EDTA_T), INTENT(INOUT) :: SELF
C------------------------------------------------------------------------
         
      SELF%NDLN   = -1
      SELF%NDLEV  = -1
      SELF%NDLES  = -1

      SELF%NPRGL  = -1
      SELF%NPRGLL = -1
      SELF%VPRGL => NULL()

      SELF%NPRGX = -1
      SELF%VPRGX => NULL()

      SELF%NPRNO  = -1
      SELF%NPRNOL = -1
      SELF%NPRNOC = -1
      SELF%VPRNO => NULL()

      SELF%NPREV  = -1
      SELF%NPREVL = -1
      SELF%NPREV_D1 = -1
      SELF%NPREV_D2 = -1
      SELF%VPREV => NULL()

      SELF%NPRES  = -1
      SELF%NPRES_D1 = -1
      SELF%NPRES_D2 = -1
      SELF%VPRES => NULL()

      SELF%NSOLC  = -1
      SELF%NSOLCL = -1
      SELF%VSOLC => NULL()

      SELF%NSOLR  = -1
      SELF%NSOLRL = -1
      SELF%VSOLR => NULL()

      SELF%NCLCND  = -1
      SELF%NCLCNV  = -1
      SELF%KCLCND => NULL()
      SELF%VCLCNV => NULL()
      SELF%KDIMP  => NULL()
      SELF%VDIMP  => NULL()
      SELF%KEIMP  => NULL()

      SELF%NDLP  = -1
      SELF%NDLL  = -1
      SELF%NDLT  = -1
      SELF%NEQP  = -1
      SELF%NEQL  = -1
      SELF%NEQT  = -1
      SELF%KLOCN => NULL()
      SELF%VDLG  => NULL()

      SELF%TEMPS = -1.0D0
      
      LM_EDTA_RST = 0
      RETURN
      END FUNCTION LM_EDTA_RST
      
      END MODULE LM_EDTA_M
