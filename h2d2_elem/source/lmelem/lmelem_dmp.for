C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_DMP
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_DMP_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Print.
C
C Description:
C     La fonction LM_ELEM_DMP
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_DMP(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_DMP
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

      IF (ERR_GOOD()) IERR = SELF%DMPPROP()

      LM_ELEM_DMP = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_DMP

      END SUBMODULE LM_ELEM_DMP_M
      