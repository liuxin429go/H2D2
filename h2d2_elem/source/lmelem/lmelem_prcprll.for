C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_PRCPRLL
C   Private:
C     INTEGER LM_ELEM_PRCPRLL_TR
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La sous-routine LM_ELEM_PRCPRLL impose les conditions limites
C     liées au calcul distribué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION LM_ELEM_PRCPRLL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_PRCPRLL
CDEC$ ENDIF

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmlcgl.fi'
      INCLUDE 'lmelem.fc'

      INTEGER IERR
      INTEGER HNUMR
      TYPE (LM_EDTA_DATA_T), POINTER :: EDTA
      TYPE (LM_GDTA_DATA_T), POINTER :: GDTA
      INTEGER LM_ELEM_PRCPRLL_TR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      HNUMR = LM_ELEM_REQHNUMC(HOBJ)
      IF (NM_LCGL_HVALIDE(HNUMR)) THEN
         GDTA => LM_ELEM_REQGDTA(HOBJ)
         EDTA => LM_ELEM_REQEDTA(HOBJ)
         IERR = LM_ELEM_PRCPRLL_TR(EDTA%NDLN,
     &                             GDTA%NNL,
     &                             EDTA%KDIMP,
     &                             EDTA%KLOCN,
     &                             HNUMR)
      ENDIF

      LM_ELEM_PRCPRLL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Fonction de travail de LM_ELEM_PRCPRLL.
C
C Description:
C     La fonction privée LM_ELEM_PRCPRLL_TR est la fonction de travail
C     pour la fonction LM_ELEM_PRCPRLL.
C
C Entrée:
C
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION LM_ELEM_PRCPRLL_TR(NDLN,
     &                            NNL,
     &                            KDIMP,
     &                            KLOCN,
     &                            HNUMR)

      IMPLICIT NONE

      INTEGER LM_ELEM_PRCPRLL_TR
      INTEGER NDLN
      INTEGER NNL
      INTEGER KDIMP(NDLN, NNL)
      INTEGER KLOCN(NDLN, NNL)
      INTEGER HNUMR

      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER IN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HNUMR))
C-----------------------------------------------------------------------

C---     Impose les conditions
      DO IN=1,NNL
         IF (.NOT. NM_LCGL_ESTNOPP(HNUMR,IN)) THEN
            KDIMP(:,IN) = IBSET(KDIMP(:,IN), EA_TPCL_PARALLEL)
         ENDIF
      ENDDO

      LM_ELEM_PRCPRLL_TR = ERR_TYP()
      RETURN
      END
