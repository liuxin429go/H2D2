C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_ASMRES
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire: Assemble le résidu complet {F} - [K]{U}
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C     VRES     F
C
C Notes:
C************************************************************************
      FUNCTION LM_ELEM_ASMRES(HOBJ, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ASMRES
CDEC$ ENDIF

      USE LM_ELEM_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VRES(*)

      INCLUDE 'lmelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfcod.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER HMETH
      TYPE (LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.residu')

      SELF => LM_ELEM_REQSELF(HOBJ)

C---     La méthode
      HMETH = 0
      IF (ERR_GOOD()) IERR = OB_VTBL_REQMTH (SELF%HVTBL,
     &                                       LM_VTBL_FNC_ASMRES,
     &                                       HMETH)

C---     Fait l'appel de méthode
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL1 (HMETH,
     &                         SO_ALLC_CST2B(VRES))

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.reso.residu')

      LM_ELEM_ASMRES = ERR_TYP()
      RETURN
      END
