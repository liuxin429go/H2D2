C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LG_LGCY_CLCJELV
C   Private:
C     INTEGER LG_LGCY_CLCJELV_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LG_LGCY_M) LG_LGCY_CLCJELV_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Call-back privé.
C
C Description:
C     La fonction privée LG_LGCY_CLCJELV_CB est le call-back pour
C     la fonction LG_LGCY_CLCJELV.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LG_LGCY_CLCJELV_CB(F, SELF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      INTEGER  F
      EXTERNAL F
      CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

      GDTA => SELF%GDTA

      IERR = F(GDTA%NDIM,
     &         GDTA%NNL,
     &         GDTA%NCELV,
     &         GDTA%NELLV,
     &         GDTA%NDJV,
     &         GDTA%VCORG,
     &         GDTA%KNGV,
     &         GDTA%VDJV)

      LG_LGCY_CLCJELV_CB = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_CLCJELV_CB

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LG_LGCY_CLCJELV(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LG_LGCY_CLCJELV
CDEC$ ENDIF

      USE SO_FUNC_M,     ONLY: SO_FUNC_CBFNC
      USE SO_ALLC_M,     ONLY: SO_ALLC_CST2B
      USE ISO_C_BINDING, ONLY: C_LOC

      CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'egfunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
C-----------------------------------------------------------------------

      CALL LOG_TODO('Qui fait les appels à CP2GDTA??')
      IERR = SELF%ASGCMN()

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EG_FUNC_CLCJELV, HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CBFNC(HFNC,
     &                        LG_LGCY_CLCJELV_CB,
     %                        SO_ALLC_CST2B(C_LOC(SELF)))

C---     Libère la fonction
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      LG_LGCY_CLCJELV = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_CLCJELV

      END SUBMODULE LG_LGCY_CLCJELV_M
