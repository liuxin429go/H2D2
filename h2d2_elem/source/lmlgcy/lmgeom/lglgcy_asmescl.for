C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LG_LGCY_ASMESCL
C   Private:
C     INTEGER LG_LGCY_ASMESCL_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LG_LGCY_M) LG_LGCY_ASMESCL_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction LG_LGCY_ASMESCL assemble les éléments de surface
C     des conditions limites
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LG_LGCY_ASMESCL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LG_LGCY_ASMESCL
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY:LM_GDTA_T
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'egfunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

      CALL LOG_TODO('Qui fait les appels à CP2GDTA??')
      IERR = SELF%ASGCMN()

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EG_FUNC_ASMESCL, HFNC)

C---     Fait l'appel
      GDTA => SELF%GDTA
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL(HFNC,
     &                       SO_ALLC_CST2B(GDTA%VCORG(:,1)),
     &                       SO_ALLC_CST2B(GDTA%KNGS(:,1)),
     &                       SO_ALLC_CST2B(GDTA%KCLLIM(:,1)),
     &                       SO_ALLC_CST2B(GDTA%KCLNOD),
     &                       SO_ALLC_CST2B(GDTA%KCLELE),
     &                       SO_ALLC_CST2B(GDTA%VCLDST))

C---     Libère la fonction
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      LG_LGCY_ASMESCL = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_ASMESCL

      END SUBMODULE LG_LGCY_ASMESCL_M
