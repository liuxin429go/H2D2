C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     INTEGER LG_LGCY_REQFNC
C     INTEGER LG_LGCY_REQNFN
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LG_LGCY_M) LG_LGCY_REQFNC_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Retourne la fonction d'après le code
C
C Description:
C     La fonction privée LG_LGCY_REQFNC retourne un handle HFUNC à
C     la fonction de code IFUNC. Il est de la responsabilité de
C     la fonction appelante de disposer du handle HFUNC par un appel
C     à SO_FUNC_DTR(...).
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IFUNC    Code de la fonction demandée
C
C Sortie:
C     HFUNC    Handle à la fonction
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LG_LGCY_REQFNC(SELF, IFUNC, HFUNC)

      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: IFUNC
      INTEGER, INTENT(OUT) :: HFUNC

      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER        IERR
      INTEGER        IPOS
      INTEGER        KFUNC(16)
      CHARACTER*(64) NFUNC
      CHARACTER*(64) NOMDLL
      CHARACTER*(64) NOMFNC
      EQUIVALENCE   (NFUNC, KFUNC)
C-----------------------------------------------------------------------

C---     Cherche le nom de la fonction
      NFUNC = ' '
      IERR = SELF%REQNFN(IFUNC, KFUNC)

C---     Split le nom
      IF (ERR_GOOD()) THEN
         CALL SP_STRN_TRM(NFUNC)
         NOMFNC = NFUNC(1:SP_STRN_LEN(NFUNC))
         IPOS = SP_STRN_LFT(NOMFNC, '@')
         NOMDLL = NFUNC(1:SP_STRN_LEN(NFUNC))
         IPOS = SP_STRN_RHT(NOMDLL, '@')
      ENDIF

C---     Charge la fonction
      IF (ERR_GOOD()) IERR=SO_FUNC_CTR   (HFUNC)
      IF (ERR_GOOD()) IERR=SO_FUNC_INIFSO(HFUNC,
     &                                    NOMDLL(1:SP_STRN_LEN(NOMDLL)),
     &                                    NOMFNC(1:SP_STRN_LEN(NOMFNC)))

      LG_LGCY_REQFNC = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_REQFNC

C************************************************************************
C Sommaire: Retourne la nom de la fonction d'après le code
C
C Description:
C     La fonction privée LG_LGCY_REQFNC retourne le nom de la fonction
C     de code IFUNC.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IFUNC    Code de la fonction demandée
C
C Sortie:
C     KFUNC    Le nom de la fonction
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LG_LGCY_REQNFN(SELF,
     &                                       IFUNC,
     &                                       KFUNC)

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B

      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: IFUNC
      INTEGER, INTENT(OUT) :: KFUNC(:)

      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HMDL
      INTEGER HFNC
C-----------------------------------------------------------------------

C---     Charge la fonction de requête de nom
      HFNC = 0
      HMDL = SELF%HMDUL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'REQNFN', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL(HFNC,
     &                       SO_ALLC_CST2B(KFUNC(1)),
     &                       SO_ALLC_CST2B(IFUNC))

      LG_LGCY_REQNFN = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_REQNFN

      END SUBMODULE LG_LGCY_REQFNC_M
      