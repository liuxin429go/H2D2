C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_FRML_XEQCTR
C     INTEGER IC_FRML_XEQMTH
C     CHARACTER*(32) IC_FRML_REQCLS
C     INTEGER IC_FRML_REQHDL
C   Private:
C     INTEGER IC_FRML_PRN
C     SUBROUTINE IC_FRML_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FRML_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FRML_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icfrml.fi'
      INCLUDE 'lmfrml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfrml.fc'

      INTEGER         IERR
      INTEGER         HOBJ
      CHARACTER*(128) FORML
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_FRML_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the formulation</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, FORML)
      IF (IERR .NE. 0) GOTO 9901
      CALL SP_STRN_TRM(FORML)

C---     Construis et initialise l'objet
      HOBJ = 0
      IF (ERR_GOOD()) IERR = LM_FRML_CTR   (HOBJ)
      IF (ERR_GOOD()) IERR = LM_FRML_INIFRM(HOBJ, FORML)

C---     Impression
      IF (ERR_GOOD()) THEN
         IERR = IC_FRML_PRN(HOBJ)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the formulation</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_FRML_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_FRML_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_FRML_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FRML_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmfrml.fi'
      INCLUDE 'icfrml.fc'

      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C     CALL ERR_PRE(LM_FRML_HVALIDE(OBJ))
C------------------------------------------------------------------------

C---     Entête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_FORMULATION'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Impression des paramètres du bloc
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      NOM = LM_FRML_REQFRML(HOBJ)
      CALL SP_STRN_CLP(NOM, 50)
      WRITE (LOG_BUF,'(3A)')  'MSG_TYPE_FORMULATION#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_FRML_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_FRML_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FRML_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'icfrml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfrml.fc'

      REAL*8         RVAL
      INTEGER        IERR
      INTEGER        IVAL
      CHARACTER*(256)SVAL
      CHARACTER*(64) PROP
      LOGICAL        LVAL
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Formulation name</comment>
         IF (PROP .EQ. 'name') THEN
            SVAL = LM_FRML_REQFRML(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', SVAL(1:SP_STRN_LEN(SVAL))

C        <comment>Geometric type of volume element</comment>
         ELSEIF (PROP .EQ. 'type_of_volume_element') THEN
            IVAL = LM_FRML_REQTGELV(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of degree of freedom per node</comment>
         ELSEIF (PROP .EQ. 'number_of_dof_per_node') THEN
            IVAL = LM_FRML_REQNDLN(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of degree of freedom per volume element</comment>
         ELSEIF (PROP .EQ. 'number_of_dof_per_volume_element') THEN
            IVAL = LM_FRML_REQNDLEV(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of degree of freedom per surface element</comment>
         ELSEIF (PROP .EQ. 'number_of_dof_per_surface_element') THEN
            IVAL = LM_FRML_REQNDLES(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of global properties (total)</comment>
         ELSEIF (PROP .EQ. 'number_of_global_properties_total') THEN
            IVAL = LM_FRML_REQNPRGL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of global properties (read)</comment>
         ELSEIF (PROP .EQ. 'number_of_global_properties_read') THEN
            IVAL = LM_FRML_REQNPRGLL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of nodal properties (total)</comment>
         ELSEIF (PROP .EQ. 'number_of_nodal_properties_total') THEN
            IVAL = LM_FRML_REQNPRNO(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of nodal properties (read)</comment>
         ELSEIF (PROP .EQ. 'number_of_nodal_properties_read') THEN
            IVAL = LM_FRML_REQNPRNOL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of volume elemental properties (total)</comment>
         ELSEIF (PROP .EQ. 'number_of_elemental_properties_total') THEN
            IVAL = LM_FRML_REQNPREV(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of volume elemental properties (read)</comment>
         ELSEIF (PROP .EQ. 'number_of_elemental_properties_read') THEN
            IVAL = LM_FRML_REQNPREVL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

!C        <comment>Number of surface elemental properties (total)</comment>
!         ELSEIF (PROP .EQ. 'number_of_dof_per_surface_element') THEN
!            IVAL = LM_FRML_REQNPRES(HOBJ)
!            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of distributed load per node (total)</comment>
         ELSEIF (PROP .EQ. 'number_of_distributed_load_total') THEN
            IVAL = LM_FRML_REQNSOLR(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of distributed load per node (read)</comment>
         ELSEIF (PROP .EQ. 'number_of_distributed_load_read') THEN
            IVAL = LM_FRML_REQNSOLRL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of concentrated load per node(total)</comment>
         ELSEIF (PROP .EQ. 'number_of_concentrated_load_total') THEN
            IVAL = LM_FRML_REQNSOLC(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of concentrated load per node(read)</comment>
         ELSEIF (PROP .EQ. 'number_of_concentrated_load_read') THEN
            IVAL = LM_FRML_REQNSOLCL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Is the formulation linear?</comment>
         ELSEIF (PROP .EQ. 'is_linear') THEN
            LVAL = LM_FRML_ESTLIN(HOBJ)
            WRITE(IPRM, '(2A,L3)') 'L', ',', LVAL

         ELSE
            GOTO 9902
         ENDIF

C     <comment>
C     The method <b>print_parameters</b> prints all the parameters of the object.
C     </comment>
      ELSEIF (IMTH .EQ. 'print_parameters') THEN
D        CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_HELE_HLP(HOBJ)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = LM_FRML_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = LM_FRML_PRN(HOBJ)
         IERR = IC_FRML_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_FRML_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_FRML_AID()

9999  CONTINUE
      IC_FRML_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FRML_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FRML_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icfrml.fi'
C-------------------------------------------------------------------------

C<comment>
C *****<br>
C This command has been DEPRECATED. Please use the specialized elements instead.<br>
C *****
C <p>
C The class <b>form</b> represents the formulation of a problem to solve. It
C will make the link between the data (mesh, etc.) and the problem.
C</comment>

      IC_FRML_REQCLS = 'form'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FRML_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FRML_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icfrml.fi'
      INCLUDE 'lmfrml.fi'
C-------------------------------------------------------------------------

      IC_FRML_REQHDL = LM_FRML_REQHBASE()
      RETURN
      END


C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_FRML_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_FRML_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icfrml.hlp')

      RETURN
      END
