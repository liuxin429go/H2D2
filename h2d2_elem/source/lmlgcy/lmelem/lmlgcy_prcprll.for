C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_LGCY
C         FTN (Sub)Module: LM_LGCY_PRCPRLL_M
C            Public:
C               MODULE INTEGER LM_LGCY_PRCPRLL
C            Private:
C               INTEGER LM_LGCY_PRCPRLL_TR
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_LGCY_M) LM_LGCY_PRCPRLL_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: Fonction de travail de LM_LGCY_PRCPRLL.
C
C Description:
C     La fonction privée LM_LGCY_PRCPRLL_TR est la fonction de travail
C     pour la fonction LM_LGCY_PRCPRLL.
C
C Entrée:
C
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_LGCY_PRCPRLL_TR(NDLN,
     &                                    NNL,
     &                                    KDIMP,
     &                                    KLOCN,
     &                                    HNUMR)

      INTEGER NDLN
      INTEGER NNL
      INTEGER KDIMP(NDLN, NNL)
      INTEGER KLOCN(NDLN, NNL)
      INTEGER HNUMR

      INCLUDE 'err.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER ID, IN
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.element.prc'

      IF (.NOT. NM_LCGL_HVALIDE(HNUMR)) GOTO 9999

C---     IMPOSE LES CONDITIONS
      DO IN=1,NNL
         IF (NM_LCGL_ESTNOPP(HNUMR,IN)) GOTO 199

         DO ID=1,NDLN
            KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_PARALLEL)
            KLOCN(ID,IN) = - KLOCN(ID,IN)
         ENDDO

199      CONTINUE
      ENDDO

      CALL LOG_DBG(LOG_ZNE, ' ')
      CALL LOG_DBG(LOG_ZNE, 'DBG_KDIMP_KLOCN_APRES_PRLL')
      DO IN=1,NNL
         DO ID=1,NDLN
            WRITE(LOG_BUF, *) IN, ID, KDIMP(ID,IN), KLOCN(ID,IN)
            CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         ENDDO
      ENDDO

9999  CONTINUE
      LM_LGCY_PRCPRLL_TR = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_PRCPRLL_TR

C************************************************************************
C Sommaire:
C
C Description:
C     La sous-routine LM_LGCY_PRCPRLL impose les conditions limites
C     liées au parallélisme.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_LGCY_PRCPRLL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_LGCY_PRCPRLL
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T

      CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

      IERR = SELF%ASGCMN()

C---     Récupère les données d'approximation
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Fait l'appel
      IERR = LM_LGCY_PRCPRLL_TR(EDTA%NDLN,
     &                          GDTA%NNL,
     &                          EDTA%KDIMP,
     &                          EDTA%KLOCN,
     &                          SELF%REQHNUMC())

      LM_LGCY_PRCPRLL = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_PRCPRLL

      END SUBMODULE LM_LGCY_PRCPRLL_M
