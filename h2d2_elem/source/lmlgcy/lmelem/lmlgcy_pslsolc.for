C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_LGCY_PSLSOLC
C   Private:
C     INTEGER LM_LGCY_PSLSOLC_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_LGCY_M) LM_LGCY_PSLSOLC_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Call-back privé.
C
C Description:
C     La fonction privée LM_LGCY_PSLSOLC_CB est le call-back pour
C     la fonction LM_LGCY_PSLSOLC.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_LGCY_PSLSOLC_CB(F, SELF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T

      EXTERNAL F
      CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les données d'approximation
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Fait l'appel
      CALL F(EDTA%VSOLC, IERR)

      LM_LGCY_PSLSOLC_CB = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_PSLSOLC_CB

C************************************************************************
C Sommaire: Post-lecture des sollicitations concentrées
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_LGCY_PSLSOLC(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_LGCY_PSLSOLC
CDEC$ ENDIF

      USE SO_FUNC_M, ONLY: SO_FUNC_CBFNC
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B
      USE ISO_C_BINDING, ONLY: C_LOC

      CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'eafunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
      LOGICAL LDTR
C-----------------------------------------------------------------------

      IERR = SELF%ASGCMN()

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EA_FUNC_PSLSOLC, HFNC, LDTR)

C---     Fait l'appel
      IF (ERR_GOOD())
     &    IERR = SO_FUNC_CBFNC(HFNC,
     &                         LM_LGCY_PSLSOLC_CB,
     &                         SO_ALLC_CST2B(C_LOC(SELF)))

C---     Libère la fonction
      IF (HFNC .NE. 0 .AND. LDTR) IERR = SO_FUNC_DTR(HFNC)

      LM_LGCY_PSLSOLC = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_PSLSOLC

      END SUBMODULE LM_LGCY_PSLSOLC_M
