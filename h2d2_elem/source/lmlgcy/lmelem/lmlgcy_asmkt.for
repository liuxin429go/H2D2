C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_LGCY_CLCKT
C   Private:
C     INTEGER LM_LGCY_CLCKT_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_LGCY_M) LM_LGCY_CLCKT_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Call-back privé.
C
C Description:
C     La fonction privée LM_LGCY_CLCKT_CB est le call-back pour
C     la fonction LM_LGCY_CLCKT.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_LGCY_CLCKT_CB(F, SELF, HMTRX, F_ASM)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T

      EXTERNAL F
      CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTRX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les données d'approximation
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Fait l'appel
      CALL F(GDTA%VCORG,
     &       EDTA%KLOCN,
     &       GDTA%KNGV,
     &       GDTA%KNGS,
     &       GDTA%VDJV,
     &       GDTA%VDJS,
     &       EDTA%VPRGL,
     &       EDTA%VPRNO,
     &       EDTA%VPREV,
     &       EDTA%VPRES,
     &       EDTA%VSOLC,
     &       EDTA%VSOLR,
     &       EDTA%KCLCND,
     &       EDTA%VCLCNV,
     &       GDTA%KCLLIM,
     &       GDTA%KCLNOD,
     &       GDTA%KCLELE,
     &       GDTA%VCLDST,
     &       EDTA%KDIMP,
     &       EDTA%VDIMP,
     &       EDTA%KEIMP,
     &       EDTA%VDLG,
     &       HMTRX,
     &       F_ASM)

      LM_LGCY_CLCKT_CB = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_CLCKT_CB

C************************************************************************
C Sommaire: Assemble la matrice [Kt]
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_LGCY_CLCKT(SELF, HMTRX, F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_LGCY_CLCKT
CDEC$ ENDIF

      USE SO_FUNC_M, ONLY: SO_FUNC_CBFNC
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B
      USE ISO_C_BINDING, ONLY: C_LOC

      CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTRX
      INTEGER F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'eafunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HSOF
      LOGICAL LDTR
      CLASS(LM_LGCY_T), POINTER :: SELF_P
C-----------------------------------------------------------------------

      SELF_P => SELF          ! const_cast
      IERR = SELF_P%ASGCMN()

C---     Charge la fonction
      HSOF = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EA_FUNC_ASMKT, HSOF, LDTR)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CBFNC(HSOF,
     &                        LM_LGCY_CLCKT_CB,
     &                        SO_ALLC_CST2B(C_LOC(SELF)),
     &                        SO_ALLC_CST2B(HMTRX),
     &                        SO_ALLC_CST2B(F_ASM,0_1))

C---     Libère la fonction
      IF (HSOF .NE. 0 .AND. LDTR) IERR = SO_FUNC_DTR(HSOF)

      LM_LGCY_CLCKT = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_CLCKT

      END SUBMODULE LM_LGCY_CLCKT_M
