C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_GEOM_CLCSRF
C   Private:
C     INTEGER LM_GEOM_ASMPEAU
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction LM_GEOM_CLCSRF assemble la peau pour des
C     éléments de volume de type T6L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION LM_GEOM_CLCSRF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_CLCSRF
CDEC$ ENDIF

      USE LM_GEOM_M
      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL MODIF

      INCLUDE 'lmgeom.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sppeau.fi'
      INCLUDE 'lmgeom.fc'

      INTEGER I_ERROR

      INTEGER IERR
      INTEGER LELS
      INTEGER NELS
      TYPE (LM_GDTA_DATA_T), POINTER :: GDTA
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      MODIF = .FALSE.

C---     Récupère les attributs
      GDTA => LM_GEOM_REQGDTA(HOBJ)

C---     Test si déjà dimensionné
      IF (ERR_BAD()) GOTO 9999
      IF (SO_ALLC_HEXIST(LELS)) GOTO 9999

C---     Alloue l'espace
      NELS = MAX(10, GDTA%NELLV)         ! HEURISTIQUE
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NELS*GDTA%NCELS, LELS)

C---     Filtre la peau
      IF (ERR_GOOD()) THEN
         IERR = SP_PEAU_FILTRE(GDTA%ITPEV,
     &                         GDTA%NNELV,
     &                         GDTA%NCELV,
     &                         GDTA%NELLV,
     &                         GDTA%KNGV,
     &                         GDTA%ITPES,
     &                         GDTA%NNELS,
     &                         GDTA%NCELS,
     &                         NELS,
     &                         KA(SO_ALLC_REQKIND(KA, LELS)))
      ENDIF

C---     Ré-alloue l'espace
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NELS*GDTA%NCELS, LELS)

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid')
      LM_GEOM_CLCSRF = ERR_TYP()
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire: SP_PEAU_FILTRE
!!!C
!!!C Description:
!!!C     La fonction SP_PEAU_FILTRE extrait la peau du maillage
!!!C     de volume passé en argument. Il est de la responsabilité de
!!!C     la fonction appelante de dimensionner la table KNGS des
!!!C     connectivités des éléments de surface.
!!!C
!!!C Entrée:
!!!C     ITPEV       TYPE DES ELEMENTS DE VOLUME
!!!C     NCELV       NOMBRE DE CONNECTIVITÉS PAR ELEMENT DE VOLUME
!!!C     NNELV       NOMBRE DE NOEUDS PAR ELEMENT DE VOLUME
!!!C     NELV        NOMBRE D'ELEMENTS DE VOLUME
!!!C     KNGV        CONNECTIVITES GLOBALE DE VOLUME
!!!C     ITPES       TYPE DES ELEMENTS DE SURFACE
!!!C     NNELS       NOMBRE DE NOEUDS PAR ELEMENT DE SURFACE
!!!C     NCELV       NOMBRE DE CONNECTIVITÉS PAR ELEMENT DE SURFACE
!!!C     NELS        DIMENSION DE LA TABLE KNGS
!!!C
!!!C Sortie:
!!!C     NELS        NOMBRE D'ELEMENTS DE SURFACE
!!!C     KNGS        CONNECTIVITES GLOBALE DE SURFACE
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION SP_PEAU_FILTRE (ITPEV,
!!!     &                         NNELV,
!!!     &                         NCELV,
!!!     &                         NELV,
!!!     &                         KNGV,
!!!     &                         ITPES,
!!!     &                         NNELS,
!!!     &                         NCELS,
!!!     &                         NELS,
!!!     &                         KNGS)
!!!C$pragma aux SP_PEAU_FILTRE export
!!!CDEC$ ATTRIBUTES DLLEXPORT :: SP_PEAU_FILTRE
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER ITPEV
!!!      INTEGER NNELV
!!!      INTEGER NCELV
!!!      INTEGER NELV
!!!      INTEGER KNGV (NCELV,NELV)
!!!      INTEGER ITPES
!!!      INTEGER NNELS
!!!      INTEGER NCELS
!!!      INTEGER NELS
!!!      INTEGER KNGS (NCELS,*)
!!!
!!!      INCLUDE 'sppeau.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'soallc.fi'
!!!
!!!      INTEGER IERR
!!!      INTEGER IE, IN
!!!      INTEGER L_PNT
!!!      INTEGER L_LIE
!!!      INTEGER LENCR
!!!      INTEGER NELS_NEW
!!!      INTEGER NNT
!!!      INTEGER SP_PEAU_XEQFLT
!!!      INTEGER SP_PEAU_TRFELE
!!!C-----------------------------------------------------------------------
!!!
!!!C---     RECHERCHE LA MAX DE NO DE NOEUD
!!!      NNT = 0
!!!      DO IE=1, NELV
!!!         DO IN=1, NNELV
!!!            NNT = MAX(NNT, KNGV(IN, IE))
!!!         ENDDO
!!!      ENDDO
!!!
!!!C---     ALLOUE L'ESPACE
!!!      L_PNT = 0
!!!      IERR = SO_ALLC_ALLINT(NNT  , L_PNT)
!!!      L_LIE = 0
!!!      LENCR = MAX(100, NINT(1.5*NELV))*4     ! 1.5 COEF. HEURISTIQUE,
!!!      IERR = SO_ALLC_ALLINT(LENCR, L_LIE)    ! 4 POUR DIMENSIONNER KLIEN
!!!
!!!C---     FILTRE
!!!100   CONTINUE
!!!         IERR = SP_PEAU_XEQFLT(ITPEV,
!!!     &                         NNELV,
!!!     &                         NCELV,
!!!     &                         NELV,
!!!     &                         KNGV,
!!!     &                         NNT,
!!!     &                         KA(SO_ALLC_REQKIND(KA,L_PNT)),
!!!     &                         LENCR/4,
!!!     &                         KA(SO_ALLC_REQKIND(KA,L_LIE)),
!!!     &                         NELS_NEW)
!!!         IF (.NOT. ERR_ESTMSG('ERR_LISTE_PLEINE')) GOTO 110
!!!         CALL ERR_RESET()
!!!         LENCR = NINT(1.5*LENCR)
!!!         IERR = SO_ALLC_ALLINT(LENCR, L_LIE)
!!!      GOTO 100
!!!110   CONTINUE
!!!
!!!C---     STOKE LES NOUVEAUX ELEMENTS
!!!      IF (ERR_GOOD()) THEN
!!!         IF (NELS_NEW .GT. NELS) GOTO 9900
!!!         IERR = SP_PEAU_TRFELE(ITPEV,
!!!     &                         NNELV,
!!!     &                         NCELV,
!!!     &                         NELV,
!!!     &                         KNGV,
!!!     &                         ITPES,
!!!     &                         NNELS,
!!!     &                         NCELS,
!!!     &                         NELS_NEW,
!!!     &                         KNGS,
!!!     &                         NNT,
!!!     &                         KA(SO_ALLC_REQKIND(KA,L_PNT)),
!!!     &                         LENCR/4,
!!!     &                         KA(SO_ALLC_REQKIND(KA,L_LIE)))
!!!         IF (ERR_GOOD()) NELS = NELS_NEW
!!!      ENDIF
!!!
!!!C---      RECUPERE L'ESPACE
!!!      IERR = SO_ALLC_ALLINT(0, L_LIE)
!!!      IERR = SO_ALLC_ALLINT(0, L_PNT)
!!!
!!!      GOTO 9999
!!!C----------------------------------------------------------------------
!!!9900  CALL ERR_ASG(ERR_ERR, 'ERR_TABLE_ELEM_SURFACE_MAL_DIMENSIONNEE')
!!!      GOTO 9999
!!!
!!!9999  CONTINUE
!!!      SP_PEAU_FILTRE = ERR_TYP()
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire: Filtre la peau avec une liste chaînée.
!!!C
!!!C Description:
!!!C     La fonction SP_PEAU_XEQFLT monte la liste chaînée qui contient
!!!C     les éléments de peau.
!!!C
!!!C Entrée:
!!!C       KNGV          GLOBAL CONECTIVITIES
!!!C       KPNT         \ ARRAY FOR THE
!!!C       KLIEN        / LINKED CHAIN
!!!C       NLIEN        MAX NUMBER OF ELEMENTS IN CHAIN
!!!C
!!!C Sortie:
!!!CC Notes: Coder pour P12L
!!!C************************************************************************
!!!      FUNCTION SP_PEAU_XEQFLT(ITPEV,
!!!     &                        NNELV,
!!!     &                        NCELV,
!!!     &                        NELV,
!!!     &                        KNGV,
!!!     &                        NNT,
!!!     &                        KPNT,
!!!     &                        NLIEN,
!!!     &                        KLIEN,
!!!     &                        NELS)
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER SP_PEAU_XEQFLT
!!!      !INTEGER ITPEV
!!!      !INTEGER NNELV
!!!      !INTEGER NCELV
!!!      !INTEGER NELV
!!!      !INTEGER KNGV (NCELV,NELV)
!!!      INTEGER NNT
!!!      INTEGER KPNT (*)
!!!      INTEGER NLIEN
!!!      INTEGER KLIEN(4,*)
!!!      INTEGER NELS
!!!
!!!      INCLUDE 'err.fi'
!!!!!!      INCLUDE 'egtpgeo.fi'
!!!      INCLUDE 'splstc.fi'
!!!
!!!      INTEGER INEXT
!!!      INTEGER IERR
!!!C----------------------------------------------------------------------
!!!
!!!C---     INITIALIZE THE LINKED CHAIN
!!!      IERR = SP_LSTC_INIT(NNT, KPNT, 3, NLIEN, KLIEN, INEXT)
!!!
!!!      IERR  LM_GEOM_ASMPEAU(HOBJ)
!!!
!!!C---     GET NUMBER OF ELEMENTS OF SKIN
!!!      NELS = SP_LSTC_REQDIM(NNT, KPNT, 3, NLIEN, KLIEN)
!!!
!!!      SP_PEAU_XEQFLT = ERR_TYP()
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire: SP_PEAU_TRFELE
!!!C
!!!C Description:
!!!C     SAUVEGARDE LES CONNECTIVITÉS DES ÉLÉMENTS DE PEAU
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION SP_PEAU_TRFELE (ITPEV,
!!!     &                         NNELV,
!!!     &                         NCELV,
!!!     &                         NELV,
!!!     &                         KNGV,
!!!     &                         ITPES,
!!!     &                         NNELS,
!!!     &                         NCELS,
!!!     &                         NELS,
!!!     &                         KNGS,
!!!     &                         NNT,
!!!     &                         KPNT,
!!!     &                         NLIEN,
!!!     &                         KLIEN)
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER SP_PEAU_TRFELE
!!!      INTEGER ITPEV
!!!      INTEGER NNELV
!!!      INTEGER NCELV
!!!      INTEGER NELV
!!!      INTEGER KNGV (NCELV,NELV)
!!!      INTEGER ITPES
!!!      INTEGER NNELS
!!!      INTEGER NCELS
!!!      INTEGER NELS
!!!      INTEGER KNGS (NCELS,NELS)
!!!      INTEGER NNT
!!!      INTEGER KPNT (*)
!!!      INTEGER NLIEN
!!!      INTEGER KLIEN(4, *)
!!!
!!!      INCLUDE 'splstc.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'egtpgeo.fi'
!!!
!!!      INTEGER IERR
!!!      INTEGER IEL
!!!      INTEGER IN
!!!      INTEGER IACTU
!!!      INTEGER INFO(3)
!!!      INTEGER IMST, ICOT
!!!      INTEGER NNO1, NNO2, NNO3
!!!      INTEGER NOP1, NOP3, NOP5, NOEMIL
!!!C-----------------------------------------------------------------------
!!!
!!!C---     STOKE LES ELEMENTS
!!!      IEL = 0
!!!      DO IN=1,NNT
!!!         IACTU = 0
!!!
!!!    Au besoin, resynchroniser avec  E:\h2d2-dev\H2D2\h2d2_umef\source\sppeau.for
!!!
!!!200      CONTINUE
!!!            IERR = SP_LSTC_REQCHN(NNT, KPNT, 3, NLIEN, KLIEN,
!!!     &                            IN, IACTU, INFO)
!!!            IF (ERR_BAD()) THEN
!!!               CALL ERR_RESET()
!!!               GOTO 299
!!!            ENDIF
!!!
!!!            IMST = INFO(3)
!!!            IF (IMST .GT. 0) THEN
!!!               IMST = IMST
!!!               NNO1 = IN
!!!               NNO2 = INFO(1)
!!!               NNO3 = INFO(2)
!!!            ELSE
!!!               IMST =-IMST
!!!               NNO2 = IN
!!!               NNO1 = INFO(1)
!!!               NNO3 = INFO(2)
!!!            ENDIF
!!!            IEL = IEL + 1
!!!
!!!C---           ELEMENT POINT1
!!!            IF (ITPES .EQ. EG_TPGEO_P1) THEN
!!!               KNGS(1,IEL) = NNO1
!!!               KNGS(2,IEL) = IMST
!!!
!!!C---           ELEMENT L2
!!!            ELSEIF (ITPES .EQ. EG_TPGEO_L2) THEN
!!!               KNGS(1,IEL) = NNO1
!!!               KNGS(2,IEL) = NNO2
!!!               KNGS(3,IEL) = IMST
!!!
!!!C---           ELEMENT L3L
!!!            ELSEIF (ITPES .EQ. EG_TPGEO_L3L) THEN
!!!               KNGS(1,IEL) = NNO1
!!!               KNGS(2,IEL) = -1
!!!               KNGS(3,IEL) = NNO2
!!!               KNGS(4,IEL) = IMST
!!!               KNGS(5,IEL) = -1
!!!C---              RECHERCHE DU NOEUD MILIEU
!!!               NOP1 = KNGV(1,IMST)
!!!               NOP3 = KNGV(3,IMST)
!!!               NOP5 = KNGV(5,IMST)
!!!               IF     (NNO1.EQ.NOP1 .AND. NNO2.EQ.NOP3) THEN  ! COTE 1-3
!!!                  NOEMIL = KNGV(2,IMST)
!!!                  ICOT = 1
!!!               ELSEIF (NNO1.EQ.NOP3 .AND. NNO2.EQ.NOP5) THEN  ! COTE 3-5
!!!                  NOEMIL = KNGV(4,IMST)
!!!                  ICOT = 2
!!!               ELSEIF (NNO1.EQ.NOP5 .AND. NNO2.EQ.NOP1) THEN  ! COTE 5-1
!!!                  NOEMIL = KNGV(6,IMST)
!!!                  ICOT = 3
!!!               ELSE
!!!                  GOTO 9900
!!!               ENDIF
!!!               KNGS(2,IEL) = NOEMIL
!!!               KNGS(5,IEL) = ICOT
!!!
!!!C---           ELEMENT L3
!!!            ELSEIF (ITPES .EQ. EG_TPGEO_L3) THEN
!!!               KNGS(1,IEL) = NNO1
!!!               KNGS(2,IEL) = -1
!!!               KNGS(3,IEL) = NNO2
!!!               KNGS(4,IEL) = IMST
!!!               KNGS(5,IEL) = -1
!!!C---              RECHERCHE DU NOEUD MILIEU
!!!               NOP1 = KNGV(1,IMST)
!!!               NOP3 = KNGV(3,IMST)
!!!               NOP5 = KNGV(5,IMST)
!!!               IF     (NNO1.EQ.NOP1 .AND. NNO2.EQ.NOP3) THEN  ! COTE 1-3
!!!                  NOEMIL = KNGV(2,IMST)
!!!                  ICOT = 1
!!!               ELSEIF (NNO1.EQ.NOP3 .AND. NNO2.EQ.NOP5) THEN  ! COTE 3-5
!!!                  NOEMIL = KNGV(4,IMST)
!!!                  ICOT = 2
!!!               ELSEIF (NNO1.EQ.NOP5 .AND. NNO2.EQ.NOP1) THEN  ! COTE 5-1
!!!                  NOEMIL = KNGV(6,IMST)
!!!                  ICOT = 3
!!!               ELSE
!!!                  GOTO 9900
!!!               ENDIF
!!!               KNGS(2,IEL) = NOEMIL
!!!               KNGS(5,IEL) = ICOT
!!!
!!!C---           ELEMENT Q4
!!!            ELSEIF (NNELV .EQ. 4) THEN
!!!               call log_todo('SP_PEAU_TRFELE Q4 non valide')
!!!               KNGS(1,IEL) = NNO1
!!!               KNGS(2,IEL) = NNO2
!!!               KNGS(3,IEL) = NNO3
!!!               KNGS(4,IEL) = IMST
!!!            ENDIF
!!!
!!!         GOTO 200
!!!299      CONTINUE
!!!
!!!      ENDDO
!!!
!!!      GOTO 9999
!!!C-----------------------------------------------------------------------
!!!9900  CALL ERR_ASG(ERR_ERR, 'ERR_CONNEC_INVALIDE_PILE_CORROMPUE')
!!!      GOTO 9999
!!!
!!!9999  CONTINUE
!!!      SP_PEAU_TRFELE = ERR_TYP()
!!!      RETURN
!!!      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction LM_GOT6L_ASMPEAU assemble la peau pour des
C     éléments de volume de type T6L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION LM_GEOM_ASMPEAU(HOBJ)

      USE LM_GEOM_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER LM_GEOM_ASMPEAU
      INTEGER HOBJ

      INCLUDE 'lmgeom.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfcod.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'lmgeom.fc'

      INTEGER IERR
      INTEGER HMETH
      TYPE (LM_GEOM_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_GEOM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Transfert les valeurs
      IERR = LM_GEOM_ASGGRID(HOBJ)

C---     Les attributs
      SELF => LM_GEOM_REQSELF(HOBJ)

C---     La méthode
      HMETH = 0
      IF (ERR_GOOD()) IERR = OB_VTBL_REQMTH(SELF%HVTBL,
     &                                      LM_VTBL_FNC_ASMPEAU,
     &                                      HMETH)

C---     L'appel
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL0(HMETH)

      LM_GEOM_ASMPEAU = ERR_TYP()
      RETURN
      END


