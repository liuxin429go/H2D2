C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Note:
C  Élément géométrique abstrait.
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_GEOM
C         FTN (Sub)Module: LM_GEOM_M
C            Public:
C               SUBROUTINE LM_GEOM_CTR
C               INTEGER LM_GEOM_DTR
C               INTEGER LM_GEOM_DEL
C            Private:
C            
C            FTN Type: LM_GEOM_T
C               Public:
C                  INTEGER LM_GEOM_INI
C                  INTEGER LM_GEOM_RST
C                  INTEGER LM_GEOM_PASDEB
C                  INTEGER LM_GEOM_REQHGRID
C                  INTEGER LM_GEOM_REQHLIMT
C                  INTEGER LM_GEOM_REQHNUMC
C                  INTEGER LM_GEOM_REQHNUME
C                  INTEGER LM_GEOM_REQHCONF
C                  INTEGER LM_GEOM_REQHDIST
C                  INTEGER LM_GEOM_REQPRM
C               Private:
C                  INTEGER LM_GEOM_INIGR
C                  INTEGER LM_GEOM_RAZ
C                  INTEGER LM_GEOM_INIPRMS
C
C************************************************************************

      MODULE LM_GEOM_M

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE GR_GRID_M, ONLY: GR_GRID_T
      IMPLICIT NONE

      PRIVATE

C========================================================================
C========================================================================
C---     Type de donnée associé à la classe
      TYPE, PUBLIC, ABSTRACT :: LM_GEOM_T
         CLASS(GR_GRID_T), POINTER :: OGRID
         INTEGER, PRIVATE :: HLIMT
         TYPE(LM_GDTA_T) :: GDTA
      CONTAINS
         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INI     => LM_GEOM_INI
         PROCEDURE, PUBLIC :: RST     => LM_GEOM_RST
         PROCEDURE, PUBLIC :: PASDEB  => LM_GEOM_PASDEB

         ! ---  Méthodes virtuelles
         PROCEDURE(DTR_GEN),     PUBLIC, DEFERRED :: DTR
         PROCEDURE(ASMESCL_GEN), PUBLIC, DEFERRED :: ASMESCL
         !!!PROCEDURE(ASMPEAU_GEN), PUBLIC, DEFERRED :: ASMPEAU
         PROCEDURE(CLCERR_GEN ), PUBLIC, DEFERRED :: CLCERR
         PROCEDURE(CLCJELS_GEN), PUBLIC, DEFERRED :: CLCJELS
         PROCEDURE(CLCJELV_GEN), PUBLIC, DEFERRED :: CLCJELV
         PROCEDURE(CLCSPLT_GEN), PUBLIC, DEFERRED :: CLCSPLT
         PROCEDURE(INTRP_GEN  ), PUBLIC, DEFERRED :: INTRP
         PROCEDURE(LCLELV_GEN ), PUBLIC, DEFERRED :: LCLELV

         ! ---  Getter
         PROCEDURE, PUBLIC :: REQHCONF => LM_GEOM_REQHCONF
         PROCEDURE, PUBLIC :: REQHDIST => LM_GEOM_REQHDIST
         PROCEDURE, PUBLIC :: REQHGRID => LM_GEOM_REQHGRID
         PROCEDURE, PUBLIC :: REQHLIMT => LM_GEOM_REQHLIMT
         PROCEDURE, PUBLIC :: REQHNUMC => LM_GEOM_REQHNUMC
         PROCEDURE, PUBLIC :: REQHNUME => LM_GEOM_REQHNUME
         PROCEDURE, PUBLIC :: REQPRM   => LM_GEOM_REQPRM

         ! ---  Méthodes "protégées"
         PROCEDURE, PUBLIC :: CP2GDTA => LM_GEOM_CP2GDTA

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: INIGR   => LM_GEOM_INIGR
         PROCEDURE, PRIVATE :: INIPRMS => LM_GEOM_INIPRMS
         PROCEDURE, PRIVATE :: RAZ     => LM_GEOM_RAZ
      END TYPE LM_GEOM_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: LM_GEOM_CTR
      PUBLIC :: LM_GEOM_DTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE LM_GEOM_DEL
      END INTERFACE DEL

      !========================================================================
      ! ---  Interface abstraite
      !========================================================================
      ABSTRACT INTERFACE

         INTEGER FUNCTION DTR_GEN(SELF)
            IMPORT :: LM_GEOM_T
            CLASS(LM_GEOM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION DTR_GEN

         INTEGER FUNCTION ASMESCL_GEN(SELF)
            IMPORT :: LM_GEOM_T
            CLASS(LM_GEOM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION ASMESCL_GEN

!!!         INTEGER FUNCTION ASMPEAU_GEN(SELF)
!!!            IMPORT :: LM_GEOM_T
!!!            CLASS(LM_GEOM_T), INTENT(IN) :: SELF
!!!         END FUNCTION ASMPEAU_GEN

         INTEGER FUNCTION CLCERR_GEN(SELF, VFRM, VHESS, VHNRM, ITPCLC)
            IMPORT :: LM_GEOM_T
            CLASS(LM_GEOM_T), INTENT(IN), TARGET :: SELF
            REAL*8,  INTENT(IN)  :: VFRM (:)
            REAL*8,  INTENT(OUT) :: VHESS(:,:)
            REAL*8,  INTENT(OUT) :: VHNRM(:)
            INTEGER, INTENT(IN)  :: ITPCLC
         END FUNCTION CLCERR_GEN

         INTEGER FUNCTION CLCJELS_GEN(SELF)
            IMPORT :: LM_GEOM_T
            CLASS(LM_GEOM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION CLCJELS_GEN

         INTEGER FUNCTION CLCJELV_GEN(SELF)
            IMPORT :: LM_GEOM_T
            CLASS(LM_GEOM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION CLCJELV_GEN

         INTEGER FUNCTION CLCSPLT_GEN(SELF, KNGZ)
            IMPORT :: LM_GEOM_T
            CLASS(LM_GEOM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(OUT) :: KNGZ(:, :)
         END FUNCTION CLCSPLT_GEN

         INTEGER FUNCTION INTRP_GEN(SELF, KELE, VCORE,
     &                              INDXS, VSRC,
     &                              INDXD, VDST)
            IMPORT :: LM_GEOM_T
            CLASS(LM_GEOM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN)  :: KELE (:)
            REAL*8,  INTENT(IN)  :: VCORE(:, :)
            INTEGER, INTENT(IN)  :: INDXS
            REAL*8,  INTENT(IN)  :: VSRC (:, :)
            INTEGER, INTENT(IN)  :: INDXD
            REAL*8,  INTENT(OUT) :: VDST (:, :)
         END FUNCTION INTRP_GEN

         INTEGER FUNCTION LCLELV_GEN(SELF, TOL, VCORP, KELEP, VCORE)
            IMPORT :: LM_GEOM_T
            CLASS(LM_GEOM_T), INTENT(IN), TARGET :: SELF
            REAL*8,  INTENT(IN)  :: TOL
            REAL*8,  INTENT(IN)  :: VCORP(:, :)
            INTEGER, INTENT(OUT) :: KELEP(:)
            REAL*8,  INTENT(OUT) :: VCORE(:, :)
         END FUNCTION LCLELV_GEN

      END INTERFACE  ! ABSTRACT INTERFACE

      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
         MODULE INTEGER FUNCTION LM_GEOM_CP2GDTA(SELF)
            CLASS (LM_GEOM_T), INTENT(INOUT), TARGET :: SELF
         END  FUNCTION LM_GEOM_CP2GDTA
      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La classe est abstraite, OKID doit exister.
C************************************************************************
      FUNCTION LM_GEOM_CTR(OKID) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_CTR
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(IN), TARGET :: OKID

      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(LM_GEOM_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Cast le pointeur
      SELF => OKID

C---     Initialise
      IF (ERR_GOOD()) IERR = SELF%RAZ()

C---     Initialise les dimensions
      IF (ERR_GOOD()) IERR = SELF%INIPRMS()

      RETURN
      END FUNCTION LM_GEOM_CTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_DTR
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER  IERR
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()

      LM_GEOM_DTR = ERR_TYP()
      RETURN
      END FUNCTION LM_GEOM_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     SELF est passé par pointeur pour permettre la précondition
C************************************************************************
      INTEGER FUNCTION LM_GEOM_DEL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_DEL
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(INOUT), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(ASSOCIATED(SELF))
C------------------------------------------------------------------------

      LM_GEOM_DEL = SELF%DTR()
      RETURN
      END FUNCTION LM_GEOM_DEL

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée LM_GEOM_INIGR
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La limite est une DT_LIMT. Il n'y a pas de GR_LIMT
C************************************************************************
      INTEGER FUNCTION LM_GEOM_INIGR(SELF,
     &                               HCONF,
     &                               HGRID,
     &                               HLIMT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_INIGR
CDEC$ ENDIF

      USE GR_GRID_M, ONLY: GR_GRID_CTR

      CLASS(LM_GEOM_T), INTENT(INOUT) :: SELF
      INTEGER, INTENT(IN) :: HCONF
      INTEGER, INTENT(IN) :: HGRID
      INTEGER, INTENT(IN) :: HLIMT

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Crée le maillage
      IF (ERR_GOOD()) SELF%OGRID => GR_GRID_CTR()
      IF (ERR_GOOD()) IERR = SELF%OGRID%INI(SELF%GDTA, HGRID, HCONF)

C---     Crée la limite
      SELF%HLIMT = HLIMT    ! cf. note

      LM_GEOM_INIGR = ERR_TYP()
      RETURN
      END FUNCTION LM_GEOM_INIGR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     SELF        Handle sur l'objet courant
C
C Sortie:
C     HGRID       Handle sur la maillage
C     HLIMT       Handle sur les limites
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_INI(SELF, HCONF, HGRID, HLIMT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_INI
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(INOUT), TARGET :: SELF
      INTEGER, INTENT(IN) :: HCONF
      INTEGER, INTENT(IN) :: HGRID
      INTEGER, INTENT(IN) :: HLIMT

      INCLUDE 'err.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtgrid.fi'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HGRID))
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HLIMT))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()

C---     Construis les données de simulation
      IF (ERR_GOOD()) IERR = SELF%INIGR(HCONF,
     &                                  HGRID,
     &                                  HLIMT)

C---     Transfert dans GDTA
      IF (ERR_GOOD()) IERR = SELF%CP2GDTA()

      LM_GEOM_INI = ERR_TYP()
      RETURN
      END FUNCTION LM_GEOM_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_RST(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_RST
CDEC$ ENDIF

      USE GR_GRID_M, ONLY: DEL

      CLASS(LM_GEOM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      ! ---  Détruis le maillage
      IF (ASSOCIATED(SELF%OGRID)) IERR = DEL(SELF%OGRID)

      IERR = LM_GEOM_RAZ(SELF)

      LM_GEOM_RST = ERR_TYP()
      RETURN
      END FUNCTION LM_GEOM_RST

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_RAZ(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_RAZ
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      SELF%OGRID => NULL()
      SELF%HLIMT =  0
      IERR = SELF%GDTA%RST()

      LM_GEOM_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode LM_GEOM_INIPRMS initialise les paramètres de
C     dimension de l'objet. Ils sont vus comme des attributs statiques.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_INIPRMS(SELF)

      CLASS(LM_GEOM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'egtpgeo.fi'
      INCLUDE 'err.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA

C---     Initialise les paramètres invariant de l'élément parent virtuel
      GDTA%NDIM   = -1
      GDTA%ITPEV  = EG_TPGEO_INDEFINI    ! TYPE DE GEOMETRIE DE L'ELEMENT DE VOLUME
      GDTA%NNELV  = -1                   ! NB DE NOEUDS
      GDTA%NCELV  = -1                   ! NB DE CONNEC
      GDTA%NDJV   = -1                   ! NB DE TERME DU DETJ
      GDTA%ITPES  = EG_TPGEO_INDEFINI    ! TYPE DE GEOMETRIE DE L'ELEMENT DE SURFACE
      GDTA%NNELS  = -1                   ! NB DE NOEUDS
      GDTA%NCELS  = -1                   ! NB DE CONNEC
      GDTA%NDJS   = -1                   ! NB DE TERME DU DETJ
      GDTA%ITPEZ  = EG_TPGEO_INDEFINI    ! TYPE DE GEOMETRIE DE SPLIT
      GDTA%NNELZ  = -1                   ! NB DE NOEUDS
      GDTA%NCELZ  = -1                   ! NB DE CONNEC
      GDTA%NDJZ   = -1                   ! NB DE TERME DU DETJ
      GDTA%NELCOL =  1                   ! NB DE COULEURS
      GDTA%KELCOL(:,1) = (/-1, -1/)

      LM_GEOM_INIPRMS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lecture.
C
C Description:
C     La sous-routine LM_GEOM_PASDEB fait toute la lecture des données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_GEOM_PASDEB(SELF, TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_PASDEB
CDEC$ ENDIF

      USE GR_GRID_M ! , ONLY: GR_GRID_T

      CLASS(LM_GEOM_T), INTENT(INOUT), TARGET :: SELF
      REAL*8, INTENT(IN) :: TSIM

      INCLUDE 'err.fi'
      INCLUDE 'dtlimt.fi'

      INTEGER IERR
      INTEGER HLIMT, HNUMR
      LOGICAL MODIF
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Charge le maillage
      IF (ERR_GOOD()) IERR = SELF%OGRID%PASDEB(SELF%GDTA, TSIM, MODIF)

C---     Charge la limite
      IF (ERR_GOOD()) THEN
         HLIMT = SELF%HLIMT
         HNUMR = SELF%OGRID%REQHNUMC()
         IERR = DT_LIMT_CHARGE(HLIMT, HNUMR, TSIM)
      ENDIF

C---     Transfert dans GDTA
      IF (ERR_GOOD()) IERR = SELF%CP2GDTA()

C---     Calcul les métriques élémentaires
      IF (ERR_GOOD() .AND. MODIF) IERR = SELF%CLCJELV()
      IF (ERR_GOOD() .AND. MODIF) IERR = SELF%CLCJELS()

      LM_GEOM_PASDEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur le maillage.
C
C Description:
C     La méthode LM_GEOM_REQHGRID(...) retourne le handle sur le maillage.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_REQHGRID(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_REQHGRID
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      LM_GEOM_REQHGRID = SELF%OGRID%REQHGRID()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur les limites.
C
C Description:
C     La méthode LM_GEOM_REQHLIMT(...) retourne le handle sur les limites.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_REQHLIMT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_REQHLIMT
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      LM_GEOM_REQHLIMT = SELF%HLIMT
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur la numérotation des noeuds.
C
C Description:
C     La méthode LM_GEOM_REQHNUMC(...) retourne le handle sur la
C     numérotation des noeuds.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_REQHNUMC(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_REQHNUMC
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      LM_GEOM_REQHNUMC = SELF%OGRID%REQHNUMC()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle la numérotation des éléments
C
C Description:
C     La méthode LM_GEOM_REQHNUME(...) retourne le handle sur la
C        numérotation des éléments.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_REQHNUME(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_REQHNUME
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      LM_GEOM_REQHNUME = SELF%OGRID%REQHNUME()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur la configuration GPU.
C
C Description:
C     La méthode LM_GEOM_REQHCONF(...) retourne le handle sur la
C     configuration GPU.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_REQHCONF(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_REQHCONF
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      LM_GEOM_REQHCONF = SELF%OGRID%REQHCONF()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur la distribution GPU des noeuds.
C
C Description:
C     La méthode LM_GEOM_REQHDIST(...) retourne le handle sur la
C     distribution GPU des noeuds.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_REQHDIST(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_REQHDIST
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      LM_GEOM_REQHDIST = SELF%OGRID%REQHDIST()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GEOM_REQPRM(SELF, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_REQPRM
CDEC$ ENDIF

      CLASS(LM_GEOM_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: IPRM

      INCLUDE 'lmgeom.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER IVAL
C-----------------------------------------------------------------------

      GDTA => SELF%GDTA
      SELECT CASE(IPRM)
         CASE(LM_GEOM_PRM_NDIM);  IVAL = GDTA%NDIM

         CASE(LM_GEOM_PRM_TGELV); IVAL = GDTA%ITPEV
         CASE(LM_GEOM_PRM_NNELV); IVAL = GDTA%NNELV
         CASE(LM_GEOM_PRM_NCELV); IVAL = GDTA%NCELV
         CASE(LM_GEOM_PRM_NDJV);  IVAL = GDTA%NDJV

         CASE(LM_GEOM_PRM_TGELS); IVAL = GDTA%ITPES
         CASE(LM_GEOM_PRM_NNELS); IVAL = GDTA%NNELS
         CASE(LM_GEOM_PRM_NCELS); IVAL = GDTA%NCELS
         CASE(LM_GEOM_PRM_NDJS);  IVAL = GDTA%NDJS

         CASE(LM_GEOM_PRM_TGELZ); IVAL = GDTA%ITPEZ
         CASE(LM_GEOM_PRM_NNELZ); IVAL = GDTA%NNELZ
         CASE(LM_GEOM_PRM_NCELZ); IVAL = GDTA%NCELZ
         CASE(LM_GEOM_PRM_NDJZ);  IVAL = GDTA%NDJZ
         CASE(LM_GEOM_PRM_NEV2EZ);IVAL = GDTA%NEV2EZ

         CASE(LM_GEOM_PRM_NNP);   IVAL = GDTA%NNP
         CASE(LM_GEOM_PRM_NNL);   IVAL = GDTA%NNL
         CASE(LM_GEOM_PRM_NNT);   IVAL = GDTA%NNT

         CASE default
            CALL ERR_ASR(.false.)
      END SELECT

      LM_GEOM_REQPRM = IVAL
      RETURN
      END

      END MODULE LM_GEOM_M
