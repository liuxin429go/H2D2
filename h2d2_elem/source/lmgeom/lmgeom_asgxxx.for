C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Functions:
C   Public:
C     INTEGER LM_GEOM_CP2GDTA
C   Private:
C     INTEGER LM_GEOM_ASGCLIM
C     INTEGER LM_GEOM_ASGCOOR
C     INTEGER LM_GEOM_ASGELES
C     INTEGER LM_GEOM_ASGELEV
C     INTEGER LM_GEOM_ASGFRMV
C     INTEGER LM_GEOM_ASGFRMS
C
C************************************************************************

      SUBMODULE(LM_GEOM_M) LM_GEOM_2GDTA_M
      
      USE GR_ELEM_M, ONLY: GR_ELEM_T
      IMPLICIT NONE
      
      !PRIVATE :: LM_GEOM_ASGCLIM
      !PRIVATE :: LM_GEOM_ASGCOOR
      !PRIVATE :: LM_GEOM_ASGELES
      !PRIVATE :: LM_GEOM_ASGELEV

      CONTAINS
      
C************************************************************************
C Sommaire:
C
C Description:
C     La fonction utilitaire LM_GEOM_ASGCLIM
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_GEOM_ASGCLIM(SELF)

      USE SO_ALLC_M
      CLASS (LM_GEOM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER HLIMT
      INTEGER LCLLIM, LCLNOD, LCLELE, LCLDST
      INTEGER,POINTER :: PCLLIM(:)
      INTEGER,POINTER :: PCLNOD(:)
      INTEGER,POINTER :: PCLELE(:)
      REAL*8, POINTER :: PCLDST(:)
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA  => SELF%GDTA
      HLIMT = SELF%REQHLIMT()
D     CALL ERR_ASR(DT_LIMT_HVALIDE(HLIMT))

C---     Les tables
      LCLLIM = DT_LIMT_REQLCLLIM(HLIMT)
      LCLNOD = DT_LIMT_REQLCLNOD(HLIMT)
      LCLELE = DT_LIMT_REQLCLELE(HLIMT)
      LCLDST = DT_LIMT_REQLCLDST(HLIMT)
      IF (LCLLIM .NE. 0) PCLLIM => SO_ALLC_REQKPTR(LCLLIM)
      IF (LCLNOD .NE. 0) PCLNOD => SO_ALLC_REQKPTR(LCLNOD)
      IF (LCLELE .NE. 0) PCLELE => SO_ALLC_REQKPTR(LCLELE)
      IF (LCLDST .NE. 0) PCLDST => SO_ALLC_REQVPTR(LCLDST)

C---     Assigne
      GDTA%NCLLIM = DT_LIMT_REQNCLLIM(HLIMT)
      GDTA%NCLNOD = DT_LIMT_REQNCLNOD(HLIMT)
      GDTA%NCLELE = DT_LIMT_REQNCLELE(HLIMT)

      IF (LCLLIM .NE. 0) GDTA%KCLLIM(1:7, 1:GDTA%NCLLIM) => PCLLIM
      IF (LCLNOD .NE. 0) GDTA%KCLNOD(1:GDTA%NCLNOD) => PCLNOD
      IF (LCLELE .NE. 0) GDTA%KCLELE(1:GDTA%NCLELE) => PCLELE
      IF (LCLDST .NE. 0) GDTA%VCLDST(1:GDTA%NCLNOD) => PCLDST

      LM_GEOM_ASGCLIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les dimensions sont prises de HGRDI
C     Les pointeurs sont pris de ceux qui les possèdent (DT ou GR)
C************************************************************************
      INTEGER FUNCTION LM_GEOM_ASGCOOR(SELF)

      USE SO_ALLC_M

      CLASS (LM_GEOM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER HCOOR
      INTEGER LCORG
      REAL*8, POINTER :: PCORG(:)
      TYPE(SO_ALLC_KPTR2_T) :: PKNGV
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (GR_GRID_T), POINTER :: OGRID
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA  => SELF%GDTA
      OGRID => SELF%OGRID

C---     Les coordonnées
      HCOOR = OGRID%REQHCOOR()
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HCOOR))
D     CALL ERR_ASR(DT_VNOD_REQNVNO(HCOOR) .EQ. GDTA%NDIM)

C---     Les tables
      LCORG = DT_VNOD_REQLVNO(HCOOR)
      IF (LCORG .NE. 0) PCORG => SO_ALLC_REQVPTR(LCORG)

C---     Assigne
      GDTA%NNL = OGRID%REQNNL()
      GDTA%NNT = OGRID%REQNNT()
      IF (LCORG .NE. 0) GDTA%VCORG(1:GDTA%NDIM, 1:GDTA%NNL) => PCORG

      LM_GEOM_ASGCOOR = ERR_TYP()
      RETURN
      END FUNCTION LM_GEOM_ASGCOOR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les dimensions sont prises de HGRID
C     Les pointeurs sont pris de ceux qui les possèdent (DT ou GR)
C
C************************************************************************
      INTEGER FUNCTION LM_GEOM_ASGELES(SELF)

      USE SO_ALLC_M
      IMPLICIT NONE

      CLASS (LM_GEOM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER HELEM
      INTEGER LNGS
      TYPE(SO_ALLC_KPTR2_T) :: PKNGS
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (GR_GRID_T), POINTER :: OGRID
      TYPE (GR_ELEM_T), POINTER :: OELES
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA  => SELF%GDTA
      OGRID => SELF%OGRID
      OELES => OGRID%OELES

C---     Les tables
      PKNGS = OELES%REQPELE()

C---     Assigne
      GDTA%NELLS =  OGRID%REQNELLS()
      GDTA%KNGS  => PKNGS%KPTR
D     CALL ERR_ASR(GDTA%NCELS .EQ. SIZE(GDTA%KNGS, 1))
D     CALL ERR_ASR(GDTA%NELLS .EQ. SIZE(GDTA%KNGS, 2))

      LM_GEOM_ASGELES = ERR_TYP()
      RETURN
      END FUNCTION LM_GEOM_ASGELES

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les dimensions sont prises de HGRDI
C     Les pointeurs sont pris de ceux qui les possèdent (DT ou GR)
C
C************************************************************************
      INTEGER FUNCTION LM_GEOM_ASGELEV(SELF)

      USE LM_GEOM_M
      USE LM_GDTA_M
      USE GR_GRID_M
      USE GR_ELEM_M
      USE SO_ALLC_M
      IMPLICIT NONE

      CLASS (LM_GEOM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE(SO_ALLC_KPTR2_T) :: PKNGV
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (GR_GRID_T), POINTER :: OGRID
      TYPE (GR_ELEM_T), POINTER :: OELEV
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA  => SELF%GDTA
      OGRID => SELF%OGRID
      OELEV => OGRID%OELEV

C---     Les pointeurs aux tables
      PKNGV = OELEV%REQPELE()

C---     Assigne
      GDTA%NELLV =  OGRID%REQNELLV()
      GDTA%KNGV  => PKNGV%KPTR
D     CALL ERR_ASR(GDTA%NCELV .EQ. SIZE(GDTA%KNGV, 1))
D     CALL ERR_ASR(GDTA%NELLV .EQ. SIZE(GDTA%KNGV, 2))

      GDTA%NELCOL = OELEV%REQNELCOL()
      IERR = OELEV%REQKELCOL(GDTA%KELCOL)

      LM_GEOM_ASGELEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction publique LM_GEOM_CP2GDTA est le point d'entrée pour
C     assigner à l'élément les tables et dimensions qui sont maintenues
C     par GR_GRID et consorts. Les informations sont transférées dans
C     la structure GDTA
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_GEOM_CP2GDTA(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GEOM_CP2GDTA
CDEC$ ENDIF

      !USE LM_GEOM_2GDTA_M
      !USE LM_GEOM_M
      !USE LM_GDTA_M
      !USE GR_GRID_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T

      CLASS (LM_GEOM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE(SO_ALLC_VPTR2_T) :: PDJV
      TYPE(SO_ALLC_VPTR2_T) :: PDJS
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (GR_GRID_T), POINTER :: OGRID
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA  => SELF%GDTA
      OGRID => SELF%OGRID

C---     Appelle les composantes
      IERR = LM_GEOM_ASGELEV(SELF)
      IERR = LM_GEOM_ASGELES(SELF)
      IERR = LM_GEOM_ASGCOOR(SELF)
      IERR = LM_GEOM_ASGCLIM(SELF)

C---     Les tables
      PDJV = OGRID%PDJV
      PDJS = OGRID%PDJS

C---     Assigne
      GDTA%VDJV => PDJV%VPTR
      GDTA%VDJS => PDJS%VPTR

      LM_GEOM_CP2GDTA = ERR_TYP()
      RETURN
      END FUNCTION LM_GEOM_CP2GDTA

      END SUBMODULE LM_GEOM_2GDTA_M
