C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Interface:
C   H2D2 Module: GR
C      H2D2 Class: GR_ELEM
C         FTN (Sub)Module: GR_ELEM_M
C            Public:
C            Private:
C               SUBROUTINE GR_ELEM_CTR
C               INTEGER GR_ELEM_DTR
C               INTEGER GR_ELEM_CLRXEQ
C            
C            FTN Type: GR_ELEM_T
C               Public:
C                  INTEGER GR_ELEM_PKL
C                  INTEGER GR_ELEM_UPK
C                  INTEGER GR_ELEM_INIVOL
C                  INTEGER GR_ELEM_INISRF
C                  INTEGER GR_ELEM_RST
C                  INTEGER GR_ELEM_PASDEB
C                  SUBROUTINE GR_ELEM_REQPELE
C                  INTEGER GR_ELEM_REQITPE
C                  INTEGER GR_ELEM_REQHCONF
C                  INTEGER GR_ELEM_REQHDIST
C                  INTEGER GR_ELEM_REQNNEL
C                  INTEGER GR_ELEM_REQNCEL
C                  INTEGER GR_ELEM_REQNELT
C                  INTEGER GR_ELEM_REQNELL
C                  INTEGER GR_ELEM_REQNELCOL
C                  INTEGER GR_ELEM_REQKELCOL
C               Private:
C                  INTEGER GR_ELEM_RAZ
C                  INTEGER GR_ELEM_LIS
C                  INTEGER GR_ELEM_DSTRB
C                  INTEGER GR_ELEM_COLOR
C                  INTEGER GR_ELEM_FLTRSRF
C
C************************************************************************

      MODULE GR_ELEM_M

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR2_T
      IMPLICIT NONE
      PRIVATE

      INTEGER, PARAMETER :: GR_ELEM_NCOLMAX = 32

      INTEGER, PARAMETER :: GR_ELEM_TYP_INDEFINI = 0
      INTEGER, PARAMETER :: GR_ELEM_TYP_VOLUME   = 1
      INTEGER, PARAMETER :: GR_ELEM_TYP_SPLIT    = 2
      INTEGER, PARAMETER :: GR_ELEM_TYP_SURFACE  = 3
      INTEGER, PARAMETER :: GR_ELEM_TYP_EXTERNE  = 4
      INTEGER, PARAMETER :: GR_ELEM_TYP_TAG_LAST = 5

C========================================================================
C========================================================================
C---     Type de donnée associé à la classe
      TYPE, PUBLIC :: GR_ELEM_T
         REAL*8,  PRIVATE :: TEMPS
         INTEGER, PRIVATE :: ITYP
         INTEGER, PRIVATE :: HCONF
         INTEGER, PRIVATE :: HDIST
         INTEGER, PRIVATE :: HELEM
         INTEGER, PRIVATE :: NELCOL
         TYPE(SO_ALLC_KPTR2_T), PRIVATE :: PELEM
         TYPE(SO_ALLC_KPTR2_T), PRIVATE :: PELCOL
         CLASS(GR_ELEM_T), POINTER, PRIVATE :: OPRNT
      CONTAINS
         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INIVOL    => GR_ELEM_INIVOL
         PROCEDURE, PUBLIC :: INISRF    => GR_ELEM_INISRF
         PROCEDURE, PUBLIC :: RST       => GR_ELEM_RST
         PROCEDURE, PUBLIC :: PKL       => GR_ELEM_PKL
         PROCEDURE, PUBLIC :: UPK       => GR_ELEM_UPK

         PROCEDURE, PUBLIC :: PASDEB    => GR_ELEM_PASDEB
         !PROCEDURE, PUBLIC :: PASFIN    => GR_ELEM_PASFIN

         ! ---  Getter
         PROCEDURE, PUBLIC :: REQHCONF  => GR_ELEM_REQHCONF
         PROCEDURE, PUBLIC :: REQHDIST  => GR_ELEM_REQHDIST
         PROCEDURE, PUBLIC :: REQPELE   => GR_ELEM_REQPELE
         PROCEDURE, PUBLIC :: REQITPE   => GR_ELEM_REQITPE
         PROCEDURE, PUBLIC :: REQNELT   => GR_ELEM_REQNELT
         PROCEDURE, PUBLIC :: REQNELL   => GR_ELEM_REQNELL
         PROCEDURE, PUBLIC :: REQNNEL   => GR_ELEM_REQNNEL
         PROCEDURE, PUBLIC :: REQNCEL   => GR_ELEM_REQNCEL
         PROCEDURE, PUBLIC :: REQNELCOL => GR_ELEM_REQNELCOL
         PROCEDURE, PUBLIC :: REQKELCOL => GR_ELEM_REQKELCOL

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: COLOR    => GR_ELEM_COLOR
         PROCEDURE, PRIVATE :: DSTRB    => GR_ELEM_DSTRB
         PROCEDURE, PRIVATE :: FLTRSRF  => GR_ELEM_FLTRSRF
         PROCEDURE, PRIVATE :: LIS      => GR_ELEM_LIS
         !PROCEDURE, PRIVATE :: DIMTBL   => GR_ELEM_DIMTBL
         PROCEDURE, PRIVATE :: RAZ       => GR_ELEM_RAZ
      END TYPE GR_ELEM_T

      PUBLIC :: GR_ELEM_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE GR_ELEM_DTR
      END INTERFACE DEL

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_PKL(SELF, HXML)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF
      INTEGER HXML

      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'

      INTEGER IERR
      INTEGER IOB
      INTEGER N, L
C------------------------------------------------------------------------

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, SELF%HELEM)
!      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, SELF%HEPAR)
!      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, GR_ELEM_NELT (IOB))
!      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, GR_ELEM_NELL (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WD_1(HXML, SELF%TEMPS)

!C---     La table
!      IF (ERR_GOOD()) THEN
!         N = GR_ELEM_REQNNEL(SELF) * GR_ELEM_NELL(IOB)
!         L = SELF%LELEM
!         IERR = IO_XML_WI_V(HXML, N, N, L, 1)
!      ENDIF

      GR_ELEM_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) Pas d'assertion: On ne peut pas faire d'appels de fonctions
C     tant que tous les objets n'ont pas été reconstruits.
C************************************************************************
      INTEGER FUNCTION GR_ELEM_UPK(SELF, LNEW, HXML)

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------

C---     Reset l'objet
      IF (ERR_GOOD()) IERR = SELF%RST()

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, SELF%HELEM)
!      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, SELF%HEPAR)
!      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, GR_ELEM_NELT (IOB))
!      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, GR_ELEM_NELL (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RD_1(HXML, SELF%TEMPS)

!C---     La table
!      L = 0
!      IF (ERR_GOOD()) IERR = IO_XML_RI_V(HXML, NX, N, L, 1)
!      SELF%LELEM = L  ! c.f. note

      GR_ELEM_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_ELEM_CTR() RESULT(SELF)

      IMPLICIT NONE

      INCLUDE 'err.fi'

      TYPE(GR_ELEM_T), POINTER :: SELF
      INTEGER IERR, IRET
C------------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise
      IERR = SELF%RAZ()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_DTR(SELF)

      CLASS(GR_ELEM_T), INTENT(INOUT), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

      IF (.NOT. ASSOCIATED(SELF)) GOTO 9999

      IERR = SELF%RST()

      DEALLOCATE(SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900
      SELF => NULL()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_DEALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GR_ELEM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: GR_ELEM_INIVOL
C
C Description:
C     La fonction GR_ELEM_INIVOL initialise l'objet à partir de connectivités
C     de volume.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HLMGO    Handle sur l'élément géométrique
C     HELEM    Handle sur le DT_ELEM, données des connectivités
C     HCONF    Handle sur le configuration (distribution du maillage)
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_INIVOL(SELF, GDTA, HELEM, HCONF)

      USE LM_GDTA_M, ONLY : LM_GDTA_T
      USE SO_ALLC_M, ONLY : SO_ALLC_KPTR2

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF
      CLASS(LM_GDTA_T), INTENT(IN)    :: GDTA
      INTEGER HELEM
      INTEGER HCONF

      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'gpconf.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (.NOT. DT_ELEM_HVALIDE(HELEM)) GOTO 9900
      IF (.NOT. (HCONF .EQ. 0 .OR. GP_CONF_HVALIDE(HCONF))) GOTO 9901

C---     Reset les données
      IERR = GR_ELEM_RST(SELF)

C---     Contrôles de validité
      IF (GDTA%ITPEV .NE. DT_ELEM_REQITPE(HELEM)) GOTO 9902
      IF (GDTA%NCELV .NE. DT_ELEM_REQNCEL(HELEM)) GOTO 9902

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%TEMPS  = -1.0D0
         SELF%ITYP   =  GR_ELEM_TYP_VOLUME
         SELF%HCONF  =  HCONF
         SELF%HDIST  =  0
         SELF%HELEM  =  HELEM
         SELF%NELCOL =  0
         SELF%PELEM  =  SO_ALLC_KPTR2()
         SELF%PELCOL =  SO_ALLC_KPTR2()
         SELF%OPRNT  =>  NULL()
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_CONFIGURATION_GPU_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_MAILLAGE_INCOHERENT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GR_ELEM_INIVOL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise comme surface
C
C Description:
C     La fonction GR_ELEM_INISRF initialise l'objet comme maillage
C     de surface dépendant d'un parent.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     OLMGO    Objet élément géométrique
C     HEPAR    Handle sur le parent
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_INISRF(SELF, GDTA, OPRNT)

      USE LM_GDTA_M, ONLY : LM_GDTA_T
      USE SO_ALLC_M, ONLY : SO_ALLC_KPTR2

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF
      CLASS(LM_GDTA_T), INTENT(IN)    :: GDTA
      CLASS(GR_ELEM_T), INTENT(IN), TARGET  :: OPRNT
      INTEGER HLMGO

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER HELEM
      INTEGER ITPES, NCELS, NNELS
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset les données
      IERR = SELF%RST()

C---     Récupère les dimensions
      ITPES = GDTA%ITPES
      NNELS = GDTA%NNELS
      NCELS = GDTA%NCELS

C---     Crée
      IERR = DT_ELEM_CTR   (HELEM)
      IERR = DT_ELEM_INIDIM(HELEM, ITPES, NCELS, NNELS, 1, 1)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%TEMPS  = -1.0D0
         SELF%ITYP   = GR_ELEM_TYP_SURFACE
         SELF%HCONF  = 0
         SELF%HDIST  = 0
         SELF%HELEM  = HELEM
         SELF%OPRNT  => OPRNT
         SELF%NELCOL = 0
         SELF%PELEM  = SO_ALLC_KPTR2()
         SELF%PELCOL = SO_ALLC_KPTR2()
      ENDIF

      GR_ELEM_INISRF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_RAZ(SELF)

      USE SO_ALLC_M, ONLY : SO_ALLC_KPTR2

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset les paramètres
      SELF%TEMPS  = -1.0D0
      SELF%ITYP   =  GR_ELEM_TYP_INDEFINI
      SELF%HCONF  =  0
      SELF%HDIST  =  0
      SELF%HELEM  =  0
      SELF%OPRNT  => NULL()
      SELF%NELCOL =  0
      SELF%PELEM  =  SO_ALLC_KPTR2()
      SELF%PELCOL =  SO_ALLC_KPTR2()

      GR_ELEM_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La mémoire est automatiquement désallouée dans RAZ
C************************************************************************
      INTEGER FUNCTION GR_ELEM_RST(SELF)

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'gpdist.fi'

      INTEGER IERR
      INTEGER HFELE, HFRGO, HDIST
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Détruis le fichier
      HFELE = SELF%HELEM
      IF (FD_ELEM_HVALIDE(HFELE)) THEN
         IERR = FD_ELEM_DTR(HFELE)
      ENDIF

C---     Détruis la distribution
      HDIST = SELF%HDIST
      IF (GP_DIST_HVALIDE(HDIST)) THEN
         IERR = GP_DIST_DTR(HDIST)
      ENDIF

C---     Reset les paramètres
      IERR = SELF%RAZ()

      GR_ELEM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: GR_ELEM_CHARGE
C
C Description:
C     La fonction GR_ELEM_CHARGE charge les éléments soit à partir du
C     fichier, soit à partir du parent avec une opération de split.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le temps n'est pas supporté pour les éléments
C     A la lecture on doit avoir NNEL=NCEL
C     La fonction n'est pas récursive, et on ne peut pas avoir de
C     parent de parent (grand-parents)
C     Si les renumérotations changent, il faudrait forcer la relecture
C************************************************************************
      INTEGER FUNCTION GR_ELEM_PASDEB(SELF, HNUMC, HNUME, TNEW, MODIF)

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF
      INTEGER HNUMC, HNUME
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'err.fi'
      INCLUDE 'gpconf.fi'

      INTEGER IERR
      INTEGER ITYPE
      INTEGER HEPAR, HCONF
      LOGICAL DODIST
      CLASS(GR_ELEM_T), POINTER :: OPRNT
C-----------------------------------------------------------------------
      LOGICAL EG
      EG() = ERR_GOOD()
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid')

C---     Récupère les attributs
      ITYPE = SELF%ITYP
      OPRNT =>SELF%OPRNT
      HCONF = SELF%HCONF

C---     Lis et traite
      DODIST = .FALSE.
      SELECT CASE(ITYPE)
         CASE (GR_ELEM_TYP_VOLUME)
            IF (EG()) IERR = SELF%LIS (HNUMC, HNUME, TNEW, MODIF)
            IF (EG() .AND. MODIF) DODIST = .TRUE.
!!!         CASE (GR_ELEM_TYP_SPLIT)
!!!            IF (EG()) IERR = OPRNT%LIS(HNUMC, HNUME, TNEW, MODIF)
!!!            IF (EG()) IERR = SELF%SPLIT(MODIF)
!!!            IF (EG() .AND. MODIF) DODIST = .TRUE.
         CASE (GR_ELEM_TYP_SURFACE)
            IF (EG()) IERR = SELF%FLTRSRF(MODIF)
!!!         CASE (GR_ELEM_TYP_EXTERNE)
!!!            IF (EG()) IERR = GR_ELEM_DIMTBL(SELF)
D        CASE DEFAULT
D           CALL ERR_ASR(.FALSE.)
      END SELECT

C---     Distribue
      IF (ERR_GOOD() .AND. DODIST) THEN
         IF (GP_CONF_HVALIDE(HCONF)) THEN
            IERR = GR_ELEM_DSTRB(SELF)
         ELSE
            IERR = GR_ELEM_COLOR(SELF)
         ENDIF
      ENDIF

C---     Met à jour les attributs
      IF (ERR_GOOD()) THEN
         SELF%TEMPS = TNEW
      ENDIF

      CALL TR_CHRN_STOP('h2d2.grid')    ! STOPPE LE CHRONO
      GR_ELEM_PASDEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: GR_ELEM_LIS
C
C Description:
C     La fonction privée GR_ELEM_LIS lis les données du fichier
C     d'éléments. L'objet ne doit donc pas avoir de parent et
C     posséder un fichier.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_LIS(SELF, HNUMC, HNUME, TNEW, MODIF)

      USE SO_ALLC_M, ONLY : SO_ALLC_KPTR2

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF
      INTEGER HNUMC, HNUME
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER IERR
      INTEGER HELEM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMC))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUME))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid')

C---     Récupère les attributs
      HELEM = SELF%HELEM
D     CALL ERR_ASR(.NOT. ASSOCIATED(SELF%OPRNT))

C---     Lis les données
      IF (ERR_GOOD()) THEN
         IERR = DT_ELEM_CHARGE(HELEM, HNUMC, HNUME, TNEW, MODIF)
      ENDIF

C---     Met à jour les attributs
      IF (ERR_GOOD()) THEN
         IF (MODIF) THEN
            SELF%NELCOL = 1                     ! Tous les elem
            SELF%PELCOL = SO_ALLC_KPTR2()   ! dans la même couleur
         ENDIF
      ENDIF

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.grid')

      GR_ELEM_LIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: GR_ELEM_DSTRB
C
C Description:
C     La méthode privée GR_ELEM_DSTRB distribue les noeuds sur la
C     configuration GPU.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_DSTRB (SELF)

      USE SO_ALLC_M, ONLY : SO_ALLC_KPTR2_T

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpconf.fi'
      INCLUDE 'gpdist.fi'

      INTEGER IERR
      INTEGER HCONF, HDIST
      INTEGER NNL, NELL, NNEL
      INTEGER NBLCS
      INTEGER, POINTER :: KNG(:,:)
      TYPE(SO_ALLC_KPTR2_T) :: PELEM
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.color')

C---     Récupère les attributs
      HCONF = SELF%HCONF
      HDIST = SELF%HDIST
D     CALL ERR_ASR(GP_CONF_HVALIDE(HCONF))

C---     Récupère les données
      PELEM = SELF%REQPELE()
      KNG => PELEM%KPTR

C---     Trouve le nombre de noeuds locaux
      IF (ERR_GOOD()) NNL = MAXVAL(KNG)

C---     Construis la distribution
      IF (.NOT. GP_DIST_HVALIDE(HDIST)) THEN
         IERR = GP_DIST_CTR(HDIST)
         SELF%HDIST = HDIST
      ENDIF

C---     Distribue
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GP_DIST_HVALIDE(HDIST))
         NBLCS = GP_CONF_REQNTSK(HCONF)  ! le nombre de tâches (gpu work item)
         IERR = GP_DIST_INI(HDIST,
     &                      NNL,
     &                      NNEL,
     &                      NELL,
     &                      KNG,
     &                      NBLCS)
      ENDIF

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.grid.color')

      GR_ELEM_DSTRB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: GR_ELEM_COLOR
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le coloriage pourrait être déplacé ailleurs et utiliser
C     une numérotation (un coloriage est en fait une numérotation
C     avec les couleurs pour sous-domaines)
C************************************************************************
      INTEGER FUNCTION GR_ELEM_COLOR (SELF)

      USE SO_ALLC_M, ONLY : SO_ALLC_KPTR2

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER KELCOL(2, GR_ELEM_NCOLMAX)
      INTEGER NELCOL
      INTEGER NELL
      INTEGER NNEL
      INTEGER NUM_THREADS
!$    INTEGER OMP_GET_MAX_THREADS
      LOGICAL DOCOLOR
      INTEGER, POINTER :: KNG(:,:)
      TYPE(SO_ALLC_KPTR2_T) :: PCLR
      TYPE(SO_ALLC_KPTR2_T) :: PELEM
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.color')

C---     Récupère les attributs
      PELEM  = SELF%REQPELE()
      NELL   = SELF%REQNELL()
      NNEL   = SELF%REQNNEL()
      NELCOL = SELF%NELCOL
      KNG => PELEM%KPTR

C---     Colorie les éléments
      NUM_THREADS = -1
!$    NUM_THREADS = OMP_GET_MAX_THREADS()
      DOCOLOR = (NUM_THREADS .GT. 1)
      IF (ERR_GOOD() .AND. DOCOLOR) THEN
         IERR = GR_ELEM_CLRXEQ(NNEL,
     &                         NELL,
     &                         KNG,
     &                         NELCOL,
     &                         KELCOL)

         IF (ERR_GOOD()) THEN
            PCLR = SO_ALLC_KPTR2(2, NELCOL)
            PCLR%KPTR = KELCOL(1:2, 1:NELCOL)
            SELF%NELCOL = NELCOL
            SELF%PELCOL = PCLR
         ENDIF
      ELSE
         SELF%NELCOL = 1
         SELF%PELCOL = SO_ALLC_KPTR2()
      ENDIF

C---     Imprime les éléments
      IF (ERR_GOOD() .AND. LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
         WRITE(LOG_BUF,'(A)') 'MSG_ELEMENTS_COLORIES:'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
         IERR = DT_ELEM_PRN(SELF%HELEM)
         CALL LOG_INCIND()
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_ELEM_LOCAL_NUL'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.color')    ! STOPPE LE CHRONO
      GR_ELEM_COLOR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: GR_ELEM_FLTRSRF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_FLTRSRF(SELF, MODIF)

      USE SO_ALLC_M, ONLY : SO_ALLC_KPTR2

      CLASS(GR_ELEM_T), INTENT(INOUT) :: SELF
      LOGICAL MODIF

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sppeau.fi'

      INTEGER I_ERROR

      INTEGER IERR
      INTEGER HELEM
      INTEGER ITPEV, ITPES
      !!INTEGER LELV, LELS
      INTEGER NELV, NELS, NELT
      INTEGER NCELV,NCELS
      INTEGER NNELV,NNELS
      TYPE(SO_ALLC_KPTR2_T) :: PELS
      TYPE(SO_ALLC_KPTR2_T) :: PELV
      CLASS(GR_ELEM_T), POINTER :: OPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ASSOCIATED(SELF%OPRNT))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      MODIF = .FALSE.

C---     DEMARRE LE CHRONO
      CALL TR_CHRN_START('h2d2.grid')

C---     Récupère les attributs
      HELEM = SELF%HELEM
      PELS  = SELF%REQPELE()
      ITPES = SELF%REQITPE()
      NNELS = SELF%REQNNEL()
      NCELS = SELF%REQNCEL()
D     CALL ERR_ASR(DT_ELEM_HVALIDE(HELEM))

C---     Récupère les données du parent
      OPRNT => SELF%OPRNT
      PELV  = OPRNT%REQPELE()
      NELV  = OPRNT%REQNELL()
      ITPEV = OPRNT%REQITPE()
      NNELV = OPRNT%REQNNEL()
      NCELV = OPRNT%REQNCEL()
D     CALL ERR_ASR(NELV .GT. 0)
D     CALL ERR_ASR(PELV%ISVALID())

C---     Test si déjà dimensionné
      IF (ERR_BAD()) GOTO 9999
      IF (PELS%ISVALID()) GOTO 9999

C---     Alloue l'espace
      NELS = MAX(10, NELV)         ! Heuristique
      IF (ERR_GOOD()) PELS = SO_ALLC_KPTR2(NCELS, NELS)

C---     Filtre la peau
      IF (ERR_GOOD()) THEN
         IERR = SP_PEAU_FILTRE(ITPEV,
     &                         NNELV,
     &                         NCELV,
     &                         NELV,
     &                         PELV%KPTR,
     &                         ITPES,
     &                         NNELS,
     &                         NCELS,
     &                         NELS,
     &                         PELS%KPTR)
      ENDIF

C---     Ré-alloue l'espace
      IF (ERR_GOOD()) PELS = SO_ALLC_KPTR2(NCELS, NELS)

C---     Contrôle les éléments
      IF (ERR_GOOD()) THEN
         IERR = SP_ELEM_CTRCON(NNELS,
     &                         NCELS,
     &                         NELS,
     &                         PELS%KPTR,
     &                        .FALSE.)  ! Not a main grid
      ENDIF

C---     Imprime les éléments
      IF (ERR_GOOD() .AND. LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
         WRITE(LOG_BUF,'(A)') 'MSG_ELEMENTS_PEAU:'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
         IERR = DT_ELEM_PRN(HELEM)
         CALL LOG_INCIND()
      ENDIF

C---     Réduction pour le nombre total d'éléments
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(NELS, NELT, 1, MP_TYPE_INT(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     Redimensionne et met à jour les données sous-jacentes
      IF (ERR_GOOD()) THEN
         IERR = DT_ELEM_INIDIM(HELEM, ITPES, NCELS, NNELS, NELS, NELS)
         IERR = DT_ELEM_MAJVAL(HELEM,
     &                         NCELS,
     &                         NELS,
     &                         PELS%KPTR,
     &                         0.0D0) ! TEMPS)
      ENDIF

C---     Met à jour les attributs
      IF (ERR_GOOD()) THEN
         MODIF = .TRUE.
         SELF%PELEM  = PELS
         SELF%NELCOL = 1
         SELF%PELCOL = SO_ALLC_KPTR2()
      ENDIF

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid')
      GR_ELEM_FLTRSRF = ERR_TYP()
      RETURN
      END


C************************************************************************
C Sommaire: GR_ELEM_CLRXEQ
C
C Description:
C     La fonction privée GR_ELEM_CLRXEQ colorie le maillage
C
C Entrée:
C     NNEL        le nombre de noeuds par élément
C     NELL        le nombre d'éléments locaux
C     KNG         les connectivités
C
C Sortie:
C     KNG         les éléments sont réordonnés
C     NELCOL      le nombre de couleurs
C     KELCOL      le tableau d'indices de couleur
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_CLRXEQ(NNEL,
     &                                NELL,
     &                                KNG,
     &                                NELCOL,
     &                                KELCOL)

      IMPLICIT NONE

      INTEGER NNEL
      INTEGER NELL
      INTEGER, INTENT(IN) :: KNG(:,:)
      INTEGER NELCOL
      INTEGER KELCOL(2, GR_ELEM_NCOLMAX)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'

      INTEGER IERR
      INTEGER L_ELC, L_TRV, L_NG
      INTEGER NNL
      INTEGER IE, JE, IN, NTMP

      INTEGER IIAMAX
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Trouve le nombre de noeuds locaux
      IF (ERR_GOOD()) THEN
         NNL = MAXVAL(KNG)
      ENDIF

C---     Alloue de l'espace pour les tables de travail
      L_ELC = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NELL, L_ELC)
      L_TRV = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNL, L_TRV)


C---     Fait l'appel à la fonction de coloriage
      IF (ERR_GOOD()) THEN
         IERR = SP_ELEM_COLO(NNL,
     &                       NNEL,
     &                       NELL,
     &                       GR_ELEM_NCOLMAX,
     &                       KNG,
     &                       KA(SO_ALLC_REQKIND(KA, L_TRV)),
     &                       NELCOL,
     &                       KA(SO_ALLC_REQKIND(KA, L_ELC)),
     &                       KELCOL)
      ENDIF

C---     Réordonne les éléments sur place
      IF (ERR_GOOD()) THEN
!!!         Voire GR_ELEM_REORD
!!!         CALL GR_ELEM_REORD(NNEL, NELL, KELC, KNG)
         L_NG = 0
         IERR = SO_ALLC_ALLINT(NNEL*NELL, L_NG)
         CALL GR_ELEM_CLRRRD(NNEL, NELL, KA(SO_ALLC_REQKIND(KA, L_ELC)),
     &                       KNG, KA(SO_ALLC_REQKIND(KA, L_NG)) )
         CALL GR_ELEM_CLRCHK(NNL, NNEL, NELL,
     &                       NELCOL, KELCOL,
     &                       KNG, KA(SO_ALLC_REQKIND(KA, L_NG)) )
         IERR = SO_ALLC_ALLINT(0, L_NG)
      ENDIF

C---     Libère la mémoire
      IF (L_TRV .NE. 0) IERR = SO_ALLC_ALLINT(0, L_TRV)
      IF (L_ELC .NE. 0) IERR = SO_ALLC_ALLINT(0, L_ELC)

      GR_ELEM_CLRXEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: GR_ELEM_CLRRRD
C
C Description:
C     La fonction privée GR_ELEM_CLRRRD réordonne une table avec une
C     permutation.
C     Du fichier original:
C     <par>
C        REORDI uses a cyclical algorithm to re-order the elements of
C        the array in place. After re-ordering, element IORDER(1) of
C        the input array is the first element of the output array,
C        element IORDER(2) is the input array is the second element of
C        the output array, and so on.
C     </par>
C
C Entrée:
C     NNEL        le nombre de noeuds par élément
C     NELT        le nombre d'éléments locaux
C     KELC
C     KNG         les connectivités
C
C Sortie:
C     KNG         les connectivités
C
C Notes:
C     Outrageusement cannibalisée de:
C  http://pdssbn.astro.umd.edu/volume/phb_1001/SOFTWARE/NAIFTOOL/SPICELIB/SOURCE/REORDI.FOR
C
C     Pour le moment le réordonne sur place ne donne pas le bon résultat.
C     On doit tourner dans le mauvais sens.
C************************************************************************
      SUBROUTINE GR_ELEM_CLRRRD(NNEL, NELT, KELC, KNG, KCHK)

      INTEGER NNEL
      INTEGER NELT
      INTEGER KELC (NELT)
      INTEGER KNG  (NNEL, NELT)
      INTEGER KCHK (NNEL, NELT)

      INTEGER ISTART
      INTEGER IE1
      INTEGER IE2
      integer in, ie

      INTEGER NDLMAX
      PARAMETER (NDLMAX = 24)
      INTEGER TEMP(NDLMAX)
C-----------------------------------------------------------------------

      do ie=1,nelt
         ie1 = ie
         ie2 = kelc(ie1)
         call icopy(nnel, kng(1,ie1), 1, kchk(1,ie2), 1)
      enddo

!!!      ISTART = 1                                            ! Cycle begin
!!!100   IF (ISTART .LT. NELT) THEN
!!!         write (6,*) 'Cycle:', istart
!!!         IE1 = ISTART
!!!         IE2 = KELC(IE1)
!!!         CALL ERR_ASR(IE1 .GT. 0 .AND. IE1 .LE. NELT)
!!!         CALL ERR_ASR(IE2 .GT. 0 .AND. IE2 .LE. NELT)
!!!         CALL ICOPY(NNEL, KNG(1,IE1), 1, TEMP, 1)
!!!
!!!110      IF (IE2 .NE. ISTART) THEN
!!!            write (6,*) '   swap:', ie2, ie1
!!!            CALL ICOPY(NNEL, KNG(1,IE2), 1, KNG(1,IE1), 1)  ! Move
!!!            IE1       =  IE2                                ! Advance in cycle
!!!            IE2       =  KELC(IE1)
!!!            CALL ERR_ASR(IE1 .GT. 0 .AND. IE1 .LE. NELT)
!!!            CALL ERR_ASR(IE2 .GT. 0 .AND. IE2 .LE. NELT)
!!!            KELC(IE1) = -KELC(IE1)                          ! Mark as done
!!!            GO TO 110
!!!         END IF
!!!
!!!         write (6,*) '   end:', ie2, istart
!!!         CALL ICOPY(NNEL, TEMP, 1, KNG(1,IE2), 1)           ! End of cycle
!!!         KELC(IE2) = -KELC(IE2)
!!!
!!!120      IF (KELC(ISTART) .LT. 0 .AND. ISTART .LT. NELT) THEN
!!!            ISTART = ISTART + 1
!!!            GO TO 120
!!!         END IF
!!!
!!!         GO TO 100
!!!      END IF
!!!
!!!      DO IE=1,NELT
!!!         CALL ERR_ASR(KELC(IE) .LT. 0)
!!!         KELC(IE) = IABS(KELC(IE))
!!!      ENDDO
!!!
!!!      do ie=1,nelt
!!!         do in=1,nnel
!!!            if (kng(in, ie) .ne. kchk(in,ie)) then
!!!               write(6,*) '--> ',ie, in
!!!            endif
!!!         enddo
!!!      enddo

      call icopy(nnel*nelt, kchk, 1, kng, 1)

      RETURN
      END

C************************************************************************
C Sommaire: GR_ELEM_CLRCHK
C
C Description:
C     La fonction GR_ELEM_CLRCHK contrôle le coloriage en s'assurant que
C     pour chaque couleur un numéro de noeud n'apparaisse qu'une seul
C     fois.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE GR_ELEM_CLRCHK(NNL, NNEL, NELT,
     &                          NELCOL, KELCOL,
     &                          KNG, KCHK)

      IMPLICIT NONE

      INTEGER NNL
      INTEGER NNEL
      INTEGER NELT
      INTEGER NELCOL
      INTEGER KELCOL(2, NELCOL)
      INTEGER KNG   (NNEL, NELT)
      INTEGER KCHK  (NNL)

      INCLUDE 'err.fi'

      INTEGER IC, IE, ID, IN
C-----------------------------------------------------------------------

      DO IC=1, NELCOL
         CALL IINIT(NNL, 0, KCHK, 1)
         DO IE=KELCOL(1,IC), KELCOL(2,IC)
            DO ID=1,NNEL
               IN = KNG(ID,IE)
               CALL ERR_ASR(KCHK(IN) .EQ. 0)
               KCHK(IN) = 1
            ENDDO
         ENDDO
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_ELEM_REQPELE(SELF) RESULT(PTR)

      USE SO_ALLC_M, ONLY : SO_ALLC_KPTR2_T, SO_ALLC_KPTR2

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF
      TYPE(SO_ALLC_KPTR2_T) :: PTR

      INCLUDE 'dtelem.fi'

      INTEGER HELEM
      INTEGER LELE
      INTEGER NCEL
      INTEGER NELL
C-----------------------------------------------------------------------

      NCEL = SELF%REQNCEL()
      NCEL = SELF%REQNCEL()
      HELEM = SELF%HELEM
D     CALL ERR_ASR(DT_ELEM_HVALIDE(HELEM))
      LELE = DT_ELEM_REQLELE(HELEM)
      PTR = SO_ALLC_KPTR2(LELE, NCEL, NELL)

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_REQITPE(SELF)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF

      INCLUDE 'dtelem.fi'

      INTEGER HELEM
C-----------------------------------------------------------------------

      HELEM = SELF%HELEM
D     CALL ERR_ASR(DT_ELEM_HVALIDE(HELEM))
      GR_ELEM_REQITPE = DT_ELEM_REQITPE(HELEM)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_REQHCONF(SELF)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_ELEM_REQHCONF = SELF%HCONF
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_REQHDIST(SELF)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_ELEM_REQHDIST = SELF%HDIST
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_REQNNEL(SELF)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF

      INCLUDE 'dtelem.fi'

      INTEGER HELEM
C-----------------------------------------------------------------------

      HELEM = SELF%HELEM
      GR_ELEM_REQNNEL = DT_ELEM_REQNNEL(HELEM)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_REQNCEL(SELF)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF

      INCLUDE 'dtelem.fi'

      INTEGER HELEM
C-----------------------------------------------------------------------

      HELEM = SELF%HELEM
      GR_ELEM_REQNCEL = DT_ELEM_REQNCEL(HELEM)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_REQNELT(SELF)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF

      INCLUDE 'dtelem.fi'

      INTEGER HELEM
C-----------------------------------------------------------------------

      HELEM = SELF%HELEM
      GR_ELEM_REQNELT = DT_ELEM_REQNELT(HELEM)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_REQNELL(SELF)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF

      INCLUDE 'dtelem.fi'

      INTEGER HELEM
C-----------------------------------------------------------------------

      HELEM = SELF%HELEM
      GR_ELEM_REQNELL = DT_ELEM_REQNELL(HELEM)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de couleurs
C
C Description:
C     La fonction GR_ELEM_REQNELCOL retourne le nombre de couleurs du
C     maillage. Le nombre de couleurs peut être nul, par exemple si
C     le maillage n'est pas chargé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     Le nombre de couleurs.
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_REQNELCOL(SELF)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_ELEM_REQNELCOL = SELF%NELCOL
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le tableau d'indices de couleurs
C
C Description:
C     La fonction GR_ELEM_REQKELCOL retourne le tableau d'indices
C     de couleurs. La table KELCOL doit être suffisamment grande pour
C     contenir 2*NELCOL valeurs.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     KELCOL      Le tableau d'indices
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_ELEM_REQKELCOL(SELF, KELCOL)

      CLASS(GR_ELEM_T), INTENT(IN) :: SELF
      INTEGER KELCOL(2, *)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IOB
      INTEGER LCOL
      INTEGER NCOL
C-----------------------------------------------------------------------

      NCOL = SELF%NELCOL
      IF (NCOL .LE. 1) THEN
         KELCOL(1,1) = 1
         KELCOL(2,1) = SELF%REQNELL()
      ELSE
         KELCOL(1:2,1:NCOL) = SELF%PELCOL%KPTR(:,:)
      ENDIF

      GR_ELEM_REQKELCOL = ERR_TYP()
      RETURN
      END

      END MODULE GR_ELEM_M
