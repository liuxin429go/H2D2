C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_HGEO_CLCSRF
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction LM_HGEO_CLCSRF assemble la peau pour des
C     éléments de volume de type T6L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION LM_HGEO_CLCSRF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_CLCSRF
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER,INTENT(IN) :: HOBJ

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQSELF(HOBJ)
      IERR = SELF%CLCSRF()

      LM_HGEO_CLCSRF = ERR_TYP()
      RETURN
      END

