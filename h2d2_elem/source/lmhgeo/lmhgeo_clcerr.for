C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_HGEO_CLCERR
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_CLCERR(HOBJ, VFRM, VHESS, VHNRM, ITPCLC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_CLCERR
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)  :: HOBJ
      REAL*8,  INTENT(IN)  :: VFRM (:)
      REAL*8,  INTENT(OUT) :: VHESS(:,:)
      REAL*8,  INTENT(OUT) :: VHNRM(:)
      INTEGER, INTENT(IN)  :: ITPCLC

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS (LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      IERR = SELF%CLCERR(VFRM, VHESS, VHNRM, ITPCLC)

      LM_HGEO_CLCERR = ERR_TYP()
      RETURN
      END
