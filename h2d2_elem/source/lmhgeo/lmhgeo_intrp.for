C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_HGEO_INTRP
C   Private:
C     INTEGER LM_HGEO_INTRP_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION LM_HGEO_INTRP(HOBJ,
     &                       KELE,
     &                       VCORE,
     &                       INDXS,
     &                       VSRC,
     &                       INDXD,
     &                       VDST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_INTRP
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: KELE (:)
      REAL*8,  INTENT(IN) :: VCORE(:, :)
      INTEGER, INTENT(IN) :: INDXS
      REAL*8,  INTENT(IN) :: VSRC (:, :)
      INTEGER, INTENT(IN) :: INDXD
      REAL*8,  INTENT(OUT):: VDST (:, :)

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      IERR = SELF%INTRP(KELE, VCORE, INDXS, VSRC, INDXD, VDST)

      LM_HGEO_INTRP = ERR_TYP()
      RETURN
      END
