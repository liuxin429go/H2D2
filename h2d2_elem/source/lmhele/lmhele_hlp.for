C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_HLP
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire: Aide.
C
C Description:
C     La fonction LM_ELEM_HLP imprime (print) les informations sur
C     toutes les données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_HLP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhele.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Impression de l'aide
      IF (ERR_GOOD()) IERR = LM_HELE_HLPPRGL(HOBJ)
      IF (ERR_GOOD()) IERR = LM_HELE_HLPPRNO(HOBJ)
      IF (ERR_GOOD()) IERR = LM_HELE_HLPCLIM(HOBJ)

      LM_HELE_HLP = ERR_TYP()
      RETURN
      END FUNCTION LM_HELE_HLP
