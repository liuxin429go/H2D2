C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_HLPCLIM
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire: Aide sur les conditions limites
C
C Description:
C     La fonction LM_HELE_HLPCLIM imprime l'aide sur les conditions limites
C     de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION LM_HELE_HLPCLIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_HLPCLIM
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhele.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'lmhele.fc'

      INTEGER IERR
      INTEGER HMDL, HFNC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      HMDL = 0
      HFNC = 0
      
C---     Le module
      IERR = SO_UTIL_REQHMDLHBASE(HOBJ, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     La fonction
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HLPCLIM', HFNC)
      
C---     L'appel
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOBJ))

      LM_HELE_HLPCLIM = ERR_TYP()
      RETURN
      END
