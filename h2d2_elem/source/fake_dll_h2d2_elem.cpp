//************************************************************************
// H2D2 - External declaration of public symbols
// Module: h2d2_elem
// Entry point: extern "C" void fake_dll_h2d2_elem()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:47:01.764124
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class GR_ELEM
F_PROT(GR_ELEM_000, gr_elem_000);
F_PROT(GR_ELEM_999, gr_elem_999);
F_PROT(GR_ELEM_PKL, gr_elem_pkl);
F_PROT(GR_ELEM_UPK, gr_elem_upk);
F_PROT(GR_ELEM_CTR, gr_elem_ctr);
F_PROT(GR_ELEM_DTR, gr_elem_dtr);
F_PROT(GR_ELEM_INIVOL, gr_elem_inivol);
F_PROT(GR_ELEM_INISRF, gr_elem_inisrf);
F_PROT(GR_ELEM_RST, gr_elem_rst);
F_PROT(GR_ELEM_REQHBASE, gr_elem_reqhbase);
F_PROT(GR_ELEM_HVALIDE, gr_elem_hvalide);
F_PROT(GR_ELEM_PASDEB, gr_elem_pasdeb);
F_PROT(GR_ELEM_FLTRSRF, gr_elem_fltrsrf);
F_PROT(GR_ELEM_SAUVE, gr_elem_sauve);
F_PROT(GR_ELEM_REQLELE, gr_elem_reqlele);
F_PROT(GR_ELEM_REQITPE, gr_elem_reqitpe);
F_PROT(GR_ELEM_REQHCONF, gr_elem_reqhconf);
F_PROT(GR_ELEM_REQHDIST, gr_elem_reqhdist);
F_PROT(GR_ELEM_REQNNEL, gr_elem_reqnnel);
F_PROT(GR_ELEM_REQNCEL, gr_elem_reqncel);
F_PROT(GR_ELEM_REQNELT, gr_elem_reqnelt);
F_PROT(GR_ELEM_REQNELL, gr_elem_reqnell);
F_PROT(GR_ELEM_REQNELCOL, gr_elem_reqnelcol);
F_PROT(GR_ELEM_REQKELCOL, gr_elem_reqkelcol);
 
// ---  class GR_GRID
F_PROT(GR_GRID_000, gr_grid_000);
F_PROT(GR_GRID_999, gr_grid_999);
F_PROT(GR_GRID_PKL, gr_grid_pkl);
F_PROT(GR_GRID_UPK, gr_grid_upk);
F_PROT(GR_GRID_CTR, gr_grid_ctr);
F_PROT(GR_GRID_DTR, gr_grid_dtr);
F_PROT(GR_GRID_INI, gr_grid_ini);
F_PROT(GR_GRID_RST, gr_grid_rst);
F_PROT(GR_GRID_REQHBASE, gr_grid_reqhbase);
F_PROT(GR_GRID_HVALIDE, gr_grid_hvalide);
F_PROT(GR_GRID_PASDEB, gr_grid_pasdeb);
F_PROT(GR_GRID_REQHGRID, gr_grid_reqhgrid);
F_PROT(GR_GRID_REQHCOOR, gr_grid_reqhcoor);
F_PROT(GR_GRID_REQHELEV, gr_grid_reqhelev);
F_PROT(GR_GRID_REQHELES, gr_grid_reqheles);
F_PROT(GR_GRID_REQHCONF, gr_grid_reqhconf);
F_PROT(GR_GRID_REQHDIST, gr_grid_reqhdist);
F_PROT(GR_GRID_REQHNUMC, gr_grid_reqhnumc);
F_PROT(GR_GRID_REQHNUME, gr_grid_reqhnume);
F_PROT(GR_GRID_REQNDIM, gr_grid_reqndim);
F_PROT(GR_GRID_REQNNL, gr_grid_reqnnl);
F_PROT(GR_GRID_REQNNP, gr_grid_reqnnp);
F_PROT(GR_GRID_REQNNT, gr_grid_reqnnt);
F_PROT(GR_GRID_REQNNELS, gr_grid_reqnnels);
F_PROT(GR_GRID_REQNNELV, gr_grid_reqnnelv);
F_PROT(GR_GRID_REQNCELS, gr_grid_reqncels);
F_PROT(GR_GRID_REQNCELV, gr_grid_reqncelv);
F_PROT(GR_GRID_REQNELLS, gr_grid_reqnells);
F_PROT(GR_GRID_REQNELLV, gr_grid_reqnellv);
F_PROT(GR_GRID_REQNELTS, gr_grid_reqnelts);
F_PROT(GR_GRID_REQNELTV, gr_grid_reqneltv);
F_PROT(GR_GRID_REQNDJV, gr_grid_reqndjv);
F_PROT(GR_GRID_REQNDJS, gr_grid_reqndjs);
F_PROT(GR_GRID_REQLDJV, gr_grid_reqldjv);
F_PROT(GR_GRID_REQLDJS, gr_grid_reqldjs);
 
// ---  class HS_CLIM
F_PROT(HS_CLIM_000, hs_clim_000);
F_PROT(HS_CLIM_999, hs_clim_999);
F_PROT(HS_CLIM_CTR, hs_clim_ctr);
F_PROT(HS_CLIM_DTR, hs_clim_dtr);
F_PROT(HS_CLIM_INI, hs_clim_ini);
F_PROT(HS_CLIM_RST, hs_clim_rst);
F_PROT(HS_CLIM_REQHBASE, hs_clim_reqhbase);
F_PROT(HS_CLIM_HVALIDE, hs_clim_hvalide);
F_PROT(HS_CLIM_PASDEB, hs_clim_pasdeb);
F_PROT(HS_CLIM_PASFIN, hs_clim_pasfin);
F_PROT(HS_CLIM_REQHLMAP, hs_clim_reqhlmap);
F_PROT(HS_CLIM_REQHGRID, hs_clim_reqhgrid);
F_PROT(HS_CLIM_REQHCLIM, hs_clim_reqhclim);
F_PROT(HS_CLIM_REQNCLCND, hs_clim_reqnclcnd);
F_PROT(HS_CLIM_REQLCLCND, hs_clim_reqlclcnd);
F_PROT(HS_CLIM_REQNCLCNV, hs_clim_reqnclcnv);
F_PROT(HS_CLIM_REQLCLCNV, hs_clim_reqlclcnv);
F_PROT(HS_CLIM_REQNCLLIM, hs_clim_reqncllim);
F_PROT(HS_CLIM_REQNCLNOD, hs_clim_reqnclnod);
F_PROT(HS_CLIM_REQNCLELE, hs_clim_reqnclele);
F_PROT(HS_CLIM_REQLCLLIM, hs_clim_reqlcllim);
F_PROT(HS_CLIM_REQLCLNOD, hs_clim_reqlclnod);
F_PROT(HS_CLIM_REQLCLELE, hs_clim_reqlclele);
F_PROT(HS_CLIM_REQLCLDST, hs_clim_reqlcldst);
F_PROT(HS_CLIM_REQLDIMP, hs_clim_reqldimp);
F_PROT(HS_CLIM_REQLDIMV, hs_clim_reqldimv);
F_PROT(HS_CLIM_REQLEIMP, hs_clim_reqleimp);
 
// ---  class HS_DLIB
F_PROT(HS_DLIB_000, hs_dlib_000);
F_PROT(HS_DLIB_999, hs_dlib_999);
F_PROT(HS_DLIB_CTR, hs_dlib_ctr);
F_PROT(HS_DLIB_DTR, hs_dlib_dtr);
F_PROT(HS_DLIB_INI, hs_dlib_ini);
F_PROT(HS_DLIB_RST, hs_dlib_rst);
F_PROT(HS_DLIB_REQHBASE, hs_dlib_reqhbase);
F_PROT(HS_DLIB_HVALIDE, hs_dlib_hvalide);
F_PROT(HS_DLIB_INIDDL, hs_dlib_iniddl);
F_PROT(HS_DLIB_INIFIC, hs_dlib_inific);
F_PROT(HS_DLIB_INIVAL, hs_dlib_inival);
F_PROT(HS_DLIB_MAJNEQ, hs_dlib_majneq);
F_PROT(HS_DLIB_PASDEB, hs_dlib_pasdeb);
F_PROT(HS_DLIB_PASFIN, hs_dlib_pasfin);
F_PROT(HS_DLIB_SAUVE, hs_dlib_sauve);
F_PROT(HS_DLIB_ESTINIT, hs_dlib_estinit);
F_PROT(HS_DLIB_PUSHLDLG, hs_dlib_pushldlg);
F_PROT(HS_DLIB_POPLDLG, hs_dlib_popldlg);
F_PROT(HS_DLIB_REQHGRID, hs_dlib_reqhgrid);
F_PROT(HS_DLIB_REQHVNOD, hs_dlib_reqhvnod);
F_PROT(HS_DLIB_REQNEQL, hs_dlib_reqneql);
F_PROT(HS_DLIB_REQNEQP, hs_dlib_reqneqp);
F_PROT(HS_DLIB_REQNNL, hs_dlib_reqnnl);
F_PROT(HS_DLIB_REQNNP, hs_dlib_reqnnp);
F_PROT(HS_DLIB_REQNNT, hs_dlib_reqnnt);
F_PROT(HS_DLIB_REQNDLN, hs_dlib_reqndln);
F_PROT(HS_DLIB_REQNDLL, hs_dlib_reqndll);
F_PROT(HS_DLIB_REQNDLP, hs_dlib_reqndlp);
F_PROT(HS_DLIB_REQNDLT, hs_dlib_reqndlt);
F_PROT(HS_DLIB_REQLDLG, hs_dlib_reqldlg);
F_PROT(HS_DLIB_REQLLOCN, hs_dlib_reqllocn);
F_PROT(HS_DLIB_REQTEMPS, hs_dlib_reqtemps);
 
// ---  class HS_PRES
F_PROT(HS_PRES_000, hs_pres_000);
F_PROT(HS_PRES_999, hs_pres_999);
F_PROT(HS_PRES_CTR, hs_pres_ctr);
F_PROT(HS_PRES_DTR, hs_pres_dtr);
F_PROT(HS_PRES_INI, hs_pres_ini);
F_PROT(HS_PRES_RST, hs_pres_rst);
F_PROT(HS_PRES_REQHBASE, hs_pres_reqhbase);
F_PROT(HS_PRES_HVALIDE, hs_pres_hvalide);
F_PROT(HS_PRES_PASDEB, hs_pres_pasdeb);
F_PROT(HS_PRES_PASFIN, hs_pres_pasfin);
F_PROT(HS_PRES_REQHLMAP, hs_pres_reqhlmap);
F_PROT(HS_PRES_REQHGRID, hs_pres_reqhgrid);
F_PROT(HS_PRES_REQNELE, hs_pres_reqnele);
F_PROT(HS_PRES_REQNPREL, hs_pres_reqnprel);
F_PROT(HS_PRES_REQNPRELL, hs_pres_reqnprell);
F_PROT(HS_PRES_REQLPREL, hs_pres_reqlprel);
 
// ---  class HS_PREV
F_PROT(HS_PREV_000, hs_prev_000);
F_PROT(HS_PREV_999, hs_prev_999);
F_PROT(HS_PREV_CTR, hs_prev_ctr);
F_PROT(HS_PREV_DTR, hs_prev_dtr);
F_PROT(HS_PREV_INI, hs_prev_ini);
F_PROT(HS_PREV_RST, hs_prev_rst);
F_PROT(HS_PREV_REQHBASE, hs_prev_reqhbase);
F_PROT(HS_PREV_HVALIDE, hs_prev_hvalide);
F_PROT(HS_PREV_PASDEB, hs_prev_pasdeb);
F_PROT(HS_PREV_PASFIN, hs_prev_pasfin);
F_PROT(HS_PREV_REQHLMAP, hs_prev_reqhlmap);
F_PROT(HS_PREV_REQHGRID, hs_prev_reqhgrid);
F_PROT(HS_PREV_REQHPREV, hs_prev_reqhprev);
F_PROT(HS_PREV_REQNELE, hs_prev_reqnele);
F_PROT(HS_PREV_REQNPREL, hs_prev_reqnprel);
F_PROT(HS_PREV_REQNPRELL, hs_prev_reqnprell);
F_PROT(HS_PREV_REQLPREL, hs_prev_reqlprel);
 
// ---  class HS_PRGL
F_PROT(HS_PRGL_000, hs_prgl_000);
F_PROT(HS_PRGL_999, hs_prgl_999);
F_PROT(HS_PRGL_CTR, hs_prgl_ctr);
F_PROT(HS_PRGL_DTR, hs_prgl_dtr);
F_PROT(HS_PRGL_INI, hs_prgl_ini);
F_PROT(HS_PRGL_RST, hs_prgl_rst);
F_PROT(HS_PRGL_REQHBASE, hs_prgl_reqhbase);
F_PROT(HS_PRGL_HVALIDE, hs_prgl_hvalide);
F_PROT(HS_PRGL_PASDEB, hs_prgl_pasdeb);
F_PROT(HS_PRGL_PASMAJ, hs_prgl_pasmaj);
F_PROT(HS_PRGL_PASFIN, hs_prgl_pasfin);
F_PROT(HS_PRGL_REQHLMAP, hs_prgl_reqhlmap);
F_PROT(HS_PRGL_REQHGRID, hs_prgl_reqhgrid);
F_PROT(HS_PRGL_REQHPRGL, hs_prgl_reqhprgl);
F_PROT(HS_PRGL_REQNPRGL, hs_prgl_reqnprgl);
F_PROT(HS_PRGL_REQNPRGLL, hs_prgl_reqnprgll);
F_PROT(HS_PRGL_REQLPRGL, hs_prgl_reqlprgl);
 
// ---  class HS_PRNO
F_PROT(HS_PRNO_000, hs_prno_000);
F_PROT(HS_PRNO_999, hs_prno_999);
F_PROT(HS_PRNO_CTR, hs_prno_ctr);
F_PROT(HS_PRNO_DTR, hs_prno_dtr);
F_PROT(HS_PRNO_INI, hs_prno_ini);
F_PROT(HS_PRNO_RST, hs_prno_rst);
F_PROT(HS_PRNO_REQHBASE, hs_prno_reqhbase);
F_PROT(HS_PRNO_HVALIDE, hs_prno_hvalide);
F_PROT(HS_PRNO_PASDEB, hs_prno_pasdeb);
F_PROT(HS_PRNO_PASFIN, hs_prno_pasfin);
F_PROT(HS_PRNO_REQHLMAP, hs_prno_reqhlmap);
F_PROT(HS_PRNO_REQHGRID, hs_prno_reqhgrid);
F_PROT(HS_PRNO_REQHPRNO, hs_prno_reqhprno);
F_PROT(HS_PRNO_REQNNL, hs_prno_reqnnl);
F_PROT(HS_PRNO_REQNPRNO, hs_prno_reqnprno);
F_PROT(HS_PRNO_REQNPRNOL, hs_prno_reqnprnol);
F_PROT(HS_PRNO_REQLPRNO, hs_prno_reqlprno);
 
// ---  class HS_SOLC
F_PROT(HS_SOLC_000, hs_solc_000);
F_PROT(HS_SOLC_999, hs_solc_999);
F_PROT(HS_SOLC_CTR, hs_solc_ctr);
F_PROT(HS_SOLC_DTR, hs_solc_dtr);
F_PROT(HS_SOLC_INI, hs_solc_ini);
F_PROT(HS_SOLC_RST, hs_solc_rst);
F_PROT(HS_SOLC_REQHBASE, hs_solc_reqhbase);
F_PROT(HS_SOLC_HVALIDE, hs_solc_hvalide);
F_PROT(HS_SOLC_PASDEB, hs_solc_pasdeb);
F_PROT(HS_SOLC_PASFIN, hs_solc_pasfin);
F_PROT(HS_SOLC_REQHLMAP, hs_solc_reqhlmap);
F_PROT(HS_SOLC_REQHGRID, hs_solc_reqhgrid);
F_PROT(HS_SOLC_REQHSOLC, hs_solc_reqhsolc);
F_PROT(HS_SOLC_REQNNL, hs_solc_reqnnl);
F_PROT(HS_SOLC_REQNSOLC, hs_solc_reqnsolc);
F_PROT(HS_SOLC_REQNSOLCL, hs_solc_reqnsolcl);
F_PROT(HS_SOLC_REQLSOLC, hs_solc_reqlsolc);
 
// ---  class HS_SOLR
F_PROT(HS_SOLR_000, hs_solr_000);
F_PROT(HS_SOLR_999, hs_solr_999);
F_PROT(HS_SOLR_CTR, hs_solr_ctr);
F_PROT(HS_SOLR_DTR, hs_solr_dtr);
F_PROT(HS_SOLR_INI, hs_solr_ini);
F_PROT(HS_SOLR_RST, hs_solr_rst);
F_PROT(HS_SOLR_REQHBASE, hs_solr_reqhbase);
F_PROT(HS_SOLR_HVALIDE, hs_solr_hvalide);
F_PROT(HS_SOLR_PASDEB, hs_solr_pasdeb);
F_PROT(HS_SOLR_PASFIN, hs_solr_pasfin);
F_PROT(HS_SOLR_REQHLMAP, hs_solr_reqhlmap);
F_PROT(HS_SOLR_REQHGRID, hs_solr_reqhgrid);
F_PROT(HS_SOLR_REQHSOLR, hs_solr_reqhsolr);
F_PROT(HS_SOLR_REQNNL, hs_solr_reqnnl);
F_PROT(HS_SOLR_REQNSOLR, hs_solr_reqnsolr);
F_PROT(HS_SOLR_REQNSOLRL, hs_solr_reqnsolrl);
F_PROT(HS_SOLR_REQLSOLR, hs_solr_reqlsolr);
 
// ---  class IC_FRML
F_PROT(IC_FRML_XEQCTR, ic_frml_xeqctr);
F_PROT(IC_FRML_XEQMTH, ic_frml_xeqmth);
F_PROT(IC_FRML_REQCLS, ic_frml_reqcls);
F_PROT(IC_FRML_REQHDL, ic_frml_reqhdl);
 
// ---  class IC_SIMD
F_PROT(IC_SIMD_AJTCTR, ic_simd_ajtctr);
F_PROT(IC_SIMD_XEQCTR, ic_simd_xeqctr);
F_PROT(IC_SIMD_XEQMTH, ic_simd_xeqmth);
F_PROT(IC_SIMD_REQCLS, ic_simd_reqcls);
F_PROT(IC_SIMD_REQHDL, ic_simd_reqhdl);
 
// ---  class IC_LM_ELEM
F_PROT(IC_LM_ELEM_XEQCTR, ic_lm_elem_xeqctr);
F_PROT(IC_LM_ELEM_XEQMTH, ic_lm_elem_xeqmth);
F_PROT(IC_LM_ELEM_OPBDOT, ic_lm_elem_opbdot);
F_PROT(IC_LM_ELEM_REQCLS, ic_lm_elem_reqcls);
F_PROT(IC_LM_ELEM_REQHDL, ic_lm_elem_reqhdl);
 
// ---  class LG_LGCY
F_PROT(LG_LGCY_000, lg_lgcy_000);
F_PROT(LG_LGCY_999, lg_lgcy_999);
F_PROT(LG_LGCY_CTR, lg_lgcy_ctr);
F_PROT(LG_LGCY_DTR, lg_lgcy_dtr);
F_PROT(LG_LGCY_INI, lg_lgcy_ini);
F_PROT(LG_LGCY_RST, lg_lgcy_rst);
F_PROT(LG_LGCY_REQHBASE, lg_lgcy_reqhbase);
F_PROT(LG_LGCY_HVALIDE, lg_lgcy_hvalide);
F_PROT(LG_LGCY_ASMESCL, lg_lgcy_asmescl);
F_PROT(LG_LGCY_CLCERR, lg_lgcy_clcerr);
F_PROT(LG_LGCY_CLCJELS, lg_lgcy_clcjels);
F_PROT(LG_LGCY_CLCJELV, lg_lgcy_clcjelv);
F_PROT(LG_LGCY_CLCSPLT, lg_lgcy_clcsplt);
F_PROT(LG_LGCY_INTRP, lg_lgcy_intrp);
F_PROT(LG_LGCY_LCLELV, lg_lgcy_lclelv);
 
// ---  class LM_ELEM
M_PROT(LM_ELEM_M, lm_elem_m, LM_ELEM_REQEDTA, lm_elem_reqedta);
M_PROT(LM_ELEM_CALLCB_M, lm_elem_callcb_m, LM_ELEM_CALLCB, lm_elem_callcb);
F_PROT(LM_ELEM_000, lm_elem_000);
F_PROT(LM_ELEM_999, lm_elem_999);
F_PROT(LM_ELEM_CTR, lm_elem_ctr);
F_PROT(LM_ELEM_DTR, lm_elem_dtr);
F_PROT(LM_ELEM_INI, lm_elem_ini);
F_PROT(LM_ELEM_RST, lm_elem_rst);
F_PROT(LM_ELEM_REQHBASE, lm_elem_reqhbase);
F_PROT(LM_ELEM_HVALIDE, lm_elem_hvalide);
F_PROT(LM_ELEM_DUMMY, lm_elem_dummy);
F_PROT(LM_ELEM_REQPRM, lm_elem_reqprm);
F_PROT(LM_ELEM_ESTINI, lm_elem_estini);
F_PROT(LM_ELEM_ESTLIS, lm_elem_estlis);
F_PROT(LM_ELEM_ESTPRC, lm_elem_estprc);
F_PROT(LM_ELEM_ESTCLC, lm_elem_estclc);
F_PROT(LM_ELEM_ESTPSC, lm_elem_estpsc);
F_PROT(LM_ELEM_PUSHLDLG, lm_elem_pushldlg);
F_PROT(LM_ELEM_POPLDLG, lm_elem_popldlg);
F_PROT(LM_ELEM_REQHASH, lm_elem_reqhash);
F_PROT(LM_ELEM_REQHKID, lm_elem_reqhkid);
F_PROT(LM_ELEM_REQHLMGO, lm_elem_reqhlmgo);
F_PROT(LM_ELEM_REQHDLIB, lm_elem_reqhdlib);
F_PROT(LM_ELEM_REQHNUMC, lm_elem_reqhnumc);
F_PROT(LM_ELEM_REQHNUME, lm_elem_reqhnume);
F_PROT(LM_ELEM_REQTEMPS, lm_elem_reqtemps);
F_PROT(LM_ELEM_DMP, lm_elem_dmp);
F_PROT(LM_ELEM_DMPPROP, lm_elem_dmpprop);
F_PROT(LM_ELEM_PRN, lm_elem_prn);
F_PROT(LM_ELEM_PRNPRGL, lm_elem_prnprgl);
F_PROT(LM_ELEM_PRNPRNO, lm_elem_prnprno);
F_PROT(LM_ELEM_CLC, lm_elem_clc);
F_PROT(LM_ELEM_CLCPRE, lm_elem_clcpre);
F_PROT(LM_ELEM_CLCPST, lm_elem_clcpst);
F_PROT(LM_ELEM_PRCCLIM, lm_elem_prcclim);
F_PROT(LM_ELEM_PRCDLIB, lm_elem_prcdlib);
F_PROT(LM_ELEM_PRCPRES, lm_elem_prcpres);
F_PROT(LM_ELEM_PRCPREV, lm_elem_prcprev);
F_PROT(LM_ELEM_PRCPRGL, lm_elem_prcprgl);
F_PROT(LM_ELEM_PRCPRLL, lm_elem_prcprll);
F_PROT(LM_ELEM_PRCPRNO, lm_elem_prcprno);
F_PROT(LM_ELEM_PRCSOLC, lm_elem_prcsolc);
F_PROT(LM_ELEM_PRCSOLR, lm_elem_prcsolr);
F_PROT(LM_ELEM_CLCCLIM, lm_elem_clcclim);
F_PROT(LM_ELEM_CLCDLIB, lm_elem_clcdlib);
F_PROT(LM_ELEM_CLCPRES, lm_elem_clcpres);
F_PROT(LM_ELEM_CLCPREV, lm_elem_clcprev);
F_PROT(LM_ELEM_CLCPRNO, lm_elem_clcprno);
F_PROT(LM_ELEM_ASMDIM, lm_elem_asmdim);
F_PROT(LM_ELEM_ASMF, lm_elem_asmf);
F_PROT(LM_ELEM_ASMFKU, lm_elem_asmfku);
F_PROT(LM_ELEM_ASMIND, lm_elem_asmind);
F_PROT(LM_ELEM_ASMK, lm_elem_asmk);
F_PROT(LM_ELEM_ASMKT, lm_elem_asmkt);
F_PROT(LM_ELEM_ASMKU, lm_elem_asmku);
F_PROT(LM_ELEM_ASMM, lm_elem_asmm);
F_PROT(LM_ELEM_ASMMU, lm_elem_asmmu);
F_PROT(LM_ELEM_PASDEB, lm_elem_pasdeb);
F_PROT(LM_ELEM_PASFIN, lm_elem_pasfin);
F_PROT(LM_ELEM_PASMAJ, lm_elem_pasmaj);
F_PROT(LM_ELEM_PSCPRNO, lm_elem_pscprno);
F_PROT(LM_ELEM_PSLCLIM, lm_elem_pslclim);
F_PROT(LM_ELEM_PSLDLIB, lm_elem_psldlib);
F_PROT(LM_ELEM_PSLPREV, lm_elem_pslprev);
F_PROT(LM_ELEM_PSLPRGL, lm_elem_pslprgl);
F_PROT(LM_ELEM_PSLPRNO, lm_elem_pslprno);
F_PROT(LM_ELEM_PSLSOLC, lm_elem_pslsolc);
F_PROT(LM_ELEM_PSLSOLR, lm_elem_pslsolr);
F_PROT(LM_ELEM_HLP, lm_elem_hlp);
F_PROT(LM_ELEM_HLPCLIM, lm_elem_hlpclim);
F_PROT(LM_ELEM_HLPPRGL, lm_elem_hlpprgl);
F_PROT(LM_ELEM_HLPPRNO, lm_elem_hlpprno);
 
// ---  class LM_FRML
F_PROT(LM_FRML_000, lm_frml_000);
F_PROT(LM_FRML_999, lm_frml_999);
F_PROT(LM_FRML_CTR, lm_frml_ctr);
F_PROT(LM_FRML_DTR, lm_frml_dtr);
F_PROT(LM_FRML_INIFRM, lm_frml_inifrm);
F_PROT(LM_FRML_RST, lm_frml_rst);
F_PROT(LM_FRML_REQHBASE, lm_frml_reqhbase);
F_PROT(LM_FRML_HVALIDE, lm_frml_hvalide);
F_PROT(LM_FRML_REQFRML, lm_frml_reqfrml);
F_PROT(LM_FRML_REQHMDL, lm_frml_reqhmdl);
F_PROT(LM_FRML_REQNDLN, lm_frml_reqndln);
F_PROT(LM_FRML_REQNDLEV, lm_frml_reqndlev);
F_PROT(LM_FRML_REQNDLES, lm_frml_reqndles);
F_PROT(LM_FRML_REQNPRGL, lm_frml_reqnprgl);
F_PROT(LM_FRML_REQNPRGLL, lm_frml_reqnprgll);
F_PROT(LM_FRML_REQNPRNO, lm_frml_reqnprno);
F_PROT(LM_FRML_REQNPRNOL, lm_frml_reqnprnol);
F_PROT(LM_FRML_REQNPREV, lm_frml_reqnprev);
F_PROT(LM_FRML_REQNPREVL, lm_frml_reqnprevl);
F_PROT(LM_FRML_REQNPRES, lm_frml_reqnpres);
F_PROT(LM_FRML_REQNSOLR, lm_frml_reqnsolr);
F_PROT(LM_FRML_REQNSOLRL, lm_frml_reqnsolrl);
F_PROT(LM_FRML_REQNSOLC, lm_frml_reqnsolc);
F_PROT(LM_FRML_REQNSOLCL, lm_frml_reqnsolcl);
F_PROT(LM_FRML_REQTGELV, lm_frml_reqtgelv);
F_PROT(LM_FRML_ESTLIN, lm_frml_estlin);
 
// ---  class LM_GEOM
M_PROT(LM_GEOM_M, lm_geom_m, LM_GEOM_REQGDTA, lm_geom_reqgdta);
M_PROT(LM_GEOM_CALLCB_M, lm_geom_callcb_m, LM_GEOM_CALLCB, lm_geom_callcb);
F_PROT(LM_GEOM_000, lm_geom_000);
F_PROT(LM_GEOM_999, lm_geom_999);
F_PROT(LM_GEOM_CTR, lm_geom_ctr);
F_PROT(LM_GEOM_DTR, lm_geom_dtr);
F_PROT(LM_GEOM_INI, lm_geom_ini);
F_PROT(LM_GEOM_RST, lm_geom_rst);
F_PROT(LM_GEOM_REQHBASE, lm_geom_reqhbase);
F_PROT(LM_GEOM_HVALIDE, lm_geom_hvalide);
F_PROT(LM_GEOM_DUMMY, lm_geom_dummy);
F_PROT(LM_GEOM_REQHGRID, lm_geom_reqhgrid);
F_PROT(LM_GEOM_REQHLIMT, lm_geom_reqhlimt);
F_PROT(LM_GEOM_REQHNUMC, lm_geom_reqhnumc);
F_PROT(LM_GEOM_REQHNUME, lm_geom_reqhnume);
F_PROT(LM_GEOM_REQHCONF, lm_geom_reqhconf);
F_PROT(LM_GEOM_REQHDIST, lm_geom_reqhdist);
F_PROT(LM_GEOM_REQPRM, lm_geom_reqprm);
F_PROT(LM_GEOM_ASGGRID, lm_geom_asggrid);
F_PROT(LM_GEOM_ASMESCL, lm_geom_asmescl);
F_PROT(LM_GEOM_CLCSRF, lm_geom_clcsrf);
F_PROT(LM_GEOM_CLCERR, lm_geom_clcerr);
F_PROT(LM_GEOM_CLCJELS, lm_geom_clcjels);
F_PROT(LM_GEOM_CLCJELV, lm_geom_clcjelv);
F_PROT(LM_GEOM_CLCSPLIT, lm_geom_clcsplit);
F_PROT(LM_GEOM_INTRP, lm_geom_intrp);
F_PROT(LM_GEOM_LCLELV, lm_geom_lclelv);
F_PROT(LM_GEOM_PASDEB, lm_geom_pasdeb);
 
// ---  class LM_LGCY
M_PROT(LM_LGCY_M, lm_lgcy_m, LM_LGCY_REQGDTA, lm_lgcy_reqgdta);
M_PROT(LM_LGCY_M, lm_lgcy_m, LM_LGCY_REQEDTA, lm_lgcy_reqedta);
F_PROT(LM_LGCY_ASMF, lm_lgcy_asmf);
F_PROT(LM_LGCY_ASMK, lm_lgcy_asmk);
F_PROT(LM_LGCY_ASMKT, lm_lgcy_asmkt);
F_PROT(LM_LGCY_ASMKU, lm_lgcy_asmku);
F_PROT(LM_LGCY_ASMM, lm_lgcy_asmm);
F_PROT(LM_LGCY_ASMMU, lm_lgcy_asmmu);
F_PROT(LM_LGCY_CLCCLIM, lm_lgcy_clcclim);
F_PROT(LM_LGCY_CLCDLIB, lm_lgcy_clcdlib);
F_PROT(LM_LGCY_CLCPRES, lm_lgcy_clcpres);
F_PROT(LM_LGCY_CLCPREV, lm_lgcy_clcprev);
F_PROT(LM_LGCY_CLCPRNO, lm_lgcy_clcprno);
F_PROT(LM_LGCY_HLPCLIM, lm_lgcy_hlpclim);
F_PROT(LM_LGCY_HLPPRGL, lm_lgcy_hlpprgl);
F_PROT(LM_LGCY_HLPPRNO, lm_lgcy_hlpprno);
F_PROT(LM_LGCY_PRCCLIM, lm_lgcy_prcclim);
F_PROT(LM_LGCY_PRCDLIB, lm_lgcy_prcdlib);
F_PROT(LM_LGCY_PRCPRES, lm_lgcy_prcpres);
F_PROT(LM_LGCY_PRCPREV, lm_lgcy_prcprev);
F_PROT(LM_LGCY_PRCPRGL, lm_lgcy_prcprgl);
F_PROT(LM_LGCY_PRCPRLL, lm_lgcy_prcprll);
F_PROT(LM_LGCY_PRCPRNO, lm_lgcy_prcprno);
F_PROT(LM_LGCY_PRCSOLC, lm_lgcy_prcsolc);
F_PROT(LM_LGCY_PRCSOLR, lm_lgcy_prcsolr);
F_PROT(LM_LGCY_PRNPRGL, lm_lgcy_prnprgl);
F_PROT(LM_LGCY_PRNPRNO, lm_lgcy_prnprno);
F_PROT(LM_LGCY_PSCPRNO, lm_lgcy_pscprno);
F_PROT(LM_LGCY_PSLCLIM, lm_lgcy_pslclim);
F_PROT(LM_LGCY_PSLDLIB, lm_lgcy_psldlib);
F_PROT(LM_LGCY_PSLPREV, lm_lgcy_pslprev);
F_PROT(LM_LGCY_PSLPRGL, lm_lgcy_pslprgl);
F_PROT(LM_LGCY_PSLPRNO, lm_lgcy_pslprno);
F_PROT(LM_LGCY_PSLSOLC, lm_lgcy_pslsolc);
F_PROT(LM_LGCY_PSLSOLR, lm_lgcy_pslsolr);
F_PROT(LM_LGCY_000, lm_lgcy_000);
F_PROT(LM_LGCY_999, lm_lgcy_999);
F_PROT(LM_LGCY_CTR, lm_lgcy_ctr);
F_PROT(LM_LGCY_DTR, lm_lgcy_dtr);
F_PROT(LM_LGCY_INI, lm_lgcy_ini);
F_PROT(LM_LGCY_RST, lm_lgcy_rst);
F_PROT(LM_LGCY_REQHBASE, lm_lgcy_reqhbase);
F_PROT(LM_LGCY_HVALIDE, lm_lgcy_hvalide);
F_PROT(LM_LGCY_REQHPRN, lm_lgcy_reqhprn);
 
// ---  class LM_UTIL
F_PROT(LM_UTIL_GEOCTR, lm_util_geoctr);
F_PROT(LM_UTIL_GEODTR, lm_util_geodtr);
F_PROT(LM_UTIL_AJTXXX, lm_util_ajtxxx);
F_PROT(LM_UTIL_REQXXX, lm_util_reqxxx);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_h2d2_elem()
{
   static char libname[] = "h2d2_elem";
 
   // ---  class GR_ELEM
   F_RGST(GR_ELEM_000, gr_elem_000, libname);
   F_RGST(GR_ELEM_999, gr_elem_999, libname);
   F_RGST(GR_ELEM_PKL, gr_elem_pkl, libname);
   F_RGST(GR_ELEM_UPK, gr_elem_upk, libname);
   F_RGST(GR_ELEM_CTR, gr_elem_ctr, libname);
   F_RGST(GR_ELEM_DTR, gr_elem_dtr, libname);
   F_RGST(GR_ELEM_INIVOL, gr_elem_inivol, libname);
   F_RGST(GR_ELEM_INISRF, gr_elem_inisrf, libname);
   F_RGST(GR_ELEM_RST, gr_elem_rst, libname);
   F_RGST(GR_ELEM_REQHBASE, gr_elem_reqhbase, libname);
   F_RGST(GR_ELEM_HVALIDE, gr_elem_hvalide, libname);
   F_RGST(GR_ELEM_PASDEB, gr_elem_pasdeb, libname);
   F_RGST(GR_ELEM_FLTRSRF, gr_elem_fltrsrf, libname);
   F_RGST(GR_ELEM_SAUVE, gr_elem_sauve, libname);
   F_RGST(GR_ELEM_REQLELE, gr_elem_reqlele, libname);
   F_RGST(GR_ELEM_REQITPE, gr_elem_reqitpe, libname);
   F_RGST(GR_ELEM_REQHCONF, gr_elem_reqhconf, libname);
   F_RGST(GR_ELEM_REQHDIST, gr_elem_reqhdist, libname);
   F_RGST(GR_ELEM_REQNNEL, gr_elem_reqnnel, libname);
   F_RGST(GR_ELEM_REQNCEL, gr_elem_reqncel, libname);
   F_RGST(GR_ELEM_REQNELT, gr_elem_reqnelt, libname);
   F_RGST(GR_ELEM_REQNELL, gr_elem_reqnell, libname);
   F_RGST(GR_ELEM_REQNELCOL, gr_elem_reqnelcol, libname);
   F_RGST(GR_ELEM_REQKELCOL, gr_elem_reqkelcol, libname);
 
   // ---  class GR_GRID
   F_RGST(GR_GRID_000, gr_grid_000, libname);
   F_RGST(GR_GRID_999, gr_grid_999, libname);
   F_RGST(GR_GRID_PKL, gr_grid_pkl, libname);
   F_RGST(GR_GRID_UPK, gr_grid_upk, libname);
   F_RGST(GR_GRID_CTR, gr_grid_ctr, libname);
   F_RGST(GR_GRID_DTR, gr_grid_dtr, libname);
   F_RGST(GR_GRID_INI, gr_grid_ini, libname);
   F_RGST(GR_GRID_RST, gr_grid_rst, libname);
   F_RGST(GR_GRID_REQHBASE, gr_grid_reqhbase, libname);
   F_RGST(GR_GRID_HVALIDE, gr_grid_hvalide, libname);
   F_RGST(GR_GRID_PASDEB, gr_grid_pasdeb, libname);
   F_RGST(GR_GRID_REQHGRID, gr_grid_reqhgrid, libname);
   F_RGST(GR_GRID_REQHCOOR, gr_grid_reqhcoor, libname);
   F_RGST(GR_GRID_REQHELEV, gr_grid_reqhelev, libname);
   F_RGST(GR_GRID_REQHELES, gr_grid_reqheles, libname);
   F_RGST(GR_GRID_REQHCONF, gr_grid_reqhconf, libname);
   F_RGST(GR_GRID_REQHDIST, gr_grid_reqhdist, libname);
   F_RGST(GR_GRID_REQHNUMC, gr_grid_reqhnumc, libname);
   F_RGST(GR_GRID_REQHNUME, gr_grid_reqhnume, libname);
   F_RGST(GR_GRID_REQNDIM, gr_grid_reqndim, libname);
   F_RGST(GR_GRID_REQNNL, gr_grid_reqnnl, libname);
   F_RGST(GR_GRID_REQNNP, gr_grid_reqnnp, libname);
   F_RGST(GR_GRID_REQNNT, gr_grid_reqnnt, libname);
   F_RGST(GR_GRID_REQNNELS, gr_grid_reqnnels, libname);
   F_RGST(GR_GRID_REQNNELV, gr_grid_reqnnelv, libname);
   F_RGST(GR_GRID_REQNCELS, gr_grid_reqncels, libname);
   F_RGST(GR_GRID_REQNCELV, gr_grid_reqncelv, libname);
   F_RGST(GR_GRID_REQNELLS, gr_grid_reqnells, libname);
   F_RGST(GR_GRID_REQNELLV, gr_grid_reqnellv, libname);
   F_RGST(GR_GRID_REQNELTS, gr_grid_reqnelts, libname);
   F_RGST(GR_GRID_REQNELTV, gr_grid_reqneltv, libname);
   F_RGST(GR_GRID_REQNDJV, gr_grid_reqndjv, libname);
   F_RGST(GR_GRID_REQNDJS, gr_grid_reqndjs, libname);
   F_RGST(GR_GRID_REQLDJV, gr_grid_reqldjv, libname);
   F_RGST(GR_GRID_REQLDJS, gr_grid_reqldjs, libname);
 
   // ---  class HS_CLIM
   F_RGST(HS_CLIM_000, hs_clim_000, libname);
   F_RGST(HS_CLIM_999, hs_clim_999, libname);
   F_RGST(HS_CLIM_CTR, hs_clim_ctr, libname);
   F_RGST(HS_CLIM_DTR, hs_clim_dtr, libname);
   F_RGST(HS_CLIM_INI, hs_clim_ini, libname);
   F_RGST(HS_CLIM_RST, hs_clim_rst, libname);
   F_RGST(HS_CLIM_REQHBASE, hs_clim_reqhbase, libname);
   F_RGST(HS_CLIM_HVALIDE, hs_clim_hvalide, libname);
   F_RGST(HS_CLIM_PASDEB, hs_clim_pasdeb, libname);
   F_RGST(HS_CLIM_PASFIN, hs_clim_pasfin, libname);
   F_RGST(HS_CLIM_REQHLMAP, hs_clim_reqhlmap, libname);
   F_RGST(HS_CLIM_REQHGRID, hs_clim_reqhgrid, libname);
   F_RGST(HS_CLIM_REQHCLIM, hs_clim_reqhclim, libname);
   F_RGST(HS_CLIM_REQNCLCND, hs_clim_reqnclcnd, libname);
   F_RGST(HS_CLIM_REQLCLCND, hs_clim_reqlclcnd, libname);
   F_RGST(HS_CLIM_REQNCLCNV, hs_clim_reqnclcnv, libname);
   F_RGST(HS_CLIM_REQLCLCNV, hs_clim_reqlclcnv, libname);
   F_RGST(HS_CLIM_REQNCLLIM, hs_clim_reqncllim, libname);
   F_RGST(HS_CLIM_REQNCLNOD, hs_clim_reqnclnod, libname);
   F_RGST(HS_CLIM_REQNCLELE, hs_clim_reqnclele, libname);
   F_RGST(HS_CLIM_REQLCLLIM, hs_clim_reqlcllim, libname);
   F_RGST(HS_CLIM_REQLCLNOD, hs_clim_reqlclnod, libname);
   F_RGST(HS_CLIM_REQLCLELE, hs_clim_reqlclele, libname);
   F_RGST(HS_CLIM_REQLCLDST, hs_clim_reqlcldst, libname);
   F_RGST(HS_CLIM_REQLDIMP, hs_clim_reqldimp, libname);
   F_RGST(HS_CLIM_REQLDIMV, hs_clim_reqldimv, libname);
   F_RGST(HS_CLIM_REQLEIMP, hs_clim_reqleimp, libname);
 
   // ---  class HS_DLIB
   F_RGST(HS_DLIB_000, hs_dlib_000, libname);
   F_RGST(HS_DLIB_999, hs_dlib_999, libname);
   F_RGST(HS_DLIB_CTR, hs_dlib_ctr, libname);
   F_RGST(HS_DLIB_DTR, hs_dlib_dtr, libname);
   F_RGST(HS_DLIB_INI, hs_dlib_ini, libname);
   F_RGST(HS_DLIB_RST, hs_dlib_rst, libname);
   F_RGST(HS_DLIB_REQHBASE, hs_dlib_reqhbase, libname);
   F_RGST(HS_DLIB_HVALIDE, hs_dlib_hvalide, libname);
   F_RGST(HS_DLIB_INIDDL, hs_dlib_iniddl, libname);
   F_RGST(HS_DLIB_INIFIC, hs_dlib_inific, libname);
   F_RGST(HS_DLIB_INIVAL, hs_dlib_inival, libname);
   F_RGST(HS_DLIB_MAJNEQ, hs_dlib_majneq, libname);
   F_RGST(HS_DLIB_PASDEB, hs_dlib_pasdeb, libname);
   F_RGST(HS_DLIB_PASFIN, hs_dlib_pasfin, libname);
   F_RGST(HS_DLIB_SAUVE, hs_dlib_sauve, libname);
   F_RGST(HS_DLIB_ESTINIT, hs_dlib_estinit, libname);
   F_RGST(HS_DLIB_PUSHLDLG, hs_dlib_pushldlg, libname);
   F_RGST(HS_DLIB_POPLDLG, hs_dlib_popldlg, libname);
   F_RGST(HS_DLIB_REQHGRID, hs_dlib_reqhgrid, libname);
   F_RGST(HS_DLIB_REQHVNOD, hs_dlib_reqhvnod, libname);
   F_RGST(HS_DLIB_REQNEQL, hs_dlib_reqneql, libname);
   F_RGST(HS_DLIB_REQNEQP, hs_dlib_reqneqp, libname);
   F_RGST(HS_DLIB_REQNNL, hs_dlib_reqnnl, libname);
   F_RGST(HS_DLIB_REQNNP, hs_dlib_reqnnp, libname);
   F_RGST(HS_DLIB_REQNNT, hs_dlib_reqnnt, libname);
   F_RGST(HS_DLIB_REQNDLN, hs_dlib_reqndln, libname);
   F_RGST(HS_DLIB_REQNDLL, hs_dlib_reqndll, libname);
   F_RGST(HS_DLIB_REQNDLP, hs_dlib_reqndlp, libname);
   F_RGST(HS_DLIB_REQNDLT, hs_dlib_reqndlt, libname);
   F_RGST(HS_DLIB_REQLDLG, hs_dlib_reqldlg, libname);
   F_RGST(HS_DLIB_REQLLOCN, hs_dlib_reqllocn, libname);
   F_RGST(HS_DLIB_REQTEMPS, hs_dlib_reqtemps, libname);
 
   // ---  class HS_PRES
   F_RGST(HS_PRES_000, hs_pres_000, libname);
   F_RGST(HS_PRES_999, hs_pres_999, libname);
   F_RGST(HS_PRES_CTR, hs_pres_ctr, libname);
   F_RGST(HS_PRES_DTR, hs_pres_dtr, libname);
   F_RGST(HS_PRES_INI, hs_pres_ini, libname);
   F_RGST(HS_PRES_RST, hs_pres_rst, libname);
   F_RGST(HS_PRES_REQHBASE, hs_pres_reqhbase, libname);
   F_RGST(HS_PRES_HVALIDE, hs_pres_hvalide, libname);
   F_RGST(HS_PRES_PASDEB, hs_pres_pasdeb, libname);
   F_RGST(HS_PRES_PASFIN, hs_pres_pasfin, libname);
   F_RGST(HS_PRES_REQHLMAP, hs_pres_reqhlmap, libname);
   F_RGST(HS_PRES_REQHGRID, hs_pres_reqhgrid, libname);
   F_RGST(HS_PRES_REQNELE, hs_pres_reqnele, libname);
   F_RGST(HS_PRES_REQNPREL, hs_pres_reqnprel, libname);
   F_RGST(HS_PRES_REQNPRELL, hs_pres_reqnprell, libname);
   F_RGST(HS_PRES_REQLPREL, hs_pres_reqlprel, libname);
 
   // ---  class HS_PREV
   F_RGST(HS_PREV_000, hs_prev_000, libname);
   F_RGST(HS_PREV_999, hs_prev_999, libname);
   F_RGST(HS_PREV_CTR, hs_prev_ctr, libname);
   F_RGST(HS_PREV_DTR, hs_prev_dtr, libname);
   F_RGST(HS_PREV_INI, hs_prev_ini, libname);
   F_RGST(HS_PREV_RST, hs_prev_rst, libname);
   F_RGST(HS_PREV_REQHBASE, hs_prev_reqhbase, libname);
   F_RGST(HS_PREV_HVALIDE, hs_prev_hvalide, libname);
   F_RGST(HS_PREV_PASDEB, hs_prev_pasdeb, libname);
   F_RGST(HS_PREV_PASFIN, hs_prev_pasfin, libname);
   F_RGST(HS_PREV_REQHLMAP, hs_prev_reqhlmap, libname);
   F_RGST(HS_PREV_REQHGRID, hs_prev_reqhgrid, libname);
   F_RGST(HS_PREV_REQHPREV, hs_prev_reqhprev, libname);
   F_RGST(HS_PREV_REQNELE, hs_prev_reqnele, libname);
   F_RGST(HS_PREV_REQNPREL, hs_prev_reqnprel, libname);
   F_RGST(HS_PREV_REQNPRELL, hs_prev_reqnprell, libname);
   F_RGST(HS_PREV_REQLPREL, hs_prev_reqlprel, libname);
 
   // ---  class HS_PRGL
   F_RGST(HS_PRGL_000, hs_prgl_000, libname);
   F_RGST(HS_PRGL_999, hs_prgl_999, libname);
   F_RGST(HS_PRGL_CTR, hs_prgl_ctr, libname);
   F_RGST(HS_PRGL_DTR, hs_prgl_dtr, libname);
   F_RGST(HS_PRGL_INI, hs_prgl_ini, libname);
   F_RGST(HS_PRGL_RST, hs_prgl_rst, libname);
   F_RGST(HS_PRGL_REQHBASE, hs_prgl_reqhbase, libname);
   F_RGST(HS_PRGL_HVALIDE, hs_prgl_hvalide, libname);
   F_RGST(HS_PRGL_PASDEB, hs_prgl_pasdeb, libname);
   F_RGST(HS_PRGL_PASMAJ, hs_prgl_pasmaj, libname);
   F_RGST(HS_PRGL_PASFIN, hs_prgl_pasfin, libname);
   F_RGST(HS_PRGL_REQHLMAP, hs_prgl_reqhlmap, libname);
   F_RGST(HS_PRGL_REQHGRID, hs_prgl_reqhgrid, libname);
   F_RGST(HS_PRGL_REQHPRGL, hs_prgl_reqhprgl, libname);
   F_RGST(HS_PRGL_REQNPRGL, hs_prgl_reqnprgl, libname);
   F_RGST(HS_PRGL_REQNPRGLL, hs_prgl_reqnprgll, libname);
   F_RGST(HS_PRGL_REQLPRGL, hs_prgl_reqlprgl, libname);
 
   // ---  class HS_PRNO
   F_RGST(HS_PRNO_000, hs_prno_000, libname);
   F_RGST(HS_PRNO_999, hs_prno_999, libname);
   F_RGST(HS_PRNO_CTR, hs_prno_ctr, libname);
   F_RGST(HS_PRNO_DTR, hs_prno_dtr, libname);
   F_RGST(HS_PRNO_INI, hs_prno_ini, libname);
   F_RGST(HS_PRNO_RST, hs_prno_rst, libname);
   F_RGST(HS_PRNO_REQHBASE, hs_prno_reqhbase, libname);
   F_RGST(HS_PRNO_HVALIDE, hs_prno_hvalide, libname);
   F_RGST(HS_PRNO_PASDEB, hs_prno_pasdeb, libname);
   F_RGST(HS_PRNO_PASFIN, hs_prno_pasfin, libname);
   F_RGST(HS_PRNO_REQHLMAP, hs_prno_reqhlmap, libname);
   F_RGST(HS_PRNO_REQHGRID, hs_prno_reqhgrid, libname);
   F_RGST(HS_PRNO_REQHPRNO, hs_prno_reqhprno, libname);
   F_RGST(HS_PRNO_REQNNL, hs_prno_reqnnl, libname);
   F_RGST(HS_PRNO_REQNPRNO, hs_prno_reqnprno, libname);
   F_RGST(HS_PRNO_REQNPRNOL, hs_prno_reqnprnol, libname);
   F_RGST(HS_PRNO_REQLPRNO, hs_prno_reqlprno, libname);
 
   // ---  class HS_SOLC
   F_RGST(HS_SOLC_000, hs_solc_000, libname);
   F_RGST(HS_SOLC_999, hs_solc_999, libname);
   F_RGST(HS_SOLC_CTR, hs_solc_ctr, libname);
   F_RGST(HS_SOLC_DTR, hs_solc_dtr, libname);
   F_RGST(HS_SOLC_INI, hs_solc_ini, libname);
   F_RGST(HS_SOLC_RST, hs_solc_rst, libname);
   F_RGST(HS_SOLC_REQHBASE, hs_solc_reqhbase, libname);
   F_RGST(HS_SOLC_HVALIDE, hs_solc_hvalide, libname);
   F_RGST(HS_SOLC_PASDEB, hs_solc_pasdeb, libname);
   F_RGST(HS_SOLC_PASFIN, hs_solc_pasfin, libname);
   F_RGST(HS_SOLC_REQHLMAP, hs_solc_reqhlmap, libname);
   F_RGST(HS_SOLC_REQHGRID, hs_solc_reqhgrid, libname);
   F_RGST(HS_SOLC_REQHSOLC, hs_solc_reqhsolc, libname);
   F_RGST(HS_SOLC_REQNNL, hs_solc_reqnnl, libname);
   F_RGST(HS_SOLC_REQNSOLC, hs_solc_reqnsolc, libname);
   F_RGST(HS_SOLC_REQNSOLCL, hs_solc_reqnsolcl, libname);
   F_RGST(HS_SOLC_REQLSOLC, hs_solc_reqlsolc, libname);
 
   // ---  class HS_SOLR
   F_RGST(HS_SOLR_000, hs_solr_000, libname);
   F_RGST(HS_SOLR_999, hs_solr_999, libname);
   F_RGST(HS_SOLR_CTR, hs_solr_ctr, libname);
   F_RGST(HS_SOLR_DTR, hs_solr_dtr, libname);
   F_RGST(HS_SOLR_INI, hs_solr_ini, libname);
   F_RGST(HS_SOLR_RST, hs_solr_rst, libname);
   F_RGST(HS_SOLR_REQHBASE, hs_solr_reqhbase, libname);
   F_RGST(HS_SOLR_HVALIDE, hs_solr_hvalide, libname);
   F_RGST(HS_SOLR_PASDEB, hs_solr_pasdeb, libname);
   F_RGST(HS_SOLR_PASFIN, hs_solr_pasfin, libname);
   F_RGST(HS_SOLR_REQHLMAP, hs_solr_reqhlmap, libname);
   F_RGST(HS_SOLR_REQHGRID, hs_solr_reqhgrid, libname);
   F_RGST(HS_SOLR_REQHSOLR, hs_solr_reqhsolr, libname);
   F_RGST(HS_SOLR_REQNNL, hs_solr_reqnnl, libname);
   F_RGST(HS_SOLR_REQNSOLR, hs_solr_reqnsolr, libname);
   F_RGST(HS_SOLR_REQNSOLRL, hs_solr_reqnsolrl, libname);
   F_RGST(HS_SOLR_REQLSOLR, hs_solr_reqlsolr, libname);
 
   // ---  class IC_FRML
   F_RGST(IC_FRML_XEQCTR, ic_frml_xeqctr, libname);
   F_RGST(IC_FRML_XEQMTH, ic_frml_xeqmth, libname);
   F_RGST(IC_FRML_REQCLS, ic_frml_reqcls, libname);
   F_RGST(IC_FRML_REQHDL, ic_frml_reqhdl, libname);
 
   // ---  class IC_SIMD
   F_RGST(IC_SIMD_AJTCTR, ic_simd_ajtctr, libname);
   F_RGST(IC_SIMD_XEQCTR, ic_simd_xeqctr, libname);
   F_RGST(IC_SIMD_XEQMTH, ic_simd_xeqmth, libname);
   F_RGST(IC_SIMD_REQCLS, ic_simd_reqcls, libname);
   F_RGST(IC_SIMD_REQHDL, ic_simd_reqhdl, libname);
 
   // ---  class IC_LM_ELEM
   F_RGST(IC_LM_ELEM_XEQCTR, ic_lm_elem_xeqctr, libname);
   F_RGST(IC_LM_ELEM_XEQMTH, ic_lm_elem_xeqmth, libname);
   F_RGST(IC_LM_ELEM_OPBDOT, ic_lm_elem_opbdot, libname);
   F_RGST(IC_LM_ELEM_REQCLS, ic_lm_elem_reqcls, libname);
   F_RGST(IC_LM_ELEM_REQHDL, ic_lm_elem_reqhdl, libname);
 
   // ---  class LG_LGCY
   F_RGST(LG_LGCY_000, lg_lgcy_000, libname);
   F_RGST(LG_LGCY_999, lg_lgcy_999, libname);
   F_RGST(LG_LGCY_CTR, lg_lgcy_ctr, libname);
   F_RGST(LG_LGCY_DTR, lg_lgcy_dtr, libname);
   F_RGST(LG_LGCY_INI, lg_lgcy_ini, libname);
   F_RGST(LG_LGCY_RST, lg_lgcy_rst, libname);
   F_RGST(LG_LGCY_REQHBASE, lg_lgcy_reqhbase, libname);
   F_RGST(LG_LGCY_HVALIDE, lg_lgcy_hvalide, libname);
   F_RGST(LG_LGCY_ASMESCL, lg_lgcy_asmescl, libname);
   F_RGST(LG_LGCY_CLCERR, lg_lgcy_clcerr, libname);
   F_RGST(LG_LGCY_CLCJELS, lg_lgcy_clcjels, libname);
   F_RGST(LG_LGCY_CLCJELV, lg_lgcy_clcjelv, libname);
   F_RGST(LG_LGCY_CLCSPLT, lg_lgcy_clcsplt, libname);
   F_RGST(LG_LGCY_INTRP, lg_lgcy_intrp, libname);
   F_RGST(LG_LGCY_LCLELV, lg_lgcy_lclelv, libname);
 
   // ---  class LM_ELEM
   M_RGST(LM_ELEM_M, lm_elem_m, LM_ELEM_REQEDTA, lm_elem_reqedta, libname);
   M_RGST(LM_ELEM_CALLCB_M, lm_elem_callcb_m, LM_ELEM_CALLCB, lm_elem_callcb, libname);
   F_RGST(LM_ELEM_000, lm_elem_000, libname);
   F_RGST(LM_ELEM_999, lm_elem_999, libname);
   F_RGST(LM_ELEM_CTR, lm_elem_ctr, libname);
   F_RGST(LM_ELEM_DTR, lm_elem_dtr, libname);
   F_RGST(LM_ELEM_INI, lm_elem_ini, libname);
   F_RGST(LM_ELEM_RST, lm_elem_rst, libname);
   F_RGST(LM_ELEM_REQHBASE, lm_elem_reqhbase, libname);
   F_RGST(LM_ELEM_HVALIDE, lm_elem_hvalide, libname);
   F_RGST(LM_ELEM_DUMMY, lm_elem_dummy, libname);
   F_RGST(LM_ELEM_REQPRM, lm_elem_reqprm, libname);
   F_RGST(LM_ELEM_ESTINI, lm_elem_estini, libname);
   F_RGST(LM_ELEM_ESTLIS, lm_elem_estlis, libname);
   F_RGST(LM_ELEM_ESTPRC, lm_elem_estprc, libname);
   F_RGST(LM_ELEM_ESTCLC, lm_elem_estclc, libname);
   F_RGST(LM_ELEM_ESTPSC, lm_elem_estpsc, libname);
   F_RGST(LM_ELEM_PUSHLDLG, lm_elem_pushldlg, libname);
   F_RGST(LM_ELEM_POPLDLG, lm_elem_popldlg, libname);
   F_RGST(LM_ELEM_REQHASH, lm_elem_reqhash, libname);
   F_RGST(LM_ELEM_REQHKID, lm_elem_reqhkid, libname);
   F_RGST(LM_ELEM_REQHLMGO, lm_elem_reqhlmgo, libname);
   F_RGST(LM_ELEM_REQHDLIB, lm_elem_reqhdlib, libname);
   F_RGST(LM_ELEM_REQHNUMC, lm_elem_reqhnumc, libname);
   F_RGST(LM_ELEM_REQHNUME, lm_elem_reqhnume, libname);
   F_RGST(LM_ELEM_REQTEMPS, lm_elem_reqtemps, libname);
   F_RGST(LM_ELEM_DMP, lm_elem_dmp, libname);
   F_RGST(LM_ELEM_DMPPROP, lm_elem_dmpprop, libname);
   F_RGST(LM_ELEM_PRN, lm_elem_prn, libname);
   F_RGST(LM_ELEM_PRNPRGL, lm_elem_prnprgl, libname);
   F_RGST(LM_ELEM_PRNPRNO, lm_elem_prnprno, libname);
   F_RGST(LM_ELEM_CLC, lm_elem_clc, libname);
   F_RGST(LM_ELEM_CLCPRE, lm_elem_clcpre, libname);
   F_RGST(LM_ELEM_CLCPST, lm_elem_clcpst, libname);
   F_RGST(LM_ELEM_PRCCLIM, lm_elem_prcclim, libname);
   F_RGST(LM_ELEM_PRCDLIB, lm_elem_prcdlib, libname);
   F_RGST(LM_ELEM_PRCPRES, lm_elem_prcpres, libname);
   F_RGST(LM_ELEM_PRCPREV, lm_elem_prcprev, libname);
   F_RGST(LM_ELEM_PRCPRGL, lm_elem_prcprgl, libname);
   F_RGST(LM_ELEM_PRCPRLL, lm_elem_prcprll, libname);
   F_RGST(LM_ELEM_PRCPRNO, lm_elem_prcprno, libname);
   F_RGST(LM_ELEM_PRCSOLC, lm_elem_prcsolc, libname);
   F_RGST(LM_ELEM_PRCSOLR, lm_elem_prcsolr, libname);
   F_RGST(LM_ELEM_CLCCLIM, lm_elem_clcclim, libname);
   F_RGST(LM_ELEM_CLCDLIB, lm_elem_clcdlib, libname);
   F_RGST(LM_ELEM_CLCPRES, lm_elem_clcpres, libname);
   F_RGST(LM_ELEM_CLCPREV, lm_elem_clcprev, libname);
   F_RGST(LM_ELEM_CLCPRNO, lm_elem_clcprno, libname);
   F_RGST(LM_ELEM_ASMDIM, lm_elem_asmdim, libname);
   F_RGST(LM_ELEM_ASMF, lm_elem_asmf, libname);
   F_RGST(LM_ELEM_ASMFKU, lm_elem_asmfku, libname);
   F_RGST(LM_ELEM_ASMIND, lm_elem_asmind, libname);
   F_RGST(LM_ELEM_ASMK, lm_elem_asmk, libname);
   F_RGST(LM_ELEM_ASMKT, lm_elem_asmkt, libname);
   F_RGST(LM_ELEM_ASMKU, lm_elem_asmku, libname);
   F_RGST(LM_ELEM_ASMM, lm_elem_asmm, libname);
   F_RGST(LM_ELEM_ASMMU, lm_elem_asmmu, libname);
   F_RGST(LM_ELEM_PASDEB, lm_elem_pasdeb, libname);
   F_RGST(LM_ELEM_PASFIN, lm_elem_pasfin, libname);
   F_RGST(LM_ELEM_PASMAJ, lm_elem_pasmaj, libname);
   F_RGST(LM_ELEM_PSCPRNO, lm_elem_pscprno, libname);
   F_RGST(LM_ELEM_PSLCLIM, lm_elem_pslclim, libname);
   F_RGST(LM_ELEM_PSLDLIB, lm_elem_psldlib, libname);
   F_RGST(LM_ELEM_PSLPREV, lm_elem_pslprev, libname);
   F_RGST(LM_ELEM_PSLPRGL, lm_elem_pslprgl, libname);
   F_RGST(LM_ELEM_PSLPRNO, lm_elem_pslprno, libname);
   F_RGST(LM_ELEM_PSLSOLC, lm_elem_pslsolc, libname);
   F_RGST(LM_ELEM_PSLSOLR, lm_elem_pslsolr, libname);
   F_RGST(LM_ELEM_HLP, lm_elem_hlp, libname);
   F_RGST(LM_ELEM_HLPCLIM, lm_elem_hlpclim, libname);
   F_RGST(LM_ELEM_HLPPRGL, lm_elem_hlpprgl, libname);
   F_RGST(LM_ELEM_HLPPRNO, lm_elem_hlpprno, libname);
 
   // ---  class LM_FRML
   F_RGST(LM_FRML_000, lm_frml_000, libname);
   F_RGST(LM_FRML_999, lm_frml_999, libname);
   F_RGST(LM_FRML_CTR, lm_frml_ctr, libname);
   F_RGST(LM_FRML_DTR, lm_frml_dtr, libname);
   F_RGST(LM_FRML_INIFRM, lm_frml_inifrm, libname);
   F_RGST(LM_FRML_RST, lm_frml_rst, libname);
   F_RGST(LM_FRML_REQHBASE, lm_frml_reqhbase, libname);
   F_RGST(LM_FRML_HVALIDE, lm_frml_hvalide, libname);
   F_RGST(LM_FRML_REQFRML, lm_frml_reqfrml, libname);
   F_RGST(LM_FRML_REQHMDL, lm_frml_reqhmdl, libname);
   F_RGST(LM_FRML_REQNDLN, lm_frml_reqndln, libname);
   F_RGST(LM_FRML_REQNDLEV, lm_frml_reqndlev, libname);
   F_RGST(LM_FRML_REQNDLES, lm_frml_reqndles, libname);
   F_RGST(LM_FRML_REQNPRGL, lm_frml_reqnprgl, libname);
   F_RGST(LM_FRML_REQNPRGLL, lm_frml_reqnprgll, libname);
   F_RGST(LM_FRML_REQNPRNO, lm_frml_reqnprno, libname);
   F_RGST(LM_FRML_REQNPRNOL, lm_frml_reqnprnol, libname);
   F_RGST(LM_FRML_REQNPREV, lm_frml_reqnprev, libname);
   F_RGST(LM_FRML_REQNPREVL, lm_frml_reqnprevl, libname);
   F_RGST(LM_FRML_REQNPRES, lm_frml_reqnpres, libname);
   F_RGST(LM_FRML_REQNSOLR, lm_frml_reqnsolr, libname);
   F_RGST(LM_FRML_REQNSOLRL, lm_frml_reqnsolrl, libname);
   F_RGST(LM_FRML_REQNSOLC, lm_frml_reqnsolc, libname);
   F_RGST(LM_FRML_REQNSOLCL, lm_frml_reqnsolcl, libname);
   F_RGST(LM_FRML_REQTGELV, lm_frml_reqtgelv, libname);
   F_RGST(LM_FRML_ESTLIN, lm_frml_estlin, libname);
 
   // ---  class LM_GEOM
   M_RGST(LM_GEOM_M, lm_geom_m, LM_GEOM_REQGDTA, lm_geom_reqgdta, libname);
   M_RGST(LM_GEOM_CALLCB_M, lm_geom_callcb_m, LM_GEOM_CALLCB, lm_geom_callcb, libname);
   F_RGST(LM_GEOM_000, lm_geom_000, libname);
   F_RGST(LM_GEOM_999, lm_geom_999, libname);
   F_RGST(LM_GEOM_CTR, lm_geom_ctr, libname);
   F_RGST(LM_GEOM_DTR, lm_geom_dtr, libname);
   F_RGST(LM_GEOM_INI, lm_geom_ini, libname);
   F_RGST(LM_GEOM_RST, lm_geom_rst, libname);
   F_RGST(LM_GEOM_REQHBASE, lm_geom_reqhbase, libname);
   F_RGST(LM_GEOM_HVALIDE, lm_geom_hvalide, libname);
   F_RGST(LM_GEOM_DUMMY, lm_geom_dummy, libname);
   F_RGST(LM_GEOM_REQHGRID, lm_geom_reqhgrid, libname);
   F_RGST(LM_GEOM_REQHLIMT, lm_geom_reqhlimt, libname);
   F_RGST(LM_GEOM_REQHNUMC, lm_geom_reqhnumc, libname);
   F_RGST(LM_GEOM_REQHNUME, lm_geom_reqhnume, libname);
   F_RGST(LM_GEOM_REQHCONF, lm_geom_reqhconf, libname);
   F_RGST(LM_GEOM_REQHDIST, lm_geom_reqhdist, libname);
   F_RGST(LM_GEOM_REQPRM, lm_geom_reqprm, libname);
   F_RGST(LM_GEOM_ASGGRID, lm_geom_asggrid, libname);
   F_RGST(LM_GEOM_ASMESCL, lm_geom_asmescl, libname);
   F_RGST(LM_GEOM_CLCSRF, lm_geom_clcsrf, libname);
   F_RGST(LM_GEOM_CLCERR, lm_geom_clcerr, libname);
   F_RGST(LM_GEOM_CLCJELS, lm_geom_clcjels, libname);
   F_RGST(LM_GEOM_CLCJELV, lm_geom_clcjelv, libname);
   F_RGST(LM_GEOM_CLCSPLIT, lm_geom_clcsplit, libname);
   F_RGST(LM_GEOM_INTRP, lm_geom_intrp, libname);
   F_RGST(LM_GEOM_LCLELV, lm_geom_lclelv, libname);
   F_RGST(LM_GEOM_PASDEB, lm_geom_pasdeb, libname);
 
   // ---  class LM_LGCY
   M_RGST(LM_LGCY_M, lm_lgcy_m, LM_LGCY_REQGDTA, lm_lgcy_reqgdta, libname);
   M_RGST(LM_LGCY_M, lm_lgcy_m, LM_LGCY_REQEDTA, lm_lgcy_reqedta, libname);
   F_RGST(LM_LGCY_ASMF, lm_lgcy_asmf, libname);
   F_RGST(LM_LGCY_ASMK, lm_lgcy_asmk, libname);
   F_RGST(LM_LGCY_ASMKT, lm_lgcy_asmkt, libname);
   F_RGST(LM_LGCY_ASMKU, lm_lgcy_asmku, libname);
   F_RGST(LM_LGCY_ASMM, lm_lgcy_asmm, libname);
   F_RGST(LM_LGCY_ASMMU, lm_lgcy_asmmu, libname);
   F_RGST(LM_LGCY_CLCCLIM, lm_lgcy_clcclim, libname);
   F_RGST(LM_LGCY_CLCDLIB, lm_lgcy_clcdlib, libname);
   F_RGST(LM_LGCY_CLCPRES, lm_lgcy_clcpres, libname);
   F_RGST(LM_LGCY_CLCPREV, lm_lgcy_clcprev, libname);
   F_RGST(LM_LGCY_CLCPRNO, lm_lgcy_clcprno, libname);
   F_RGST(LM_LGCY_HLPCLIM, lm_lgcy_hlpclim, libname);
   F_RGST(LM_LGCY_HLPPRGL, lm_lgcy_hlpprgl, libname);
   F_RGST(LM_LGCY_HLPPRNO, lm_lgcy_hlpprno, libname);
   F_RGST(LM_LGCY_PRCCLIM, lm_lgcy_prcclim, libname);
   F_RGST(LM_LGCY_PRCDLIB, lm_lgcy_prcdlib, libname);
   F_RGST(LM_LGCY_PRCPRES, lm_lgcy_prcpres, libname);
   F_RGST(LM_LGCY_PRCPREV, lm_lgcy_prcprev, libname);
   F_RGST(LM_LGCY_PRCPRGL, lm_lgcy_prcprgl, libname);
   F_RGST(LM_LGCY_PRCPRLL, lm_lgcy_prcprll, libname);
   F_RGST(LM_LGCY_PRCPRNO, lm_lgcy_prcprno, libname);
   F_RGST(LM_LGCY_PRCSOLC, lm_lgcy_prcsolc, libname);
   F_RGST(LM_LGCY_PRCSOLR, lm_lgcy_prcsolr, libname);
   F_RGST(LM_LGCY_PRNPRGL, lm_lgcy_prnprgl, libname);
   F_RGST(LM_LGCY_PRNPRNO, lm_lgcy_prnprno, libname);
   F_RGST(LM_LGCY_PSCPRNO, lm_lgcy_pscprno, libname);
   F_RGST(LM_LGCY_PSLCLIM, lm_lgcy_pslclim, libname);
   F_RGST(LM_LGCY_PSLDLIB, lm_lgcy_psldlib, libname);
   F_RGST(LM_LGCY_PSLPREV, lm_lgcy_pslprev, libname);
   F_RGST(LM_LGCY_PSLPRGL, lm_lgcy_pslprgl, libname);
   F_RGST(LM_LGCY_PSLPRNO, lm_lgcy_pslprno, libname);
   F_RGST(LM_LGCY_PSLSOLC, lm_lgcy_pslsolc, libname);
   F_RGST(LM_LGCY_PSLSOLR, lm_lgcy_pslsolr, libname);
   F_RGST(LM_LGCY_000, lm_lgcy_000, libname);
   F_RGST(LM_LGCY_999, lm_lgcy_999, libname);
   F_RGST(LM_LGCY_CTR, lm_lgcy_ctr, libname);
   F_RGST(LM_LGCY_DTR, lm_lgcy_dtr, libname);
   F_RGST(LM_LGCY_INI, lm_lgcy_ini, libname);
   F_RGST(LM_LGCY_RST, lm_lgcy_rst, libname);
   F_RGST(LM_LGCY_REQHBASE, lm_lgcy_reqhbase, libname);
   F_RGST(LM_LGCY_HVALIDE, lm_lgcy_hvalide, libname);
   F_RGST(LM_LGCY_REQHPRN, lm_lgcy_reqhprn, libname);
 
   // ---  class LM_UTIL
   F_RGST(LM_UTIL_GEOCTR, lm_util_geoctr, libname);
   F_RGST(LM_UTIL_GEODTR, lm_util_geodtr, libname);
   F_RGST(LM_UTIL_AJTXXX, lm_util_ajtxxx, libname);
   F_RGST(LM_UTIL_REQXXX, lm_util_reqxxx, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
