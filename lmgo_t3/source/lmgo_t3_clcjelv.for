C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LMGO_T3_CLCJELV
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T3_CLCJELV
C
C Description:
C     La fonction LMGO_T3_CLCJELV calcule les métriques pour des
C     éléments de volume de type T3.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T3_CLCJELV(NDIM,
     &                         NNT,
     &                         NCELV,
     &                         NELV,
     &                         NDJV,
     &                         VCORG,
     &                         KNGV,
     &                         VDJV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T3_CLCJELV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDIM
      INTEGER NNT
      INTEGER NCELV
      INTEGER NELV
      INTEGER NDJV
      REAL*8  VCORG(NDIM,  NNT)
      INTEGER KNGV (NCELV, NELV)
      REAL*8  VDJV (NDJV,  NELV)

      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IE, IN
      INTEGER NO1,  NO2,  NO3

      REAL*8 ZERO
      PARAMETER (ZERO = 0.0000000000000000000D0)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNT   .GE. 3)
D     CALL ERR_PRE(NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     BOUCLE SUR LES ÉLÉMENTS
      DO IE=1,NELV
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         VDJV(1,IE) = VCORG(2,NO3) - VCORG(2,NO1)                   ! Ksi,x
         VDJV(2,IE) = VCORG(2,NO1) - VCORG(2,NO2)                   ! Eta,x
         VDJV(3,IE) = VCORG(1,NO1) - VCORG(1,NO3)                   ! Ksi,y
         VDJV(4,IE) = VCORG(1,NO2) - VCORG(1,NO1)                   ! Eta,y
         VDJV(5,IE) = VDJV(4,IE)*VDJV(1,IE) - VDJV(3,IE)*VDJV(2,IE) ! Det J

C---        IMPRIME LES ELEMENTS A DETERMINANT NEGATIF
         IF (VDJV(5,IE) .LE. ZERO) THEN
            WRITE(ERR_BUF, '(A)') 'ERR_DETJ_NEGATIF'
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
            WRITE(LOG_BUF, '(A,I9,A,16I9)')
     &         'ERR_DETJ_NEGATIF:', IE, '--', (KNGV(IN,IE),IN=1,NCELV)
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
      ENDDO

      LMGO_T3_CLCJELV = ERR_TYP()
      RETURN
      END

