C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LMGO_T3_CLCJELS
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T3_CLCJELS
C
C Description:
C     La fonction LMGO_T3_CLCJELS calcule les métriques de pour les
C     éléments de surface de type L2, surface d'éléments T3.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T3_CLCJELS (NDIM,
     &                          NNT,
     &                          NCELV,
     &                          NCELS,
     &                          NELV,
     &                          NELS,
     &                          NDJV,
     &                          NDJS,
     &                          VCORG,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T3_CLCJELS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDIM
      INTEGER NNT
      INTEGER NCELV
      INTEGER NCELS
      INTEGER NELV
      INTEGER NELS
      INTEGER NDJV
      INTEGER NDJS
      REAL*8  VCORG(NDIM, NNT)
      INTEGER KNGV (NCELV,NELV)
      INTEGER KNGS (NCELS,NELS)
      REAL*8  VDJV (NDJV, NELV)
      REAL*8  VDJS (NDJS, NELS)

      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      REAL*8  UN_2
      PARAMETER (UN_2 = 5.0000000000000000000D-01)

      REAL*8  TX, TY, TL
      REAL*8  TXN, TYN
      INTEGER IEP, IE
      INTEGER NP1, NP2, NO1
C-----------------------------------------------------------------------

      DO 10 IEP=1,NELS
         NP1 = KNGS(1,IEP)
         NP2 = KNGS(2,IEP)
         IE  = KNGS(3,IEP)
         NO1 = KNGV(1,IE)
         IF (NP1 .EQ. NO1) THEN            ! COTE 1-2
            TX =   VDJV(4,IE)
            TY = - VDJV(2,IE)
         ELSEIF (NP2 .EQ. NO1) THEN        ! COTE 3-1
            TX =   VDJV(3,IE)
            TY = - VDJV(1,IE)
         ELSE                              ! COTE 2-3
            TX = - (VDJV(3,IE) + VDJV(4,IE))
            TY =   (VDJV(1,IE) + VDJV(2,IE))
         ENDIF
         TL  = HYPOT(TX, TY)
         TXN = TX/TL
         TYN = TY/TL
         VDJS(1,IEP) = TXN
         VDJS(2,IEP) = TYN
         VDJS(3,IEP) = UN_2*TL
10    CONTINUE

      LMGO_T3_CLCJELS = ERR_TYP()
      RETURN
      END

