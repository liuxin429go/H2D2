C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Functions:
C   Public:
C     INTEGER LM_GOT3_ASMPEAU
C   Private:
C
C Notes:
C     Travail en cours
C************************************************************************

      SUBMODULE(LM_GOT3_M) LM_GOT3_ASMPEAU_M
      
      CONTAINS
      
C************************************************************************
C Sommaire:  LM_GOT3_ASMPEAU
C
C Description:
C     La fonction LMGO_T3_ASMPEAU assemble la peau pour des
C     éléments de volume de type T3.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_GOT3_ASMPEAU (SELF,
     &                                         KPNT,
     &                                         NLIEN,
     &                                         KLIEN,
     &                                         INEXT)

      CLASS(LM_GOT3_T), INTENT(INOUT), TARGET :: SELF
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER INEXT

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER, DIMENSION(:,:), POINTER :: KNGV
      INTEGER NNL, NCELV, NELLV

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF
      INTEGER INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------
D     CALL ERR_PRE(LM_GOT3_HVALIDE(HOBJ))
C----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     CALL ERR_PRE(GDTA%NNL   .GE. 1)
D     CALL ERR_PRE(GDTA%NELLV .GE. 1)
D     CALL ERR_PRE(GDTA%NCELV .EQ. 6)
      NNL   = GDTA%NNL
      NELLV = GDTA%NNELV
      KNGV  => GDTA%KNGV

C---     Boucle sur les éléments
      NVLNK = .FALSE.
      INFO(2) = 0
      DO IE=1,NELLV

C---        SIDE 1 -- NODES 1-2
         IF (KNGV(1,IE) .LT. KNGV(2,IE)) THEN
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(2, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(2, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNL, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 2 -- NODES 2-3
         IF (KNGV(2,IE) .LT. KNGV(3,IE)) THEN
            ICLEF   = KNGV(2, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(2, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNL, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 3 -- NODES 3-1
         IF (KNGV(3,IE) .LT. KNGV(1,IE)) THEN
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNL, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

      ENDDO

9999  CONTINUE
      LM_GOT3_ASMPEAU = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT3_ASMPEAU

      END SUBMODULE LM_GOT3_ASMPEAU_M
