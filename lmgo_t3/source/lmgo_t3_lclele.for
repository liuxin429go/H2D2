C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LMGO_T3_LCLELE
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T3_LCLELE
C
C Description:
C     La fonction LMGO_T3_LCLELE localise l'élément sous les points
C     de la liste VCORP. Elle retourne dans KELEP les numéros d'élément
C     et dans VCORE les coordonnées sur l'élément de référence du point.
C     Si un point n'est pas trouvé son numéro d'élément est 0.
C
C Entrée:
C     NPNT        Nombre de points
C     VCORP       Coordonnées des points
C
C Sortie:
C     KELEP       Table des numéros d'élément
C     VCORE       Coordonnées sur l'élément de référence
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T3_LCLELE (TOL,
     &                         NDIM,
     &                         NNT,
     &                         NCELV,
     &                         NELV,
     &                         NDJV,
     &                         VCORG,
     &                         KNGV,
     &                         VDJV,
     &                         NPNT,
     &                         VCORP,
     &                         KELEP,
     &                         VCORE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T3_LCLELE
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  TOL
      INTEGER NDIM
      INTEGER NNT
      INTEGER NCELV
      INTEGER NELV
      INTEGER NDJV
      REAL*8  VCORG(NDIM, NNT)
      INTEGER KNGV (NCELV,NELV)
      REAL*8  VDJV (NDJV, NELV)
      INTEGER NPNT
      REAL*8  VCORP(NDIM, NPNT)
      INTEGER KELEP(NPNT)
      REAL*8  VCORE(NDIM, NPNT)

      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      REAL*8  ZERO, UN
      PARAMETER (ZERO = 0.0D0)
      PARAMETER (UN   = 1.0D0)

      REAL*8  VKX, VEX, VKY, VEY, DETJ
      REAL*8  X0X1, Y0Y1
      REAL*8  KSI,  ETA,  LAMBDA, DIST
      REAL*8  KOLD, EOLD, LOLD,   DOLD
      INTEGER IE, IP
      INTEGER NO1
      INTEGER NRESTE
      LOGICAL DEDANS
C----------------------------------------------------------------------

      CALL IINIT(NPNT, 0, KELEP, 1)
      NRESTE = NPNT

      DO IE=1,NELV
         NO1 = KNGV(1,IE)

C---        Les métriques
         VKX  = VDJV(1,IE)           ! Ksi,x
         VEX  = VDJV(2,IE)           ! Eta,x
         VKY  = VDJV(3,IE)           ! Ksi,y
         VEY  = VDJV(4,IE)           ! Eta,y
         DETJ = VDJV(5,IE)           ! Det J

C---        Les points
         DO IP=1,NPNT
            IF (KELEP(IP) .GT. 0) GOTO 199

            X0X1 = VCORP(1,IP) - VCORG(1,NO1)
            Y0Y1 = VCORP(2,IP) - VCORG(2,NO1)

C---           Coordonnées sur l'élément de référence
            KSI = (VKX * X0X1 + VKY * Y0Y1) / DETJ
            ETA = (VEY * Y0Y1 + X0X1 * VEX) / DETJ
            LAMBDA = UN - KSI - ETA

            DEDANS = (KSI .GE. ZERO) .AND.
     &               (ETA .GE. ZERO) .AND.
     &               (LAMBDA .GE. ZERO)
            IF (DEDANS) THEN
               KELEP(IP)   = IE
               VCORE(1,IP) = KSI
               VCORE(2,IP) = ETA
               NRESTE = NRESTE - 1
            ELSEIF (KELEP(IP) .EQ. 0) THEN
               KELEP(IP)   = -IE
               VCORE(1,IP) = KSI
               VCORE(2,IP) = ETA
            ELSEIF (KELEP(IP) .LT. 0) THEN
               KOLD = VCORE(1,IP)
               EOLD = VCORE(2,IP)
               LOLD = UN - KOLD - EOLD
               DOLD = ABS( MIN(KOLD, EOLD, LOLD) )
               DIST = ABS( MIN(KSI,  ETA,  LAMBDA) )
               IF (DIST .LT. DOLD) THEN
                  KELEP(IP)   = -IE
                  VCORE(1,IP) = KSI
                  VCORE(2,IP) = ETA
               ENDIF
            ENDIF

199         CONTINUE
         ENDDO

         IF (NRESTE .LE. 0) GOTO 200
      ENDDO
200   CONTINUE

C---     Apply tolerance
      DO IP=1,NPNT
         IF (KELEP(IP) .LT. 0) THEN
            KSI = VCORE(1,IP)
            ETA = VCORE(2,IP)
            LAMBDA = UN - KSI - ETA
            DIST = ABS( MIN(KSI, ETA, LAMBDA) )
            IF (DIST .LE. TOL) KELEP(IP) = -KELEP(IP)
         ENDIF
      ENDDO

      LMGO_T3_LCLELE = ERR_TYP()
      RETURN
      END
