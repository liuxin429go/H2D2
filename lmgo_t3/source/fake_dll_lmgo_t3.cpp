//************************************************************************
// H2D2 - External declaration of public symbols
// Module: lmgo_t3
// Entry point: extern "C" void fake_dll_lmgo_t3()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:03.082000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class LM_GOT3
F_PROT(LM_GOT3_CLCGRAD, lm_got3_clcgrad);
F_PROT(LM_GOT3_CLCDDX, lm_got3_clcddx);
F_PROT(LM_GOT3_CLCDDY, lm_got3_clcddy);
F_PROT(LM_GOT3_CLCHESS, lm_got3_clchess);
F_PROT(LM_GOT3_ASMDDX, lm_got3_asmddx);
F_PROT(LM_GOT3_ASMDDY, lm_got3_asmddy);
F_PROT(LM_GOT3_ASMML, lm_got3_asmml);
F_PROT(LM_GOT3_KUEHSSV, lm_got3_kuehssv);
F_PROT(LM_GOT3_KUEHSSS, lm_got3_kuehsss);
F_PROT(LM_GOT3_KUEGRDV, lm_got3_kuegrdv);
F_PROT(LM_GOT3_KUEDDX, lm_got3_kueddx);
F_PROT(LM_GOT3_KUEDDY, lm_got3_kueddy);
F_PROT(LM_GOT3_KUEML, lm_got3_kueml);
F_PROT(LM_GOT3_000, lm_got3_000);
F_PROT(LM_GOT3_999, lm_got3_999);
F_PROT(LM_GOT3_CTR, lm_got3_ctr);
F_PROT(LM_GOT3_DTR, lm_got3_dtr);
F_PROT(LM_GOT3_INI, lm_got3_ini);
F_PROT(LM_GOT3_RST, lm_got3_rst);
F_PROT(LM_GOT3_REQHBASE, lm_got3_reqhbase);
F_PROT(LM_GOT3_HVALIDE, lm_got3_hvalide);
F_PROT(LM_GOT3_ASMESCL, lm_got3_asmescl);
F_PROT(LM_GOT3_ASMPEAU, lm_got3_asmpeau);
F_PROT(LM_GOT3_CLCERR, lm_got3_clcerr);
F_PROT(LM_GOT3_CLCJELS, lm_got3_clcjels);
F_PROT(LM_GOT3_CLCJELV, lm_got3_clcjelv);
F_PROT(LM_GOT3_INTRP, lm_got3_intrp);
F_PROT(LM_GOT3_LCLELV, lm_got3_lclelv);
F_PROT(LM_GOT3_LCLELV_, lm_got3_lclelv_);
 
// ---  class LMGO_T3
F_PROT(LMGO_T3_ASGCMN, lmgo_t3_asgcmn);
F_PROT(LMGO_T3_ASMESCL, lmgo_t3_asmescl);
F_PROT(LMGO_T3_CLCERR, lmgo_t3_clcerr);
F_PROT(LMGO_T3_ERRRCOV, lmgo_t3_errrcov);
F_PROT(LMGO_T3_CLCJELS, lmgo_t3_clcjels);
F_PROT(LMGO_T3_CLCJELV, lmgo_t3_clcjelv);
F_PROT(LMGO_T3_INTRP, lmgo_t3_intrp);
F_PROT(LMGO_T3_LCLELE, lmgo_t3_lclele);
F_PROT(LMGO_T3_REQNFN, lmgo_t3_reqnfn);
F_PROT(LMGO_T3_REQPRM, lmgo_t3_reqprm);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_lmgo_t3()
{
   static char libname[] = "lmgo_t3";
 
   // ---  class LM_GOT3
   F_RGST(LM_GOT3_CLCGRAD, lm_got3_clcgrad, libname);
   F_RGST(LM_GOT3_CLCDDX, lm_got3_clcddx, libname);
   F_RGST(LM_GOT3_CLCDDY, lm_got3_clcddy, libname);
   F_RGST(LM_GOT3_CLCHESS, lm_got3_clchess, libname);
   F_RGST(LM_GOT3_ASMDDX, lm_got3_asmddx, libname);
   F_RGST(LM_GOT3_ASMDDY, lm_got3_asmddy, libname);
   F_RGST(LM_GOT3_ASMML, lm_got3_asmml, libname);
   F_RGST(LM_GOT3_KUEHSSV, lm_got3_kuehssv, libname);
   F_RGST(LM_GOT3_KUEHSSS, lm_got3_kuehsss, libname);
   F_RGST(LM_GOT3_KUEGRDV, lm_got3_kuegrdv, libname);
   F_RGST(LM_GOT3_KUEDDX, lm_got3_kueddx, libname);
   F_RGST(LM_GOT3_KUEDDY, lm_got3_kueddy, libname);
   F_RGST(LM_GOT3_KUEML, lm_got3_kueml, libname);
   F_RGST(LM_GOT3_000, lm_got3_000, libname);
   F_RGST(LM_GOT3_999, lm_got3_999, libname);
   F_RGST(LM_GOT3_CTR, lm_got3_ctr, libname);
   F_RGST(LM_GOT3_DTR, lm_got3_dtr, libname);
   F_RGST(LM_GOT3_INI, lm_got3_ini, libname);
   F_RGST(LM_GOT3_RST, lm_got3_rst, libname);
   F_RGST(LM_GOT3_REQHBASE, lm_got3_reqhbase, libname);
   F_RGST(LM_GOT3_HVALIDE, lm_got3_hvalide, libname);
   F_RGST(LM_GOT3_ASMESCL, lm_got3_asmescl, libname);
   F_RGST(LM_GOT3_ASMPEAU, lm_got3_asmpeau, libname);
   F_RGST(LM_GOT3_CLCERR, lm_got3_clcerr, libname);
   F_RGST(LM_GOT3_CLCJELS, lm_got3_clcjels, libname);
   F_RGST(LM_GOT3_CLCJELV, lm_got3_clcjelv, libname);
   F_RGST(LM_GOT3_INTRP, lm_got3_intrp, libname);
   F_RGST(LM_GOT3_LCLELV, lm_got3_lclelv, libname);
   F_RGST(LM_GOT3_LCLELV_, lm_got3_lclelv_, libname);
 
   // ---  class LMGO_T3
   F_RGST(LMGO_T3_ASGCMN, lmgo_t3_asgcmn, libname);
   F_RGST(LMGO_T3_ASMESCL, lmgo_t3_asmescl, libname);
   F_RGST(LMGO_T3_CLCERR, lmgo_t3_clcerr, libname);
   F_RGST(LMGO_T3_ERRRCOV, lmgo_t3_errrcov, libname);
   F_RGST(LMGO_T3_CLCJELS, lmgo_t3_clcjels, libname);
   F_RGST(LMGO_T3_CLCJELV, lmgo_t3_clcjelv, libname);
   F_RGST(LMGO_T3_INTRP, lmgo_t3_intrp, libname);
   F_RGST(LMGO_T3_LCLELE, lmgo_t3_lclele, libname);
   F_RGST(LMGO_T3_REQNFN, lmgo_t3_reqnfn, libname);
   F_RGST(LMGO_T3_REQPRM, lmgo_t3_reqprm, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
