C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Élément:
C
C Functions:
C   Public:
C     SUBROUTINE LMGO_T3_REQPRM
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne les paramètres de l'élément.
C
C Description:
C     La fonction <code>LMGO_T3_REQPRM</code> retourne tous les
C     paramètres caractéristiques de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE LMGO_T3_REQPRM (NDIM,
     &                           ITPEV, NNELV, NCELV, NDJV,
     &                           ITPES, NNELS, NCELS, NDJS,
     &                           ITPEZ, NNELZ, NCELZ, NDJZ, NEV2EZ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T3_REQPRM
CDEC$ ENDIF

      INTEGER NDIM
      INTEGER ITPEV, NNELV, NCELV, NDJV
      INTEGER ITPES, NNELS, NCELS, NDJS
      INTEGER ITPEZ, NNELZ, NCELZ, NDJZ, NEV2EZ

      INCLUDE 'egtpgeo.fi'
C-----------------------------------------------------------------------

      NDIM   =  2              ! NB DE DIMENSIONS

C---     Volume
      ITPEV  =  EG_TPGEO_T3    ! TYPE DE L'ELEMENT
      NNELV  =  3              ! NB DE NOEUDS
      NCELV  =  NNELV          ! NB DE CONNECTIVITÉS
      NDJV   =  NDIM*NDIM + 1  ! NB DE MÉTRIQUES
C---     Surface
      ITPES  =  EG_TPGEO_L2    ! TYPE DE L'ELEMENT
      NNELS  =  2              ! NB DE NOEUDS
      NCELS  =  NNELS + 2      ! NB DE CONNECTIVITÉS
                               ! NNELS + NUM. ELEM PARENT + NUM CÔTE
      NDJS   =  3              ! NB DE MÉTRIQUES
C---     Split
      ITPEZ  =  EG_TPGEO_INDEFINI    ! TYPE DE L'ELEMENT SPLIT
      NNELZ  =  0              ! NB DE NOEUDS
      NCELZ  =  NNELZ          ! NB DE CONNECTIVITÉS
      NDJZ   =  0              ! NB DE MÉTRIQUES
      NEV2EZ =  0              ! NB ELE VOL à ELE SPLIT

      RETURN
      END
