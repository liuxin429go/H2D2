//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Fonction C de lecture/écriture dans des fichiers ASCII
//
// Description:
//
// Notes:
//
//************************************************************************
#ifndef C_IDX_H_DEJA_INCLU
#define C_IDX_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define IINDEX F2C_CONF_DECOR_FNC(IINDEX, iindex)
#define ISWAPF F2C_CONF_DECOR_FNC(ISWAPF, iswapf)
#define ISWAPB F2C_CONF_DECOR_FNC(ISWAPB, iswapb)
#define DINDEX F2C_CONF_DECOR_FNC(DINDEX, dindex)
#define DSWAPF F2C_CONF_DECOR_FNC(DSWAPF, dswapf)
#define DSWAPB F2C_CONF_DECOR_FNC(DSWAPB, dswapb)
#define XINDEX F2C_CONF_DECOR_FNC(XINDEX, xindex)
#define XSWAPF F2C_CONF_DECOR_FNC(XSWAPF, xswapf)
#define XSWAPB F2C_CONF_DECOR_FNC(XSWAPB, xswapb)


F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL IINDEX (fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL ISWAPF (fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL ISWAPB (fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL DINDEX (fint_t*, double*, fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL DSWAPF (fint_t*, fint_t*, double*, fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL DSWAPB (fint_t*, fint_t*, double*, fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL XINDEX (fint_t*, int64_t*,fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL XSWAPF (fint_t*, fint_t*, int64_t*,fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL XSWAPB (fint_t*, fint_t*, int64_t*,fint_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_IDX_H_DEJA_INCLU
