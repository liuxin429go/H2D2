//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Contrôle du coprocesseur mathématique
//
// Description:
//    Fonctions C de contrôle du coprocesseur mathématique
//    (Floating Point Unit)
//
// Notes:
//
//************************************************************************
#ifndef C_FP_H_DEJA_INCLU
#define C_FP_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_FP_INIT     F2C_CONF_DECOR_FNC(C_FP_INIT,   c_fp_init)
#define C_FP_PRINT    F2C_CONF_DECOR_FNC(C_FP_PRINT,  c_fp_print)
#define C_FP_EPSMAC   F2C_CONF_DECOR_FNC(C_FP_EPSMAC, c_fp_epsmac)
#define C_FP_EPSILON  F2C_CONF_DECOR_FNC(C_FP_EPSILON,c_fp_epsilon)


F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FP_INIT   (fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FP_PRINT  ();
F2C_CONF_DLL_EXPORT double F2C_CONF_CNV_APPEL C_FP_EPSMAC ();
F2C_CONF_DLL_EXPORT double F2C_CONF_CNV_APPEL C_FP_EPSILON();


#ifdef __cplusplus
}
#endif

#endif   // C_FP_H_DEJA_INCLU
