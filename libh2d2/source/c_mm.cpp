//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  Memory Management
// Sommaire: Fonctions C de gestion de mémoire (Memory Management)
//************************************************************************

#include "c_mm.h"
#include "c_os.h"
#include "c_st.h"

#include <assert.h>
#include <malloc.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits>
#include <unordered_map>

#define MM_SIZE_MAX std::numeric_limits<size_t>::max()
#define MM_FINT_MAX std::numeric_limits<fint_t>::max()

// ---  Alignement
#if   defined(H2D2_WINDOWS)
#  define memalign(a,b) _aligned_malloc(b, a)
#  define memfree(a)    _aligned_free(a)
   const unsigned long MEM_alignement = 64;
#elif defined(H2D2_UNIX)
#  define memfree(a)    free(a)
   const unsigned long MEM_alignement = 64;
#else
#  error Invalid operating system
#endif

//#define C_MM_MODE_DEBUG
#ifdef C_MM_MODE_DEBUG
#   include <stdio.h>
#   define PRINT_DEBUG(a, b) printf(a,b)
#else
#   define PRINT_DEBUG(a, b)
#endif

// ---  Code d'erreur
enum MEM_CodeErreurMemoire
{
   MEM_MAP_INVALIDE,
   MEM_ALLOUEZEROBYTES,
   MEM_ALLOCATION,
   MEM_BLOC_INVALIDE,
   MEM_PTR_ABSENT,
   MEM_ECRASEMENT_BAS,
   MEM_ECRASEMENT_HAUT
};

// ---  Constantes
static int  MEM_compteur = 0;
static const uint64_t MEM_TAG_INT64 = 0xDDDDDDDDDDDDDDDD;
static const uint32_t MEM_TAG_INT32 = 0xDDDDDDDD;
static const unsigned char    MEM_TAG_CHAR  = 0xDD;
static const unsigned char    MEM_ERA_CHAR  = 0xEE;
static const unsigned int     MEM_HDL_BASE  = 0xAAAA0000;
static const unsigned char    MEM_TYP_ALLC  = 0x01;
static const unsigned char    MEM_TYP_PNTR  = 0x02;

//=============================================================================
// Schéma d'un bloc d'allocation
//
//    <  info  ><          data                         ><fin> 
//           TAG                                          TAG
//   |-------xxx|----------------------------------------|xxx|
//   ^          ^                                        ^
// debP       dataP                                     finP
//
//=============================================================================

// ---  Structures
template <size_t N>
bool tagValid32(const unsigned char* src)
{
   const uint32_t* src_as_int = reinterpret_cast<const uint32_t*>(src);
   for (size_t ii = 0; ii < N; ++ii)
      if (*src_as_int++ != MEM_TAG_INT32) return false;
   return true;
}
template <size_t N>
bool tagValid64(const unsigned char* src)
{
   const uint64_t* src_as_int = reinterpret_cast<const uint64_t*>(src);
   for (size_t ii = 0; ii < N; ++ii)
      if (*src_as_int++ != MEM_TAG_INT64) return false;
   return true;
}

struct MEM_infoDebut
{
   struct TTInfo
   {
      size_t size;
      size_t count;    // Ref count
      fint_t hndl;     // Handle
      fint_t fkind;    // Fortran type
      fint_t type;     // Alloc or external pointer
   };
   union
   {
      TTInfo info;
      unsigned char buf[MEM_alignement];
   };
   unsigned char* debP () { return (unsigned char*)(this); }
   unsigned char* tagP () { return debP () + sizeof(TTInfo); }
   void*          dataP() { return debP () + sizeof(MEM_infoDebut); }
   unsigned char* finP () { return debP () + sizeof(MEM_infoDebut) + size(); }
   size_t         size () { return info.size; }
   fint_t         hndl () { return info.hndl; }
   size_t         count() { return info.count; }
   fint_t         fkind() { return info.fkind; }
   fint_t         type () { return info.type; }
   bool isTagValid()      { return tagValid32<((sizeof(MEM_infoDebut) - sizeof(TTInfo)) >> 2)>(tagP()); }
};
typedef MEM_infoDebut *MEM_infoDebutP;

struct MEM_infoFin
{
   unsigned char buf[MEM_alignement];
   bool isTagValid()      { return tagValid64<(MEM_alignement >> 3)>(buf); }
};
typedef MEM_infoFin *MEM_infoFinP;

// ---  Assertion statiques de contrôle des structures
static_assert(sizeof(MEM_infoDebut) == MEM_alignement, "sizeof(MEM_infoDebut) != MEM_alignement");
static_assert(sizeof(MEM_infoDebut::TTInfo) <= MEM_alignement, "sizeof(TTInfo) < MEM_alignement");
static_assert(((sizeof(MEM_infoDebut) - sizeof(MEM_infoDebut::TTInfo)) & 0x03) == 0, "Header tag size not a multiple of int32_t");

static_assert(sizeof(MEM_infoFin) == MEM_alignement, "sizeof(MEM_infoDebut) != MEM_alignement");
static_assert((sizeof(MEM_infoFin) & 0x07) == 0, "Header tag size not a multiple of int64_t");


// ---  Map des blocs
typedef std::unordered_map<fint_t, MEM_infoDebutP> TTMapOnHndl;
typedef std::unordered_map<void*,  MEM_infoDebutP> TTMapOnData;
struct TTMaps
{
   TTMapOnHndl mapOnHndl;  // map sur les handles
   TTMapOnData mapOnData;  // map sur les données externes
};
struct TTMemStruc
{
   TTMaps glbMaps;
};
hndl_t  mapP = 0;    // Utilisé par chkcrc32 lorsque appelé en prologue

// ---  Prototypes
void           BSE_erreur  (MEM_CodeErreurMemoire, fint_t = 0, size_t = 0, void* = NULL);
void           BSE_assert  (TTMemStruc*, fint_t);

void           BSE_addBloc (TTMemStruc*, MEM_infoDebutP);
void           BSE_decBloc (TTMemStruc*, fint_t);
void           BSE_incBloc (TTMemStruc*, fint_t);
MEM_infoDebutP BSE_getBloc (TTMemStruc*, fint_t);

void           BSE_delXtrn (TTMemStruc*, void*);
void           BSE_addXtrn (TTMemStruc*, MEM_infoDebutP, void*);
void           BSE_decXtrn (TTMemStruc*, void*);
void           BSE_incXtrn (TTMemStruc*, void*);
MEM_infoDebutP BSE_getXtrn (TTMemStruc*, void*);

fint_t         BSE_alloc   (TTMemStruc*, fint_t, size_t, fint_t, void*);
fint_t         BSE_free    (TTMemStruc*, fint_t, fint_t);

// ---  Prototypes
bool           MEM_isBloc  (TTMemStruc*, fint_t);
MEM_infoDebutP MEM_getBloc (TTMemStruc*, fint_t);
fint_t         MEM_getCount(TTMemStruc*, fint_t);
void*          MEM_getData (TTMemStruc*, fint_t);
fint_t         MEM_getKind (TTMemStruc*, fint_t);
size_t         MEM_getLen  (TTMemStruc*, fint_t);
//fint_t         MEM_getHndl (TTMemStruc*, void*);

fint_t         MEM_decRef  (TTMemStruc*, fint_t);
fint_t         MEM_incRef  (TTMemStruc*, fint_t);
fint_t         MEM_alloc   (TTMemStruc*, fint_t, size_t, fint_t, void*);
fint_t         MEM_free    (TTMemStruc*, fint_t, fint_t);

#if defined(__INTEL_COMPILER)
#  pragma warning( push )
#  pragma warning( disable : 1195 )  // conversion from integer to smaller pointer
#  pragma warning( disable : 1684 )  // conversion from pointer to same-sized integral type
#  pragma warning( disable : 3180 )  // unrecognized OpenMP #pragma
#endif

//==============================================================
// BSE: 
//    Primitives de gestion des structures d'allocation
//    et de gestion de la mémoire. Ces fonctions ne sont pas 
//    thread safe.
//    Les fonctions BSE ne doivent faire d'appels qu'à d'autres
//    fonction BSE.
//==============================================================

//**************************************************************
// Sommaire: Affiche un message d'erreur.
//
// Description:
//    La fonction privée BSE_erreur(...) affiche le message
//    associé à l'erreur détectée.
//
// Entrée:
//    MEM_CodeErreurMemoire err        // Type d'erreur
//    fint_t hndl                      // Handle sur une table
//    size_t nbrBytes                  // Longueur de la structure
//    void*  ptrP                      // Pointeur à la structure
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_erreur (MEM_CodeErreurMemoire err,
                 fint_t hndl,
                 size_t nbrBytes,
                 void*  ptrP)
{
   switch (err)
   {
      case  MEM_MAP_INVALIDE:     // Map invalide, pointeur corrompu
         printf("\nMAP --- Pointeur aux structures internes corrompu\n");
         break;
      case  MEM_ALLOUEZEROBYTES:  // Allocation of 0 bytes
         printf("\nNEW --- Allocation de 0 bytes\n");
         break;
      case  MEM_ALLOCATION:  // Allocation error
         printf("\nNEW --- Erreur système d'allocation de mémoire\n");
         break;
      case  MEM_BLOC_INVALIDE:    // Bloc invalide, pointeur corrompu
         printf("\nMAP --- Bloc d'allocation corrompu\n");
         printf("hndl = %x\n", hndl);
         break;
      case  MEM_PTR_ABSENT:  // Pointer does not exist
         printf("\nDELETE; TEST --- Le pointeur n'existe pas\n");
         printf("hndl = %x\n", hndl);
         break;
      case  MEM_ECRASEMENT_BAS:  // Écrasement de données par le bas
         printf("\nDELETE; TEST--- Ecrasement de données par le bas\n");
         printf("hndl = %x ; Adresse = %px  : Nbr Bytes = %lu\n", hndl, ptrP, static_cast<unsigned long>(nbrBytes));
         break;
      case  MEM_ECRASEMENT_HAUT:  // Écrasement de données par le haut
         printf("\nDELETE; TEST--- Ecrasement de données par le haut\n");
         printf("hndl = %x ; Adresse = %px  : Nbr Bytes = %lu\n", hndl, ptrP, static_cast<unsigned long>(nbrBytes));
         break;
   }

   C_OS_RAISE(&C_OS_SIGSEGV);
}

//**************************************************************
// Sommaire: Retourne le pointeur au bloc d'allocation
//
// Description:
//    La fonction privée MEM_getBloc(...) retourne le pointeur au
//    bloc d'allocation correspondant au handle passé en argument.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t         hndl     Handle sur les données
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_assert(TTMemStruc* bP, fint_t hndl)
{
   // ---  Cherche le pointeur
   MEM_infoDebutP debP = BSE_getBloc(bP, hndl);
   MEM_infoFinP   finP = (MEM_infoFinP) debP->finP();

   // ---  Contrôles
   if (debP == NULL)          BSE_erreur(MEM_PTR_ABSENT, hndl);
   if (debP->hndl() != hndl)  BSE_erreur(MEM_BLOC_INVALIDE, hndl);
   if (debP->count() <=  0)   BSE_erreur(MEM_BLOC_INVALIDE, hndl);
   if (debP->type() != MEM_TYP_ALLC && 
       debP->type() != MEM_TYP_PNTR) BSE_erreur(MEM_BLOC_INVALIDE, hndl);
   if (!debP->isTagValid())   BSE_erreur(MEM_ECRASEMENT_BAS,  hndl, debP->size(), debP->dataP());
   if (!finP->isTagValid())   BSE_erreur(MEM_ECRASEMENT_HAUT, hndl, debP->size(), debP->dataP());
}

//**************************************************************
// Sommaire: Ajoute le bloc d'allocation
//
// Description:
//    La fonction privée BSE_addBloc(...) ajoute
//    les pointeurs au bloc d'allocation et aux données.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    MEM_infoDebutP debP     Pointeur au bloc d'allocation
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_addBloc(TTMemStruc* bP, MEM_infoDebutP debP)
{
   fint_t hndl = debP->hndl();
   bP->glbMaps.mapOnHndl[hndl] = debP;
}

//**************************************************************
// Sommaire: Efface le bloc d'allocation
//
// Description:
//    La fonction privée BSE_delBloc(...) efface
//    le bloc d'allocation.
//
// Entrée:
//    TTMemStruc* bP       Map des blocs d'allocation
//    fint_t      hndl     Handle sur le bloc d'allocation
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_delBloc(TTMemStruc* bP, fint_t hndl)
{
   bP->glbMaps.mapOnHndl.erase(hndl);
}

//**************************************************************
// Sommaire: Décrémente le compte de référence d'un bloc d'allocation
//
// Description:
//    La fonction privée BSE_decBloc(...) décrémente
//    le compte de référence du bloc d'allocation. 
//    En cas de débordement du compteur, indiqué par un
//    la valeur max, le compte n'est pas décrémenté.
//
// Entrée:
//    TTMemStruc* bP       Map des blocs d'allocation
//    fint_t      hndl     Handle sur le bloc d'allocation
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_decBloc(TTMemStruc* bP, fint_t hndl)
{
   MEM_infoDebutP debP = bP->glbMaps.mapOnHndl[hndl];
   if (debP->info.count < MM_SIZE_MAX) debP->info.count -= 1;
}

//**************************************************************
// Sommaire: Incrémente le compte de référence d'un bloc d'allocation
//
// Description:
//    La fonction privée BSE_decBloc(...) incrémente
//    le compte de référence du bloc d'allocation.
//    Bien que grand, le compte peut déborder. Dans ce cas,
//    il est maintenu au max.
//
// Entrée:
//    TTMemStruc* bP       Map des blocs d'allocation
//    fint_t      hndl     Handle sur le bloc d'allocation
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_incBloc(TTMemStruc* bP, fint_t hndl)
{
   MEM_infoDebutP debP = bP->glbMaps.mapOnHndl[hndl];
   if (debP->info.count < MM_SIZE_MAX) debP->info.count += 1;
}

//**************************************************************
// Sommaire: Retourne le pointeur au bloc d'allocation
//
// Description:
//    La fonction privée BSE_getBloc(...) retourne
//    le pointeur au bloc d'allocation si le bloc existe, NULL 
//    sinon.
//
// Entrée:
//    TTMemStruc* bP       Map des blocs d'allocation
//    fint_t      hndl     Handle sur le bloc d'allocation
//
// Sortie:
//
// Notes:
//**************************************************************
MEM_infoDebutP BSE_getBloc(TTMemStruc* bP, fint_t hndl)
{
   const TTMapOnHndl& mHndl = bP->glbMaps.mapOnHndl;
   TTMapOnHndl::const_iterator bI = mHndl.find(hndl);
   return (bI == mHndl.end()) ? NULL : (*bI).second;
}

//**************************************************************
// Sommaire: Retourne le pointeur au bloc d'allocation
//
// Description:
//    La fonction privée BSE_getBloc(...) retourne
//    le pointeur au bloc d'allocation si le bloc existe, NULL 
//    sinon.
//
// Entrée:
//    TTMemStruc* bP       Map des blocs d'allocation
//    void*       dtaP     Pointeur aux données
//
// Sortie:
//
// Notes:
//**************************************************************
MEM_infoDebutP BSE_getBloc(TTMemStruc* bP, void* dtaP)
{
   const TTMapOnData& mData = bP->glbMaps.mapOnData;
   TTMapOnData::const_iterator bI = mData.find(dtaP);
   return (bI == mData.end()) ? NULL : (*bI).second;
}

//**************************************************************
// Sommaire: Ajoute les données externes.
//
// Description:
//    La fonction privée BSE_addXtrn(...) enregistre
//    le pointeur aux données et son bloc d'allocation.
//
// Entrée:
//    TTMemStruc*    bP       Map des données externes
//    MEM_infoDebutP debP     Pointeur au bloc d'allocation
//    void*          dtaP     Pointeur aux données
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_addXtrn(TTMemStruc* bP, MEM_infoDebutP debP, void* dtaP)
{
   bP->glbMaps.mapOnData[dtaP] = debP;
}

//**************************************************************
// Sommaire: Retire les données externes
//
// Description:
//    La fonction privée BSE_delXtrn(...) retire le pointeur
//    aux données externes.
//
// Entrée:
//    TTMemStruc* bP       Map des blocs d'allocation
//    void*       dtaP     Pointeur aux données
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_delXtrn(TTMemStruc* bP, void* dtaP)
{
   MEM_infoDebutP debP = bP->glbMaps.mapOnData[dtaP];
   bP->glbMaps.mapOnData.erase(debP->dataP());
}

//**************************************************************
// Sommaire: Décrémente le compte de référence d'un bloc d'allocation
//
// Description:
//    La fonction privée BSE_decBloc(...) décrémente
//    le compte de référence du bloc d'allocation. 
//    En cas de débordement du compteur, indiqué par un
//    la valeur max, le compte n'est pas décrémenté.
//
// Entrée:
//    TTMemStruc* bP       Map des blocs d'allocation
//    fint_t      hndl     Handle sur le bloc d'allocation
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_decXtrn(TTMemStruc* bP, void* dtaP)
{
   MEM_infoDebutP debP = bP->glbMaps.mapOnData[dtaP];
   if (debP->info.count < MM_SIZE_MAX) debP->info.count -= 1;
}

//**************************************************************
// Sommaire: Incrémente le compte de référence d'un bloc d'allocation
//
// Description:
//    La fonction privée BSE_decBloc(...) incrémente
//    le compte de référence du bloc d'allocation.
//    Bien que grand, le compte peut déborder. Dans ce cas,
//    il est maintenu au max.
//
// Entrée:
//    TTMemStruc* bP       Map des blocs d'allocation
//    fint_t      hndl     Handle sur le bloc d'allocation
//
// Sortie:
//
// Notes:
//**************************************************************
void BSE_incXtrn(TTMemStruc* bP, void* dtaP)
{
   MEM_infoDebutP debP = bP->glbMaps.mapOnData[dtaP];
   if (debP->info.count < MM_SIZE_MAX) debP->info.count += 1;
}

//**************************************************************
// Sommaire: Retourne le pointeur au bloc d'allocation
//
// Description:
//    La fonction privée BSE_getBloc(...) retourne
//    le pointeur au bloc d'allocation si le bloc existe, NULL 
//    sinon.
//
// Entrée:
//    TTMemStruc* bP       Map des blocs d'allocation
//    void*       dtaP     Pointeur aux données
//
// Sortie:
//
// Notes:
//**************************************************************
MEM_infoDebutP BSE_getXtrn(TTMemStruc* bP, void* dtaP)
{
   const TTMapOnData& mData = bP->glbMaps.mapOnData;
   TTMapOnData::const_iterator bI = mData.find(dtaP);
   return (bI == mData.end()) ? NULL : (*bI).second;
}

//**************************************************************
// Sommaire:   Alloue un bloc de mémoire
//
// Description:
//    La fonction BSE_alloc(...) alloue un bloc de mémoire. Elle
//    alloue un bloc de mémoire de taille len bytes et retourne un handle
//    sur le bloc.
//    La fonction retourne le handle en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t*        len      Longueur du bloc à allouer
//
// Sortie:
//
// Notes:
//**************************************************************
fint_t BSE_alloc(TTMemStruc* bP, fint_t fknd, size_t nbytes, fint_t type, void* dtaP)
{
   // ---  Alloue
   size_t nbytesGlobal = nbytes + sizeof(MEM_infoDebut) + sizeof(MEM_infoFin);
   MEM_infoDebutP debP = (MEM_infoDebutP) memalign(MEM_alignement, nbytesGlobal);
   if (debP == NULL) BSE_erreur(MEM_ALLOCATION);

   // ---  Initialise
   void* dataP = debP->dataP();
   memset(debP, MEM_TAG_CHAR, nbytesGlobal);
   switch (type)
   {
      case MEM_TYP_ALLC: memset(dataP, 0x00, nbytes); break;
      case MEM_TYP_PNTR: memcpy(dataP, &dtaP, nbytes); break;
      default:
         BSE_erreur(MEM_ALLOCATION);
   }

   // ---  Remplis l'info
   debP->info.hndl  = MEM_HDL_BASE + (++MEM_compteur);
   debP->info.size  = nbytes;
   debP->info.count = 1;
   debP->info.fkind = fknd;
   debP->info.type  = type;

   // ---  Insère le nouveau bloc dans le map
   (void) BSE_addBloc(bP, debP);

   return debP->hndl();
}

//**************************************************************
// Sommaire: Libère la mémoire.
//
// Description:
//    La fonction BSE_free(...) libère un bloc de mémoire.
//    Le bloc est mis à zéro.
//    La fonction retourne 0 en cas d'échec et sinon -1.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t         fknd     Fortran type
//    fint_t         hndl     Handle sur le bloc à libérer
//
// Sortie:
//
// Notes:
//**************************************************************
fint_t BSE_free(TTMemStruc* bP, fint_t fknd, fint_t hndl)
{
   // ---  Contrôles
   (void) BSE_assert(bP, hndl);
   MEM_infoDebutP debP = BSE_getBloc(bP, hndl);
   PRINT_DEBUG("MEM_free count  = %d\n", debP->count());
   if (debP->count() != 1) return 0;
   if (debP->fkind() != fknd) return 0;

   // ---  Enlève du map
   (void) BSE_delBloc(bP, hndl);

   // ---  Efface la mémoire
   size_t nbytesGlobal = debP->size() + sizeof(MEM_infoDebut) + sizeof(MEM_infoFin);
   memset(debP, MEM_ERA_CHAR, nbytesGlobal);

   // ---  Désalloue la mémoire
   memfree(debP);

   return -1;
}


//==============================================================
// MEM: 
//    Les fonctions ajoutent les sections critiques OMP.
//==============================================================


//**************************************************************
// Sommaire: Retourne true si le bloc d'allocation existe
//
// Description:
//    La fonction privée MEM_isBloc(...) retourne true si le
//    handle passé en argument est valide.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t         hndl     Handle sur le bloc de données
//
// Sortie:
//
// Notes:
//**************************************************************
bool MEM_isBloc(TTMemStruc* bP, fint_t hndl)
{
   MEM_infoDebutP debP = NULL;
#pragma omp critical (OMP_CS_ALLC)
   debP = BSE_getBloc(bP, hndl);
   return debP != NULL;
}

//**************************************************************
// Sommaire: Retourne le pointeur au bloc d'allocation
//
// Description:
//    La fonction privée MEM_getBloc(...) retourne le pointeur au
//    bloc d'allocation correspondant au handle passé en argument.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t         hndl     Handle sur les données
//
// Sortie:
//
// Notes:
//**************************************************************
MEM_infoDebutP MEM_getBloc(TTMemStruc* bP, fint_t hndl)
{
   MEM_infoDebutP debP = NULL;

#pragma omp critical (OMP_CS_ALLC)
   {
      (void) BSE_assert(bP, hndl);
      debP = BSE_getBloc(bP, hndl);
   }

   return debP;
}

//**************************************************************
// Sommaire: Retourne le handle du pointeur
//
// Description:
//    La fonction privée MEM_getHndl(...) retourne le handle
//    associé au pointeur externe passé en argument. La fonction
//    retourne 0 en cas d'erreur.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    void*          dP       Pointeur aux données
//
// Sortie:
//
// Notes:
//**************************************************************
////fint_t MEM_getHndl(TTMemStruc* bP, void* dP)
////{
////   MEM_infoDebutP debP = NULL;
////
////#pragma omp critical (OMP_CS_ALLC)
////   debP = BSE_getBloc(bP, dP);
////   if (debP == NULL) return 0;
////   if (debP->type() != MEM_TYP_PNTR) return 0;
////
////   // ---  Contrôle l'écrasement
////   fint_t hndl = debP->hndl();
////   if (!debP->isTagValid())
////      BSE_erreur(MEM_ECRASEMENT_BAS, hndl, debP->size(), debP->dataP());
////   MEM_infoFinP finP = (MEM_infoFinP)debP->finP();
////   if (!finP->isTagValid())
////      BSE_erreur(MEM_ECRASEMENT_HAUT, hndl, debP->size(), debP->dataP());
////
////   return debP->hndl();
////}

//**************************************************************
// Sommaire: Retourne le compte de référence des données
//
// Description:
//    La fonction privée MEM_getKnd(...) retourne le compte de
//    référence correspondant au handle passé en argument.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t         hndl     Handle sur les données
//
// Sortie:
//
// Notes:
//**************************************************************
fint_t MEM_getCount(TTMemStruc* bP, fint_t hndl)
{
   MEM_infoDebutP debP = MEM_getBloc(bP, hndl);
   return (debP->count() < MM_FINT_MAX) ? (fint_t) debP->count() : -1;
}

//**************************************************************
// Sommaire: Retourne le pointeur aux données
//
// Description:
//    La fonction privée MEM_getData(...) retourne le pointeur aux
//    données correspondant au handle passé en argument.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t         hndl     Handle sur les données
//
// Sortie:
//
// Notes:
//**************************************************************
void* MEM_getData(TTMemStruc* bP, fint_t hndl)
{
   MEM_infoDebutP debP = MEM_getBloc(bP, hndl);

   void* rP = NULL;
   switch (debP->type())
   {
      case MEM_TYP_ALLC:   rP = debP->dataP(); break;
      case MEM_TYP_PNTR:   rP = *(fint_t**) debP->dataP(); break;
      default: BSE_erreur(MEM_ALLOCATION);
   }
   return rP;
}

//**************************************************************
// Sommaire: Retourne la taille des données
//
// Description:
//    La fonction privée MEM_getLen(...) retourne la longueur des
//    données correspondant au handle passé en argument.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t         hndl     Handle sur les données
//
// Sortie:
//
// Notes:
//**************************************************************
size_t MEM_getLen(TTMemStruc* bP, fint_t hndl)
{
   MEM_infoDebutP debP = MEM_getBloc(bP, hndl);
   return debP->size();
}

//**************************************************************
// Sommaire: Retourne le type des données
//
// Description:
//    La fonction privée MEM_getKnd(...) retourne le type des
//    données correspondant au handle passé en argument.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t         hndl     Handle sur les données
//
// Sortie:
//
// Notes:
//**************************************************************
fint_t MEM_getKind(TTMemStruc* bP, fint_t hndl)
{
   MEM_infoDebutP debP = MEM_getBloc(bP, hndl);
   return debP->fkind();
}

//**************************************************************
// Sommaire:   Alloue un bloc de mémoire
//
// Description:
//    La fonction MEM_alloc(...) alloue un bloc de mémoire. Elle
//    alloue un bloc de mémoire de taille len bytes et retourne un handle
//    sur le bloc.
//    La fonction retourne le handle en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t*        len      Longueur du bloc à allouer
//
// Sortie:
//
// Notes:
//**************************************************************
fint_t MEM_alloc(TTMemStruc* bP,
                 fint_t fknd,
                 size_t nbytes,
                 fint_t type,
                 void*  dtaP)
{
   fint_t hndl = 0;

#pragma omp critical (OMP_CS_ALLC)
   hndl = BSE_alloc(bP, fknd, nbytes, type, dtaP);

   return hndl;
}

//**************************************************************
// Sommaire: Libère la mémoire.
//
// Description:
//    La fonction MEM_free(...) libère un bloc de mémoire.
//    Le bloc est mis à zéro.
//    La fonction retourne 0 en cas d'échec et sinon -1.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t         fknd     Fortran type
//    fint_t         hndl     Handle sur le bloc à libérer
//
// Sortie:
//
// Notes:
// ?? Pourquoi ne pas désallouer les MEM_TYP_PNTR ??
//**************************************************************
fint_t MEM_free(TTMemStruc* bP, fint_t fknd, fint_t hndl)
{
   fint_t iret = -1;

#pragma omp critical (OMP_CS_ALLC)
   iret = BSE_free(bP, fknd, hndl);

   return iret;
}

//**************************************************************
// Sommaire: Réalloue un bloc de mémoire.
//
// Description:
//    La fonction C_MM_REALL(...) est la fonction appelable depuis
//    le FORTRAN pour réallouer de la mémoire. Elle réalloue
//    le bloc donné par le handle hdlOld en un bloc de taille
//    lenNew. Elle retourne un handle sur le bloc.
//    La fonction retourne le handle en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hdlOld       Handle de l'ancien bloc
//    fint_t* lenNew       Nouvelle taille du bloc
//
// Sortie:
//    fint_t* hdlNew       Handle du nouveau bloc
//
// Notes:
//
//**************************************************************
fint_t MEM_reall(TTMemStruc* bP, fint_t fknd, fint_t hdlOld, size_t sizeNew)
{
   fint_t hdlNew = 0;

   // ---  Contrôles
   MEM_infoDebutP debOldP = MEM_getBloc(bP, hdlOld);
   if (debOldP->type()  != MEM_TYP_ALLC) return 0;
   if (debOldP->fkind() != fknd) return 0;

#pragma omp critical (OMP_CS_ALLC)
   {
      // ---  Le bloc existant
      MEM_infoDebutP debOldP = BSE_getBloc(bP, hdlOld);  // Reload inside CS
      size_t countOld = debOldP->count();
      void*  dataOldP = debOldP->dataP();
      size_t sizeOld  = debOldP->size();

      // ---  Alloue le nouveau bloc
      hdlNew = BSE_alloc(bP, fknd, sizeNew, MEM_TYP_ALLC, NULL);
      //if (hdlNew == 0) return 0;

      // ---  Le nouveau bloc
      MEM_infoDebutP debNewP = BSE_getBloc(bP, hdlNew);
      size_t sizeNew  = debNewP->size();
      void*  dataNewP = debNewP->dataP();

      // ---  Transfert les données
      debNewP->info.count = countOld;
      if (sizeNew > sizeOld)
         memmove(dataNewP, dataOldP, sizeOld);
      else
         memmove(dataNewP, dataOldP, sizeNew);

      // ---  Désalloue l'ancien bloc
      debOldP->info.count = 1;      // Pour forcer le free
      (void) BSE_free(bP, fknd, hdlOld);
   }

   return hdlNew;
}

//**************************************************************
// Sommaire:   Incrémente le compte de référence d'un bloc.
//
// Description:
//    La fonction privée MEM_incRef(...) incrémente le compte de
//    référence pour un bloc d'allocation.
//    La fonction retourne -1 et 0 en cas d'échec.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t*        hndl     Handle sur le bloc
//
// Sortie:
//
// Notes:
//**************************************************************
fint_t MEM_incRef(TTMemStruc* bP, fint_t hndl)
{
   // ---  Contrôles
   MEM_infoDebutP debP = MEM_getBloc(bP, hndl);
   if (debP == NULL) return 0;
   if (debP->type()  != MEM_TYP_ALLC) return 0;
   if (debP->count() <= 0) return 0;

#pragma omp critical (OMP_CS_ALLC)
   {
      (void) BSE_incBloc(bP, hndl);
      PRINT_DEBUG("MEM_incRef count   = %d\n", debP->count());
   }

   return -1;
}

//**************************************************************
// Sommaire:   Décrémente le compte de référence d'un bloc.
//
// Description:
//    La fonction privée MEM_decRef(...) décrémente le compte de
//    référence pour un bloc d'allocation. Lorsque le compte tombe
//    à 0, le bloc est désalloué.
//    La fonction retourne -1 et 0 en cas d'échec.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t*        hndl     Handle sur le bloc
//
// Sortie:
//
// Notes:
//**************************************************************
fint_t MEM_decRef(TTMemStruc* bP, fint_t hndl)
{
   fint_t iret = 0;

   // ---  Contrôles
   MEM_infoDebutP debP = MEM_getBloc(bP, hndl);
   if (debP == NULL) return 0;
   if (debP->type() != MEM_TYP_ALLC) return 0;
   PRINT_DEBUG("MEM_incRef count   = %d\n", debP->count());

#pragma omp critical (OMP_CS_ALLC)
   {
      // ---  Décrémente le compte de référence
      if (debP->count() > 1)
      {
         (void) BSE_decBloc(bP, hndl);
      }
      // ---  Sinon désalloue
      else
      {
         fint_t fkind = -1;
         iret = BSE_free(bP, fkind, hndl);
      }
   }

   return iret;
}

//**************************************************************
// Sommaire:   Ajoute un pointeur externe.
//
// Description:
//    La fonction privée MEM_addptr(...) ajoute un pointeur comme
//    pointeur externe.
//    au système de gestion de mémoire. Son symétrique est la fonction
//    C_MM_REMPTR(...). La système maintient un compte de référence pour
//    les insertions répétitives.
//    La fonction retourne le handle en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t*        dtaP     Pointeur aux données
//
// Sortie:
//
// Notes:
//**************************************************************
fint_t MEM_addptr(TTMemStruc* bP, void* dtaP)
{
   fint_t hndl = 0;

#pragma omp critical (OMP_CS_ALLC)
   {
      // ---  Pointeur déjà enregistré ==> incrémente le compte de référence
      MEM_infoDebutP debP = BSE_getBloc(bP, dtaP);
      if (debP != NULL)
      {
         (void) BSE_incXtrn(bP, dtaP);
         PRINT_DEBUG("MEM_addptr count   = %d\n", debP->count());
         hndl = debP->hndl();
      }
      // ---  Handle inexistant ==> alloue
      else
      {
         // ---  Alloue un bloc
         fint_t fkind = -1;
         size_t nbytes = sizeof(fint_t*);
         hndl = BSE_alloc(bP, fkind, nbytes, MEM_TYP_PNTR, dtaP);
         PRINT_DEBUG("MEM_addptr hndl    = %x\n", hndl);
         // ---  Enregistre les données
         debP = BSE_getBloc(bP, hndl);
         (void) BSE_addXtrn(bP, debP, dtaP);
      }
   }

   return hndl;
}

//**************************************************************
// Sommaire:   Retire un pointeur externe.
//
// Description:
//    La fonction privée MEM_remptr(...) retire un pointeur comme
//    pointeur externe au système de gestion de mémoire.
//    La fonction retourne -1 en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    TTMemStruc*    bP       Map des blocs d'allocation
//    fint_t*        dtaP     Pointeur aux données
//
// Sortie:
//
// Notes:
//**************************************************************
fint_t MEM_remptr(TTMemStruc* bP, void* dtaP)
{
   fint_t iret = 0;

#pragma omp critical (OMP_CS_ALLC)
   {
      // ---  Cherche le pointeur
      MEM_infoDebutP debP = BSE_getXtrn(bP, dtaP);

      // ---  Décrémente le compte de référence
      if (debP->count() > 1)
      {
         (void) BSE_decXtrn(bP, dtaP);
      }
      // ---  Sinon désalloue
      else
      {
         // ---  Retire du map externe
         (void) BSE_delXtrn(bP, dtaP);
         // ---  Sinon désalloue
         fint_t fkind = -1;
         fint_t hndl  = debP->hndl();
         iret = BSE_free(bP, fkind, hndl);
      }
   }

   return iret;
}


//==============================================================
// C_MM: 
//    Fonction d'interface avec le FORTRAN.
//==============================================================


//************************************************************************
// Sommaire:   Constructeur de map
//
// Description:
//    La fonction C_MM_CTR() retourne un objet d'allocation nouvellement
//    alloué. Il est de la responsabilité de la méthode appelante de
//    disposer de la mémoire par un appel à C_MM_DTR.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    On assigne la variable globale mapP qui est utilisée par le chkcrc32
//    lorsque appelé dans le prologue d'une fonction.
//************************************************************************
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_CTR()
{
   TTMemStruc* bP = new TTMemStruc();

   mapP = reinterpret_cast<hndl_t>(bP);
   return mapP;
}

//************************************************************************
// Sommaire:   Destructeur de map.
//
// Description:
//    La fonction C_MM_DTR(...) détruis le map dont le handle est passé
//    en argument.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_DTR(hndl_t* hmmb)
{
   // ---  Contrôle la mémoire
   (void) C_MM_TEST(hmmb);

   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   delete bP;

   return 0;
}

//**************************************************************
// Sommaire:   Alloue un bloc de mémoire
//
// Description:
//    La fonction C_MM_ALLOC(...) est la fonction appelable depuis
//    le FORTRAN pour allouer une bloc de mémoire. Elle alloue un
//    bloc de mémoire de taille len bytes et retourne un handle
//    sur le bloc. Le système maintient un compte de référence qui
//    peut être géré par les fonction C_MM_INCREF et C_MM_DECREF.
//    <b>
//    La fonction retourne le handle en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    hndl_t* hmmb      Handle sur le map d'allocation
//    fint_t* fknd      Fortran type
//    fint_t* len       Longueur du bloc à allouer
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_ALLOC (hndl_t* hmmb,
                                                          fint_t* fknd,
                                                          fint_t* len)
{
   // ---  Contrôle les paramètres
   PRINT_DEBUG("C_MM_ALLOC len     = %d\n", *len);
   if (*len <= 0) return 0;

   // ---  Contrôle la mémoire
   (void) C_MM_TEST(hmmb);

   // ---  Alloue
   TTMemStruc* bP  = reinterpret_cast<TTMemStruc*>(*hmmb);
   fint_t hndl = MEM_alloc(bP, *fknd, *len, MEM_TYP_ALLC, NULL);

   // ---  Le handle
   PRINT_DEBUG("C_MM_ALLOC hndl    = %x\n", hndl);

   return hndl;
}

//**************************************************************
// Sommaire: Réalloue un bloc de mémoire.
//
// Description:
//    La fonction C_MM_REALL(...) est la fonction appelable depuis
//    le FORTRAN pour réallouer de la mémoire. Elle réalloue
//    le bloc donné par le handle hdlOld en un bloc de taille
//    lenNew. Elle retourne un handle sur le bloc.
//    La fonction retourne le handle en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hdlOld       Handle de l'ancien bloc
//    fint_t* lenNew       Nouvelle taille du bloc
//
// Sortie:
//    fint_t* hdlNew       Handle du nouveau bloc
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REALL(hndl_t* hmmb,
                                                         fint_t* fknd,
                                                         fint_t* hdlOld,
                                                         fint_t* lenNew)
{
   // ---  Contrôle les paramètres
   PRINT_DEBUG("C_MM_REALL hdlOld = %x\n", *hdlOld);
   PRINT_DEBUG("C_MM_REALL lenNew = %d\n", *lenNew);
   if (*lenNew <= 0) return 0;

   // ---  Contrôle la mémoire
   (void) C_MM_TEST(hmmb);

   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   fint_t hdlNew = MEM_reall(bP, *fknd, *hdlOld, *lenNew);

   return hdlNew;
}

//**************************************************************
// Sommaire: Libère la mémoire.
//
// Description:
//    La fonction C_MM_FREE(...) est la fonction appelable depuis
//    le FORTRAN pour libérer de la mémoire. Le bloc est mis à zéro.
//    La fonction retourne 0 en cas d'échec et sinon -1.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* fknd
//    fint_t* hndl         Handle sur la mémoire à libérer
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_FREE(hndl_t* hmmb,
                                                        fint_t* fknd,
                                                        fint_t* hndl)
{
   PRINT_DEBUG("C_MM_FREE hndl   = %x\n", *hndl);

   // ---  Contrôle la mémoire
   (void) C_MM_TEST(hmmb);

   // ---  Désalloue
   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   fint_t err = MEM_free(bP, *fknd, *hndl);

   return err;
}

//**************************************************************
// Sommaire:  Incrémente le compte de référence d'un bloc
//
// Description:
//    La fonction C_MM_INCREF(...) est la fonction appelable depuis
//    le FORTRAN pour incrémenter le compte de référence
//    maintenu pour le bloc d'allocation de handle hndl.
//    La fonction retourne -1 succès et 0 en cas d'échec.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le bloc d'allocation
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_INCREF(hndl_t* hmmb,
                                                          fint_t* hndl)
{
   // ---  Contrôle les paramètres
   PRINT_DEBUG("C_MM_INCREF hndl    = %p\n", hndl);

   // ---  Contrôle la mémoire
#ifdef C_MM_MODE_DEBUG
   (void) C_MM_TEST(hmmb);
#endif

   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   fint_t iret = MEM_incRef(bP, *hndl);

   return iret;
}

//**************************************************************
// Sommaire:   Décrémente le comte de référence d'un bloc.
//
// Description:
//    La fonction C_MM_INCREF(...) est la fonction appelable depuis
//    le FORTRAN pour décrémenter le compte de référence
//    maintenu pour le bloc d'allocation de handle hndl.
//    La fonction retourne -1 succès et 0 en cas d'échec.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le bloc d'allocation
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_DECREF(hndl_t* hmmb,
                                                          fint_t* hndl)
{
   PRINT_DEBUG("C_MM_DECREF hndl   = %x\n", *hndl);

   // ---  Contrôle la mémoire
#ifdef C_MM_MODE_DEBUG
   (void) C_MM_TEST(hmmb);
#endif

   // ---  Cherche le pointeur
   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   fint_t iret = MEM_decRef(bP, *hndl);

   return iret;
}

//**************************************************************
// Sommaire:   Ajoute un pointeur externe.
//
// Description:
//    La fonction C_MM_ADDPTR(...) est la fonction appelable depuis
//    le FORTRAN pour ajouter un pointeur comme pointeur externe
//    au système de gestion de mémoire. Son symétrique est la fonction
//    C_MM_REMPTR(...). La système maintient un compte de référence pour
//    les insertions répétitives.
//    La fonction retourne le handle en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    void*   dtaP         Pointeur aux données
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_ADDPTR(hndl_t* hmmb,
                                                          void* dtaP)
{
   // ---  Contrôle les paramètres
   PRINT_DEBUG("C_MM_ADDPTR data    = %p\n", dP);

   // ---  Contrôle la mémoire
#ifdef C_MM_MODE_DEBUG
   (void) C_MM_TEST(hmmb);
#endif

   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   fint_t hndl = MEM_addptr(bP, dtaP);

   return hndl;
}

//**************************************************************
// Sommaire:   Retire un pointeur externe.
//
// Description:
//    La fonction C_MM_REMPTR(...) est la fonction appelable depuis
//    le FORTRAN pour retirer un pointeur comme pointeur externe
//    au système de gestion de mémoire.
//    La fonction retourne -1 en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    void*   dtaP         Pointeur aux données
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REMPTR(hndl_t* hmmb,
                                                          void*   dtaP)
{
   PRINT_DEBUG("C_MM_REMPTR hndl   = %x\n", *hndl);

   // ---  Contrôle la mémoire
#ifdef C_MM_MODE_DEBUG
   (void) C_MM_TEST(hmmb);
#endif

   // ---  Cherche le pointeur
   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   fint_t iret = MEM_remptr(bP, dtaP);

   return iret;
}

//**************************************************************
// Sommaire: Test pour l’existence d'un bloc d'allocation
//
// Description:
//    La fonction C_MM_EXIST(...) retourne -1 si le handle hndl
//    existe et sinon retourne 0.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le pointeur à tester
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_EXIST(hndl_t* hmmb,
                                                         fint_t* hndl)
{
   // ---  Contrôle la mémoire
#ifdef C_MM_MODE_DEBUG
   (void) C_MM_TEST(hmmb);
#endif

   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   return MEM_isBloc(bP, *hndl) ? -1 : 0;
}

//**************************************************************
// Sommaire:   Calcul le CRC32 d'un bloc d'allocation
//
// Description:
//    La fonction C_MM_CLCCRC32(...) est la fonction appelable depuis
//    le FORTRAN pour calculer le CRC32 d'un bloc d'allocation de mémoire.
//    La fonction retourne -1 en cas de succès et 0 en cas d'échec.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le pointeur à traiter
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_CLCCRC32 (hndl_t* hmmb,
                                                             fint_t* hndl,
                                                             fint_t* icrc)
{
   *icrc = 0;

   // ---  Cherche le pointeur
   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   MEM_infoDebutP debP = MEM_getBloc(bP, *hndl);
   if (debP->size() <= 0) return 0;

   // ---  Calcule le CRC32
   unsigned int crc;
   crc32((unsigned char*)debP->dataP(), debP->size(), &crc);
   *icrc = crc;

   return -1;
}

//**************************************************************
// Sommaire:
//
// Description:
//    La fonction C_MM_CHKCRC32(...) est la fonction appelable depuis
//    le FORTRAN pour contrôler les CRC32 de chacun des blocs
//    d'allocation de mémoire.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
fint_t c_mm_crc32_int (hndl_t*, fint_t*, unsigned long long);

#if defined(H2D2_WINDOWS)
#  include <windows.h>
#  define C_MM_SLEEP(nsec) Sleep(nsec*1000)
#elif defined(H2D2_UNIX)
#  include <unistd.h>
#  define C_MM_SLEEP(nsec) sleep(nsec)
#else
#  error Invalid operating system
#endif

/*----------------------------------
   Entrée/sortie de fonction en mode profile
  ----------------------------------
#if defined(H2D2_CMPLR_GCC)
extern "C" void __attribute__((__no_instrument_function__)) __cyg_profile_func_enter(void*, void*);
extern "C" void __attribute__((__no_instrument_function__)) __cyg_profile_func_exit (void*, void*);

void __cyg_profile_func_enter(void*, void*)
{
   static unsigned long long ctr = 0;
   fint_t fknd = 32;    // SO_ALLC_RE8
   if (mapP != 0)
   {
      ++ctr;
      const unsigned long long bse = 108603000ULL;
      if (ctr >= (bse-1000) && ctr <= (bse+10)) c_mm_crc32_int(&mapP, &fknd, ctr);
//      if (ctr >= bse && (ctr % 1000) == 0) c_mm_crc32_int(&mapP, &fknd, ctr);
   }
}

void __cyg_profile_func_exit(void*, void*)
{
}

#elif defined(H2D2_WINDOWS)
extern "C" void __declspec(naked) _cdecl _penter( void )
{
   _asm {
      push eax
      push ebx
      push ecx
      push edx
      push ebp
      push edi
      push esi
    }

   fint_t fknd = 32;    // SO_ALLC_RE8
   if (mapP != 0) C_MM_CHKCRC32(&mapP, &fknd);

   _asm {
      pop esi
      pop edi
      pop ebp
      pop edx
      pop ecx
      pop ebx
      pop eax
      ret
    }
}
#endif
*/

//#include <mpi.h>
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_CHKCRC32 (hndl_t* hmmb,
                                                             fint_t* fknd)
{
   return c_mm_crc32_int(hmmb, fknd, 1);
}
fint_t c_mm_crc32_int (hndl_t* hmmb, fint_t* fknd, unsigned long long ctr)
{
   struct MEM_crc32
   {
      fint_t info;
      void*  dtaP;
      size_t size;
      unsigned int crc;
   };

   // ---  Ouvre le fichier
   static int i_rank = 0;
   static FILE* fd  = NULL;
   static bool lis_fic = false;
   if (fd == NULL)
   {
      char nom_fic[16] = "crc32_0x.log";
//      int i_err = MPI_Comm_rank(MPI_COMM_WORLD, &i_rank);
      if (i_rank == 0) nom_fic[7] = '0';
      if (i_rank == 1) nom_fic[7] = '1';
      if (i_rank == 2) nom_fic[7] = '2';
      if (i_rank == 3) nom_fic[7] = '3';

      lis_fic = true;         // Essaye en lecture
      fd = fopen(nom_fic, "rb");
      if (fd == 0)
      {
         lis_fic = false;     // Sinon en écriture
         fd = fopen(nom_fic, "wb");
         if (fd == 0)
            return -1;
      }
   }

   // ---  Contrôle la mémoire
//   (void) C_MM_TEST(hmmb);

   // ---  Calcule les crc32
   bool en_erreur = false;
   const TTMapOnHndl& mHndl = reinterpret_cast<TTMemStruc*>(*hmmb)->glbMaps.mapOnHndl;
   for (TTMapOnHndl::const_iterator dI = mHndl.begin(); dI != mHndl.end(); ++dI)
   {
      fint_t hndl = (*dI).first;
      MEM_infoDebutP debP  = (*dI).second;
      if (debP->type()  != MEM_TYP_ALLC) continue;
      if (debP->fkind() != *fknd) continue;

      unsigned int crc;
      crc32((unsigned char*)debP->dataP(), debP->size(), &crc);

      MEM_crc32 crc_n;
      crc_n.info = hndl;
      crc_n.dtaP = debP->dataP();
      crc_n.size = debP->size();
      crc_n.crc  = crc;
      if (! lis_fic)
      {
         fwrite(&crc_n, sizeof(MEM_crc32), 1, fd);
      }
      else
      {
         MEM_crc32 crc_o;
         fread(&crc_o, sizeof(MEM_crc32), 1, fd);

         bool idem = crc_n.info == crc_o.info && crc_n.size == crc_o.size && crc_n.crc == crc_o.crc;
         if (! idem)
         {
            printf("%llu %i: [%x] %px %10lu; CRC32 = %#08x\n", ctr, i_rank, crc_o.info, crc_o.dtaP, crc_o.size, crc_o.crc);
            printf("%llu %i: [%x] %px %10lu; CRC32 = %#08x\n", ctr, i_rank, crc_n.info, crc_n.dtaP, crc_n.size, crc_n.crc);
            en_erreur = true;
         }
      }
//      printf("[%2d] %px %8lu; CRC32 = %#08x\n", hndl, debP->dataP(), debP->size(), crc);
   }
   if (en_erreur)
   {
      printf("%llu %i - Waiting: ", ctr, i_rank);
      int cont = -1;
      while (cont != 0)
      {
         printf(".");
         C_MM_SLEEP(10);
         --cont;     // Pour le debug
      }
   }

   return (en_erreur) ? 0 : -1;
}

//**************************************************************
// Sommaire: Test les structures d'allocation.
//
// Description:
//    La fonction C_MM_TEST(...) est la fonction appelable depuis
//    le FORTRAN pour tester la structure interne des tables
//    d'allocation de mémoire.
//    La fonction retourne -1 en cas de succès et 0 en cas d'échec.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_TEST (hndl_t* hmmb)
{
   if (mapP != *hmmb) BSE_erreur(MEM_MAP_INVALIDE, 0, 0, NULL);

   // ---  Boucle sur les pointeurs
   const TTMapOnHndl& mHndl = reinterpret_cast<TTMemStruc*>(*hmmb)->glbMaps.mapOnHndl;
   for (TTMapOnHndl::const_iterator dI = mHndl.begin(); dI != mHndl.end(); ++dI)
   {
      // ---  Pointeurs
      fint_t hndl = (*dI).first;
      MEM_infoDebutP debP  = (*dI).second;
      MEM_infoFinP   finP  = (MEM_infoFinP) debP->finP();

      // ---  Contrôle l'écrasement
      if (! debP->isTagValid())
         BSE_erreur(MEM_ECRASEMENT_BAS, hndl, debP->size(), debP->dataP());
      if (! finP->isTagValid())
         BSE_erreur(MEM_ECRASEMENT_HAUT,hndl, debP->size(), debP->dataP());
   }

   return -1;
}

//**************************************************************
// Sommaire: Retourne la longueur d'un bloc d'allocation
//
// Description:
//    La fonction C_MM_REQLEN(...) retourne la taille en bytes des
//    données associées à hndl.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le pointeur
//
// Sortie:
//    Retourne la longueur du bloc d'allocation.
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REQLEN(hndl_t* hmmb,
                                                          fint_t* hndl)
{
   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   return static_cast<fint_t>(MEM_getLen(bP, *hndl));
}

//**************************************************************
// Sommaire: Retourne le type d'un bloc d'allocation
//
// Description:
//    La fonction C_MM_REQTYP(...) retourne le type des
//    données associées à hndl.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le pointeur
//
// Sortie:
//    Retourne le type du bloc d'allocation.
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REQTYP(hndl_t* hmmb,
                                                          fint_t* hndl)
{
   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   return MEM_getKind(bP, *hndl);
}

//**************************************************************
// Sommaire: Retourne le type d'un bloc d'allocation
//
// Description:
//    La fonction C_MM_REQDATA(...) retourne les données
//    associées à hndl sous la forme d'un pointeur à void.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le pointeur
//
// Sortie:
//    void**  dtaP
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REQDTA(hndl_t* hmmb,
                                                          fint_t* hndl, 
                                                          void**  dtaP)
{
   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   *dtaP = MEM_getData(bP, *hndl);
   return -1;
}

//************************************************************************
// Sommaire: Retourne l'indice du bloc
//
// Description:
//    La fonction C_MM_REQINDBYT(...) retourne l'indice du handle hndl par
//    rapport à la base d'allocation baseP. En cas d'erreur la fonction lève
//    l'exception SIGSEGV.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le pointeur
//    fint_t* baseP        Base pour l'allocation
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_REQINDBYT(hndl_t* hmmb,
                                                             fint_t* hndl,
                                                             char*   baseP)
{
   // ---  Contrôle la mémoire
#ifdef C_MM_MODE_DEBUG
   (void) C_MM_TEST(hmmb);
#endif

   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   char*  dataP = (char*) MEM_getData(bP, *hndl);
   hndl_t indItg = (dataP - baseP);
   ++indItg;

   return indItg;
}

//************************************************************************
// Sommaire: Retourne l'indice du bloc
//
// Description:
//    La fonction C_MM_REQINDIN4(...) retourne l'indice du handle hndl par
//    rapport à la base d'allocation baseP. En cas d'erreur la fonction lève
//    l'exception SIGSEGV.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le pointeur
//    fint_t* baseP        Base pour l'allocation
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_REQINDIN4(hndl_t* hmmb,
                                                             fint_t* hndl,
                                                             fint_t* baseP)
{
   // ---  Contrôle la mémoire
#ifdef C_MM_MODE_DEBUG
   (void) C_MM_TEST(hmmb);
#endif

   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   fint_t*  dataP  = (fint_t*) MEM_getData(bP, *hndl);
   hndl_t   indItg = (dataP - baseP);
   if ((char*)dataP == ((char*)baseP + indItg*sizeof(fint_t)) &&
      (reinterpret_cast<intptr_t>(dataP) % sizeof(fint_t)) == 0)
   {
      ++indItg;
   }
   else
   {
      C_OS_RAISE(&C_OS_SIGSEGV);
   }
   return indItg;
}

//************************************************************************
// Sommaire: Retourne l'indice du bloc
//
// Description:
//    La fonction C_MM_REQINDIN4(...) retourne l'indice du handle hndl par
//    rapport à la base d'allocation baseP. En cas d'erreur la fonction lève
//    l'exception SIGSEGV.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le pointeur
//    hndl_t* baseP        Base pour l'allocation
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_REQINDIN8(hndl_t* hmmb,
                                                             fint_t* hndl,
                                                             hndl_t* baseP)
{
   // ---  Contrôle la mémoire
#ifdef C_MM_MODE_DEBUG
   (void) C_MM_TEST(hmmb);
#endif

   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   hndl_t*  dataP  = (hndl_t*) MEM_getData(bP, *hndl);
   hndl_t   indItg = (dataP - baseP);
   if ((char*)dataP == ((char*)baseP + indItg*sizeof(hndl_t)) &&
      (reinterpret_cast<intptr_t>(dataP) % sizeof(hndl_t)) == 0)
   {
      ++indItg;
   }
   else
   {
      C_OS_RAISE(&C_OS_SIGSEGV);
   }
   return indItg;
}

//************************************************************************
// Sommaire: Retourne l'indice du bloc
//
// Description:
//    La fonction C_MM_REQINDRE8(...) retourne l'indice du handle hndl par
//    rapport à la base d'allocation baseP. En cas d'erreur la fonction lève
//    l'exception SIGSEGV.
//
// Entrée:
//    hndl_t* hmmb         Handle sur le map d'allocation
//    fint_t* hndl         Handle sur le pointeur
//    double* baseP        Base pour l'allocation
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_REQINDRE8(hndl_t* hmmb,
                                                             fint_t* hndl,
                                                             double* baseP)
{
   // ---  Contrôle la mémoire
#ifdef C_MM_MODE_DEBUG
   (void) C_MM_TEST(hmmb);
#endif

   TTMemStruc* bP = reinterpret_cast<TTMemStruc*>(*hmmb);
   double* dataP = (double*) MEM_getData(bP, *hndl);
   hndl_t indItg = (dataP - baseP);
   if ((char*)dataP == ((char*)baseP + indItg*sizeof(double)) &&
      (reinterpret_cast<intptr_t>(dataP) % sizeof(double)) == 0)
   {
      ++indItg;
   }
   else
   {
      C_OS_RAISE(&C_OS_SIGSEGV);
   }
   return indItg;
}

//************************************************************************
// Sommaire: Retourne la taille d'un INTEGER
//
// Description:
//    La fonction C_MM_REQINTSIZ(...) retourne la taille d'un INTEGER
//    FORTRAN.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REQINTSIZ()
{
   return static_cast<fint_t>(sizeof(fint_t));
}

//************************************************************************
// Sommaire: Contrôle la taille d'un INTEGER
//
// Description:
//    La fonction C_MM_CHKINTSIZ(...) contrôle la taille d'un INTEGER
//    FORTRAN en faisant la différence entre les pointeurs de deux
//    indices de la même table INTEGER FORTRAN.
//    <p>
//    Un exemple d'appel serait:
//       INTEGER KA(2)
//       IERR = C_MM_CHKINTSIZ(KA(1), KA(2))
//
// Entrée:
//    fint_t* p1P         Premier pointeur
//    fint_t* p2P         Second  pointeur
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_CHKINTSIZ(fint_t* p1P,
                                                             fint_t* p2P)
{
   PRINT_DEBUG("C_MM_CHKINTSIZ p1P = %px\n", p1P);
   PRINT_DEBUG("C_MM_CHKINTSIZ p2P = %px\n", p2P);
   PRINT_DEBUG("C_MM_CHKINTSIZ (char*)p2P-(char*)p1P = %ld\n", (char*)p2P-(char*)p1P);
   PRINT_DEBUG("C_MM_CHKINTSIZ sizeof(fint_t) = %ld\n", sizeof(fint_t));
   return (((char*)p2P-(char*)p1P) == sizeof(fint_t)) ? 0 : -1;
}

//************************************************************************
// Sommaire: Contrôle la taille d'un HANDLE
//
// Description:
//    La fonction C_MM_CHKHDLSIZ(...) contrôle que la taille du type C pour
//    un HANDLE soit aussi grande que la taille du type pour un
//    INTEGER*8 FORTRAN.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_CHKHDLSIZ(hndl_t* p1P,
                                                             hndl_t* p2P)
{
   PRINT_DEBUG("C_MM_CHKHDLSIZ p1P = %px\n", p1P);
   PRINT_DEBUG("C_MM_CHKHDLSIZ p2P = %px\n", p2P);
   PRINT_DEBUG("C_MM_CHKHDLSIZ (char*)p2P-(char*)p1P = %ld\n", (char*)p2P-(char*)p1P);
   PRINT_DEBUG("C_MM_CHKHDLSIZ sizeof(hndl_t) = %ld\n", sizeof(hndl_t));
   return (((char*)p2P-(char*)p1P) == sizeof(hndl_t)) ? 0 : -1;
}

#if defined(__INTEL_COMPILER)
#  pragma warning( pop )
#endif
