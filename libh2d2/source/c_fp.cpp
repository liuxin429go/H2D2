//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  FPu
// Sommaire: Fonctions C du Floating Point unit
//************************************************************************

#include "c_fp.h"

#if   defined(H2D2_WINDOWS)
//
#elif defined(H2D2_UNIX)
#  include <fenv.h>
#  include <fpu_control.h>
#else
#  error Invalid operating system
#endif

#include <float.h>
#include <math.h>

//************************************************************************
// Sommaire:   Démarre les fonctionnalités en C.
//
// Description:
//    La fonction <code>C_FP_INIT()</code> initialise le FPU avec la
//    configurations suivante:
//    <pre>
//    - pas d'exceptions
//    - arrondi au plus proche
//    - précision sur 53 bits
//    </pre>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FP_INIT(fint_t* xprec)
{
   int ierr = 0;

#if defined(H2D2_WINDOWS)
   errno_t err = 0;
   unsigned int ctrl = 0;
   unsigned int curr = 0;

   // ---  Disable all exceptions
   ctrl = (_EM_INVALID | _EM_DENORMAL | _EM_ZERODIVIDE | _EM_OVERFLOW | _EM_UNDERFLOW | _EM_INEXACT);
   if (! err) err = _controlfp_s(&curr, ctrl, _MCW_EM);

   // ---  Round to nearest
   ctrl = _RC_NEAR;
   if (! err) err = _controlfp_s(&curr, ctrl, _MCW_RC);

   // ---  Set precision (only valid on IA32)
   if (H2D2_ARCH_TYPE == H2D2_ARCH_WIN_IA32)
   {
      if (*xprec == 0)
         ctrl = _PC_53;
      else
         ctrl = _PC_64;
      if (!err) err = _controlfp_s(&curr, ctrl, _MCW_PC);
   }

   ierr = err ? -1 : 0;
#elif defined (H2D2_UNIX)

   // ---  Disable all exceptions
#if defined (__GNUC__)
   if (! ierr) ierr = fedisableexcept(FE_ALL_EXCEPT);
#endif

   // ---  Round to nearest
   if (! ierr) ierr = fesetround(FE_TONEAREST);

   // ---  Set precision
   unsigned int ctrl = 0;
   unsigned int mask = ~(_FPU_EXTENDED | _FPU_DOUBLE | _FPU_SINGLE);

   _FPU_GETCW(ctrl);
   if (*xprec == 0)
      ctrl = (ctrl & mask) | _FPU_DOUBLE;
   else
      ctrl = (ctrl & mask) | _FPU_EXTENDED;
   _FPU_SETCW(ctrl);

#else
#  error Invalid operating system
#endif

   return ierr;
}

//************************************************************************
// Sommaire: Imprime l'état du FPU
//
// Description:
//    La fonction <code>C_FP_PRINT()</code> imprime l'état du FPU.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FP_PRINT ()
{
   return 0;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_FP_EPSMAC()</code>
/* smallest such that 1.0+DBL_EPSILON != 1.0 *///
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT double F2C_CONF_CNV_APPEL C_FP_EPSMAC()
{
   return DBL_EPSILON;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_FP_EPSILON()</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT double F2C_CONF_CNV_APPEL C_FP_EPSILON()
{
   return 10.0 * sqrt(DBL_EPSILON);
}
