//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Fonctions et structures utilitaires C
//
// Description:
//
// Notes:
//
//************************************************************************
#ifndef C_ST_H_DEJA_INCLU
#define C_ST_H_DEJA_INCLU

#include "cconfig.h"
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

void crc32(unsigned char const*, size_t, unsigned int*);
void crc64(unsigned char const*, size_t, unsigned int*, unsigned int*);

#define C_ST_CRC32 F2C_CONF_DECOR_FNC(C_ST_CRC32, c_st_crc32)
#define C_ST_CRC64 F2C_CONF_DECOR_FNC(C_ST_CRC64, c_st_crc64)
#define C_ST_MD5   F2C_CONF_DECOR_FNC(C_ST_MD5,   c_st_md5)


F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_ST_CRC32 (F2C_CONF_STRING, fint_t*          F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_ST_CRC64 (F2C_CONF_STRING, fint_t*, fint_t* F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_ST_MD5   (fint_t*, void*, fint_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_ST_H_DEJA_INCLU

