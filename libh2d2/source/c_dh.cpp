//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  Date-Heure
// Sommaire: Fonctions C de date-heure
//************************************************************************
#include "c_dh.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <string>
#if defined(_OPENMP)
#  include <omp.h>
#endif
#if defined(H2D2_WINDOWS)
#  include <Windows.h>
#elif defined(H2D2_UNIX)
#  include <sys/time.h>
#  include <sys/times.h>
#  include <unistd.h>
#else
#  error Invalid operating system
#endif

//************************************************************************
// Sommaire: Retourne le temps Wall
//
// Description:
//    La fonction privée getWallTime(...) retourne le temps Wall sous forme
//    d'un double, nombre de secondes depuis un temps de référence non fixe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    La version Windows est prise de:
//    http://www.openasthra.com/c-tidbits/gettimeofday-function-for-windows/
//************************************************************************
double getWallTime()
{
   double res = 0;

#if defined(H2D2_WINDOWS)
   FILETIME ft;
   GetSystemTimeAsFileTime(&ft);

   ULARGE_INTEGER t;
   t.LowPart  = ft.dwLowDateTime;
   t.HighPart = ft.dwHighDateTime;
   t.QuadPart -= 116444736000000000ULL;  // DELTA EPOCH IN 100 nano

   res = static_cast<double>(t.QuadPart) / static_cast<double>(10000000);

#elif defined(H2D2_UNIX)
   struct timeval tp;
   gettimeofday(&tp, NULL);
   res = static_cast<double>(tp.tv_sec) + static_cast<double>(tp.tv_usec) / static_cast<double>(1000000);

#else
#  error Invalid operating system
#endif

   return res;
}

//************************************************************************
// Sommaire: Retourne le temps CPU
//
// Description:
//    La fonction privée getCPUTime(...) retourne le temps CPU sous forme
//    d'un double, nombre de secondes depuis le démarrage du process.
//
// Entrée:
//
// Sortie:
//    double* tmr;         Le temps de démarrage du chrono
//
// Notes:
//  Le code n'est pas supporté sur windows 95/98/ME, voir
//  http://msdn.microsoft.com/en-us/library/ms683223(VS.85).aspx
//************************************************************************
double getCPUTime ()
{
   double res = 0;

#if defined(H2D2_WINDOWS)
   // --- Prend le handle du process
   HANDLE handleProcess = GetCurrentProcess();

   // --- Demande les temps du process
   FILETIME tempsCreation;
   FILETIME tempsFin;
   FILETIME tempsSys;
   FILETIME tempsUser;
   GetProcessTimes(handleProcess,
                   &tempsCreation,
                   &tempsFin,
                   &tempsSys,
                   &tempsUser);

   // --- Transfère les typeTemps dans un ULARGE_INTEGER pour faire la somme en 64 bits
   ULARGE_INTEGER tempsSys64;
   ULARGE_INTEGER tempsUser64;
   tempsSys64.LowPart = tempsSys.dwLowDateTime;
   tempsSys64.HighPart = tempsSys.dwHighDateTime;
   tempsUser64.LowPart = tempsUser.dwLowDateTime;
   tempsUser64.HighPart = tempsUser.dwHighDateTime;

   // --- Fait la somme des temps user et système
   tempsSys64.QuadPart += tempsUser64.QuadPart;

   res = static_cast<double>(tempsSys64.QuadPart) / static_cast<double>(10000000);

#elif defined(H2D2_UNIX)
   struct tms use;
   times(&use);
   clock_t temps = use.tms_utime + use.tms_stime;
   res = static_cast<double>(temps) / static_cast<double>(sysconf(_SC_CLK_TCK));

#else
#  error Invalid operating system
#endif

   return res;
}

//************************************************************************
// Sommaire: time structure into unix UTC time and reverse
//
// Description:
//    Le namespace privé
//
// Notes:
//************************************************************************
namespace gmt_i64
{
   typedef int64_t time_t;

   static const int YEAR0      =  1900;
   static const int EPOCH_YEAR =  1970;
   static const int SECS_DAY   = (24 * 3600);

   static const short ndays[2][12] =
   {
       { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
       { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }
   };

   bool is_leap(int y) {
      return ( (y % 4) == 0 && ((y % 100) != 0 || (y % 400) == 0) );
   }

   int year_size(int y) {
      return is_leap(y) ? 366 : 365;
   }

/*
 * Copyright (c) 1997 Kungliga Tekniska Högskolan
 * (Royal Institute of Technology, Stockholm, Sweden).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

   /*
      Converts a time structure into unix UTC time.
      Time can be negative.
   */
   gmt_i64::time_t timegm(struct tm const *tm)
   {
      gmt_i64::time_t res = 0;

      const int year = tm->tm_year + YEAR0;
      for (int i = year; i < EPOCH_YEAR; ++i)
         res -= year_size(i);
      for (int i = EPOCH_YEAR; i < year; ++i)
         res += year_size(i);

      const int idx_ndays = is_leap(year) ? 1 : 0;
      for (int i = 0; i < tm->tm_mon; ++i)
         res += ndays[idx_ndays][i];
      res += tm->tm_mday - 1;
      res *= 24;
      res += tm->tm_hour;
      res *= 60;
      res += tm->tm_min;
      res *= 60;
      res += tm->tm_sec;
      return res;
   }

   /*
      Converts unix GMT time into time structure
      Time can be negative.
   */
   struct tm * gmtime(const gmt_i64::time_t *timer)
   {
      static struct tm br_time;
      struct tm *tm = &br_time;

      gmt_i64::time_t time = *timer;

      int year = EPOCH_YEAR;
      int dayno = static_cast<int>(time / SECS_DAY);
      if (time < 0) dayno -= 1;
      int dayclock = static_cast<int>(time - (dayno * SECS_DAY));

      if (time > 0) {
         while (dayno >= year_size(year)) {
            dayno -= year_size(year);
            year++;
         }
      }
      else {
         while (dayno < 0) {
            year--;
            dayno += year_size(year);
         }
      }
      tm->tm_year = year - YEAR0;
      tm->tm_yday = dayno;
      tm->tm_wday = (dayno + 4) % 7;       /* day 0 was a thursday */

      tm->tm_mon = 0;
      const int idx_ndays = is_leap(year) ? 1 : 0;
      while (dayno >= ndays[idx_ndays][tm->tm_mon])
      {
         dayno -= ndays[idx_ndays][tm->tm_mon];
         tm->tm_mon++;
      }
      tm->tm_mday = dayno + 1;
      tm->tm_isdst = 0;

      tm->tm_hour = dayclock / 3600;
      tm->tm_min  = (dayclock % 3600) / 60;
      tm->tm_sec  = dayclock % 60;

      return tm;
   }
}  // namespace gmt_i64

//************************************************************************
// Sommaire:   Convertis une date en temps local
//
// Description:
//    La fonction C_DH_C2DLCL(...) convertis une date à partir de ses
//    composantes en un double.
//
// Entrée:
//    int* annee, int* mois, int* jour
//    int* heure, int* min,  int* sec     Les composantes de la date
//
// Sortie:
//    double* date                        La date
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_C2DLCL (double* date,
                                                           fint_t* annee, fint_t* mois, fint_t* jour,
                                                           fint_t* heure, fint_t* min,  fint_t* sec, fint_t* msc)
{
   if (*msc < 0 || *msc >= 1000000) return -1;

   struct tm t;
   t.tm_sec  = static_cast<int>(*sec);
   t.tm_min  = static_cast<int>(*min);
   t.tm_hour = static_cast<int>(*heure);
   t.tm_mday = static_cast<int>(*jour);
   t.tm_mon  = static_cast<int>(*mois-1);
   t.tm_year = static_cast<int>(*annee-1900);
   *date = static_cast<double>(mktime(&t));
   *date += static_cast<double>(*msc) / 1000000.0;

   return 0;
}

//************************************************************************
// Sommaire:   Convertis une date en temps UTC
//
// Description:
//    La fonction C_DH_C2DUTC(...) convertis une date à partir de ses
//    composantes en un double.
//
// Entrée:
//    int* annee, int* mois, int* jour
//    int* heure, int* min,  int* sec     Les composantes de la date
//
// Sortie:
//    double* date                        La date
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_C2DUTC (double* date,
                                                           fint_t* annee, fint_t* mois, fint_t* jour,
                                                           fint_t* heure, fint_t* min,  fint_t* sec, fint_t* msc)
{
   if (*msc < 0 || *msc >= 1000000) return -1;

   struct tm t;
   t.tm_sec  = static_cast<int>(*sec);
   t.tm_min  = static_cast<int>(*min);
   t.tm_hour = static_cast<int>(*heure);
   t.tm_mday = static_cast<int>(*jour);
   t.tm_mon  = static_cast<int>(*mois-1);
   t.tm_year = static_cast<int>(*annee-1900);
   *date = static_cast<double>( gmt_i64::timegm(&t) );
   *date += static_cast<double>(*msc) / 1000000.0;

   return 0;
}

//************************************************************************
// Sommaire:   Convertis une date en temps local
//
// Description:
//    La fonction C_DH_D2CLCL(...) convertis une date à partir d'un double
//    en ses composantes.
//
// Entrée:
//    double* date                        La date
//
// Sortie:
//    int* annee, int* mois, int* jour
//    int* heure, int* min,  int* sec     Les composantes de la date
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_D2CLCL (fint_t* annee, fint_t* mois, fint_t* jour,
                                                           fint_t* heure, fint_t* min,  fint_t* sec, fint_t* msc,
                                                           double* date)
{
   time_t t = static_cast<time_t>(*date);
   struct tm const* tm = localtime(&t);

   *annee = tm->tm_year + 1900;
   *mois  = tm->tm_mon + 1;
   *jour  = tm->tm_mday;
   *heure = tm->tm_hour;
   *min   = tm->tm_min;
   *sec   = tm->tm_sec;
   *msc   = static_cast<int>((*date - t) * 1000000.0 + 0.5);

   return 0;
}

//************************************************************************
// Sommaire:   Convertis une date en temps UTC
//
// Description:
//    La fonction C_DH_D2CUTC(...) convertis une date à partir d'un double
//    en ses composantes.
//
// Entrée:
//    double* date                        La date
//
// Sortie:
//    int* annee, int* mois, int* jour
//    int* heure, int* min,  int* sec     Les composantes de la date
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_D2CUTC (fint_t* annee, fint_t* mois, fint_t* jour,
                                                           fint_t* heure, fint_t* min,  fint_t* sec, fint_t* msc,
                                                           double* date)
{
   gmt_i64::time_t t = static_cast<gmt_i64::time_t>(*date);
   struct tm const* tm = gmt_i64::gmtime(&t);

   *annee = tm->tm_year + 1900;
   *mois  = tm->tm_mon + 1;
   *jour  = tm->tm_mday;
   *heure = tm->tm_hour;
   *min   = tm->tm_min;
   *sec   = tm->tm_sec;
   *msc   = static_cast<int>((*date - t) * 1000000.0 + 0.5);

   return 0;
}

//************************************************************************
// Sommaire:   Ecris une date en temps local
//
// Description:
//    La fonction C_DH_ECRLCL(...) écris la date contenue dans le double date
//    dans la chaîne de caractères str sous le format AAAA-MM-JJ HH:MM:SS.
//    Elle retourne 0 en cas de succès et -1 en cas d'erreur.
//
// Entrée:
//    double* date  :      La date à écrire
//
// Sortie:
//    WatForStr* str:      Chaîne contenant la date écrite
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_ECRLCL (double* date,
                                                          F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_ECRLCL (double* date,
                                                          F2C_CONF_STRING str)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   if (l > 27)
   {
      char buf[20];
      time_t t  = static_cast<time_t>(*date);
      int    ms = static_cast<int>((*date - t) * 1000000.0 + 0.5);

      memset(sP, ' ', l);
      strftime(buf, 20, "%Y-%m-%d %H:%M:%S", localtime(&t));
      if (ms > 0)
         sprintf(sP, "%s.%06d", buf, ms);
      else
         sprintf(sP, "%s", buf);
      sP[strlen(sP)] = ' ';
      ierr = 0;
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Ecris une date en temps UTC
//
// Description:
//    La fonction C_DH_ECRUTC(...) écris la date contenue dans le double date
//    dans la chaîne de caractères str sous le format AAAA-MM-JJ HH:MM:SS.
//    Elle retourne 0 en cas de succès et -1 en cas d'erreur.
//
// Entrée:
//    double* date  :      La date à écrire
//
// Sortie:
//    WatForStr* str:      Chaîne contenant la date écrite
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_ECRUTC (double* date,
                                                          F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_ECRUTC (double* date,
                                                          F2C_CONF_STRING str)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   if (l > 27)
   {
      char buf[20];
      gmt_i64::time_t t = static_cast<gmt_i64::time_t>(*date);
      int    ms = static_cast<int>((*date - t) * 1000000.0 + 0.5);

      memset(sP, ' ', l);
      strftime(buf, 20, "%Y-%m-%d %H:%M:%S", gmt_i64::gmtime(&t));
      if (ms > 0)
         sprintf(sP, "%s.%06d", buf, ms);
      else
         sprintf(sP, "%s", buf);
      sP[strlen(sP)] = ' ';
      ierr = 0;
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Lis une date en temps local
//
// Description:
//    La fonction C_DH_LISUTC(...) lis une date de la chaîne str dans le format
//    AAAA-MM-JJ HH:MM:SS et la retourne sous forme du double date. La date
//    est interprétée comme date UTC.
//    La fonction retourne 0 en cas de succès et -1 en cas d'erreur.
//
// Entrée:
//    WatForStr* str:      Chaîne contenant la date à lire
//
// Sortie:
//    double* date:        Date lue
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_LISLCL(double* date,
                                                          F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_LISLCL(double* date,
                                                          F2C_CONF_STRING str)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   if (l < 19) return ierr;

   // ---  Setup the format
   std::string fmt;
   if (sP[10] == 'T')
      fmt = "%4d-%2d-%2dT%2d:%2d:%f";
   else if (sP[10] == ' ')
      fmt = "%4d-%2d-%2d %2d:%2d:%f";
   else
      return ierr;

   // ---  Read and control the fields
   std::string s(sP, l);
   int a, m, j, hh, mm;
   float tt;
   int ret = sscanf(s.c_str(), fmt.c_str(), &a, &m, &j, &hh, &mm, &tt);
   if (ret != 6) return ierr;
   if (a  < 0 || a  > 9999) return ierr;
   if (m  < 0 || m  > 12) return ierr;
   if (j  < 0 || j  > 31) return ierr;
   if (hh < 0 || hh > 24) return ierr;
   if (mm < 0 || mm > 60) return ierr;
   if (tt < 0.0) return ierr;

   int ss = static_cast<int>(tt);
   int ms = static_cast<int>( (tt-ss) * 1000000.0 + 0.5);

   struct tm t;
   t.tm_sec  = ss;
   t.tm_min  = mm;
   t.tm_hour = hh;
   t.tm_mday = j;
   t.tm_mon  = m-1;
   t.tm_year = a-1900;
   t.tm_isdst = 0;
   time_t dt = mktime(&t);
   *date = static_cast<double>(dt) + static_cast<double>(ms) / 1000000.0;
   ierr = 0;

   return ierr;
}

//************************************************************************
// Sommaire:   Lis une date en temps UTC
//
// Description:
//    La fonction C_DH_LISUTC(...) lis une date de la chaîne str dans le format
//    AAAA-MM-JJ HH:MM:SS et la retourne sous forme du double date. La date
//    est interprétée comme date UTC.
//    La fonction retourne 0 en cas de succès et -1 en cas d'erreur.
//
// Entrée:
//    WatForStr* str:      Chaîne contenant la date à lire
//
// Sortie:
//    double* date:        Date lue
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_LISUTC(double* date,
                                                          F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_LISUTC(double* date,
                                                          F2C_CONF_STRING str)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   if (l < 19) return ierr;

   // ---  Setup the format
   std::string fmt;
   if (sP[10] == 'T')
      fmt = "%4d-%2d-%2dT%2d:%2d:%f";
   else if (sP[10] == ' ')
      fmt = "%4d-%2d-%2d %2d:%2d:%f";
   else
      return ierr;
   if (sP[l-1] == 'Z')
      fmt += "Z";
   else
      fmt += "%d:%d";

   // ---  Read and control the fields
   std::string s(sP, l);
   int a, m, j, hh, mm, tzh = 0, tzm = 0;
   float tt;
   int ret = sscanf(s.c_str(), fmt.c_str(), &a, &m, &j, &hh, &mm, &tt, &tzh, &tzm);
   if (ret < 6) return ierr;
   if (a  < 0 || a  > 9999) return ierr;
   if (m  < 0 || m  > 12) return ierr;
   if (j  < 0 || j  > 31) return ierr;
   if (hh < 0 || hh > 24) return ierr;
   if (mm < 0 || mm > 60) return ierr;
   if (tt < 0.0) return ierr;

   if (tzh < 0) tzm = -tzm;
   int ss = static_cast<int>(tt);
   int ms = static_cast<int>( (tt-ss) * 1000000.0 + 0.5);

   struct tm t;
   t.tm_sec  = ss;
   t.tm_min  = mm + tzm;
   t.tm_hour = hh + tzh;
   t.tm_mday = j;
   t.tm_mon  = m-1;
   t.tm_year = a-1900;
   t.tm_isdst = 0;
   gmt_i64::time_t dt = gmt_i64::timegm(&t);
   *date = static_cast<double>(dt) + static_cast<double>(ms) / 1000000.0;
   ierr = 0;

   return ierr;
}

//************************************************************************
// Sommaire:   Date/Heure actuel
//
// Description:
//    La fonction C_DH_TIMLCL(...) retourne la date/heure actuelle sous la
//    forme du double date.
//
// Entrée:
//
// Sortie:
//    double* date      La date actuelle
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TIMLCL (double* date)
{
   time_t t = time(NULL);
   *date = static_cast<double>( mktime(localtime(&t)) );
   return 0;
}

//************************************************************************
// Sommaire:   Date/Heure actuel
//
// Description:
//    La fonction C_DH_TIMUTC(...) retourne la date/heure UTC sous la
//    forme du double date.
//
// Entrée:
//
// Sortie:
//    double* date      La date actuelle
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TIMUTC (double* date)
{
   time_t t = time(NULL);
   *date = static_cast<double>( gmt_i64::timegm(gmtime(&t)) );
   return 0;
}

//************************************************************************
// Sommaire:   Nombre de jours julien
//
// Description:
//    La fonction C_DH_JDN(...) retourne le nombre de jours Julien
//    (Julian day number), soit le nombre de jour écoulés depuis
//    12h Jan 1, 4713 BC.
//
// Entrée:
//    double* date      La date
//
// Sortie:
//
// Notes:
//    https://en.wikipedia.org/wiki/Julian_day
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_JDN (double* date)
{
   gmt_i64::time_t t = static_cast<gmt_i64::time_t>(*date);
   struct tm const* tm = gmt_i64::gmtime(&t);
   const int dd = tm->tm_mday;
   const int mm = tm->tm_mon + 1;
   const int yy = tm->tm_year + 1900;

   int a = (14-mm)/12;
   int y = yy + 4800 - a;
   int m = mm + 12*a - 3;
   fint_t jdn = dd + (153*m+2)/5 + 365*y + y/4 - y/100 + y/400 - 32045;

   return jdn;
}

//************************************************************************
// Sommaire: Démarre le chrono
//
// Description:
//    La fonction C_DH_TMRSTART(...) démarre le chrono.
//
// Entrée:
//
// Sortie:
//    double* tmr;         Le temps de démarrage du chrono
//
// Notes:
//    La fonction omp_get_wtime ne fonctionne pas sous SUNPRO
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TMRSTART (double* tmr)
{

#if defined(_OPENMP)
   if (omp_get_max_threads() > 1)
   {
      #if defined(H2D2_WINDOWS)
         *tmr = getWallTime();
      #elif defined(H2D2_UNIX)
         *tmr = getWallTime();
      #else
#        error Invalid operating system
      #endif
   }
   else
#endif
   {
#if defined(H2D2_WINDOWS)
////   *tmr = getCPUTime();
   *tmr = getWallTime();

#elif defined(H2D2_UNIX)
////   *tmr = getCPUTime();
   *tmr = getWallTime();

#else
#  error Invalid operating system
#endif
   } // else

   return 0;
}

//************************************************************************
// Sommaire: Retourne le temps écoulé depuis le démarrage du chrono
//
// Description:
//    La fonction C_DH_TMRDELT(...) retourne la différence entre le temps
//    actuel et le démarrage du chrono au temps donné par tmr.
//
// Entrée:
//    double* tmr;         Le temps de démarrage du chrono
//
// Sortie:
//    double* del;         Le temps écoulé depuis le démarrage du chrono
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TMRDELT (double* del,
                                                            double* tmr)
{
   double tmr2;
   C_DH_TMRSTART(&tmr2);
   *del = tmr2 - *tmr;
   return 0;
}

//************************************************************************
// Sommaire: Temps écoulé
//
// Description:
//    La fonction C_DH_TMRSTR(...) écris ce temps écoulé
//    dans la chaîne de caractères str sous le format JJ:HH:MM:SS.
//    Elle retourne 0 en cas de succès et -1 en cas d'erreur.
//
// Entrée:
//    double* tmr;         Le temps de démarrage du chrono
//
// Sortie:
//    WatForStr* str;      Chaîne contenant le temps écoulé
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TMRSTR (double* tmr,
                                                           F2C_CONF_STRING  str
                                                           F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TMRSTR (double* tmr,
                                                           F2C_CONF_STRING str)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   if (l > 26)
   {
      int    jours, heures, minutes;
      double delS = *tmr;
      jours = static_cast<int>(delS / (3600*24));
      delS -= jours * (3600*24);
      heures = static_cast<int>(delS / 3600);
      delS -= heures * 3600;
      minutes = static_cast<int>(delS / 60);
      delS -= minutes*60;

      memset(sP, ' ', l);
      sprintf(sP, "%d:%02d:%02d:%06.3f", jours, heures, minutes, delS);
      sP[strlen(sP)] = ' ';
      ierr = 0;
   }

   return ierr;
}

#ifdef C_DH_TEST
#include "assert.h"
void assert_tm(struct tm *t1, struct tm *t2)
{
   if ((t1->tm_sec == t2->tm_sec) &&
       (t1->tm_min == t2->tm_min) &&
       (t1->tm_hour== t2->tm_hour) &&
       (t1->tm_mday== t2->tm_mday) &&
       (t1->tm_mon == t2->tm_mon) &&
       (t1->tm_year== t2->tm_year)) return;

   printf("\n");
   printf("year : %i  %i\n", t1->tm_year,t2->tm_year);
   printf("month: %i  %i\n", t1->tm_mon, t2->tm_mon);
   printf("day  : %i  %i\n", t1->tm_mday,t2->tm_mday);
   printf("hour : %i  %i\n", t1->tm_hour,t2->tm_hour);
   printf("min  : %i  %i\n", t1->tm_min, t2->tm_min);
   printf("sec  : %i  %i\n", t1->tm_sec, t2->tm_sec);

   assert(t1->tm_sec == t2->tm_sec);
   assert(t1->tm_min == t2->tm_min);
   assert(t1->tm_hour== t2->tm_hour);
   assert(t1->tm_mday== t2->tm_mday);
   assert(t1->tm_mon == t2->tm_mon);
   assert(t1->tm_year== t2->tm_year);
}

int main()
{
   struct tm tm;
   tm.tm_sec  = 1;
   tm.tm_min  = 1;
   tm.tm_hour = 1;
   tm.tm_mday = 1;
   tm.tm_mon  = 0;
   tm.tm_year = 0;
   for (int y = 0; y <= 5000; ++y)
   {
      printf("\n%4i: ", y);
      tm.tm_year = y - gmt_i64::YEAR0;
      for (tm.tm_mon = 0; tm.tm_mon < 12; ++tm.tm_mon)
      {
         printf(".");
         for (tm.tm_mday = 1; tm.tm_mday < 29; ++tm.tm_mday)
         {
            gmt_i64::time_t t = gmt_i64::timegm(&tm);
            struct tm *t2;
            t2 = gmt_i64::gmtime(&t);
            assert_tm(&tm, t2);
            time_t tt = static_cast<time_t>(t);
            if (tt == t)
            {
               struct tm *t3 = gmtime(&tt);
               if (t3) assert_tm(t2, t3);
            }
         }
      }
   }
}
#endif   // ifdef C_DH_TEST
