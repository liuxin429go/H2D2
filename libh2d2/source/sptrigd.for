C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$C
C
C Functions:
C   Public:
C   Private:
C     SUBROUTINE DATAN2D
C     SUBROUTINE ATAN2D
C     SUBROUTINE NORM
C
C************************************************************************

      MODULE TRIGD_M

      IMPLICIT NONE

      INTEGER, PRIVATE, PARAMETER :: SP = KIND(1.0E0), DP = KIND(1.0D0)
      REAL(SP), PARAMETER :: DEGRAD_SP = 1.7453292519943295769E-2
      REAL(SP), PARAMETER :: RADDEG_SP = 5.7295779513082320877E+1
      REAL(DP), PARAMETER :: DEGRAD_DP = 1.7453292519943295769D-2
      REAL(DP), PARAMETER :: RADDEG_DP = 5.7295779513082320877D+1

      PUBLIC DEG2RAD, DDEG2RAD

      PUBLIC SIND, DSIND
      PUBLIC COSD, DCOSD
      PUBLIC TAND, DTAND

      PUBLIC ASIND, DASIND
      PUBLIC ACOSD, DACOSD
      PUBLIC ATAND, DATAND
      PUBLIC ATAN2D, DATAN2D

      INTERFACE DEG2RAD
         MODULE PROCEDURE DEG2RAD
         MODULE PROCEDURE DDEG2RAD
      END INTERFACE DEG2RAD

      INTERFACE SIND
         MODULE PROCEDURE SIND
         MODULE PROCEDURE DSIND
      END INTERFACE SIND

      INTERFACE COSD
         MODULE PROCEDURE COSD
         MODULE PROCEDURE DCOSD
      END INTERFACE COSD

      INTERFACE TAND
         MODULE PROCEDURE TAND
         MODULE PROCEDURE DTAND
      END INTERFACE TAND

      INTERFACE ASIND
         MODULE PROCEDURE ASIND
         MODULE PROCEDURE DASIND
      END INTERFACE ASIND

      INTERFACE ACOSD
         MODULE PROCEDURE ACOSD
         MODULE PROCEDURE DACOSD
      END INTERFACE ACOSD

      INTERFACE ATAND
         MODULE PROCEDURE ATAND
         MODULE PROCEDURE DATAND
      END INTERFACE ATAND

      INTERFACE ATAN2D
         MODULE PROCEDURE ATAN2D
         MODULE PROCEDURE DATAN2D
      END INTERFACE ATAN2D

      CONTAINS

C---------------------------------------------------------------------
C---     Normalise un angle entre [0-360[
C---------------------------------------------------------------------
      FUNCTION NORM(A)

      REAL(DP) :: NORM
      REAL(DP), INTENT(IN) :: A

      INTEGER M
C---------------------------------------------------------------------
      M = INT(A / 360.0D0)
      NORM = A - M*360.0D0
      IF (NORM .LT. 0.0D0) NORM = NORM + 360.0D0
      END FUNCTION NORM

C---------------------------------------------------------------------
C---     Angle de degré en radian
C---------------------------------------------------------------------
      FUNCTION DEG2RAD(A)
      REAL(SP) :: DEG2RAD
      REAL(SP), INTENT(IN) :: A
      DEG2RAD = (A * DEGRAD_SP)
      END FUNCTION DEG2RAD

      ELEMENTAL FUNCTION DDEG2RAD(A)
      REAL(DP) :: DDEG2RAD
      REAL(DP), INTENT(IN) :: A
      DDEG2RAD = (A * DEGRAD_DP)
      END FUNCTION DDEG2RAD

C---------------------------------------------------------------------
C---     SIN en degrés
C---------------------------------------------------------------------
      ELEMENTAL FUNCTION SIND(A)
      REAL(SP) :: SIND
      REAL(SP), INTENT(IN) :: A
      SIND = SIN(A * DEGRAD_SP)
      END FUNCTION SIND

      ELEMENTAL FUNCTION DSIND(A)
      REAL(DP) :: DSIND
      REAL(DP), INTENT(IN) :: A
      DSIND = DSIN(A * DEGRAD_DP)
      END FUNCTION DSIND

C---------------------------------------------------------------------
C---     COS en degrés
C---------------------------------------------------------------------
      ELEMENTAL FUNCTION COSD(A)
      REAL(SP) :: COSD
      REAL(SP), INTENT(IN) :: A
      COSD = COS(A * DEGRAD_SP)
      END FUNCTION COSD

      ELEMENTAL FUNCTION DCOSD(A)
      REAL(DP) :: DCOSD
      REAL(DP), INTENT(IN) :: A
      DCOSD = DCOS(A * DEGRAD_DP)
      END FUNCTION DCOSD

C---------------------------------------------------------------------
C---     TAN en degrés
C---------------------------------------------------------------------
      ELEMENTAL FUNCTION TAND(A)
      REAL(SP) :: TAND
      REAL(SP), INTENT(IN) :: A
      TAND = TAN(A * DEGRAD_SP)
      END FUNCTION TAND

      ELEMENTAL FUNCTION DTAND(A)
      REAL(DP) :: DTAND
      REAL(DP), INTENT(IN) :: A
      DTAND = DTAN(A * DEGRAD_DP)
      END FUNCTION DTAND

C---------------------------------------------------------------------
C---     ASIN en degrés
C---------------------------------------------------------------------
      ELEMENTAL FUNCTION ASIND(A)
      REAL(SP) :: ASIND
      REAL(SP), INTENT(IN) :: A
      ASIND = ASIN(A) * RADDEG_SP
      END FUNCTION ASIND

      ELEMENTAL FUNCTION DASIND(A)
      REAL(DP) :: DASIND
      REAL(DP), INTENT(IN) :: A
      DASIND = DASIN(A) * RADDEG_DP
      END FUNCTION DASIND

C---------------------------------------------------------------------
C---     ACOS en degrés
C---------------------------------------------------------------------
      ELEMENTAL FUNCTION ACOSD(A)
      REAL(SP) :: ACOSD
      REAL(SP), INTENT(IN) :: A
      ACOSD = ACOS(A) * RADDEG_SP
      END FUNCTION ACOSD

      ELEMENTAL FUNCTION DACOSD(A)
      REAL(DP) :: DACOSD
      REAL(DP), INTENT(IN) :: A
      DACOSD = DACOS(A) * RADDEG_DP
      END FUNCTION DACOSD

C---------------------------------------------------------------------
C---     ATAN en degrés
C---------------------------------------------------------------------
      ELEMENTAL FUNCTION ATAND(A)
      REAL(SP) :: ATAND
      REAL(SP), INTENT(IN) :: A
      ATAND = ATAN(A) * RADDEG_SP
      END FUNCTION ATAND

      ELEMENTAL FUNCTION DATAND(A)
      REAL(DP) :: DATAND
      REAL(DP), INTENT(IN) :: A
      DATAND = DATAN(A) * RADDEG_DP
      END FUNCTION DATAND

C---------------------------------------------------------------------
C---     ATAN2 en degrés
C---------------------------------------------------------------------
      FUNCTION ATAN2D(Y, X)
      REAL(SP) :: ATAN2D
      REAL(SP), INTENT(IN) :: Y, X
      ATAN2D = ATAN2(Y, X) * RADDEG_SP
      END FUNCTION ATAN2D

      FUNCTION DATAN2D(Y, X)
      REAL(DP) :: DATAN2D
      REAL(DP), INTENT(IN) :: Y, X
      DATAN2D = DATAN2(Y, X) * RADDEG_DP
      END FUNCTION DATAN2D

      END MODULE TRIGD_M
