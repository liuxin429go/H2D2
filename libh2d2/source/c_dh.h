//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Fonctions C de Date-Heure
//
// Description:
//    Le fichier c_dh.h
//
// Notes:
//
//************************************************************************
#ifndef C_DH_H_DEJA_INCLU
#define C_DH_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_DH_C2DLCL   F2C_CONF_DECOR_FNC(C_DH_C2DLCL,  c_dh_c2dlcl)
#define C_DH_C2DUTC   F2C_CONF_DECOR_FNC(C_DH_C2DUTC,  c_dh_c2dutc)
#define C_DH_D2CLCL   F2C_CONF_DECOR_FNC(C_DH_D2CLCL,  c_dh_d2clcl)
#define C_DH_D2CUTC   F2C_CONF_DECOR_FNC(C_DH_D2CUTC,  c_dh_d2cutc)
#define C_DH_ECRLCL   F2C_CONF_DECOR_FNC(C_DH_ECRLCL,  c_dh_ecrlcl)
#define C_DH_ECRUTC   F2C_CONF_DECOR_FNC(C_DH_ECRUTC,  c_dh_ecrutc)
#define C_DH_LISLCL   F2C_CONF_DECOR_FNC(C_DH_LISLCL,  c_dh_lislcl)
#define C_DH_LISUTC   F2C_CONF_DECOR_FNC(C_DH_LISUTC,  c_dh_lisutc)
#define C_DH_TIMLCL   F2C_CONF_DECOR_FNC(C_DH_TIMLCL,  c_dh_timlcl)
#define C_DH_TIMUTC   F2C_CONF_DECOR_FNC(C_DH_TIMUTC,  c_dh_timutc)
#define C_DH_JDN      F2C_CONF_DECOR_FNC(C_DH_JDN,     c_dh_jdn)
#define C_DH_TMRSTART F2C_CONF_DECOR_FNC(C_DH_TMRSTART,c_dh_tmrstart) 
#define C_DH_TMRDELT  F2C_CONF_DECOR_FNC(C_DH_TMRDELT, c_dh_tmrdelt)
#define C_DH_TMRSTR   F2C_CONF_DECOR_FNC(C_DH_TMRSTR,  c_dh_tmrstr)


F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_C2DLCL   (double*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*,  fint_t*,  fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_C2DUTC   (double*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*,  fint_t*,  fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_D2CLCL   (fint_t*, fint_t*, fint_t*, fint_t*, fint_t*,  fint_t*,  fint_t*, double*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_D2CUTC   (fint_t*, fint_t*, fint_t*, fint_t*, fint_t*,  fint_t*,  fint_t*, double*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_ECRLCL   (double*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_ECRUTC   (double*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_LISLCL   (double*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_LISUTC   (double*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TIMLCL   (double*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TIMUTC   (double*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_JDN      (double*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TMRSTART (double*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TMRDELT  (double*, double*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_DH_TMRSTR   (double*, F2C_CONF_STRING F2C_CONF_SUP_INT);


#ifdef __cplusplus
}
#endif

#endif   // C_DH_H_DEJA_INCLU

