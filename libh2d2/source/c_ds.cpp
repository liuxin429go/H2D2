//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Groupe:  fonctions C
// Sous-Groupe:  Data Structures
// Sommaire: Fonction C d'accès à des structures de données complexes (map, liste)
//************************************************************************
#include "c_ds.h"

#include <algorithm>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <string.h>

struct IVData
{
   void* p;
   int   i;
   IVData() : p(NULL), i(0) {}
   IVData(void* p_, int i_) : p(p_), i(i_) {}
};
typedef fint_t  TTMapIVKey;
typedef IVData  TTMapIVData;
typedef std::map<TTMapIVKey, TTMapIVData> TTMapIV;

typedef std::string TTMapSSKey;
typedef std::string TTMapSSData;
typedef std::map<TTMapSSKey, TTMapSSData> TTMapSS;

typedef std::string TTListData;
typedef std::vector<TTListData> TTList;

// ---  Redéfinition de std::min local à cause de MSVC win64
template<typename Tp> inline const Tp& std_min(const Tp& l, const Tp& r)
{
   return (l < r) ? l : r;
}

// ---  Using namespace à cause de MSVC win64
using namespace std;

#if defined(_MSC_VER)
#  pragma warning( push )
#  pragma warning( disable : 1195 )  // conversion from integer to smaller pointer
#  pragma warning( disable : 1684 )  // conversion from pointer to same-size integral type
#endif

//************************************************************************
// Sommaire:   Constructeur de map
//
// Description:
//    La fonction C_MIV_CTR() retourne un map nouvellement alloué. Il
//    est de la responsabilité de la méthode appelante de disposer de la
//    mémoire par un appel à C_MIV_DTR.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MIV_CTR()
{
   return reinterpret_cast<hndl_t>(new TTMapIV());
}

//************************************************************************
// Sommaire:   Destructeur de map.
//
// Description:
//    La fonction C_MIV_DTR(...) détruis le map dont le handle est passé
//    en argument.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_DTR(hndl_t* handle)
{
   delete reinterpret_cast<TTMapIV*>(*handle);
   return 0;
}

//************************************************************************
// Sommaire:   Assigne une valeur à un clef.
//
// Description:
//    La fonction C_MIV_ASGVAL assigne une valeur à la clef passée en argument.
//    Si la clef n'existe pas dans le map, elle est ajoutée.
//    En cas de succès on retourne 0, sinon -1.
//
// Entrée:
//    hndl_t*     handle;     Handle sur le map
//    fint_t*     key;        Clef
//    hndl_t*     val;        Pointeur aux données
//    fint_t*     ctr;        Compte de référence
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_ASGVAL (hndl_t* handle,
                                                            fint_t* key,
                                                            hndl_t* val,
                                                            fint_t* ctr)
{
   // ---  Assigne
   TTMapIV* mP = reinterpret_cast<TTMapIV*>(*handle);
   (*mP)[*key] = IVData(reinterpret_cast<void*>(*val), *ctr);

   return 0;
}

//************************************************************************
// Sommaire:   Efface une clef.
//
// Description:
//    La fonction C_MIV_EFFCLF efface du map la clef passée en argument. En
//    cas de succès on retourne 0. Si la clef n'existe pas, on retourne -1.
//
// Entrée:
//    hndl_t*     handle;     Handle sur le map
//    fint_t*     key;        Clef
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_EFFCLF(hndl_t* handle,
                                                           fint_t* key)
{
   // ---  Efface la clef
   fint_t ierr = -1;
   TTMapIV* mP = reinterpret_cast<TTMapIV*>(*handle);
   TTMapIV::iterator kI = (*mP).find(*key);
   if (kI != mP->end())
   {
      mP->erase(kI);
      ierr = 0;
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Itère sur le map.
//
// Description:
//    La fonction C_MIV_ITER d'itérer sur le map. En première entrée *iter
//    doit être 0, puis sa valeur est passée lors des appels successifs. 
//    En première entrée, la valeur de key est utilisée pour avancer 
//    rapidement dans le map.
//    Les itérations peuvent être stoppées en affectant à key la valeur -1.
//    <b>
//    La fonction retourne 0 en cas de succès et -1 en fin d'itération.
//
// Entrée:
//    hndl_t*     handle;     Handle sur le map
//    hndl_t*     iter;       On first entry, *iter = 0
//    fint_t*     key;        *key=min_value On first entry
//                            *key=-1        To notify EOI
//
// Sortie:
//    fint_t*     key;        Clef
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_ITER (hndl_t* handle,
                                                          hndl_t* iter,
                                                          fint_t* key)
{

   TTMapIV const*           mP  = reinterpret_cast<TTMapIV const*>(*handle);
   TTMapIV::const_iterator* kIP = reinterpret_cast<TTMapIV::const_iterator *>(*iter);

   // ---  Avance dans le map
   bool EOI = false;
   if (*iter == 0)
   {
      kIP = new TTMapIV::const_iterator(mP->begin());
      while (*kIP != mP->end() && (*kIP)->first < *key) (*kIP)++;
   }
   else if (*key == -1)
   {
      EOI = true;
   }
   else
   {
      if (*kIP != mP->end()) (*kIP)++;
      if (*kIP == mP->end()) EOI = true;
   }

   // ---  Copie la clef
   fint_t ierr = 0;
   if (EOI)
   {
      delete kIP;
      ierr  = -1;
      *key  = -1;
      *iter =  0;
   }
   else
   {
      ierr  =  0;
      *key  = (*kIP)->first;
      *iter = reinterpret_cast<hndl_t>(kIP);
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Retourne la clef à un indice.
//
// Description:
//    La fonction C_MIV_REQCLF retourne la clef à l'indice indx. Couplée à la
//    fonction C_MIV_REQDIM, elle permet d'itérer sur les clefs. La fonction
//    retourne 0 en cas de succès et -1 sinon.
//
// Entrée:
//    hndl_t*     handle;     Handle sur le map
//    fint_t*     indx;       Indice de la clef en num base 1
//
// Sortie:
//    fint_t*     key;        Clef
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_REQCLF (hndl_t* handle,
                                                            fint_t* indx,
                                                            fint_t* key)
{

   TTMapIV const* mP = reinterpret_cast<TTMapIV const*>(*handle);
   if (*indx < 0) return -1;
   if (*indx > static_cast<fint_t>(mP->size())) return -1;

   // ---  Avance dans le map
   TTMapIV::const_iterator kI = mP->begin();
   for (int i = 1; i < (*indx); ++i) ++kI;

   // ---  Copie la clef
   *key = (*kI).first;

   return 0;
}

//************************************************************************
// Sommaire:   Retourne la valeur associée à une clef.
//
// Description:
//    La fonction C_MIV_REQVAL retourne la valeur associée à la clef passée
//    en argument.
//
// Entrée:
//    hndl_t*     handle;     Handle sur le map
//    fint_t*     key;        Clef
//
// Sortie:
//    hndl_t*     val;        Pointeur aux données
//    fint_t*     ctr;        Compte de référence
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_REQVAL (hndl_t* handle,
                                                            fint_t* key,
                                                            hndl_t* val,
                                                            fint_t* ctr)
{
   // ---  Avance dans le map
   TTMapIV const* mP = reinterpret_cast<TTMapIV const*>(*handle);
   TTMapIV::const_iterator kI = mP->find(*key);

   // ---  Copie la valeur
   fint_t ierr = -1;
   if (kI != mP->end())
   {
      const IVData& d = (*kI).second;
      *val = reinterpret_cast<hndl_t>(d.p);
      *ctr = d.i;
      ierr = 0;
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Dimension du map.
//
// Description:
//    La fonction C_MIV_REQDIM(...) retourne la dimension du map dont le
//    handle est passé en argument.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_REQDIM(hndl_t* handle)
{
   TTMapIV const* mP = reinterpret_cast<TTMapIV const*>(*handle);
   return static_cast<fint_t>(mP->size());
}

//************************************************************************
// Sommaire:   Retourne 0 si la clef existe.
//
// Description:
//    La fonction C_MIV_ESTCLF test si un clef existe. Elle retourne 0 si
//    la clef existe et -1 sinon.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_ESTCLF (hndl_t* handle,
                                                            fint_t* key)
{
   // ---  Avance dans le map
   TTMapIV const* mP = reinterpret_cast<TTMapIV const*>(*handle);
   TTMapIV::const_iterator kI = mP->find(*key);

   return (kI == mP->end()) ? -1 : 0;
}

//************************************************************************
// Sommaire:   Constructeur de map
//
// Description:
//    La fonction C_MAP_CTR() retourne un map nouvellement alloué. Il
//    est de la responsabilité de la méthode appelante de disposer de la
//    mémoire par un appel à C_MAP_DTR.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MAP_CTR()
{
   return reinterpret_cast<hndl_t>(new TTMapSS());
}

//************************************************************************
// Sommaire:   Destructeur de map.
//
// Description:
//    La fonction C_MAP_DTR(...) détruis le map dont le handle est passé
//    en argument.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_DTR(hndl_t* handle)
{
   delete reinterpret_cast<TTMapSS*>(*handle);
   return 0;
}

//************************************************************************
// Sommaire:   Assigne une valeur à un clef.
//
// Description:
//    La fonction C_MAP_ASGVAL assigne une valeur à la clef passée en argument.
//    Si la clef n'existe pas dans le map, elle est ajoutée.
//    En cas de succès on retourne 0, sinon -1.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_ASGVAL (hndl_t* handle,
                                                            F2C_CONF_STRING  key,
                                                            F2C_CONF_STRING  val
                                                            F2C_CONF_SUP_INT klen
                                                            F2C_CONF_SUP_INT vlen)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_ASGVAL (hndl_t* handle,
                                                            F2C_CONF_STRING  key,
                                                            F2C_CONF_STRING  val)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* kP = key;
   int   kl = klen;
   char* vP = val;
   int   vl = vlen;
#else
   char* kP = key->strP;
   int   kl = key->len;
   char* vP = val->strP;
   int   vl = val->len;
#endif

   // ---  Transforme en chaînes C
   string k(kP, kl);
   string v(vP, vl);

   // ---  Assigne
   TTMapSS* mP = reinterpret_cast<TTMapSS*>(*handle);
   (*mP)[k] = v;

   return 0;
}

//************************************************************************
// Sommaire:   Efface une clef.
//
// Description:
//    La fonction C_MAP_EFFCLF efface du map la clef passée en argument. En
//    cas de succès on retourne 0. Si la clef n'existe pas, on retourne -1.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_EFFCLF(hndl_t* handle,
                                                           F2C_CONF_STRING  key
                                                           F2C_CONF_SUP_INT klen)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_EFFCLF(hndl_t* handle,
                                                           F2C_CONF_STRING  key)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* kP = key;
   int   kl = klen;
#else
   char* kP = key->strP;
   int   kl = key->len;
#endif

   // ---  Transforme en chaîne C
   string k(kP, kl);

   // ---  Efface la clef
   fint_t ierr = -1;
   TTMapSS* mP = reinterpret_cast<TTMapSS*>(*handle);
   TTMapSS::iterator kI = (*mP).find(k.c_str());
   if (kI != mP->end())
   {
      mP->erase(kI);
      ierr = 0;
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Retourne la clef à un indice.
//
// Description:
//    La fonction C_MAP_REQCLF retourne la clef à l'indice indx. Couplée à la
//    fonction C_MAP_REQDIM, elle permet d'itérer sur les clefs.
//    La fonction retourne:
//        0 en cas de succès
//       -1 si l'indice est invalide
//       -2 si la taille de key est trop petite 
//
// Entrée:
//    hndl_t*      handle;       Handle sur le map
//    fint_t*      indx;         Indice de la clef en num base 1
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_REQCLF (hndl_t* handle,
                                                            fint_t* indx,
                                                            F2C_CONF_STRING  key
                                                            F2C_CONF_SUP_INT klen)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_REQCLF (hndl_t* handle,
                                                            fint_t* indx,
                                                            F2C_CONF_STRING  key)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* kP = key;
   int   kl = klen;
#else
   char* kP = key->strP;
   int   kl = key->len;
#endif

   TTMapSS const* mP = reinterpret_cast<TTMapSS const*>(*handle);
   if (*indx < 0) return -1;
   if (*indx > static_cast<fint_t>(mP->size())) return -1;

   // ---  Avance dans le map
   TTMapSS::const_iterator kI = mP->begin();
   for (int i = 1; i < (*indx); ++i) ++kI;

   // ---  Copie la clef
   fint_t ierr = -2;
   if (static_cast<TTMapSSKey::size_type>(kl) >= (*kI).first.size())
   {
      memset(kP, ' ', kl);
      memcpy(kP, (*kI).first.c_str(), (*kI).first.size());
      ierr = 0;
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Retourne la valeur associée à une clef.
//
// Description:
//    La fonction C_MAP_REQVAL retourne la valeur associée à la clef passée
//    en argument.
//    La fonction retourne:
//        0 en cas de succès
//       -1 si la clef key est invalide
//       -2 si la taille de val est trop petite 
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_REQVAL (hndl_t* handle,
                                                            F2C_CONF_STRING  key,
                                                            F2C_CONF_STRING  val
                                                            F2C_CONF_SUP_INT klen
                                                            F2C_CONF_SUP_INT vlen)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_REQVAL (hndl_t* handle,
                                                            F2C_CONF_STRING  key,
                                                            F2C_CONF_STRING  val)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* kP = key;
   int   kl = klen;
   char* vP = val;
   int   vl = vlen;
#else
   char* kP = key->strP;
   int   kl = key->len;
   char* vP = val->strP;
   int   vl = val->len;
#endif

   // ---  Transforme en chaîne C
   string k(kP, kl);

   // ---  Avance dans le map
   TTMapSS const* mP = reinterpret_cast<TTMapSS const*>(*handle);
   TTMapSS::const_iterator kI = mP->find(k.c_str());

   // ---  Copie la valeur
   fint_t ierr = -1;
   if (kI != mP->end())
   {
      ierr = -2;
      if (static_cast<TTMapSSData::size_type>(vl) >= (*kI).second.size())
      {
         memset(vP, ' ', vl);
         memcpy(vP, (*kI).second.c_str(), (*kI).second.size());
         ierr = 0;
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Dimension du map.
//
// Description:
//    La fonction C_MAP_REQDIM(...) retourne la dimension du map dont le
//    handle est passé en argument.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_REQDIM(hndl_t* handle)
{
   TTMapSS const* mP = reinterpret_cast<TTMapSS const*>(*handle);
   return static_cast<fint_t>(mP->size());
}

//************************************************************************
// Sommaire:   Retourne 0 si la clef existe.
//
// Description:
//    La fonction C_MAP_ESTCLF test si un clef existe. Elle retourne 0 si
//    la clef existe et -1 sinon.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_ESTCLF (hndl_t* handle,
                                                            F2C_CONF_STRING  key
                                                            F2C_CONF_SUP_INT klen)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_ESTCLF (hndl_t* handle,
                                                            F2C_CONF_STRING  key)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* kP = key;
   int   kl = klen;
#else
   char* kP = key->strP;
   int   kl = key->len;
#endif

   // ---  Transforme en chaîne C
   string k(kP, kl);

   // ---  Avance dans le map
   TTMapSS const* mP = reinterpret_cast<TTMapSS const*>(*handle);
   TTMapSS::const_iterator kI = mP->find(k.c_str());

   return (kI == mP->end()) ? -1 : 0;
}

//************************************************************************
// Sommaire:   Constructeur de liste
//
// Description:
//    La fonction C_LST_CTR() retourne une liste nouvellement allouée. Il
//    est de la responsabilité de la méthode appelante de disposer de la
//    mémoire par un appel à C_LST_DTR.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_LST_CTR()
{
   return reinterpret_cast<hndl_t>(new TTList());
}

//************************************************************************
// Sommaire:   Destructeur de liste.
//
// Description:
//    La fonction C_LST_DTR(...) détruis la liste dont le handle est passé
//    en argument.
//
// Entrée:
//    hndl_t*      handle;      Handle sur la liste
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_DTR(hndl_t* handle)
{
   delete reinterpret_cast<TTList*>(*handle);
   return 0;
}

//************************************************************************
// Sommaire:   Ajoute une valeur à la liste.
//
// Description:
//    La fonction C_LST_AJTVAL ajoute une valeur à la fin de la liste.
//    En cas de succès on retourne 0, sinon -1.
//
// Entrée:
//    hndl_t*           handle;      Handle sur la liste
//    F2C_CONF_STRING   val;         La valeur à ajouter
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_AJTVAL (hndl_t* handle,
                                                            F2C_CONF_STRING  val
                                                            F2C_CONF_SUP_INT vlen)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_AJTVAL (hndl_t* handle,
                                                            F2C_CONF_STRING  val)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* vP = val;
   int   vl = vlen;
#else
   char* vP = val->strP;
   int   vl = val->len;
#endif

   // ---  Transforme en chaîne C
   string v(vP, vl);

   // ---  Assigne
   TTList* lP = reinterpret_cast<TTList*>(*handle);
   (*lP).push_back(v);

   return 0;
}

//************************************************************************
// Sommaire:   Assigne une valeur à un indice.
//
// Description:
//    La fonction C_LST_ASGVAL assigne une nouvelle valeur à l'indice passé
//    en argument.
//    En cas de succès on retourne 0, sinon -1.
//
// Entrée:
//    hndl_t*           handle;      Handle sur la liste
//    fint_t*           i;           L'indice à modifier (en base 1)
//    F2C_CONF_STRING   val;         La nouvelle valeur
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_ASGVAL (hndl_t* handle,
                                                            fint_t* i,
                                                            F2C_CONF_STRING  val
                                                            F2C_CONF_SUP_INT vlen)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_ASGVAL (hndl_t* handle,
                                                            fint_t* i,
                                                            F2C_CONF_STRING  val)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* vP = val;
   int   vl = vlen;
#else
   char* vP = val->strP;
   int   vl = val->len;
#endif

   fint_t ierr = -1;

   TTList* lP = reinterpret_cast<TTList*>(*handle);
   if (*i >= 1)
   {
      TTList::size_type ii = *i;
      if (ii <= (*lP).size())
      {
         --ii;
         // ---  Transforme en chaînes C
         string v(vP, vl);
         // ---  Assigne
         (*lP)[ii] = v;
         ierr = 0;
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Vide la liste.
//
// Description:
//    La fonction C_LST_CLR vide la liste.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_CLR(hndl_t* handle)
{

   TTList* lP = reinterpret_cast<TTList*>(*handle);
   (*lP).clear();

   return 0;
}

//************************************************************************
// Sommaire:   Efface un indice.
//
// Description:
//    La fonction C_LST_EFFVAL efface de la liste l'indice passé en argument.
//    En cas de succès on retourne 0. Si l'indice est invalide, on retourne -1.
//
// Entrée:
//    hndl_t*      handle;      Handle sur le map
//    fint_t*      i;           L'indice à effacer (en base 1)
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_EFFVAL(hndl_t* handle,
                                                           fint_t* i)
{
   fint_t ierr = -1;

   TTList* lP = reinterpret_cast<TTList*>(*handle);
   if (*i >= 1)
   {
      TTList::size_type ii = *i;
      if (ii <= (*lP).size())
      {
         --ii;
         (*lP).erase( (*lP).begin() + ii);
         ierr = 0;
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Retourne la valeur à un indice.
//
// Description:
//    La fonction C_LST_REQVAL retourne la valeur à l'indice passé
//    en argument.
//    La fonction retourne:
//        0 en cas de succès
//       -1 si l'indice est invalide
//       -2 si la taille de val est trop petite 
//
// Entrée:
//    hndl_t*           handle;      Handle sur le map
//    fint_t*           i;           L'indice (en base 1)
//
// Sortie:
//    F2C_CONF_STRING   val;         La valeur
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_REQVAL (hndl_t* handle,
                                                            fint_t* i,
                                                            F2C_CONF_STRING  val
                                                            F2C_CONF_SUP_INT vlen)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_REQVAL (hndl_t* handle,
                                                            fint_t* i,
                                                            F2C_CONF_STRING  val)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* vP = val;
   int   vl = vlen;
#else
   char* vP = val->strP;
   int   vl = val->len;
#endif

   fint_t ierr = -1;

   // ---  Copie la valeur
   TTList const* lP = reinterpret_cast<TTList const*>(*handle);
   if (*i >= 1)
   {
      TTList::size_type ii = *i;
      if (ii <= (*lP).size())
      {
         --ii;
         ierr = -2;
         fint_t l1 = vl;
         fint_t l2 = static_cast<fint_t>( (*lP)[ii].size() );
         if (l1 >= l2)
         {
            memset(vP, ' ', vl);
            memcpy(vP, (*lP)[ii].c_str(), l2);
            ierr = 0;
         }
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Retourne la valeur à un indice.
//
// Description:
//    La fonction C_LST_TRVVAL retourne l'indice de la valeur passée
//    en argument.
//    En cas de succès on retourne 0, sinon -1.
//
// Entrée:
//    hndl_t*           handle;      Handle sur le map
//    F2C_CONF_STRING   val;         La valeur recherchée
//
// Sortie:
//    fint_t*           i;           L'indice (en base 1)
//
// Notes:
//  Sun utilise par défaut une "vielle" librairie STL de RogueWave qui
//  ne supporte pas std:distance dans sa version standard.
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_TRVVAL (hndl_t* handle,
                                                            fint_t* i,
                                                            F2C_CONF_STRING  val
                                                            F2C_CONF_SUP_INT vlen)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_TRVVAL (hndl_t* handle,
                                                            fint_t* i,
                                                            F2C_CONF_STRING  val)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* vP = val;
   int   vl = vlen;
#else
   char* vP = val->strP;
   int   vl = val->len;
#endif

   fint_t ierr = -1;

   // ---  Copie la valeur
   TTList const* lP = reinterpret_cast<TTList const*>(*handle);
   if (vl > 0)
   {
      string v(vP, vl);
      TTList::const_iterator vI = std::find((*lP).begin(), (*lP).end(), v);
      if (vI != (*lP).end())
      {
         TTList::difference_type ii;
#if defined(H2D2_CMPLR_SUN) && (H2D2_CMPLR_VERS <= 0x5130)
         std::distance((*lP).begin(), vI, ii);
#else
         ii = std::distance((*lP).begin(), vI);
#endif
         *i = static_cast<fint_t>(++ii);
         ierr = 0;
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire:   Dimension de la liste.
//
// Description:
//    La fonction C_LST_REQDIM(...) retourne la dimension de la liste dont le
//    handle est passé en argument.
//
// Entrée:
//    hndl_t*      handle;      Handle sur la liste
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_REQDIM(hndl_t* handle)
{
   TTList const* lP = reinterpret_cast<TTList const*>(*handle);
   return static_cast<fint_t>(lP->size());
}

#if defined(_MSC_VER)
#  pragma warning( pop )
#endif
