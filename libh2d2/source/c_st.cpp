//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Groupe:  fonctions C
// Sous-Groupe:  Service de Traduction
// Sommaire: Fonctions en C du service de traduction
//************************************************************************
#include "c_st.h"
#include <cstdint>

/* crc32,  efg, 3/12/95

   Calculate a CRC-32 for each file, and a composite CRC-32 for a
   set of files.

   Copyright (C) 1995, 1998, Earl F. Glynn, Overland Park, KS  66214-3057.

   The composite CRC-32 is not dependent on the endian type of the
   machine executing the program.  This means the composite CRC-32
   can be used to test the transfer of a set of files, when
   transferred in binary mode, between machines of different
   architecture.

   Adapted from charcnt.c program and crc16.u unit and modified
   to include CRC32 table from Microsoft Systems Journal (MSJ).

   "crc32" gives the same values as the PKZIP utility.  After zipping a
   set of files, use "PKZIP -V filename" to view the CRC-32 values.

   "crc32" has been verified using
     - Borland C/C++ 4.02 under DOS 6.22 (both 16- and 32-bit compilers)
     - Watcom C 9.5 under QNX 4.21
     - Sun C++ under 4.1.1

   Borland C++:   Link with WILDARGS.OBJ for command-line expansion
                  of file specifications.
                  bcc crc32.c \bc4\lib\16bit\wildargs.obj
   Watcom/Sun C:  cc crc32.c -o crc32

   Verify correct operation of "crc32," by generating 256 test files
   using the program "tstcrc32" (all the BIN files would have exactly
   the same checksum). Create the CRCs for these BIN files using
   the following:  CRC32 *.BIN

   The overall expected result for these 256 files is that each
   CRC matches one created by PKZIP (see below), and the summary
   line shows 65536 total bytes with a CRC of a4527e20.

   Create a PKZIP file of the same BIN files:  PKZIP -a CRCBIN *.BIN
   Look at the CRC32s from PKZIP:  PKZIP -v CRCBIN

   The CRCs from CRC32 should match those created by PKZIP.

   2/17/98:  Added verification option.  Changed composite CRC to be
   on 8-character hex CRC-32s instead of internal 4-byte integer values
   that are endian sensitive.

*/


/**  data  ************************************************************/

/* Table used for byte-wise calculation of CRC 32. */
/* MSJ,  March 1995,  pp. 107-108 */
  static const unsigned long CRC32_TABLE[256] =
  {
    0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
    0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
    0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
    0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
    0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
    0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
    0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
    0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
    0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
    0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
    0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
    0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
    0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
    0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
    0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
    0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,

    0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
    0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
    0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
    0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
    0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
    0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
    0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
    0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
    0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
    0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
    0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
    0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
    0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
    0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
    0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
    0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,

    0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
    0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
    0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
    0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
    0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
    0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
    0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
    0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
    0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
    0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
    0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
    0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
    0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
    0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
    0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
    0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,

    0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
    0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
    0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
    0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
    0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
    0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
    0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
    0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
    0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
    0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
    0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
    0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
    0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
    0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
    0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
    0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D,
  };


  /*The following macro is a little cryptic (but executes very quickly).
    The algorithm is as follows:
      1.  exclusive-or the input byte with the low-order byte of
          the CRC "register" to get an INDEX
      2.  shift the CRC "register" eight bits to the right
      3.  exclusive-or the CRC register with the contents of
          Table[INDEX]
      4.  repeat steps 1 through 3 for all bytes
  */

#define CALC_CRC32(CRC32, INPUT)  \
    CRC32 = ((CRC32) >> 8) ^ CRC32_TABLE[ (INPUT) ^ ((CRC32) & 0x000000FF) ]

void crc32(unsigned char const* bP, size_t len, unsigned int* crc)
{
   unsigned long res = 0xFFFFFFFF;
   for (size_t i = 0; i < len; ++i)
   {
      CALC_CRC32(res, (unsigned char) bP[i]);
   }
   res = ~res;

   *crc = res;
}


//========================================================================
//========================================================================
//========================================================================
//========================================================================

// Portions Copyright (c) 1996-2001, PostgreSQL Global Development Group
// (Any use permitted, subject to terms of PostgreSQL license; see.)

// If we have a 64-bit integer type, then a 64-bit CRC looks just like the
// usual sort of implementation. (See Ross Williams' excellent introduction
// A PAINLESS GUIDE TO CRC ERROR DETECTION ALGORITHMS, available from
// ftp://ftp.rocksoft.com/papers/crc_v3.txt or several other net sites.)
// If we have no working 64-bit type, then fake it with two 32-bit registers.
//
// The present implementation is a normal (not "reflected", in Williams'
// terms) 64-bit CRC, using initial all-ones register contents and a final
// bit inversion. The chosen polynomial is borrowed from the DLT1 spec
// (ECMA-182, available from http://www.ecma.ch/ecma1/STAND/ECMA-182.HTM):
//
// x^64 + x^62 + x^57 + x^55 + x^54 + x^53 + x^52 + x^47 + x^46 + x^45 +
// x^40 + x^39 + x^38 + x^37 + x^35 + x^33 + x^32 + x^31 + x^29 + x^27 +
// x^24 + x^23 + x^22 + x^21 + x^19 + x^17 + x^13 + x^12 + x^10 + x^9 +
// x^7 + x^4 + x + 1

#define INT64CONST(x) UINT64_C(x)

// Constant table for CRC calculation
const uint64_t crc_table[256] = {
  INT64CONST(0x0000000000000000), INT64CONST(0x42F0E1EBA9EA3693),
  INT64CONST(0x85E1C3D753D46D26), INT64CONST(0xC711223CFA3E5BB5),
  INT64CONST(0x493366450E42ECDF), INT64CONST(0x0BC387AEA7A8DA4C),
  INT64CONST(0xCCD2A5925D9681F9), INT64CONST(0x8E224479F47CB76A),
  INT64CONST(0x9266CC8A1C85D9BE), INT64CONST(0xD0962D61B56FEF2D),
  INT64CONST(0x17870F5D4F51B498), INT64CONST(0x5577EEB6E6BB820B),
  INT64CONST(0xDB55AACF12C73561), INT64CONST(0x99A54B24BB2D03F2),
  INT64CONST(0x5EB4691841135847), INT64CONST(0x1C4488F3E8F96ED4),
  INT64CONST(0x663D78FF90E185EF), INT64CONST(0x24CD9914390BB37C),
  INT64CONST(0xE3DCBB28C335E8C9), INT64CONST(0xA12C5AC36ADFDE5A),
  INT64CONST(0x2F0E1EBA9EA36930), INT64CONST(0x6DFEFF5137495FA3),
  INT64CONST(0xAAEFDD6DCD770416), INT64CONST(0xE81F3C86649D3285),
  INT64CONST(0xF45BB4758C645C51), INT64CONST(0xB6AB559E258E6AC2),
  INT64CONST(0x71BA77A2DFB03177), INT64CONST(0x334A9649765A07E4),
  INT64CONST(0xBD68D2308226B08E), INT64CONST(0xFF9833DB2BCC861D),
  INT64CONST(0x388911E7D1F2DDA8), INT64CONST(0x7A79F00C7818EB3B),
  INT64CONST(0xCC7AF1FF21C30BDE), INT64CONST(0x8E8A101488293D4D),
  INT64CONST(0x499B3228721766F8), INT64CONST(0x0B6BD3C3DBFD506B),
  INT64CONST(0x854997BA2F81E701), INT64CONST(0xC7B97651866BD192),
  INT64CONST(0x00A8546D7C558A27), INT64CONST(0x4258B586D5BFBCB4),
  INT64CONST(0x5E1C3D753D46D260), INT64CONST(0x1CECDC9E94ACE4F3),
  INT64CONST(0xDBFDFEA26E92BF46), INT64CONST(0x990D1F49C77889D5),
  INT64CONST(0x172F5B3033043EBF), INT64CONST(0x55DFBADB9AEE082C),
  INT64CONST(0x92CE98E760D05399), INT64CONST(0xD03E790CC93A650A),
  INT64CONST(0xAA478900B1228E31), INT64CONST(0xE8B768EB18C8B8A2),
  INT64CONST(0x2FA64AD7E2F6E317), INT64CONST(0x6D56AB3C4B1CD584),
  INT64CONST(0xE374EF45BF6062EE), INT64CONST(0xA1840EAE168A547D),
  INT64CONST(0x66952C92ECB40FC8), INT64CONST(0x2465CD79455E395B),
  INT64CONST(0x3821458AADA7578F), INT64CONST(0x7AD1A461044D611C),
  INT64CONST(0xBDC0865DFE733AA9), INT64CONST(0xFF3067B657990C3A),
  INT64CONST(0x711223CFA3E5BB50), INT64CONST(0x33E2C2240A0F8DC3),
  INT64CONST(0xF4F3E018F031D676), INT64CONST(0xB60301F359DBE0E5),
  INT64CONST(0xDA050215EA6C212F), INT64CONST(0x98F5E3FE438617BC),
  INT64CONST(0x5FE4C1C2B9B84C09), INT64CONST(0x1D14202910527A9A),
  INT64CONST(0x93366450E42ECDF0), INT64CONST(0xD1C685BB4DC4FB63),
  INT64CONST(0x16D7A787B7FAA0D6), INT64CONST(0x5427466C1E109645),
  INT64CONST(0x4863CE9FF6E9F891), INT64CONST(0x0A932F745F03CE02),
  INT64CONST(0xCD820D48A53D95B7), INT64CONST(0x8F72ECA30CD7A324),
  INT64CONST(0x0150A8DAF8AB144E), INT64CONST(0x43A04931514122DD),
  INT64CONST(0x84B16B0DAB7F7968), INT64CONST(0xC6418AE602954FFB),
  INT64CONST(0xBC387AEA7A8DA4C0), INT64CONST(0xFEC89B01D3679253),
  INT64CONST(0x39D9B93D2959C9E6), INT64CONST(0x7B2958D680B3FF75),
  INT64CONST(0xF50B1CAF74CF481F), INT64CONST(0xB7FBFD44DD257E8C),
  INT64CONST(0x70EADF78271B2539), INT64CONST(0x321A3E938EF113AA),
  INT64CONST(0x2E5EB66066087D7E), INT64CONST(0x6CAE578BCFE24BED),
  INT64CONST(0xABBF75B735DC1058), INT64CONST(0xE94F945C9C3626CB),
  INT64CONST(0x676DD025684A91A1), INT64CONST(0x259D31CEC1A0A732),
  INT64CONST(0xE28C13F23B9EFC87), INT64CONST(0xA07CF2199274CA14),
  INT64CONST(0x167FF3EACBAF2AF1), INT64CONST(0x548F120162451C62),
  INT64CONST(0x939E303D987B47D7), INT64CONST(0xD16ED1D631917144),
  INT64CONST(0x5F4C95AFC5EDC62E), INT64CONST(0x1DBC74446C07F0BD),
  INT64CONST(0xDAAD56789639AB08), INT64CONST(0x985DB7933FD39D9B),
  INT64CONST(0x84193F60D72AF34F), INT64CONST(0xC6E9DE8B7EC0C5DC),
  INT64CONST(0x01F8FCB784FE9E69), INT64CONST(0x43081D5C2D14A8FA),
  INT64CONST(0xCD2A5925D9681F90), INT64CONST(0x8FDAB8CE70822903),
  INT64CONST(0x48CB9AF28ABC72B6), INT64CONST(0x0A3B7B1923564425),
  INT64CONST(0x70428B155B4EAF1E), INT64CONST(0x32B26AFEF2A4998D),
  INT64CONST(0xF5A348C2089AC238), INT64CONST(0xB753A929A170F4AB),
  INT64CONST(0x3971ED50550C43C1), INT64CONST(0x7B810CBBFCE67552),
  INT64CONST(0xBC902E8706D82EE7), INT64CONST(0xFE60CF6CAF321874),
  INT64CONST(0xE224479F47CB76A0), INT64CONST(0xA0D4A674EE214033),
  INT64CONST(0x67C58448141F1B86), INT64CONST(0x253565A3BDF52D15),
  INT64CONST(0xAB1721DA49899A7F), INT64CONST(0xE9E7C031E063ACEC),
  INT64CONST(0x2EF6E20D1A5DF759), INT64CONST(0x6C0603E6B3B7C1CA),
  INT64CONST(0xF6FAE5C07D3274CD), INT64CONST(0xB40A042BD4D8425E),
  INT64CONST(0x731B26172EE619EB), INT64CONST(0x31EBC7FC870C2F78),
  INT64CONST(0xBFC9838573709812), INT64CONST(0xFD39626EDA9AAE81),
  INT64CONST(0x3A28405220A4F534), INT64CONST(0x78D8A1B9894EC3A7),
  INT64CONST(0x649C294A61B7AD73), INT64CONST(0x266CC8A1C85D9BE0),
  INT64CONST(0xE17DEA9D3263C055), INT64CONST(0xA38D0B769B89F6C6),
  INT64CONST(0x2DAF4F0F6FF541AC), INT64CONST(0x6F5FAEE4C61F773F),
  INT64CONST(0xA84E8CD83C212C8A), INT64CONST(0xEABE6D3395CB1A19),
  INT64CONST(0x90C79D3FEDD3F122), INT64CONST(0xD2377CD44439C7B1),
  INT64CONST(0x15265EE8BE079C04), INT64CONST(0x57D6BF0317EDAA97),
  INT64CONST(0xD9F4FB7AE3911DFD), INT64CONST(0x9B041A914A7B2B6E),
  INT64CONST(0x5C1538ADB04570DB), INT64CONST(0x1EE5D94619AF4648),
  INT64CONST(0x02A151B5F156289C), INT64CONST(0x4051B05E58BC1E0F),
  INT64CONST(0x87409262A28245BA), INT64CONST(0xC5B073890B687329),
  INT64CONST(0x4B9237F0FF14C443), INT64CONST(0x0962D61B56FEF2D0),
  INT64CONST(0xCE73F427ACC0A965), INT64CONST(0x8C8315CC052A9FF6),
  INT64CONST(0x3A80143F5CF17F13), INT64CONST(0x7870F5D4F51B4980),
  INT64CONST(0xBF61D7E80F251235), INT64CONST(0xFD913603A6CF24A6),
  INT64CONST(0x73B3727A52B393CC), INT64CONST(0x31439391FB59A55F),
  INT64CONST(0xF652B1AD0167FEEA), INT64CONST(0xB4A25046A88DC879),
  INT64CONST(0xA8E6D8B54074A6AD), INT64CONST(0xEA16395EE99E903E),
  INT64CONST(0x2D071B6213A0CB8B), INT64CONST(0x6FF7FA89BA4AFD18),
  INT64CONST(0xE1D5BEF04E364A72), INT64CONST(0xA3255F1BE7DC7CE1),
  INT64CONST(0x64347D271DE22754), INT64CONST(0x26C49CCCB40811C7),
  INT64CONST(0x5CBD6CC0CC10FAFC), INT64CONST(0x1E4D8D2B65FACC6F),
  INT64CONST(0xD95CAF179FC497DA), INT64CONST(0x9BAC4EFC362EA149),
  INT64CONST(0x158E0A85C2521623), INT64CONST(0x577EEB6E6BB820B0),
  INT64CONST(0x906FC95291867B05), INT64CONST(0xD29F28B9386C4D96),
  INT64CONST(0xCEDBA04AD0952342), INT64CONST(0x8C2B41A1797F15D1),
  INT64CONST(0x4B3A639D83414E64), INT64CONST(0x09CA82762AAB78F7),
  INT64CONST(0x87E8C60FDED7CF9D), INT64CONST(0xC51827E4773DF90E),
  INT64CONST(0x020905D88D03A2BB), INT64CONST(0x40F9E43324E99428),
  INT64CONST(0x2CFFE7D5975E55E2), INT64CONST(0x6E0F063E3EB46371),
  INT64CONST(0xA91E2402C48A38C4), INT64CONST(0xEBEEC5E96D600E57),
  INT64CONST(0x65CC8190991CB93D), INT64CONST(0x273C607B30F68FAE),
  INT64CONST(0xE02D4247CAC8D41B), INT64CONST(0xA2DDA3AC6322E288),
  INT64CONST(0xBE992B5F8BDB8C5C), INT64CONST(0xFC69CAB42231BACF),
  INT64CONST(0x3B78E888D80FE17A), INT64CONST(0x7988096371E5D7E9),
  INT64CONST(0xF7AA4D1A85996083), INT64CONST(0xB55AACF12C735610),
  INT64CONST(0x724B8ECDD64D0DA5), INT64CONST(0x30BB6F267FA73B36),
  INT64CONST(0x4AC29F2A07BFD00D), INT64CONST(0x08327EC1AE55E69E),
  INT64CONST(0xCF235CFD546BBD2B), INT64CONST(0x8DD3BD16FD818BB8),
  INT64CONST(0x03F1F96F09FD3CD2), INT64CONST(0x41011884A0170A41),
  INT64CONST(0x86103AB85A2951F4), INT64CONST(0xC4E0DB53F3C36767),
  INT64CONST(0xD8A453A01B3A09B3), INT64CONST(0x9A54B24BB2D03F20),
  INT64CONST(0x5D45907748EE6495), INT64CONST(0x1FB5719CE1045206),
  INT64CONST(0x919735E51578E56C), INT64CONST(0xD367D40EBC92D3FF),
  INT64CONST(0x1476F63246AC884A), INT64CONST(0x568617D9EF46BED9),
  INT64CONST(0xE085162AB69D5E3C), INT64CONST(0xA275F7C11F7768AF),
  INT64CONST(0x6564D5FDE549331A), INT64CONST(0x279434164CA30589),
  INT64CONST(0xA9B6706FB8DFB2E3), INT64CONST(0xEB46918411358470),
  INT64CONST(0x2C57B3B8EB0BDFC5), INT64CONST(0x6EA7525342E1E956),
  INT64CONST(0x72E3DAA0AA188782), INT64CONST(0x30133B4B03F2B111),
  INT64CONST(0xF7021977F9CCEAA4), INT64CONST(0xB5F2F89C5026DC37),
  INT64CONST(0x3BD0BCE5A45A6B5D), INT64CONST(0x79205D0E0DB05DCE),
  INT64CONST(0xBE317F32F78E067B), INT64CONST(0xFCC19ED95E6430E8),
  INT64CONST(0x86B86ED5267CDBD3), INT64CONST(0xC4488F3E8F96ED40),
  INT64CONST(0x0359AD0275A8B6F5), INT64CONST(0x41A94CE9DC428066),
  INT64CONST(0xCF8B0890283E370C), INT64CONST(0x8D7BE97B81D4019F),
  INT64CONST(0x4A6ACB477BEA5A2A), INT64CONST(0x089A2AACD2006CB9),
  INT64CONST(0x14DEA25F3AF9026D), INT64CONST(0x562E43B4931334FE),
  INT64CONST(0x913F6188692D6F4B), INT64CONST(0xD3CF8063C0C759D8),
  INT64CONST(0x5DEDC41A34BBEEB2), INT64CONST(0x1F1D25F19D51D821),
  INT64CONST(0xD80C07CD676F8394), INT64CONST(0x9AFCE626CE85B507)
};

// Initialize a CRC accumulator
inline void
crc64_init(uint64_t& crc) { crc = INT64CONST(0xffffffffffffffff); }

// Finish a CRC calculation
inline void
crc64_fin(uint64_t& crc) { crc ^= INT64CONST(0xffffffffffffffff); }

// Accumulate some (more) bytes into a CRC
inline void
crc64_compute(uint64_t& crc, unsigned char const* data, size_t len)
{
  while (len-- > 0)
  {
    int tab_index = ((int) (crc >> 56) ^ *data++) & 0xFF;
    crc = crc_table[tab_index] ^ (crc << 8);
  }
}

void crc64(unsigned char const* bP, size_t len, unsigned int* crcL, unsigned int* crcH)
{
  uint64_t crc;
  crc64_init(crc);
  crc64_compute(crc, bP, len);
  crc64_fin(crc);

  *crcL = static_cast<unsigned int>(crc & 0xffffffff);
  *crcH = static_cast<unsigned int>((crc >> 32) & 0xffffffff);
}


//************************************************************************
// Sommaire: Calcule le CRC32 d'une chaîne.
//
// Description:
//    La fonction <code>C_ST_CRC32()</code> calcule le CRC 32 bit de la
//    chaîne passée en argument. Le CRC32 permet d'encoder de manière unique
//    une chaîne sous la forme d'un entier 32 bits.
//
// Entrée:
//    char* str;
//
// Sortie:
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_ST_CRC32 (F2C_CONF_STRING  str,
                                                          fint_t*          crc
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_ST_CRC32 (F2C_CONF_STRING  str,
                                                          fint_t*          crc)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   unsigned int c;
   crc32(reinterpret_cast<unsigned char*>(sP), l, &c);
   *crc= c;
   return 0;
}

//************************************************************************
// Sommaire: Calcule le CRC64 d'une chaîne.
//
// Description:
//    La fonction <code>C_ST_CRC64()</code> calcule le CRC 64 bit de la
//    chaîne passée en argument. Le CRC64 permet d'encoder de manière unique
//    une chaîne sous la forme d'une table de 2 entiers.
//
// Entrée:
//    char* str;
//
// Sortie:
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_ST_CRC64 (F2C_CONF_STRING  str,
                                                          fint_t*          crc
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_ST_CRC64 (F2C_CONF_STRING  str,
                                                          fint_t*          crc)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   unsigned int cL, cH;
   crc64(reinterpret_cast<unsigned char*>(sP), l, &cL, &cH);
   crc[0] = cL;
   crc[1] = cH;
   return 0;
}

//************************************************************************
// Sommaire: Calcule le MD5 d'un buffer.
//
// Description:
//    La fonction <code>C_ST_MD5()</code> calcule le MD5 du buffer passé
//    en argument.
//    Le MD5 permet d'encoder de manière unique un buffer sous la forme
//    d'une table de 4 entiers.
//
// Entrée:
//    find_t* lbuf
//    void*   buf
//
// Sortie:
//
// Notes:
//************************************************************************
extern "C"
{
#include "md5.h"
}
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_ST_MD5(fint_t* lbuf,
                                                       void*   buf,
                                                       fint_t* md5)
{
   MD5_CTX ctx;
   unsigned char result[16];

   MD5_Init  (&ctx);
   MD5_Update(&ctx, buf, *lbuf);
   MD5_Final (result, &ctx);

   md5[0] = *reinterpret_cast<fint_t*>(&result[ 0]);
   md5[1] = *reinterpret_cast<fint_t*>(&result[ 4]);
   md5[2] = *reinterpret_cast<fint_t*>(&result[ 8]);
   md5[3] = *reinterpret_cast<fint_t*>(&result[12]);

   return 0;
}
