C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de transport-diffusion
C         de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire:  CD2D_BSE_PRNPRNO
C
C Description:
C     IMPRESSION DES PROPRIÉTÉS NODALES:
C                    6) 7) 8) 9) 10)
C  Propriétés nodales initiales:  1) u  2) v  3) H  4) Dv  5) Dh
C  Tenseur de dispersion (cts):   6) Kxx   7) Kxy   8) Kyy
C  Propriétés convect-incomplète: 9) uH   10) vH
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSE_PRNPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PRNPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     IMPRESSION DE L'INFO
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('PROPRIETES NODALES DE TRANSPORT-DIFFUSION LUES:')
      CALL LOG_INCIND()
      CALL LOG_ECRIS('COMPOSANTE DE VITESSE U SUIVANT X;')
      CALL LOG_ECRIS('COMPOSANTE DE VITESSE V SUIVANT Y;')
      CALL LOG_ECRIS('PROFONDEUR H;')
      CALL LOG_ECRIS('DIFFUSIVITE VERTICALE;')
      CALL LOG_ECRIS('DIFFUSIVITE HORIZONTALE.')
      CALL LOG_DECIND()

      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS(
     &           'PROPRIETES NODALES DE TRANSPORT-DIFFUSION CALCULEES:')
      CALL LOG_INCIND()
      CALL LOG_ECRIS('DIFFUSIVITE Dxx;')
      CALL LOG_ECRIS('DIFFUSIVITE Dxy=Dyx;')
      CALL LOG_ECRIS('DIFFUSIVITE Dyy;')
      CALL LOG_ECRIS('PRODUIT H.U;')
      CALL LOG_ECRIS('PRODUIT H.V.')
      CALL LOG_DECIND()

      RETURN
      END

