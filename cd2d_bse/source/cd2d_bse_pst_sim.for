C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER CD2D_BSE_PST_SIM_000
C     INTEGER CD2D_BSE_PST_SIM_999
C     INTEGER CD2D_BSE_PST_SIM_CTR
C     INTEGER CD2D_BSE_PST_SIM_DTR
C     INTEGER CD2D_BSE_PST_SIM_INI
C     INTEGER CD2D_BSE_PST_SIM_INIH
C     INTEGER CD2D_BSE_PST_SIM_RST
C     INTEGER CD2D_BSE_PST_SIM_REQHBASE
C     LOGICAL CD2D_BSE_PST_SIM_HVALIDE
C     INTEGER CD2D_BSE_PST_SIM_ACC
C     INTEGER CD2D_BSE_PST_SIM_FIN
C     INTEGER CD2D_BSE_PST_SIM_XEQ
C     INTEGER CD2D_BSE_PST_SIM_ASGHSIM
C     INTEGER CD2D_BSE_PST_SIM_REQHVNO
C     CHARACTER*256 CD2D_BSE_PST_SIM_REQNOMF
C     INTEGER CD2D_BSE_PST_SIM_CLC
C   Private:
C     INTEGER CD2D_BSE_PST_SIM_CLC2
C     INTEGER CD2D_BSE_PST_SIM_LOG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CD2D_BSE_PST_SIM_NOBJMAX,
     &                   CD2D_BSE_PST_SIM_HBASE,
     &                   'CD2D - Post-traitement Simulation')

      CD2D_BSE_PST_SIM_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER  IERR
      EXTERNAL CD2D_BSE_PST_SIM_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CD2D_BSE_PST_SIM_NOBJMAX,
     &                   CD2D_BSE_PST_SIM_HBASE,
     &                   CD2D_BSE_PST_SIM_DTR)

      CD2D_BSE_PST_SIM_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CD2D_BSE_PST_SIM_NOBJMAX,
     &                   CD2D_BSE_PST_SIM_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
         IOB = HOBJ - CD2D_BSE_PST_SIM_HBASE

         CD2D_BSE_PST_SIM_HPRNT(IOB) = 0
      ENDIF

      CD2D_BSE_PST_SIM_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CD2D_BSE_PST_SIM_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CD2D_BSE_PST_SIM_NOBJMAX,
     &                   CD2D_BSE_PST_SIM_HBASE)

      CD2D_BSE_PST_SIM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL CD2D_BSE_PST_SIM_CLC
      EXTERNAL CD2D_BSE_PST_SIM_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = CD2D_BSE_PST_SIM_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,
     &                                      HOBJ, CD2D_BSE_PST_SIM_CLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFLOG,
     &                                      HOBJ, CD2D_BSE_PST_SIM_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD()) IERR = PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD()) IERR = PS_SIMU_INI(HPRNT,
     &                                   HFCLC,  HFLOG,
     &                                   NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - CD2D_BSE_PST_SIM_HBASE
         CD2D_BSE_PST_SIM_HPRNT(IOB) = HPRNT
      ENDIF

      CD2D_BSE_PST_SIM_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_INIH(HOBJ,
     &                               HFCLC, HFLOG,
     &                               NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_INIH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HFCLC
      INTEGER       HFLOG
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
      EXTERNAL CD2D_BSE_PST_SIM_CLC
      EXTERNAL CD2D_BSE_PST_SIM_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = CD2D_BSE_PST_SIM_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (HFCLC .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
         IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,
     &                                         HOBJ,
     &                                         CD2D_BSE_PST_SIM_CLC)
      ENDIF
      IF (HFLOG .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFLOG)
         IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFLOG,
     &                                         HOBJ,
     &                                         CD2D_BSE_PST_SIM_LOG)
      ENDIF

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR = PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD())IERR = PS_SIMU_INI(HPRNT,
     &                                  HFCLC, HFLOG,
     &                                  NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - CD2D_BSE_PST_SIM_HBASE
         CD2D_BSE_PST_SIM_HPRNT(IOB) = HPRNT
      ENDIF

      CD2D_BSE_PST_SIM_INIH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - CD2D_BSE_PST_SIM_HBASE

C---     Détruis les données
      HPRNT = CD2D_BSE_PST_SIM_HPRNT(IOB)
      IF (PS_SIMU_HVALIDE(HPRNT)) THEN
         IERR = PS_SIMU_DTR(HPRNT)
      ENDIF

C---     Reset
      CD2D_BSE_PST_SIM_HPRNT(IOB) = 0

      CD2D_BSE_PST_SIM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CD2D_BSE_PST_SIM_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'
C------------------------------------------------------------------------

      CD2D_BSE_PST_SIM_REQHBASE = CD2D_BSE_PST_SIM_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CD2D_BSE_PST_SIM_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'
C------------------------------------------------------------------------

      CD2D_BSE_PST_SIM_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       CD2D_BSE_PST_SIM_NOBJMAX,
     &                                       CD2D_BSE_PST_SIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = CD2D_BSE_PST_SIM_HPRNT(HOBJ-CD2D_BSE_PST_SIM_HBASE)
      CD2D_BSE_PST_SIM_ACC = PS_SIMU_ACC(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = CD2D_BSE_PST_SIM_HPRNT(HOBJ-CD2D_BSE_PST_SIM_HBASE)
      CD2D_BSE_PST_SIM_FIN = PS_SIMU_FIN(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
      INTEGER NPOST
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère NDLN
      NPOST = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)

C---     Transfert l'appel au parent
      HPRNT = CD2D_BSE_PST_SIM_HPRNT(HOBJ-CD2D_BSE_PST_SIM_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, NPOST)

      CD2D_BSE_PST_SIM_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSE_PST_SIM_ASGHSIM
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le contrôle de cohérence est très faible.
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
      INTEGER NPOST
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère NDLN
      NPOST = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)

C---     Transfert l'appel au parent
      HPRNT = CD2D_BSE_PST_SIM_HPRNT(HOBJ-CD2D_BSE_PST_SIM_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, NPOST)

      CD2D_BSE_PST_SIM_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSE_PST_SIM_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = CD2D_BSE_PST_SIM_HPRNT(HOBJ-CD2D_BSE_PST_SIM_HBASE)
      CD2D_BSE_PST_SIM_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSE_PST_SIM_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_SIM_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = CD2D_BSE_PST_SIM_HPRNT(HOBJ-CD2D_BSE_PST_SIM_HBASE)
      CD2D_BSE_PST_SIM_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: CD2D_PST_SIM_CLC
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_CLC(HOBJ,
     &                              HELE,
     &                              NPST,
     &                              NNL,
     &                              VPOST)

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELE
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER IERR
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les données
      GDTA => LM_GEOM_REQGDTA(HELE)
      EDTA => LM_ELEM_REQEDTA(HELE)

C---     Fait le calcul
      IERR = CD2D_BSE_PST_SIM_CLC2(HOBJ,
     &                             EDTA%VDLG,
     &                             NPST,
     &                             NNL,
     &                             VPOST)

      CD2D_BSE_PST_SIM_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_PST_SIM_CLC2
C
C Description:
C     La fonction privée CD2D_PST_SIM_CLC2 fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_CLC2(HOBJ,
     &                               VDLG,
     &                               NPST,
     &                               NNL,
     &                               VPOST)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER HOBJ
      REAL*8  VDLG (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER ID
C-----------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .GE. LM_CMMN_NDLN)
D     CALL ERR_PRE(NNL  .EQ. EG_CMMN_NNL)
C-----------------------------------------------------------------------

C---     Copie les concentrations de VDLG dans VPOST
      DO ID=1,LM_CMMN_NDLN
         CALL DCOPY(EG_CMMN_NNL,
     &              VDLG (ID,1), LM_CMMN_NDLN,
     &              VPOST(ID,1), NPST)
      ENDDO

      CD2D_BSE_PST_SIM_CLC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PST_SIM_LOG
C
C Description:
C     La fonction privée CD2D_BSE_PST_SIM_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_SIM_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'cd2d_bse_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'cd2d_bse_pst_sim.fc'

      INTEGER IERR
      INTEGER IP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_CD2D_POST_SIM:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<35>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)')    'MSG_VALEUR#<35>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      DO IP=1, NPST
         IERR = PS_PSTD_LOGVAL(HNUMR, IP, NPST, NNL, VPOST, 'c')
      ENDDO
      CALL LOG_DECIND()

      CALL LOG_DECIND()

      CD2D_BSE_PST_SIM_LOG = ERR_TYP()
      RETURN
      END
