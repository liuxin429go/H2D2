C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:    Aide sur une propriété
C
C Description:
C     La fonction auxiliaire CD2D_BSE_HLP1 imprime l'aide sur une
C     seule propriété.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_HLP1(IP, TXT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_HLP1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       IP
      CHARACTER*(*) TXT

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,A)') IP, TXT
      CALL LOG_ECRIS(LOG_BUF)

      CD2D_BSE_HLP1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire CD2D_BSE_PRN1PRGL imprime une seule
C     propriété globale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PRN1PRGL(IP, TXT, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PRN1PRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       IP
      CHARACTER*(*) TXT
      REAL*8        V

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      IF (TXT(1:3) .EQ. '---') THEN
         WRITE (LOG_BUF, '(I3,1X,A)') IP, TXT, '#<55>#'
      ELSE
         WRITE (LOG_BUF, '(I3,1X,2A,1PE14.6E3)') IP, TXT, '#<55>#=', V
      ENDIF
      CALL LOG_ECRIS(LOG_BUF)

      CD2D_BSE_PRN1PRGL = ERR_TYP()
      RETURN
      END

