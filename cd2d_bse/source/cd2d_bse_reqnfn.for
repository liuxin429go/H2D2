C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction CD2D_BSE_REQNFN retourne le nom de la fonction
C     associé au code demandé. Le nom combine le nom de la DLL et
C     de la fonction sous la forme "nomDll@nomFonction".
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     KFNC     Nom de la fonction stocké dans une table INTEGER
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_REQNFN(KFNC, IFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_REQNFN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER CD2D_BSE_REQNFN
      INTEGER KFNC(*)
      INTEGER IFNC

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'

      INTEGER        I
      INTEGER        JF(16)
      CHARACTER*(64) NF
      EQUIVALENCE (NF, JF)
C-----------------------------------------------------------------------

      IF     (IFNC .EQ. EA_FUNC_CLCCLIM) THEN
                                    NF = 'CD2D_BSE_CLCCLIM@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCDLIB) THEN
                                    NF = 'CD2D_BSE_CLCDLIB@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCPRES) THEN
                                    NF = 'CD2D_BSE_CLCPRES@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCPREV) THEN
                                    NF = 'CD2D_BSE_CLCPREV@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCPRNO) THEN
                                    NF = 'CD2D_BSE_CLCPRNO@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCDLIB) THEN
                                    NF = 'CD2D_BSE_PRCDLIB@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRGL) THEN
                                    NF = 'CD2D_BSE_PRCPRGL@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCSOLC) THEN
                                    NF = 'CD2D_BSE_PRCSOLC@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCSOLR) THEN
                                    NF = 'CD2D_BSE_PRCSOLR@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_HLPCLIM) THEN
                                    NF = 'CD2D_BSE_HLPCLIM@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_HLPPRGL) THEN
                                    NF = 'CD2D_BSE_HLPPRGL@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_HLPPRNO) THEN
                                    NF = 'CD2D_BSE_HLPPRNO@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNCLIM) THEN
                                    NF = 'CD2D_BSE_PRNCLIM@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRGL) THEN
                                    NF = 'CD2D_BSE_PRNPRGL@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRNO) THEN
                                    NF = 'CD2D_BSE_PRNPRNO@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PSCPRNO) THEN
                                    NF = 'CD2D_BSE_PSCPRNO@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLCLIM) THEN
                                    NF = 'CD2D_BSE_PSLCLIM@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLDLIB) THEN
                                    NF = 'CD2D_BSE_PSLDLIB@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPREV) THEN
                                    NF = 'CD2D_BSE_PSLPREV@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPRGL) THEN
                                    NF = 'CD2D_BSE_PSLPRGL@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPRNO) THEN
                                    NF = 'CD2D_BSE_PSLPRNO@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLSOLC) THEN
                                    NF = 'CD2D_BSE_PSLSOLC@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLSOLR) THEN
                                    NF = 'CD2D_BSE_PSLSOLR@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_REQNFN) THEN
                                    NF = 'CD2D_BSE_REQNFN@cd2d_bse'
      ELSEIF (IFNC .EQ. EA_FUNC_REQPRM) THEN
                                    NF = 'CD2D_BSE_REQPRM@cd2d_bse'
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_CODE_FNCT_INVALIDE')
      ENDIF

      IF (ERR_GOOD()) THEN
         DO I=1,16
            KFNC(I) = JF(I)
         ENDDO
      ENDIF

      CD2D_BSE_REQNFN = ERR_TYP()
      RETURN
      END
