C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  CD2D_BSE_CLCPRES
C
C Description:
C     Calcul des propriétés élémentaires dépendantes de VDLG
C     pour les elements de surface
C
C     Propriétés:
C        1) u1.n    2) u2.n
C        3) HKxx1 4) HKxx2 5) HKxy1 6) HKxy2 7) HKyy1 8) HKyy2
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BSE_CLCPRES(VCORG,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VDLG,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_CLCPRES
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV(LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES(LM_CMMN_NPRES, EG_CMMN_NELS)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      IERR = ERR_TYP()
      RETURN
      END

