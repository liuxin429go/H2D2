C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Assemble
C
C Description:
C     La fonction CD2D_BSEC_ASGCMN copie les valeurs du common externe
C     passés en paramètre sous la forme d'un vecteur sur les valeurs
C     du common local à la DLL.
C
C Entrée:
C     KA       Les valeurs du common
C
C Sortie:
C
C Notes:
C     les common(s) sont privés au DLL, il faut donc propager les
C     changement entre l'externe et l'interne.
C
C************************************************************************
      FUNCTION CD2D_BSEC_ASGCMN(EG_KA, EA_KA, EA_VA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSEC_ASGCMN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER CD2D_BSEC_ASGCMN
      INTEGER EG_KA(*)
      INTEGER EA_KA(*)
      REAL*8  EA_VA(*)

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER CD2D_BSE_ASGCMN
C-----------------------------------------------------------------------

      IERR = CD2D_BSE_ASGCMN(EG_KA, EA_KA, EA_VA)

      CD2D_BSEC_ASGCMN = ERR_TYP()
      RETURN
      END
