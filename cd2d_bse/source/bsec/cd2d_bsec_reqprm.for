C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de convection-diffusion eulérienne 2-D
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C     Subroutines de base.
C************************************************************************

C************************************************************************
C Sommaire: Retourne les paramètres de l'élément.
C
C Description:
C     La fonction <code>CD2D_BSEC_REQPRM</code> retourne tous les
C     paramètres caractéristiques de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSEC_REQPRM(TGELV,
     &                           NPRGL,
     &                           NPRGLL,
     &                           NPRNO,
     &                           NPRNOL,
     &                           NPREV,
     &                           NPREVL,
     &                           NPRES,
     &                           NSOLC,
     &                           NSOLCL,
     &                           NSOLR,
     &                           NSOLRL,
     &                           NDLN,
     &                           NDLEV,
     &                           NDLES,
     &                           ASURFACE,
     &                           ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSEC_REQPRM
CDEC$ ENDIF

      INTEGER TGELV
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES

      LOGICAL ASURFACE
      LOGICAL ESTLIN
C-----------------------------------------------------------------------

C---     Initialise les paramètres de l'élément parent virtuel
      CALL CD2D_BSE_REQPRM(TGELV,
     &                     NPRGL,
     &                     NPRGLL,
     &                     NPRNO,
     &                     NPRNOL,
     &                     NPREV,
     &                     NPREVL,
     &                     NPRES,
     &                     NSOLC,
     &                     NSOLCL,
     &                     NSOLR,
     &                     NSOLRL,
     &                     NDLN,
     &                     NDLEV,
     &                     NDLES,
     &                     ASURFACE,
     &                     ESTLIN)

      RETURN
      END
