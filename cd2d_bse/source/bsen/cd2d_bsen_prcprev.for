C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D DE BASE SANS CINÉTIQUES.
C                FORMULATION NON-CONSERVATIVE POUR (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_BSEN_PRCPREV
C
C Description:
C     Pré-traitement des propriétés élémentaires de volume
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BSEN_PRCPREV(VCORG,
     &                            KNGV,
     &                            VDJV,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSEN_PRCPREV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV(LM_CMMN_NPREV, EG_CMMN_NELV)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IC, IE
      INTEGER IPH, IPKXX, IPKXY, IPKYY
      INTEGER NO1, NO2, NO3
      REAL*8  X1, Y1, X2, Y2, X3, Y3, DELX, DELY
      REAL*8  SH, SH1, SH2, SH3
C-----------------------------------------------------------------------

C---     INDICES DANS VPRNO
      IPH   = 3
      IPKXX = LM_CMMN_NPRNOL + 1
      IPKXY = LM_CMMN_NPRNOL + 2
      IPKYY = LM_CMMN_NPRNOL + 3

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(NO1, NO2, NO3)
!$omp& private(X1, X2, X3, Y1, Y2, Y3, DELX, DELY)
!$omp& private(SH, SH1, SH2, SH3)

C------    BOUCLE SUR LES ÉLÉMENTS
C          =======================
      DO 10 IC = 1,EG_CMMN_NELCOL
!$omp  do
      DO 20 IE = EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1 = KNGV(1,IE)
         NO2 = KNGV(2,IE)
         NO3 = KNGV(3,IE)

C---        CALCUL DES TAILLES DE MAILLES EN X ET EN Y POUR LA STABILISATION DE LA CONVECTION
         X1 = VCORG(1,NO1)
         X2 = VCORG(1,NO2)
         X3 = VCORG(1,NO3)
         Y1 = VCORG(2,NO1)
         Y2 = VCORG(2,NO2)
         Y3 = VCORG(2,NO3)
         DELX = MAX(ABS(X2-X1),ABS(X3-X2),ABS(X1-X3))
         DELY = MAX(ABS(Y2-Y1),ABS(Y3-Y2),ABS(Y1-Y3))
         VPREV(1,IE) = DELX
         VPREV(2,IE) = DELY

C---        CALCUL DES COMPOSANTES ÉLÉMENTAIRES DU TENSEUR DE DISPERSION: 4, 5, 6
         SH  = VPRNO(IPH,NO1)+VPRNO(IPH,NO2)+VPRNO(IPH,NO3)
         SH1 = VPRNO(IPH,NO1)+SH
         SH2 = VPRNO(IPH,NO2)+SH
         SH3 = VPRNO(IPH,NO3)+SH

         VPREV(4,IE) = SH1*VPRNO(IPKXX,NO1)
     &               + SH2*VPRNO(IPKXX,NO2)
     &               + SH3*VPRNO(IPKXX,NO3)
         VPREV(5,IE) = SH1*VPRNO(IPKXY,NO1)
     &               + SH2*VPRNO(IPKXY,NO2)
     &               + SH3*VPRNO(IPKXY,NO3)
         VPREV(6,IE) = SH1*VPRNO(IPKYY,NO1)
     &               + SH2*VPRNO(IPKYY,NO2)
     &               + SH3*VPRNO(IPKYY,NO3)

20    CONTINUE
!$omp end do
10    CONTINUE

!$omp end parallel

      IERR = ERR_TYP()
      RETURN
      END
