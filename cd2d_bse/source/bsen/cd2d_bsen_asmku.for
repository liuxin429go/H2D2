C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D.
C                         FORMULATION NON-CONSERVATIVE POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de
C        transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire: CD2D_BSEN_ASMKU
C
C Description:
C     ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKU(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSEN_ASMKU
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse.fi'

      REAL*8  VKE  (CD2D_BSE_NDLEMAX, CD2D_BSE_NDLEMAX)
      REAL*8  VFE  (CD2D_BSE_NDLEMAX)
      REAL*8  VDLE (CD2D_BSE_NDLEMAX)
      INTEGER KLOCE(CD2D_BSE_NDLEMAX)
      INTEGER IERR
C-----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. CD2D_BSE_NDLNMAX)
C-----------------------------------------------------------------

      IERR = ERR_OK

      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VDLE, VKE, VFE, KLOCE)
         CALL CD2D_BSEN_ASMKU_V(KLOCE,
     &                         VDLE,
     &                         VKE,
     &                         VFE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLR,
     &                         VDLG,
     &                         VFG)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

      IF (ERR_GOOD()) THEN
         CALL CD2D_BSEN_ASMKU_S(KLOCE,
     &                         VDLE,
     &                         VKE,
     &                         VFE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSEN_ASMKU_V
C
C Description:
C     ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKU_V(KLOCE,
     &                             VDLE,
     &                             VKE,
     &                             VFE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLR,
     &                             VDLG,
     &                             VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLE  (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NEQL)

      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER IERR
      INTEGER IC, IE, IN, ID, II, NO

      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      PARAMETER (NNEL_LCL =  3)
      PARAMETER (NDLE_LCL = CD2D_BSE_NDLEMAX)
      PARAMETER (NPRN_LCL = 50)

      INTEGER KNE (NNEL_LCL)
      REAL*8  VPRN(NPRN_LCL * NNEL_LCL)
      REAL*8  VSOL(NDLE_LCL * NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        Initialise la matrice élémentaire
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)

C---        Transfert des connectivités élémentaires
         KNE(:) = KNGV(:,IE)

C---        Transfert des DDL
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NDLN, VDLG(1,NO), 1, VDLE(1,IN), 1)
         ENDDO

C---        Transfert des PRNO
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        Transfert des SOLR
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NSOLR, VSOLR(1,NO), 1, VSOL(II), 1)
            II=II+LM_CMMN_NSOLR
         ENDDO

C---        Calcul de la matrice élémentaire de volume
         CALL CD2D_BSEN_ASMKE_V(VKE,
     &                          VDJV(1,IE),
     &                          VPRGL,
     &                          VPRN,
     &                          VPREV(1,IE),
     &                          VSOL,
     &                          VDLE)

C---        Produit matrice-vecteur
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKE,               ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLE,              ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VFE,               ! y
     &              1)                 ! incy

C---        Table kloce de l'élément
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL ICOPY(LM_CMMN_NDLN, KLOCN(1,NO), 1, KLOCE(1,IN), 1)
         ENDDO

C---        ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMKT_S
C
C Description:
C     La fonction CD2D_BSEN_ASMKT_S calcul la matrice tangente élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     c.f. CD2D_BSEN_ASMKT_S
C
C     Le noeud opposé à l'élément de surface est perturbé en trop.
C     La matrice Ke peut être nulle.
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKU_S(KLOCE,
     &                             VDLEV,
     &                             VKE,
     &                             VFE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES, ! pas utilisé
     &                             KEIMP,
     &                             VDLG,
     &                             VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLEV (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER NDIM_LCL
      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      INTEGER NPRE_LCL
      PARAMETER (NDIM_LCL =  2)
      PARAMETER (NNEL_LCL =  3)
      PARAMETER (NDLE_LCL = 18)
      PARAMETER (NPRN_LCL = 18)
      PARAMETER (NPRE_LCL =  8)

      REAL*8  CD2D_PNUMR_DELPRT
      REAL*8  CD2D_PNUMR_DELMIN
      PARAMETER(CD2D_PNUMR_DELPRT   = 1.0D-07)
      PARAMETER(CD2D_PNUMR_DELMIN   = 1.0D-15)

      REAL*8  VCORE(NDIM_LCL, NNEL_LCL)
      REAL*8  VPRNV(NPRN_LCL * NNEL_LCL)
      REAL*8  VRESE(NDLE_LCL), VRESEP(NDLE_LCL)
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      INTEGER IERR
      INTEGER IN, ID, II, NO, I
      INTEGER IES, IEV, ICT
      INTEGER HVFT, HPRNE, HPREVE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDIM_LCL .EQ. EG_CMMN_NDIM)
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
D     CALL ERR_PRE(NPRE_LCL .GE. LM_CMMN_NPREV)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO IES=1,EG_CMMN_NELS

C---        Élément parent et coté
         IEV = KNGS(3,IES)
         ICT = KNGS(4,IES)

C---        Transfert des DDL de volume
         VDLEV(:,:) = VDLG(:,KNGV(:,IEV))

C---        Transfert des PRNO
!!!         VPRNV(:,:) = VPRNO(:,KNGV(:,IEV))
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN,IEV)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRNV(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        Initialise la matrice élémentaire
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV, ZERO, VKE, 1)

C---        [K(u)]
         CALL CD2D_BSEN_ASMKE_S(VKE,
     &                          VDJV(1,IEV),
     &                          VDJS(1,IES),
     &                          VPRGL,
     &                          VPRNV,
     &                          VPREV(1,IEV),
     &                          VPRES(1,IES), ! pas utilisé
     &                          VDLEV,
     &                          KEIMP(IES),
     &                          ICT)

C---        Produit matrice-vecteur
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKE,               ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLEV,             ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VFE,               ! y
     &              1)                 ! incy


C---        ASSEMBLAGE DU VECTEUR GLOBAL
         KLOCE(:,:) = KLOCN(:,KNGV(:,IEV))
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMKU_V
C
C Description:
C     ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKU0_V(KLOCE,
     &                            VFE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            VDLG,
     &                            VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'cd2d_bse.fc'

      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DETJ
      REAL*8  CPEC, CLAP, DISP, CONV, SOLR, SPEC, SLAP, SPECX, SPECY
      REAL*8  DELX, DELY, AKXX, AKXY, AKYY
      REAL*8  C1, C2, C3, HU1, HU2, HU3, HV1, HV2, HV3
      REAL*8  SHU, SHV, ASHU, ASHV
      REAL*8  DFX, DFY
      REAL*8  CUX1, CUX2, CUX3, CUY1, CUY2, CUY3
      REAL*8  CNV1, CNV2, CNV3
      REAL*8  STBX, STBY, STB1, STB2, STB3
      REAL*8  CX, CY, DCNRM
      REAL*8  QH1, QH2, QH3, SQH
      REAL*8  SOL11, SOL12, SOL13, SOL22, SOL23, SOL33, SOL1, SOL2, SOL3
      REAL*8  DIVQ, SDIV
      INTEGER IERR
      INTEGER IC, IE, ID, ID1, ID2, ID3
      INTEGER IPHU, IPHV
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPHU = LM_CMMN_NPRNOL + 4
      IPHV = LM_CMMN_NPRNOL + 5

C---     Prop. globales
      CPEC = UN / CD2D_STABI_PECLET    ! Peclet
      CLAP = CD2D_STABI_LAPIDUS        ! Lapidus

C---     Initialise
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VFE,  1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C---     Boucle sur les éléments
C        =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        CONNECTIVITES DU T3
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)

C---        MÉTRIQUES DU T3
         VKX  = VDJV(1,IE)
         VEX  = VDJV(2,IE)
         VKY  = VDJV(3,IE)
         VEY  = VDJV(4,IE)
         DETJ = VDJV(5,IE)
         VSX  = -(VKX+VEX)
         VSY  = -(VKY+VEY)

C---        CONSTANTES DÉPENDANT DU VOLUME
         DISP  = UN_24/DETJ            ! Dispersion
         CONV  = UN_24*CD2D_CMULT_CONV ! Convection
         SDIV  = UN_24*CD2D_CMULT_DIVV ! Divergence
         SOLR  = UN_120*DETJ           ! Sol. réparties
         SPEC  = UN_6*CPEC/DETJ        ! Stabilisation Peclet
         SLAP  = UN_2*CLAP/DETJ        ! Stabilisation Lapidus

C---        Initialisation des propriétés élémentaires
         DELX = VPREV(1,IE)
         DELY = VPREV(2,IE)
         AKXX = VPREV(4,IE)
         AKXY = VPREV(5,IE)
         AKYY = VPREV(6,IE)

C---        Initialisation des propriétés nodales
         HU1 = VPRNO(IPHU,NO1)
         HV1 = VPRNO(IPHV,NO1)
         HU2 = VPRNO(IPHU,NO2)
         HV2 = VPRNO(IPHV,NO2)
         HU3 = VPRNO(IPHU,NO3)
         HV3 = VPRNO(IPHV,NO3)
         SHU = HU1 + HU2 + HU3
         SHV = HV1 + HV2 + HV3
         ASHU = ABS(HU1) + ABS(HU2) + ABS(HU3)
         ASHV = ABS(HV1) + ABS(HV2) + ABS(HV3)

C---        Convection
         CUX1 = CONV*(SHU+HU1)
         CUX2 = CONV*(SHU+HU2)
         CUX3 = CONV*(SHU+HU3)
         CUY1 = CONV*(SHV+HV1)
         CUY2 = CONV*(SHV+HV2)
         CUY3 = CONV*(SHV+HV3)

C---        Stabilisation de la convection (Peclet)
         SPECX = SPEC * DELX * ASHU
         SPECY = SPEC * DELY * ASHV

C---        Divergence du débit
         DIVQ = SDIV * (VKX*(HU2-HU1) + VEX*(HU3-HU1) +
     &                  VKY*(HV2-HV1) + VEY*(HV3-HV1))

C---        Boucle sur les DDL
         DO ID=1,LM_CMMN_NDLN

C---           Degrés de liberté
            C1 = VDLG(ID, NO1)
            C2 = VDLG(ID, NO2)
            C3 = VDLG(ID, NO3)
            CX = (VSX*C1 + VKX*C2 + VEX*C3)
            CY = (VSY*C1 + VKY*C2 + VEY*C3)

C---           Convection
            CNV1= CUX1*CX + CUY1*CY
            CNV2= CUX2*CX + CUY2*CY
            CNV3= CUX3*CX + CUY3*CY

C---           Dispersion
            DFX = DISP*(AKXX*CX + AKXY*CY)
            DFY = DISP*(AKXY*CX + AKYY*CY)

C---           Stabilisation de Lapidus
            DCNRM = SLAP * MAX(HYPOT(CX, CY), 1.0D-12)

C---           STABILISATION GLOBALE
            STBX = (SPECX + DCNRM) * CX
            STBY = (SPECY + DCNRM) * CY
            STB1 = STBX*VSX + STBY*VSY
            STB2 = STBX*VKX + STBY*VKY
            STB3 = STBX*VEX + STBY*VEY

C---           Sollicitations réparties (Q en entrée)
            QH1   = SOLR * VSOLR(ID,NO1)
            QH2   = SOLR * VSOLR(ID,NO2)
            QH3   = SOLR * VSOLR(ID,NO3)
            SQH   = DEUX*(QH1 + QH2 + QH3)
            SOL11 = (SQH + QUATRE*QH1) + DIVQ+DIVQ
            SOL12 = (SQH - QH3)        + DIVQ
            SOL13 = (SQH - QH2)        + DIVQ
            SOL22 = (SQH + QUATRE*QH2) + DIVQ+DIVQ
            SOL23 = (SQH - QH1)        + DIVQ
            SOL33 = (SQH + QUATRE*QH3) + DIVQ+DIVQ
            SOL1  = SOL11*C1 + SOL12*C2 + SOL13*C3
            SOL2  = SOL12*C1 + SOL22*C2 + SOL23*C3
            SOL3  = SOL13*C1 + SOL23*C2 + SOL33*C3

C---           ASSEMBLAGE DES CONTRIBUTIONS
            VFE(ID,1) = CNV1 + (VSX*DFX+VSY*DFY) + SOL1 + STB1
            VFE(ID,2) = CNV2 + (VKX*DFX+VKY*DFY) + SOL2 + STB2
            VFE(ID,3) = CNV3 + (VEX*DFX+VEY*DFY) + SOL3 + STB3
         ENDDO


C---        TABLE KLOCE DE LOCALISATION DES DDLS
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NO1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NO2)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 3)= KLOCN(ID, NO3)
         ENDDO

C---        ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMKU0_S
C
C Description:
C     ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : VDLG=C
C
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKU0_S(KLOCE,
     &                            VFE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            VFG)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'

      INTEGER IERR
      INTEGER IES, IEV, ID
      INTEGER NO1, NO2, NO3, NP1, NP2, NP3
      INTEGER NDLES
      REAL*8  VSX, VKX, VEX, VNX
      REAL*8  VSY, VKY, VEY, VNY
      REAL*8  DETJL2, DETJT3
      REAL*8  C1, C2, C3, CX, CY
      REAL*8  QN1, QN2, SQN
      REAL*8  DISC, REP
      REAL*8  AKXXH1, AKXYH1, AKYYH1
      REAL*8  AKXXH2, AKXYH2, AKYYH2
      REAL*8  DIF1, DIF2
      REAL*8  VKE11, VKE21, VKE12, VKE22, VKE13, VKE23
C-----------------------------------------------------------------------
      INTEGER IE
      LOGICAL ELE_ESTTYPE1
      LOGICAL ELE_ESTTYPE2
      ELE_ESTTYPE1(IE) = BTEST(KEIMP(IE), EA_TPCL_ENTRANT) .AND.
     &                   (BTEST(KEIMP(IE), EA_TPCL_CAUCHY) .OR.
     &                    BTEST(KEIMP(IE), EA_TPCL_OUVERT))
      ELE_ESTTYPE2(IE) = BTEST(KEIMP(IE), EA_TPCL_SORTANT) .AND.
     &                   BTEST(KEIMP(IE), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------

      NDLES = LM_CMMN_NDLN*2

C---     Initialise
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VFE,  1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C---     BOUCLE POUR ASSEMBLER LES TERMES DE CONTOUR DE CAUCHY "ENTRANTS"
C        ================================================================
      DO IES=1,EG_CMMN_NELS
         IF (.NOT. ELE_ESTTYPE1(IES)) GOTO 199

         NP1 = KNGS(1,IES)
         NP2 = KNGS(2,IES)

C---        Constante dépendant du volume
         DETJL2 = VDJS(3,IES)
         REP = -UN_6*DETJL2

C---        Flux normal (Vn*H)
         QN1 = REP*VPRES(1,IES)
         QN2 = REP*VPRES(2,IES)
         SQN = QN1 + QN2

C---        Matrice de rigidite élémentaire de CAUCHY-ENTRANT
!         VKE11 = SQN + QN1+QN1
!         VKE21 = SQN
!         VKE12 = SQN
!         VKE22 = SQN + QN2+QN2
         VKE11 = SQN+SQN + QN1+QN1    ! Matrice lumpée
         VKE22 = SQN+SQN + QN2+QN2

C---        Boucle sur les ddl
         DO ID=1,LM_CMMN_NDLN
            C1 = VDLG(ID,NP1)
            C2 = VDLG(ID,NP2)
!            VFE(ID,1) = VKE11*C1 + VKE12*C2
!            VFE(ID,2) = VKE21*C1 + VKE22*C2
            VFE(ID,1) = VKE11*C1    ! Matrice lumpée
            VFE(ID,2) = VKE22*C2
         ENDDO

C---        Table KLOCE de localisation des ddls
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NP1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NP2)
         ENDDO

C---        ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(NDLES, KLOCE, VFE, VFG)

199      CONTINUE
      ENDDO


C---     BOUCLE POUR ASSEMBLER LES TERMES DE CONTOUR OUVERTS "SORTANTS"
C        ==============================================================
      DO IES=1,EG_CMMN_NELS
!!!         IF (.NOT. ELE_ESTTYPE2(IES)) GOTO 299

         NP1 = KNGS(1,IES)
         NP2 = KNGS(2,IES)
         IEV = KNGS(3,IES)
         NO1 = KNGV(1,IEV)
         NO2 = KNGV(2,IEV)
         NO3 = KNGV(3,IEV)

C---        Recherche le troisième noeud de l'élément de volume
         IF    (NP1 .EQ. NO1 .AND. NP2 .EQ. NO2) THEN
            NP3 = NO3
         ELSEIF(NP1 .EQ. NO2 .AND. NP2 .EQ. NO3) THEN
            NP3 = NO1
         ELSEIF(NP1 .EQ. NO3 .AND. NP2 .EQ. NO1) THEN
            NP3 = NO2
D        ELSE
D           CALL ERR_ASR(.FALSE.)
         ENDIF

C---        Métriques du T3 parent (recalculées car permutés)
         VKX    = VCORG(2,NP3) - VCORG(2,NP1)               ! Ksi,x
         VEX    = VCORG(2,NP1) - VCORG(2,NP2)               ! Eta,x
         VKY    = VCORG(1,NP1) - VCORG(1,NP3)               ! Ksi,y
         VEY    = VCORG(1,NP2) - VCORG(1,NP1)               ! Eta,y
         DETJT3 = VDJV(5,IEV)
         VSX    = -(VKX+VEX)
         VSY    = -(VKY+VEY)

C---        MÉTRIQUES DE L'ÉLÉMENT - COMPOSANTES DE LA NORMALE EXTÉRIEURE
         VNY    = -VDJS(1,IES)
         VNX    =  VDJS(2,IES)
         DETJL2 =  VDJS(3,IES)

C---        Constante dépendant du contour
         DISC = -UN_2*DETJL2/DETJT3

C---        Propriétés nodales du contour
         AKXXH1 = VPRES(3,IES)
         AKXXH2 = VPRES(4,IES)
         AKXYH1 = VPRES(5,IES)
         AKXYH2 = VPRES(6,IES)
         AKYYH1 = VPRES(7,IES)
         AKYYH2 = VPRES(8,IES)

C---       BOUCLE D'ASSEMBLAGE SUR LES DDL
         DO ID=1,LM_CMMN_NDLN
            C1 = VDLG(ID,NP1)
            C2 = VDLG(ID,NP2)
            C3 = VDLG(ID,NP3)

C---          DÉRIVÉES DU CONTOUR
            CX = VKX*(C2-C1) + VEX*(C3-C1)
            CY = VKY*(C2-C1) + VEY*(C3-C1)
            DIF1 = DISC * ( (AKXXH1*CX + AKXYH1*CY)*VNX
     &                  +   (AKXYH1*CX + AKYYH1*CY)*VNY )
            DIF2 = DISC * ( (AKXXH2*CX + AKXYH2*CY)*VNX
     &                  +   (AKXYH2*CX + AKYYH2*CY)*VNY )

C---          ASSEMBLAGE
            VFE(ID,1) = DIF1
            VFE(ID,2) = DIF2
         ENDDO

C---       TABLE KLOCE DE LOCALISATION DES DDLS
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NP1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NP2)
         ENDDO

C---       ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(NDLES, KLOCE, VFE, VFG)

299      CONTINUE
      ENDDO

      RETURN
      END

