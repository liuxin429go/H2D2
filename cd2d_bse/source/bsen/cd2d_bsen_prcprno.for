C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D DE BASE SANS CINÉTIQUES.
C                FORMULATION NON-CONSERVATIVE POUR (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_BSEN_PRCPRNO
C
C Description:
C     Pré-traitement au calcul des propriétés nodales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C     Propriétés nodales initiales lues:       1) u  2) v  3) H  4) Dv  5) Dh
C     Tenseur de dispersion calculé:           6) Kxx   7) Kxy   8) Kyy
C     Propriétés convect-incomplète calculées: 9) uH   10) vH
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BSEN_PRCPRNO(VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSEN_PRCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER IN
      INTEGER IPU, IPV, IPH, IPDV, IPDH
      INTEGER IPKXX, IPKXY, IPKYY, IPHU, IPHV
      REAL*8  U, V, H, W
      REAL*8  TETA, SI, CO
      REAL*8  ALFA, VN1, VN2, UN_DH, D0 !, FL, FT
      REAL*8  AKT_C, AKT_D, AKT
      REAL*8  AKL_C, AKL_D, AKL
      REAL*8  CU_C, CU_D, CU
      REAL*8  CV_C, CV_D, CV
      REAL*8  PROF, SOM
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Indices dans VPRNO
      IPU   = 1
      IPV   = 2
      IPH   = 3
      IPDV  = 4
      IPDH  = 5
      IPKXX = LM_CMMN_NPRNOL + 1
      IPKXY = LM_CMMN_NPRNOL + 2
      IPKYY = LM_CMMN_NPRNOL + 3
      IPHU  = LM_CMMN_NPRNOL + 4
      IPHV  = LM_CMMN_NPRNOL + 5

C---     Limite les profondeurs
      DO IN=1,EG_CMMN_NNL
         VPRNO(IPH,IN) = MAX(VPRNO(IPH,IN), CD2D_DECOU_HMIN)
      ENDDO

C---     Calcule les diffusivités
      UN_DH = UN / (CD2D_DECOU_HTRIG - CD2D_DECOU_HMIN)
!      FL = 6.0D-00 * CD2D_DECOU_CONV * CD2D_DECOU_VDIMP
!      FT = 6.0D-01 * CD2D_DECOU_CONV * CD2D_DECOU_VDIMP
      DO IN = 1,EG_CMMN_NNL

         U = VPRNO(IPU,IN)
         V = VPRNO(IPV,IN)
         H = VPRNO(IPH,IN)

C---        Direction de la vitesse
         W = SIGN( MAX(ABS(U), 1.0D-15), U)
         IF (ABS(W) .LE. PETIT) W = SIGN(PETIT, W)
         TETA = ATAN2(V, W)
         SI = SIN(TETA)
         CO = COS(TETA)

C---        ALFA, facteur de découvrement
         ALFA = (H - CD2D_DECOU_HMIN) * UN_DH
         ALFA = MIN(UN, MAX(ZERO, ALFA))
         VN1 = UN - ALFA
         VN2 = ALFA

C---        Advection - Couvert
         CU_C = 1.0D0
         CV_C = 1.0D0

C---        Advection - Découvert
         CU_D = CD2D_DECOU_CONV  ! FL*CO - FT*SI
         CV_D = CD2D_DECOU_CONV  ! FL*SI + FT*CO

C---        Dispersion - Couvert
         D0 = CD2D_DIFFU_MOLEC
         AKT_C = CD2D_DIFFU_CMULT_VERT *VPRNO(IPDV,IN)
     &         + CD2D_DIFFU_CMULT_HORIZ*VPRNO(IPDH,IN)
         AKL_C = CD2D_DIFFU_CMULT_LONG*AKT_C + D0
         AKT_C =                       AKT_C + D0

C---        Dispersion - Découvert
         D0 = CD2D_DIFFU_MOLEC * CD2D_DECOU_TORTUOSITE
         AKT_D = CD2D_DECOU_DIFFU_HORIZ
         AKL_D = CD2D_DECOU_CMULT_LONG*AKT_D + D0
         AKT_D =                       AKT_D + D0

C---        Composantes du tenseur de dispersion
         AKT = VN1*AKT_D + VN2*AKT_C
         AKL = VN1*AKL_D + VN2*AKL_C
         VPRNO(IPKXX,IN) = AKL*CO*CO + AKT*SI*SI
         VPRNO(IPKXY,IN) = (AKL-AKT)*SI*CO
         VPRNO(IPKYY,IN) = AKL*SI*SI + AKT*CO*CO

C---        Composantes de la convection-incomplète
         CU = VN1*CU_D + VN2*CU_C
         CV = VN1*CV_D + VN2*CV_C
         VPRNO(IPHU,IN) = CU * H * U
         VPRNO(IPHV,IN) = CV * H * V
      ENDDO

C---     Contrôles des paramètres physiques
      DO IN=1,EG_CMMN_NNL
         SOM = VPRNO(IPKXX,IN) + VPRNO(IPKXY,IN) + VPRNO(IPKYY,IN)
         IF (SOM .LE. PETIT) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_PROP_NODALE_NULLE')
            WRITE(LOG_BUF,'(A,I9)') 'ERR_PROP_NODALE_NULLE: ', IN
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
      ENDDO

      IERR = ERR_TYP()
      RETURN
      END
