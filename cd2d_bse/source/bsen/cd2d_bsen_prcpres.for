C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D DE BASE SANS CINÉTIQUES.
C                FORMULATION NON-CONSERVATIVE POUR (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  CD2D_BSEN_PRCPRES
C
C Description:
C     Calcul des propriétés élémentaires indépendantes de VDLG
C     pour les éléments de surface
C
C     Propriétés:
C        1) hu1.n 2) hu2.n
C        3) HKxx1 4) HKxx2 5) HKxy1 6) HKxy2 7) HKyy1 8) HKyy2
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BSEN_PRCPRES(VCORG,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSEN_PRCPRES
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV(LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES(LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER IERR

      INCLUDE 'err.fi'

      REAL*8  HUP1, HVP1, HP1, AKXX1, AKXY1, AKYY1
      REAL*8  HUP2, HVP2, HP2, AKXX2, AKXY2, AKYY2
      REAL*8  TXN, TYN
      INTEGER NP1, NP2
      INTEGER IES, IPHU, IPHV, IPH, IPKXX, IPKXY, IPKYY
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPH   = 3
      IPKXX = LM_CMMN_NPRNOL + 1
      IPKXY = LM_CMMN_NPRNOL + 2
      IPKYY = LM_CMMN_NPRNOL + 3
      IPHU  = LM_CMMN_NPRNOL + 4
      IPHV  = LM_CMMN_NPRNOL + 5

C---     Boucle sur les éléments de surface
      DO IES = 1,EG_CMMN_NELS
         NP1 = KNGS(1,IES)
         NP2 = KNGS(2,IES)

         TXN = VDJS(1,IES)
         TYN = VDJS(2,IES)

         HUP1  = VPRNO(IPHU, NP1)
         HVP1  = VPRNO(IPHV, NP1)
         HP1   = VPRNO(IPH,  NP1)
         AKXX1 = VPRNO(IPKXX,NP1)
         AKXY1 = VPRNO(IPKXY,NP1)
         AKYY1 = VPRNO(IPKYY,NP1)
         HUP2  = VPRNO(IPHU, NP2)
         HVP2  = VPRNO(IPHV, NP2)
         HP2   = VPRNO(IPH,  NP2)
         AKXX2 = VPRNO(IPKXX,NP2)
         AKXY2 = VPRNO(IPKXY,NP2)
         AKYY2 = VPRNO(IPKYY,NP2)

         VPRES(1,IES) = HUP1*TYN - HVP1*TXN
         VPRES(2,IES) = HUP2*TYN - HVP2*TXN
         VPRES(3,IES) = (AKXX1+UN_3*AKXX2)*HP1 + UN_3*(AKXX1+AKXX2)*HP2
         VPRES(4,IES) = UN_3*(AKXX1+AKXX2)*HP1 + (AKXX1+UN_3*AKXX2)*HP2
         VPRES(5,IES) = (AKXY1+UN_3*AKXY2)*HP1 + UN_3*(AKXY1+AKXY2)*HP2
         VPRES(6,IES) = UN_3*(AKXY1+AKXY2)*HP1 + (AKXY1+UN_3*AKXY2)*HP2
         VPRES(7,IES) = (AKYY1+UN_3*AKYY2)*HP1 + UN_3*(AKYY1+AKYY2)*HP2
         VPRES(8,IES) = UN_3*(AKYY1+AKYY2)*HP1 + (AKYY1+UN_3*AKYY2)*HP2

      ENDDO

      IERR = ERR_TYP()
      RETURN
      END

