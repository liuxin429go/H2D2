C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D.
C                         FORMULATION NON-CONSERVATIVE POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de
C        transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire: CD2D_BSEN_ASMKE_V
C
C Description:
C     La fonction CD2D_BSEN_ASMKE_V calcul le matrice de rigidité
C     élémentaire due aux éléments de volume.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) Les écarts signés ralentissent incroyablement la convergence.
C        On utilise ici les écarts en valeur absolue.
C     2) On devrait calculer la rigidité, d'abord cont puis lin.
C        La première implantation, (en fait erronée!! nais qui marche)
C        calcule plutôt la rigidité comme le serait la force, lin. puis quad.
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKE_V(VKE,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VSOLR,
     &                             VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE   (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
      REAL*8   VDJV  (EG_CMMN_NDJV )
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRN  (LM_CMMN_NPRNO, LM_CMMN_NNELV)
      REAL*8   VPRE  (LM_CMMN_NPREV)
      REAL*8   VSOLR (LM_CMMN_NSOLR, LM_CMMN_NNELV)
      REAL*8   VDLE  (LM_CMMN_NDLN,  LM_CMMN_NNELV)

      INCLUDE 'cd2d_bse.fc'

      REAL*8  CDMP, SDMP, DMP1, DMP2, DMP3, DMPS, D, E, R
      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DETJ
      REAL*8  CPEC, CLAP, DISP, CONV, SOLR, SPEC, SLAP
      REAL*8  DELX, DELY, AKXX, AKXY, AKYY
      REAL*8  C1, C2, C3, H1, H2, H3, HU1, HU2, HU3, HV1, HV2, HV3
      REAL*8  SHU, SHV, ASHU, ASHV
      REAL*8  DFX1, DFX2, DFX3, DFY1, DFY2, DFY3
      REAL*8  CUX1, CUX2, CUX3, CUY1, CUY2, CUY3
      REAL*8  STBX, STBY, STBX1, STBX2, STBX3, STBY1, STBY2, STBY3
      REAL*8  CX, CY, DCNRM
      REAL*8  SOL11,SOL12,SOL13, SOL22,SOL23, SOL33
      REAL*8  VKE11,VKE21,VKE31, VKE12,VKE22,VKE32, VKE13,VKE23,VKE33
      REAL*8  QH1, QH2, QH3, SQH
      REAL*8  DIVQ, SDIV
      INTEGER ID, ID1, ID2, ID3
      INTEGER IPH, IPHU, IPHV
C-----------------------------------------------------------------------
      INTEGER IN
      LOGICAL ESTWD
      ESTWD(IN) = ((CD2D_DECOU_CLIM .GE. 10) .AND.
     &             (VPRN(IPH,IN) .LE. CD2D_DECOU_HMIN))
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPH  = 3
      IPHU = LM_CMMN_NPRNOL + 4
      IPHV = LM_CMMN_NPRNOL + 5

C---     Prop. globales
      CPEC = UN / CD2D_STABI_PECLET    ! Peclet
      CLAP = CD2D_STABI_LAPIDUS        ! Lapidus
      CDMP = CD2D_STABI_DMP_RGD

C---     Métriques du t3
      VKX  = VDJV(1)
      VEX  = VDJV(2)
      VKY  = VDJV(3)
      VEY  = VDJV(4)
      DETJ = VDJV(5)
      VSX  = -(VKX+VEX)
      VSY  = -(VKY+VEY)

C---     Constantes dépendant du volume
      DISP  = UN_24/DETJ            ! Dispersion
      CONV  = UN_24*CD2D_CMULT_CONV ! Convection
      SDIV  = UN_24*CD2D_CMULT_DIVV ! Divergence
      SOLR  = UN_120*DETJ           ! Sol. réparties
      SPEC  = UN_6*CPEC/DETJ        ! Stabilisation Peclet
      SLAP  = UN_2*CLAP/DETJ        ! Stabilisation Lapidus
      SDMP  = UN_6*CDMP*DETJ        ! Stabilisation amortissement

C---     Initialisation des propriétés élémentaires
      DELX = VPRE(1)
      DELY = VPRE(2)
      AKXX = VPRE(4)
      AKXY = VPRE(5)
      AKYY = VPRE(6)

C---     Initialisation des propriétés nodales
      H1  = VPRN(IPH,1)
      H2  = VPRN(IPH,2)
      H3  = VPRN(IPH,3)
      HU1 = VPRN(IPHU,1)
      HV1 = VPRN(IPHV,1)
      HU2 = VPRN(IPHU,2)
      HV2 = VPRN(IPHV,2)
      HU3 = VPRN(IPHU,3)
      HV3 = VPRN(IPHV,3)
      SHU = HU1 + HU2 + HU3
      SHV = HV1 + HV2 + HV3
      ASHU = ABS(HU1) + ABS(HU2) + ABS(HU3)
      ASHV = ABS(HV1) + ABS(HV2) + ABS(HV3)

C---     Dispersion
      DFX1 = DISP*(AKXX*VSX + AKXY*VSY)
      DFX2 = DISP*(AKXX*VKX + AKXY*VKY)
      DFX3 = DISP*(AKXX*VEX + AKXY*VEY)
      DFY1 = DISP*(AKXY*VSX + AKYY*VSY)
      DFY2 = DISP*(AKXY*VKX + AKYY*VKY)
      DFY3 = DISP*(AKXY*VEX + AKYY*VEY)

C---     Convection
      CUX1 = CONV*(SHU+HU1)
      CUX2 = CONV*(SHU+HU2)
      CUX3 = CONV*(SHU+HU3)
      CUY1 = CONV*(SHV+HV1)
      CUY2 = CONV*(SHV+HV2)
      CUY3 = CONV*(SHV+HV3)

C---     Stabilisation de la convection (Peclet)
      STBX  = SPEC * DELX * ASHU
      STBY  = SPEC * DELY * ASHV
      STBX1 = STBX * VSX
      STBX2 = STBX * VKX
      STBX3 = STBX * VEX
      STBY1 = STBY * VSY
      STBY2 = STBY * VKY
      STBY3 = STBY * VEY

C---     Divergence du débit
      DIVQ = SDIV * (VKX*(HU2-HU1) + VEX*(HU3-HU1) +
     &               VKY*(HV2-HV1) + VEY*(HV3-HV1))

C---     Termes communs à tous les ddl
      VKE11 = (VSX*DFX1 + VSY*DFY1) + (VSX*CUX1 + VSY*CUY1) +
     .        (VSX*STBX1 + VSY*STBY1)
      VKE21 = (VKX*DFX1 + VKY*DFY1) + (VSX*CUX2 + VSY*CUY2) +
     .        (VKX*STBX1 + VKY*STBY1)
      VKE31 = (VEX*DFX1 + VEY*DFY1) + (VSX*CUX3 + VSY*CUY3) +
     .        (VEX*STBX1 + VEY*STBY1)
      VKE12 = (VSX*DFX2 + VSY*DFY2) + (VKX*CUX1 + VKY*CUY1) +
     .        (VSX*STBX2 + VSY*STBY2)
      VKE22 = (VKX*DFX2 + VKY*DFY2) + (VKX*CUX2 + VKY*CUY2) +
     .        (VKX*STBX2 + VKY*STBY2)
      VKE32 = (VEX*DFX2 + VEY*DFY2) + (VKX*CUX3 + VKY*CUY3) +
     .        (VEX*STBX2 + VEY*STBY2)
      VKE13 = (VSX*DFX3 + VSY*DFY3) + (VEX*CUX1 + VEY*CUY1) +
     .        (VSX*STBX3 + VSY*STBY3)
      VKE23 = (VKX*DFX3 + VKY*DFY3) + (VEX*CUX2 + VEY*CUY2) +
     .        (VKX*STBX3 + VKY*STBY3)
      VKE33 = (VEX*DFX3 + VEY*DFY3) + (VEX*CUX3 + VEY*CUY3) +
     .        (VEX*STBX3 + VEY*STBY3)

C---     Boucle sur les DDL
      DO ID=1,LM_CMMN_NDLN
         ID1 = ID
         ID2 = ID1+LM_CMMN_NDLN
         ID3 = ID2+LM_CMMN_NDLN

C---        Degrés de liberté (pour Lapidus)
         C1 = VDLE(ID, 1)
         C2 = VDLE(ID, 2)
         C3 = VDLE(ID, 3)
         CX = (VSX*C1 + VKX*C2 + VEX*C3)
         CY = (VSY*C1 + VKY*C2 + VEY*C3)

C---        Amortissement
         IF (ESTWD(1)) THEN   ! (c.f. note 1)
            D =  ABS(C1 - CD2D_DECOU_VDIMP)
         ELSE
            D = (MAX(C1, CD2D_STABI_DMP_MAX) - CD2D_STABI_DMP_MAX) - ! Écart par rapport aux bornes
     &          (MIN(C1, CD2D_STABI_DMP_MIN) - CD2D_STABI_DMP_MIN)
         ENDIF
         E = D / CD2D_STABI_DMP_LIN                                  ! Écart relatif vs la limite de linéarité
!!!         R = MAX(1.0D0, E)                                           ! Rigidité relative
         R = MAX(E, E**CD2D_STABI_DMP_EXP)                           ! Rigidité relative (c.f. note 2)
         DMP1 = SDMP * H1 * D * R
         IF (ESTWD(2)) THEN
            D =  ABS(C2 - CD2D_DECOU_VDIMP)
         ELSE
            D = (MAX(C2, CD2D_STABI_DMP_MAX) - CD2D_STABI_DMP_MAX) - ! Écart par rapport aux bornes
     &          (MIN(C2, CD2D_STABI_DMP_MIN) - CD2D_STABI_DMP_MIN)
         ENDIF
         E = D / CD2D_STABI_DMP_LIN                                  ! Écart relatif vs la limite de linéarité
         R = MAX(E, E**CD2D_STABI_DMP_EXP)                           ! Rigidité relative
         DMP2 = SDMP * H2 * D * R
         IF (ESTWD(3)) THEN
            D =  ABS(C3 - CD2D_DECOU_VDIMP)
         ELSE
            D = (MAX(C3, CD2D_STABI_DMP_MAX) - CD2D_STABI_DMP_MAX) - ! Écart par rapport aux bornes
     &          (MIN(C3, CD2D_STABI_DMP_MIN) - CD2D_STABI_DMP_MIN)
         ENDIF
         E = D / CD2D_STABI_DMP_LIN                                  ! Écart relatif vs la limite de linéarité
         R = MAX(E, E**CD2D_STABI_DMP_EXP)                           ! Rigidité relative
         DMP3 = SDMP * H3 * D * R
         DMPS = DMP1 + DMP2 + DMP3

C---        Stabilisation de Lapidus
         DCNRM = SLAP * MAX(HYPOT(CX, CY), 1.0D-12)
         STBX1 = DCNRM * VSX
         STBX2 = DCNRM * VKX
         STBX3 = DCNRM * VEX
         STBY1 = DCNRM * VSY
         STBY2 = DCNRM * VKY
         STBY3 = DCNRM * VEY

C---        Sollicitations réparties (Q en entrée)
         QH1   = SOLR * VSOLR(ID,1)
         QH2   = SOLR * VSOLR(ID,2)
         QH3   = SOLR * VSOLR(ID,3)
         SQH   = DEUX*(QH1 + QH2 + QH3)
         SOL11 = (SQH + QUATRE*QH1) + DIVQ+DIVQ + DMPS+DMP1
         SOL12 = (SQH - QH3)        + DIVQ
         SOL13 = (SQH - QH2)        + DIVQ
         SOL22 = (SQH + QUATRE*QH2) + DIVQ+DIVQ + DMPS+DMP2
         SOL23 = (SQH - QH1)        + DIVQ
         SOL33 = (SQH + QUATRE*QH3) + DIVQ+DIVQ + DMPS+DMP3

C---        Assemblage des contributions
         VKE(ID1,ID1) = VKE11 + (SOL11 + VSX*STBX1 + VSY*STBY1)
         VKE(ID2,ID1) = VKE21 + (SOL12 + VKX*STBX1 + VKY*STBY1)
         VKE(ID3,ID1) = VKE31 + (SOL13 + VEX*STBX1 + VEY*STBY1)
         VKE(ID1,ID2) = VKE12 + (SOL12 + VSX*STBX2 + VSY*STBY2)
         VKE(ID2,ID2) = VKE22 + (SOL22 + VKX*STBX2 + VKY*STBY2)
         VKE(ID3,ID2) = VKE32 + (SOL23 + VEX*STBX2 + VEY*STBY2)
         VKE(ID1,ID3) = VKE13 + (SOL13 + VSX*STBX3 + VSY*STBY3)
         VKE(ID2,ID3) = VKE23 + (SOL23 + VKX*STBX3 + VKY*STBY3)
         VKE(ID3,ID3) = VKE33 + (SOL33 + VEX*STBX3 + VEY*STBY3)
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMK_S
C
C Description:
C     La fonction CD2D_BSEN_ASMK_S calcule le matrice de rigidité
C     élémentaire due aux éléments de surface.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Il s'agit plus de CL que de surface
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKE_S(VKE,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VDLEV,
     &                             KEIMP,
     &                             ICOTE)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VDJV  (EG_CMMN_NDJV)
      REAL*8   VDJS  (EG_CMMN_NDJS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV)
      REAL*8   VPRES (LM_CMMN_NPRES)
      REAL*8   VDLEV (LM_CMMN_NDLN)
      INTEGER  KEIMP
      INTEGER  ICOTE

      INTEGER ID, ID1, ID2, ID3
      REAL*8  VSX, VKX, VEX, VNX
      REAL*8  VSY, VKY, VEY, VNY
      REAL*8  DETJL2, DETJT3
      REAL*8  QN1, QN2, SQN
      REAL*8  COEF
      REAL*8  AKXXH1, AKXYH1, AKYYH1
      REAL*8  AKXXH2, AKXYH2, AKYYH2
      REAL*8  D11, D12, D13, D21, D22, D23
      REAL*8  VKE11, VKE21, VKE12, VKE22, VKE13, VKE23
      REAL*8  VDJX(4), VDJY(4), VDJT3(5)
C-----------------------------------------------------------------------
      LOGICAL ELE_ESTTYPE1
      LOGICAL ELE_ESTTYPE2
      ELE_ESTTYPE1() = BTEST(KEIMP, EA_TPCL_ENTRANT) .AND.
     &                   (BTEST(KEIMP, EA_TPCL_CAUCHY) .OR.
     &                    BTEST(KEIMP, EA_TPCL_OUVERT))
      ELE_ESTTYPE2() = BTEST(KEIMP, EA_TPCL_SORTANT) .AND.
     &                   BTEST(KEIMP, EA_TPCL_OUVERT)
C-----------------------------------------------------------------------

C---     Monte les tables aux. pour permuter les métriques
      VDJX(1) = VDJV(1)
      VDJX(2) = VDJV(2)
      VDJX(3) = -(VDJX(1)+VDJX(2))
      VDJX(4) = VDJX(1)
      VDJY(1) = VDJV(3)
      VDJY(2) = VDJV(4)
      VDJY(3) = -(VDJY(1)+VDJY(2))
      VDJY(4) = VDJY(1)

C---     Métriques permutées de l'élément de volume
      VDJT3(1) = VDJX(ICOTE)
      VDJT3(2) = VDJX(ICOTE+1)
      VDJT3(3) = VDJY(ICOTE)
      VDJT3(4) = VDJY(ICOTE+1)
      VDJT3(5) = VDJV(5)


C---     CAUCHY "ENTRANTS"
C        =================
      IF (ELE_ESTTYPE1()) THEN
C---        Constante dépendant du volume
         DETJL2 = VDJS(3)
         COEF = -UN_6*DETJL2

C---        Flux normal (Vn*H)
         QN1 = COEF*VPRES(1)
         QN2 = COEF*VPRES(2)
         SQN = QN1 + QN2

C---        Matrice de rigidité élémentaire de CAUCHY-ENTRANT
!         VKE11 = SQN + QN1+QN1
!         VKE21 = SQN
!         VKE12 = SQN
!         VKE22 = SQN + QN2+QN2
         VKE11 = SQN+SQN + QN1+QN1    ! Matrice lumpée
         VKE22 = SQN+SQN + QN2+QN2

C---        Boucle sur les ddl
         DO ID=1,LM_CMMN_NDLN
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN

            VKE(ID1,ID1) = VKE11
!            VKE(ID2,ID1) = VKE21
!            VKE(ID1,ID2) = VKE12
            VKE(ID2,ID2) = VKE22
         ENDDO
      ENDIF

C---     OUVERTS "SORTANTS"
C        ==================
      IF (ELE_ESTTYPE2()) THEN

C---        Métriques du T3 de volume (permuté)
         VKX    = VDJT3(1)               ! Ksi,x
         VEX    = VDJT3(2)               ! Eta,x
         VKY    = VDJT3(3)               ! Ksi,y
         VEY    = VDJT3(4)               ! Eta,y
         DETJT3 = VDJT3(5)
         VSX    = -(VKX+VEX)
         VSY    = -(VKY+VEY)

C---        Métriques de l'élément de surface - composantes de la normale extérieure
         VNY    = -VDJS(1)
         VNX    =  VDJS(2)
         DETJL2 =  VDJS(3)

C---        Constante dépendant du contour
         COEF = -UN_2*DETJL2/DETJT3

C---        Propriétés nodales du contour
         AKXXH1 = VPRES(3)
         AKXXH2 = VPRES(4)
         AKXYH1 = VPRES(5)
         AKXYH2 = VPRES(6)
         AKYYH1 = VPRES(7)
         AKYYH2 = VPRES(8)

C---        Termes de contour
         D11 = (AKXXH1*VSX+AKXYH1*VSY)*VNX + (AKXYH1*VSX+AKYYH1*VSY)*VNY
         D12 = (AKXXH1*VKX+AKXYH1*VKY)*VNX + (AKXYH1*VKX+AKYYH1*VKY)*VNY
         D13 = (AKXXH1*VEX+AKXYH1*VEY)*VNX + (AKXYH1*VEX+AKYYH1*VEY)*VNY
         D21 = (AKXXH2*VSX+AKXYH2*VSY)*VNX + (AKXYH2*VSX+AKYYH2*VSY)*VNY
         D22 = (AKXXH2*VKX+AKXYH2*VKY)*VNX + (AKXYH2*VKX+AKYYH2*VKY)*VNY
         D23 = (AKXXH2*VEX+AKXYH2*VEY)*VNX + (AKXYH2*VEX+AKYYH2*VEY)*VNY

C---        Matrice élémentaire de frontière ouverte
         VKE11 = COEF*D11
         VKE21 = COEF*D21
         VKE12 = COEF*D12
         VKE22 = COEF*D22
         VKE13 = COEF*D13
         VKE23 = COEF*D23

C---        BOUCLE D'ASSEMBLAGE SUR LES DDL
         DO ID=1,LM_CMMN_NDLN
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN
            ID3 = ID2+LM_CMMN_NDLN
            VKE(ID1,ID1) = VKE11
            VKE(ID2,ID1) = VKE21
            VKE(ID1,ID2) = VKE12
            VKE(ID2,ID2) = VKE22
            VKE(ID1,ID3) = VKE13
            VKE(ID2,ID3) = VKE23
         ENDDO
      ENDIF

      RETURN
      END

