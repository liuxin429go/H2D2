C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de transport-diffusion
C         de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire:  CD2D_BSE_HLPCLIM
C
C Description:
C     Aide sur les conditions limites:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSE_HLPCLIM ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_HLPCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fi'

      INTEGER I
      INTEGER IERR
C-----------------------------------------------------------------------

      I = 0

C---     IMPRESSION DE L'INFO
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_CL_CD2D:') ! CONDITIONS LIMITES DE TRANSPORT-DIFFUSION
      CALL LOG_INCIND()

      I = I + 1
      IERR = CD2D_BSE_HLP1(I, 'MSG_CL_DIRICHLET')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I, 'MSG_CL_CAUCHY')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I, 'MSG_CL_OUVERT')

      CALL LOG_DECIND()

      RETURN
      END

