C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER CD2D_BSE_PST_DIF_000
C     INTEGER CD2D_BSE_PST_DIF_999
C     INTEGER CD2D_BSE_PST_DIF_CTR
C     INTEGER CD2D_BSE_PST_DIF_DTR
C     INTEGER CD2D_BSE_PST_DIF_INI
C     INTEGER CD2D_BSE_PST_DIF_RST
C     INTEGER CD2D_BSE_PST_DIF_REQHBASE
C     LOGICAL CD2D_BSE_PST_DIF_HVALIDE
C     INTEGER CD2D_BSE_PST_DIF_ACC
C     INTEGER CD2D_BSE_PST_DIF_FIN
C     INTEGER CD2D_BSE_PST_DIF_XEQ
C     INTEGER CD2D_BSE_PST_DIF_ASGHSIM
C     INTEGER CD2D_BSE_PST_DIF_REQHVNO
C     CHARACTER*256 CD2D_BSE_PST_DIF_REQNOMF
C     INTEGER CD2D_BSE_PST_DIF_CLC
C   Private:
C     INTEGER CD2D_BSE_PST_DIF_CLC2
C     INTEGER CD2D_BSE_PST_DIF_LOG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CD2D_BSE_PST_DIF_NOBJMAX,
     &                   CD2D_BSE_PST_DIF_HBASE,
     &                   'CD2D - Post-traitement Diffusivités')

      CD2D_BSE_PST_DIF_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER  IERR
      EXTERNAL CD2D_BSE_PST_DIF_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CD2D_BSE_PST_DIF_NOBJMAX,
     &                   CD2D_BSE_PST_DIF_HBASE,
     &                   CD2D_BSE_PST_DIF_DTR)

      CD2D_BSE_PST_DIF_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CD2D_BSE_PST_DIF_NOBJMAX,
     &                   CD2D_BSE_PST_DIF_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
         IOB = HOBJ - CD2D_BSE_PST_DIF_HBASE

         CD2D_BSE_PST_DIF_HPRNT(IOB) = 0
         CD2D_BSE_PST_DIF_LPST (IOB) = 0
      ENDIF

      CD2D_BSE_PST_DIF_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CD2D_BSE_PST_DIF_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CD2D_BSE_PST_DIF_NOBJMAX,
     &                   CD2D_BSE_PST_DIF_HBASE)

      CD2D_BSE_PST_DIF_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL CD2D_BSE_PST_DIF_CLC
      EXTERNAL CD2D_BSE_PST_DIF_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = CD2D_BSE_PST_DIF_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,
     &                                      HOBJ, CD2D_BSE_PST_DIF_CLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFLOG,
     &                                      HOBJ, CD2D_BSE_PST_DIF_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD()) IERR = PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD()) IERR = PS_SIMU_INI(HPRNT,
     &                                   HFCLC,  HFLOG,
     &                                   NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - CD2D_BSE_PST_DIF_HBASE
         CD2D_BSE_PST_DIF_HPRNT(IOB) = HPRNT
      ENDIF

      CD2D_BSE_PST_DIF_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - CD2D_BSE_PST_DIF_HBASE

C---     Détruis les données
      HPRNT = CD2D_BSE_PST_DIF_HPRNT(IOB)
      IF (PS_SIMU_HVALIDE(HPRNT)) THEN
         IERR = PS_SIMU_DTR(HPRNT)
      ENDIF

C---     Reset
      CD2D_BSE_PST_DIF_HPRNT(IOB) = 0

      CD2D_BSE_PST_DIF_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CD2D_BSE_PST_DIF_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'
C------------------------------------------------------------------------

      CD2D_BSE_PST_DIF_REQHBASE = CD2D_BSE_PST_DIF_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CD2D_BSE_PST_DIF_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'
C------------------------------------------------------------------------

      CD2D_BSE_PST_DIF_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                     CD2D_BSE_PST_DIF_NOBJMAX,
     &                                     CD2D_BSE_PST_DIF_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = CD2D_BSE_PST_DIF_HPRNT(HOBJ-CD2D_BSE_PST_DIF_HBASE)
      IERR = PS_SIMU_ACC(HPRNT)

      CD2D_BSE_PST_DIF_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = CD2D_BSE_PST_DIF_HPRNT(HOBJ-CD2D_BSE_PST_DIF_HBASE)
      IERR = PS_SIMU_FIN(HPRNT)

      CD2D_BSE_PST_DIF_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = CD2D_BSE_PST_DIF_HPRNT(HOBJ-CD2D_BSE_PST_DIF_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, CD2D_BSE_PST_DIF_NPOST)

      CD2D_BSE_PST_DIF_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSE_PST_DIF_ASGHSIM
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le contrôle de cohérence est très faible.
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = CD2D_BSE_PST_DIF_HPRNT(HOBJ-CD2D_BSE_PST_DIF_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, CD2D_BSE_PST_DIF_NPOST)

      CD2D_BSE_PST_DIF_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSE_PST_DIF_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = CD2D_BSE_PST_DIF_HPRNT(HOBJ-CD2D_BSE_PST_DIF_HBASE)
      CD2D_BSE_PST_DIF_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSE_PST_DIF_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = CD2D_BSE_PST_DIF_HPRNT(HOBJ-CD2D_BSE_PST_DIF_HBASE)
      CD2D_BSE_PST_DIF_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: CD2D_PST_DIF_CLC
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_CLC(HOBJ,
     &                              HELE,
     &                              NPST,
     &                              NNL,
     &                              VPOST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PST_DIF_CLC
CDEC$ ENDIF

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELE
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IERR
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C------------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les données
      GDTA => LM_GEOM_REQGDTA(HELE)
      EDTA => LM_ELEM_REQEDTA(HELE)

C---     Fait le calcul
      IERR = CD2D_BSE_PST_DIF_CLC2(HOBJ,
     &                             EDTA%VPRGL,
     &                             EDTA%VPRNO,
     &                             NPST,
     &                             NNL,
     &                             VPOST)

      CD2D_BSE_PST_DIF_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PST_DIF_CLC2
C
C Description:
C     La fonction privée CD2D_BSE_PST_DIF_CLC2 fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_CLC2(HOBJ,
     &                               VPRGL,
     &                               VPRNO,
     &                               NPST,
     &                               NNL,
     &                               VPOST)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER HOBJ
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'cd2d_bse.fc'
      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER ID, IN
      INTEGER IPDV, IPDH
      REAL*8  AKT, AKL, D0

      PARAMETER (IPDV = 4)
      PARAMETER (IPDH = 5)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .EQ. CD2D_BSE_PST_DIF_NPOST)
D     CALL ERR_PRE(NNL  .EQ. EG_CMMN_NNL)
C-----------------------------------------------------------------------

C---     Les diffusivités longitudinales et tangentielles
      DO IN = 1,EG_CMMN_NNL
         D0 = CD2D_DIFFU_MOLEC
         AKT = CD2D_DIFFU_CMULT_VERT *VPRNO(IPDV,IN)
     &       + CD2D_DIFFU_CMULT_HORIZ*VPRNO(IPDH,IN)
         AKL = CD2D_DIFFU_CMULT_LONG*AKT + D0
         AKT =                       AKT + D0

         VPOST(1,IN) = AKL
         VPOST(2,IN) = AKT
      ENDDO

C---     Les composantes du tenseur de diffusivité
      CALL DCOPY(EG_CMMN_NNL,
     &           VPRNO(LM_CMMN_NPRNOL+1, 1), LM_CMMN_NPRNO,
     &           VPOST(3, 1), NPST)
      CALL DCOPY(EG_CMMN_NNL,
     &           VPRNO(LM_CMMN_NPRNOL+2, 1), LM_CMMN_NPRNO,
     &           VPOST(4, 1), NPST)
      CALL DCOPY(EG_CMMN_NNL,
     &           VPRNO(LM_CMMN_NPRNOL+3, 1), LM_CMMN_NPRNO,
     &           VPOST(5, 1), NPST)

      CD2D_BSE_PST_DIF_CLC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PST_DIF_LOG
C
C Description:
C     La fonction privée CD2D_BSE_PST_DIF_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PST_DIF_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'cd2d_bse_pst_dif.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'cd2d_bse_pst_dif.fc'

      INTEGER IERR
      INTEGER IP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(CD2D_BSE_PST_DIF_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .EQ. CD2D_BSE_PST_DIF_NPOST)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_CD2D_POST_DIFF:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<35>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)')    'MSG_VALEUR#<35>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR, 1, NPST, NNL, VPOST, 'akl')
      IERR = PS_PSTD_LOGVAL(HNUMR, 2, NPST, NNL, VPOST, 'akt')
      IERR = PS_PSTD_LOGVAL(HNUMR, 3, NPST, NNL, VPOST, 'dxx')
      IERR = PS_PSTD_LOGVAL(HNUMR, 4, NPST, NNL, VPOST, 'dxy')
      IERR = PS_PSTD_LOGVAL(HNUMR, 5, NPST, NNL, VPOST, 'dyy')
      CALL LOG_DECIND()

      CALL LOG_DECIND()

      CD2D_BSE_PST_DIF_LOG = ERR_TYP()
      RETURN
      END
