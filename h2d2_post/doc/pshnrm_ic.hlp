class post_hessian_norm
=======================
 
   The class **post_hessian_norm** represents the post-treatment that computes
   the approximation errors as the norm of the Hessian of a variable. This
   post-treatment should not be used as a post-treatment during resolution.
    
   constructor handle post_hessian_norm(hvno, ivno, nomt, nomfic, mode, oprd,
                                       opwr)
      The constructor **post_hessian_norm** constructs an object, with the
      given arguments, and returns a handle on this object.
         handle   HOBJ        Return value: Handle on the post-treatment
         handle   hvno        Handle on the nodal values
         integer  ivno        Index on the nodal value on which the error will
                           be calculated
         string   nomt        Type of computation ['green', 'dxdx',
                           'recovery']
         string   nomfic      Name of the post-treatment file
         string   mode        Opening mode ['w', 'a'] (default 'a')
         string   oprd        Reduction operation ['sum', 'sumabs', 'min',
                           'minabs', 'max', 'maxabs', 'mean'] (default 'noop')
         string   opwr        Writing operation ['write', 'nowrite'] (default
                           'nowrite')
    
   getter hvno
      Handle on the underlying nodal values
    
   method accumulate(hsim)
      The method **accumulate** add the present post-treatment values. Values
      are reduced according to the reduction operation, and written to file
      according to the writing operation. Simulation data must be loaded.
         handle   hsim        Handle on the simulation
    
   method finalize()
      The method **finalize** finalizes the post-treatment process. It shall
      be used to write to file the results when the option 'nowrite' is used.
    
   method xeq(hsim)
      The method **xeq** executes the post-treatment process. It is a
      combination of a call to **accumulate** followed by a call to
      **finalize**.
         handle   hsim        Handle on the simulation
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
