C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_PS_XEQ_FIN
C     INTEGER IC_PS_XEQ_XEQ
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_PS_XEQ_ACC est la version générique d'exécution de
C     post-traitement. Elle contrôle les paramètres, puis appelle la méthode
C     virtuelle PS_POST_ACC sur l'objet HPOST.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_XEQ_ACC(HPST, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_XEQ_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HPST
      CHARACTER*(*) IPRM

      INCLUDE 'psxeq_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HSIM
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     LIS LES PARAM
      HSIM = 0
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the simulation</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HSIM)
      IF (IERR .NE. 0) GOTO 9901

C---     VALIDE
      IF (.NOT. PS_POST_HVALIDE(HPST)) GOTO 9902
      IF (.NOT. LM_HELE_HVALIDE(HSIM)) GOTO 9903
      IF (.NOT. LM_HELE_ESTINI(HSIM)) GOTO 9904

C---     EXECUTION
      IF (ERR_GOOD()) IERR = PS_POST_ASGHSIM(HPST, HSIM)
      IF (ERR_GOOD()) IERR = PS_POST_ACC    (HPST)

C---     RETOURNE RIEN
      IPRM = ' '

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_PARAMETRE_INVALIDE',': ', HPST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_PARAMETRE_INVALIDE',': ', HSIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A)') 'ERR_SIMD_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PS_XEQ_ACC = ERR_TYP()
      RETURN
      END
      
C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_PS_XEQ_FIN est la version générique de finalisation du
C     post-traitement. Elle contrôle les paramètres, puis appelle la méthode
C     virtuelle PS_POST_FIN sur l'objet HPOST.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_XEQ_FIN(HPOST, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_XEQ_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HPOST
      CHARACTER*(*) IPRM

      INCLUDE 'psxeq_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     VALIDE
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9901
      IF (.NOT. PS_POST_HVALIDE(HPOST)) GOTO 9902

C---     EXECUTION
      IF (ERR_GOOD()) IERR = PS_POST_FIN(HPOST)

C---     RETOURNE RIEN
      IPRM = ' '

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_PARAMETRE_INVALIDE',': ', HPOST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PS_XEQ_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_PS_XEQ_XEQ est la version générique d'exécution de
C     post-traitement. Elle contrôle les paramètres, puis appelle la méthode
C     virtuelle PS_POST_XEQ sur l'objet HPOST.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_XEQ_XEQ(HPST, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_XEQ_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HPST
      CHARACTER*(*) IPRM

      INCLUDE 'psxeq_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HSIM
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     LIS LES PARAM
      HSIM = 0
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the simulation</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HSIM)
      IF (IERR .NE. 0) GOTO 9901

C---     VALIDE
      IF (.NOT. PS_POST_HVALIDE(HPST)) GOTO 9902
      IF (.NOT. LM_HELE_HVALIDE(HSIM)) GOTO 9903
      IF (.NOT. LM_HELE_ESTINI(HSIM)) GOTO 9904

C---     EXECUTION
      IF (ERR_GOOD()) IERR = PS_POST_XEQ(HPST, HSIM)

C---     RETOURNE RIEN
      IPRM = ' '

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_PARAMETRE_INVALIDE',': ', HPST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_PARAMETRE_INVALIDE',': ', HSIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A)') 'ERR_SIMD_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PS_XEQ_XEQ = ERR_TYP()
      RETURN
      END
