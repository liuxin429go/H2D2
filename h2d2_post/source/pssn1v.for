C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  PoSt-traitement
C Objet:   SoNde 1d on Vector
C Type:    Concret
C
C Functions:
C   Public:
C     INTEGER PS_SN1V_000
C     INTEGER PS_SN1V_999
C     INTEGER PS_SN1V_CTR
C     INTEGER PS_SN1V_DTR
C     INTEGER PS_SN1V_INI
C     INTEGER PS_SN1V_RST
C     INTEGER PS_SN1V_REQHBASE
C     LOGICAL PS_SN1V_HVALIDE
C     INTEGER PS_SN1V_ACC
C     INTEGER PS_SN1V_FIN
C     INTEGER PS_SN1V_XEQ
C     INTEGER PS_SN1V_ASGHSIM
C     CHARACTER*256 PS_SN1V_REQNOMF
C   Private:
C     INTEGER PS_SN1V_INTG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1V_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn1v.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PS_SN1V_NOBJMAX,
     &                   PS_SN1V_HBASE,
     &                   'Post-Treatment Probe 1D on vector')

      PS_SN1V_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1V_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn1v.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER  IERR
      EXTERNAL PS_SN1V_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PS_SN1V_NOBJMAX,
     &                   PS_SN1V_HBASE,
     &                   PS_SN1V_DTR)

      PS_SN1V_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1V_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1v.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PS_SN1V_NOBJMAX,
     &                   PS_SN1V_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PS_SN1V_HVALIDE(HOBJ))
         IOB = HOBJ - PS_SN1V_HBASE

         PS_SN1V_HSN1D(IOB) = 0
         PS_SN1V_HFITG(IOB) = 0
      ENDIF

      PS_SN1V_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1V_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1v.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1V_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PS_SN1V_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PS_SN1V_NOBJMAX,
     &                   PS_SN1V_HBASE)
      HOBJ = 0

      PS_SN1V_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les contrôles sont faits par PS_SN1D_INI.
C     Les tables sont dimensionnées au nombre de noeuds globaux alors
C     qu'elles pourraient l'être au nombre de noeuds locaux.
C     Chaque process a tous les points. Il faut une op de réduction à
C     la fin.
C************************************************************************
      FUNCTION PS_SN1V_INI(HOBJ,
     &                     NOMFIC,
     &                     ISTAT,
     &                     IOPR,
     &                     IOPW,
     &                     NDIM,
     &                     NPNT,
     &                     X1,
     &                     X2,
     &                     HVNO,
     &                     IDDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW
      INTEGER       NDIM
      INTEGER       NPNT
      REAL*8        X1(NDIM)
      REAL*8        X2(NDIM)
      INTEGER       HVNO(NDIM)
      INTEGER       IDDL(NDIM)

      INCLUDE 'pssn1v.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFITG, HSN1D

      EXTERNAL PS_SN1V_INTG
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1V_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = PS_SN1V_RST(HOBJ)

C---     Construit la fonction d'interpolation
      HFITG = 0
      IF (ERR_GOOD())IERR = SO_FUNC_CTR   (HFITG)
      IF (ERR_GOOD())IERR = SO_FUNC_INIFNC(HFITG, PS_SN1V_INTG)

C---     Construit et initialise le parent
      HSN1D = 0
      IF (ERR_GOOD())IERR = PS_SN1D_CTR(HSN1D)
      IF (ERR_GOOD())IERR = PS_SN1D_INI(HSN1D,
     &                                  NOMFIC,
     &                                  ISTAT,
     &                                  IOPR,
     &                                  IOPW,
     &                                  NDIM,
     &                                  NPNT,
     &                                  X1,
     &                                  X2,
     &                                  NDIM,   ! NVNO
     &                                  HVNO,
     &                                  IDDL,
     &                                  HFITG)

C---     Détruis la fonction en cas d'erreur
      IF (ERR_BAD() .AND. SO_FUNC_HVALIDE(HFITG))
     &   IERR = SO_FUNC_DTR (HFITG)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - PS_SN1V_HBASE
         PS_SN1V_HSN1D(IOB) = HSN1D
         PS_SN1V_HFITG(IOB) = HFITG
      ENDIF

      PS_SN1V_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Réinitialise
C
C Description:
C     La fonction PS_SN1V_RST réinitialise l'objet. La mémoire est
C     désallouée, les objets attributs détruis et tous les attributs
C     réinitialisés.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1V_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1v.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFITG, HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1V_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_SN1V_HBASE

C---     Détruis le parent
      HSN1D = PS_SN1V_HSN1D(IOB)
      IF (PS_SN1D_HVALIDE(HSN1D)) IERR = PS_SN1D_DTR(HSN1D)

C---     Détruis la fonction d'intégration
      HFITG = PS_SN1V_HSN1D(IOB)
      IF (SO_FUNC_HVALIDE(HFITG)) IERR = SO_FUNC_DTR(HFITG)

C---     RESET
      IF (ERR_GOOD()) THEN
         PS_SN1V_HSN1D(IOB) = 0
         PS_SN1V_HSN1D(IOB) = 0
      ENDIF

      PS_SN1V_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_SN1V_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1V_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn1v.fi'
      INCLUDE 'pssn1v.fc'
C------------------------------------------------------------------------

      PS_SN1V_REQHBASE = PS_SN1V_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_SN1V_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1V_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1v.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pssn1v.fc'
C------------------------------------------------------------------------

      PS_SN1V_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PS_SN1V_NOBJMAX,
     &                                  PS_SN1V_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     En principe, les données ont déjà été chargées
C************************************************************************
      FUNCTION PS_SN1V_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1v.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1V_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HSN1D = PS_SN1V_HSN1D(HOBJ-PS_SN1V_HBASE)
      PS_SN1V_ACC = PS_SN1D_ACC(HSN1D)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1V_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1v.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1V_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HSN1D = PS_SN1V_HSN1D(HOBJ-PS_SN1V_HBASE)
      PS_SN1V_FIN = PS_SN1D_FIN(HSN1D)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1V_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn1v.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1V_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

      HSN1D = PS_SN1V_HSN1D(HOBJ-PS_SN1V_HBASE)
      PS_SN1V_XEQ = PS_SN1D_XEQ(HSN1D, HSIM)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN1V_ASGHSIM
C
C Description:
C     La fonction PS_SN1V_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1V_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn1v.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1V_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HSN1D = PS_SN1V_HSN1D(HOBJ-PS_SN1V_HBASE)
      PS_SN1V_ASGHSIM = PS_SN1D_ASGHSIM(HSN1D, HSIM)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN1V_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1V_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1V_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1v.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1v.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1V_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HSN1D = PS_SN1V_HSN1D(HOBJ-PS_SN1V_HBASE)
      PS_SN1V_REQNOMF = PS_SN1D_REQNOMF(HSN1D)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN1V_INTG
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Interpolation linéaire
C     La normale et la tangente forment un système direct pour être
C     comme les frontières.
C     La normale est donnée par (dy, -dx) / ds
C************************************************************************
      FUNCTION PS_SN1V_INTG(NDIM,
     &                      NNL,
     &                      VCORG,
     &                      NNEL,
     &                      NELL,
     &                      KNGV,
     &                      NVNO,
     &                      VNOD,
     &                      VINT)

      IMPLICIT NONE

      INTEGER NDIM
      INTEGER NNL
      REAL*8  VCORG(NDIM, NNL)
      INTEGER NNEL
      INTEGER NELL
      INTEGER KNGV(NNEL, NELL)
      INTEGER NVNO
      REAL*8  VNOD(NVNO, NNL)
      REAL*8  VINT

      INCLUDE 'err.fi'
      INCLUDE 'pssn1v.fc'

      REAL*8  DX, DY, U, V
      INTEGER IE
      INTEGER NO1, NO2

      REAL*8, PARAMETER :: DEMI = 0.5000000000000000D0
C------------------------------------------------------------------------
C     CALL ERR_PRE(NDIM .EQ. 2)
C     CALL ERR_PRE(NVNO .EQ. NDIM)
C------------------------------------------------------------------------

      VINT = 0.0D0
      DO IE=1, NELL
         NO1 = KNGV(1, IE)
         NO2 = KNGV(2, IE)

         DX = VCORG(1, NO2) - VCORG(1, NO1)
         DY = VCORG(2, NO2) - VCORG(2, NO1)

         U  = VNOD(1, NO2) + VNOD(1, NO1)  ! * DEMI
         V  = VNOD(2, NO2) + VNOD(2, NO1)  ! * DEMI

         VINT = VINT + U*DY - V*DX         ! / DS * DS
      ENDDO
      VINT = DEMI*VINT

      PS_SN1V_INTG = ERR_TYP()
      RETURN
      END
