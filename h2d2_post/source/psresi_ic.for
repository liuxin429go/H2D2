C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_RESI_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_RESI_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'psresi_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'psresi.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'psresi_ic.fc'

      INTEGER IERR
      INTEGER ISTAT
      INTEGER HPST, HOBJ
      INTEGER IOPR, IOPW
      CHARACTER*(256) NOMFIC
      CHARACTER*(256) NTMP
      CHARACTER*(  8) MODE
      CHARACTER*(  8) OPRD, OPWR
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PS_RESI_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the post-treatment file</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Opening mode ['w', 'a'] (default 'a')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 2, MODE)
      IF (IERR .NE. 0) MODE = 'a'
C     <comment>Reduction operation ['sum', 'sumabs', 'min', 'minabs', 'max', 'maxabs', 'mean'] (default 'noop')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 3, OPRD)
      IF (IERR .NE. 0) OPRD = 'noop'
C     <comment>Writing operation ['write', 'nowrite'] (default 'nowrite')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 4, OPWR)
      IF (IERR .NE. 0) OPWR = 'nowrite'
      CALL SP_STRN_TRM(NOMFIC)

C---     VALIDE
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9904
      IOPR = PS_PSTD_STR2OP(OPRD(1:SP_STRN_LEN(OPRD)))
      IF (IOPR .EQ. PS_OP_INDEFINI .OR.
     &    IOPR .EQ. PS_OP_NOWRITE  .OR.
     &    IOPR .EQ. PS_OP_WRITE) GOTO 9905
      IOPW = PS_PSTD_STR2OP(OPWR(1:SP_STRN_LEN(OPWR)))
      IF (IOPW .NE. PS_OP_NOWRITE .AND.
     &    IOPW .NE. PS_OP_WRITE) GOTO 9906

C---     CONSTRUIS ET INITIALISE L'OBJET
      HPST = 0
      IF (ERR_GOOD()) IERR=PS_RESI_CTR(HPST)
      IF (ERR_GOOD()) IERR=PS_RESI_INI(HPST,NOMFIC,ISTAT,IOPR,IOPW)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_PS_RESI_PRN(HPST)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>post_residu</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_INVALIDE',': ', MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(3A)') 'ERR_OPRDUC_INVALIDE',': ', OPRD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF, '(3A)') 'ERR_OPWRITE_INVALIDE',': ', OPWR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_RESI_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_PS_RESI_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_RESI_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_RESI_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'psresi_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psresi.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'psresi_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Les méthodes virtuelles sont gérées par le proxy
D     IF (IMTH .EQ. 'xeq') CALL ERR_ASR(.FALSE.)
D     IF (IMTH .EQ. 'del') CALL ERR_ASR(.FALSE.)
C     <include>IC_PS_POST_XEQMTH@pspost_ic.for</include>

C     <comment>The method <b>print</b> prints information about the object.</comment>
      IF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(PS_RESI_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = PS_RESI_PRN(HOBJ)
         IERR = IC_PS_RESI_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PS_RESI_AID()

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_RESI_AID()

9999  CONTINUE
      IC_PS_RESI_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_RESI_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_RESI_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psresi_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C   The class <b>post_residu</b> represents the post-treatment that computes the residuum.
C</comment>
      IC_PS_RESI_REQCLS = 'post_residu'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_RESI_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_RESI_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psresi_ic.fi'
      INCLUDE 'psresi.fi'
C-------------------------------------------------------------------------

      IC_PS_RESI_REQHDL = PS_RESI_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_PS_RESI_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PS_RESI_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('psresi_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_RESI_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'psresi.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'psresi_ic.fc'

      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_POST_RESIDU'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<25>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      NOM = PS_RESI_REQNOMF(HOBJ)
      IF (SP_STRN_LEN(NOM) .NE. 0) THEN
         CALL SP_STRN_CLP(NOM, 50)
         WRITE (LOG_BUF,'(3A)')  'MSG_NOM_FICHIER#<25>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      CALL LOG_DECIND()

      IC_PS_RESI_PRN = ERR_TYP()
      RETURN
      END
      