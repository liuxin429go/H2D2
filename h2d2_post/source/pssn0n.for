C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  PoSt-traitement
C Objet:   SoNde 0d sur Nodes
C Type:    Concret
C
C Functions:
C   Public:
C     INTEGER PS_SN0N_000
C     INTEGER PS_SN0N_999
C     INTEGER PS_SN0N_CTR
C     INTEGER PS_SN0N_DTR
C     INTEGER PS_SN0N_INI
C     INTEGER PS_SN0N_RST
C     INTEGER PS_SN0N_REQHBASE
C     LOGICAL PS_SN0N_HVALIDE
C     INTEGER PS_SN0N_AJTNOD
C     INTEGER PS_SN0N_ACC
C     INTEGER PS_SN0N_FIN
C     INTEGER PS_SN0N_XEQ
C     INTEGER PS_SN0N_ASGHSIM
C     INTEGER PS_SN0N_REQHVNO
C     CHARACTER*256 PS_SN0N_REQNOMF
C   Private:
C     INTEGER PS_SN0N_ACCCPY
C     INTEGER PS_SN0N_LOG
C     INTEGER PS_SN0N_NINT
C
C************************************************************************

      MODULE PS_SN0N_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: PS_SN0N_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: PS_SN0N_SELF_T
C        <comment>Handle on parent</comment>
         INTEGER HPRNT
C        <comment>Handle on local identity renumbering</comment>
         INTEGER HNUMR
C        <comment>Handle on grid</comment>
         INTEGER HGRID
C        <comment>Total number of nodes to monitor</comment>
         INTEGER NNOT
C        <comment>Local number of nodes to monitor</comment>
         INTEGER NNOL
C        <comment>Nodes to monitor locally</comment>
         INTEGER LNOD
C        <comment>DOF to monitor</comment>
         INTEGER LDDL
C        <comment>Work table for the values</comment>
         INTEGER LVAL
      END TYPE PS_SN0N_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée PS_SN0N_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0N_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (PS_SN0N_SELF_T), POINTER :: PS_SN0N_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      PS_SN0N_REQSELF => SELF
      RETURN
      END FUNCTION PS_SN0N_REQSELF

      END MODULE PS_SN0N_M

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction <code>PS_SN0N_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0N_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_000
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INCLUDE 'pssn0n.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(PS_SN0N_HBASE,
     &                   'Post-Treatment Probe 0D on nodes')

      PS_SN0N_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0N_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_999
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INCLUDE 'pssn0n.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL PS_SN0N_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(PS_SN0N_HBASE,
     &                   PS_SN0N_DTR)

      PS_SN0N_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction PS_SN0N_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_CTR
CDEC$ ENDIF

      USE PS_SN0N_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0n.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn0n.fc'

      INTEGER IERR, IRET
      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   PS_SN0N_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = PS_SN0N_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. PS_SN0N_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      PS_SN0N_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_DTR
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0n.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = PS_SN0N_RST(HOBJ)

C---     Dé-alloue
      SELF => PS_SN0N_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, PS_SN0N_HBASE)

      PS_SN0N_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée PS_SN0N_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_RAZ(HOBJ)

      USE PS_SN0N_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0n.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn0n.fc'

      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN0N_REQSELF(HOBJ)
      SELF%HPRNT = 0
      SELF%HNUMR = 0
      SELF%HGRID = 0
      SELF%NNOT  = 0
      SELF%NNOL  = 0
      SELF%LNOD  = 0
      SELF%LDDL  = 0
      SELF%LVAL  = 0

      PS_SN0N_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les tables sont dimensionnées au nombre de noeuds globaux alors
C     qu'elles pourraient l'être au nombre de noeuds locaux.
C************************************************************************
      FUNCTION PS_SN0N_INI(HOBJ,
     &                     NOMFIC,
     &                     ISTAT,
     &                     IOPR,
     &                     IOPW,
     &                     HGRID,
     &                     NNOT,
     &                     KNOD,
     &                     KDDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_INI
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW
      INTEGER       HGRID
      INTEGER       NNOT
      INTEGER       KNOD(NNOT)
      INTEGER       KDDL(NNOT)

      INCLUDE 'pssn0n.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0n.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER IN
      INTEGER HPRNT
      INTEGER LNOD, LDDL, LVAL
      INTEGER NNTG
      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE LES DONNEES
      IF (NNOT .LE. 0) GOTO 9900
      IF (.NOT. DT_GRID_HVALIDE(HGRID)) GOTO 9901
      NNTG = DT_GRID_REQNNT(HGRID)
      DO IN=1, NNOT
         IF (KNOD(IN) .LE. 0 .OR. KNOD(IN) .GT. NNTG) GOTO 9902
         IF (KDDL(IN) .LE. 0) GOTO 9903
      ENDDO

C---     RESET LES DONNEES
      IERR = PS_SN0N_RST(HOBJ)

C---     CONSTRUIT ET INITIALISE LE PARENT
      IF (ERR_GOOD())IERR=PS_PSTD_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_PSTD_INI(HPRNT,NOMFIC,ISTAT,IOPR,IOPW)

C---     Alloue la mémoire
      LNOD = 0
      LDDL = 0
      LVAL = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNOT, LNOD)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNOT, LDDL)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NNOT, LVAL)

C---     Copie les noeuds
      IF (ERR_GOOD())
     &   CALL ICOPY(NNOT,
     &              KNOD, 1,
     &              KA(SO_ALLC_REQKIND(KA,LNOD)), 1)
      IF (ERR_GOOD())
     &   CALL ICOPY(NNOT,
     &              KDDL, 1,
     &              KA(SO_ALLC_REQKIND(KA,LDDL)), 1)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF => PS_SN0N_REQSELF(HOBJ)
         SELF%HPRNT = HPRNT
         SELF%HGRID = HGRID
         SELF%NNOT  = NNOT
         SELF%LNOD  = LNOD
         SELF%LDDL  = LDDL
         SELF%LVAL  = LVAL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NNT_INVALIDE', ': ', NNOT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HGRID
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HGRID, 'MSG_GRID')
      GOTO 9999
9902  WRITE(ERR_BUF, '(A,2(A,I12))') 'ERR_NOEUD_INVALIDE', ': ',
     &            KNOD(IN), ' /', NNTG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_INDICE_INVALIDE', ': ', KDDL(IN)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN0N_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Réinitialise
C
C Description:
C     La fonction PS_SN0N_RST réinitialise l'objet. La mémoire est
C     désallouée, les objets attributs détruis et tous les attributs
C     réinitialisés.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0N_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_RST
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0n.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'pssn0n.fc'

      INTEGER IERR
      INTEGER HPRNT, HNUMR
      INTEGER LNOD, LDDL, LVAL
      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN0N_REQSELF(HOBJ)

C---     Détruis la renum
      HNUMR = SELF%HNUMR
      IF (NM_NUMR_CTRLH(HNUMR)) IERR = NM_NUMR_DTR(HNUMR)

C---     Détruis le parent
      HPRNT = SELF%HPRNT
      IF (PS_PSTD_HVALIDE(HPRNT)) IERR = PS_PSTD_DTR(HPRNT)

C---     Récupère la mémoire
      LNOD = SELF%LNOD
      LDDL = SELF%LDDL
      LVAL = SELF%LVAL
      IF (LNOD .NE. 0) IERR = SO_ALLC_ALLINT(0, LNOD)
      IF (LDDL .NE. 0) IERR = SO_ALLC_ALLINT(0, LDDL)
      IF (LVAL .NE. 0) IERR = SO_ALLC_ALLRE8(0, LVAL)

C---     RESET
      IF (ERR_GOOD()) IERR = PS_SN0N_RAZ(HOBJ)

      PS_SN0N_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_SN0N_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0N_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_REQHBASE
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INCLUDE 'pssn0n.fi'
C------------------------------------------------------------------------

      PS_SN0N_REQHBASE = PS_SN0N_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_SN0N_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0N_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_HVALIDE
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0n.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      PS_SN0N_HVALIDE = OB_OBJN_HVALIDE(HOBJ, PS_SN0N_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute des noeuds.
C
C Description:
C     La méthode PS_SN0P_AJTNOD permet d'ajouter des noeuds de sondage.
C     C'est une complément à la méthode PS_SN0N_INI et doit être appelée
C     avant l'utilisation de l'objet pour des calculs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les tables sont dimensionnées au nombre de noeuds globaux alors
C     qu'elles pourraient l'être au nombre de noeuds locaux.
C************************************************************************
      FUNCTION PS_SN0N_AJTNOD(HOBJ,
     &                        HGRID,
     &                        NADD,
     &                        KNOD,
     &                        KDDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_AJTNOD
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGRID
      INTEGER NADD
      INTEGER KNOD(NADD)
      INTEGER KDDL(NADD)

      INCLUDE 'pssn0n.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0n.fc'

      INTEGER IERR
      INTEGER IN
      INTEGER LNOD, LDDL, LVAL
      INTEGER NNOT, NNTG
      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_SN0N_REQSELF(HOBJ)
      NNOT = SELF%NNOT
      LNOD = SELF%LNOD
      LDDL = SELF%LDDL
      LVAL = SELF%LVAL

C---     Contrôle les données
      IF (NADD .LE. 0) GOTO 9900
      IF (HGRID .NE. SELF%HGRID) GOTO 9901
      NNTG = DT_GRID_REQNNT(HGRID)
      DO IN=1, NADD
         IF (KNOD(IN) .LE. 0 .OR. KNOD(IN) .GT. NNTG) GOTO 9902
         IF (KDDL(IN) .LE. 0) GOTO 9903
      ENDDO

C---     (Ré)-alloue la mémoire
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNOT+NADD, LNOD)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNOT+NADD, LDDL)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NNOT+NADD, LVAL)

C---     Copie les noeuds
      IF (ERR_GOOD())
     &   CALL ICOPY(NNOT,
     &              KNOD, 1,
     &              KA(SO_ALLC_REQKIND(KA,LNOD)+NNOT), 1)
      IF (ERR_GOOD())
     &   CALL ICOPY(NNOT,
     &              KDDL, 1,
     &              KA(SO_ALLC_REQKIND(KA,LDDL)+NNOT), 1)
      IF (ERR_GOOD())
     &   NNOT = NNOT+NADD

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%NNOT = NNOT
         SELF%LNOD = LNOD
         SELF%LDDL = LDDL
         SELF%LVAL = LVAL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NNT_INVALIDE', ': ', NADD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12,A,I12)') 'ERR_HANDLES_INCOHERENT', ': ',
     &   HGRID, '/', SELF%HGRID
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HGRID, 'MSG_GRID')
      IERR = OB_OBJC_ERRH(SELF%HGRID, 'MSG_GRID')
      GOTO 9999
9902  WRITE(ERR_BUF, '(A,2(A,I12))') 'ERR_NOEUD_INVALIDE', ': ',
     &            KNOD(IN), ' /', NNTG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_INDICE_INVALIDE', ': ', KDDL(IN)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN0N_AJTNOD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie les données pour un noeud.
C
C Description:
C     La fonction privée PS_SN0N_ACCCPY copie les données de la table
C     VSRC vers la table VDST pour le noeud INOD. Elle permet de résoudre
C     les deux tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_ACCCPY(NNOD, KNOD, KDDL,
     &                        NDLN_S, NNT_S, VSRC,
     &                        NDLN_D, NNT_D, VDST)

      IMPLICIT NONE

      INTEGER NNOD
      INTEGER KNOD(NNOD)
      INTEGER KDDL(NNOD)
      INTEGER NDLN_S
      INTEGER NNT_S
      REAL*8  VSRC(NDLN_S, NNT_S)
      INTEGER NDLN_D
      INTEGER NNT_D
      REAL*8  VDST(NDLN_D, NNT_D)

      INCLUDE 'err.fi'
      INCLUDE 'pssn0n.fc'

      INTEGER I, ID, IN
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      DO I=1,NNOD
         ID = KDDL(I)
         IN = KNOD(I)
         VDST(1, I) = VSRC(ID, IN)
      ENDDO

      PS_SN0N_ACCCPY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     En principe, les données ont déjà été chargées
C************************************************************************
      FUNCTION PS_SN0N_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_ACC
CDEC$ ENDIF

      USE PS_SN0N_M
      USE LM_HELE_M, ONLY: LM_HELE_REQEDTA, LM_HELE_REQGDTA
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0n.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'tgtrig.fi'
      INCLUDE 'pssn0n.fc'

      INTEGER IERR
      INTEGER IOP
      INTEGER IN, INDXS
      INTEGER HPRNT
      INTEGER HSIM, HDLIB, HVNO
      INTEGER HNUMR
      INTEGER        NNL_D, NPST_D      ! Données
      INTEGER NNT_L, NNL_L, NDLN_L      ! Local
      INTEGER NNOT, NNOL
      INTEGER LNOD, LDDL, LVAL
      INTEGER LVNO_D
      REAL*8  VAL
      REAL*8  TSIM_S, TSIM
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.post.probe.0d.nodes'

C---     Récupère les attributs
      SELF => PS_SN0N_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))

C---     Récupère les attributs
      HNUMR = SELF%HNUMR
      NNOT = SELF%NNOT
      NNOL = SELF%NNOL
      LNOD = SELF%LNOD
      LDDL = SELF%LDDL
      LVAL = SELF%LVAL

C---     Récupère les attributs du parent
      HSIM = PS_PSTD_REQHSIM(HPRNT)
D     CALL ERR_ASR(LM_HELE_HVALIDE(HSIM))
      HVNO = PS_PSTD_REQHVNO(HPRNT)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données de simulation
      GDTA => LM_HELE_REQGDTA(HSIM)
      EDTA => LM_HELE_REQEDTA(HSIM)
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Nos dimensions locales
      NNL_L  = NNOL
      NNT_L  = NNOT
      NDLN_L = 1

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Contrôles
      IF (NNL_D  .NE. NNL_L)  GOTO 9900
      IF (NPST_D .NE. NDLN_L) GOTO 9901
      DO IN=1,NNOL         ! les kddl(i) <= NDLN_S
         INDXS = SO_ALLC_REQIN4(LDDL, IN)
         IF (INDXS  .GT. EDTA%NDLN) GOTO 9902   ! Index invalide
      ENDDO

C---     Log
      IF (ERR_GOOD()) IERR = PS_PSTD_LOGACC(HPRNT, 
     &                                      LOG_ZNE,
     &                                      'MSG_POST_PROBE_0D',
     &                                      TSIM)

C---     Copie les valeurs dans VAL
      IF (ERR_GOOD()) THEN
         IERR = PS_SN0N_ACCCPY(NNOL,
     &                         KA(SO_ALLC_REQKIND(KA,LNOD)),
     &                         KA(SO_ALLC_REQKIND(KA,LDDL)),
     &                         EDTA%NDLN,
     &                         GDTA%NNL,
     &                         EDTA%VDLG,
     &                         NPST_D,
     &                         NNL_D,
     &                         VA(SO_ALLC_REQVIND(VA,LVAL)))
      ENDIF

C---     Operation de réduction dans VAL
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUC(HPRNT,
     &                       NPST_D*NNL_D,
     &                       PVNO_D%VPTR, 1,
     &                       VA(SO_ALLC_REQVIND(VA,LVAL)), 1)
      ENDIF

C---     Met à jour les données
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(HVNO,
     &                          HNUMR,
     &                          VA(SO_ALLC_REQVIND(VA,LVAL)),
     &                          TSIM)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD()) THEN
         IOP = PS_PSTD_REQOPW(HPRNT)
         IF (IOP .EQ. PS_OP_WRITE) IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNL_INCOMPATIBLES', ': ',
     &                               NNL_L, '/', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INCOMPATIBLES', ': ',
     &                               NDLN_L, '/', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A,2(A,I12))') 'ERR_INDICE_INVALIDE', ': ',
     &            INDXS, ' /', EDTA%NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN0N_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Suivant les post-traitements, HNUMR peut être nul. Il est souvent
C     calculé dans ACC, et ACC peut ne pas être appelé si le trigger
C     n'est pas déclenché, résultant en FIN appelé sans ACC.
C************************************************************************
      FUNCTION PS_SN0N_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_FIN
CDEC$ ENDIF

      USE PS_SN0N_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0n.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'pssn0n.fc'

      INTEGER IERR
      INTEGER HPRNT, HSIM, HVNO, HNUMR
      INTEGER NNL_D, NPST_D
      INTEGER LVNO_D
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_SN0N_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
      HSIM = PS_PSTD_REQHSIM(HPRNT)
      HVNO = PS_PSTD_REQHVNO(HPRNT)
      IF (HSIM .EQ. 0) GOTO 9900
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      IF (NNL_D  .LE. 0) GOTO 9901
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      IF (NPST_D .LE. 0) GOTO 9902
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Récupère la numérotation
      HNUMR  = SELF%HNUMR

C---     Opération de réduction
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUCFIN(HPRNT,
     &                          NNL_D,
     &                          PVNO_D%VPTR, 1)
      ENDIF

C---     Log
      IF (ERR_GOOD()) THEN
         IERR = PS_SN0N_LOG(NPST_D,
     &                      NNL_D,
     &                      PVNO_D%VPTR)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD() .AND. HNUMR .NE. 0) THEN
         IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_APPEL_FINALIZE_SANS_ACC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INVALIDE', ': ', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INVALIDE', ': ', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN0N_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn0n.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pssn0n.fc'

      INTEGER IERR
      REAL*8  TSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère le handle des DDL et le temps
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_HELE_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLC(HSIM)

C---     Conserve la simulation (dimensionne)
      IF (ERR_GOOD()) IERR = PS_SN0N_ASGHSIM(HOBJ, HSIM)

C---     Accumule
      IF (ERR_GOOD()) IERR = PS_SN0N_ACC(HOBJ)

C---     Finalise
      IF (ERR_GOOD()) IERR = PS_SN0N_FIN(HOBJ)

      PS_SN0N_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN0N_ASGHSIM
C
C Description:
C     La fonction PS_SN0N_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_ASGHSIM
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn0n.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'pssn0n.fc'

      INTEGER IERR
      INTEGER HGRID, HNUMR
      INTEGER HPRNT
      INTEGER LNOD, LDDL
      INTEGER NPST, NNOL, NNOT
      PARAMETER (NPST = 1)
      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_SN0N_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
      HGRID = SELF%HGRID
      HNUMR = SELF%HNUMR
      NNOL  = SELF%NNOL
      NNOT  = SELF%NNOT
      LNOD  = SELF%LNOD
      LDDL  = SELF%LDDL

C---     Monte la renumérotation locale
      IF (HNUMR .EQ. 0) THEN
         IERR = PS_SN0N_NINT(HNUMR,
     &                       HGRID,
     &                       NNOT,
     &                       KA(SO_ALLC_REQKIND(KA,LNOD)),
     &                       KA(SO_ALLC_REQKIND(KA,LDDL)),
     &                       NNOL)

         IF (ERR_GOOD()) THEN
            SELF%HNUMR = HNUMR
            SELF%NNOL  = NNOL
         ENDIF
      ENDIF

      PS_SN0N_ASGHSIM = PS_PSTD_ASGHSIM(HPRNT, HSIM, NPST, NNOL, NNOT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN0N_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_REQHVNO
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0n.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN0N_REQSELF(HOBJ)
      PS_SN0N_REQHVNO = PS_PSTD_REQHVNO(SELF%HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN0N_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0N_REQNOMF
CDEC$ ENDIF

      USE PS_SN0N_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0n.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      TYPE (PS_SN0N_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN0N_REQSELF(HOBJ)
      PS_SN0N_REQNOMF = PS_PSTD_REQNOMF(SELF%HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: Log les résultats
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_LOG(NDLN, NNL, VDLG)

      IMPLICIT NONE

      INTEGER PS_SN0N_LOG
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER ID, IRMAX, IRMIN
      INTEGER IDAMAX, IDAMIN
      REAL*8  RMX, RMN
C------------------------------------------------------------------------

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_POST_PROBE_ON_NODES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE (LOG_BUF,'(A,I12)')  'MSG_NDLN#<25>#= ', NDLN
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)')  'MSG_NNL#<25>#= ', NNL
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)')      'MSG_VAL#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

C---     Normes pour chaque DDL
      CALL LOG_INCIND()
      IRMIN = IDAMIN(NNL, VDLG(1,1), NDLN)
      IRMAX = IDAMAX(NNL, VDLG(1,1), NDLN)
      RMN = VDLG(1, IRMIN)
      RMX = VDLG(1, IRMAX)
      WRITE (LOG_BUF,'(2(A,1PE14.6E3,I7))') '(min)= ', RMN, IRMIN,
     &                                  '  (max)= ', RMX, IRMAX
      CALL LOG_ECRIS(LOG_BUF)

      DO ID=2, NDLN
         IRMIN = IDAMIN(NNL, VDLG(ID,1), NDLN)
         IRMAX = IDAMAX(NNL, VDLG(ID,1), NDLN)
         RMN = VDLG(1, IRMIN)
         RMX = VDLG(1, IRMAX)
         WRITE (LOG_BUF,'(2(A,1PE14.6E3,I7))') '(min)= ', RMN, IRMIN,
     &                                     '  (max)= ', RMX, IRMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDDO
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      PS_SN0N_LOG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Numéro internes des noeuds
C
C Description:
C     La fonction privée PS_SN0N_NINT traduis les numéros de noeuds de la
C     numérotation globale à la numérotation locale. Seuls les noeuds
C     privés au process sont considérés. Les tables sont compactées et la
C     fonction retourne le nombre de noeuds locaux.
C
C Entrée:
C     HGRID    Handle sur le maillage des noeuds
C     NNG      Nombre de noeuds en numérotation globale
C     KNOD     Table des noeuds en numérotation globale
C
C Sortie:
C     HNUMR    Handle sur la numérotation du post-traitement
C     KNOD     Table des noeuds en numérotation locale
C     NNL      Le nombre de noeuds locaux
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0N_NINT(HNUMR, HGRID, NNG, KNOD, KDDL, NNL)

      IMPLICIT NONE

      INTEGER PS_SN0N_NINT
      INTEGER HNUMR
      INTEGER HGRID
      INTEGER NNG
      INTEGER KNOD(NNG)
      INTEGER KDDL(NNG)
      INTEGER NNL

      INCLUDE 'dtgrid.fi'
      INCLUDE 'nmamno.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IG_NG, IL_NG, ID_NG
      INTEGER IG_NL, IL_NL, IP_NL
      INTEGER I_PROC, I_RANK, I_ERROR
      INTEGER HNUMC, HALGN
C------------------------------------------------------------------------

C---     PARAMETRES MPI
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
      I_PROC = I_RANK + 1

C---     Construit et initialise l'algo de renum
      HALGN = 0
      IF (ERR_GOOD())IERR=NM_AMNO_CTR(HALGN)
      IF (ERR_GOOD())IERR=NM_AMNO_INI(HALGN, NNG)

C---     Scanne les noeuds, compacte KNOD et monte HNUMA
      HNUMC = DT_GRID_REQHNUMC(HGRID)
      NNL = 0
      DO IG_NL=1,NNG
         IF (ERR_BAD()) GOTO 199

         IG_NG = KNOD(IG_NL)                    ! Dans la renum externe
         ID_NG = KDDL(IG_NL)                    !     i global, i DDL
         IL_NG = NM_NUMR_REQNINT(HNUMC, IG_NG)  !     i local

         KNOD(IG_NL) = 0
         KDDL(IG_NL) = 0

         IL_NL = 0                              ! Dans la renum interne
         IP_NL = -1                             !     i local, i process

         IF (IL_NG .GT. 0) THEN
            IF (NM_NUMR_ESTNOPP(HNUMC, IL_NG)) THEN
               NNL = NNL + 1
               KNOD(NNL) = IL_NG
               KDDL(NNL) = ID_NG

               IP_NL = I_PROC
               IL_NL = NNL
            ENDIF
         ENDIF

         IERR = NM_AMNO_ASGROW(HALGN, IG_NL, IL_NL, IP_NL)
      ENDDO
199   CONTINUE

C---     Génère la renumérotation
      IF (NM_NUMR_CTRLH(HNUMR)) IERR = NM_NUMR_DTR(HNUMR)
      HNUMR = 0
      IF (ERR_GOOD()) IERR = NM_AMNO_GENNUM(HALGN, HNUMR)

C---     Détruis l'algo de renum
      IF (NM_AMNO_HVALIDE(HALGN)) IERR = NM_AMNO_DTR(HALGN)

      PS_SN0N_NINT = ERR_TYP()
      RETURN
      END
