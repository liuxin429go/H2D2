C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SIMU_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SIMU_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'pssimu_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pssimu_ic.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
      CALL ERR_PRE(.FALSE.)
C------------------------------------------------------------------------

      CALL LOG_ECRIS('The use of post_simulation has been deprecated')
      CALL LOG_ECRIS('Use instead the specialized form')
      CALL LOG_ECRIS('of the different elements')

      IPRM = ' '

      IC_PS_SIMU_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SIMU_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SIMU_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'pssimu_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssimu_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Les méthodes virtuelles sont gérées par le proxy
D     IF (IMTH .EQ. 'xeq') CALL ERR_ASR(.FALSE.)
D     IF (IMTH .EQ. 'del') CALL ERR_ASR(.FALSE.)
C     <include>IC_PS_POST_XEQMTH@pspost_ic.for</include>

C     <comment>The method <b>print</b> prints information about the object.</comment>
      IF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(PS_SIMU_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = PS_SIMU_PRN(HOBJ)
         CALL LOG_TODO('A nettoyer')
         CALL ERR_ASR(.FALSE.)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
!!!         CALL IC_PS_SIMU_AID()
         CALL LOG_TODO('A nettoyer')
         CALL ERR_ASR(.FALSE.)

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
!!!      CALL IC_PS_SIMU_AID()
       CALL LOG_TODO('A nettoyer')
       CALL ERR_ASR(.FALSE.)

9999  CONTINUE
      IC_PS_SIMU_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_SIMU_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SIMU_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssimu_ic.fi'
C-------------------------------------------------------------------------

      IC_PS_SIMU_REQCLS = '#__dummy_placeholder__#__PS_SIMU__'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_SIMU_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SIMU_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssimu_ic.fi'
      INCLUDE 'pssimu.fi'
C-------------------------------------------------------------------------

      IC_PS_SIMU_REQHDL = PS_SIMU_REQHBASE()
      RETURN
      END

