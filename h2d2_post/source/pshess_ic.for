C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Interface Commandes: PoSt-traitement
C Objet:   PoSt-traitement HESSian
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_HESS_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_HESS_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'pshess_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'egtperr.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'psellps.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pshess_ic.fc'

      INTEGER IERR, IRET
      INTEGER ISTAT,ITPCLC
      INTEGER IOPR, IOPW
      INTEGER HPST, HOBJ
      INTEGER HVNO, IVNO
      CHARACTER*(256) NOMFIC
      CHARACTER*(  8) MODE, NOMT
      CHARACTER*(  8) OPRD, OPWR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PS_HESS_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the nodal values</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HVNO)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Index on the nodal value on which the error will be calculated</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 2, IVNO)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Type of computation ['green', 'dxdx', 'recovery']</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 3, NOMT)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Name of the post-treatment file</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 4, NOMFIC)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Opening mode ['w', 'a'] (default 'a')</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 5, MODE)
      IF (IRET .NE. 0) MODE = 'a'
C     <comment>Reduction operation ['sum', 'sumabs', 'min', 'minabs', 'max', 'maxabs', 'mean'] (default 'noop')</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 6, OPRD)
      IF (IRET .NE. 0) OPRD = 'noop'
C     <comment>Writing operation ['write', 'nowrite'] (default 'nowrite')</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 7, OPWR)
      IF (IRET .NE. 0) OPWR = 'nowrite'
      CALL SP_STRN_TRM(NOMFIC)

C---     Monte le type de calcul
      ITPCLC = 0
      ITPCLC = IBSET(ITPCLC, EG_TPERR_HESSIEN)
      IF     (NOMT .EQ. 'green') THEN
         ITPCLC = IBSET(ITPCLC, EG_TPERR_HESSGREEN)
      ELSEIF (NOMT .EQ. 'dxdx') THEN
         ITPCLC = IBSET(ITPCLC, EG_TPERR_HESSDXDX)
      ELSEIF (NOMT .EQ. 'recovery') THEN
         ITPCLC = IBSET(ITPCLC, EG_TPERR_HESSRECOV)
      ELSE
         GOTO 9902
      ENDIF

C---     Valide
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9903
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9904
      IOPR = PS_PSTD_STR2OP(OPRD(1:SP_STRN_LEN(OPRD)))
      IF (IOPR .NE. PS_OP_NOOP) GOTO 9905
      IOPW = PS_PSTD_STR2OP(OPWR(1:SP_STRN_LEN(OPWR)))
      IF (IOPW .NE. PS_OP_NOWRITE .AND.
     &    IOPW .NE. PS_OP_WRITE) GOTO 9906

C---     Construis et initialise l'objet
      HPST = 0
      IF (ERR_GOOD()) IERR = PS_ELLPS_CTR(HPST)
      IF (ERR_GOOD()) IERR = PS_ELLPS_INI(HPST,
     &                                    NOMFIC, ISTAT, IOPR, IOPW,
     &                                    HVNO,
     &                                    IVNO,
     &                                    ITPCLC)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = IC_PS_HESS_PRN(HPST)
      END IF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>post_hessian</b> constructs an object, with the given
C  arguments, and returns a handle on this object. The reduction operations
C  of the hessian, used during resolution, are not implemented.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_TYPE_INVALIDE', ': ', NOMT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_INVALIDE',': ', MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(3A)') 'ERR_OPRDUC_INVALIDE',': ', OPRD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF, '(3A)') 'ERR_OPWRITE_INVALIDE',': ', OPWR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_HESS_AID()

9999  CONTINUE
      IC_PS_HESS_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_HESS_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_HESS_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'pshess_ic.fi'
      INCLUDE 'psxeq_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psellps.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'psellps.fc'
      INCLUDE 'pshess_ic.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      HPRNT
!      INTEGER      IVAL
!      REAL*8       RVAL
      CHARACTER*64 PROP
      CHARACTER*256 SVAL
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Les méthodes virtuelles sont gérées par le proxy
D     IF (IMTH .EQ. 'xeq') CALL ERR_ASR(.FALSE.)
D     IF (IMTH .EQ. 'del') CALL ERR_ASR(.FALSE.)
C     <include>IC_PS_POST_XEQMTH@pspost_ic.for</include>

!!!!!C---     GET
!!!!!      IF (IMTH .EQ. '##property_get##') THEN
!!!!!D        CALL ERR_ASR(PS_ELLPS_HVALIDE(HOBJ))
!!!!!         IOB = HOBJ - PS_ELLPS_HBASE
!!!!!         HPRNT = PS_ELLPS_HPRNT(IOB)
!!!!!
!!!!!         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!!!!!         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
!!!!!         IF (IERR .NE. 0) GOTO 9901
!!!!!
!!!!!C        <comment>Output file name</comment>
!!!!!         IF (PROP .EQ. 'file') THEN
!!!!!            SVAL = PS_POST_REQNOMF(HPRNT)
!!!!!            WRITE(IPRM, '(3A)') 'S', ',', SVAL
!!!!!         ELSE
!!!!!            GOTO 9902
!!!!!         ENDIF
!!!!!
!!!!!C---     SET
!!!!!      ELSEIF (IMTH .EQ. '##property_set##') THEN
!!!!!D        CALL ERR_ASR(PS_ELLPS_HVALIDE(HOBJ))
!!!!!         IOB = HOBJ - PS_ELLPS_HBASE
!!!!!
!!!!!         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!!!!!         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
!!!!!         IF (IERR .NE. 0) GOTO 9901
!!!!!
!!!!!!         IF (PROP .EQ. 'istep') THEN
!!!!!!            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
!!!!!!            IF (IERR .NE. 0) GOTO 9901
!!!!!!            PS_POST_ISTEP(IOB) = IVAL
!!!!!!         ELSE
!!!!!            GOTO 9902
!!!!!!         ENDIF
!!!!!
!!!!!C     <comment>Print statement</comment>
!!!!!      ELSEIF (IMTH .EQ. 'print') THEN
!!!!!D        CALL ERR_ASR(PS_ELLPS_HVALIDE(HOBJ))
!!!!!         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
!!!!!C         IERR = PS_ELLPS_PRN(HOBJ)
!!!!!         IERR = IC_PS_HESS_PRN(HOBJ)
!!!!!
!!!!!      ELSEIF (IMTH .EQ. 'help') THEN
!!!!!         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
!!!!!         CALL IC_PS_HESS_AID()
!!!!!
!!!!!      ELSE
!!!!!         GOTO 9903
!!!!!      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_HESS_AID()

9999  CONTINUE
      IC_PS_HESS_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_HESS_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_HESS_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pshess_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>post_hessian</b> represents the post-treatment that computes the
C  approximation errors as the Hessian of a variable. This post-treatment should
C  not be used as a post-treatment during resolution.
C</comment>
      IC_PS_HESS_REQCLS = 'post_hessian'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_HESS_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_HESS_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pshess_ic.fi'
      INCLUDE 'psellps.fi'
C-------------------------------------------------------------------------

      IC_PS_HESS_REQHDL = 1999021000
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_PS_HESS_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PS_HESS_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('pshess_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_HESS_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'psellps.fi'
      INCLUDE 'psellps.fc'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pshess_ic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_POST_HESSIAN'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      IOB = HOBJ - PS_ELLPS_HBASE

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_HANDLE#<25>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      NOM = PS_ELLPS_REQNOMF(HOBJ)
      IF (SP_STRN_LEN(NOM) .NE. 0) THEN
         CALL SP_STRN_CLP(NOM, 50)
         WRITE (LOG_BUF,'(3A)')  'MSG_NOM_FICHIER#<25>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      IERR = OB_OBJC_REQNOMCMPL(NOM, PS_ELLPS_HVNO(IOB))
      WRITE (LOG_BUF,'(3A)')  'MSG_HANDLE_SOURCE#<25>#', '= ',
     &                         NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      WRITE (LOG_BUF,'(2A,I12)')  'MSG_INDICE_SOURCE#<25>#', '= ',
     &                         PS_ELLPS_IVNO(IOB)
      CALL LOG_ECRIS(LOG_BUF)


      CALL LOG_DECIND()

      IC_PS_HESS_PRN = ERR_TYP()
      RETURN
      END
