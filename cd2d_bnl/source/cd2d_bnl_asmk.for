C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de convection-diffusion eulerienne 2-D
C     Contaminant conservatif
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_BNL_ASMK
C
C Description:
C     Assemble la matrice [K]
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BNL_ASMK (VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BNL_ASMK
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse.fi'

      REAL*8  VKE  (CD2D_BSE_NDLEMAX, CD2D_BSE_NDLEMAX)
      REAL*8  VDLE (CD2D_BSE_NDLEMAX)
      INTEGER KLOCE(CD2D_BSE_NDLEMAX)
      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .GE. 1)
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. CD2D_BSE_NDLNMAX)
D     CALL ERR_PRE(LM_CMMN_NDLEV.LE. CD2D_BSE_NDLEMAX)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     ASSEMBLAGE DU TERME PUITS-SOURCE DANS LA MATRICE
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKE, VDLE, KLOCE)
         CALL CD2D_BNL_ASMK_V(KLOCE,
     &                        VDLE,
     &                        VKE,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLR,
     &                        VDLG,
     &                        HMTX,
     &                        F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     ASSEMBLAGE DE LA MATRICE DE TRANSPORT-DIFFUSION
      IF (ERR_GOOD())
     &   CALL CD2D_BSEN_ASMK (VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLC,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        HMTX,
     &                        F_ASM)

      RETURN
      END

C************************************************************************
C Sommaire :  CD2D_BNL_ASMK_V
C
C Description:
C     ASSEMBLAGE DANS LA MATRICE DU TERME PUITS-SOURCE
C
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BNL_ASMK_V(KLOCE,
     &                           VDLE,
     &                           VKE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VSOLR,
     &                           VDLG,
     &                           HMTX,
     &                           F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLE  (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IC, IE, ID
      INTEGER NO1, NO2, NO3
      INTEGER IPA, ID1, ID2, ID3
      REAL*8  A1, A2, A3, SA
      REAL*8  DETJT3, CONST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .GE. 1)
C-----------------------------------------------------------------------

C---     INITIALISE
      CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VDLE, 1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        CONNECTIVITES DU T3
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)

C---        MÉTRIQUES DU T3
         DETJT3 = VDJV(5,IE)

C---        CONSTANTES DÉPENDANT DU VOLUME
         CONST = UN_120 * DETJT3

C---        INDICES DANS VPRNO
         IPA = LM_CMMN_NPRNOL + 6

C---        BOUCLE SUR LES DDL
         DO ID=1,LM_CMMN_NDLN
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN
            ID3 = ID2+LM_CMMN_NDLN

C---           INITIALISATION DES PROPRIÉTÉS NODALES
            A1 = - CONST*VPRNO(IPA,NO1)
            A2 = - CONST*VPRNO(IPA,NO2)
            A3 = - CONST*VPRNO(IPA,NO3)
            SA = DEUX*(A1 + A2 + A3)

C---           ASSEMBLAGE DES CONTRIBUTIONS
            VKE(ID1,ID1) = (SA + QUATRE*A1)
            VKE(ID2,ID1) = (SA - A3)
            VKE(ID3,ID1) = (SA - A2)
            VKE(ID1,ID2) = (SA - A3)
            VKE(ID2,ID2) = (SA + QUATRE*A2)
            VKE(ID3,ID2) = (SA - A1)
            VKE(ID1,ID3) = (SA - A2)
            VKE(ID2,ID3) = (SA - A1)
            VKE(ID3,ID3) = (SA + QUATRE*A3)

            IPA = IPA + 2
         ENDDO

C---       TABLE KLOCE DE LOCALISATION DES DDLS
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NO1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NO2)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 3)= KLOCN(ID, NO3)
         ENDDO

C---       ASSEMBLAGE DE LA MATRICE
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (.NOT. ERR_GOOD() .AND.
D    &       .NOT. ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D           WRITE(LOG_BUF,*)'ERR_CALCUL_MATRICE_K: ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      RETURN
      END
