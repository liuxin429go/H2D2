C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de convection-diffusion eulerienne 2-D
C     Contaminant conservatif
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_BNL_PRNPRNO
C
C Description:
C     IMPRESSION DES PROPRIETES NODALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BNL_PRNPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BNL_PRNPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     APPEL DU PARENT
      CALL CD2D_BSE_PRNPRNO()

C---     IMPRESSION DE L'INFO
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('PROPRIETES NODALES DE CINETIQUE CALCULEES:')
      CALL LOG_INCIND()
      CALL LOG_ECRIS('PUITS/SOURCES A(C) + B : A')
      CALL LOG_ECRIS('PUITS/SOURCES A(C) + B : B')
      CALL LOG_DECIND()

      RETURN
      END

