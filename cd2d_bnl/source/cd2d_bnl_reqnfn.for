C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER CD2D_BNL_REQNFN
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction CD2D_BNL_REQNFN retourne le nom de la fonction
C     associé au code demandé. Le nom combine le nom de la DLL et
C     de la fonction sous la forme "nomFonction@nomDll".
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     KFNC     Nom de la fonction stocké dans une table INTEGER
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BNL_REQNFN(KFNC, IFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BNL_REQNFN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER CD2D_BNL_REQNFN
      INTEGER KFNC(*)
      INTEGER IFNC

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'

      INTEGER        CD2D_BSE_REQNFN
      INTEGER        I
      INTEGER        IERR
      INTEGER        JF(16)
      CHARACTER*(64) NF
      EQUIVALENCE (NF, JF)
      LOGICAL        SAUTE
C-----------------------------------------------------------------------

      IERR = CD2D_BSE_REQNFN(KFNC, IFNC)
      IF (.NOT. ERR_GOOD()) GOTO 9999

      SAUTE = .FALSE.
      IF (IFNC .EQ. EA_FUNC_ASMF) THEN
                                    NF = 'CD2D_BNL_ASMF@cd2d_bnl'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMK) THEN
                                    NF = 'CD2D_BNL_ASMK@cd2d_bnl'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKT) THEN
                                    NF = 'CD2D_BNL_ASMKT@cd2d_bnl'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKU) THEN
                                    NF = 'CD2D_BNL_ASMKU@cd2d_bnl'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCCLIM) THEN
                                    NF = 'CD2D_BNL_PRCCLIM@cd2d_bnl'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRNO) THEN
                                    NF = 'CD2D_BNL_PRNPRNO@cd2d_bnl'
      ELSEIF (IFNC .EQ. EA_FUNC_REQPRM) THEN
                                    NF = 'CD2D_BNL_REQPRM@cd2d_bnl'
      ELSEIF (IFNC .EQ. EA_FUNC_REQNFN) THEN
                                    NF = 'CD2D_BNL_REQNFN@cd2d_bnl'
      ELSE
         SAUTE = .TRUE.
      ENDIF

      IF (ERR_GOOD() .AND. .NOT. SAUTE) THEN
         DO I=1,16
            KFNC(I) = JF(I)
         ENDDO
      ENDIF

9999  CONTINUE
      CD2D_BNL_REQNFN = ERR_TYP()
      RETURN
      END
