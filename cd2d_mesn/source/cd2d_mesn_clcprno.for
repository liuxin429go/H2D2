C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES) pour N classes
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MESN_CLCPRNO
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG
C
C Entrée:
C      REAL*8    VCORG      Table des COoRdonnées Globales
C      INTEGER   KNGV       Table des coNectivités Globales de Volume
C      REAL*8    VDJV       Table des métriques (Determinant, Jacobien) de Volume
C      REAL*8    VPRGL      Table des PRopriétés GLobales
C      REAL*8    VPRNO      Table des Propriétés NOdales
C      REAL*8    VDLG       Table des Degrés de Liberté nodaux Globaux
C
C Sortie:
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MESN_CLCPRNO (VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             VDLG,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MESN_CLCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     APPELLE LE PARENT
      CALL CD2D_BSE_CLCPRNO(VCORG,
     &                      KNGV,
     &                      VDJV,
     &                      VPRGL,
     &                      VPRNO,
     &                      VDLG,
     &                      IERR)

C---     CALCUL DES TERMES PUITS-SOURCES
      CALL CD2D_MESN_CLCTPS(VPRGL,
     &                      VPRNO,
     &                      VDLG)

      RETURN
      END

C************************************************************************
C Sommaire :  CD2D_MESN_CLCTPS
C
C Description:
C     Calcul des composantes des termes puits-source
C
C Entrée:
C     VPRGL      Table des PRopriétés GLobales
C     VPRNO      Table des PRopriétés Nodales
C     VDLG       Table des Degrés de Liberté Globaux
C
C Sortie:
C     VPRNO       Table mise à jour
C
C Notes:
C     S'il y a plusieurs itérations, le résultat sera éroné. On aura
C     ws*pd**2, puis ws*pd**3, etc...
C************************************************************************
      SUBROUTINE CD2D_MESN_CLCTPS(VPRGL, VPRNO, VDLG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8 VPRGL(LM_CMMN_NPRGL)
      REAL*8 VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8 VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_mesn.fc'

      INTEGER IN, ID
      INTEGER IPA
      REAL*8  WS, RHOS, CKS, WSED, EXPN
      REAL*8  CSOL, PD

      REAL*8 QTIER
      PARAMETER (QTIER=1.333333333333333333333333333D0)
C-----------------------------------------------------------------------

C---     BOUCLE SUR SUR LES NOEUDS
      DO IN =1,EG_CMMN_NNL

C---        INDICES DANS VPRNO
         IPA = LM_CMMN_NPRNOL + 6

C---        BOUCLE SUR SUR LES DDL
         DO ID=1, LM_CMMN_NDLN
            IF (CD2D_MESN_WS(ID) .GT. PETIT) GOTO 199 ! WS constante : calcul déjà complet

C---           CONCENTRATION DE SOLIDES EN SUPENSION
            CSOL = MAX(ZERO, VDLG(ID, IN))

C---           VITESSE DE SEDIMENTATION
            IF     (CD2D_MESN_ICSOL(ID) .EQ. 1) THEN  ! solides cohésifs
               CKS = CD2D_MESN_CKS (ID)
               WS  = CKS*(CSOL**QTIER)
            ELSEIF (CD2D_MESN_ICSOL(ID) .EQ. 2) THEN  ! solides non cohésifs
               WSED = CD2D_MESN_WSED(ID)
               EXPN = CD2D_MESN_EXPN(ID)
               RHOS = CD2D_MESN_RHOS(ID)
               WS = WSED*(UN - CSOL/RHOS)**EXPN
            ENDIF

C---           PROBABILITÉ DE DEPOT
            PD = VPRNO(IPA,IN)                  ! Probabilité de déposition

C---           PROPRIETES NODALES
            VPRNO(IPA,IN) = PD*WS               ! WS corrigée

199         CONTINUE
            IPA = IPA + 2
         ENDDO

      ENDDO

      RETURN
      END
