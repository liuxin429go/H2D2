%% Fonction qui calcule les sollicitations à ajouter
function[SOLRqx, SOLRqy, SOLRh] = TESTSOL()
fprintf('Les paramètres par défaut sont: g=1 \n')
syms x y SOLRqx SOLRqy SOLRh
DEFAUT = input('Conserver ces paramètres? O/N [O]: ', 's');
if isempty(DEFAUT)
    DEFAUT = 'O';
end
if DEFAUT == 'O'
% Constantes
    g      = 1;
    n      = 1;
    rau    = 1000;
    raua   = 1.2475;
    omega  = 0.7292*10^-4;
% Initialisation
    K      = 0;
    wx     = 0;
    wy     = 0;
    phi    = 0;
    u = input('Entrer la vitesse en x(m/s) [0]: ');
    if isempty(u)
        u = 0;
    end
    v = input('Entrer la vitesse en y(m/s) [0]: ');
    if isempty(v)
        v = 0;
    end
    H = input('Entrer la profondeur (m) [1]: ');
    if isempty(H)
        H = 1;
    end
    
    CONV = input('Entrer le coefficient d activation de la convection 1/0 [1]: ');
    if isempty(CONV)
        CONV = 1;
    end
    LAM = input('Entrer le coefficient d activation de la viscosite laminaire 1/0 [1]: ');
    if isempty(LAM)
        LAM = 1;
    end
    TURB = input('Entrer le coefficient d activation de la viscosite turbulente 1/0 [1]: ');
    if isempty(TURB)
        TURB = 1;
    end
    if TURB==1
        K = input('Entrer le coefficient de longueur de mélange [0]: K = ');
    end
    GRAV = input('Entrer le coefficient d activation de la gravite 1/0 [1]: ');
    if isempty(GRAV)
        GRAV = 1;
    end
    MANN = input('Entrer le coefficient d activation du Manning 1/0 [1]: ');
    if isempty(MANN)
        MANN = 1;
    end
    if MANN==1
        n = input('Entrer le coefficient de Manning de fond et glace [0]:');
    end
    VENT = input('Entrer le coefficient d activation du vent 1/0 [1]: ');
    if isempty(VENT)
        VENT = 1;
    end
    if VENT==1
        wx = input('Entrer la vitesse du vent en x(m/s) [0]: ');
        if isempty(wx)
         wx = 0;
        end
        wy = input('Entrer la vitesse du vent en y(m/s) [0]: ');
        if isempty(wy)
         wy = 0;
        end
    end
    CORI = input('Entrer le coefficient d activation de Coriolis 1/0 [1]: ');
    if isempty(CORI)
        CORI = 1;
    end
    if CORI==1
        phi = input('Entrer la latitude (degre) [0]: ');
        if isempty(phi)
            phi = 0;
        end
        phi = phi*pi/180;
    end
    dhdx = input('Entrer la pente topographique en x [0]: dhdx=');
    if isempty(dhdx)
        dhdx = 0;
    end
    dhdy = input('Entrer la pente topographique en y [0]: dhdy=');
    if isempty(dhdy)
        dhdy = 0;
    end
    z0   = input('Entrer la hauteur du fond (m)[0]: z0=');
    if isempty(z0)
        z0 = 0;
    end
    
else
    fprintf('Les paramètres pour les tests doivent rester simples')
    exit
end



%% Termes à calculer

%Calculs des dérivées et autres termes
lm     = K*H;          z    = z0 +dhdx*x+dhdy*y;         h = H + z;
dudx   = diff(u,x);    dudy = diff(u,y); 
dvdy   = diff(v,y);    dvdx = diff(v,x);
dhdx   = diff(h,x);    dhdy = diff(h,y);

           
% Viscosité
nuT    = (lm^2)*(2*(dudx)^2+2*(dvdy)^2+(dudy+dvdx)^2)^(1/2);
tauxx = (LAM + TURB*nuT)*2*H*dudx;
tauxy = (LAM + TURB*nuT)*H*(dudy+dvdx);
tauyy = (LAM + TURB*nuT)*2*H*dvdy;

% Vent
wmod = sqrt(wx^2+wy^2);
cw = 0;
if wmod ==0
    cw = 0;
elseif wmod<=1.0
    cw = 1.25*10^-3*wmod^0.4;
elseif wmod<=225.0;
    cw = 0.5*10^-3*wmod^0.75;
else
    cw = 2.6*10^-3*sqrt(wmod);
end
tauWX = cw*raua/rau*wx;
tauWY = cw*raua/rau*wy;

% Manning
MANX = n^2*g*sqrt((u*H)^2+(v*H)^2)/(H^(7/3))*u*H;
MANY = n^2*g*sqrt((u*H)^2+(v*H)^2)/(H^(7/3))*v*H;

% Coriolis
CX = (2*omega*sin(phi)*v*H);
CY = (2*omega*sin(phi)*u*H);

% Gravité
GX = g*H*dhdx;
GY = g*H*dhdy;

%Convection
CONX = diff(u^2*H,x) + diff(u*v*H,y);
CONY = diff(u*v*H,y) + diff(v^2*H,y);

%% Résolution des équations différentielles

E1 = -(CONV)*CONX - (GRAV)*GX - MANN*MANX + diff(tauxx,x) + diff(tauxy,y) + (VENT)*tauWX + (CORI)*CX + SOLRqx;
E2 = -(CONV)*CONY - (GRAV)*GY - MANN*MANY + diff(tauxy,x) + diff(tauyy,y) + (VENT)*tauWY - (CORI)*CY + SOLRqy;
E3 = diff(u*H,x) + diff(v*H,y) - SOLRh;

SOLRqx = vpa(solve(E1,SOLRqx))
SOLRqy = vpa(solve(E2,SOLRqy))
SOLRh  = vpa(solve(E3,SOLRh))
