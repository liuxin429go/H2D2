clear
syms x;
g  = 9.806;
u0 = 0.1

h      = 2-x;       
u      = sqrt(g*(x-2)) +u0;             %On pose u 
dudx   = diff(u,x);
du2hdx = diff(u^2*h,x);

f = du2hdx -g*h                        %Conservation des moments

%s = solve(f,x);
rangex = [0 30 0 10];
g = inline(f,'t','x');
implot2(g,rangex);

subplot(2,1,1)
ezplot(f, [0 3])                       % équation différentielle
                                        %
%ezplot(qx,[0 1])

%subplot(2,1,2)
;          %
%ezplot(q0t,[0 1])
