C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISCND(NOMFIC, NNT, NDLN, VDLG, KDIMP)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)

      CHARACTER*256 NOMCL, TMPCL
      CHARACTER*32  FMT
      LOGICAL       WRITE_DONE
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I3,', NDLN, '(1X,1PE25.17E3),A)'

C---     Compte le nombre de conditions
      NCND = 0
100   CONTINUE
C---        Cherche la prochaine condition non traitée
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               WRITE(TMPCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
               CALL TRIMSTR(TMPCL)
               CALL SBSTSTR(TMPCL, ' ','_')
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199
         write(6,*) '==>', TMPCL(1:75)
C---        Flag tous les noeuds de cette condition
         NCND = NCND + 1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
                WRITE(NOMCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
                CALL TRIMSTR(NOMCL)
                CALL SBSTSTR(NOMCL, ' ','_')
                IF (NOMCL .EQ. TMPCL) KDIMP(1,IN) = -KDIMP(1,IN)
            ENDIF
         ENDDO
      GOTO 100
199   CONTINUE

C---     Reset les codes
      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

C---     Écris l'entête
      WRITE(10, '(I6,1X,1PE25.17E3)') NCND, 0.0

C---     Écris les conditions
200   CONTINUE
C---        Cherche la prochaine condition non traitée
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               WRITE(TMPCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
               CALL TRIMSTR(TMPCL)
               CALL SBSTSTR(TMPCL, ' ','_')
               GOTO 209
            ENDIF
         ENDDO
209      CONTINUE
         IF (IC .LT. 0) GOTO 299

C---        Flag tous les noeuds de cette condition
C---        Ecris la condition
         WRITE_DONE = .FALSE.
         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
                WRITE(NOMCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
                CALL TRIMSTR(NOMCL)
                CALL SBSTSTR(NOMCL, ' ','_')

                IF (NOMCL .EQ. TMPCL) THEN
                    KDIMP(1,IN) = -KDIMP(1,IN)
                    IF (.NOT. WRITE_DONE) THEN
                        WRITE(10, '(A,I3,6(1X,1PE25.17E3))')
     &                                  NOMCL(1:LENSTR(NOMCL)),
     &                                  MOD(IC,1000),
     &                                  (VDLG(ID,IN),ID=1,NDLN)
                        WRITE_DONE = .TRUE.
                    ENDIF
                ENDIF
            ENDIF
         ENDDO

      GOTO 200
299   CONTINUE

      CLOSE(10)

C---     Reset les codes
      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END