clear
%--------------------------
% Résolution pour le vent
% Constantes
rhoa = 1.2475; %kg/m^3
rhoe = 1000;   %kg/m^3
g    = 9.806;  %m/s^2
nu   = 1.0;       %m^2/s
%--------------------------
% Graphique du coefficient de traînée du vent sur l'eau
w1   = 0:.1:1;
w2   = 1:.1:15;
w3   = 15:.1:17;
w    = 0:.1:17;
H    = 1.5;
cw1  = w1.*(1.25*10^(-3)).*(w1.^(-1/5));    %Coef. de trainée selon Wu
cw2  = w2.*0.5*10^(-3).*w2.^(0.5);
cw3  = w3.*2.6*10^(-3)+0*w3;
plot(w1,cw1);
hold on
plot(w2,cw2);
hold on
plot (w3,cw3);  %Trace le coef. de trainée en fonction de la vitesse
figure
%--------------------------
%% Cas w=wx=0.75 m/s  avec gravité et viscosité(non calculée ici car trop
%% compliqué
syms Hdhdx
w    = 0.75;
tauw = (1.25*10^(-3)*w^(-1/5))*rhoa*w^2;

eqn    = -g*Hdhdx+tauw/rhoe;
Hdhdx = solve(eqn,Hdhdx);
Hdhdx = vpa(Hdhdx)
%% Cas w=wx=0.75 m/s  avec viscosité
syms y x h;
format long e
u    = y;   v    = 0;   zf   = 0;   wx    = 0.75;
dudy = diff(u,y);   dvdx = diff(v,x);   dudx = diff(u,x);
tauw = (1.25*wx^(-1/5))*rhoa*wx^2; %x10^(-3)

eqn1 = tauw*10^(-3) + nu*(h-zf)*dudy;
eqn2 = 0 + nu*(h-zf)*dudy + 2*nu*(h-zf)*dvdx;
hy = int(-tauw*10^(-3),y)+1;  %Hauteur d'eau (analytique)
dhdy = diff(hy,y)
ezplot(hy,[0 1]);     %Graphique de h
qx = u*hy;             %qx analytique qx=y*(1-tauw*y)

y = 0:.25:1;
qx = subs(qx);
qx = sprintf('%.18f\n',qx)  %affiche qx aux noeuds

y=1; y1 = subs(hy);
hy  = linspace(1,y1,5);
hy  = sprintf('%.18f\n',hy) %affiche h(y) aux noeuds
tauw  = sprintf('%.18f\n',tauw)



