#-----------------------------------
#  Test des termes visqueux
#     u = 0
#     v = y
#     h = 1
#  Seul la continuité est non nulle
#  Avec les solicitations, solution exacte
#-----------------------------------


h_num = 0
h_frm = form('NS_Macro3D')


n_fic  = 'test'

#-----------------------------------
#  Maillage
#-----------------------------------
h_cor  = coor(n_fic+'.cor')
h_ele  = elem(n_fic+'.ele')
h_grid = grid(h_cor, h_ele)

#-----------------------------------
#  Propriétés nodales
#-----------------------------------
h_mnt  = vnod(n_fic+'.pnt')      # terrain
h_prn  = prno(h_mnt)             # regroupement des champs en prno

#-----------------------------------
#  Propriétés élémentaires
#-----------------------------------
h_pre  = 0   # prel()            # regroupement des champs en prel

#-----------------------------------
#  Propriétés globales
#-----------------------------------
h_pgl  = prgl(1.0,         #  1 msg_gravite
              0.0,         #  2 msg_latitude
              1.0e-00,     #  3 msg_viscosite_laminaire
              0.0,         #  4 msg_coef_longueur_melange
              0.0,         #  5 msg_coef_longueur_melange_maillage
              1.0e-99,     #  6 msg_borne_inf_viscosite
              1.0e+99,     #  7 msg_borne_sup_viscosite
#
              0.1,         #  8 msg_manning_decouvrement
              1.0,         #  9 msg_porosite_decouvrement
              1.0e-03,     # 10 msg_prof_min_decouvrement
              0.0,         # 11 msg_fact_convection_decouvrement
              0.0e-00,     # 12 msg_nu_diffusion_decouvrement
              1.0e-06,     # 13 msg_nu_darcy_decouvrement
#
              1.0e+99,     # 14 msg_peclet
              1.0e-99,     # 15 msg_coef_lissage_surface_libre_darcy
              1.0e-99,     # 16 msg_coef_lissage_surface_libre
#
              0.0,         # 17 msg_coef_convection
              1.0,         # 18 msg_coef_gravité
              0.0,         # 19 msg_coef_manning
              0.0,         # 20 msg_coef_vent
              0.0,         # 21 msg_coef_integrale_contour
              1.0,         # 22 msg_coef_vitesse_verticale_convection
              1.0,         # 23 msg_coef_viscosite_turbulente_xy
              1.0,         # 24 msg_coef_viscosite_turbulente_z
              1.0,         # 25 msg_coef_viscosite_totale_z
              1.0,         # 26 msg_coef_terme_diffusion_txy_tyx
#
              1.0e-18,     # 27 msg_coef_penalite
              1.0e-08,     # 28 msg_coef_perturbation_Kt (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
              1.0)         # 29 msg_coef_relaxation_Kt

#-----------------------------------
#  Sollicitations
#-----------------------------------
h_slr  = solr(vnod(n_fic+'.slr'))
h_slc  = 0

#-----------------------------------
#  Conditions limites
#-----------------------------------
h_cnd = condition(n_fic+'.cnd')
h_lmt = boundary (n_fic+'.bnd')
h_bc  = boundary_condition(h_lmt, h_cnd)

#-----------------------------------
#  Degrés de liberté
#-----------------------------------
h_ddl  = dof()

#-----------------------------------
#  Données globales de simulation
#-----------------------------------
h_sol  = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_pgl, h_prn, h_pre)

#-----------------------------------
#  Définition des algo de résolution
#-----------------------------------
h_resmat  = ldu_memory()
h_btrk = 0
h_newton  = newton (h_btrk,h_resmat, 5)

#------------------------------------
#  Critère d'arrêt, limiteur
#------------------------------------
h_limiter  = limiter(h_sol, 1.0, 1.0, 1.0, 1.0, 0.2)
h_criarret = cria_l2_allrel(h_sol, 0.0, 1.0e-15)

#-----------------------------------
#  Résolution
#-----------------------------------
h_post = 0
h_sol.init_f(n_fic+'.ini', 0)
h_newton.solve(h_sol, h_criarret, h_limiter, h_post, 1)
h_sol.save(n_fic+'.fin')

stop()


