C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: LMGO_P12L_MAIN
C
C Description:
C     Le program NS_TEST_EVALUE_MAIN  est un test de la fonction
C     NS_MACRO3D_EVALUE du module NS_MACRO3D.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      PROGRAM NS_TEST_EVALUE_MAIN

      INCLUDE 'eacmmn.fc' !LM_CMMN_NDLN
      INCLUDE 'egcmmn.fc' !EG_CMMN_NNELV

C--   ASSIGNATION DU COMMON
      EG_CMMN_NDIM  = 2
      EG_CMMN_NNELV = 6
      EG_CMMN_NDJV  = 5
      LM_CMMN_NDLN  = 5

      CALL NS_MACRO3D_ASGCMN(EG_CMMN_KA, LM_CMMN_KA)
      CALL LMGO_P12L_ASGCMN (EG_CMMN_KA)

      CALL TEST_EVALUE()

      END

C************************************************************************
C Sommaire: SUBROUTINE TEST_EVALUE(TYPEELEMENT, INDICEPT)
C
C Description: La routine TEST_EVALUE reçoit un type d'élément à tester
C              ainsi que le numéro d'un point de test. Elle appelle
C              la fonction lmgo_p12l et imprime les réponses
C
C Entrée:
C
C Sortie: Impression des résultats dans le fichier sortie.txt
C         se trouvant dans le répertoire resultat associé au test
C
C
C Notes:
C************************************************************************
      SUBROUTINE TEST_EVALUE()

      INCLUDE 'eacnst.fi' !UN_2
      INCLUDE 'eacmmn.fc' !LM_CMMN_NDLN
      INCLUDE 'egcmmn.fc' !EG_CMMN_NNELV

      INTEGER  NDJV
      INTEGER  NDIM
      INTEGER  NNELV
      INTEGER  NCELV
      INTEGER  NELV
      INTEGER  NDIMELV

      PARAMETER (NDJV=5)
      PARAMETER (NDIM=2)
      PARAMETER (NNELV=6)
      PARAMETER (NCELV=6)
      PARAMETER (NELV=1)
      PARAMETER (NDIMELV=3)
      PARAMETER (NNT = 6)
      PARAMETER (NFNAPPROXUV= 6)
      PARAMETER (NFNAPPROXH = 3)

      INTEGER NNT
      INTEGER KNGV(NCELV, NELV)
      INTEGER I,J

      INTEGER IERR

C--   VARIABLES LUES
      REAL*8   VCORE (EG_CMMN_NDIM, EG_CMMN_NNELV)
      REAL*8   VZCORE(2, EG_CMMN_NNELV)       ! donne les zs et zf pour chacun des 6 noeuds.
      REAL*8   VDJE  (EG_CMMN_NDJV)
      REAL*8   VCOPT (NDIMELV)
      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)

C--   VARIABLES SORTIES DE NS_MACRO3D_EVALUE (variables testées)
      REAL*8   VNUV(NFNAPPROXUV)             !évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH(NFNAPPROXH)               !évaluation des N de h au pt gauss
      REAL*8   VNUV_XI(NDIMELV, NFNAPPROXUV) !évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   VNH_XI(NDIMELV, NFNAPPROXH)   !évaluation des N,x_i (les N pour les h) aux pt de Gauss
      REAL*8   VDL_XIPT(3,NDIMELV)
      REAL*8   VW_XIPT(NDIMELV)
      REAL*8   VDLEPT(3)                     !sortie : évaluation des degrés de liberté au point d'intégration
      REAL*8   WPT                           !sortie : évaluation de la vitesse verticale au point d'intégration
      REAL*8   DJP12L                        !évaluation du déterminant au pt de gauss
      REAL*8   DJP6                          !évaluation du déterminant au pt de gauss
      INTEGER  IP6                           !Entier de 1 à 4 indiquant dans quel sous-élémnet se trouve le pt de gauss

C---  AJOUT
      REAL*8 VPRN(14, 6)                       !NPRN, NNELV
C ---------------------------------------------------------------------
C ---------------------------------------------------------------------
C---  Les propriétés nodales sont initialisées à 1. En fait, elles servent pour
C     l'évaluation des vitesses verticale, mais le test n'est pas en place pour
C     ces dernières
      CALL DINIT(14*6,UN,VPRN,1)

C--   CONNECTIVITÉS
      KNGV(1,1) = 1
      KNGV(2,1) = 2
      KNGV(3,1) = 3
      KNGV(4,1) = 4
      KNGV(5,1) = 5
      KNGV(6,1) = 6

C---  LECTURES :

C---  LECTURE DE LA GÉOMÉTRIE DE L'ÉLÉMENT
      OPEN(12, FILE='test.cor', STATUS='OLD')
      READ(12,*) ((VCORE (I,J),J=1,6),I=1,2)
      READ(12,*) ((VZCORE(I,J),J=1,6),I=1,2)
      CLOSE(12)

C---     S'assure de la linéarité (test si le test est valide !)
D     CALL ERR_ASR(VZCORE(1,2) .EQ. UN_2*(VZCORE(1,1)+VZCORE(1,3)))
D     CALL ERR_ASR(VZCORE(1,4) .EQ. UN_2*(VZCORE(1,3)+VZCORE(1,5)))
D     CALL ERR_ASR(VZCORE(1,6) .EQ. UN_2*(VZCORE(1,5)+VZCORE(1,1)))
D     CALL ERR_ASR(VZCORE(2,2) .EQ. UN_2*(VZCORE(2,1)+VZCORE(2,3)))
D     CALL ERR_ASR(VZCORE(2,4) .EQ. UN_2*(VZCORE(2,3)+VZCORE(2,5)))
D     CALL ERR_ASR(VZCORE(2,6) .EQ. UN_2*(VZCORE(2,5)+VZCORE(2,1)))

C---  S'assure que profondeur positive
D     CALL ERR_ASR(VZCORE(2,1) .GT. VZCORE(1,1))
D     CALL ERR_ASR(VZCORE(2,3) .GT. VZCORE(1,3))
D     CALL ERR_ASR(VZCORE(2,5) .GT. VZCORE(1,5))

C---  LECTURE DU POINT D'ÉVALUATION
      OPEN(12, FILE='test.pg', STATUS='OLD')
      READ(12,*) (VCOPT(I),i=1,ndimelv)
      CLOSE(12)

C---  LECTURE DES DDL
      OPEN(12, FILE='test.vno', STATUS='OLD')
      READ(12,*) ((VDLE(I, J), I=1,5), J=1,EG_CMMN_NNELV)
      CLOSE(12)

C---  RÉCUPÉRATION DES MÉTRIQUES
      IERR = LMGO_P12L_CLCJELV(  NDIM,
     &                           NNT,
     &                           NCELV,
     &                           NELV,
     &                           NDJV,
     &                           VCORE,
     &                           KNGV,
     &                           VDJE)

      CALL    NS_MACRO3D_EVALUE( NFNAPPROXUV,
     &                           NFNAPPROXH,
     &                           NDIMELV,
     &                           VCORE,
     &                           VZCORE,
     &                           VDJE,
     &                           VCOPT,
     &                           VDLE,
     &                           VPRN,
     &                           VNUV,
     &                           VNH,
     &                           VNUV_XI,
     &                           VNH_XI,
     &                           VDLEPT,
     &                           VDL_XIPT,
     &                           WPT,  !à analyser...
     &                           VW_XIPT, !à analyser
     &                           DJP12L,
     &                           DJP6,
     &                           IP6)

C--   ÉCRITURE DES RÉSULTATS
      OPEN (12, FILE='test.fin')
      WRITE(12,*) 'VNUV'
      WRITE(12,*) VNUV
      WRITE(12,*)
      WRITE(12,*) 'VNH'
      WRITE(12,*) VNH
      WRITE(12,*)
      WRITE(12,*) 'VNUV_XI'
      WRITE(12,*) VNUV_XI
      WRITE(12,*)
      WRITE(12,*) 'VNH_XI'
      WRITE(12,*) VNH_XI
      WRITE(12,*)
      WRITE(12,*) 'VDLEPT'
      WRITE(12,*) VDLEPT
      WRITE(12,*)
      WRITE(12,*) 'VDL_XIPT'
      WRITE(12,*) VDL_XIPT
      WRITE(12,*)
      WRITE(12,*) 'WPT'
      WRITE(12,*) WPT
      WRITE(12,*)
      WRITE(12,*) 'VW_XIPT'
      WRITE(12,*) VW_XIPT
      WRITE(12,*)
      WRITE(12,*) 'DJP12L'
      WRITE(12,*) DJP12L
      WRITE(12,*)
      WRITE(12,*) 'DJP6'
      WRITE(12,*) DJP6
      WRITE(12,*)
      WRITE(12,*) 'IP6'
      WRITE(12,*) IP6
      WRITE(12,*)

C---  FERMETURE DES FICHIERS
      CLOSE(12)

      RETURN
      END
      