//************************************************************************
// H2D2 - External declaration of public symbols
// Module: test_nsmacro3d_evalue
// Entry point: extern "C" void fake_dll_libtest_nsmacro3d_evalue()
//
// This file is generated automatically, any change will be lost.
// Generated 2008-01-23 16:37:51.953000
//************************************************************************
#include "cconfig.h"

#undef DSYM
#undef DNAM
#undef DNAM_UTL

#define DNAM_UTL(f) #f
#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define DSYM(fmaj, fmin) fmaj
#  define DNAM(fmaj, fmin) DNAM_UTL( fmaj )
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define DSYM(fmaj, fmin) fmin ## _
#  define DNAM(fmaj, fmin) DNAM_UTL( fmin ## _ )
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define DSYM(fmaj, fmin) fmin ## __
#  define DNAM(fmaj, fmin) DNAM_UTL( fmin ## __ )
#else
#  error Invalid Fortran to C mangling convention
#endif

#ifdef __cplusplus
extern "C"
{
#endif


// ---  class TEST_EVALUE

void fake_dll_lib_reg(void (*)(), const char*, const char*);

#ifdef FAKE_DLL
void fake_dll_libtest_nsmacro3d_evalue()
{
   static char libname[] = "libtest_nsmacro3d_evalue.so";

   // ---  class TEST_EVALUE
}
#endif    // FAKE_DLL

#ifdef __cplusplus
}
#endif

