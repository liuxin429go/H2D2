C=============================================================================
C
C=============================================================================
      PROGRAM ERR_ML

C      USE IFPORT

      IMPLICIT REAL*8 (A-H, O-Z)

      INTEGER      LISFICHIERS
      CHARACTER*256 LIGNE
      CHARACTER*24 NOMREP
      CHARACTER*24 NOMCOR
      CHARACTER*24 NOMINI
      CHARACTER*24 NOMFIN
      CHARACTER*24 TOKEN
      DIMENSION ERRL2(99)
      DIMENSION ERRMX(99)
      DIMENSION DELTA(99)
      LOGICAL      LRESGLOB
C-----------------------------------------------------------------------------

C      ISTAT = SYSTEM('dir /ad /b *.* > dir_lst.tmp')
      ISTAT = SYSTEM('find . -maxdepth 1 -type d -print > dir_lst.tmp')
      IF (ISTAT .eq. -1) GOTO 9000

      OPEN(UNIT=10,
     &     FILE='dir_lst.tmp',
     &     ACCESS= 'SEQUENTIAL',
     &     FORM= 'FORMATTED',
     &     STATUS='OLD',
     &     ERR=9001)

C ---  LIS LES DONNEES
      NFIC = 0
100   CONTINUE
         READ(10, '(A)', ERR=9002, END=110) NOMREP
         LNOMREP= LENSTR(NOMREP)
         IF (LNOMREP .LE. 0) GOTO 110
         NOMCOR = NOMREP(1:LNOMREP) // '/' // 'test.cor'
         NOMINI = NOMREP(1:LNOMREP) // '/' // 'test.sol'
         NOMFIN = NOMREP(1:LNOMREP) // '/' // 'test.fin'
         IERR = LISFICHIERS(NOMCOR(1:LENSTR(NOMCOR)),
     &                      NOMINI(1:LENSTR(NOMINI)),
     &                      NOMFIN(1:LENSTR(NOMFIN)),
     &                      DELTATMP,
     &                      ERRL2TMP,
     &                      ERRMXTMP)
         IF (IERR .EQ. 0) THEN
            NFIC = NFIC + 1
            DELTA(NFIC) = DELTATMP
            ERRL2(NFIC) = ERRL2TMP
            ERRMX(NFIC) = ERRMXTMP
         ENDIF
      GOTO 100
110   CONTINUE
      CLOSE(10)

C ---  TRIE LES DONNEES
      DO I=1,(NFIC-1)
         DO J=(I+1),NFIC
            IF (DELTA(J) .GT. DELTA(I)) THEN
               DELTMP = DELTA(I)
               DELTA(I) = DELTA(J)
               DELTA(J) = DELTMP
               ERRTMP = ERRL2(I)
               ERRL2(I) = ERRL2(J)
               ERRL2(J) = ERRTMP
               ERRTMP = ERRMX(I)
               ERRMX(I) = ERRMX(J)
               ERRMX(J) = ERRTMP
            ENDIF
         ENDDO
      ENDDO

C ---  LIS LES DONNEES CIBLE
      TGTERR = -1.0D0
      TGTPNT = -1.0D0
      TGTTOL = -1.0D0
      TGTMAX = -1.0D0
      TGTMIN = -1.0D0
      OPEN(UNIT=10, FILE='test.tgt', STATUS='OLD', ERR=9001)
200   CONTINUE
         READ(10, '(A)', ERR=9002, END=210) LIGNE
         INDEQ = INDEX(LIGNE, '=')
         IF (INDEQ .LE. 1) GOTO 200
         TOKEN = LIGNE(1:INDEQ-1)
         CALL TRIMSTR(TOKEN)
         CALL UCASESTR(TOKEN)
         READ(LIGNE(INDEQ+1:LENSTR(LIGNE)), *) VAL
         IF     (TOKEN(1:LENSTR(TOKEN)) .EQ. 'ERRL2') THEN
            TGTEL2 = VAL
C         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'ERRMAX') THEN
C            TGTEMX = VAL
         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'PENTE') THEN
            TGTPNT = VAL
         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'TOL') THEN
            TGTTOL = VAL
         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'MAX') THEN
            TGTMAX = VAL
         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'MIN') THEN
            TGTMIN = VAL
         ENDIF
      GOTO 200
210   CONTINUE
      CLOSE(10)

C ---  ANALYSE
      LRESGLOB = .FALSE.
      OPEN(UNIT=10, FILE='test.err', STATUS='UNKNOWN')
      IF (NFIC .LT. 1) THEN
         WRITE(10,*) 'pas de fichiers'
      ELSEIF (NFIC .LT. 2) THEN
         WRITE(10,*) 'del, errl2, errMax =',
     &                DELTA(NFIC), ERRL2(NFIC), ERRMX(NFIC)
         IF (TGTTOL .GE. 0.0D0) THEN
            WRITE(10,*) 'target tol=', TGTEL2, TGTTOL
            IF (ABS(ERRL2(NFIC)-TGTEL2) .LE. TGTTOL) LRESGLOB = .TRUE.
         ELSEIF (TGTMAX .GE. 0.0D0) THEN
            WRITE(10,*) 'target max=', TGTMAX
            IF (ERRL2(NFIC) .LE. TGTMAX) LRESGLOB = .TRUE.
         ELSEIF (TGTMIN .GE. 0.0D0) THEN
            WRITE(10,*) 'target min=', TGTMIN
            IF (ERRL2(NFIC) .GE. TGTMIN) LRESGLOB = .TRUE.
         ENDIF
      ELSE
         DO IFIC=1,NFIC
            WRITE(10,*) 'del, errl2, errMax =',
     &                   DELTA(IFIC), ERRL2(IFIC), ERRMX(IFIC)
         ENDDO
         IF (ERRL2(NFIC) .LT. 1.0D-15 .OR.
     &       ERRL2(NFIC) .LT. 1.0D-15) THEN
            PENTE = 1.0D123
         ELSE
            DERR  = LOG(ERRL2(NFIC))-LOG(ERRL2(NFIC-1))
            DDEL  = LOG(DELTA(NFIC))-LOG(DELTA(NFIC-1))
            PENTE = DERR/DDEL
         ENDIF
         WRITE(10,*) 'pente=',PENTE
         IF (TGTTOL .GE. 0.0D0) THEN
            WRITE(10,*) 'target tol=', TGTPNT, TGTTOL
            IF (ABS(PENTE-TGTPNT) .LE. TGTTOL) LRESGLOB = .TRUE.
         ELSEIF (TGTMAX .GE. 0.0D0) THEN
            WRITE(10,*) 'target max=', TGTMAX
            IF (PENTE .LE. TGTMAX) LRESGLOB = .TRUE.
         ELSEIF (TGTMIN .GE. 0.0D0) THEN
            WRITE(10,*) 'target min=', TGTMIN
            IF (PENTE .GE. TGTMIN) LRESGLOB = .TRUE.
         ENDIF
      ENDIF
      IF (LRESGLOB) THEN
         WRITE(10,*) 'test=ok'
      ELSE
         WRITE(10,*) 'test=erreur'
      ENDIF
      CLOSE(10)

      GOTO 9999

C-----------------------------------------------------------------------------
9000  WRITE(6,*) 'ERREUR A LA LECTURE DU REPERTOIRE'
      GOTO 9999
9001  WRITE(6,*) 'ERREUR A L''OUVERTURE DU FICHIER'
      GOTO 9999
9002  WRITE(6,*) 'ERREUR A LA LECTURE DU FICHIER'
      GOTO 9999
C-----------------------------------------------------------------------------

9999  CONTINUE
C      ISTAT = SYSTEM('del /f /q dir_lst.tmp')
      ISTAT = SYSTEM('rm -f dir_lst.tmp')

      END

C=============================================================================
C
C=============================================================================
      FUNCTION LISFICHIERS(NOMCOR, NOMINI, NOMFIN, DEL, ERRL2, ERRMAX)

      IMPLICIT REAL*8 (A-H, O-Z)

      INTEGER LISFICHIERS
      CHARACTER*(*) NOMCOR
      CHARACTER*(*) NOMINI
      CHARACTER*(*) NOMFIN

      DIMENSION C_EX(20)
      DIMENSION C_EF(20)
C-----------------------------------------------------------------------------

      IERR = 1
      ERRMAX = 0.0D0
      SUM  = 0.0D0

      OPEN(UNIT=20, FILE=NOMINI, STATUS='OLD', ERR=999)
      OPEN(UNIT=21, FILE=NOMFIN, STATUS='OLD', ERR=999)
      OPEN(UNIT=22, FILE=NOMCOR, STATUS='OLD', ERR=999)

      READ (20,*) NNT, NDLN
      READ (21,*) NNT, NDLN
      DO IN=1, NNT
         READ(20,*) (C_EX(ID),ID=1,NDLN)
         READ(21,*) (C_EF(ID),ID=1,NDLN)
         DO ID=1,NDLN
            DEL = ABS(C_EX(ID)-C_EF(ID))
            ERRMAX = MAX(ERRMAX, DEL)
            SUM = SUM + DEL*DEL
         ENDDO
      ENDDO
      SUM = SUM / DBLE(NDLN)

      READ (22,*) NNT, NDIM
      READ (22,*) X1,Y1
      READ (22,*) X2,Y2

      DEL = Y2-Y1
      ERRL2 = SQRT(SUM) / DBLE(NNT)
      IERR = 0

999   CONTINUE
      CLOSE(20)
      CLOSE(21)
      CLOSE(22)

      LISFICHIERS = IERR
      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE TRIMSTR(STR)
      CHARACTER*(*) STR
C-----------------------------------------------------------------------------

C------- LENGTH
      ILLIN=LENSTR(STR)

C-------  REMOVE LEADING BLANKS
      I = 0
100   CONTINUE
         I = I + 1
      IF ((STR(I:I) .EQ. ' ') .AND. (I .LT. ILLIN)) GOTO 100
      STR = STR(I:ILLIN)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      FUNCTION LENSTR(STR)
      CHARACTER*(*) STR
C-----------------------------------------------------------------------------

      DO I=LEN(STR),1,-1
         IF (STR(I:I) .NE. ' ') GOTO 100
      ENDDO
100   LENSTR = I

      RETURN
      END

C=============================================================================
C     ALL UPPERCASE EXCEPT BETWEEN '
C=============================================================================
      SUBROUTINE UCASESTR(STR)
      CHARACTER*(*) STR
      LOGICAL ITRUP
C-----------------------------------------------------------------------

      ITRUP = .TRUE.
      DO I=1,LENSTR(STR)
         IF (STR(I:I) .EQ. '''') ITRUP = .NOT. ITRUP
         IF ((STR(I:I) .GE. 'a') .AND. (STR(I:I) .LE. 'z')) THEN
            IF (ITRUP) STR(I:I) = CHAR(ICHAR(STR(I:I))-32)
         ENDIF
      ENDDO

      RETURN
      END

