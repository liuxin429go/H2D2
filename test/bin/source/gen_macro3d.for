C=============================================================================
C     h2d2 NS_MACRO3D
C     Ne génère pour l'instant que des solutions constantes sur la verticale
C=============================================================================
      PROGRAM GEN_MACRO3D

      INCLUDE 'gentest.fi'

      PARAMETER (NDIM =2)
      PARAMETER (NPRNT=6)
      PARAMETER (NPRNH=5)
      PARAMETER (NDLMAX2D=3)
      PARAMETER (NDLMAX=5)
      PARAMETER (NDIMV=150*150)

      ! On alloue dynamiquement
      ALLOCATABLE :: VCORG(:)    !Table des coordonnées globales
      ALLOCATABLE :: VPRNT(:)    !Tables des propriétés nodales
      ALLOCATABLE :: VPRNH(:)    !
      ALLOCATABLE :: VSOLR(:)    !Table des sollicitations réparties
      ALLOCATABLE :: VDLG (:)    !Table des degrés de libertés
      ALLOCATABLE :: KDIMP(:)    !(table des valeurs imposées ??)
      ALLOCATABLE :: KNG  (:)
      CHARACTER*256 NOMBASE
      CHARACTER*256 MNT
      CHARACTER*256 ECOUL
      CHARACTER*256 CONDLIM
      CHARACTER*4   TYPEL
      LOGICAL       LNODRND
      INTEGER       I
      INTEGER       NDLN   !nombre de dl par noeud (5)

C-----------------------------------------------------------------------------
C---  VARIABLES
      NDLN = 5

C-------  LIS LES DONNEES
      READ(5,*) NOMBASE
      READ(5,*) NELX
      READ(5,*) NELY

      READ(5,*) XORI
      READ(5,*) YORI
      READ(5,*) DIMX
      READ(5,*) DIMY
      READ(5,*) NNEL       !3 ou 6
      READ(5,*) TYPEL      !droit, penché, en x... pour mailler
      READ(5,*) MNT        !modèle numérique de terrain...
      READ(5,*) ECOUL      !écoulement...
      READ(5,*) CONDLIM    !conditions limites

C-------  PRE-TRAITE
C---  TYPEL
      LNODRND = .FALSE.
      CALL TRIMSTR(TYPEL)
      CALL UCASESTR(TYPEL)
      IF (TYPEL(1:1) .EQ. '~') THEN
         LNODRND = .TRUE.
         TYPEL(1:1) = ' '
         CALL TRIMSTR(TYPEL)
      ENDIF
      IF (TYPEL(1:1) .EQ. '/') THEN
         ITYPEL = IEL$SLASH
      ELSEIF (TYPEL(1:1) .EQ. '\') THEN
         ITYPEL = IEL$SLINV
      ELSEIF (TYPEL(1:1) .EQ. 'v') THEN
         ITYPEL = IEL$V
      ELSEIF (TYPEL(1:1) .EQ. 'V') THEN
         ITYPEL = IEL$V
      ELSEIF (TYPEL(1:1) .EQ. '^') THEN
         ITYPEL = IEL$VINV
      ELSEIF (TYPEL(1:1) .EQ. '>') THEN
         ITYPEL = IEL$GT
      ELSEIF (TYPEL(1:1) .EQ. '<') THEN
         ITYPEL = IEL$LT
      ELSEIF (TYPEL(1:1) .EQ. 'X') THEN
         ITYPEL = IEL$X
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:TYPEL=', TYPEL
         STOP
      ENDIF
C---  NNEL
      IF (NNEL .EQ. 3) THEN
         NNX = NELX + 1
         NNY = NELY + 1
      ELSEIF(NNEL .EQ. 6) THEN
         NNX = 2*NELX + 1
         NNY = 2*NELY + 1
      ELSE
         WRITE(*,*) 'NOMBRE DE NOEUDS PAR ELEMENT INVALIDE:', NNEL
         STOP
      ENDIF

C---  NNT
      NNT = NNX*NNY

C---  Extrait noms...
      CALL TRIMSTR(NOMBASE)
      CALL LCASESTR(NOMBASE)
      CALL TRIMSTR(MNT)
      CALL UCASESTR(MNT)
      CALL TRIMSTR(ECOUL)
      CALL UCASESTR(ECOUL)

C-------  ALLOUE LES TABLES
      ALLOCATE(VCORG(NDIM*NNT))
      ALLOCATE(VPRNT(NPRNT*NNT))
      ALLOCATE(VPRNH(NPRNH*NNT))
      ALLOCATE(VSOLR(NDLMAX*NNT))
      ALLOCATE(VDLG (NDLMAX*NNT))
      ALLOCATE(KDIMP(NDLMAX*NNT))
      ALLOCATE(KNG  (6*NNT))

C-------  GENERE LE MAILLAGE
      CALL GENCOOR(LNODRND, XORI, YORI, DIMX, DIMY, NNX, NNY, VCORG) !sortie : vcorg
      CALL GENELEM(ITYPEL, NNX, NNY, VCORG, NNEL, NELT, KNG) !sortie KNG

C-------  GENERE LE MODELE NUMERIQUE DE TERRAIN
      CALL GENMNT  (MNT(1:LENSTR(MNT)),
     &              NDIM, NNX, NNY, NNT, VCORG,
     &              NNEL, NELT, KNG,
     &              NPRNT, VPRNT) !sortie VPRNT

C-------  GENERE L'HYDRODYNAMIQUE
      CALL GENHYDRO(ECOUL(1:LENSTR(ECOUL)),
     &              NDIM, NNX, NNY, NNT, VCORG,
     &              NNEL, NELT, KNG,
     &              NPRNH, VPRNH, !nprnh = 3
     &              NDLN,  VSOLR, VPRNT) !sorties : vsolr, vprnh
      CALL GENHYDRO3D(NNT, NDLN, VSOLR) !entrée VSOLR, NNT, NDLN, sortie VSOLR

C-------  GENERE LA SOLUTION
      CALL GENSOL_M3D  (NNT,
     &              NPRNT, VPRNT,
     &              NPRNH, VPRNH,
     &              NDLN,  VDLG) !sorties : vdlg

C-------  GENERE LES CONDITIONS LIMITES
      CALL GENCL   (CONDLIM(1:LENSTR(CONDLIM)),
     &              NNX, NNY, NNT,
     &              NDLN, KDIMP) !sortie : kdimp

C-------  ECRIS LES RESULTATS
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.cor',
     &              NNT,
     &              NDIM,
     &              VCORG)
      CALL ECRISELE(NOMBASE(1:LENSTR(NOMBASE))//'.ele',
     &              NELT,
     &              NNEL,
     &              KNG)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.pnt',
     &              NNT,
     &              NPRNT,
     &              VPRNT)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.slr',
     &              NNT,
     &              NDLN,
     &              VSOLR)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.ini',
     &              NNT,
     &              NDLN,
     &              VDLG)
      CALL ECRISCND(NOMBASE(1:LENSTR(NOMBASE))//'.cnd',
     &              NNT,
     &              NDLN,
     &              VDLG, KDIMP)
      CALL ECRISBND(NOMBASE(1:LENSTR(NOMBASE))//'.bnd',
     &              NNT,
     &              NDLN,
     &              VDLG, KDIMP)

C-------  DESALLOUE LES TABLES
      DEALLOCATE(VCORG)
      DEALLOCATE(VPRNT)
      DEALLOCATE(VPRNH)
      DEALLOCATE(VSOLR)
      DEALLOCATE(VDLG)
      DEALLOCATE(KDIMP)
      DEALLOCATE(KNG)

      END

C=============================================================================
C     SORTIE : VDLG
C=============================================================================
      SUBROUTINE GENSOL_M3D(NNT, NPRNT, VPRNT, NPRNH, VPRNH, NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VPRNT(NPRNT, 1)
      DIMENSION VPRNH(NPRNH, 1)
      DIMENSION VDLG(NDLN,1)
C-----------------------------------------------------------------------------

C---  POUR US=UF et VS=VF seuleemnt
      DO IN=1,NNT
         VDLG(1, IN) = VPRNH(1, IN)   ! u
         VDLG(2, IN) = VPRNH(1, IN)   ! u
         VDLG(3, IN) = VPRNH(2, IN)   ! v
         VDLG(4, IN) = VPRNH(2, IN)   ! v
         VDLG(5, IN) = VPRNH(3, IN) !+ VPRNT(1, IN)   ! h = zf+H
      ENDDO

      RETURN
      END

C=============================================================================
C     SORTIE : VSOLR
C     Passe d'un VSOLR 3d en uvh à un VSOLR 3d en uf us vf vs h
C     Serait plus joli avec 2 tables, une VSOLR2D et une VSOLR
C=============================================================================
      SUBROUTINE GENHYDRO3D(  NNT, NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION     VSOLR(NDLN, 1)   !Table des sollicitations réparties

C-----------------------------------------------------------------------------

C---  POUR US=UF et VS=VF seuleemnt
      DO IN=1,NNT
         VSOLR(5, IN) = VSOLR(3, IN)! + VPRNT(1, IN)   ! h = zf+H
         VSOLR(4, IN) = VSOLR(2, IN)   ! v
         VSOLR(3, IN) = VSOLR(2, IN)   ! v
         VSOLR(2, IN) = VSOLR(1, IN)   ! u
!         VSOLR(1, IN) = VSOLR(1, IN)   ! u
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISCND(NOMFIC, NNT, NDLN, VDLG, KDIMP)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)

      CHARACTER*256 NOMCL
      CHARACTER*32  FMT
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I3,', NDLN, '(1X,1PE25.17E3),A)'

      NCND = 0
      DO IN=1,NNT
         IF (KDIMP(1, IN) .GT. 0) NCND = NCND + 1
      ENDDO
      WRITE(10, '(I6,1X,1PE25.17E3)') NCND, 0.0


100   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199

         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               KDIMP(1,IN) = -KDIMP(1,IN)

               WRITE(NOMCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
               CALL TRIMSTR(NOMCL)
               CALL SBSTSTR(NOMCL, ' ','_')

               WRITE(10, '(A,I3,6(1X,1PE25.17E3))')
     &                        NOMCL(1:LENSTR(NOMCL)),
     &                        MOD(IC,1000),
     &                        (VDLG(ID,IN),ID=1,NDLN)
            ENDIF
         ENDDO

      GOTO 100
199   CONTINUE

      CLOSE(10)

      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISBND(NOMFIC, NNT, NDLN, VDLG, KDIMP)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)

      CHARACTER*256 NOMCL
      CHARACTER*32  FMT
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I3,', NDLN, '(1X,1PE25.17E3),A)'

      NCL = 0
      DO IN=1,NNT
         IF (KDIMP(1, IN) .GT. 0) NCL = NCL + 1
      ENDDO
      WRITE(10, '(I9)') NCL

100   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199

         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               KDIMP(1,IN) = -KDIMP(1,IN)

               WRITE(NOMCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
               CALL TRIMSTR(NOMCL)
               CALL SBSTSTR(NOMCL, ' ','_')

               WRITE(10, '(A,I9)') NOMCL(1:LENSTR(NOMCL)), IN
            ENDIF
         ENDDO

      GOTO 100
199   CONTINUE

      CLOSE(10)

      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISELE(NOMFIC, NELT, NNEL, KNG)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION KNG(NNEL, 1)
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')

      WRITE(10, '(3I6,1X,1PE25.17E3)') NELT, NNEL, 303, 0.0 !303 pour macro3d


      DO IE=1,NELT
         WRITE(10, '(6I9)') (KNG(I, IE), I=1,NNEL)
      ENDDO
      CLOSE(10)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISPRN(NOMFIC, NNT, NPRN, VPRN)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VPRN(NPRN, 1)
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(10, '(2I6,1X,1PE25.17E3)') NNT, NPRN, 0.0
      DO IN=1,NNT
         WRITE(10, '(25(1X,1PE25.17E3))') (VPRN(I, IN), I=1,NPRN)
      ENDDO
      CLOSE(10)

      RETURN
      END
