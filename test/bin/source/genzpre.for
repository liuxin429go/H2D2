C=============================================================================
C     SORTIE VPRN, VDLG
C=============================================================================
      SUBROUTINE GENZPRE(ECOULEMENT,
     &                           NDIM, NNX, NNY, NNT, VCORG,
     &                           NNEL, NELT, KNG,
     &                           NPRN, VPRN,
     &                           NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      CHARACTER*(*) ECOULEMENT

      IF (ECOULEMENT .EQ. 'ZPREC') THEN
         CALL ZPRE_C  (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NDLN, VDLG)
      ELSEIF (ECOULEMENT .EQ. 'ZPRE1XY') THEN
         CALL ZPRE_1XY  (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NDLN, VDLG)
      ELSEIF (ECOULEMENT .EQ. 'ZPREEXY') THEN
         CALL ZPREE_XY  (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NDLN, VDLG)
      ELSEIF (ECOULEMENT .EQ. 'ZPREEIXY') THEN
         CALL ZPREE_IXY  (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NDLN, VDLG)
      ELSEIF (ECOULEMENT .EQ. 'ZPREBX') THEN
         CALL ZPREB_X  (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NDLN, VDLG)
      ELSEIF (ECOULEMENT .EQ. 'ZPREX') THEN
         CALL ZPRE_X  (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NDLN, VDLG)
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:ECOUL=', ECOULEMENT
         STOP
      ENDIF

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ZPRE_C(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      ZPRE  = 0.0D0
      IDZ = NDLN

      DO IN=1,NNT
         VDLG(IDZ,IN) = ZPRE
         VPRN(1,IN)   = ZPRE
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ZPRE_1XY(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      DO IN=1,NNT
       X            = VCORG(1, IN) - VCORG(1, 1)
       Y            = VCORG(2, IN) - VCORG(2, 1)
       ZPRE         = X + Y
       VDLG(IDZ,IN) = ZPRE
       VPRN(1,IN)   = ZPRE
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ZPREE_XY(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      DO IN=1,NNT
         X          = VCORG(1, IN)
         Y          = VCORG(2, IN)
       ZPRE         = EXP(X+Y)
       VDLG(IDZ,IN) = ZPRE + ZPRE
       VPRN(1,IN)   = ZPRE
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ZPREE_IXY(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      DO IN=1,NNT
         X          = VCORG(1, IN)
         Y          = VCORG(2, IN)
       ZPRE         = -EXP(X+Y)
       VDLG(IDZ,IN) = ZPRE + ZPRE
       VPRN(1,IN)   = ZPRE
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ZPREB_X(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      ARTVIS = 1.0D0
      G      = 9.810D0
      RHOW   = 1.0D+03
      VISCW  = 1.00D-06
      RHOS   = 2.65D+03
      PHIS   = 1.00D+0
      DMTS   = 2.0D0**PHIS/1000.0D0
      H      = 0.10D0
      FRS      = 0.025D0
      RD_UN  = RHOS / RHOW - 1.0D0
      DGR    = DMTS * (G*RD_UN/VISCW**2.0D0)**(1.0D0/3.0D0)
      FN     = 1.0D0 - 5.60D-01*LOG10(DGR)
      A      = 2.30D-01/SQRT(DGR) + 1.4D-01
      FM     = 6.830D0 / DGR + 1.670D0
      C      = 10.0D0**(2.790D0*LOG10(DGR)-
     &         0.98D0*LOG10(DGR)**2 - 3.46D0)
      UBAR   = 1.0D0
      USTR   = SQRT(G/H**(1.0D0/3.0D0))*FRS*UBAR
      FGR1   = USTR**FN /SQRT(G*RD_UN*DMTS)
      FGR2   = UBAR /(SQRT(32.0D0)*LOG10(10.0D0*H/DMTS))
      FGR    = FGR1 * FGR2**(1.0D0-FN)
      IF (FGR .GT. A)THEN
        GGR = C*((FGR/A-1.0D0)**FM)
        QB  = GGR*DMTS*((UBAR/USTR)**FN)*UBAR
      ELSE
        DQBDX  = 0.0D0
      ENDIF

      DO IN=1,NNT
       X            = (VCORG(1, IN) - VCORG(1, 1))
       ZPRE         = QB/(2.0D0*UBAR*ARTVIS)*X*X
       VDLG(IDZ,IN) = ZPRE
       VPRN(1,IN)   = ZPRE
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ZPRE_X(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      DO IN=1,NNT
       X            = VCORG(1, IN) - VCORG(1, 1)
       ZPRE         = -1.40D-03*X
       VDLG(IDZ,IN) = ZPRE
       VPRN(1,IN)   = ZPRE
      ENDDO

      RETURN
      END
		