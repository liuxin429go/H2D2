      FUNCTION R1MACH(I)
      IMPLICIT REAL*8 (A-H,O-Z)

      IF (I .EQ. 1) THEN
         R1MACH = 2.2250738585072020D-308    ! DBL_MIN
      ELSEIF  (I .EQ. 2) THEN
         R1MACH = 1.7976931348623150D+308    ! DBL_MAX
      ELSEIF  (I .EQ. 3) THEN
         R1MACH = 2.2204460492503131D-16/2   ! DBL_EPSILON/FLT_RADIX
      ELSEIF  (I .EQ. 4) THEN
         R1MACH = 2.2204460492503131D-16     ! DBL_EPSILON
      ELSEIF  (I .EQ. 5) THEN
         R1MACH = LOG10(2.0D0)               ! log10((double)FLT_RADIX)
      ENDIF

      RETURN
      END

*DECK CSEVL
      FUNCTION CSEVL (X, CS, N)
C***BEGIN PROLOGUE  CSEVL
C***PURPOSE  Evaluate a Chebyshev series.
C***LIBRARY   SLATEC (FNLIB)
C***CATEGORY  C3A2
C***TYPE      SINGLE PRECISION (CSEVL-S, DCSEVL-D)
C***KEYWORDS  CHEBYSHEV SERIES, FNLIB, SPECIAL FUNCTIONS
C***AUTHOR  Fullerton, W., (LANL)
C***DESCRIPTION
C
C  Evaluate the N-term Chebyshev series CS at X.  Adapted from
C  a method presented in the paper by Broucke referenced below.
C
C       Input Arguments --
C  X    value at which the series is to be evaluated.
C  CS   array of N terms of a Chebyshev series.  In evaluating
C       CS, only half the first coefficient is summed.
C  N    number of terms in array CS.
C
C***REFERENCES  R. Broucke, Ten subroutines for the manipulation of
C                 Chebyshev series, Algorithm 446, Communications of
C                 the A.C.M. 16, (1973) pp. 254-256.
C               L. Fox and I. B. Parker, Chebyshev Polynomials in
C                 Numerical Analysis, Oxford University Press, 1968,
C                 page 56.
C***ROUTINES CALLED  R1MACH, XERMSG
C***REVISION HISTORY  (YYMMDD)
C   770401  DATE WRITTEN
C   890831  Modified array declarations.  (WRB)
C   890831  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900315  CALLs to XERROR changed to CALLs to XERMSG.  (THJ)
C   900329  Prologued revised extensively and code rewritten to allow
C           X to be slightly outside interval (-1,+1).  (WRB)
C   920501  Reformatted the REFERENCES section.  (WRB)
C***END PROLOGUE  CSEVL
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 B0, B1, B2, CS(*), ONEPL, TWOX, X
      LOGICAL FIRST
      SAVE FIRST, ONEPL
      DATA FIRST /.TRUE./
C***FIRST EXECUTABLE STATEMENT  CSEVL
      IF (FIRST) ONEPL = 1.0D0 + R1MACH(4)
      FIRST = .FALSE.
      IF (N .LT. 1) CALL XERMSG ('SLATEC', 'CSEVL',
     +   'NUMBER OF TERMS .LE. 0', 2, 2)
      IF (N .GT. 1000) CALL XERMSG ('SLATEC', 'CSEVL',
     +   'NUMBER OF TERMS .GT. 1000', 3, 2)
      IF (ABS(X) .GT. ONEPL) CALL XERMSG ('SLATEC', 'CSEVL',
     +   'X OUTSIDE THE INTERVAL (-1,+1)', 1, 1)
C
      B1 = 0.0D0
      B0 = 0.0D0
      TWOX = 2.0D0*X
      DO 10 I = 1,N
         B2 = B1
         B1 = B0
         NI = N + 1 - I
         B0 = TWOX*B1 - B2 + CS(NI)
   10 CONTINUE
C
      CSEVL = 0.5D0*(B0-B2)
C
      RETURN
      END

*DECK ERFC
      FUNCTION ERFC (X)
C***BEGIN PROLOGUE  ERFC
C***PURPOSE  Compute the complementary error function.
C***LIBRARY   SLATEC (FNLIB)
C***CATEGORY  C8A, L5A1E
C***TYPE      SINGLE PRECISION (ERFC-S, DERFC-D)
C***KEYWORDS  COMPLEMENTARY ERROR FUNCTION, ERFC, FNLIB,
C             SPECIAL FUNCTIONS
C***AUTHOR  Fullerton, W., (LANL)
C***DESCRIPTION
C
C ERFC(X) calculates the single precision complementary error
C function for single precision argument X.
C
C Series for ERF        on the interval  0.          to  1.00000D+00
C                                        with weighted error   7.10E-18
C                                         log weighted error  17.15
C                               significant figures required  16.31
C                                    decimal places required  17.71
C
C Series for ERFC       on the interval  0.          to  2.50000D-01
C                                        with weighted error   4.81E-17
C                                         log weighted error  16.32
C                        approx significant figures required  15.0
C
C
C Series for ERC2       on the interval  2.50000D-01 to  1.00000D+00
C                                        with weighted error   5.22E-17
C                                         log weighted error  16.28
C                        approx significant figures required  15.0
C                                    decimal places required  16.96
C
C***REFERENCES  (NONE)
C***ROUTINES CALLED  CSEVL, INITS, R1MACH, XERMSG
C***REVISION HISTORY  (YYMMDD)
C   770701  DATE WRITTEN
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   890531  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900315  CALLs to XERROR changed to CALLs to XERMSG.  (THJ)
C   920618  Removed space from variable names.  (RWC, WRB)
C***END PROLOGUE  ERFC
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION ERFCS(13), ERFCCS(24), ERC2CS(23)
      LOGICAL FIRST
      SAVE ERFCS, ERC2CS, ERFCCS, SQRTPI, NTERF, NTERFC,
     1 NTERC2, XSML, XMAX, SQEPS, FIRST
      DATA ERFCS( 1) /   -.0490461212 34691808D0 /
      DATA ERFCS( 2) /   -.1422612051 0371364D0 /
      DATA ERFCS( 3) /    .0100355821 87599796D0 /
      DATA ERFCS( 4) /   -.0005768764 69976748D0 /
      DATA ERFCS( 5) /    .0000274199 31252196D0 /
      DATA ERFCS( 6) /   -.0000011043 17550734D0 /
      DATA ERFCS( 7) /    .0000000384 88755420D0 /
      DATA ERFCS( 8) /   -.0000000011 80858253D0 /
      DATA ERFCS( 9) /    .0000000000 32334215D0 /
      DATA ERFCS(10) /   -.0000000000 00799101D0 /
      DATA ERFCS(11) /    .0000000000 00017990D0 /
      DATA ERFCS(12) /   -.0000000000 00000371D0 /
      DATA ERFCS(13) /    .0000000000 00000007D0 /
      DATA ERC2CS( 1) /   -.0696013466 02309501D0 /
      DATA ERC2CS( 2) /   -.0411013393 62620893D0 /
      DATA ERC2CS( 3) /    .0039144958 66689626D0 /
      DATA ERC2CS( 4) /   -.0004906395 65054897D0 /
      DATA ERC2CS( 5) /    .0000715747 90013770D0 /
      DATA ERC2CS( 6) /   -.0000115307 16341312D0 /
      DATA ERC2CS( 7) /    .0000019946 70590201D0 /
      DATA ERC2CS( 8) /   -.0000003642 66647159D0 /
      DATA ERC2CS( 9) /    .0000000694 43726100D0 /
      DATA ERC2CS(10) /   -.0000000137 12209021D0 /
      DATA ERC2CS(11) /    .0000000027 88389661D0 /
      DATA ERC2CS(12) /   -.0000000005 81416472D0 /
      DATA ERC2CS(13) /    .0000000001 23892049D0 /
      DATA ERC2CS(14) /   -.0000000000 26906391D0 /
      DATA ERC2CS(15) /    .0000000000 05942614D0 /
      DATA ERC2CS(16) /   -.0000000000 01332386D0 /
      DATA ERC2CS(17) /    .0000000000 00302804D0 /
      DATA ERC2CS(18) /   -.0000000000 00069666D0 /
      DATA ERC2CS(19) /    .0000000000 00016208D0 /
      DATA ERC2CS(20) /   -.0000000000 00003809D0 /
      DATA ERC2CS(21) /    .0000000000 00000904D0 /
      DATA ERC2CS(22) /   -.0000000000 00000216D0 /
      DATA ERC2CS(23) /    .0000000000 00000052D0 /
      DATA ERFCCS( 1) /   0.0715179310 202925D0 /
      DATA ERFCCS( 2) /   -.0265324343 37606719D0 /
      DATA ERFCCS( 3) /    .0017111539 77920853D0 /
      DATA ERFCCS( 4) /   -.0001637516 63458512D0 /
      DATA ERFCCS( 5) /    .0000198712 93500549D0 /
      DATA ERFCCS( 6) /   -.0000028437 12412769D0 /
      DATA ERFCCS( 7) /    .0000004606 16130901D0 /
      DATA ERFCCS( 8) /   -.0000000822 77530261D0 /
      DATA ERFCCS( 9) /    .0000000159 21418724D0 /
      DATA ERFCCS(10) /   -.0000000032 95071356D0 /
      DATA ERFCCS(11) /    .0000000007 22343973D0 /
      DATA ERFCCS(12) /   -.0000000001 66485584D0 /
      DATA ERFCCS(13) /    .0000000000 40103931D0 /
      DATA ERFCCS(14) /   -.0000000000 10048164D0 /
      DATA ERFCCS(15) /    .0000000000 02608272D0 /
      DATA ERFCCS(16) /   -.0000000000 00699105D0 /
      DATA ERFCCS(17) /    .0000000000 00192946D0 /
      DATA ERFCCS(18) /   -.0000000000 00054704D0 /
      DATA ERFCCS(19) /    .0000000000 00015901D0 /
      DATA ERFCCS(20) /   -.0000000000 00004729D0 /
      DATA ERFCCS(21) /    .0000000000 00001432D0 /
      DATA ERFCCS(22) /   -.0000000000 00000439D0 /
      DATA ERFCCS(23) /    .0000000000 00000138D0 /
      DATA ERFCCS(24) /   -.0000000000 00000048D0 /
      DATA SQRTPI /1.772453850 9055160D0/
      DATA FIRST /.TRUE./
C***FIRST EXECUTABLE STATEMENT  ERFC
      IF (FIRST) THEN
         ETA = 0.1D0*R1MACH(3)
         NTERF = INITS (ERFCS, 13, ETA)
         NTERFC = INITS (ERFCCS, 24, ETA)
         NTERC2 = INITS (ERC2CS, 23, ETA)
C
         XSML = -SQRT (-LOG(SQRTPI*R1MACH(3)))
         TXMAX = SQRT (-LOG(SQRTPI*R1MACH(1)))
         XMAX = TXMAX - 0.5D0*LOG(TXMAX)/TXMAX - 0.01D0
         SQEPS = SQRT (2.0D0*R1MACH(3))
      ENDIF
      FIRST = .FALSE.
C
      IF (X.GT.XSML) GO TO 20
C
C ERFC(X) = 1.0 - ERF(X) FOR X .LT. XSML
C
      ERFC = 2.0D0
      RETURN
C
 20   IF (X.GT.XMAX) GO TO 40
      Y = ABS(X)
      IF (Y.GT.1.0D0) GO TO 30
C
C ERFC(X) = 1.0 - ERF(X) FOR -1. .LE. X .LE. 1.
C
      IF (Y.LT.SQEPS) ERFC = 1.0D0 - 2.0D0*X/SQRTPI
      IF (Y.GE.SQEPS) ERFC = 1.0D0 -
     1  X*(1.0D0 + CSEVL (2.0D0*X*X-1.0D0, ERFCS, NTERF) )
      RETURN
C
C ERFC(X) = 1.0 - ERF(X) FOR 1. .LT. ABS(X) .LE. XMAX
C
 30   Y = Y*Y
      IF (Y.LE.4.0D0) ERFC = EXP(-Y)/ABS(X) *
     1   (0.5D0 + CSEVL ((8.0D0/Y-5.0D0)/3.0D0, ERC2CS, NTERC2) )
      IF (Y.GT.4.0D0) ERFC = EXP(-Y)/ABS(X) *
     1   (0.5D0 + CSEVL (8.0D0/Y-1.0D0, ERFCCS, NTERFC) )
      IF (X.LT.0.0D0) ERFC = 2.0D0 - ERFC
      RETURN
C
 40   CALL XERMSG ('SLATEC', 'ERFC', 'X SO BIG ERFC UNDERFLOWS', 1, 1)
      ERFC = 0.0D0
      RETURN
C
      END

*DECK INITS
      FUNCTION INITS (OS, NOS, ETA)
C***BEGIN PROLOGUE  INITS
C***PURPOSE  Determine the number of terms needed in an orthogonal
C            polynomial series so that it meets a specified accuracy.
C***LIBRARY   SLATEC (FNLIB)
C***CATEGORY  C3A2
C***TYPE      SINGLE PRECISION (INITS-S, INITDS-D)
C***KEYWORDS  CHEBYSHEV, FNLIB, INITIALIZE, ORTHOGONAL POLYNOMIAL,
C             ORTHOGONAL SERIES, SPECIAL FUNCTIONS
C***AUTHOR  Fullerton, W., (LANL)
C***DESCRIPTION
C
C  Initialize the orthogonal series, represented by the array OS, so
C  that INITS is the number of terms needed to insure the error is no
C  larger than ETA.  Ordinarily, ETA will be chosen to be one-tenth
C  machine precision.
C
C             Input Arguments --
C   OS     single precision array of NOS coefficients in an orthogonal
C          series.
C   NOS    number of coefficients in OS.
C   ETA    single precision scalar containing requested accuracy of
C          series.
C
C***REFERENCES  (NONE)
C***ROUTINES CALLED  XERMSG
C***REVISION HISTORY  (YYMMDD)
C   770401  DATE WRITTEN
C   890831  Modified array declarations.  (WRB)
C   891115  Modified error message.  (WRB)
C   891115  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900315  CALLs to XERROR changed to CALLs to XERMSG.  (THJ)
C***END PROLOGUE  INITS
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 OS(*)
C***FIRST EXECUTABLE STATEMENT  INITS
      IF (NOS .LT. 1) CALL XERMSG ('SLATEC', 'INITS',
     +   'Number of coefficients is less than 1', 2, 1)
C
      ERR = 0.0D0
      DO 10 II = 1,NOS
        I = NOS + 1 - II
        ERR = ERR + ABS(OS(I))
        IF (ERR.GT.ETA) GO TO 20
   10 CONTINUE
C
   20 IF (I .EQ. NOS) CALL XERMSG ('SLATEC', 'INITS',
     +   'Chebyshev series too short for specified accuracy', 1, 1)
      INITS = I
C
      RETURN
      END

      SUBROUTINE XERMSG(MSG1, MSG2, MSG3, I1, I2)
      CHARACTER*(*) MSG1
      CHARACTER*(*) MSG2
      CHARACTER*(*) MSG3

      WRITE(6,*) MSG1
      WRITE(6,*) MSG2
      WRITE(6,*) MSG3

      STOP
      END
