C=============================================================================
C     SORTIE VPRN
C=============================================================================
      SUBROUTINE GENTL2(ECOULEMENT,
     &                  NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN, TL2)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)

      CHARACTER*(*) ECOULEMENT
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         VPRN(1,IN) = TL2
      ENDDO
      RETURN
      END
      