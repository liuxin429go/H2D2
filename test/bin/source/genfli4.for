C=============================================================================
C     SORTIE VPRN, VSOL
C=============================================================================
      SUBROUTINE GENFLI4(ECOULEMENT,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRN, VPRN)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)

      CHARACTER*(*) ECOULEMENT
C-----------------------------------------------------------------------------

      IF (ECOULEMENT .EQ. 'FLIC' .OR. ECOULEMENT .EQ. 'FLI1X') THEN
         CALL FLI_C4  (NDIM, NNX, NNY, NNT, VCORG,
     &               NNEL, NELT, KNG,
     &               NPRN, VPRN)
      ELSE IF (ECOULEMENT .EQ. 'FLIL') THEN
         CALL FLI_L4  (NDIM, NNX, NNY, NNT, VCORG,
     &               NNEL, NELT, KNG,
     &               NPRN, VPRN)
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:ECOUL=', ECOULEMENT
         STOP
      ENDIF

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE FLI_C4(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
C-----------------------------------------------------------------------------

      FLI   = 1.0D0/NPRN

      DO IN=1,NNT
	      DO IC = 1,NPRN
            VPRN(IC,IN) = FLI
	      ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE FLI_L4(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
C-----------------------------------------------------------------------------

      FLI1   = 0.4D0
      FLI2   = 0.6D0
      DO IN=1,NNT
         VPRN(1,IN) = FLI1
	      DO IC = 2,NPRN
            VPRN(IC,IN) = FLI2
	      ENDDO
      ENDDO

      RETURN
      END
