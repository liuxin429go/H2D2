C=============================================================================
C     SORTIE VPRN, VDLG
C=============================================================================
      SUBROUTINE GENFLI1(ECOULEMENT,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRN, VPRN,
     &                   NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)

      CHARACTER*(*) ECOULEMENT
C-----------------------------------------------------------------------------

      IF (ECOULEMENT .EQ. 'FLIC') THEN
         CALL FLI_C1  (NDIM, NNX, NNY, NNT, VCORG,
     &               NNEL, NELT, KNG,
     &               NPRN, VPRN,
     &               NDLN, VDLG)
      ELSE IF (ECOULEMENT .EQ. 'FLIL') THEN
         CALL FLI_L1  (NDIM, NNX, NNY, NNT, VCORG,
     &               NNEL, NELT, KNG,
     &               NPRN, VPRN,
     &               NDLN, VDLG)
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:ECOUL=', ECOULEMENT
         STOP
      ENDIF

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE FLI_C1(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      FLI   = 1.0D0/NPRN
      DO IN=1,NNT
	      DO IC = 1,NPRN
            VDLG(IC,IN) = FLI
            VPRN(IC,IN) = FLI
	      ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE FLI_L1(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NDLN, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      FLI1   = 1.0D0
      FLI2   = 0.0D0
      DO IN=1,NNT
         VDLG(1,IN) = FLI1
         VPRN(1,IN) = FLI1
	      DO IC = 2,NPRN
            VDLG(IC,IN) = FLI2
            VPRN(IC,IN) = FLI2
	      ENDDO
      ENDDO

      RETURN
      END