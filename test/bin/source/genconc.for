C=============================================================================
C
C=============================================================================
      SUBROUTINE GENCONC(SUBCMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) SUBCMD
      DIMENSION VCORG(*)
      DIMENSION KNG  (*)
      DIMENSION VPRH (*)
      DIMENSION VPRD (*)
      DIMENSION VDLG (*)
      DIMENSION VSOLR(*)

      CHARACTER*256 TYP_CONC
      CHARACTER*256 PARAM_CMD
C-----------------------------------------------------------------------------

      IDEB = INSTR(SUBCMD, '(')
      IF (IDEB .NE. -1) THEN
         IFIN = INSTR(SUBCMD, ')')
         TYP_CONC  = SUBCMD(1:IDEB-1)
         PARAM_CMD = SUBCMD(IDEB+1:IFIN-1)
      ELSE
         TYP_CONC  = SUBCMD
         PARAM_CMD = ' '
      ENDIF

      IF (TYP_CONC .EQ. 'C') THEN
         CALL C_C    (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'C_SOL') THEN
         CALL C_SOL  (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'PONCTUEL') THEN
         CALL C_PONCTUEL(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'X') THEN
         CALL C_X    (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'XDECOUVERT') THEN
         CALL C_XDEC (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'Y') THEN
         CALL C_Y    (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'XY') THEN
         CALL C_XY   (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'X2') THEN
         CALL C_X2   (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'Y2') THEN
         CALL C_Y2   (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'X2Y2') THEN
         CALL C_X2Y2 (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'EXY') THEN
         CALL C_EXY  (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'GAUSS') THEN
         CALL C_GAUSS(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'CNC_FRONT_TRAN') THEN
         CALL CNC_FRNT_TRN(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'CNC_GAUSS_TRAN') THEN
         CALL CNC_GAUSS_TRN(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'AGE_FRONT') THEN
         CALL AGE_FRNT(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'CLF_EXY') THEN
         CALL CLF_EXY(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'CLF_EXYK') THEN
         CALL C_EXY_K (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'CLF_FRONT') THEN
         CALL CLF_FRNT(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'CLF_FRONT_TRAN') THEN
         CALL CLF_FRNT_TRN(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'DBO_EXY') THEN
         CALL DBO_EXY(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'DBO_FRONT') THEN
         CALL DBO_FRNT(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'MES_EXY') THEN
         CALL MES_EXY(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'MES_CHS_FRONT') THEN
         CALL MCO_FRNT(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'MES_NCH_FRONT') THEN
         CALL MNC_FRNT(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'MET_EXY') THEN
         CALL MET_EXY(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'MET_FRONT') THEN
         CALL MET_FRNT(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'TMP_C') THEN
         CALL TMP_C  (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'TMP_XY') THEN
         CALL TMP_XY (PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'TMP_EXY') THEN
         CALL TMP_EXY(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'TMP_FRONT') THEN
         CALL TMP_FRNT(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'TOX_EXY') THEN
         CALL TOX_EXY(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSEIF (TYP_CONC .EQ. 'TST_EXY') THEN
         CALL TST_EXY(PARAM_CMD(1:LENSTR(PARAM_CMD)),
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:TYPECONC=', TYP_CONC
         STOP
      ENDIF

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_C (PARAM_CMD,
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         VDLG (1, IN) = 1.0D0
         VSOLR(1, IN) = 0.0D0
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE C_SOL (PARAM_CMD,
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         VDLG (1, IN) = 1.0D0
         VSOLR(1, IN) = 0.0D0
      ENDDO
      VSOLR(1,13) = 1.5D0

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE C_PONCTUEL (PARAM_CMD,
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         VDLG (1, IN) = 1.0D0
         VSOLR(1, IN) = 0.0D0
      ENDDO
      VDLG(1, 600) = 1.0D0
      VSOLR(1,600) = 1.0D-4
      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_X (PARAM_CMD,
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         VDLG (1, IN) = VCORG(1, IN)
         VSOLR(1, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_XDEC(PARAM_CMD,
     &                  NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRH, VPRH,
     &                  NPRD, VPRD,
     &                  NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      IN=0
      DO IX=1, NNX
         DO IY=1, NNY
            IN = IN + 1
            IF (IY .LE. 2) THEN
               VDLG (1, IN) = 0.0D0			!C=0 si y<=2ème noeud
            ELSE
               VDLG (1, IN) = VCORG(1, IN)	!C=x si y >2ème noeud
            ENDIF
            VSOLR(1, IN) = 0.0D0
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_Y (PARAM_CMD,
     &                NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRH, VPRH,
     &                NPRD, VPRD,
     &                NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1,NNT
         VDLG (1, IN) = VCORG(2, IN) !C=y
         VSOLR(1, IN) = 0.0D0        !Sollicitation
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_XY (PARAM_CMD,
     &                 NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRH, VPRH,
     &                 NPRD, VPRD,
     &                 NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG (1, IN) =  X+Y
         VSOLR(1, IN) = -1.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_X2 (PARAM_CMD,
     &                 NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRH, VPRH,
     &                 NPRD, VPRD,
     &                 NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG (1, IN) =  X*X
         VSOLR(1, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_Y2 (PARAM_CMD,
     &                 NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRH, VPRH,
     &                 NPRD, VPRD,
     &                 NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG (1, IN) = Y*Y
         VSOLR(1, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_X2Y2(PARAM_CMD,
     &                  NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRH, VPRH,
     &                  NPRD, VPRD,
     &                  NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG (1, IN) =  X*X + Y*Y
         VSOLR(1, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_EXY(PARAM_CMD,
     &                 NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRH, VPRH,
     &                 NPRD, VPRD,
     &                 NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG (1, IN) = EXP(-X-Y)   ! C = exp(-x-y)
         VSOLR(1, IN) = VPRH(1, IN) + VPRH(2, IN) + 2.0D0 ! C = u + v + 2
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE C_EXY_K(PARAM_CMD,
     &                 NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRH, VPRH,
     &                 NPRD, VPRD,
     &                 NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0
      K = 1.0D0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG (1, IN) = EXP(-X-Y)   ! C = exp(-x-y)
         VSOLR(1, IN) = VPRH(1, IN) + VPRH(2, IN) + 2.0D0+
     &                  K/VDLG(1,IN)  ! C = u + v + 2+K/C
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE C_GAUSS(PARAM_CMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)

      PARAMETER (ZERO  = 0.0D0)
      PARAMETER (UN_2  = 0.5D0)
      PARAMETER (UN    = 1.0D0)
      PARAMETER (DEUX  = 2.0D0)
      PARAMETER (QUATRE= 4.0D0)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

      PI = QUATRE * ATAN(UN)

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         D = SQRT(X*X + Y*Y)
         IF (D .LT. UN_2) THEN
            VDLG(1, IN) = COS(DEUX*PI*D + UN_2*PI)**2
         ELSE
            VDLG(1, IN) = ZERO
         ENDIF
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CNC_FRNT_TRN(PARAM_CMD,
     &                        NDIM, NNX, NNY, NNT, VCORG,
     &                        NNEL, NELT, KNG,
     &                        NPRH, VPRH,
     &                        NPRD, VPRD,
     &                        NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1)
      DIMENSION VSOLR(1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 1

      VKXX = 1.0D-4
      VKD  = 0.0D0
      T    = 100.0D0

      CEXP = SQRT(VKD/VKXX)
      CERF1= 1.0D0 / SQRT(4.0D0*VKXX*T)
      CERF2= SQRT(VKD*T)

C-------  SOLUTION
      C0 = 10.0D0
      IN = 0
      DO IX=1, NNX
         X = VCORG(1,IN+1)
         C = EXP(-X*CEXP) * ERFC(X*CERF1 - CERF2)
     &     + EXP( X*CEXP) * ERFC(X*CERF1 + CERF2)
         C = 0.5D0*C0*C
         DO IY=1, NNY
            IN = IN + 1
            VDLG (IN) = C
            VSOLR(IN) = 0.0D0
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CNC_GAUSS_TRN(PARAM_CMD,
     &                         NDIM, NNX, NNY, NNT, VCORG,
     &                         NNEL, NELT, KNG,
     &                         NPRH, VPRH,
     &                         NPRD, VPRD,
     &                         NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1)
      DIMENSION VSOLR(1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 1

      PI      = 4.0D0 * ATAN(1.0D0)
      SIXIEME = 1.0D0 / 6.0D0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN) - SIXIEME		! x-5/30
         Y = VCORG(2, IN) + SIXIEME		! y-5/30
         D = X*X + Y*Y
         IF (D .LE. 0.04D0) THEN
            C = 0.25D0*(1.0D0+COS(5.0D0*PI*X))*(1.0D0+COS(5.0D0*PI*Y))	! 1/4(1+cos(Pi*(x-5/30)/0.2)(1+cos(Pi*(y-5/30)/0.2)   un gauss situé dans le 4e cadran
         ELSE
            C = 0.0D0
         ENDIF
         VDLG (IN) = C
         VSOLR(IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE AGE_FRNT(PARAM_CMD,
     &                    NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRH, VPRH,
     &                    NPRD, VPRD,
     &                    NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         VDLG (1, IN) = X  + 1.0D0	! C = X + 1
         VSOLR(1, IN) = 0.0D0		! SolR = 0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CLF_EXY(PARAM_CMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      CALL C_EXY  (PARAM_CMD,
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRH, VPRH,
     &             NPRD, VPRD,
     &             NDLN, VDLG, VSOLR, ITRNST)

      NDLN = 1
      NPRD = 0
      ITRNST = 0

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CLF_FRNT(PARAM_CMD,
     &                    NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRH, VPRH,
     &                    NPRD, VPRD,
     &                    NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      VKD = LOG(2.0D0)
      DO IN=1, NNT
         X = VCORG(1, IN)
         VDLG (1, IN) = EXP(-X*VKD) !C=exp(-ln2*x)
         VSOLR(1, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE DBO_EXY(PARAM_CMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (2,    1)
      DIMENSION VDLG (2, 1)
      DIMENSION VSOLR(2, 1)
C-----------------------------------------------------------------------------

      NDLN = 2
      NPRD = 2
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG (1, IN) = EXP(-X-Y)
         VDLG (2, IN) = EXP(-X-Y)
         VSOLR(1, IN) = VPRH(1, IN) + VPRH(2, IN) + 2.0D0
         VSOLR(2, IN) = VPRH(1, IN) + VPRH(2, IN) + 2.0D0
      ENDDO

C-------  PROPRIETES NODALES
      VSED = 0.0D0
      DBEN = 0.0D0
      DO IN=1, NNT
         VPRD(1, IN) = VSED
         VPRD(2, IN) = DBEN
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE DBO_FRNT(PARAM_CMD,
     &                    NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRH, VPRH,
     &                    NPRD, VPRD,
     &                    NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (2,    1)
      DIMENSION VDLG (2, 1)
      DIMENSION VSOLR(2, 1)
C-----------------------------------------------------------------------------

      NDLN = 2
      NPRD = 2
      ITRNST = 0

C-------  SOLUTION
      VKD = 0.010D0
      VLS = 0.012D0
      COD0 =  12.0D0
      CDBO0= 100.0D0
      DO IN=1, NNT
         X = VCORG(1, IN)
         U = VPRH (1, IN)
         H = VPRH (3, IN)
         CDBO = CDBO0*EXP(-VKD*X/U)
         VDLG (1, IN) = COD0 + CDBO - VLS*X/(U*H)
         VDLG (2, IN) = CDBO
         VSOLR(1, IN) = 0.0D0
         VSOLR(2, IN) = 0.0D0
      ENDDO

C-------  PROPRIETES NODALES
      VSED = 0.000D0
      DBEN = 0.012D0
      DO IN=1, NNT
         VPRD(1, IN) = VSED
         VPRD(2, IN) = DBEN
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE MES_EXY(PARAM_CMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1,    1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

C-------  SOLUTION
      CALL C_EXY  (PARAM_CMD,
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRH, VPRH,
     &             NPRD, VPRD,
     &             NDLN, VDLG, VSOLR, ITRNST)

      NDLN = 1
      NPRD = 1
      ITRNST = 0

C-------  PROPRIETES NODALES
      VCIS = 0.0D0
      DO IN=1, NNT
         VPRD(1, IN) = VCIS
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE MCO_FRNT(PARAM_CMD,
     &                    NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRH, VPRH,
     &                    NPRD, VPRD,
     &                    NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (3, 1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)

      PARAMETER (QUATRETIERS = 4.0D0/3.0D0)
      PARAMETER (TROISQUARTS = 3.0D0/4.0D0)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 1
      ITRNST = 0

      UETOIL= 0.0D0
      VKS   = 0.0002D0
      C0    = 1.0D0

C-------  SOLUTION
      C0_43 = C0 ** (-QUATRETIERS)
      DO IN=1, NNT
         X = VCORG(1, IN)
         VDLG (1, IN) = (C0_43 + QUATRETIERS*VKS*X)**(-TROISQUARTS)
         VSOLR(1, IN) = 0.0D0
      ENDDO

C-------  PROPRIETES NODALES
      DO IN=1, NNT
         VPRD(1, IN) = UETOIL
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE MNC_FRNT(PARAM_CMD,
     &                    NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRH, VPRH,
     &                    NPRD, VPRD,
     &                    NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (3, 1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)

      EXTERNAL MNC_FRNT_DCDX
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 1
      ITRNST = 0

      UETOIL= 0.0D0

C-------  SOLUTION
      DELX = VCORG(1,NNY+1) - VCORG(1,1)
      C0 = 1.0D0
      IN = 0
      DO IX=1, NNX
         DO IY=1, NNY
            IN = IN + 1
            VDLG (1, IN) = C0
            VSOLR(1, IN) = 0.0D0
         ENDDO
         X0 = VCORG(1,IN)
         CALL RK4(X0, C0, X0+DELX, 1.0D-5, C1, MNC_FRNT_DCDX)
         C0 = C1
      ENDDO

C-------  PROPRIETES NODALES
      DO IN=1, NNT
         VPRD(1, IN) = UETOIL
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE MNC_FRNT_DCDX(X, C, DCDX)
      INCLUDE 'gentest.fi'

      PARAMETER (G    = 9.0D0)
      PARAMETER (D    = 1111.11111111111D-6)
      PARAMETER (RHOW = 1000.0D0)
      PARAMETER (RHOS = 2500.0D0)
C-----------------------------------------------------------------------------

      WS = 1.1D0*SQRT((RHOS/RHOW-1.0D0)*G*D)
      V  = 1.0D0 - C/RHOS
      DCDX = -WS * C * (V*V*V*V)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE MET_EXY(PARAM_CMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (3,    1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

C-------  SOLUTION
      CALL C_EXY  (PARAM_CMD,
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRH, VPRH,
     &             NPRD, VPRD,
     &             NDLN, VDLG, VSOLR, ITRNST)

      NDLN = 1
      NPRD = 3
      ITRNST = 0

C-------  PROPRIETES NODALES
      CMES = 0.0D0
      VSED = 0.0D0
      PSED = 0.0D0
      DO IN=1, NNT
         VPRD(1, IN) = CMES
         VPRD(2, IN) = VSED
         VPRD(3, IN) = PSED
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE MET_FRNT(PARAM_CMD,
     &                    NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRH, VPRH,
     &                    NPRD, VPRD,
     &                    NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (3, 1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 3
      ITRNST = 0

      CS0 = 10.0D0
      CM0 = 1.0D0
      WS  = 0.01D0
      VKD = 0.0D0
      VKP = 100.0D0
      P   = 1.0D0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         U = VPRH (1, IN)
         CS = CS0*EXP(-WS*X/U)
         VTMP = 1.0D0 / (VKP*CS0)   !! CS ou CS0
         VDLG (1, IN) = (CM0/(1.0D0+VTMP))*(EXP(-WS*X/U)+VTMP)
         VSOLR(1, IN) = 0.0D0
      ENDDO

C-------  PROPRIETES NODALES
      DO IN=1, NNT
         X = VCORG(1, IN)
         U = VPRH (1, IN)
         VPRD(1, IN) = CS0*EXP(-WS*X/U)
         VPRD(2, IN) = WS
         VPRD(3, IN) = P
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE TMP_EXY(PARAM_CMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (11,   1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

C-------  SOLUTION
      CALL C_EXY  (PARAM_CMD,
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRH, VPRH,
     &             NPRD, VPRD,
     &             NDLN, VDLG, VSOLR, ITRNST)

      NDLN = 1
      NPRD = 11
      ITRNST = 0

C-------  PROPRIETES NODALES
      DO IN=1, NNT
         DO ID=1, NPRD
            !IF (ID .NE. 7) THEN
                VPRD(ID,IN) = 0.0D0
            !ELSEIF (ID .EQ. 7) THEN
               !VPRD(ID,IN) = 1.0D-50    ! Humidite relative>0
            !ENDIF
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE TMP_C(PARAM_CMD,
     &                 NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRH, VPRH,
     &                 NPRD, VPRD,
     &                 NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (11,   1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

C-------  SOLUTION
      CALL C_C(PARAM_CMD,
     &         NDIM, NNX, NNY, NNT, VCORG,
     &         NNEL, NELT, KNG,
     &         NPRH, VPRH,
     &         NPRD, VPRD,
     &         NDLN, VDLG, VSOLR, ITRNST)

      NDLN = 1
      NPRD = 11
      ITRNST = 0

C-------  PROPRIETES NODALES
      READ(PARAM_CMD, *) FLX_RAD_SOL_INC,
     &                   FRC_COUV_VEG,
     &                   COF_TURB,
     &                   TMP_AIR,
     &                   EMM_ATM,
     &                   FCT_VENT,
     &                   HUM_REL,
     &                   PRC_EAU_NEIGE,
     &                   CND_GLACE,
     &                   FCT_EXT_ATM,
     &                   CND_FOND

      DO IN=1, NNT
         VPRD( 1, IN) = FLX_RAD_SOL_INC
         VPRD( 2, IN) = FRC_COUV_VEG
         VPRD( 3, IN) = COF_TURB
         VPRD( 4, IN) = TMP_AIR
         VPRD( 5, IN) = EMM_ATM
         VPRD( 6, IN) = FCT_VENT
         VPRD( 7, IN) = HUM_REL
         VPRD( 8, IN) = PRC_EAU_NEIGE
         VPRD( 9, IN) = CND_GLACE
         VPRD(10, IN) = FCT_EXT_ATM
         VPRD(11, IN) = CND_FOND
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE TMP_XY(PARAM_CMD,
     &                  NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRH, VPRH,
     &                  NPRD, VPRD,
     &                  NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (11,   1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 11
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG (1, IN) = X+Y
         VSOLR(1, IN) = 0.0D0
      ENDDO

C-------  PROPRIETES NODALES
      READ(PARAM_CMD, *) FLX_RAD_SOL_INC,
     &                   FRC_COUV_VEG,
     &                   COF_TURB,
     &                   TMP_AIR,
     &                   EMM_ATM,
     &                   FCT_VENT,
     &                   HUM_REL,
     &                   PRC_EAU_NEIGE,
     &                   CND_GLACE,
     &                   FCT_EXT_ATM,
     &                   CND_FOND

      DO IN=1, NNT
         VPRD( 1, IN) = FLX_RAD_SOL_INC
         VPRD( 2, IN) = FRC_COUV_VEG
         VPRD( 3, IN) = COF_TURB
         VPRD( 4, IN) = TMP_AIR
         VPRD( 5, IN) = EMM_ATM
         VPRD( 6, IN) = FCT_VENT
         VPRD( 7, IN) = HUM_REL
         VPRD( 8, IN) = PRC_EAU_NEIGE
         VPRD( 9, IN) = CND_GLACE
         VPRD(10, IN) = FCT_EXT_ATM
         VPRD(11, IN) = CND_FOND
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE TMP_FRNT_BASE(PARAM_CMD,
     &                         NDIM, NNX, NNY, NNT, VCORG,
     &                         NNEL, NELT, KNG,
     &                         NPRH, VPRH,
     &                         NPRD, VPRD,
     &                         NDLN, VDLG, VSOLR, ITRNST,
     &                         T0, A, B)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (NPRD, 1)
      DIMENSION VDLG (NDLN, 1)
      DIMENSION VSOLR(NDLN, 1)
C-----------------------------------------------------------------------------

C-------  SOLUTION
      AB = A / B
      DO IN=1, NNT
         X = VCORG(1, IN)
         VDLG (1, IN) = (T0+AB)*EXP(A*X) - AB
         VSOLR(1, IN) = 0.0D0
      ENDDO

C-------  PROPRIETES NODALES
      DO IN=1, NNT
         DO ID=1,NPRD
            VPRD(ID, IN) = 0.0D0
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE TMP_FRNT(PARAM_CMD,
     &                    NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRH, VPRH,
     &                    NPRD, VPRD,
     &                    NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (11, 1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 11
      ITRNST = 0

C-------  PROPRIETES NODALES
      READ(PARAM_CMD, *) FLX_RAD_SOL_INC,
     &                   FRC_COUV_VEG,
     &                   COF_TURB,
     &                   TMP_AIR,
     &                   EMM_ATM,
     &                   FCT_VENT,
     &                   HUM_REL,
     &                   PRC_EAU_NEIGE,
     &                   CND_GLACE,
     &                   FCT_EXT_ATM,
     &                   CND_FOND,
     &                   T0, A, B

C-------  TEMPERATURE
      CALL TMP_FRNT_BASE(PARAM_CMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST,
     &                   T0, A, B)

C-------  PROPRIETES NODALES
      DO IN=1, NNT
         VPRD( 1, IN) = FLX_RAD_SOL_INC
         VPRD( 2, IN) = FRC_COUV_VEG
         VPRD( 3, IN) = COF_TURB
         VPRD( 4, IN) = TMP_AIR
         VPRD( 5, IN) = EMM_ATM
         VPRD( 6, IN) = FCT_VENT
         VPRD( 7, IN) = HUM_REL
         VPRD( 8, IN) = PRC_EAU_NEIGE
         VPRD( 9, IN) = CND_GLACE
         VPRD(10, IN) = FCT_EXT_ATM
         VPRD(11, IN) = CND_FOND
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE TOX_EXY(PARAM_CMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (3,    1)
      DIMENSION VDLG (1, 1)
      DIMENSION VSOLR(1, 1)
C-----------------------------------------------------------------------------

C-------  SOLUTION
      CALL MET_EXY(PARAM_CMD,
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRH, VPRH,
     &             NPRD, VPRD,
     &             NDLN, VDLG, VSOLR, ITRNST)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE TST_EXY(PARAM_CMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRH, VPRH,
     &                   NPRD, VPRD,
     &                   NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (5, 1)
      DIMENSION VSOLR(5, 1)
C-----------------------------------------------------------------------------

      NDLN = 5
      NPRD = 0
      ITRNST = 0

C-------  SOLUTION
      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG (1, IN) = EXP(-X-Y)
         VDLG (2, IN) = EXP(-X-Y)
         VDLG (3, IN) = EXP(-X-Y)
         VDLG (4, IN) = EXP(-X-Y)
         VDLG (5, IN) = EXP(-X-Y)
         VSOLR(1, IN) = VPRH(1, IN) + VPRH(2, IN) + 2.0D0
         VSOLR(2, IN) = VPRH(1, IN) + VPRH(2, IN) + 2.0D0
         VSOLR(3, IN) = VPRH(1, IN) + VPRH(2, IN) + 2.0D0
         VSOLR(4, IN) = VPRH(1, IN) + VPRH(2, IN) + 2.0D0
         VSOLR(5, IN) = VPRH(1, IN) + VPRH(2, IN) + 2.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CLF_FRNT_TRN(PARAM_CMD,
     &                        NDIM, NNX, NNY, NNT, VCORG,
     &                        NNEL, NELT, KNG,
     &                        NPRH, VPRH,
     &                        NPRD, VPRD,
     &                        NDLN, VDLG, VSOLR, ITRNST)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) PARAM_CMD
      DIMENSION VCORG(NDIM, 1)
      DIMENSION KNG  (NNEL, 1)
      DIMENSION VPRH (NPRH, 1)
      DIMENSION VPRD (1)
      DIMENSION VDLG (1)
      DIMENSION VSOLR(1)
C-----------------------------------------------------------------------------

      NDLN = 1
      NPRD = 0
      ITRNST = 1

      VKXX = 1.0D-4
      VKD  = 1.0D-4
      T    = 100.0D0

      CEXP = SQRT(VKD/VKXX)
      CERF1= 1.0D0 / SQRT(4.0D0*VKXX*T)
      CERF2= SQRT(VKD*T)

C-------  SOLUTION
      C0 = 10.0D0
      IN = 0
      DO IX=1, NNX
         X = VCORG(1,IN+1)
         C = EXP(-X*CEXP) * ERFC(X*CERF1 - CERF2)
     &     + EXP( X*CEXP) * ERFC(X*CERF1 + CERF2)
         C = 0.5D0*C0*C
         DO IY=1, NNY
            IN = IN + 1
            VDLG (IN) = C
            VSOLR(IN) = 0.0D0
         ENDDO
      ENDDO

      RETURN
      END