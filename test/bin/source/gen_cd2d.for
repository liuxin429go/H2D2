C=============================================================================
C
C=============================================================================
      PROGRAM GEN_CD2D

      INCLUDE 'gentest.fi'

      PARAMETER (NDIM =2)
      PARAMETER (NPRNH=5)
C      PARAMETER (NPRND=1)  déterminé par la cynétique
      PARAMETER (NPRMAX=20)
      PARAMETER (NDLMAX=5)
C      PARAMETER (NDIMV=150*150)

C      DIMENSION     VCORG(NDIM*NDIMV)
C      DIMENSION     VPRNH(NPRNH*NDIMV)
C      DIMENSION     VPRND(NPRMAX*NDIMV)
C      DIMENSION     VSOLR(NDLMAX*NDIMV)
C      DIMENSION     VDLG (NDLMAX*NDIMV)
C      DIMENSION     KDIMP(NDLMAX*NDIMV)
C      DIMENSION     KNG  (6*NDIMV)
      !! On alloue plutôt dynamiquement
      ALLOCATABLE :: VCORG(:)
      ALLOCATABLE :: VPRNH(:)
      ALLOCATABLE :: VPRND(:)
      ALLOCATABLE :: VSOLR(:)
      ALLOCATABLE :: VDLG(:)
      ALLOCATABLE :: KDIMP(:)
      ALLOCATABLE :: KNG(:)
      ALLOCATABLE :: VPRNT(:)

      CHARACTER*256 NOMBASE
      CHARACTER*256 ECOUL
      CHARACTER*256 DISP
      CHARACTER*256 CONDLIM
      CHARACTER*4   TYPEL
      LOGICAL       LNODRND

      !! Les variables sont placées par défaut sur le stack
      !! De trop grosses tables font sauter le stack
      !! On crée donc un common bidon pour contourner le pblm.
C      COMMON /DUMMY/ VCORG, VPRNH, VPRND, VSOLR, VDLG, KDIMP, KNG
C-----------------------------------------------------------------------------

C-------  LIS LES DONNEES
      READ(5,*) NOMBASE
      READ(5,*) NELX
      READ(5,*) NELY
      READ(5,*) XORI
      READ(5,*) YORI
      READ(5,*) DIMX
      READ(5,*) DIMY
      READ(5,*) NNEL
      READ(5,*) TYPEL
      READ(5,*) ECOUL
      READ(5,*) DISP
      READ(5,*) CONDLIM


C-------  PRE-TRAITE
      LNODRND = .FALSE.
      CALL TRIMSTR(TYPEL)
      CALL UCASESTR(TYPEL)
      IF (TYPEL(1:1) .EQ. '~') THEN
         LNODRND = .TRUE.
         TYPEL(1:1) = ' '
         CALL TRIMSTR(TYPEL)
      ENDIF
      IF (TYPEL(1:1) .EQ. '/') THEN
         ITYPEL = IEL$SLASH
      ELSEIF (TYPEL(1:1) .EQ. '\') THEN
         ITYPEL = IEL$SLINV
      ELSEIF (TYPEL(1:1) .EQ. 'v') THEN
         ITYPEL = IEL$V
      ELSEIF (TYPEL(1:1) .EQ. 'V') THEN
         ITYPEL = IEL$V
      ELSEIF (TYPEL(1:1) .EQ. '^') THEN
         ITYPEL = IEL$VINV
      ELSEIF (TYPEL(1:1) .EQ. '>') THEN
         ITYPEL = IEL$GT
      ELSEIF (TYPEL(1:1) .EQ. '<') THEN
         ITYPEL = IEL$LT
      ELSEIF (TYPEL(1:1) .EQ. 'X') THEN
         ITYPEL = IEL$X
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:TYPEL=', TYPEL
         STOP
      ENDIF
      IF (NNEL .EQ. 3) THEN
         NNX = NELX + 1
         NNY = NELY + 1
      ELSEIF(NNEL .EQ. 6) THEN
         NNX = 2*NELX + 1
         NNY = 2*NELY + 1
      ELSE
         WRITE(*,*) 'NOMBRE DE NOEUDS PAR ELEMENT INVALIDE:', NNEL
         STOP
      ENDIF
      NNT = NNX*NNY
      CALL TRIMSTR(NOMBASE)
      CALL LCASESTR(NOMBASE)
      CALL TRIMSTR(ECOUL)
      CALL UCASESTR(ECOUL)
      CALL TRIMSTR(DISP)
      CALL UCASESTR(DISP)

C-------  ALLOUE LES TABLES
      ALLOCATE(VCORG(NDIM*NNT))
      ALLOCATE(VPRNH(NPRNH*NNT))
      ALLOCATE(VPRND(NPRMAX*NNT))
      ALLOCATE(VSOLR(NDLMAX*NNT))
      ALLOCATE(VDLG(NDLMAX*NNT))
      ALLOCATE(KDIMP(NDLMAX*NNT))
      ALLOCATE(KNG(6*NNT))
      ALLOCATE(VPRNT(NPRNH*NNT))

C-------  GENERE LE MAILLAGE
      CALL GENCOOR(LNODRND, XORI, YORI, DIMX, DIMY, NNX, NNY, VCORG)
      CALL GENELEM(ITYPEL, NNX, NNY, VCORG, NNEL, NELT, KNG)

C-------  GENERE LA SOLUTION HYDRODYNAMIQUE
      NDLN = 1
      CALL GENHYDRO(ECOUL(1:LENSTR(ECOUL)),
     &              NDIM, NNX, NNY, NNT, VCORG,
     &              NNEL, NELT, KNG,
     &              NPRNH, VPRNH,
     &              NDLN, VSOLR,VDLG)

C-------  GENERE LA SOLUTION CONCENTRATION
      CALL GENCONC(DISP(1:LENSTR(DISP)),
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRNH, VPRNH,
     &             NPRND, VPRND,
     &             NDLN, VDLG, VSOLR, ITRNST)

C-------  GENERE LES CONDITIONS LIMITES
      CALL GENCL_CD2D  (CONDLIM(1:LENSTR(CONDLIM)),
     &					NNX, NNY, NNT,
     &					NDLN, KDIMP)

C-------  ECRIS LES RESULTATS
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.cor',
     &              NNT,
     &              NDIM,
     &              VCORG)
      CALL ECRISELE(NOMBASE(1:LENSTR(NOMBASE))//'.ele',
     &              NELT,
     &              NNEL,
     &              KNG)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.pnh',
     &              NNT,
     &              NPRNH,
     &              VPRNH)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.pnd',
     &              NNT,
     &              NPRND,
     &              VPRND)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.slr',
     &              NNT,
     &              NDLN,
     &              VSOLR)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.ini',
     &              NNT,
     &              NDLN,
     &              VDLG)
      CALL ECRISCND(NOMBASE(1:LENSTR(NOMBASE))//'.cnd',
     &              NNT,
     &              NDLN,
     &              VDLG, KDIMP)
      CALL ECRISBND(NOMBASE(1:LENSTR(NOMBASE))//'.bnd',
     &              NNT,
     &              NDLN,
     &              VDLG, KDIMP)

C-------  DESALLOUE LES TABLES
      DEALLOCATE(KNG)
      DEALLOCATE(KDIMP)
      DEALLOCATE(VDLG)
      DEALLOCATE(VSOLR)
      DEALLOCATE(VPRND)
      DEALLOCATE(VPRNH)
      DEALLOCATE(VCORG)
      DEALLOCATE(VPRNT)
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISCND(NOMFIC, NNT, NDLN, VDLG, KDIMP)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)

      CHARACTER*256 NOMCL, TMPCL
      CHARACTER*32  FMT
      LOGICAL       WRITE_DONE
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I3,', NDLN, '(1X,1PE25.17E3),A)'

C---     Compte le nombre de conditions
      NCND = 0
100   CONTINUE
C---        Cherche la prochaine condition non traitée
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               WRITE(TMPCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
               CALL TRIMSTR(TMPCL)
               CALL SBSTSTR(TMPCL, ' ','_')
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199
         write(6,*) '==>', TMPCL(1:75)
C---        Flag tous les noeuds de cette condition
         NCND = NCND + 1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
                WRITE(NOMCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
                CALL TRIMSTR(NOMCL)
                CALL SBSTSTR(NOMCL, ' ','_')
                IF (NOMCL .EQ. TMPCL) KDIMP(1,IN) = -KDIMP(1,IN)
            ENDIF
         ENDDO
      GOTO 100
199   CONTINUE

C---     Reset les codes
      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

C---     Écris l'entête
      WRITE(10, '(I6,1X,1PE25.17E3)') NCND, 0.0

C---     Écris les conditions
200   CONTINUE
C---        Cherche la prochaine condition non traitée
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               WRITE(TMPCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
               CALL TRIMSTR(TMPCL)
               CALL SBSTSTR(TMPCL, ' ','_')
               GOTO 209
            ENDIF
         ENDDO
209      CONTINUE
         IF (IC .LT. 0) GOTO 299

C---        Flag tous les noeuds de cette condition
C---        Ecris la condition
         WRITE_DONE = .FALSE.
         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
                WRITE(NOMCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
                CALL TRIMSTR(NOMCL)
                CALL SBSTSTR(NOMCL, ' ','_')

                IF (NOMCL .EQ. TMPCL) THEN
                    KDIMP(1,IN) = -KDIMP(1,IN)
                    IF (.NOT. WRITE_DONE) THEN
                        WRITE(10, '(A,I3,6(1X,1PE25.17E3))')
     &                                  NOMCL(1:LENSTR(NOMCL)),
     &                                  MOD(IC,1000),
     &                                  (VDLG(ID,IN),ID=1,NDLN)
                        WRITE_DONE = .TRUE.
                    ENDIF
                ENDIF
            ENDIF
         ENDDO

      GOTO 200
299   CONTINUE

      CLOSE(10)

C---     Reset les codes
      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISBND(NOMFIC, NNT, NDLN, VDLG, KDIMP)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)

      CHARACTER*256 NOMCL
      CHARACTER*32  FMT
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I3,', NDLN, '(1X,1PE25.17E3),A)'

      NCL = 0
      DO IN=1,NNT
         IF (KDIMP(1, IN) .GT. 0) NCL = NCL + 1
      ENDDO
      WRITE(10, '(I9)') NCL

100   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199

         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               KDIMP(1,IN) = -KDIMP(1,IN)

               WRITE(NOMCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
               CALL TRIMSTR(NOMCL)
               CALL SBSTSTR(NOMCL, ' ','_')

               WRITE(10, '(A,I9)') NOMCL(1:LENSTR(NOMCL)), IN
            ENDIF
         ENDDO

      GOTO 100
199   CONTINUE

      CLOSE(10)

      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISELE(NOMFIC, NELT, NNEL, KNG)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION KNG(NNEL, 1)
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      IF (NNEL .EQ. 3) THEN
         WRITE(10, '(3I9,1X,1PE25.17E3)') NELT, NNEL, 201, 0.0
      ELSE
         WRITE(10, '(3I9,1X,1PE25.17E3)') NELT, NNEL, 203, 0.0
      ENDIF
      DO IE=1,NELT
         WRITE(10, '(6I9)') (KNG(I, IE), I=1,NNEL)
      ENDDO
      CLOSE(10)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISPRN(NOMFIC, NNT, NPRN, VPRN)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VPRN(NPRN, 1)
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(10, '(2I9,1X,1PE25.17E3)') NNT, NPRN, 0.0
      DO IN=1,NNT
         WRITE(10, '(25(1X,1PE25.17E3))') (VPRN(I, IN), I=1,NPRN)
      ENDDO
      CLOSE(10)

      RETURN
      END