C=============================================================================
C     SORTIE VPRN, VSOLR
C=============================================================================
      SUBROUTINE GENHYDRO(ECOULEMENT,
     &                    NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR, VPRNT)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) ECOULEMENT
      DIMENSION     VCORG(*)
      DIMENSION     KNG  (*)
      DIMENSION     VPRN (*)
      DIMENSION     VSOLR(NDLN,1)
	  DIMENSION     VPRNT(NDLN,NPRN)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            VSOLR(ID,IN) = 0.0D0
         ENDDO
      ENDDO

      IF (ECOULEMENT .EQ. 'PISCINE') THEN
         CALL PISCINE (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'PISCINEV') THEN
         CALL PISCINEV(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CANALX') THEN
         CALL CANALX  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CANALX_NEG') THEN
         CALL CANALX_NEG  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CANALY') THEN
         CALL CANALY  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CANALXY') THEN
         CALL CANALXY (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CANAL-XY') THEN
         CALL CANALMXY(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'COUETTEX') THEN
         CALL COUETTEX(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'COUETTEY') THEN
         CALL COUETTEY(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CONVX0') THEN
         CALL CONVX0  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CONV0Y') THEN
         CALL CONV0Y  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CONVYX') THEN
         CALL CONVYX  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CONVYX_MANNING') THEN
         CALL CONVYX_MANNING(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)

      ELSEIF (ECOULEMENT .EQ. 'ROTATION') THEN
         CALL ROTATION(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EXPO') THEN
         CALL EXPO    (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EX_0_1') THEN
         CALL F_EX_0_1 (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EY_0_1') THEN
         CALL F_EY_0_1 (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. '0_EX_1') THEN
         CALL F_0_EX_1 (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. '0_EY_1') THEN
         CALL F_0_EY_1 (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'COUVRANT_DECOUVRANT') THEN
         CALL COUDECOU(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'COUETTEXWX') THEN
         CALL COUETTEXWX(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR, VPRNT)
	  ELSEIF (ECOULEMENT .EQ. 'CANALXW') THEN
         CALL CANALXW(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR, VPRNT)
	  ELSEIF (ECOULEMENT .EQ. 'CANALXW2') THEN
         CALL CANALXW2(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR, VPRNT)
	  ELSEIF (ECOULEMENT .EQ. 'CANALXCO') THEN
         CALL CANALXCO(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EXPOCO') THEN
         CALL EXPOCO  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EXPOLM') THEN
         CALL EXPOLM  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EY_0_1_LM') THEN
         CALL F_EY_0_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. '0_EX_1_LM') THEN
         CALL F_0_EX_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EX_0_1_LM') THEN
         CALL F_EX_0_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. '0_EY_1_LM') THEN
         CALL F_0_EY_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EY_0_FX_LM') THEN
         CALL EY_0_FX_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'UY_0_1_LM') THEN
         CALL UY_0_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'CONVECYX') THEN
         CALL CONVECYX  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EXPOXY') THEN
         CALL EXPOXY    (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'EXPOXY_MANNING') THEN
         CALL EXPOXY_MANNING(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'UFAIBLE') THEN
         CALL C_UFAIBLE(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'PROPAG_CANALX') THEN
         CALL PROPAG_CANALX  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'QUAD_X') THEN
         CALL QUAD_X(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_0') THEN
         CALL Y_EXP_0(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_05_LAM') THEN
         CALL Y_EXP_05_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_05_TUR') THEN
         CALL Y_EXP_05_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_1') THEN
         CALL Y_EXP_1(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_15_LAM') THEN
         CALL Y_EXP_15_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_15_TUR') THEN
         CALL Y_EXP_15_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_2_LAM') THEN
         CALL Y_EXP_2_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_2_TUR') THEN
         CALL Y_EXP_2_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_3_LAM') THEN
         CALL Y_EXP_3_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_3_TUR') THEN
         CALL Y_EXP_3_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_15_1_LAM') THEN
         CALL Y_EXP_15_1_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSEIF (ECOULEMENT .EQ. 'Y_EXP_15_1_TUR') THEN
         CALL Y_EXP_15_1_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NDLN, VSOLR)
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:', ECOULEMENT
         STOP
      ENDIF

      RETURN
      END

C=============================================================================
C     SORTIE : VPRN, VSLOR
C=============================================================================
      SUBROUTINE PISCINE(NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRN, VPRN,
     &                   NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      U  = 0.0D0
      V  = 0.0D0
      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END

C=============================================================================
C     SORTIE VPRN, VSOLR==0 (mais déjà initialisé ainsi)
C=============================================================================
      SUBROUTINE PISCINEV(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      U  = 0.0D0
      V  = 0.0D0
      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0

      IN = 0
      DO IX=1,NNX
         DO IY=1,NNY
            IN = IN + 1
            VPRN(1,IN) = U
            VPRN(2,IN) = V
            IF (IY .LE. 3) THEN
               VPRN(3,IN) = -H
            ELSE
               VPRN(3,IN) =  H
            ENDIF
            VPRN(4,IN) = DV
            VPRN(5,IN) = DH
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CANALX(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      U  = 1.0D0
      V  = 0.0D0
      H  = 1.5D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE CANALX_NEG(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      U  = -1.0D0
      V  = 0.0D0
      H  = 1.5D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CANALY(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      U  = 0.0D0
      V  = 1.0D0
      H  = 1.5D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CANALXY(NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRN, VPRN,
     &                   NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      U  = 1.0D0
      V  = 1.0D0
      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CANALMXY(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      U  =-1.0D0
      V  =-1.0D0
      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE COUETTEX(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      V  = 0.0D0
      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = VCORG(2, IN) - VCORG(2, 1)
         VPRN(2,IN) = V !0
         VPRN(3,IN) = H !1
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE COUETTEY(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      U  = 0.0D0
      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = VCORG(1, IN) - VCORG(1, 1)
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CONVX0  (NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT

         VPRN(1,IN) = VCORG(1, IN) - VCORG(1, 1) ! u = x-x0... u linéaire
         VPRN(2,IN) = 0.0D0                      ! v = 0
         VPRN(3,IN) = H                          ! H = 1
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0
         VSOLR(2,IN) = 0.0D0
         VSOLR(3,IN) = 1.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CONV0Y  (NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = 0.0D0
         VPRN(2,IN) = VCORG(2, IN) - VCORG(1, 1)
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0
         VSOLR(2,IN) = 0.0D0
         VSOLR(3,IN) = 1.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE CONVYX  (NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = VCORG(2, IN) - VCORG(2, 1)	! u = y
         VPRN(2,IN) = VCORG(1, IN) - VCORG(1, 1)	! v = x
         VPRN(3,IN) = H								! H = 1
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE CONVYX_MANNING(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 2.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
        X = VCORG(1, IN) - VCORG(1, 1)
        Y = VCORG(2, IN) - VCORG(2, 1)
         VPRN(1,IN)  = Y                            ! u = y
         VPRN(2,IN)  = X                            ! v = x
         VPRN(3,IN)  = H                            ! H = 2
         VPRN(4,IN)  = DV
         VPRN(5,IN)  = DH

         VSOLR(1,IN) = 9.0D0*2**(2.0D0/3.0D0)*Y*SQRT(X*X+Y*Y)
     &                 /20000 -1.5D-3
         VSOLR(2,IN) = 9.0D0*2**(2.0D0/3.0D0)*X*SQRT(X*X+Y*Y)/20000
         VSOLR(3,IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ROTATION(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      IN = (NNX/2)*NNY + ((NNY/2)+1)
      XC = VCORG(1, IN)
      YC = VCORG(2, IN)
      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = -(VCORG(2,IN)-YC)		! u=-(y-ycentre)
         VPRN(2,IN) =  (VCORG(1,IN)-XC)		! v= x-xcentre
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE EXPO(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = EXP(X) !u
         VPRN(2,IN) = EXP(Y) !v
         VPRN(3,IN) = EXP(-X-Y) !H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -EXP(-X-Y)*EXP(-X-Y)        ! SOLR2
         VSOLR(2,IN) = -EXP(-X-Y)*EXP(-X-Y)        ! SOLR3
         VSOLR(3,IN) = 0.0D0                       ! SOLR1
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE F_EX_0_1  (NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = EXP(X)
         VPRN(2,IN) = 0.0d0
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -2.0D0*EXP(X)
         VSOLR(2,IN) = 0.0D0
         VSOLR(3,IN) = EXP(X)
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE F_EY_0_1  (NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = EXP(Y)
         VPRN(2,IN) = 0.0D0
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -EXP(Y)          ! VSOLRqx = -exp(y)
         VSOLR(2,IN) = 0.0D0            ! VSOLRqy
         VSOLR(3,IN) = 0.0D0            ! VSOLRh
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE F_0_EY_1  (NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = 0.0D0 !U
         VPRN(2,IN) = EXP(Y) !V
         VPRN(3,IN) = H !H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0                          ! SOLRqx
         VSOLR(2,IN) = -2.0D0*EXP(Y)                  ! SOLRqy
         VSOLR(3,IN) = EXP(Y)                         ! SOLRh
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE F_0_EX_1 (NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = 0.0D0
         VPRN(2,IN) = EXP(X)
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0
         VSOLR(2,IN) = -EXP(X)
         VSOLR(3,IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE COUDECOU(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------
      VN  = 0.02D0
      VIB = -3.19554D-3
      VM  = 0.125D0

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         IF (Y .LE. 0.0D0) THEN
            VPRN(1,IN) = 0.0D0
         ELSE
            VPRN(1,IN) = SQRT(ABS(VIB)) / VN * (VM*Y)**(2.0D0/3.0D0)
         ENDIF
         VPRN(2,IN) = 0.0D0
         VPRN(3,IN) = VM*Y
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0
         VSOLR(2,IN) = 0.0D0
         VSOLR(3,IN) = 0.0D0
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE COUETTEXWX(NDIM, NNX, NNY, NNT, VCORG,
     &						NNEL, NELT, KNG,
     &						NPRN, VPRN,
     &						NDLN, VSOLR, VPRNT)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
      DIMENSION VPRNT(NDLN,NPRN)
C-----------------------------------------------------------------------------

      V   = 0.0D0
      DV  = 1.0D0
      DH  = 1.0D0
      TAU = 1.25D-3*1.2475D0*VPRNT(5,1)**(1.80D0) !CW*RHOAIR*WX^1.8
      DO IN=1,NNT
         Y			= VCORG(2, IN) - VCORG(2, 1)
         VPRN(1,IN) = Y !u=y
         VPRN(2,IN) = V !v=0
         VPRN(3,IN) = 1-TAU*VPRN(1,IN)-VPRNT(1,IN) !H=(1-TAU*Y)-ZF
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0                       ! SOLRqx
         VSOLR(2,IN) = (-TAU/1000)                 ! SOLRqy               À CHANGER
         VSOLR(3,IN) = 0.0D0                       ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE CANALXW(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NDLN, VSOLR, VPRNT)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
      DIMENSION VPRNT(NDLN,NPRN)
C-----------------------------------------------------------------------------

      U  = 1.0D0
      V  = 0.0D0
      H  = 1.5D0
      DV = 1.0D0
      DH = 1.0D0
      WX = VPRNT(5,1)
      WY = VPRNT(6,1)

      WMOD2 = WX*WX+WY*WY
      UN_RAUE = 0.0010000000000000000000D0
      RAUA = 1.2475000000000000000000D0
      QUINZE2 = 125.000000000000000000000D0

      CW   = 0.0D0
      TAUX = 0.0D0
      TAUY = 0.0D0
      IF (WMOD2 .EQ. 0.0D0) THEN
         CW = 0.0000000000000000000D0
      ELSEIF (WMOD2 .LE. 1.0D0) THEN
         CW =1.25D-3*WMOD2**(0.40D0)
      ELSEIF (WMOD2 .LE. QUINZE2) THEN
         CW =0.50D-3*WMOD2**(0.75D0)
      ELSE
         CW = 2.60D-3*SQRT(WMOD2)
      ENDIF

      TAUX = CW*WX*RAUA
      TAUY = CW*WY*RAUA

      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -TAUX*UN_RAUE             ! SOLRqx = -TauX/rau
         VSOLR(2,IN) = -TAUY*UN_RAUE             ! SOLRqy = -TauY/rau
         VSOLR(3,IN) = 0.0D0                     ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE CANALXW2(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NDLN, VSOLR, VPRNT)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
      DIMENSION VPRNT(NDLN,NPRN)
C-----------------------------------------------------------------------------

      U  = 1.0D0
      V  = 0.0D0
      H  = 1.5D0
      DV = 1.0D0
      DH = 1.0D0
      WX = VPRNT(5,1)-U
      WY = VPRNT(6,1)-V

      WMOD2 = WX*WX+WY*WY
      UN_RAUE = 0.0010000000000000000000D0
      RAUA = 1.2475000000000000000000D0
      QUINZE2 = 125.000000000000000000000D0

      CW   = 0.0D0
      TAUX = 0.0D0
      TAUY = 0.0D0
      IF (WMOD2 .EQ. 0.0D0) THEN
         CW = 0.0000000000000000000D0
      ELSEIF (WMOD2 .LE. 1.0D0) THEN
         CW =1.25D-3*WMOD2**(0.40D0)
      ELSEIF (WMOD2 .LE. QUINZE2) THEN
         CW =0.50D-3*WMOD2**(0.75D0)
      ELSE
         CW = 2.60D-3*SQRT(WMOD2)
      ENDIF

      TAUX = CW*WX*RAUA
      TAUY = CW*WY*RAUA

      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -TAUX*UN_RAUE             ! SOLRqx = -TauX/rau
         VSOLR(2,IN) = -TAUY*UN_RAUE             ! SOLRqy = -TauY/rau
         VSOLR(3,IN) = 0.0D0                     ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE CANALXCO(NDIM, NNX, NNY, NNT, VCORG,
     &						NNEL, NELT, KNG,
     &						NPRN, VPRN,
     &						NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

	  U    = 1.0D0
	  H    = 1.5D0
	  W    = 0.7292D-4
	  SPHI = 0.5D0
      DV   = 1.0D0
      DH   = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = U !U
         VPRN(2,IN) = 0.0D0 !V
         VPRN(3,IN) = H !H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0               ! SOLRqx
         VSOLR(2,IN) = 2.0D0*W*SPHI*U*H    ! SOLRqy = 2*w*sin(30)*u*H
         VSOLR(3,IN) = 0.0D0               ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE EXPOCO(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      DV = 1.0D0
      DH = 1.0D0
      W    = 0.7292D-4
	  SPHI = 0.5D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = EXP(X) !u
         VPRN(2,IN) = EXP(Y) !v
         VPRN(3,IN) = EXP(-X-Y) !H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -EXP(-X-Y)*EXP(-X-Y)-2.0D0*W*SPHI
     &       *EXP(-X)                                         ! SOLRqx
         VSOLR(2,IN) = -EXP(-X-Y)*EXP(-X-Y)+2.0D0*W*SPHI
     &       *EXP(-Y)                                         ! SOLRqy
         VSOLR(3,IN) = 0.0D0                                  ! SOLRh
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE EXPOLM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      DV = 1.0D0
      DH = 1.0D0
      LM = 1.0D0
      UN   = 1.0D0
      DEU  = 2.0D0
      TROI = 3.0D0
      DEMI = 0.5D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = EXP(X) !u
         VPRN(2,IN) = EXP(Y) !v
         VPRN(3,IN) = EXP(-X-Y) !H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH



         VSOLR(1, IN) = EXP(-DEU*X-DEU*Y)*( -UN
     &      +DEU*DEU* (DEU*EXP(DEU*X)+DEU*EXP(2*Y))**0.5D0
     &      / EXP(Y) -4*EXP(DEU*X-Y)/(DEU*EXP(DEU*X)+DEU
     &      *EXP(DEU*Y))**DEMI)                                       ! SOLRqx


         VSOLR(2, IN) = EXP(-DEU*X-DEU*Y)*( -UN
     &      +DEU*DEU* (DEU*EXP(DEU*X)+DEU*EXP(2*Y))**0.5D0
     &      / EXP(X) -4*EXP(DEU*Y-X)/(DEU*EXP(DEU*X)+DEU
     &      *EXP(DEU*Y))**DEMI)                                       ! SOLRqy
         VSOLR(3, IN) = 0.0D0                                         ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE F_EY_0_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = EXP(Y)
         VPRN(2,IN) = 0.0D0
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -EXP(Y)*EXP(Y)-EXP(Y)*EXP(Y)     ! SOLRqx = -2exp(2y)
         VSOLR(2,IN) = 0.0D0                            ! SOLRqy
         VSOLR(3,IN) = 0.0D0                            ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE F_0_EX_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN) - VCORG(1,1)
         Y = VCORG(2, IN) - VCORG(2,1)
         VPRN(1,IN) = 0.0D0
         VPRN(2,IN) = EXP(X)
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0                           ! SOLRqx
         VSOLR(2,IN) = -EXP(X)*EXP(X)-EXP(X)*EXP(X)    ! SOLRqy = 2exp(2x)
         VSOLR(3,IN) = 0.0D0                           ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE F_EX_0_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN) - VCORG(1,1)
         Y = VCORG(2, IN) - VCORG(2,1)
         VPRN(1,IN) = EXP(X)
         VPRN(2,IN) = 0.0D0
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -4.0D0*SQRT(2.0D0)*EXP(X)*EXP(X)     ! SOLRqx = -4sqrt(2)*exp(2x)
         VSOLR(2,IN) = 0.0D0                                ! SOLRqy
         VSOLR(3,IN) = EXP(X)                               ! SOLRh  = exp(x)
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE F_0_EY_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = 0.0D0
         VPRN(2,IN) = EXP(Y)
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0
         VSOLR(2,IN) = -4.0D0*SQRT(2.0D0)*EXP(Y)*EXP(Y)
         VSOLR(3,IN) = EXP(Y)
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE EY_0_FX_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------
      DHDX = 0.00075D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN) - VCORG(1,1)
         Y = VCORG(2, IN) - VCORG(2,1)
         H = 1.0D0 - DHDX*X
         VPRN(1,IN) = EXP(Y)
         VPRN(2,IN) = 0
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -DHDX*H-2.0D0*EXP(Y)*EXP(Y)
     &*H*H*H                                                   ! SOLRqx
         VSOLR(2,IN) = -3.0D0*DHDX*H*H*EXP(Y)*EXP(Y)           ! SOLRqy
         VSOLR(3,IN) = -DHDX*EXP(Y)                            ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE UY_0_1_LM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------
      TROIS = 3.0D0
      DEUX  = 2.0D0
      H     = 1.0D0
      DV    = 1.0D0
      DH    = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN) - VCORG(1,1)
         Y = VCORG(2, IN) - VCORG(2,1)
         VPRN(1,IN) = DEUX/TROIS*Y**(TROIS/DEUX)
         VPRN(2,IN) = 0
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -1.0D0                                  ! SOLRqx
         VSOLR(2,IN) = 0.0D0                                   ! SOLRqy
         VSOLR(3,IN) = 0.0D0                                   ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE CONVECYX(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------
      CALL CONVYX(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)
      DO IN=1,NNT
         X = VCORG(1,IN)-VCORG(1,1)
         Y = VCORG(2,IN)-VCORG(2,1)
         VSOLR(1,IN) = X
         VSOLR(2,IN) = Y
         VSOLR(3,IN) = 0.0D0
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE EXPOXY(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VPRN(1,IN) = EXP(X) !u
         VPRN(2,IN) = EXP(Y) !v
         VPRN(3,IN) = H      !H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 2.0D0*(EXP(X)*EXP(X)-EXP(X))
     &                 +EXP(X+Y)                   ! SOLR2
         VSOLR(2,IN) = 2.0D0*(EXP(Y)*EXP(Y)-EXP(Y))
     &                 +EXP(X+Y)                   ! SOLR3
         VSOLR(3,IN) = 0.0D0                       ! SOLR1
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE C_UFAIBLE(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      U  = 1.0D-10
      V  = 0.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         VPRN(1,IN) = U      !u
         VPRN(2,IN) = V      !v
         VPRN(3,IN) = H      !H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE EXPOXY_MANNING(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      CALL EXPOXY(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)
      DO IN=1,NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VSOLR(1,IN) = EXP(X)*SQRT(EXP(X+X)+EXP(Y+Y))
     &                                                          ! SOLR2
         VSOLR(2,IN) = EXP(Y)*SQRT(EXP(X+X)*EXP(Y+Y))
     &                                                          ! SOLR3
         VSOLR(3,IN) = EXP(X)+EXP(Y)                            ! SOLR1
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE PROPAG_CANALX(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      U  = 1.0D-2
      V  = 0.0D0
      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNY+1
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
       ENDDO
       DO IN=NNY+1,NNT
         VPRN(1,IN) = 0.0D0
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH
       ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE QUAD_X(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      V  = 0.0D0
      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2,IN) - VCORG(2,1)
         VPRN(1,IN) = (Y**1.5D0)/1.5D0          ! u = (sqrt(y))/1.5
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1, IN) = -1.0D0                  ! SOLRqx = -1
         VSOLR(2, IN) = 0.0D0                   ! SOLRqy
         VSOLR(3, IN) = 0.0D0                   ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_0  (NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = 1.0D0 ! u
         VPRN(2,IN) = 0.0D0 ! v
         VPRN(3,IN) = H     ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0                          ! SOLRqx
         VSOLR(2,IN) = 0.0D0                          ! SOLRqy
         VSOLR(3,IN) = 0.0D0                          ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_05_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         X = VCORG(2, IN)
         VPRN(1,IN) = SQRT(Y) ! u
         VPRN(2,IN) = 0.0D0   ! v
         VPRN(3,IN) = H       ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         IF (Y .LE. 1.0D-6) THEN
            VSOLR(1,IN) = 0.0D0
         ELSE
            VSOLR(1,IN) = 0.5D0*Y**(-1.5D0)              ! SOLRqx
            VSOLR(2,IN) = 0.0D0                          ! SOLRqy
            VSOLR(3,IN) = 0.0D0                          ! SOLRh
         ENDIF
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_05_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = Y**0.5D0/0.5D0                  ! u
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         IF (Y .LE. 1.0D-6) THEN
            VSOLR(1,IN) = 0.0D0
         ELSE
            VSOLR(1,IN) = 1.0D0/Y**2                     ! SOLRqx
            VSOLR(2,IN) = 0.0D0                          ! SOLRqy
            VSOLR(3,IN) = 0.0D0                          ! SOLRh
         ENDIF
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_1(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = Y                               ! u=couettex
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = 0.0D0                          ! SOLRqx
         VSOLR(2,IN) = 0.0D0                          ! SOLRqy
         VSOLR(3,IN) = 0.0D0                          ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_15_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = Y**1.5D0/1.5D0                  ! u
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         IF (Y .LE. 1.0D-6) THEN
            VSOLR(1,IN) = 0.0D0
         ELSE
            VSOLR(1,IN) = -0.5D0/Y**0.5D0                ! SOLRqx
            VSOLR(2,IN) = 0.0D0                          ! SOLRqy
            VSOLR(3,IN) = 0.0D0                          ! SOLRh
         ENDIF

      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_15_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = Y**1.5D0/1.5D0                  ! u
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -1.0D0                         ! SOLRqx
         VSOLR(2,IN) = 0.0D0                          ! SOLRqy
         VSOLR(3,IN) = 0.0D0                          ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_2_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = Y**2.0D0/2.0D0                  ! u
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -1.0D0                         ! SOLRqx
         VSOLR(2,IN) = 0.0D0                          ! SOLRqy
         VSOLR(3,IN) = 0.0D0                          ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_2_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = Y**2.0D0/2.0D0                  ! u
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         IF (Y .LE. 1.0D-6) THEN
            VSOLR(1,IN) = 0.0D0
         ELSE
            VSOLR(1,IN) = -2.0D0/Y                       ! SOLRqx
            VSOLR(2,IN) = 0.0D0                          ! SOLRqy
            VSOLR(3,IN) = 0.0D0                          ! SOLRh
         ENDIF

      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_3_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = Y**3.0D0/3.0D0                  ! u
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -2/Y                           ! SOLRqx
         VSOLR(2,IN) = 0.0D0                          ! SOLRqy
         VSOLR(3,IN) = 0.0D0                          ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_3_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = Y**3.0D0/3.0D0                  ! u
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -4.0D0*Y**3.0D0                ! SOLRqx
         VSOLR(2,IN) = 0.0D0                          ! SOLRqy
         VSOLR(3,IN) = 0.0D0                          ! SOLRh
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_15_1_LAM(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = 1.0D0+Y**1.5D0/1.5D0            ! u
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         IF (Y .LE. 1.0D-6) THEN
            VSOLR(1,IN) = 0.0D0
         ELSE
            VSOLR(1,IN) = -0.5D0/Y**0.5D0                ! SOLRqx
            VSOLR(2,IN) = 0.0D0                          ! SOLRqy
            VSOLR(3,IN) = 0.0D0                          ! SOLRh
         ENDIF

      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE Y_EXP_15_1_TUR(NDIM, NNX, NNY, NNT, VCORG,
     &                    NNEL, NELT, KNG,
     &                    NPRN, VPRN,
     &                    NDLN, VSOLR)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VSOLR(NDLN,1)
C-----------------------------------------------------------------------------

      H  = 1.0D0
      DV = 1.0D0
      DH = 1.0D0
      DO IN=1,NNT
         Y = VCORG(2, IN)
         VPRN(1,IN) = 1.0D0+Y**1.5D0/1.5D0            ! u
         VPRN(2,IN) = 0.0D0                           ! v
         VPRN(3,IN) = H                               ! H
         VPRN(4,IN) = DV
         VPRN(5,IN) = DH

         VSOLR(1,IN) = -1.0D0                         ! SOLRqx
         VSOLR(2,IN) = 0.0D0                          ! SOLRqy
         VSOLR(3,IN) = 0.0D0                          ! SOLRh
      ENDDO

      RETURN
      END