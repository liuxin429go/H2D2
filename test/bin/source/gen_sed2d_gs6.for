C=============================================================================
C
C=============================================================================
      PROGRAM GEN_SED2D_GS6

      INCLUDE 'gentest.fi'

      PARAMETER (NDIM    = 2)
      PARAMETER (NCLASS  = 6)
      PARAMETER (NDLN    = NCLASS+1)
      PARAMETER (NLAYER  = 5)
      PARAMETER (NPRNH   = 4)
      PARAMETER (NPRNZ   = 1)
      PARAMETER (NPRNT   = 1)
      PARAMETER (NPRNF   = NCLASS)
      PARAMETER (DT      = 1.0D0)
      PARAMETER (UN_LMDA = 1.0D0 - 0.20D0)
      PARAMETER (TL2 = 1.0D-0)

      ALLOCATABLE :: VCORG(:)
      ALLOCATABLE :: VPRNH(:)
      ALLOCATABLE :: VPRNZ(:)
      ALLOCATABLE :: VPRNT(:)
      ALLOCATABLE :: VPRNF1(:)
      ALLOCATABLE :: VPRNF2(:)
      ALLOCATABLE :: VPRNF3(:)
      ALLOCATABLE :: VPRNF4(:)
      ALLOCATABLE :: VPRNF5(:)
      ALLOCATABLE :: VSOL(:)
      ALLOCATABLE :: VDLG(:)
      ALLOCATABLE :: KDIMP(:)
      ALLOCATABLE :: KNG(:)

      CHARACTER*256 NOMBASE
      CHARACTER*256 CZPREI
      CHARACTER*256 CFLII
      CHARACTER*256 CQB
      CHARACTER*256 CONDLIM
      CHARACTER*4   TYPEL
      LOGICAL       LNODRND
C-----------------------------------------------------------------------------

C-------  LIS LES DONNEES
      READ(5,*) NOMBASE
      READ(5,*) NELX
      READ(5,*) NELY
      READ(5,*) XORI
      READ(5,*) YORI
      READ(5,*) DIMX
      READ(5,*) DIMY
      READ(5,*) NNEL
      READ(5,*) TYPEL
      READ(5,*) CZPREI
      READ(5,*) CFLII
      READ(5,*) CQB
      READ(5,*) CONDLIM
C-------  PRE-TREATMENT
      LNODRND = .FALSE.
      CALL TRIMSTR(TYPEL)
      CALL UCASESTR(TYPEL)
      IF (TYPEL(1:1) .EQ. '~') THEN
         LNODRND = .TRUE.
         TYPEL(1:1) = ' '
         CALL TRIMSTR(TYPEL)
      ENDIF
      IF (TYPEL(1:1) .EQ. '/') THEN
         ITYPEL = IEL$SLASH
      ELSEIF (TYPEL(1:1) .EQ. '\') THEN
         ITYPEL = IEL$SLINV
      ELSEIF (TYPEL(1:1) .EQ. 'v') THEN
         ITYPEL = IEL$V
      ELSEIF (TYPEL(1:1) .EQ. 'V') THEN
         ITYPEL = IEL$V
      ELSEIF (TYPEL(1:1) .EQ. '^') THEN
         ITYPEL = IEL$VINV
      ELSEIF (TYPEL(1:1) .EQ. '>') THEN
         ITYPEL = IEL$GT
      ELSEIF (TYPEL(1:1) .EQ. '<') THEN
         ITYPEL = IEL$LT
      ELSEIF (TYPEL(1:1) .EQ. 'X') THEN
         ITYPEL = IEL$X
      ELSE
         WRITE(*,*) 'UNKONOWN OPTION:', TYPEL
         STOP
      ENDIF
      IF (NNEL .EQ. 3) THEN
         NNX = NELX + 1
         NNY = NELY + 1
      ELSEIF(NNEL .EQ. 6) THEN
         NNX = 2*NELX + 1
         NNY = 2*NELY + 1
      ELSE
         WRITE(*,*) 'NUMBER OF NODES PER ELEMENT INVALID:', NNEL
         STOP
      ENDIF
      NNT = NNX*NNY
      CALL TRIMSTR(NOMBASE)
      CALL LCASESTR(NOMBASE)
      CALL TRIMSTR(CQB)
      CALL UCASESTR(CQB)
      CALL TRIMSTR(CZPREI)
      CALL UCASESTR(CZPREI)
      CALL TRIMSTR(CFLII)
      CALL UCASESTR(CFLII)

C-------  ALLOCATE ARRAYS
      ALLOCATE(VCORG(NDIM*NNT))
      ALLOCATE(VPRNH(NPRNH*NNT))
      ALLOCATE(VPRNZ(NPRNZ*NNT))
      ALLOCATE(VPRNT(NPRNT*NNT))
      ALLOCATE(VPRNF1(NPRNF*NNT))
      ALLOCATE(VPRNF2(NPRNF*NNT))
      ALLOCATE(VPRNF3(NPRNF*NNT))
      ALLOCATE(VPRNF4(NPRNF*NNT))
      ALLOCATE(VPRNF5(NPRNF*NNT))
      ALLOCATE(VSOL(NDLN*NNT))
      ALLOCATE(VDLG(NDLN*NNT))
      ALLOCATE(KDIMP(NDLN*NNT))
      ALLOCATE(KNG(6*NNT))

C-------  GENERATE MESH
      CALL GENCOOR(LNODRND, XORI, YORI, DIMX, DIMY, NNX, NNY, VCORG)
      CALL GENELEM(ITYPEL, NNX, NNY, VCORG, NNEL, NELT, KNG)

C-------  GENERATE INITIAL BED LEVEL
      CALL GENZPRE(CZPREI(1:LENSTR(CZPREI)),
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRNZ, VPRNZ,
     &             NDLN, VDLG)

C-------  GENERATE SUBLAYER THICKNESS
      CALL GENTL2(CFLII(1:LENSTR(CFLII)),
     &            NDIM, NNX, NNY, NNT, VCORG,
     &            NNEL, NELT, KNG,
     &            NPRNT, VPRNT, TL2)

C-------  GENERATE INITIAL BED LAYER INFORMATION
      CALL GENFLI1(CFLII(1:LENSTR(CFLII)),
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRNF, VPRNF1,
     &             NDLN, VDLG)

      CALL GENFLI2(CFLII(1:LENSTR(CFLII)),
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRNF, VPRNF2)

      CALL GENFLI3(CFLII(1:LENSTR(CFLII)),
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRNF, VPRNF3)

      CALL GENFLI4(CFLII(1:LENSTR(CFLII)),
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRNF, VPRNF4)

      CALL GENFLI5(CFLII(1:LENSTR(CFLII)),
     &             NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRNF, VPRNF5)

C-------  GENERATE LIMITING CONDITIONS
      CALL GENCL_ML  (CONDLIM(1:LENSTR(CONDLIM)),
     &                NNX, NNY, NNT,
     &                NDLN, KDIMP)

C-------  GENERATE HYDRODYNAMIC, BED LEVEL AND SIZE FRACTIONS SOLUTION
C-------  FOR SPECIFIED TRANSPORT RATE DISTRIBUTION
      CALL GENQB(CQB(1:LENSTR(CQB)),
     &           NDIM, NNX, NNY, NNT, VCORG,
     &           NNEL, NELT, KNG,
     &           NPRNH, VPRNH,
     &           NPRNZ, VPRNZ,
     &           NDLN, VSOL, VDLG, DT, UN_LMDA)


C-------  WRITE RESULTS
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.cor',
     &              NNT,
     &              NDIM,
     &              VCORG)
      CALL ECRISELE(NOMBASE(1:LENSTR(NOMBASE))//'.ele',
     &              NELT,
     &              NNEL,
     &              KNG)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.pnh',
     &              NNT,
     &              NPRNH,
     &              VPRNH)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.zpr',
     &              NNT,
     &              NPRNZ,
     &              VPRNZ)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.tl2',
     &              NNT,
     &              NPRNT,
     &              VPRNT)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.fl1',
     &              NNT,
     &              NPRNF,
     &              VPRNF1)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.fl2',
     &              NNT,
     &              NPRNF,
     &              VPRNF2)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.fl3',
     &              NNT,
     &              NPRNF,
     &              VPRNF3)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.fl4',
     &              NNT,
     &              NPRNF,
     &              VPRNF4)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.fl5',
     &              NNT,
     &              NPRNF,
     &              VPRNF5)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.sol',
     &              NNT,
     &              NDLN,
     &              VSOL)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.ini',
     &              NNT,
     &              NDLN,
     &              VDLG)
      CALL ECRISCND(NOMBASE(1:LENSTR(NOMBASE))//'.cnd',
     &              NNT,
     &              NDLN,
     &              VDLG, VSOL, DT, KDIMP)
      CALL ECRISBND(NOMBASE(1:LENSTR(NOMBASE))//'.bnd',
     &              NNT,
     &              NDLN,
     &              VDLG, KDIMP)

C-------  DEALLOCATE ARRAYS
      DEALLOCATE(KNG)
      DEALLOCATE(KDIMP)
      DEALLOCATE(VDLG)
      DEALLOCATE(VSOL)
      DEALLOCATE(VPRNH)
      DEALLOCATE(VPRNZ)
      DEALLOCATE(VPRNT)
      DEALLOCATE(VPRNF1)
      DEALLOCATE(VPRNF2)
      DEALLOCATE(VPRNF3)
      DEALLOCATE(VPRNF4)
      DEALLOCATE(VPRNF5)
      DEALLOCATE(VCORG)

      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISCND(NOMFIC, NNT, NDLN, VDLG, VSOL, DT, KDIMP)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION VSOL (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)

      CHARACTER*256 NOMCL
      CHARACTER*100  FMT
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I3,', 1, '(1X,1PE25.17E3),A)'

      NCND = 0
      DO IN=1,NNT
         IF (KDIMP(1, IN) .GT. 0) NCND = NCND + 1
      ENDDO
      WRITE(10, '(I9,1X,1PE25.17E3)') NCND, 0.0


100   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199

         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               KDIMP(1,IN) = -KDIMP(1,IN)

               WRITE(NOMCL,FMT) '$',IC/1,(VDLG(ID,IN),ID=1,1),'$'
               CALL TRIMSTR(NOMCL)
               CALL SBSTSTR(NOMCL, ' ','_')

               WRITE(10,'(A,I3,11(1X,1PE25.17E3))')
     &                                 NOMCL(1:LENSTR(NOMCL)),
     &                                 1,
     &                                 (VDLG(ID,IN),ID=1,NDLN)
            ENDIF
         ENDDO


      GOTO 100

199   CONTINUE

      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      WRITE(10, '(I9,1X,1PE25.17E3)') NCND, DT


200   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               GOTO 209
            ENDIF
         ENDDO
209      CONTINUE
         IF (IC .LT. 0) GOTO 299

         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               KDIMP(1,IN) = -KDIMP(1,IN)

               WRITE(NOMCL,FMT) '$',IC/1,(VDLG(ID,IN),ID=1,1),'$'
               CALL TRIMSTR(NOMCL)
               CALL SBSTSTR(NOMCL, ' ','_')

               WRITE(10,'(A,I3,11(1X,1PE25.17E3))')
     &                                 NOMCL(1:LENSTR(NOMCL)),
     &                                 1,
     &                                 (VSOL(ID,IN),ID=1,NDLN)
            ENDIF
         ENDDO

      GOTO 200

299   CONTINUE

      CLOSE(10)

      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISBND(NOMFIC, NNT, NDLN, VDLG, KDIMP)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)

      CHARACTER*256 NOMCL
      CHARACTER*32  FMT
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I3,', 1, '(1X,1PE25.17E3),A)'

      NCL = 0
      DO IN=1,NNT
         IF (KDIMP(1, IN) .GT. 0) NCL = NCL + 1
      ENDDO
      WRITE(10, '(I9)') NCL

100   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199

         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               KDIMP(1,IN) = -KDIMP(1,IN)

               WRITE(NOMCL,FMT) '$',IC/1,(VDLG(ID,IN),ID=1,1),'$'
               CALL TRIMSTR(NOMCL)
               CALL SBSTSTR(NOMCL, ' ','_')

               WRITE(10, '(A,I9)') NOMCL(1:LENSTR(NOMCL)), IN
            ENDIF
         ENDDO

      GOTO 100
199   CONTINUE

      CLOSE(10)

      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISELE(NOMFIC, NELT, NNEL, KNG)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION KNG(NNEL, 1)
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      IF (NNEL .EQ. 3) THEN
         WRITE(10, '(3I9,1X,1PE25.17E3)') NELT, NNEL, 201, 0.0
      ELSE
         WRITE(10, '(3I9,1X,1PE25.17E3)') NELT, NNEL, 203, 0.0
      ENDIF
      DO IE=1,NELT
         WRITE(10, '(6I9)') (KNG(I, IE), I=1,NNEL)
      ENDDO
      CLOSE(10)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISPRN(NOMFIC, NNT, NPRN, VPRN)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VPRN(NPRN, 1)
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(10, '(2I9,1X,1PE25.17E3)') NNT, NPRN, 0.0
      DO IN=1,NNT
         WRITE(10, '(25(1X,1PE25.17E3))') (VPRN(I, IN), I=1,NPRN)
      ENDDO
      CLOSE(10)

      RETURN
      END
