set SCONSTRUCT=%HOME%/dev/build/MUC_Scons/SConstruct
call scons -Q --file=%SCONSTRUCT%	compilers=intel-9.0 	builds=debug,release
call scons -Q --file=%SCONSTRUCT%	compilers=intel-9.1 	builds=debug,release
call scons -Q --file=%SCONSTRUCT%	compilers=itlX64-9.0 	builds=debug,release
call scons -Q --file=%SCONSTRUCT%	compilers=itlX64-9.1 	builds=debug,release
call scons -Q --file=%SCONSTRUCT%	compilers=itlI64-9.0 	builds=debug,release
call scons -Q --file=%SCONSTRUCT%	compilers=itlI64-9.1 	builds=debug,release
call scons -Q --file=%SCONSTRUCT%	compilers=gcc-3.4 	builds=debug,release
