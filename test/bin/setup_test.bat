::  make + DIRCMD causent une erreur dans H2D2
::  lors de la commande "dir" pour obtenir la liste
::  des fichiers
set DIRCMD=

set PATH=%ProgramFiles%/MSYS/1.0/bin;%PATH%

set BT_GENTEST_BINDIR=%INRS_DEV%/H2D2/test/bin

::set BT_GENTEST=%BT_GENTEST_BINDIR%/bin/gen_cd2d.exe
set BT_GENTEST=%BT_GENTEST_BINDIR%/bin/gen_sv2d.exe
::set BT_GENTEST=%BT_GENTEST_BINDIR%/bin/gen_macro3d.exe
::set BT_GENTEST=%BT_GENTEST_BINDIR%/bin/gen_sed2d_gs1.exe

set BT_ERRCNV=%BT_GENTEST_BINDIR%/bin/errcnv.exe

set BT_TARGET_DIR=%INRS_DEV%\Debug\x64\bin
set BT_TARGET_EXE=%BT_TARGET_DIR%/h2d2.exe

make clean -f makefile.mak
make -f makefile.mak