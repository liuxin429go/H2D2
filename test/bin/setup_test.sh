#!/bin/bash
###
#   A utiliser avec la commande source pour que les modifications
#   à l'environement soient prisent en compte par l'appelant:
#       source setup_test.sh
###
BT_GENTEST_BINDIR=$INRS_DEV/H2D2/test/bin; export BT_GENTEST_BINDIR
#BT_GENTEST=$BT_GENTEST_BINDIR/gen_cd2d; export BT_GENTEST
BT_GENTEST=$BT_GENTEST_BINDIR/gen_sv2d; export BT_GENTEST
#BT_GENTEST=$BT_GENTEST_BINDIR/gen_macro3d; export BT_GENTEST

BT_ERRCNV=$BT_GENTEST_BINDIR/errcnv; export BT_ERRCNV

BT_TARGET_DIR=$INRS_DEV/_run/H2D2/win32/deino_mpi/gcc-xx/static/debug/bin; export BT_TARGET_DIR
BT_TARGET_EXE=$BT_TARGET_DIR/h2d2; export BT_TARGET_EXE
LD_LIBRARY_PATH=$BT_TARGET_DIR:$HOME/bin; export LD_LIBRARY_PATH

