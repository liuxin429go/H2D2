# Modeleur 1.0a07b
#
# Fichier de commandes généré automatiquement pour H2D2
# (spécifique aux élements Couvrant-Découvrant Version Yves Secretan)
#--------------------------------------------------------------------

#-----------------------------------
#  Conditions initiales
#-----------------------------------

u_ini = 0.0
v_ini = 0.0
h_ini = 95.45

#-----------------------------------
#  Répertoire de simulation
#-----------------------------------
h_log = log()
rep_sim = '../'

#-----------------------------------
#  Maillage
#-----------------------------------
h_cor  = coor(rep_sim+'simul000.cor')
h_ele  = elem(rep_sim+'simul000.ele')
h_grid = grid(h_cor, h_ele)
h_num  = 0                      # Partitionnement, renumérotation

#-----------------------------------
#  Propriétés globales
#-----------------------------------
h_pgl  = prgl(9.8059999999999992e+00, #  1 gravite
              4.7000000000000000e+01, #  2 latitude
              0.0000000000000000e-06, #  3 laminar viscosity
              1.0000000000000000e+00, #  4 mixing length coef
              0.0000000000000000e+00, #  5 mixing length coef related to grid size
              9.9999999999999995e-07, #  6 viscosity limiter: inf
              1.0000000000000000e+02, #  7 viscosity limiter: sup
              2.0000000000000000e-02, #  8 dry/wet: min depth
              2.0000000000000000e-02, #  9
              5.0000000000000000e-01, #  10 dry/wet: manning
              1.0e+003,               # 11 dry/wet: |u| max
              1.0000000000000000e+00, # 12 dry/wet: porosity
              0.0e-000,               # 13 dry/wet: damping
              0.0,                    # 14 dry/wet: coef. convection
              0.0,                    # 15 dry/wet: coef. gravity
              1.0,                    # 16 dry/wet: nu diffusion   !!1/Hmin!!
              1.0,                    # 17 dry/wet: nu darcy
              5.0000000000000000e-01, # 18 peclet
              0.0e-000,               # 19 damping coef.
              1.0e-05,                # 20 free surface smoothing (Darcy)
              1.0000000000000001e-05, # 21 free surface smoothing (Lapidus)
              1.0000000000000000e+00, # 22 coef. convection
              1.0,                    # 23 coef. gravity
              1.0,                    # 24 coef. manning
              1.0,                    # 25 coef. wind
              1.0,                    # 26 coef. boundary integral
              1.0e-18,                # 27 coef. penality
              1.0e-007,               # 28 coef. Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
              1.0,                    # 29 coef. Kt relaxation
              1,                      # 30 flag prel perturbation (0 = false, !=0 = true)
              1)                      # 31 flag prno perturbation (0 = false, !=0 = true)

#-----------------------------------
#  Bathymetrie
#-----------------------------------
h_vno_bth = vnod(rep_sim+'simul000.bth')      # zf

#-----------------------------------
#  Frottement du substrat
#-----------------------------------
h_vno_frs = vnod(rep_sim+'simul000.frs')      # n manning

#-----------------------------------
#  Propriétés nodales
#-----------------------------------
h_vno_gla = vnod(rep_sim+'simul000.gla')                    # Pas de glace
h_vno_vnt = vnod(rep_sim+'simul000.vnt')                    # Pas de vent
h_prn = prno(h_vno_bth, h_vno_frs, h_vno_gla, h_vno_vnt)    # regroupement des champs en prno

#-----------------------------------
#  Propriétés élémentaires
#-----------------------------------
h_pre  = 0                      # Pas de propriétés élémentaires

#-----------------------------------
#  Sollicitations
#-----------------------------------
h_slc  = 0                      # Pas de sollicitations concentrées
h_slr  = 0                      # Pas de sollicitations réparties

#-----------------------------------
#  Conditions limites
#-----------------------------------
h_cnd = condition(rep_sim+'simul000.cnd')
h_lmt = boundary (rep_sim+'simul000.slc')
h_bc  = boundary_condition(h_lmt, h_cnd)

#-----------------------------------
#  Degrés de liberté
#-----------------------------------
h_ddl  = dof()

#-----------------------------------
#  Données globales de simulation
#-----------------------------------
h_frm = form('SV2D_Conservatif_CDYS_NN')       # Formulation
h_sol = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_pgl, h_prn, h_pre)

#------------------------------------
#  Définition de l'algo de résolution
#------------------------------------
nprec = 4
nrdem = 10
niter = 25
eps0  = 1.0e-06    # eps0 + (eps11 + eps12*eta)*res
eps11 = 0.0
eps12 = 0.0
h_precond = pr_ilun()
h_btrk   = 0 # backtracking_2()
h_solver = gmres(h_btrk, h_precond, nprec, nrdem, niter, eps0, eps11, eps12)

#------------------------------------
#  Critère d'arrêt, limiteur
#------------------------------------
h_lmtr  = limiter(h_sol, 1.0e-00, 1.0e-00, 0.1e-00)
h_cria = cria_l2_allrel(h_sol,       # du < (rel*|u| + abs)
1.0e-08,         # relatif
1.0000000000000000e-04)     # absolu

#-----------------------------------
#  Résidus
#-----------------------------------
h_pst_res = post_residu('simul000.res')

#-----------------------------------
#  Post-traitement
#-----------------------------------
h_pst_sim = post_simulation     ('simul000.pst.sim')
h_pst_q   = sv2d_pst_bilan_debit('simul000.pst.q')
h_pst_chkq = sv2d_pst_checkq()

#-----------------------------------
#  Compose les post-traitement
#-----------------------------------
h_post = post_compose(h_pst_res, h_pst_sim, h_pst_q, h_pst_chkq)

#-----------------------------------
#  Initialisation et résolution
#-----------------------------------
h_pst = 0         # post-traitement en cours de résolution non implanté
temps = 0.0
h_log.level = 0

#-----------------------------------
#  Hmin = 1.0e-2
#-----------------------------------
h_pgl[8]  = 0.125   # n
h_pgl[10] = 1.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 0.25    # n
h_pgl[10] = 1.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 0.5     # n
h_pgl[10] = 1.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 1.0     # n
h_pgl[10] = 1.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 2.0     # n
h_pgl[10] = 1.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

#-----------------------------------
#  Hmin = 2.0e-2
#-----------------------------------
h_pgl[8]  = 0.125   # n
h_pgl[10] = 2.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 0.25    # n
h_pgl[10] = 2.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 0.5     # n
h_pgl[10] = 2.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 1.0     # n
h_pgl[10] = 2.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 2.0     # n
h_pgl[10] = 2.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

#-----------------------------------
#  Hmin = 4.0e-2
#-----------------------------------
h_pgl[8]  = 0.125   # n
h_pgl[10] = 4.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 0.25    # n
h_pgl[10] = 4.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 0.5     # n
h_pgl[10] = 4.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 1.0     # n
h_pgl[10] = 4.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

h_pgl[8]  = 2.0     # n
h_pgl[10] = 4.0e-2  # Hmin
h_sol.init_v(u_ini, v_ini, h_ini)
h_solver.solve(h_sol, h_cria, h_lmtr, h_pst, temps)
h_post.xeq(h_sol)

#-----------------------------------
#  Erreurs
#-----------------------------------
##
##------------------------------------------------
##  Le calcul d'erreur n'est par encore implanté
##------------------------------------------------
##h_pst_err = post_erreur('simul000.err')
##h_pst_err.xeq(h_sol)
##

#-----------------------------------
#  Résultats finaux
#-----------------------------------
h_sol.save('simul000.fin', 'w')

#-----------------------------------
#  Fin du calcul
#-----------------------------------
stop()

