#=====================================================================
# $Header$
# $Date$
#=====================================================================

#=====================================================================
# Description:
#    Makefile pour faire les tests
#
#=====================================================================

SOUS_REPERTOIRES=$(wildcard *)

# ---  Suffixes connus
.SUFFIXES:
.SUFFIXES: .clean .all

%.clean:
	@if (test -d $(*F)) && (test -r $(*F)/makefile.mak); then \
		cd $(*F); $(MAKE) -f makefile.mak clean; \
		echo --------  Rule $(*F) done; \
	fi

%.all:
	@if (test -d $(*F)) && (test -r $(*F)/makefile.mak); then \
		cd $(*F); $(MAKE) $(if $(MAKEFLAGS),-$(MAKEFLAGS)) -f makefile.mak all; \
		echo --------  Rule $(*F) done; \
	fi

all:	$(foreach rep,$(SOUS_REPERTOIRES),$(rep).all)


clean:	$(foreach rep,$(SOUS_REPERTOIRES),$(rep).clean)


