CAS_TEST=10x10;
NB_PROC = 2

# indique si on teste le partitionnement ou la numérotation
# "part" pour partitionnement, "num" pour numérotation
TYPE_TEST = part_num

include $(BT_GENTEST_BINDIR)/makefile.inc
