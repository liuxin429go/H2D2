C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: TEST_NI_P12L_MAIN
C
C Description:
C     Le program TEST_NI_P12L_MAIN  est un test de l'intégration numérique
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      PROGRAM TEST_NI_P12L_MAIN

      INCLUDE 'eacmmn.fc' !LM_CMMN_NDLN
      INCLUDE 'egcmmn.fc' !EG_CMMN_NNELV

C--   ASSIGNATION DU COMMON
      EG_CMMN_NDIM  = 2
      EG_CMMN_NNELV = 6
      EG_CMMN_NDJV  = 5
      LM_CMMN_NDLN  = 5

      CALL NS_MACRO3D_ASGCMN(EG_CMMN_KA, LM_CMMN_KA)
      CALL LMGO_P12L_ASGCMN (EG_CMMN_KA)

      CALL TEST_INTEGRE()

      END

C************************************************************************
C Sommaire: SUBROUTINE TEST_INTEGRE()
C
C Description: La routine TEST_INTEGRE l'élément sur lequel intégrer
C              ainsi que les valeurs des dl aux sommets. Elle effectue une
C              intégration et retourne la réponse
C
C Entrée:
C
C Sortie: Impression des résultats
C
C
C Notes:
C************************************************************************
      SUBROUTINE TEST_INTEGRE()

      INCLUDE 'eacnst.fi' !ZERO
      INCLUDE 'eacmmn.fc' !LM_CMMN_NDLN
      INCLUDE 'egcmmn.fc' !EG_CMMN_NNELV
      INCLUDE 'ns_macro3d_idl.fi'

      INTEGER  NDLN
      INTEGER  NDJV
      INTEGER  NDIM
      INTEGER  NNELV
      INTEGER  NCELV
      INTEGER  NELV
      INTEGER  NDIMELV
      INTEGER NNT
      INTEGER NFNAPPROXUV
      INTEGER NFNAPPROXH

      PARAMETER (NDLN=5)
      PARAMETER (NDJV=5)
      PARAMETER (NDIM=2)
      PARAMETER (NNELV=6)
      PARAMETER (NCELV=6)
      PARAMETER (NELV=1)
      PARAMETER (NDIMELV=3)
      PARAMETER (NNT = 6)
      PARAMETER (NFNAPPROXUV= 6)
      PARAMETER (NFNAPPROXH = 3)

      INTEGER KNGV(NCELV, NELV)
      INTEGER I,J

      INTEGER IERR

C--   VARIABLES LUES
      REAL*8   VCORE (NDIM, NNELV)
      REAL*8   VZCORE(2, NNELV)       ! donne les zs et zf pour chacun des 6 noeuds.
      REAL*8   VDJE  (NDJV)
      REAL*8   VDLE (NDLN, NNELV)

C--   INTEGRATION NUMÉRIQUE
      INTEGER  NPG               !nombre de points de gauss
      INTEGER  IPG               !itérateur sur les points de gauss
      REAL*8   VCOPG (NDIMELV)   !point de gauss
      REAL*8   POIDSPG           !poids du point de gauss


C--   VARIABLES SORTIES DE NS_MACRO3D_EVALUE (variables testées)
      REAL*8   VNUV(NFNAPPROXUV)             !évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH(NFNAPPROXH)               !évaluation des N de h au pt gauss
      REAL*8   VNUV_XI(NDIMELV, NFNAPPROXUV) !évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   VNH_XI(NDIMELV, NFNAPPROXH)   !évaluation des N,x_i (les N pour les h) aux pt de Gauss
      REAL*8   VDL_XIPT(3,NDIMELV)
      REAL*8   VW_XIPT(NDIMELV)
      REAL*8   VDLEPT(3)                     !sortie : évaluation des degrés de liberté au point d'intégration
      REAL*8   WPT                           !sortie : évaluation de la vitesse verticale au point d'intégration
      REAL*8   DJP12L                        !évaluation du déterminant au pt de gauss
      REAL*8   DJP6                          !évaluation du déterminant au pt de gauss
      INTEGER  IP6                           !Entier de 1 à 4 indiquant dans quel sous-élémnet se trouve le pt de gauss

C--   VARIABLES RÉSULTAT
      REAL*8   INTDETJP12L                   !intégrale sur l'élément de référence de detJP12L équivaut à intégrale sur l'élément rééel de "1", donc au volume
      REAL*8   INTU                          !intégrale sur l'élément de référence de la vitesse en x

C ---------------------------------------------------------------------
C ---------------------------------------------------------------------

C--   CONNECTIVITÉS
      KNGV(1,1) = 1
      KNGV(2,1) = 2
      KNGV(3,1) = 3
      KNGV(4,1) = 4
      KNGV(5,1) = 5
      KNGV(6,1) = 6

C---  LECTURES :

C---  LECTURE DE LA GÉOMÉTRIE DE L'ÉLÉMENT
      OPEN(12, FILE='test.cor', STATUS='OLD')
      READ(12,*) ((VCORE (I,J),J=1,6),I=1,2)
      READ(12,*) ((VZCORE(I,J),J=1,6),I=1,2)
      CLOSE(12)

C---  S'assure de la linéarité (test si le test est valide !)
D     CALL ERR_ASR(VZCORE(1,2) .EQ. UN_2*(VZCORE(1,1)+VZCORE(1,3)))
D     CALL ERR_ASR(VZCORE(1,4) .EQ. UN_2*(VZCORE(1,3)+VZCORE(1,5)))
D     CALL ERR_ASR(VZCORE(1,6) .EQ. UN_2*(VZCORE(1,5)+VZCORE(1,1)))
D     CALL ERR_ASR(VZCORE(2,2) .EQ. UN_2*(VZCORE(2,1)+VZCORE(2,3)))
D     CALL ERR_ASR(VZCORE(2,4) .EQ. UN_2*(VZCORE(2,3)+VZCORE(2,5)))
D     CALL ERR_ASR(VZCORE(2,6) .EQ. UN_2*(VZCORE(2,5)+VZCORE(2,1)))

C---  S'assure que profondeur positive
D     CALL ERR_ASR(VZCORE(2,1) .GT. VZCORE(1,1))
D     CALL ERR_ASR(VZCORE(2,3) .GT. VZCORE(1,3))
D     CALL ERR_ASR(VZCORE(2,5) .GT. VZCORE(1,5))

C---  LECTURE DES DDL
      OPEN(12, FILE='test.vno', STATUS='OLD')
      READ(12,*) ((VDLE(I, J), I=1,5), J=1,EG_CMMN_NNELV)
      CLOSE(12)

C---  RÉCUPÉRATION DES MÉTRIQUES
      IERR = LMGO_P12L_CLCJELV(  NDIM,
     &                           NNT,
     &                           NCELV,
     &                           NELV,
     &                           NDJV,
     &                           VCORE,
     &                           KNGV,
     &                           VDJE)

C---  INITIALISATION
      INTDETJP12L = ZERO
      INTU = ZERO

C---  INTÉGRATION NUMÉRIQUE
      NPG = NI_P12L_REQNPG()
D     CALL ERR_ASR(NPG .GT. 0)
      DO IPG = 1, NPG

C---     Récupére les coordonnées du points de Gauss et le poids associé
         IERR = NI_P12L_REQCPG(IPG, NDIMELV, VCOPG)
         IERR = NI_P12L_REQWPG(IPG, POIDSPG)

         CALL NS_MACRO3D_EVALUE( NFNAPPROXUV,
     &                           NFNAPPROXH,
     &                           NDIMELV,
     &                           VCORE,
     &                           VZCORE,
     &                           VDJE,
     &                           VCOPG,
     &                           VDLE,
     &                           VNUV,
     &                           VNH,
     &                           VNUV_XI,
     &                           VNH_XI,
     &                           VDLEPT,
     &                           VDL_XIPT,
     &                           WPT,  !à analyser...
     &                           VW_XIPT, !à analyser
     &                           DJP12L,
     &                           DJP6,
     &                           IP6)

C--      AJOUT DE LA CONTRIBUTION DU POINT

         INTDETJP12L = INTDETJP12L + POIDSPG*DJP12L
         INTU  = INTU + POIDSPG*DJP12L*VDLEPT(NS_IDL_UPT)

      END DO !intégration numérique

C--   ÉCRITURE DES RÉSULTATS
      OPEN (12, FILE='test.fin')
      WRITE(12,*) 'INTDETJP12L'
      WRITE(12,*) INTDETJP12L
      WRITE(12,*)
      WRITE(12,*) 'INTU'
      WRITE(12,*) INTU
      WRITE(12,*)

C---  FERMETURE DES FICHIERS
      CLOSE(12)

      RETURN
      END
      