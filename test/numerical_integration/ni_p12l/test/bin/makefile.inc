#=====================================================================
# $Header$
# $Date$
# $Author$
# $Locker$
#=====================================================================

#=====================================================================
# Description:
#    Fichier include de Makefile pour faire les tests
#
#=====================================================================

# ---  Canonise la variable des cas tests
CAS_TEST:=$(subst ;, ,$(CAS_TEST))
CAS_TEST:=$(subst X,x,$(CAS_TEST))

# ---  Suffixes connus
.SUFFIXES:
.SUFFIXES: .exe .err .fin .out .inp .ini .tst .mkdir .rmdir

# ---  Règles implicites
%.mkdir:
	@if test ! -d $(*F); then \
		mkdir -p $(*F); \
	fi

%.rmdir:
	@if ((test -d $(*F)) && (test $(*F) != 'CVS')); then \
		rm -f -r $(*F); \
	fi

#test.pg: $(TEST_INP)/$(CAS_ACTU).pg
#	@cp $< $@

test.cor: $(TEST_INP)/$(GEO_TEST).cor
	@cp $< $@

test.tgt: ../$(CAS_ACTU).tgt
	@cp $< $@

%.vno: ../../%.vno
	@cp $< $@

%.fin: %.vno
	@cd $(@D); $(TEST_EXE)

%.err: $(TEST_BIN)/cmp.py
	@python $(TEST_BIN)/cmp.py > test.err

%.exe:


# ---  Règles explicites
all:		$(foreach cas,$(CAS_TEST),$(cas).mkdir) $(CAS_TEST) test_err

clean:	$(foreach cas,$(wildcard *),$(cas).rmdir)
	@rm -r -f test.err
	@rm -r -f *.tmp

.PHONY: $(CAS_TEST)
$(CAS_TEST):
	@cd $@; \
	$(MAKE) $(MAKEFLAGS) -f../makefile.mak CAS_ACTU=$@ test_err;
	@echo --------  Rule $@ done

test.fin: $(TEST_EXE) test.cor test.vno
test.err: test.fin test.tgt
test_err: test.err

