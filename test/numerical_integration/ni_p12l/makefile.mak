#=====================================================================
# $Header$
# $Date$
#=====================================================================

#=====================================================================
# Description:
#    Makefile pour faire les tests
#
#=====================================================================

SOUS_REPERTOIRES=$(wildcard *)

export TEST_ROOT=e:/dev/H2D2/test/numerical_integration/ni_p12l
export TEST_BIN=$(TEST_ROOT)/test/bin
export TEST_INP=$(TEST_ROOT)/test/fichiersinput
export TEST_EXE=e:/dev/Debug/test_ni_p12l.exe

# ---  Suffixes connus
.SUFFIXES:
.SUFFIXES: .clean .all

%.clean:
	@if (test -d $(*F)) && (test -r $(*F)/makefile.mak); then \
		cd $(*F); $(MAKE) -f makefile.mak clean; \
		echo --------  Rule $(*F) done; \
	fi

%.all:
	@if (test -d $(*F)) && (test -r $(*F)/makefile.mak); then \
		cd $(*F); $(MAKE) $(if $(MAKEFLAGS),-$(MAKEFLAGS)) -f makefile.mak all; \
		echo --------  Rule $(*F) done; \
	fi

all:	$(foreach rep,$(SOUS_REPERTOIRES),$(rep).all)


clean:	$(foreach rep,$(SOUS_REPERTOIRES),$(rep).clean)


