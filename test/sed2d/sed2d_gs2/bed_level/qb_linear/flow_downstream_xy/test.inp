#-----------------------------------
#  Formulation
#-----------------------------------
h_frm = form('sed2d_gs2')

#-----------------------------------
#  File name
#-----------------------------------
n_fic  = 'test'

#-----------------------------------
#  Mesh
#-----------------------------------
h_cor  = coor(n_fic+'.cor')
h_ele  = elem(n_fic+'.ele')
h_grid = grid(h_cor, h_ele)

#-----------------------------------
#  Nodal properties
#-----------------------------------
h_vno_hydro  = vnod(n_fic+'.pnh')
h_vno_zpre   = vnod(n_fic+'.zpr')
h_vno_tl2    = vnod(n_fic+'.tl2')
h_vno_fl1    = vnod(n_fic+'.fl1')
h_vno_fl2    = vnod(n_fic+'.fl2')
h_vno_fl3    = vnod(n_fic+'.fl3')
h_vno_fl4    = vnod(n_fic+'.fl4')
h_vno_fl5    = vnod(n_fic+'.fl5')
h_prn        = prno(h_vno_hydro, h_vno_zpre, h_vno_tl2,
                    h_vno_fl1, h_vno_fl2, h_vno_fl3, h_vno_fl4, h_vno_fl5)           # regroup nodal properties

#-----------------------------------
#  Elemental properties
#-----------------------------------
h_pre  = 0                      # No elemental properties

#-----------------------------------
#  Global properties
#-----------------------------------
h_pgl  = prgl(9.81,         #  1 gravite
              1.0e+03,      #  2 water density
              1.0e-06,      #  3 water viscosity
              2.65e+03,     #  4 sediment density
              0.2,          #  5 bed material porosity
              1.0e+00,      #  6 active layer thickness
              1.0e-16,      #  7 numerical viscosity - z
              1.0e-16,      #  8 numerical viscosity - fai
              1.0e+00,      #  9 exchange parameter
              0.0e+00,      # 10 bed shear direction correction for helical flow, A
                            #   (common values of A: 7-11) (0 = for no correction)
              0,            # 11 bed shear direction correction for bed slope, 1/fs
                            #   (common values of fs: 1-2) (0 = for no correction)
              1,            # 12 total shear stress formula (1 - Manning law, 2 - Modified manning law)
              1,            # 13 effective shear stress formula (1 - no shear partition (effective stress = total stress),
                            #                                    2 - using Engelund and Hansen, 1967 formula,
                            #                                    3 - using Wu and Wang, 1999 formula)
              0.05e-00,     # 14 dimensionless critical shear stress: varies between 0.023 to 0.06 (used by bedload equation using Wu et al, 200)
              3,            # 15 bed load equation type:
                            #    1)Ackers and White (1973)
                            #    2)WU et al. (2000)
                            #    3)For model testing
                            #    4)For testing sed2d-sv2d connection
              1.0e+00,      #  16 upper size of class 1 sediment in phi units
              2.0e+00)      #  17 upper size of class 2 sediment in phi units

#-----------------------------------
#  Loads
#-----------------------------------
h_slr  = 0
h_slc  = 0

#-----------------------------------
#  Boundary conditions
#-----------------------------------
h_cnd = condition(n_fic+'.cnd')
h_lmt = boundary (n_fic+'.bnd')
h_bc  = boundary_condition(h_lmt, h_cnd)

#-----------------------------------
#  Unknowns
#-----------------------------------
h_ddl  = dof()

#-----------------------------------
#  Simulation data
#-----------------------------------
h_sol  = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_pgl, h_prn, h_pre)

#------------------------------------
#  Stopping criteria, limiter
#------------------------------------
h_limiter = limiter(h_sol, 1, 1, 10)
h_criarret = cria_l2_allrel(h_sol, 0.0, 1.0D-0)

#-----------------------------------
#  Définition des algo de résolution
#-----------------------------------
h_resmat = mkl_pardiso()
h_btrk = 0
h_ressys = picard(h_btrk, h_resmat, 1)

#-----------------------------------
#  Résolution
#-----------------------------------
h_post = 0

t_ini  = 0.0
t_del  = 1.0e-01
t_npas = 10
h_algo = euler(h_ressys, t_del, 0.)
h_sol.init_f(n_fic+'.ini', 0)
h_algo.solve(h_sol, h_criarret, h_limiter, h_post, t_ini, t_del, t_npas)

#-----------------------------------
#  Final results
#-----------------------------------
h_sol.save(n_fic+'.fin')

#-----------------------------------
#  End of calculation
#-----------------------------------
stop()

