//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#include "c_remesh.h"

#include "c_RemailleurH2D2.h"

#if defined(_MSC_VER)
#  pragma warning( push )
#  pragma warning( disable : 1195 )  // conversion from integer to smaller pointer
#  pragma warning( disable : 1684 )  // conversion from pointer to same-sized integral type
#endif

//************************************************************************
// Sommaire:   Constructeur
//
// Description:
//    La fonction <code>C_RMESH_CTR(...)</code> construit un algo
//    d'adaptation de maillage.
//
// Entrée:
//
// Sortie:
//    hndl_t* hndl      Handle sur l'algo
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_CTR(hndl_t* hndl)
{

   RemailleurH2D2* rP = ctrRemailleur();
   *hndl = reinterpret_cast<hndl_t>(rP);

   return 0;
}

//************************************************************************
// Sommaire:   Destructeur
//
// Description:
//    La fonction <code>C_RMESH_DTR(...)</code> détruis l'algo.
//
// Entrée:
//    hndl_t* hndl      Handle sur l'algo
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_DTR(hndl_t* hndl)
{

   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);

   dtrRemailleur(rP);
   *hndl = 0;

   return 0;
}

//************************************************************************
// Sommaire:   Assigne un message d'erreur
//
// Description:
//    La fonction <code>C_REMSH_ERRMSG(...)</code> assigne un message
//    d'erreur à <code>msgErr</code> à partir du code d'erreur <code>ierr</code>
//
// Entrée:
//
// Sortie:
//    F2C_CONF_STRING msgErr   Message d'erreur
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ERRMSG(hndl_t* hndl,
                                                             F2C_CONF_STRING  msgErr
                                                             F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ERRMSG(hndl_t* hndl,
                                                             F2C_CONF_STRING  msgErr)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* msgErrP = msgErr;
   int   l  = len;
#else
   char* msgErrP = msgErr->strP;
   int   l  = str->len;
#endif

   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);
   fint_t ierr = reqErrMsg(rP, msgErrP, l);

   for (char* cP = msgErrP; cP < (msgErrP+l); ++cP)
   {
      if (*cP == 0x0) *cP = ' ';
   }

   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_RMESH_ASGRI(...)</code>
//
// Entrée:
//
// Sortie:
//    F2C_CONF_STRING msgErr   Message d'erreur
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ASGGRI(hndl_t* hndl,
                                                             fint_t* ndim,
                                                             fint_t* nnt,
                                                             double* vcorg,
                                                             fint_t* itpv,
                                                             fint_t* nnelv,
                                                             fint_t* ncelv,
                                                             fint_t* nelv,
                                                             fint_t* kngv,
                                                             fint_t* dblcLmt)
{
   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);

   // ---  Passe en numérotation C
   const fint_t nbrConnecV = (*ncelv)*(*nelv);
   for (fint_t i = 0; i < nbrConnecV; ++i) --kngv[i];

   fint_t ierr = asgMaillage(rP,
                             *ndim, *nnt, vcorg,
                             *itpv, *nnelv, *ncelv, *nelv, kngv,
                             (*dblcLmt != 0));

   // ---  Repasse en numérotation FORTRAN
   for (fint_t i = 0; i < nbrConnecV; ++i) ++kngv[i];

   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_RMESH_ASGLMT(...)</code>
//
// Entrée:
//
// Sortie:
//    F2C_CONF_STRING msgErr   Message d'erreur
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ASGLMT(hndl_t* hndl,
                                                             fint_t* itps,
                                                             fint_t* nnels,
                                                             fint_t* ncels,
                                                             fint_t* nels,
                                                             fint_t* kngs,
                                                             fint_t* info,
                                                             fint_t* nclele,
                                                             fint_t* kclele)
{
   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);

   // ---  Passe en numérotation C
   const fint_t nbrConnecS = (*ncels)*(*nels);
   for (fint_t i = 0; i < nbrConnecS; ++i) --kngs[i];
   for (fint_t i = 0; i < *nclele; ++i) --kclele[i];

   fint_t ierr = asgLimite (rP,
                            *itps, *nnels, *ncels, *nels, kngs,
                            *info, *nclele, kclele);

   // ---  Repasse en numérotation FORTRAN
   for (fint_t i = 0; i < nbrConnecS; ++i) ++kngs[i];
   for (fint_t i = 0; i < *nclele; ++i) ++kclele[i];

   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_RMESH_ASGERR(...)</code>
//
// Entrée:
//
// Sortie:
//    F2C_CONF_STRING msgErr   Message d'erreur
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ASGERR(hndl_t* hndl,
                                                             fint_t* nerr,
                                                             fint_t* nnt,
                                                             double* verr,
                                                             double* hscl,
                                                             double* hmin,
                                                             double* hmax)
{
   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);
   return asgChampErr(rP, *nerr, *nnt, verr, *hscl, *hmin, *hmax);
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_RMESH_AJTERR(...)</code>
//
// Entrée:
//
// Sortie:
//    F2C_CONF_STRING msgErr   Message d'erreur
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_AJTERR(hndl_t* hndl,
                                                             fint_t* nerr,
                                                             fint_t* nnt,
                                                             double* verr,
                                                             double* hscl,
                                                             double* hmin,
                                                             double* hmax)
{
   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);
   return ajtChampErr(rP, *nerr, *nnt, verr, *hscl, *hmin, *hmax);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ADAPT(hndl_t* hndl,
                                                            fint_t* ninfo,
                                                            double* info,
                                                            TTInfoCb cb,
                                                            fint_t* ntgt,
                                                            double* htgt,
                                                            fint_t* rmin,
                                                            fint_t* rmax,
                                                            fint_t* n1,
                                                            fint_t* n2,
                                                            double* acts)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);
   return adapte(rP, *ninfo, info, cb, *ntgt, *htgt, *rmin, *rmax, *n1, *n2, acts);
}


//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQNNT(hndl_t* hndl,
                                                             fint_t* ityp)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);
   return reqNbrNoeuds(rP, *ityp);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQNELT(hndl_t* hndl,
                                                              fint_t* ityp)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);
   return reqNbrElements(rP, *ityp);
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_RMESH_REQGRO(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQGRO(hndl_t* hndl,
                                                             fint_t* ndim,
                                                             fint_t* nnt,
                                                             double* vcorg,
                                                             fint_t* ityp,
                                                             fint_t* nnel,
                                                             fint_t* nelt,
                                                             fint_t* kng)
{
   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);

   fint_t ierr = reqMaillage(rP, *ityp, *ndim, *nnt, vcorg, *nnel, *nelt, kng);

   // ---  Passe en numérotation FORTRAN
   const fint_t nbrConnecGlb = (*nnel)*(*nelt);
   for (fint_t i = 0; i < nbrConnecGlb; ++i) ++kng[i];

   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQNCLLIM(hndl_t* hndl)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);
   return reqNbrLimites(rP);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQNCLNOD(hndl_t* hndl)
{
#ifdef MODE_DEBUG
#endif   // MODE_DEBUG

   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);
   return reqNbrNoeudsLimites(rP);
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_RMESH_REQLMT(...)</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQLMT(hndl_t* hndl,
                                                             fint_t* ncllim,
                                                             fint_t* kcllim,
                                                             fint_t* nclnod,
                                                             fint_t* kclnod)
{
   RemailleurH2D2* rP = reinterpret_cast<RemailleurH2D2*>(*hndl);

   fint_t ierr = reqLimites(rP, *ncllim, kcllim, *nclnod, kclnod);

   // ---  Passe en numérotation FORTRAN
   for (fint_t i = 0; i < *nclnod; ++i) ++kclnod[i];
   for (fint_t i = 0; i < *ncllim; ++i) ++kcllim[i*7+2];

   return ierr;
}

